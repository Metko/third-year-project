# Get average summary vote scores
select avg(sv.score) as 'Average Score', st.name from summary_vote sv
left join summary s on sv.summary_id = s.id
left join text t on s.text_id = t.id
left join user u on sv.user_id = u.id
left join summary_type st on s.type_id = st.id
where sv.user_id != 7
and sv.score != 0
group by s.type_id
order by 'Average Score' desc;

# Get summary vote scores
select sv.score, u.email, t.text, st.name, t.id from summary_vote sv
left join summary s on sv.summary_id = s.id
left join text t on s.text_id = t.id
left join user u on sv.user_id = u.id
left join summary_type st on s.type_id = st.id
where sv.user_id != 7
and sv.score != 0
order by sv.score desc;


# Get average summary vote scores based on summary length
# Multiply the number of sentences not included in the summary by the score assigned by the users
select avg((LENGTH(t.text) - LENGTH(s.text)) / 100 * sv.score) as 'Average Score', st.name from summary_vote sv
left join summary s on sv.summary_id = s.id
left join text t on s.text_id = t.id
left join user u on sv.user_id = u.id
left join summary_type st on s.type_id = st.id
where sv.user_id != 7
and sv.score != 0
group by s.type_id
order by 'Average Score' desc;