INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2046, 'Enhancement of Knowledge and Technology
Transforming Portuguese Talent
Commitment to Knowledge
Each year COTEC’s Board of Directors submits an Action Plan to its associative structures. The initiatives within it are planned by taking the association’s strategic goals into account.
', '2018-12-07 19:24:00');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2050, 'Contact us
About
Research shows that learning in the later stages of life can boost confidence, give a more positive outlook on life and delay on the onset of dementia. L4A believes that there is a gap in provision of educational and learning services to people who live in care settings and seeks to address this gap.
We define learning as ‘a tool for creating wellbeing, a good later life and can be a catalyst for change.’
We define our fourth age as ‘not a numerical age but a time in later life when older people need care and support to go about daily living.’
Our Vision
Older age is a time of development enhanced by learning.
Our Mission
To offer learning opportunities to older people in need of care or support, primarily in Leicester and Leicestershire.
Our Values
At Learning for the Fourth Age (L4A), our values are central. They inform our governance, our work with older people and the impact that we create with them. Our values are:
We see learning as a tool for creating wellbeing and a good later life that can also be a catalyst for change.
We value learning for ourselves as an organisation, as well as learning being our specialism for older people and our volunteers
We empower our older people by offering them real choices in what and how they learn
We are determined to deliver quality, as well as to innovate and find creative solutions to problems
We constructively challenge stereotypes about care settings, older people and what constitutes learning. Older people are never too old to learn.
We share our ideas, our experiences and work with others, as L4A is a demonstration organisation and a thought and practice leader for learning in the fourth age.
L4A believes that there is a gap in provision of educational and learning services to people who live in care homes and seeks to address this gap by:
Encouraging older people to follow up existing interests or develop new ones using a multimedia range of resources.
Supporting learners with their individual needs by a trained learning mentor working with them on a one-to-one basis, using appropriate resources for each learner and taking into account their physical needs.
Promoting the value of education as a tool for increasing wellbeing., giving a more positive outlook on life, increasing confidence and delaying the onset of dementia.
Working towards raising the expectations that residents, relatives and society have for the quality of life and mental stimulus for the elderly, especially for those in care.
Encouraging the Care Quality Commission to strengthen the standards for social care inspection to include mental stimulation as a threshold requirement and one-to-one learning, complemented by group learning as the norm.
Annual reports
Please click the links below to read our most recent annual report or those from previous years.
', '2018-12-07 19:24:00');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2063, 'Need support or information around depression and anxiety,
for yourself, a friend or family member?
Please visit Get immediate support  to contact the beyondblue Support Service and for details of other national crisis and help lines .
The beyondblue Support Service  provides advice and support via telephone 24/7 (just call 1300 22 4636), daily web chat  (between 3pm–12am) and  email  (with a response provided within 24 hours).
How to get in touch with beyondblue
The head office details listed below are for beyondblue corporate and office enquiries only.  
Please note that we are unable to respond to inquiries for assistance with school or university assignments, as we don’t have the staff/resources to meet the demand. Please browse The facts , Who does it affect?  and Healthy places  sections of this website for information on anxiety and depression, and the  About Us  section for information on beyondblue as an organisation.
If you''d like to make a complaint, please visit our complaints page .
beyondblue head office details 
', '2018-12-07 19:24:01');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2063, 'Who we are and what we do
Who we are and what we do
Our vision, values and mission
Our vision
All people in Australia achieve their best possible mental health.
Our values
Collaboration. Respect. Enthusiasm. Excellence. Innovation. Integrity.   
Our mission
We promote good mental health. We create change to protect everyone’s mental health and improve the lives of individuals, families and communities affected by anxiety ,  depression  and suicide .
Our key result areas
Community heart. Business head. beyondblue will adopt a community heart and a business head to achieve our goals:
Reduce the impact of anxiety, depression and suicide by supporting people to protect their mental health and to recover when they are unwell.
Reduce people’s experiences of stigma and discrimination.
Improve people’s opportunities to get effective support and services at the right time.
Use best business practices to deliver integrated, evidence-based and cost-effective initiatives through our people and resources.
How we do it
Mental health conditions don’t discriminate, and neither do we. Our programs and initiatives support people at all stages of life, wherever they live, work, study and play. 
We listen and respond. We will place a high priority on seeking out, listening and responding to the experiences of people affected by anxiety, depression and suicide, and combine this with evidence generated by researchers and evaluators.
We connect with people and advocate for positive change.
We work in all States and Territories, aiming for our campaigns, communications, resources and programs to be as accessible in remote communities as inner metropolitan suburbs, to:
Inform and connect people to enable them to achieve their best possible mental health and access support when they need it.
Influence and challenge discriminatory behaviour by advocating for positive change and prompting discussions across Australia.
Innovate and initiate effective ways to improve access to support and improve outcomes for people, families and communities.
Three million Australians are currently experiencing anxiety or depression. 1  Every day, nearly eight people take their own lives. 2
Because this affects all of us, we’re equipping everyone in Australia with the knowledge and skills to protect their own mental health. We’re giving people the confidence to support those around them, and making  anxiety ,  depression  and  suicide  part of everyday conversations.
And as well as tackling stigma, prejudice and discrimination, we’re breaking down the barriers that prevent people from speaking up and reaching out.
We''re here for everyone in Australia – at  work , home,  school ,  university ,  online , and in communities across the country.
', '2018-12-07 19:24:01');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2063, 'What can I do?
What can I do?
There are varying types of support that can be provided during this time, both practical and emotional.
Practical support
Remember that this type of bereavement is long-term and you will not be able to ''fix'' it or make it go away. People need assistance and support, usually for a long period of time, as they come to terms with what has happened.
Try to help attend to the things that might get left behind during this difficult period. For example, help look after children or cook meals occasionally.
Offer to do something specific, for example, “I could come and mow the lawn”, “I could look after the pets for a while” or just bring some food or a meal. Many bereaved people will find it difficult to ask for assistance and they may also have difficulty making decisions or identifying ways you can assist. Be proactive and simply offer assistance.
Emotional support
Be aware that the person is having a really hard time. Respect their right to grieve and accept the intensity of the grief. Allow them to grieve in the way that is most natural and comfortable for them and provide support that is helpful. There is no one right way to grieve.
Some other ways you can provide support:
Contact the person when you hear of the death. Tell them you are sorry to hear of their loss, or send a card or flowers. If you don’t know what to say, you can just write ‘thinking of you’.
Maintain contact personally or by telephone, text, notes, cards. Visits need not be long.
Listen: This is possibly the most important thing you can do.
Invite them to talk about the person who has died, mention the person''s name, ask to see photos, share stories.
Accept their behaviour – crying, being quiet, laughing. Allow expressions of anger, guilt and blame.
Offer specific practical help, such as bringing in a cooked meal, taking care of the children, cutting the grass, shopping.
Really try to understand and accept the person. Everyone is different and a range of responses are normal.
Be patient. People may need to tell their story over and over again without interruption or judgment. This helps them to come to terms with, and make sense of, what has happened.
Include children and young people in the grieving process and be aware that they may have particular need for support.
Be aware of and acknowledge special times that might be significant, and particularly difficult, for the bereaved person such as Christmas, anniversaries, birthdays, Father''s Day, Mother''s Day.
Realise your feelings of awkwardness and helplessness are normal. Just listening and ''being with'' the person who is grieving can be a powerful support.
Look after yourself. Set limits as you need. To support a grieving person you need to maintain your own wellbeing.
Other pages in This Section
', '2018-12-07 19:24:01');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2063, 'Knowing what to say to a person bereaved by suicide can be a big challenge.
A few guidelines are listed below:
Try not to say ''committed'' suicide. This language harks back to historic times when suicide was a crime and a mortal sin and some bereaved people find it distressing and stigmatising. You can say ‘died by suicide’, ‘suicided’, or ‘took their life’.
Do not use clichés and platitudes to try and comfort by saying things like “you''re so strong”, “time will heal”, “he''s at peace now”, “you have other children”, “you''ll get married again” or “I know how you feel”. While well-intentioned, they rarely comfort and can leave the bereaved person feeling misunderstood and more isolated.
Don''t avoid the subject of suicide. This can create a barrier making it hard for them to discuss personal issues later.
Avoid judgments about the person who suicided such as saying they were selfish, cowardly or weak, or even brave or strong. People need to come to their own understanding of the person and what has happened.
Avoid simplistic explanations for the suicide. Suicide is very complex and there are usually many contributing factors.
Listen and hear their experience.
Be truthful, honest and aware of your limitations: acknowledge if you don''t understand or know how to react to what they are going through.
Say the name of the person who has died and talk about them. Not saying their name can leave the bereaved feeling as though the one who died is being forgotten or dismissed.
Be aware of those who are grieving who may be overlooked, for example, siblings, children, grandparents, and friends.
Ask "How are you getting along?" and then really listen to the response. Stay and listen and try to understand. Allow the person to speak whatever they need to say however difficult and complex it is.
Other pages in This Section
', '2018-12-07 19:24:01');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2063, 'Contact us
Stay in touch with us
Sign up below for regular emails filled with information, advice and support for you or your loved ones.
First name
Last name
Email address
I agree to receive email communications from beyondblue (you can unsubscribe from this at a later date if you wish).
Sign me up
All done! You should’ve received a confirmation email, so please check when you’re finished here and click the link in the email. If you can’t see it, we might be in your junk mail.
Subscribe failed. Please try later or contact us.
Your session is about to expire. You have 2 minutes left before being logged out. Please select ''ok'' to extend your session and prevent losing any content you are working on from being lost.
Talk it through with us, we''ll point you in the right direction
', '2018-12-07 19:24:01');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2065, 'So, What do YOU do?
By Ant Meads - 24th November 2014
What do you do? It’s a question I’ve been bracing myself for over the last few months. I’m in a new relationship and at that point of meeting friends and family, the question is sure to arise. When you haven’t worked for over 2 years due to severe OCD and depression it’s rather tough to put a positive spin on things. ‘Oh, I’m between jobs right now’ or ‘I’m taking a sabbatical,’ only seem like reasonable answers if you can then answer the follow up question, ‘What do you plan to do next?’ ‘Sit in my pants watching Netflix and eating my body weight in Coco Pops’ probably isn’t the response people want to hear.
The truth is, when you are struggling with mental health issues, you spend a lot of your time justifying yourself. Not just your future plans but your entire existence. When Lord Freud said ‘some disabled people aren’t worth minimum wage’ my heart sank. He may have apologised but we know what he meant and we know much of our society agrees with him. We seem to live in a world where our only barometer of self-worth is our job and our position in the economy. When you are asked what you do, very few people want to hear ‘I can’t work and I rely on benefits.’
I find myself complaining about my illness and my situation on Twitter. I can’t help myself. It’s not even so much about my own misery feeling so ill at times, but about defending myself. I see friends tweet about their work, how their day has gone, what they have planned for later and I almost feel obliged to tweet an excuse for why I won’t be doing any of that for some time. ‘Oh, I can’t stop sleeping’ I’ll tweet. It’s true, my new medication knocks me for six and I spend lots of time in bed. My depression certainly doesn’t help matters there. But what I really want to say on Twitter is ‘I’d love to be working, I’d love to have plans to see friends tonight but I’m just too ill.’ Every time I tweet about my illness what I’m really saying is ‘I’m sorry I can’t contribute more to the world.’
I have recently been awarded Personal Independence Payments. It’s taken about 6 months of sheer hell, waiting for answers, dealing with the stress of it all, including the financial implications of it taking so long in the first place. When I received the letter with the award, I cried. Not for me but for the people I know still struggling in the system. The people who have a medical assessment next week, or who have just been signed off work and could be starting on a journey I’ve been on for some time now. I have a kind of survivor’s guilt. I question myself, ‘am I worth it?’ do ‘I deserve to get these benefits?’ Never mind that all I’m receiving is what I’m entitled to. The system is designed to keep you quantifying your contribution in life. The questions you are asked make you form such a low opinion of yourself, as you have to admit to needing prompting to change your dirty clothes after 3 days. Or that you need help to walk down the road and get a pint of milk because you are so terrified of the outside world that you daren’t leave your house alone. You finish the process asking yourself what contribution you bring to society and the conclusions you draw aren’t pretty.
Of course this entire piece is me justifying my life, my right to exist, just on a larger platform than Twitter. I convince myself that if I can write enough articles and blogs about mental health then I may make a small difference in the world. Then I can say I contributed.
What do I do? I survive. For now, that’s enough.
Related issues
', '2018-12-07 19:24:01');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2076, 'Contact Us
You can find answers to common queries on our FAQ page .
If you wish to get in touch with us, there are a number of ways to do so.
Feedback
You can contact FutureLearn support via our feedback tool, Zendesk. Just press the Support button in the bottom right corner of any page in a course. There you can find answers to common questions, leave comments, vote on popular ideas that you’d like to see built, and generally help us improve FutureLearn. If you don’t see the button you can email your comments, questions or ideas to feedback@futurelearn.com .
If you are having technical problems with FutureLearn, please check our Status Blog to see if it is an issue we are currently fixing.
If you would like to make a complaint, please see our complaints procedure .
Media and Press Enquiries
Please send an email to  media.enquiries@futurelearn.com . You can find more information in our Press area.
Becoming a Partner
Interested in becoming a FutureLearn Partner?
Email us at  partner.enquiries@futurelearn.com .
Your stories
Have a great story about your experience on FutureLearn? Send us an e-mail at yourstories@futurelearn.com . We’d love to hear from you.
', '2018-12-07 19:24:02');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2076, 'Home › Contact us
Contact us
If you have a query, you might want to check our frequently asked questions . If you still need to get in touch about an order or something more general, email us at orders@futurelearn.com with your order number if necessary.
 
', '2018-12-07 19:24:02');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2108, 'Why not follow, like or link in with us on social media?
Follow Us:
', '2018-12-07 19:24:05');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2108, 'About  Us
We are co-creating the next generation of sustainable health solutions that improve access to health.
Our objective is to serve humanity’s most pressing needs. Our team is driven by a passion to solve a social problem, and we choose to use business as a mechanism to solve these problems.
We co-create ! Co-creation happens at the intersection of the social and business sectors, in order to serve unmet needs of individuals and communities at scale.
Since 2011, we are a team of engineers, doctors and designers working together to improve care delivery. We value transparency, creativity, curiosity and persistence.
We are developing mhealth solutions
for caregivers and people living with dementia
for people living with type 2 diabetes helping them to maintain good metabolic control in order to prevent chronic complications.
to help people keeping High Blood Pressure under control.
We apply agile methodology when flexibility can propel far more efficient results. We know how to do it and we do it well!
Who are we?
', '2018-12-07 19:24:05');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2112, 'Copyright ©2014-2018 DiabetesLab. P.IVA 02347840221
Some icons thanks to FreePik
', '2018-12-07 19:24:05');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2112, 'to give you the best solutions
About us
Home
About us
DiabetesLab is an innovative startup where passion and creativity are indispensable for                             any project. Our team is young, international and creative, and nothing is impossible for us.                             There are only challenges.
Przemek, one of our team members, has lived with diabetes for 18 years. This is why we are                             working on solutions that facilitate the daily lives of people who live with insulin-dependent                             diabetes (type 1 diabetes).
Our diabetes solutions use an interdisciplinary approach and combine knowledge from multiple                             fields, including medicine, IT and engineering.
We have experience of working with a number of European projects and collaborating with the                             academic world. Thanks to that, we have a deep understanding of new technologies and data                             analytics.
Our awards:
July 2014: won one-year funding as a result of the program TechPeaks - The People Accelerator in Italy.
September 2014: third place in the Pirate                                 Summit pitch competition in Germany.
December 2014: were among the finalists invited to the European Venture Summit in Germany.
DiabetesLab
', '2018-12-07 19:24:05');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2124, 'Follow Pass It On Network
Who We Are, Where We Are
Our dream is to see Pass It On Network programs in use in every country of the world. Think about what you can do to help us achieve that goal. We need to identify country liaisons and linguists who are able to communicate in both the regional language and English. If you want to help us please contact Moira.
Founders
Moira Allan
We look forward to expanding the group of Advisors worldwide to enrich our impact through diversity. If you are interested in contributing, please contact Moira . Advisors will be asked to help assess the performance and sustainability of the Pass It On Network.
Advisors
', '2018-12-07 19:24:05');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2124, 'Follow Pass It On Network
What We Do, Why We Do It
Many older adults have been asked about their basic values: “What values matter the most to you?” Consistently, they talk about the importance of self-determination — making decisions for themselves. Then they tend to talk about maintaining maximum feasible self-sufficiency and wellness. Community participation is important, along with interaction with family and friends. And doing some meaningful work, paid or unpaid, contributes to healthy aging — and to economic security, as needed.
The Positive Aging movement stresses the importance of encouraging older adults to be proactive in support of all of the factors contributing to healthy aging: finding passion and purpose, creative expression, meaningful work, lifelong learning, community recognition and accessibility, physical and mental exercise. The Pass It On Network emphasizes the capacity of older adults to self-organize and help themselves and others find or create what’s needed to thrive. This website has been developed to encourage older adults to care for themselves and each other and their communities.
Here are some unique attributes of the Pass It On Network that fit current global needs of our aging society:
Asset-based programs complement the needs-based services of current systems
Older adults doing for themselves save public expenditures in a time when the world is aging and budgets are tight
All of the programs depend on older adults volunteering to share their strengths
Our Vision
Positive, productive aging worldwide. As older adults we will share our strengths with others to help ourselves, each other and our communities as much and as long as is feasible.
 Our Mission
Spread information worldwide about Positive Aging programs led by older adults who share their strengths to help themselves, each other, and their communities.
Develop an on-line directory of program guides to encourage program replication around the globe that is adaptive to local language, culture and needs.
Our Objectives
Develop a user-friendly, multi-lingual Internet platform at www.passitonnetwork.org
Identify leaders of innovative Positive Aging programs around the world who are willing to share detailed program information that will guide adaptive replication, for no charge
Post information about low-cost Positive Aging programs shown to be successful in achieving one or more of the following goals:
Developing peer-to-peer and/or intergenerational support networks
Creating pathways to meaningful work, paid or unpaid
Expanding opportunities for teaching and learning
Promoting education and advocacy for Positive Aging (e.g., celebrating role models, advocating for self-determination, encouraging wellness)
Recruit Country Liaisons as volunteers who will help with translation and encourage: a) current program leaders in their countries to submit program information; and b) prospective leaders to make use of the programs on www.passitonnetwork.org
Work with scientific researchers to evaluate the efficacy of the Pass It On Network programs and www.passitonnetwork.org and report on the impact they are having.
Encourage the organic growth of the network by celebrating successful programs and sharing information about the network in a collaborative, open source manner.
Find ways of self-financing the www.passitonnetwork.org infrastructure (International Coordination, Internet Manager, expenses for travel, etc. for Country Liaisons and Advisors)
Why Now?
Many factors make it vital that we invent how to live this new phase of life beginning at age 50+. So rich in promises and challenges, the second half of life calls for us to be bold, imaginative and caring and to realize that  “We are the ones“ who need to self organize and help ourselves, others and our communities.
The statistics are clear: Never in the history of mankind have there been so many 60+ year-old citizens worldwide. We are the only growing age group. Our sheer numbers along with economic turmoil and budget shortfalls make it impossible for nations to scale up existing facilities and services to meet our demands. We need to be very aware that we have to fend for ourselves.
We believe the work of elders is to “pass it on”, to share our know-how with others, to capitalize on what works, to save money and to shape new expectations for lifelong working, learning and sharing.
Proof that we are on the right track came from our involvement with the World Café Europe’s EVAA project (European Voices for Active Aging) funded by the European Commission. Six thematic cafés were organized to coincide with the “2012, the European Year for Active Aging and Solidarity between Generations. They were held in Spain, Germany, the Czech Republic, Italy, the United Kingdom, and in France in 2012. More than 600 seniors in these countries showed they wanted to carry on playing a positive role in society.
Click here to see the full color report to the European Union.
Richard Adler, The Institute for the Future.
Further proof that we are on the right track has come from Richard Adler, a futurist who explains in this TedX video why it is vital to develop self-supporting ground roots projects to meet the multiple challenges of the longevity revolution. “No government in the world has the means to upscale its existing facilities to meet the growing needs of its  + 60 population”.
Our Story
We Are the Ones — Working Together
by Jan Hively and Moira Allan
Dr Jan Hively, now a young octogenarian from Cape Cod in the US, and Moira Allan, 67, from Paris, France, met at the first Positive Aging Conference at Eckerd College, in Saint Petersburg, Florida, US, in 2007.  Moira has just founded 2Young2Retire-Europe / Cercle des Seniors Actifs-Europe. The Pass It On Network has grown out of their friendship and common passion for “meaningful work, paid or unpaid, through the last breath.” In 2009, Jan and Moira proposed developing a global “We are the ones” network of elders, inspired by the Hopi Indian leader who said, “We are the ones we’ve been waiting for.”
Download the poem by the Hopi American Indian leader who inspired this movement.
From 2010 through 2012, Jan and Moira teamed with Patricia Munro and World Café Europe to engage older adults in discussions about issues relevant to an Active/Positive Aging network. In February 2013, they presented the concept of a “Pass It On Network” at an Encore.org Summit Conference, and began reaching out to Positive Aging advocates who might fill the role of liaisons stimulating “do it yourself” involvement in their home countries. And here we are!
Download the story about this history that was published by Second Journey in its online publication Odysseys of the Soul 2013 n° 1 .
Time Line: Where are we heading?
2008–2011
The Pass It On Network emerged from “We are the ones.” See the poem by the Hopi American Indian leader who inspired this movement.
2012
The Pass It On Network founders participated in 6 thematic cafés engaging seniors in Bilbao, Bonn, Prague, Bologne, London and Strasbourg through the World Café Europe “Voices for Active Aging project.”
2013, Feb
The Pass It On Network went public at the Encore Summit Conference in San Francisco. See Jan’s Big Idea .
2013, Oct
The www.passitonnetwork.org site is up and running
2013, Nov
Important consolidation meeting in Paris, November 2013 with co-constructors, Old’Up and International Longevity Centre-France.
2014,
Feb 9-12
Presentation of the Pass It On Network and official U.S. launch at the 7th Positive Aging Conference in Sarasota, Florida
2014,
IFA 12th Global Conference on Aging in Hyderabad, India, on Health, Security and Community
2015,
July 26-29
Progress Report and official Global Launch at the International Conference on Positive Aging supported by the World Health Organization in Johannesburg, South Africa
2016
Growth to include representation on each continent
2017
Further international growth
Call for Support from the Founders Jan and Moira
We started planning the Pass It On Network a number of years ago, with the assumption that grant funds would be plentiful to support the work. But times have changed. The economy is in turmoil and budget shortfalls are plentiful. Many of us feel anxiety about economic security. And up until now, we haven’t been able to find funding to develop and market this global market exchange.
So now, it’s even more important to launch this effort that calls on the strengths and skills of all of us who are living out the promises of Positive Aging. “We are the ones we have been waiting for.”  The only way we are going to achieve the vision for the Pass It On Network is to reach out for support to our friends and colleagues, children, grandchildren… and all of you.
Please help us launch the Pass It On Network.
 
', '2018-12-07 19:24:05');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2137, 'Learn more
Programs
Generations United works to promote high-quality intergenerational programs that improve the lives of participants of all ages and their communities. Through resource development, awards, and recognition, Generations United highlights intergenerational solutions to community challenges.
Programs
Generations United is a strong voice supporting the development and expansion of intergenerational programs bringing children, youth and older adults together. We serve as the clearinghouse for information on different types of intergenerational programs and a champion making the case for local programs across the county and around the world.
Learn more
Public Policy
Generations United advocates directly for federal and state policies and programs that build intergenerational connections and meet the needs of babies, children, youth and older adults. We also provide information and tools to engage Generations United’s broad and diverse membership to protect and prioritize investments that build toward a more secure future for all generations.
Public Policy
Generations United believes that public policy should meet the needs of all generations and that resources are more wisely used when they connect generations rather than separate them. We promote an intergenerational approach to framing public policies that impact children, youth and older adult issues.
Learn more
Technical Assistance
Generations United provides custom technical assistance, workshops, training, and webinars on intergenerational issues and opportunities for businesses, government and non-profit organizations.  We convene the largest international intergenerational conference every other year.
', '2018-12-07 19:24:05');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2137, 'Donate Now
Contact Us
Generations United welcomes your involvement and questions. For inquiries, please use the form below or contact us at:  25 E Street NW, 3rd Floor | Washington, D.C. 20001 | 202-289-3979
Name
', '2018-12-07 19:24:05');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2137, 'for intergenerational strategies in the U.S.
Our Results
Generations United is a proven, effective advocate for children, youth and older adults in communities and in Washington, DC. Founded in 1986 by leaders at the National Council on Aging, Child Welfare League of America, Children’s Defense Fund and AARP, Generations United''s work inspires, empowers, advocates and engages.
', '2018-12-07 19:24:05');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2145, 'London,
WC1R 4HE
If you would like any further information or have any queries regarding our services please do not hesitate to get in contact with our team.
T: 0207 400 8621
', '2018-12-07 19:24:06');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2145, 'Home | About Us | PhD Studentship
PhD Studentship
The ukactive Research Institute working in collaboration with  Coventry University  has hired three PhD candidates to join a research unit focused on collaboration with the wider physical activity sector and delivering real world impact. The three PhD studentships are fully funded, with the successful students having opportunity to complete their studies whilst engaging with industry partners, national policy makers, and wider academic networks. 
There are three areas of study are outlined below:
1. Exercise Referral
To tackle the inactivity pandemic, the activity sector in the UK has been called upon to design and implement programmes with a focus on engaging inactive people, provide inclusive services aimed at reducing health inequalities, and work to refine physical activity programmes, interventions and services.
We will investigate consumer adoption and retention from a variety of angles, including economic, behavioural and psychological. The aim will be to provide an explanation of the factors affecting consumer adoption and retention and ultimately trial data informed strategies aimed at delivering effective behaviour change. Previous research has commonly used unrelated or indirect factors such as trust and commitment, price terms, and loyalty terms. Such studies lack a clear theoretical background and a solid empirical proof to support their findings of consumer operant retention behaviour.     
Position now closed. 
2. Physical Activity Adoption and Retention
This project aims to build an evidence base for the use the of exercise and physical activity programmes in the improvement of quality of life, prevention, and management of disease. This is achieved by engaging with the physical activity sector to encourage best practice in data capture, management, and dissemination. 
The student will seek to understand the impact of exercise on referral in the UK, the determinants of its success or failure to deliver clinically relevant outcomes, and develop recommendations for best practice that can be shared directly with operators and wider industry stakeholders.
Position now closed. 
3. Children’s Fitness Testing 
Following on from Generation inactive project is to test the feasibility of delivering cardiorespiratory fitness testing in a primary school environment, gain a deeper understanding of children’s physical activity habits, and inform public policy as a result.
This programme of study will inform parents, teachers, Ofsted and Public Health England in relation to the fitness levels of children and the way in which it can be improved / monitored, and consequently providing longer term benefits to the child. Further, data will act as a benchmark for further investigatory work.      
Together ukactive and Coventry University will be able to use its unique position as the hub for the physical activity sector, to disseminate data and key learnings to practitioners, operators, policymakers, local government, and health agencies. 
Position now closed. 
If you would like any  further information  please contact the research team on research@ukactive.org.uk.
Our Partners
', '2018-12-07 19:24:06');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2145, 'The ukactive Research Institute works alongside the following academic institutions:
 
', '2018-12-07 19:24:06');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2145, 'Home | About Us
About Us
The ukactive Research Institute aims to build an evidence base for the use of exercise and physical activity programmes in the improvement of quality of life, and the prevention and management of disease. Our primary objective is to bridge the evidence gap between traditional laboratory based ‘exercise is medicine’ research and realworld interventions.
This is achieved by conducting research assessing the effectiveness of interventions on directly-measured physical activity levels, clinically-relevant markers of cardiovascular and metabolic health, mental well-being, economic development and other core variables across a variety of real-world settings.
The Institute is in a unique position to disseminate data collected and the key messages gathered to those at the coal face of health and physical activity delivery. ukactive has a network of over 4,000 members and stakeholders, and is in a position to communicate findings to those members, creating real impact.
How We Support our Members
We provide a number of services for members, the main categories for which are:
›› Research and Evaluation
', '2018-12-07 19:24:06');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2145, 'CONTACT US
”ukactive exists to improve the health of the nation by getting more people, more active, more often. ukactive provides services and facilitates partnerships for a broad range of organisations, all of which support our vision and have a role to play in achieving that goal.
We serve over 4,000 members and partners from across the public, private and third sectors, from multinational giants to local voluntary community groups. We do so by facilitating partnerships, campaigning and providing outstanding membership services.
Steven WardCEO
Improving the health of the nation
Together achieving more
Our Mission and Vision
Our long-standing and uncompromising vision is to get more people, more active, more often. We are committed to improving the health of the nation through promoting active lifestyles.
We achieve this by facilitating big impact partnerships, campaigning and providing world class membership services. We exist to serve any organisation with a role in getting more people, more active, more often.
We provide a supportive, professional and innovative platform for our partners to succeed in achieving their goals and create the conditions in which our sector can grow. ukactive – together achieving more
Testimonials
“Once again the ukactive National Summit doesn’t disappoint. An event that is designed to tackle the real issues that the industry faces around the physical state of the nation. Active Nation as regular supporters of the Summit see this as a ''must-attend'' in order to design and create meaningful and relevant communication messages to campaign to persuade people to be more active. I would have no hesitation in recommending this event!"
”
Stuart MartinDirector of Active Nation
“By far the best team I''ve ever worked with. They truly understood the look I was going for and completely nailed it! I would highly recommend them as a company, you simply just won''t find any better team!”
Aura BrooksGraphic Designer
“Our learnings from the summit helped to inform the role of Inspiring healthy lifestyles working with GP clusters in Wigan Borough on integrating wellbeing, health and social care and the development of Healthier Wigan Together, the integrated care organisation, joining up services for residents to help them manage their own care, and stay well and independent within their communities.
Collectively we continue to present our joined up approach across Wigan, Greater Manchester and nationally, strengthening our co-design of solutions for increasing physical activity. We involve ukactive as key external contributors to projects across Wigan Borough, something we’re looking to extend across Greater Manchester moving forwards.”
”
Peter Burt Managing Director of Inspiring Healthy Lifestyles
“It was an absolute pleasure to spend time with such an engaging and dedicated group of leaders at the first ukactive Global Active Leaders event in Madrid in February. It’s not often that you have the opportunity to step back and reflect on your own role and leadership style in such an environment. We could explore, discuss, analyse and share ideas from both within and outside of our sector regarding leadership effectiveness and excellence. The standard of delivery by all the professors at IESE Business School was outstanding, and it was a pleasure to observe their energy and ability to facilitate and guide group discussions to get the very best out of each session. I would highly recommend the experience that ukactive has provided for our sector leaders in partnership with IESE to any leader who continually strives to be better.”
”
Jenny PatricksonManaging Director of Active IQ
“There’s no question about it – the UK is really getting hot over the boutique market. Sweat, as ukactive has proven for the second year now, is the place to get the insights, to understand what’s going on and to get the latest trends, whether local or global.”
”
Dave WrightCEO of MyZone
“One of the quotes a professor said was “Leadership you cannot touch it but you can feel it “ and after the week with some great people from within the industry this quote sticks with me as a summary of my time in Barcelona and the future leaders programme.”
”
Hugh HanleyVirgin Active
“I’ve noticed that the more I do, the more I’m capable of doing. I don’t feel so out of breath and I feel much fitter. When you haven’t exercised for a while, your joints start to ache from lack of movement, but I’m pleased to say that my joints have got used to moving and I can keep on going. Exercise gets your heart going and as a result, it gets you fitter!”
”
', '2018-12-07 19:24:06');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2147, 'Home ›About us
About us
CSR Europe is the leading European business network for Corporate Social Responsibility. Through our network of  45 corporate members  and  41 National CSR organisations , we gather over 10,000 companies, and act as a united platform for those businesses looking to enhance sustainable growth and positively contribute to society. In our mission to bring the sustainability agenda forward, we at CSR Europe go beyond European borders and cooperate with CSR organisations in other regions across the world. CSR Europe is the European hub incubating multi-stakeholder initiatives that tackle the UN 2030 Agenda for Sustainable Development.
', '2018-12-07 19:24:07');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2153, 'Page Content
Latvian Information and communications technology association​ – LIKTA​​​
Latvian Information and communications technology association (LIKTA) was founded in 1998 and it unites leading industry companies and organizations, as well as ICT professionals – more than 160 members in total.
The goal of LIKTA is to foster growth of ICT sector in Latvia by promoting the development of information society and ICT education thus increasing the competitiveness of Latvia on a global scale. The association provides professional opinion to government institutions on legislation and other issues related to the industry, while also maintaining close relationships with other Latvian NGOs and international ICT associations.
In 2012 six main goals for ICT industry were defined in the Charter of LIKTA​ , whi​ch will help the society to become more innovative and technologically educated.
1. Empowered e-citizen: using digital technologies an e-environment, which would allow the citizens of Latvia to use all the services that are available in real environment – in electronic environment, thus reducing the bureaucratic burden and improving administrative processes. 
2. Smart e-citizen: it is necessary to develop e-skills of the general public to allow everyone to use e-environment infrastructure and benefits provided by it. 
3. The most modern e-government in Europe: a modern e-government provides access to public services to everyone, including the groups of people that do not use modern technologies in their day to day lives. Efficient e-government on municipal and state level will be ensured by integrated and intuitive services.
4. Competitive business environment: organized and efficient technologies provide business infrastructure and environment that free the entrepreneurs from waste of resources by promoting productivity, competitiveness, growth of exporting ability and contribution to state economy. 
5. Active ageing: by promoting the technology as an instrument to improve quality of life in all fields and at any age, not only the principles of inclusive society would be developed in e-environment, but also the access and security of health care data would be ensured, as well as the opportunities for lifelong education.
6. Easy access to cultural heritage: by identifying and popularising the cultural heritage both in Latvia and abroad the digital technologies not only increase the national self-confidence, but also integrate the cultural heritage in education. 
Technology development significantly improves the competitiveness of all fields of economy. Information and communication technologies have increased added value and has one of the highest growth potential in Latvian export.
During its years of operation LIKTA has grown from a small organisation to a representative association talking on behalf of the whole industry. Our organization is unique due to the fact that both individual ICT professionals and the largest businesses of the field are members of LIKTA.​
 
Stabu iela 47-1, Rīga LV-1011
Phone: +371 67311821, +371 67291896
Email: Office@likta.lv
', '2018-12-07 19:24:07');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2159, 'About Us
Who  we  are  and  what  we  do
Welcome to Women Like Us. We are a multi award-winning social business that works to build a better future for women who want to carry on working after they''ve had children.
Our aim is to give women the choice to fit work around the needs of their family, without losing their value in the workplace. We:
Provide a range of free online resources for women, prepared by professional career coaches.
', '2018-12-07 19:24:08');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2159, 'London SE1 1NL
How to find us:
We are a 5 minute walk from from both London Bridge or Borough stations. Find your way to Borough High Street from whichever station you use. When you arrive at Three Tuns House, you will need to buzz our sister company ''Timewise'' to reach our office reception from the lobby. Someone from the team will come down to collect you from the reception area.
Women Like Us is a division of Timewise Foundation C.I.C, a limited company registered in England and Wales. Company number 5274371 and VAT registration number 190590594.
Name
', '2018-12-07 19:24:08');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2165, 'Thank you for contacting us.
We will get back to you as soon as possible
Oops, there was an error sending your message.
Please try again later
', '2018-12-07 19:24:08');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2165, 'About us
More than words on a page
The mission, vision and values of Western Home Communities provide daily guidance for decision making, whether we''re creating strategic vision for the future or responding to a resident''s request. 
Western Home Communities does not and will not discriminate against any person on the grounds of race, color, religion, sex, national origin, age, disability, creed, gender identity, sexual orientation or any other basis regarding the admission, retention, treatment, and terms and conditions of residing in Western Home​ ​Communities as long as the services needed can be properly provided by the organization.  Western Home Communities has a stated mission for serving seniors and the elderly; selected facilities of the organization, therefore, have stated age entrance requirements.
Western Home Communities and its programs and activities are accessible to and usable by disabled persons, including persons with impaired hearing and vision, in accordance with Section 504 of the Rehabilitation Act of 1973.
Western Home Communities is proud to be a member of LeadingAge and the American Health Care Association/National Center for Assisted Living.   
Share by:
', '2018-12-07 19:24:08');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2166, '* Enter the code in the box below:
Information
', '2018-12-07 19:24:08');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2166, 'Home » About Us
About Us
The Eden Alternative® is an international, non-profit 501(c)(3) organization dedicated to creating quality of life for Elders and their care partners, wherever they may live.
Information
', '2018-12-07 19:24:08');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2170, 'We will get back to you as soon as possible.
Shop enquiries
The shop team are happy to help with any enquiry about ordering from us that you have.
', '2018-12-07 19:24:08');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2170, 'About us
About us
House of Memories is a museum-led dementia awareness programme which offers training, access to resources, and museum-based activities to enable carers to provide person-centred care for people living with dementia.
Museums are experts at recording and caring for people’s memories. Using our experience in reminiscence work, as well as access to museum objects, House of Memories’ unique and innovative training and resources support carers to creatively share memories with the people living with dementia that they are caring for.
“Thank you for the start of my understanding and feelings of how I can help on the journey of dementia (locally at home and with family)”
Tina, family carer
', '2018-12-07 19:24:08');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2170, 'Contact the shop team
We are happy to help with any enquiry about ordering from us that you have.
If you have a query about your order or would like more information about ordering a large number of publications please contact onlinesales@liverpoolmuseums.org.uk or phone our online shop team on 0151 478 4736.
National Museums Liverpool
', '2018-12-07 19:24:08');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2171, 'To create a vibrant care community enabling people to live well and age gracefully.
What we do
We reach out to caregivers and seniors with information on staying active and ageing well, and connect people to services they need. We support stakeholders in their efforts to raise the quality of care, and also work with health and social care partners to increase services for the ageing population. Our work in the community brings care services and information closer to those in need.
', '2018-12-07 19:24:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2171, 'For Service-related matters, please send your feedback using the online form .
Join Team AIC
To create a vibrant care community enabling people to live well and age gracefully.
Stay Updated!
', '2018-12-07 19:24:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2172, 'For Service-related matters, please send your feedback using the online form .
Join Team AIC
To create a vibrant care community enabling people to live well and age gracefully.
Stay Updated!
', '2018-12-07 19:24:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2172, 'To create a vibrant care community enabling people to live well and age gracefully.
What we do
We reach out to caregivers and seniors with information on staying active and ageing well, and connect people to services they need. We support stakeholders in their efforts to raise the quality of care, and also work with health and social care partners to increase services for the ageing population. Our work in the community brings care services and information closer to those in need.
', '2018-12-07 19:24:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2209, 'Enter the word you see in the image.
Submit
Volunteer with Us
Want to make a difference in your community and in people''s lives? Our programmes rely on hundreds of dedicated volunteers across Ireland and we are always looking for more! 
Find out about how you can become part of our projects and help make Ireland a better place in which to grow old.
', '2018-12-07 19:24:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2209, 'Contact
Contact the Seniorline Team
Seniorline is a confidential listening service for older people by trained older volunteers, Freephone 1800 80 45 91.
Our vision and mission is that every older person in Ireland would know the Freephone number, call if they need us and receive an empathic response.
Our lines are open every day from 10am to 10pm, 365 days a year.
If you would like to find out more about volunteering with Seniorline, contact us by post at 83 Amiens Street, Dublin 1 or by phone on (046) 955 8237. Alternatively, contact us online .
Third Age, Summerhill, Co. Meath, Ireland
', '2018-12-07 19:24:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2214, '×
Buy some games
You added Feelif Gamer to your cart. Feelif gamer comes with preloaded demo games, of which you can choose two to fully activate. In addition to those two you can also buy more games.
', '2018-12-07 19:24:10');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2223, 'WHY SO MANY ELEPHANTS?
—
The title of Solomon originates from the main character of the book “The Elephant’s Journey” by the Portuguese author Jose Saramago. The book tells the story of Solomon, an elephant who does not speak with words but only with actions. The king of Portugal gave Solomon to the Archduke of Austria as a wedding gift, and Solomon travelled from Lisbon to Vienna on foot. However, before that, Solomon had to walk from India to Portugal as well, when the king first purchased him. During his journeys, Solomon ‘transforms’ into various personalities through the eyes of the people he meets according to their culture, although in reality, his character remains the same. This, with the long distances he travels, makes him a symbolic representative of those who are forced to travel across countries but at the end of the day they are just human beings.
Know Us Better
', '2018-12-07 19:24:10');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2232, 'About us gestor 2017-08-01T11:12:45+00:00
About us
Mouse4all is a startup created tby José Ángel Jiménez Vadillo and Javier Montaner Gutiérrez to change the world. At least a tiny little bit of it. We are engneers and we like to apply technology to solve relevant problems.  We enjoy creating hardware (the Mouse4all box) and software (the Android app), but watching our happy users playing with our inventions is what we like most: their joyful faces when they choose the video they want to watch, or when they receive and read in their privacy a WhatsApp message, or when they shoot a selfie all by themselves!. They are our greatest motivation to move forward.
We are also social entrepreneurs, since we cannot only live on motivation. Till now we have funded Mouse4all with our work and savings, but our objective is to make it a sustainable business in the long term. After two years developing, testing and optimising the product working together with users ans therapists, we officially launched Mouse4all in the summer of 2017.
Our challenge is that the next social network that will appear after Facebook, Instagram or WhatsApp is accessible for all from the first minute. Are you also motivated by this vision? Tell the world about us and help us to make it real.
Acknowledgements
Winners “Vodafone Innovation Prize 2016” in the category Physical accessibility
Third Prize “Printing Real Lives 2017”, Saxoprint España.
We are currently selected among the 30 semifinalists of  “The European Social Innovation Competition 2017” oganised by the European Comission.
Partners
Mouse4all would not be possible without the users, families and therapists that have helped us from day one, testing, improving and validating the iterative versions of the product. They are the real heroes of this story. Thank you very much Rafa, César, Monchi, Ana, Julio, Ángel, Lourdes, Teresa, Eva, Mila, Clara, Ángela… and the tenths of persons that have helped us along the way.
Many thanks also to the centres and associatations that partner with us.
Partners
', '2018-12-07 19:24:12');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2256, 'Need support or information around depression and anxiety,
for yourself, a friend or family member?
Please visit Get immediate support  to contact the beyondblue Support Service and for details of other national crisis and help lines .
The beyondblue Support Service  provides advice and support via telephone 24/7 (just call 1300 22 4636), daily web chat  (between 3pm–12am) and  email  (with a response provided within 24 hours).
How to get in touch with beyondblue
The head office details listed below are for beyondblue corporate and office enquiries only.  
Please note that we are unable to respond to inquiries for assistance with school or university assignments, as we don’t have the staff/resources to meet the demand. Please browse The facts , Who does it affect?  and Healthy places  sections of this website for information on anxiety and depression, and the  About Us  section for information on beyondblue as an organisation.
If you''d like to make a complaint, please visit our complaints page .
beyondblue head office details 
', '2018-12-07 19:24:13');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2256, 'Who we are and what we do
Who we are and what we do
Our vision, values and mission
Our vision
All people in Australia achieve their best possible mental health.
Our values
Collaboration. Respect. Enthusiasm. Excellence. Innovation. Integrity.   
Our mission
We promote good mental health. We create change to protect everyone’s mental health and improve the lives of individuals, families and communities affected by anxiety ,  depression  and suicide .
Our key result areas
Community heart. Business head. beyondblue will adopt a community heart and a business head to achieve our goals:
Reduce the impact of anxiety, depression and suicide by supporting people to protect their mental health and to recover when they are unwell.
Reduce people’s experiences of stigma and discrimination.
Improve people’s opportunities to get effective support and services at the right time.
Use best business practices to deliver integrated, evidence-based and cost-effective initiatives through our people and resources.
How we do it
Mental health conditions don’t discriminate, and neither do we. Our programs and initiatives support people at all stages of life, wherever they live, work, study and play. 
We listen and respond. We will place a high priority on seeking out, listening and responding to the experiences of people affected by anxiety, depression and suicide, and combine this with evidence generated by researchers and evaluators.
We connect with people and advocate for positive change.
We work in all States and Territories, aiming for our campaigns, communications, resources and programs to be as accessible in remote communities as inner metropolitan suburbs, to:
Inform and connect people to enable them to achieve their best possible mental health and access support when they need it.
Influence and challenge discriminatory behaviour by advocating for positive change and prompting discussions across Australia.
Innovate and initiate effective ways to improve access to support and improve outcomes for people, families and communities.
Three million Australians are currently experiencing anxiety or depression. 1  Every day, nearly eight people take their own lives. 2
Because this affects all of us, we’re equipping everyone in Australia with the knowledge and skills to protect their own mental health. We’re giving people the confidence to support those around them, and making  anxiety ,  depression  and  suicide  part of everyday conversations.
And as well as tackling stigma, prejudice and discrimination, we’re breaking down the barriers that prevent people from speaking up and reaching out.
We''re here for everyone in Australia – at  work , home,  school ,  university ,  online , and in communities across the country.
', '2018-12-07 19:24:13');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2256, 'Knowing what to say to a person bereaved by suicide can be a big challenge.
A few guidelines are listed below:
Try not to say ''committed'' suicide. This language harks back to historic times when suicide was a crime and a mortal sin and some bereaved people find it distressing and stigmatising. You can say ‘died by suicide’, ‘suicided’, or ‘took their life’.
Do not use clichés and platitudes to try and comfort by saying things like “you''re so strong”, “time will heal”, “he''s at peace now”, “you have other children”, “you''ll get married again” or “I know how you feel”. While well-intentioned, they rarely comfort and can leave the bereaved person feeling misunderstood and more isolated.
Don''t avoid the subject of suicide. This can create a barrier making it hard for them to discuss personal issues later.
Avoid judgments about the person who suicided such as saying they were selfish, cowardly or weak, or even brave or strong. People need to come to their own understanding of the person and what has happened.
Avoid simplistic explanations for the suicide. Suicide is very complex and there are usually many contributing factors.
Listen and hear their experience.
Be truthful, honest and aware of your limitations: acknowledge if you don''t understand or know how to react to what they are going through.
Say the name of the person who has died and talk about them. Not saying their name can leave the bereaved feeling as though the one who died is being forgotten or dismissed.
Be aware of those who are grieving who may be overlooked, for example, siblings, children, grandparents, and friends.
Ask "How are you getting along?" and then really listen to the response. Stay and listen and try to understand. Allow the person to speak whatever they need to say however difficult and complex it is.
Other pages in This Section
', '2018-12-07 19:24:13');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2256, 'What can I do?
What can I do?
There are varying types of support that can be provided during this time, both practical and emotional.
Practical support
Remember that this type of bereavement is long-term and you will not be able to ''fix'' it or make it go away. People need assistance and support, usually for a long period of time, as they come to terms with what has happened.
Try to help attend to the things that might get left behind during this difficult period. For example, help look after children or cook meals occasionally.
Offer to do something specific, for example, “I could come and mow the lawn”, “I could look after the pets for a while” or just bring some food or a meal. Many bereaved people will find it difficult to ask for assistance and they may also have difficulty making decisions or identifying ways you can assist. Be proactive and simply offer assistance.
Emotional support
Be aware that the person is having a really hard time. Respect their right to grieve and accept the intensity of the grief. Allow them to grieve in the way that is most natural and comfortable for them and provide support that is helpful. There is no one right way to grieve.
Some other ways you can provide support:
Contact the person when you hear of the death. Tell them you are sorry to hear of their loss, or send a card or flowers. If you don’t know what to say, you can just write ‘thinking of you’.
Maintain contact personally or by telephone, text, notes, cards. Visits need not be long.
Listen: This is possibly the most important thing you can do.
Invite them to talk about the person who has died, mention the person''s name, ask to see photos, share stories.
Accept their behaviour – crying, being quiet, laughing. Allow expressions of anger, guilt and blame.
Offer specific practical help, such as bringing in a cooked meal, taking care of the children, cutting the grass, shopping.
Really try to understand and accept the person. Everyone is different and a range of responses are normal.
Be patient. People may need to tell their story over and over again without interruption or judgment. This helps them to come to terms with, and make sense of, what has happened.
Include children and young people in the grieving process and be aware that they may have particular need for support.
Be aware of and acknowledge special times that might be significant, and particularly difficult, for the bereaved person such as Christmas, anniversaries, birthdays, Father''s Day, Mother''s Day.
Realise your feelings of awkwardness and helplessness are normal. Just listening and ''being with'' the person who is grieving can be a powerful support.
Look after yourself. Set limits as you need. To support a grieving person you need to maintain your own wellbeing.
Other pages in This Section
', '2018-12-07 19:24:13');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2287, 'About Us
A COMMUNITY OF SENIORS PROVIDING SUPPORT FOR SENIORS
SeniorsAloud began on the internet in May 2008 as a blog with postings of articles and videos that would appeal to older adults aged 50 and above. Today it has evolved into a go-to site for seniors seeking information, inspiration or connection with other seniors. 
Transitioning from one stage of life to the next can be quite challenging, more so for seniors entering semi- or full-time retirement. SeniorsAloud blog together with SeniorsAloud Facebook page aims to provide a platform where the 50plus can share their life experiences and be a source of inspiration for others on their journey through the retirement years. 
Senior citizens the world over are bonded by a common goal – to live a long and happy life, in good health and with financial security. We believe that seniors can help seniors reach this goal, for who best understand seniors than fellow seniors? 
We welcome you to join our SeniorsAloud community. As a member, you can participate in our offline activities and events which are held regularly to promote successful ageing through lifelong learning, social networking and community service. 
With adequate preparation and the right attitude, the retirement years can truly be the best years of our lives.
', '2018-12-07 19:24:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2288, 'This e-mail address is being protected from spambots. You need JavaScript enabled to view it
- Learning Center Success Items for inclusion on the SeniorNet Web site
This e-mail address is being protected from spambots. You need JavaScript enabled to view it
- SeniorNet membership questions & requests, questions on policies and procedures
This e-mail address is being protected from spambots. You need JavaScript enabled to view it
- Sponsorship, grants, and advertising 
This e-mail address is being protected from spambots. You need JavaScript enabled to view it
- Press and media inquiries 
This e-mail address is being protected from spambots. You need JavaScript enabled to view it
- Accounting questions
This e-mail address is being protected from spambots. You need JavaScript enabled to view it
- Web Related Issues 
', '2018-12-07 19:24:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2305, 'Healthy People Listserv
You can subscribe to receive e-mail notices of Healthy People-related     publications, grants, events, Web sites, and other information.  (If     your e-mail does not launch automatically from the link, you can     send an e-mail message to  listserv@list.nih.gov with the message text: "subscribe HEALTHYPEOPLE".)
', '2018-12-07 19:24:17');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2305, 'Office of Disease Prevention and Health Promotion
Office of Disease Prevention and Health Promotion
1101 Wootton Parkway, LL-100
 
Healthy People Team
Within the Office of Disease Prevention and Health Promotion, a team of professionals is available to assist you with questions regarding Healthy People.
 
Contact the team by email at  healthypeople@hhs.gov .
Find us on:
Enter your email for updates:
 
', '2018-12-07 19:24:17');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2311, 'If you are a journalist, please contact us on:
Tel: 07557 109755 or 020 7224 2020
or [email protected]
Complaints
We are committed to improving the lives of older people who are affected by loneliness and isolation and we work hard to give everyone the best possible service we can.  If you are unhappy about any aspect of our work, we would like to hear about it.  Please click here to read our full our complaints policy.
Whistleblowing
For details of our whistleblowing policy, please click here .
If you would like to sign up to our seasonal newsletter please fill in your details below:
Email address*
', '2018-12-07 19:24:18');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2311, 'Donate now
What We Do
The Silver Line operates the only confidential, free helpline for older people across the UK that’s open 24 hours a day, seven days a week.
The helpline number is: 0800 4 70 80 90.
We also offer telephone and letter friendship schemes where we match volunteers with older people based on their interests; facilitated group calls; and help to connect people with local services in their area.
The Silver Line Helpline
The Silver Line is the only confidential, free helpline for older people across the UK open every day and night of the year. Our specially-trained helpline team:
Offer information, friendship and advice.
Link callers to local groups and services.
Offer regular friendship calls.
Protect and support older people who are suffering abuse and neglect.
68% of calls to the helpline are made at night and weekends.
Who can use The Silver Line
The Silver Line is a helpline and friendship service for people aged 55 and over.
All helpline calls are free
The Silver Line Helpline is free to callers.
All costs of calls are covered by The Silver Line. We rely entirely on donations from organisations and people who care about the welfare and safety of older people. Find out more about how to support The Silver line here .
All helpline calls are confidential
The Silver Line is a confidential helpline.
Callers are free to express their feelings, and describe their lives honestly, and can trust us to respect their privacy.
In cases of abuse or neglect, with the callers’ permission, we will involve specialist safeguarding organisations such as Action on Elder Abuse or the appropriate social services team.
Silver Line Scotland
From February 1, 2017 all calls from across the UK will be answered by our helpline team in Blackpool. We will continue to refer any requests for advice and information from people living in Scotland to Age Scotland between 9-5pm Monday – Friday.
Scottish helpline callers, people with Silver Line Friends, and our volunteers, will be supported by our head office and helpline team and our Nations Manager who is based in Scotland.
To find out more about our Blackpool based helpline hub, please click here
Silver Line Telephone Friends
If callers would like to be put in touch with a Silver Line Friend, they can enjoy a regular weekly friendship telephone call. Silver Line Friends are volunteers who have contacted The Silver Line because they enjoy talking to older people.
Calls are free for both parties, as costs are covered by The Silver Line through donations .
We match the older person to like-minded volunteers, based on their interests and preferences. It is not a counselling service and Silver Line Friends will not meet, or know the telephone numbers of the people they speak to.
Silver Letters
Telephone friendship schemes are not suitable for everyone. We created the Silver Letters scheme for people who might be hearing impaired, and would like a Silver Line Friend, as well as those who just prefer the written word.
Silver Letters are a regular exchange of correspondence between an older person and a Silver Line Friend. All postage is free for both parties, and sent via our mail centre, so costs are covered by The Silver Line. It is not a counselling service and Silver Line Friends will not meet or know the address of the people they write to.
You can find out more about Silver Letters here. 
Silver Circles
Silver Circles are facilitated group telephone calls where people with shared interests can discuss topics that interest them with a wider group. This is enjoyed by people who would like to speak with more than one person at a time.
Calls are free for people taking part, as costs are covered by The Silver Line.
Silver Connects
The Silver Connects team provides older people with more intensive support, advice, and help with connecting to local services. Since launching in November 2015, the team has worked on issues ranging from housing and health issues, to finding lunch clubs and even financial concerns.
Find out about more about volunteering opportunities with The Silver Line here .
Learn More
', '2018-12-07 19:24:18');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2313, 'About Us
WHO WE ARE
We are a not for profit organisation who provide life enhancing exercise programmes to older adults and patient groups. Siel Bleu Ireland is the first specific effort to promote exercise among older adults and patient groups in Ireland, with the aim of improving overall wellbeing. We design our programmes with a preventive approach that benefits all adults, at any age, and at any stage of their lives. We promote the autonomy and wellbeing of older people and people with chronic disease, ensuring that everyone has the capacity and self-confidence necessary to live an independent and happy life.
 
Siel Bleu offers group exercise programmes  in nursing homes/daycare centres, with patient groups, in communities and one to one sessions in private homes. Our expert Physical Trainers gauge the ability of each participant and tailor classes to meet the group needs. Together this results in programmes which have a real physical, social and psychological impact.
THE SIEL BLEU ASSOCIATION
Siel Bleu was established in France in 1997 by two Sports Science students who wanted to utilise adapted exercise programmes to fulfil the unmet need for preventative exercises for older adults. With over 20 years of experience delivering these programmes to older adults, The Siel Bleu Association now comprises of 450 Physical Trainers delivering 180,000 of physical activity France , Spain , Belgium and Ireland.
 
SIEL BLEU IRELAND GOVERNANCE
Vision
Our vision is to create a society where older adults can experience the benefits of physical activity, irrespective of social, cultural or economic background.
Mission
Siel Bleu Ireland aim to provide affordable and accessible exercise programmes for all members of Irelands older population irrespective of their level of ability or physical health
Governance
Siel Bleu Ireland is on the journey towards full adherence with the governance code; A Code of Practice for Good Governance of Community, Voluntary and Charitable Organisations in Ireland. To see the register of compliant organisations and those on their journey to compliance,  click here .
When we Started
Siel Bleu Ireland was founded in Ireland September 2010 as part of an Ashoka Changemakers initiative, starting off with three classes per week. Siel Bleu Ireland now delivers services to over 4000 people per week
How we are Funded
Siel Bleu Ireland charge a fee for the delivery of our services. In 2017, 89% of our overall revenue came from revenue for the delivery of services. We are funded by the corporate partners and private foundations. For more information, please visit our partners page.
Legal
Siel Bleu Ireland is a Company Limited by Guarantee (Company No: 488914c) with charitable status (Charity Number CHY19489). The registered office is 18 Eustace St, Temple Bar, Dublin 2.
Our Board
We are governed by a Board of Directors comprising of people with the appropriate mix of skills, knowledge and experience to support Siel Bleu Ireland. The Board is responsible for the overall governance and strategic direction of Siel Bleu Ireland with progress and goals reviewed at each Board Meeting.
Roger Jupp – Chairman of The Board
Caroline Daly – Board Member
', '2018-12-07 19:24:18');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2327, 'Email: enquiries@linkagenetwork.org.uk
Stay in touch
We produce a monthly eNewsletter to keep you up to date on any exciting news and opportunities for older people across Bristol. If you want to subscribe to receive our eNewsletter click here .
We can also keep you up to date on activities, events and offers that sometimes become available to LinkAge Network. If you want to subscribe to receive information on activities, events and offers click here .
You can unsubscribe or update your choices at any time by emailing unsubscribe@linkagenetwork.org.uk .
Name*
', '2018-12-07 19:24:19');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2327, 'inform over 55s about what’s on across the West of England
host like-minded projects
All of the LinkAge Network’s work is under-pinned by the following values and behaviours. The organisation:
promotes positive ageing and challenges negative stereotyping
collaborates with local communities and organisations to enhance specialist knowledge and skills
connects groups and organisations to share their knowledge and provision
takes its lead from older people and their local communities
facilitates access to civic participation by older people
 
', '2018-12-07 19:24:19');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2357, '1
 ITNCountry is a transportation solution built for small towns and rural places. It is designed to bring the ITN model anywhere, to put modern tools previously available only to metro areas into the hands of any town, city or region. With ITNCountry anyone can build a community-based transportation solution that connects vehicles, drivers and riders with businesses, healthcare providers and families. Because every community needs dignified solutions for senior transportation.
LEARN MORE
Trusted Transportation Partners
With 13 affiliates in 12 states, ITNAmerica supports sustainable, community-based transportation for seniors and people with visual impairments. But we aren’t yet everywhere, so to ensure older people in communities across America have access to quality transportation options we developed our Trusted Transportation Partners program. Trusted Transportation Partners, or TTPs, offer the highest level of transportation service, the sort of service we expect from our own affiliates, in non-ITN communities. It’s a way to take the guesswork out of deciding on your senior transportation selection—a TTP is an independent organization that has earned the ITN seal of excellence. We support them, because they support you.
AWARD WINNING PROGRAMS
Personal Transportation Accounts™
Personal Transportation Accounts™ are the mobility portfolios at the heart of the ITN model. Transportation credits can be earned by driving or issued in exchange for your vehicle through the CarTrade program. They can be shared among friends, traded among communities or saved for future needs. The Personal Transportation Account is the one stop solution to lifelong mobility. It is an ITN original, as are the award winning innovative programs that channel mobility resources into your future.
Ride & Shop
Are you a business that cares about its older customers? Want to ensure they can keep browsing your aisles even after they limit or stop driving? Our Ride & Shop program can help. Ride & Shop partners ITNs with local businesses to help keep the costs low for seniors needing to reach your location. Losing customers can have a real impact on your bottom line, but Ride & Shop ensures no one is stuck at home. If older shoppers can reach you, everyone wins.
Healthy Miles
Are you a medical professional, healthcare provider or hospital looking to ensure your older patients can keep their appointments? ITNAmerica’s Healthy Miles program partners ITN affiliates with local healthcare providers to help support older patients who need rides. Healthy Miles ensures riders arrive on time and cancel fewer appointments. Healthcare professionals can rest easy knowing their patients will arrive safely and on time. If they show up, everyone wins.
Road Scholarship
The ITN Road Scholarship program helps low income seniors access rides regardless of ability to pay. Many ITN Volunteer drivers do not need to use all of the transportation credits they earn as they drive others. By contributing their excess credits to the Road Scholarship program, they actually give twice: once for the ride they give, and again with the scholarship they fund.
A Community Road Scholarship is the same kind of program, only it is set up by a town, a church, or a service organization. When their members volunteer as ITN drivers, they can contribute their transportation credits into an account for their own residents or members to distribute credits to those in need. It is a way to ensure everyone can afford to live a full life even after they leave the driver’s seat.
CarTrade
Is your car lonely, just sitting there perfectly serviceable but silent? Is it taking up driveway space or filling your garage? It used to carry you around town, but not these days? Our CarTrade program can turn your car into rides whenever and wherever you want to go. CarTrade lets your car keep you going places even after you’ve stopped taking the wheel.
Because neither you nor your car should be stuck at home.
Want to find out more about our programs? Contact us.
LEARN MORE
RESEARCH
ITNAmerica is research-driven, and research begins with good information. To this end, ITNAmerica has built and maintains three of the best senior transportation databases in the country: ITNRides, Rides in Sight, and the 50 State Policy Project. We conduct our own original research, and we have worked with universities, the Centers for Disease Control, the National Highway Traffic Safety Administration, the Federal Transit Administration, the National Conference of State Legislatures, the Transportation Research Board (National Academies of Science) and numerous private philanthropies. Our theoretical knowledge is further informed with decades of on-the-ground experience working in the field serving older people. Our expertise has grown alongside our network, starting with service to one small community and expanding to cover every corner of the U.S. If you are looking for a place to access information on senior transportation nationwide, ITNAmerica is where to begin.
POLICY
ITNAmerica is a national resource. Our expertise is in mobility, senior transportation and sustainable community mobility. The ITNAmerica Knowledge Center catalogs the breadth of our work, making it available to students, researchers, policymakers and family members everywhere. Because supporting older people and community mobility is what we do.
The 50 State Policy Project is a hub for policy makers and members of the public looking for a policy database for transportation in all 50 states. The database focuses on policies that create incentives or remove barriers to the use of private resources for sustainable transportation. This information was compiled with the help of the National Governors Association Center for Best Practices , the American Bar Association Commission for Aging and Law , the National Conference of State Legislatures , the American Automobile Association and AARP .
For the latest media mentions and coverage, visit our  Press Room .
LEARN MORE
EDUCATION
ITNAmerica not only studies mobility and develops models for senior transportation, we also to serve as an educational resource. Mobility has become a major issue of our time, and ITNAmerica is dedicated to informing the public, policymakers, local communities and organizations about the specific needs of older people facing age-related mobility issues. From our 50 State Policy Project to the ITNCountry Online Learning System to the Rides in Sight database, ITNAmerica offers a wealth of resources on the subject of age, aging and lifelong mobility. We offer resources to help individuals, families, social service organizations, businesses, lawmakers, health care workers providers and others. Our dedication to education is second only to our dedication to seniors.
And as a recognized expert in the field, ITNAmerica president and founder Katherine Freund has for two decades used her platform as well-regarded speaker to further this educational mission. Katherine is a 2012 Askoka Fellow. She was featured in the Wall Street Journal as one of the “12 People Who Are Changing Your Retirement,” and on CNN’s “Breakthrough Women” series. She has received the AARP Inspire Award, the Maxwell Pollack Award from the Gerontological Society of America, and the Social Enterprise Alliance Award for Leadership in Innovative Enterprise Ideas. Katherine is also the recipient of the Access Award from the American Foundation for the Blind, the Archstone Award for Excellence in Program Innovation from the American Public Health Association, and the Giraffe Award for sticking her neck out for the common good.
Katherine has participated in more than 150 national and international panels and conference sessions on alternative transportation for older people. She has spoken in Canada, Australia, Germany, Ireland, England, Switzerland, South Korea and Taiwan. Her efforts are a key part of ITN’s educational efforts. Contact us at (207) 857-9001 if your organization would like to learn more about inviting Katherine to speak.
ASHOKA FELLOWSHIP
Making More Health Fellows from every continent at the Ashoka/Boehringer Ingelheim Conference in Ingelheim, Germany, October 2017.
Katherine Freund, President and Founder of ITNAmerica, has been chosen as an Ashoka Fellow . She joins a worldwide network of 3,300 social entrepreneurs from 88 countries who are changing the way we solve problems. She is also one of 85 Making More Health Ashoka Fellows, sponsored by Boehringer Ingelheim, a 125–year-old family owned pharmaceutical company seeking to bring the innovation of social entrepreneurs to help solve global healthcare problems. Katherine’s work to build ITN and ITNAmerica, consumer-oriented, economically sustainable transportation for older people, is part of this effort.
Rides In Sight
Rides in Sight is the nation’s largest comprehensive database of transportation services for older or visually impaired people. It’s curated, searchable and available to anyone. It’s an online site and a toll-free number, with trained customer representatives to help you find the best available transportation to suit your needs. It’s meant to make finding ride solutions easy. Need a ride? We can help.
ITNAmerica is dedicated to senior mobility. That’s why we developed the Rides in Sight website and hotline. It doesn’t matter if you’re in New York City, rural Idaho or somewhere in West Texas, if there is an organization offering rides to seniors in your area Rides in Sight is the best and the simplest way to find it. It’s easy. Go to the website or call the hotline, enter your county or tell your customer representative where you live and get a slate of ride options in return. There is no cost, no fee, and no small print. Rides in Sight is a nationwide solution for older people everywhere. It is continually updated and carefully combed to ensure you won’t be calling defunct agencies for a ride.
Rides in Sight is one more way ITNAmerica ensures seniors everywhere have access to safe, reliable transportation.
', '2018-12-07 19:24:21');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2359, 'Berkshire
RG40 1BJ
Or use the form below to send us a message. Please allow up to 2 business days for a response.
When to contact you?
', '2018-12-07 19:24:21');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2367, 'About Us
Mission
The mission of Connecting For Good is to provide computer skills, computers, and internet access to low and moderate income communities.
Vision
A connected future for all—that through Digital Literacy Training, low-cost computers and Internet access, low income residents will achieve measurable and sustainable gains in Education, Employment, Economic Impact, and the Environment.
Our Core Values
Internet connectivity equals opportunity. It is an 
absolute necessity in order to fully participate as a productive citizen in a digital society.
Education is the number one thing that lifts people from poverty. In a digital society it is impossible to pursue a quality education without access to the Internet.
In-home Internet is as essential as any modern utility.
Digital Inclusion—and Connecting For Good as a National Leader
The National Digital Inclusion Alliance (NDIA) defines Digital Inclusion as having five elements: Internet Access, Affordable Computers, Computer Skills Training, Educational Online Content and Technical Support.
Connecting For Good is one of only a small number of organizations in the United States offering programs across each of these five elements.  Since we started in 2012, we have:
Built and installed over 60 internet networks for
low-income KC community partners;
Refurbished and sold, for as little as $50 each, over 2,000 donated computers;
Provided training—from basic to advanced digital skills—to over 15,000 low-income KC residents
Created customized curriculum as well as access to hundreds of available courses for topics ranging from youth STEM to technical certifications;
Supported over 100 non-profit partners with technical support and computer repairs.
As a founding member of the Kansas City Coalition for Digital Inclusion , Connecting For Good is helping Kansas City forge a national leadership model to effectively bridging the Digital Divide.
Program Impact
Connecting For Good has been bridging the Digital Divide since 2012 by providing Connectivity, Hardware, and Training, and delivering impact through a focus on Education, Employment, Economic Impact, and the Environment. We operate two Community Computer Training and Donation Centers, at 2006 North 3rd Street in Kansas City, Kansas, and 3210 Michigan Avenue in Kansas City, Missouri. These areas are two of Kansas City’s most under resourced neighborhoods. Our centers serve as a base of operations on both sides of the state line.
Sustainable Outcomes
Education: In 2016, we trained 7,000 individuals from the urban core by teaching over 53 different digital literacy topics in over 15 different Kansas City locations.
Employment: We employ individuals we teach—six of our current staff started out as volunteers or students. We provide technical skills training through internships and partnerships with local work investment organizations like the Full Employment Council, and, in 2017, the KC Social Innovation Center, who organized the HireKC Youth program. In total we have hosted over 30 interns since 2015, one of whom, has become a paid staff member.
Economic Impact: Our programs are built with sustainability in mind. We refurbish donated computers for resale to income-qualified consumers, and we offer tech support as well as Internet service support to community centers and non-profits serving the urban core. In 2016 we sold nearly 300 computers and connected over 1,400 internet subscribers in low-income housing. With growing demand from residents and non-profits for more computers and Internet service, the earned income from computer sales and tech support helps to defray the costs of our services to the community.
Environment: Our communities benefit from the planet-friendly impact on our environment through our donation centers, computer refurbishing, and responsible recycling of any unused computer equipment. We have prevented several tons of landfill from claiming unused computers through partnership with The Surplus Exchange and EPC, both certified recyclers of tech equipment. We are always looking to increase the number of donated computers we service and the amount of recyclable material we generate.
Our Approach
The bulk of CFG’s impact has come from building relationships through individualized training in both of our community computer labs, as well as remote training provided to low-income residents at churches, community centers, and public housing facilities. 
All but two of the staff began as clients or volunteers. According to the National Telecommunications and Information Administration (NTIA), “To successfully increase broadband adoption, all barriers must be addressed through a diverse set of local partners with established roots in the community. Each local solution should be tailored to the individual community. Equally important, trust – between the individual and organization providing instruction – is essential.”  Every staff member has a high degree of empathy with clients.
As the only organization in the region whose sole mission is digital inclusion, CFG seeks to optimize collective impact across any organization offering help for low-income residents who seek to improve their lives through digital fluency.
Going Beyond Technology Access
CFG targets 7,500 participants in the current year, an average of 26 people per day across all sites served. Over 80% have annual incomes below federal poverty guidelines. Demographics include 85% African American, 10% Caucasian, and 5% Hispanic/Latino in KCMO, and in KCK, the demographic is 85% African American, 5% Latino, and 10% Somali. Sixty-five percent are working age adults over the age of 21; 25% are retirees and older adults and 20% school-age children. Many clients are considered high-need based on risk factors such as a single-mother leading a household, poor academic attainment, lost time at work, negative behaviors (smoking, alcoholism, drug use), and health issues (depression, obesity, heart disease), and language barriers among parents.  The most common difficulty CFG faces is the constant interruption of these risk factors in the lives of its clients.  This is why CFG’s staffing criteria include empathy, patience, and positive persistence. CFG’s mission is founded on researched principles that is just now starting to become part of a national and global dialogue. 
The increasing number of CFG programs over the past four years showcases the comprehensive, collaborative, and entrepreneurial sustainability of Connecting For Good as a model for digital inclusion leadership among community-based organizations.
From partnering with Front Porch Alliance and Operation Breakthrough to teach digital literacy to moms with young kids;
to youth STEM camps at Juniper Gardens;
supplying laptops and support to the Black Family Technology Awareness Association STEM Olympics fair;
to equipping GreenWorks with computers and curriculum to supplement their interns’ professional development;
to teaching a wide variety of digital courses to residents of the Housing Authority of KC;
and participating monthly as a Steering Council member of the KC Coalition for Digital Inclusion.
', '2018-12-07 19:24:21');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2367, 'Click to Subscribe
About Connecting For Good
The mission of Connecting For Good is to provide computer skills training, computers, and internet access to low income communities.
', '2018-12-07 19:24:21');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2367, 'The Digital Divide is very real in Kansas City.  We believe it is one of the most important social justice issues of our day.
25% of Kansas City area residents don’t have broadband Internet access at home.
42% of those who don’t use the Internet have annual household incomes of under $25,000, most of whom live in low income housing.
46% of nonusers are minorities. *
Additionally, 70% of Kansas City Public Schools students do not have the Internet in their homes.  40% of students will move at least once during the school year.
Only 20% urban core households in KC’s predominantly minority, low income neighborhoods,  have a PC and Internet connection. (2013 US Census)
Used by permission of the 
Open Technology Institute 2014
About Connecting for Good
Connecting for Good been bridging the Digital Divide since 2011 with wireless mesh networks, community technology centers, low cost refurbished PCs and free digital life skills classes. It is the only Kansas City area nonprofit, that has digital inclusion as its core mission.  That’s because our core belief is that connectivity equals opportunity. Access to the Internet brings with it a chance to apply for jobs online, connections with family and friends, access to virtual library shelves, information about medical and health issues, online education — GED completion and college courses — and a whole lot more.
Connecting for Good is governed by a community-based board of directors. Our mission is to enable organizations and individuals to use technology to connect with one another in order to have a positive impact on society and the environment. Learn more about the  history of Connecting for Good .
Our core values are:
Internet connectivity equals opportunity. It is an absolute necessity in order to fully participate as a productive citizen in a digital society.
Education is the number one thing that lifts people from poverty. In a digital society it is impossible to pursue a quality education without access to the Internet.
In-home Internet access is as essential as any modern utility; like phone service, electricity and running water.
Program Description
Our three-pronged strategy of Connectivity, Hardware and Digital Literacy is being accomplished through these ongoing activities:
A. Community Technology Centers: We currently operate two community technology centers, at 2006 N 3rd Street in Kansas City KS and 3201 Michigan in Kansas City MO. Located in two of Kansas City’s most under resourced neighborhoods, we offer a variety of programs to promote technology use and access, including regular free classes in computer and Internet use.
B. Digital Life Skills Classes : We have developed a curriculum designed to help people in under-resourced urban communities to access online resources and create a brighter future for themselves and their families. In order to take hands on experiences with broadband Internet to these neighborhoods we have a portable computer lab that can be setup in key locations in our targeted communities in order to introduce residents to the online world. We hold Family Computer Days at various community institutions every month.
C.  Inexpensive Internet-ready Refurbished PCs : Thousands in our area have no computer at home and cannot take advantage of all the benefits of Internet connectivity. We collect used desktops, laptops, tablets and smartphones for this purpose. This equipment is provided to low income residents at an extremely low price, as low as $75.00 for those who qualify. We also assist those who purchase computers to find the most affordable options for Internet service.
D. Internet Access for Low Income Families: We operate free Wi-Fi networks at three low income housing projects; Rosedale Ridge and Juniper Gardens in Kansas City KS and Posada del Sol, a senior high rise in Kansas City MO. The mesh network at Juniper Gardens covers over four city blocks. In all we are providing connectivity, free of charge to 500 low income households. We are have plans for similar projects at additional properties on both sides of the state line.
E.  Technology Assistance for for Nonprofit Organizations : Connecting for Good works to support other local organizations to close the Digital Divide.  Through our Train-the-Trainer activities, we teach staff members and volunteers from other agencies how to teach the digital life we’ve developed. To spread connectivity throughout our community, we install wireless mesh networks, community access computer centers, and public access Wi-Fi hotspots.
Demographics
While we serve people from throughout Greater Kansas City, our efforts have been focused on two of the areas neediest areas, NE Wyandotte County and neighborhoods East of Troost Avenue in Kansas City, Missouri. Up to 80% of residents in these parts of the city do not own computers or have in-home Internet access.
Since March 2013, over 2,000 people from these communities participated in our free classes and took home our high quality, Internet-ready refurbished computers. Our records reveal that:
25% have never used a computer
75% have incomes of under $20,000 a year
75% are over 50 years old
80% are minorities, predominantly African American
2/3 of participants in the free classes are women
1/2 of women 60+ have a child under 18 living with them
90% purchased a computer from us after taking the free classes
The fact that a large majority of them are black women over 50 years old significant. Most have children under 18 years old living with them, many of whom attend the Kansas City Public Schools where 70% of students do not have in-home Internet access.  In a very real way, we are supporting these women, who are looking out for their communities – especially the children – by getting themselves up to speed with the online world. At the same time, he provide them with low cost computers so that the children in their care can have the same advantages of being online as their suburban peers
Funding
Besides seeking grant funding and contributions from the public, Connecting for Good is committed to a social enterprise model of sustainability. Delivering refurbished PCs and providing IT support for other nonprofits covers nearly half of our operating costs.
Videos:
Watch CFG President, Michael Liimatta’s presentation to the Kansas City MO City Council, Oct. 31, 2013.
View our  1 Million Cups Presentation  at the Kauffman Foundation on Jan. 30, 2013.
How you can be involved:
Take a few moments to  learn more about our programs
Sign up for our e-newsletter to receive regular updates via e-mail
Download the PDF Version of our most recent Programs and Activities Presentation
See “CFG in the Media” on the right menu for local and national media coverage of our efforts.
If you want to get involved please contact us .
Connecting for Good is a registered Kansas not-for-profit corporation #4563276 and has 501(c)(3) status with the Internal Revenue Service. Contributions in the form of monetary gifts and equipment donations are tax-deductible.
Written by Tom Esselman
', '2018-12-07 19:24:21');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2368, 'About Us
Let’s Make San Francisco a Great City to Grow Older
At the Community Living Campaign, we’re working toward a San Francisco where we can all live – and thrive – in our homes and neighborhoods as we grow older.
We bring greater joy, health, and connectedness to San Francisco seniors and people with disabilities through community-building, empowerment, and advocacy.
The Community Living Campaign helps community-driven, collaborative solutions come to life while also serving as a strong voice at City Hall. We advance the ideas, talents, and energies of seniors and people with disabilities in all that we do.
Together, we can create an inclusive San Francisco where people of all ages and abilities can truly live “in community” – with all of the rich relationships and opportunities that define a good life.
Community-Building, Empowerment, and Advocacy
The Community Living Campaign brings greater joy, health, and connectedness to seniors and people with disabilities in San Francisco through community-building, empowerment, and advocacy.
Community-Building
Our community-building work cultivates and strengthens the friendships and supports we need to be able to age and thrive in place. It also strengthens our neighborhoods and the entire city!
Empowerment
At the Community Living Campaign, we help build the skills and tools needed to have a good life, whether that’s learning how to prepare healthy food, use technology, find employment, or be a strong advocate for yourself and others.
Advocacy
Our advocacy focuses on winning the policies, services, and supports needed to make San Francisco a diverse, compassionate, and welcoming place for all of us. We help individuals speak out and also bring together diverse coalitions to work toward a truly aging and disability-friendly city.
Our Mission
The Community Living Campaign strengthens and mobilizes individuals and organizations to assure every person’s right to live in community. Our strategy is to leverage the power of relationships to improve services and support.
', '2018-12-07 19:24:22');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2368, '1663 Mission Street, Suite 525
San Francisco, CA 94103
Trying to reach a specific program? Please dial 415-821-1003 and the following extensions:
Office/General Questions: extension 100
Executive Director/Marie Jobling: extension 101
Deputy Director/Kate Kuckro: extension 102
Bayview Food Network/Chester Williams: extension 103
OMI Food Network/Deb Glen: extension 104
Park Merced & University Park Food Network/Karen Holt: extension 105
Cayuga Connectors Neighborhood Network/Patti Spaniak: extension 106
St. Francis Square Neighborhood Network/Betty Traynor: extension 107
Connections for Healthy Aging/Lizette Martinez: extension 108
SF Connected Computer Classes/Judy Auda: extension 109
SF ReServe Support Line: extension 110
SF ReServe Director/Alicia Crawford: extension 114
Vision & Hearing Initiative/LaNay Eastman: extension 111
SF Tech Council/Susan Poor: extension 112
Annual Event and Other Special Events: extension 113
Dignity Fund Coalition: extension 115
Program Locations
', '2018-12-07 19:24:22');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2370, 'Suite 1501, 5328 Calgary Trail NW
Edmonton, Alberta T6H 4J8
 
2018 Saint John Games Inquiries
For information about the upcoming 2018 Games in Saint John NB, or to volunteer please contact:
General Information and Volunteering:  2018saintjohn@canada55plusgames.com
Name:
', '2018-12-07 19:24:22');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2370, 'About Us
WHAT ARE THE CANADA 55+ GAMES?
The Canada 55+ Games is a nation-wide program to sponsor wellness – that is, the spiritual, mental and physical well being – among Canadians 55 years of age and older.  Provincial programs are staged annually in different provinces and territories.  The first national Games were held in Regina in 1998.  The Games should not be categorized as sporting events, as they span a wide range of physical and mental challenges – from slow-pitch to contract bridge and darts to lawn bowling. The Canadian Senior Games Association is the sanctioning body for these Games.
These events bring together amateur competitors who participate for the sheer joy of competition, for the opportunity to visit other parts of Canada and for the camaraderie and social interaction that are integral and essential part of the Games.
​
The Games bring together relatively small groups, representing their respective provinces or territories.  These groups earn the right as a result of competition against thousands of other seniors in their home province or territory.  The 1500 competitors represent over 10,000 Canadian seniors who actively participate in local events leading up to the national games.
VISION
Our Vision is to create a recurring, popular and truly national multi-activity event engaging older Canadians from coast to coast to coast.  The Canada 55+ Games enrich the lives of participants and host communities and are widely recognized for their unique “fun-focused” competition model and for their significant influence on participation in an active and enjoyable lifestyle by all older adults in Canada.
VALUES
Participants Come First: We believe in respecting participants above all else, never forgetting they come to us by choice
Fun and Safe Games: We believe all Games related sports & activities should be fun and safe for all participants
Balance is Key: We believe the Games must retain a balance of physical, mental and social activities as the comradeship is truly one of the Game’s greatest outcomes and reinforcing the sense among participants that “Win or Lose, they are all winners” is paramount
Fair Play:  We believe in fair play characterized by equity, integrity, trust and respect
Proper Planning:  We believe in the concept of proper planning to ensure maximum efficiency and effectiveness of the Association, its programs and resources
Accountability:  We believe that the Association must strive to be accountable and responsive to the needs of the participants, partners and stakeholders and we must deliver on what we say
MISSION
The Canadian Senior Games Association delivers the biannual multi-event Canada 55+ Games as a unique blend of active and passive activities and works continuously to influence personal behaviour and social supports that encourage healthy, active living for older adults in Canada.
Copyright 2017 Canadian Senior Games Association
', '2018-12-07 19:24:22');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2372, 'Firststop Care Advice > About us
FirstStop Advice
FirstStop Advice is an independent, impartial and free service offering advice and information to older people, their families and carers about housing and care options for later life.
The service is provided by Elderly Accommodation Counsel (EAC) in partnership with a number of other national and local organisations, and brings together a wealth of expertise to help older people explore the options and choices open to them. We can provide specialist help on any aspect of care, support or housing for elderly people, including financial issues as well as statutory rights and entitlements.
A growing network of affiliated FirstStop Local services bring additional local knowledge, and in many cases can offer face-to-face advice including home visits. It is therefore our aim to now work closely with national and local partners who want to further develop the FirstStop model.
Elderly Accommodation Counsel
Elderly Accommodation Counsel’s aim is to help older people make informed choices about meeting their housing and care needs. It was founded in 1984, and became registered as a charity in 1985. It is now more commonly known as EAC.
Having delivered a free, impartial and highly regarded information and advice (I&A) service to the public since 1985, from 2002 we made repeated efforts to build the scale and reach of the service by forging partnerships with other organisations. In 2008 these efforts resulted in the launch of FirstStop, a comprehensive I&A service covering care, housing and associated financial matters.
Work with us
We are always looking to work with new national and local partners who share our vision of improving access to independent and expert housing & care options advice for older people and their families. FirstStop already works with a wide range local partners across the country, which has allowed us to help more than 30,000 older people in the last year alone, and we anticipate that in 2015/16 that number will increase. So if you are interesting in either working with us, and joining our network, or sponsoring FirstStop Advice, then see below for more information.
Partners
If you would like to join our network of partners then we would love to hear from you. View our Partners page for more information on who we are working with at the moment and how you and your organisation can work with us.
BECOME A PARTNER
Sponsors
If you are in a position where you would like to discuss sponsoring FirstStop to develop or enhance an Information & Advice service, then find out more on our Sponsors page.
Frequently asked questions
How do I become a FirstStop partner?
We would love to hear from you if you would like to work with us in order to benefit older people and their families. Read our partners page for more information on how to become a FirstStop partner.
What support do FirstStop partners receive?
We’re committed to supporting the organisations that we work with, we are therefore able to offer a wide range of supporting resources which includes training on housing and care options and localised versions of our publications and toolkits. Find out more here: Our resources
How is FirstStop currently funded?
For the last few years we have received the majority of our funding from the Department for Communities and Local Government (DCLG), although we have also received funding for other bodies of work, such as the Live Safely and Well at Home project by Nationwide.
Who are the current FirstStop partners?
We have a number of national and local partners. National partners include; Independent Age, Beacon and the Society of Later Life Advisers (SOLLA). Our local partners include; Age UKs, local authorities and a number of Home Improvement Agencies. Find out more here: Local partners
Quotes about FirstStop
“As we grow older we may not just need to change how we live, we might also need to change the homes we live in. For some older people, this can be an overwhelming prospect, so the free information and advice and practical support provided by FirstStop is invaluable”
Brandon Lewis
“Having someone to talk, someone who knew what they were talking about was wonderful”
FirstStop Caller
“Without the support of the local FirstStop services, people would have struggled on and endured a lower quality of life and wellbeing”
Dr Gemma Burgess
Cambridge Centre for Housing and Planning Research
Share this page:
', '2018-12-07 19:24:22');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2386, 'Arlington, VA 22202
571-527-3900
We are located in Crystal City, above the Crystal City Metro station on the yellow and blue lines.
', '2018-12-07 19:24:23');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2391, 'Call us at 212.582.0343 or e-mail us at info@clubhouse-intl.org
Inquiries about Clubhouse Operations, New Clubhouse Development, Membership and Training programs:
Contact Jack Yatsko: jyatsko@clubhouse-intl.org , 808.651.8598
Inquiries about Clubhouse Accreditation:
Contact Robby Vorspan: rvorspan@clubhouse-intl.org , 516.729.3088
For Information about Contributions and Volunteering:
Contact Anna Sackett Rountree: asackett@clubhouse-intl.org , 212.582.0343
Media Inquiries:
Contact Joel Corcoran: jdcorcoran@clubhouse-intl.org , 212.582.0343
Awards + Recognition
', '2018-12-07 19:24:24');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2404, 'Level 1, 420 Bagot Road
Subiaco WA 6008
ph +61 8 9212 4333
fax: +61 8 9212 4334
', '2018-12-07 19:24:25');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2470, 'The DO School inspires you to get things DOne. To seize new opportunities, to solve important problems.
Our society and business models are changing fast. We equip you to shape what comes next.
At the DO School, you become a DOer.
DOers clarify new ideas, and bring them to life quickly as new products, processes and services.
DOers inspire seismic cultural shifts that transform people, organisations and society.
DOers are purposeful: they create not just better business but a better world – for everyone.
for Entrepreneurs
', '2018-12-07 19:24:28');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2470, 'Welcome › The DO Lab
Your own way to innovation.
The DO Lab creates learning experiences that empower participants to become courageous doers, effective and purposeful leaders that know how to turn ideas into action and drive your organization forward.
The DO Lab continuously pushes the boundaries of how we learn to ensure participants gain and experience the most important skills in today´s quickly changing world. These skills empower individuals and teams to create innovation and become effective entrepreneurial agents.
Based on the DO School Method , our team analyses your needs and builds an impact driven program. The DO Lab´s excellence lies in programs that create measurable impact – we build workshop series, multi-year leadership programs as well as build the strategy for entire corporate academies.
In order to learn more about the programs we are currently building and to find out what we might be able to develop together, feel free to get in touch.
We look forward to hear from you.
Want to know more about what we can DO?
', '2018-12-07 19:24:28');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2482, 'BETTER URBAN MOBILITY
bringing the CPH cycling experience to the world .
We believe that cycling is not only a means but the only possibility we have to create a better urban future.
MULTIDISCIPLINARY EXPERTISE
many competencies,
one goal.
From urban consulting to engineering and on-ground implementation -we strive to make cities a better place .
RADICAL INNOVATION
we''re disruptive .
We‘re not an everyday infrastructure company - we bring start-up mentality to a field that has long been undynamic. We encourage out-of-the-box-thinking in order to create change.
', '2018-12-07 19:24:28');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2528, 'Herne Hill, London, SE5 8ET
The Bike Project Workshop
For refugees collecting bikes, and volunteers.
Address
Arches 223 - 224, Edward Street
London, SE8 5HD
info@thebikeproject.co.uk
or call 020 7733 8098
One thing to bear in mind: we''re a small team and sometimes we have to pop out. If we miss your call, please don''t strike us off your Christmas list – we''ll do our best to get back to you within 24 hours.
For the same reason, if you''d like to pop in to donate a bike – firstly THANK YOU – but do drop us a line so we can make sure we''re around to give you a warm welcome and a cup of tea!
GET TO KNOW US
', '2018-12-07 19:24:29');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2528, 'About us
About us
The Bike Project is a community of refugees, mechanics and volunteers in London. Our mission is simple: we take second-hand bikes, fix them up in our workshop and then give them to refugees.
We also run cycling proficiency training for refugee women in Croydon and Wapping. The course empowers and inspires these awesome people to feel the freedom of their very own bike – perhaps for the first time in their lives.
As well as our charitable activities, we sell a small proportion of our wonderful refurbished bikes to the general public through our online bike shop. Every single penny we make from bike sales is ploughed straight back into the charity. This much-needed cash injection helps secure our long-term sustainability – and that means even more refugees cycling!
We''re a young project and have a warm welcome ready for anyone keen to volunteer, learn practical skills and meet funky people. If that''s you, drop us a line!
Our staff
We run a tight ship here at The Bike Project, with our office staff frequently wearing so many hats that it can become a fire hazard. Thank goodness they''re all super-talented and hard-working!
Jem Stein
Founder
I set up The Bike Project after seeing how much my brother''s old bike helped Adam, a Darfuri refugee I mentored at university. Now I''m on a mission to get every refugee in ...
Read more
Leila Wong
Leila manages our women-only project where refugee women learn to ride and develop cycling skills in a supportive environment. She also manages our exciting new scheme - B...
Nicola Hill
Operations Manager
Nicola was a major force behind organising our Critical Mass bike ride to Calais in 2015. She then began volunteering as an instructor at our cycle training for refugee wom...
Read more
Our trustees
Our trusty trustees are a treasure trove of experience, full of invaluable business and charity know-how that has been a huge factor in our success and our ambitions for the future. Hat tip.
Will Cardy
Will is an online marketing expert from an agency called Platypus Digital. Like many of our volunteers, he approached us offering his skills rather than the other way round...
Hannah Warwick
Treasurer
Hannah is a Chartered Accountant and is Associate Director of Finance at the Kew Foundation, the charity that fundraises for the Royal Botanic Gardens Kew. She trained at t...
Read more
Sarah Payne
Sarah is a solicitor who works for Open Society Foundations, a global network of human rights organisations, ensuring it is compliant with its legal and regulatory obligati...
', '2018-12-07 19:24:29');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2535, 'IMPORTANT: Our office is at a different address to the postal address.
The Learning Unlimited office
Our office address is 9-11 Endsleigh Gardens, London WC1H 0EH,
Endsleigh Gardens is just a short walk from Euston station, facing the Friends’ Meeting House garden.  Please go the main entrance on the corner of Endsleigh Street and press the Learning Unlimited buzzer.
 
The Learning Unlimited postal address
Learning Unlimited, UCL Institute of Education, University of London, 20 Bedford Way, London WC1H 0AL
Follow Us!
With thanks to our funders past and present
Some of our current and recent partners
', '2018-12-07 19:24:30');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2535, '£0.00 0 items
About us
Learning Unlimited is a not for profit social enterprise that specialises in adult and family learning, ESOL and integration, literacy, numeracy and teacher education. We believe in learning as empowerment and engage adults and families in local communities who face a wide range of issues and barriers to social inclusion. We also train the volunteers and professionals who work with and support them. We lead on local, national and international projects, develop and publish books and resources, and do a wide variety of consultancy work.
Read more
Our Equality and Diversity Statement
Learning Unlimited recognises that everyone has a contribution to make to our society and a right to equality of opportunity.
Learning Unlimited is committed to:
understanding, recognising and valuing diversity
treating everyone fairly and with dignity and respect
pro-actively promoting equal opportunities
removing barriers caused by inequality and discrimination.
No learner, trainee, volunteer, job applicant, employee or organisation/individual to which we provide services will be discriminated against by us on the grounds of:
gender (including sex, marriage, gender re-assignment);
race (including ethnic origin, colour, nationality and national origin);
disability;
religion or belief;
age.
Please let us know if there is anything we can do to support you in accessing our services or working with us.
Follow Us!
With thanks to our funders past and present
Some of our current and recent partners
', '2018-12-07 19:24:30');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2542, 'Proseguendo con l''invio del form
si considera letta e accettata
For further information on the project, please fill in the contact form below : we will reply to you as soon as possible.
The owner of this website is responsible for the processing of any personal information.
All such information will be processed in accordance with Privacy Policy.
Mandatory fields are marked with (*)
Name (*)
', '2018-12-07 19:24:31');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2543, 'NEWCOMER COMPANIES HELPER ABOUT US LOG IN
Meet the Team
We are a team of young people who believe in the potential of diversity and therefore want to actively shape tomorrow''s society.
Meet our team!
How did HIRE start?
HIRE started in fall 2015 as an innovation project of the entrepreneurial scholarship Manage&More at the UnternehmerTUM in Munich. We decided to team up for our shared motivation to give back and impact our society. The goal was to see how we can help in the current refugee crisis in Europe. 
In order find out about the problems more than 40 Newcomers have been interviewed in the early stage of the project. We learned that work is one of the main needs. The goal of HIRE to support the integration of Newcomers into workforce was born.
The team
Christopher Rehberg
Co-Founder & CEO
Raised in East-Berlin, equipped with an Original DDR (Eastern German) birth certificate, successfully migrated into Western Germany, BRD. Still struggling with Bavarian, though.
Johannes Geiger
Co-Founder & CEO
Was always warmly welcomed during his stay abroad in South Africa and Canada. He has been socially engaged with Rotaract for many years.
Timo Ludwig
Niklas Wolf
Co-Founder
Born into a standard-German speaking family and raised among Bavarians, Niklas first foreign language was Bavarian.
Robert Urban
Web Development
Robert''s family benefited of the German integration system in the late 1980s. He wants to help others to get the same chance as he did.
Alexander Dobler
Business Development
A real child of Munich. Now he lives part-time in England and therefore knows how important and appreciated support from locals is.
Felix Thiele
Web Development
Having just arrived in Munich himself, he hopes to help others arriving in Germany with HIRE.
Alexander Modes
Web Development
Loves to live in Germany. He wants to help that other people have the chance to feel like him.
Supporters
For some special things we do not have the needed skills or resources ourself... So we are very very thankful that we have the following people, who (did) help and support us:
Andreas Liebl
McKinsey-trained project mentor. Thank you, Andreas for your support!
Lena May
Thank you Lena , your efforts for HIRE were amazing!
Timo Friesland
Coach and entrepreneur. Timo , the team still needs you! ;)
Zoltan Elekes
Incredible designer and creator. Check out the LittleBlueBox -group!
Manuel Pfeifer
Thank you for the legal work. If you want to work with an amazing lawyer supporting social causes check him out!
Newsletter
', '2018-12-07 19:24:31');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2544, 'About us
STORY
It all began in Summer 2015 with „Hostenstattposten“ („hostinginsteadofposting“), continued to whet the appetite for more in Jannuary 2016 as „Nächstenliebe“ („love of neighbour“), and eventually led to the opening of this restaurant in May 2016: To give back dignity to individuals, to be able to develop a future.
HOSPITALITY
Enjoying Austrian Oriental fusion cuisine six days a week. Offering a rich lunch buffet at noon and the one & only Austrian Oriental set menu family dinner in the evening. For taste knows no borders.
TRUST
What would we be without our generous and broadminded supporters? Acquisition and adaption of the premise as well as the sustained continuation of our Vision is ensured by prominent supporters. We say
THANK YOU! !
ENTERPRENEURSHIP
To not just offer a job to individuals, but to find true entrepreneurial spirits whose potential temporarily lies dormant. To help them take the first hurdles and to equip them with a entrepreneurial tool kit. Additional Habibi & Hawara branches shall be opened in near future.
ARRIVED
From the heart of Vienna into the hearts of the Viennese. The restaurant is located in the first district, right next to the Vienna Stock Exchange and ideally accessible through tram and subway. Parking lots are available as well. The cozy stroll around the adjacent ring road is included in our well-led prices.
FUTURE
Industry specific language and preparatory courses for final apprenticeship examination or concession approval for gastronomy shall complete the already acquired practical education of our employees to allow us to open up additional Habibi & Hawara branches in the near future. For this we are constantly looking for active support and cooperation partners.
Newsletter
Deine Daten werden für keinen anderen Zweck als die Versendung des Newsletters verwendet und nicht an Dritte weitergegeben.Wir weisen darauf hin, dass Du der Verarbeitung deiner personenbezogenen Daten gemäß Art. 21 DSGVO jederzeit widersprechen kannst. Weitere Informationen findest Du in unserer »Datenschutzerklärung.
Ja, ich habe die Informationen zum Datenschutz gelesen und bin mit der Verarbeitung der Daten einverstanden.
Leave this field empty if you''re human:
Folge uns
', '2018-12-07 19:24:31');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2559, 'Please give me a call
Comment
Please enter the word displayed in the image correctly.
 
The fields marked in bold are the mandatory fields!
 
We gladly notify you about the latest updates
Name
', '2018-12-07 19:24:34');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2559, 'We gladly notify you about the latest updates
Name
', '2018-12-07 19:24:34');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2578, 'We''d Love to Hear from You
Name
', '2018-12-07 19:24:34');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (2581, 'Captcha*
GDPR Agreement*
I consent to having HELIOZ store and process the data submitted by me so that my inquiry can be processed.
Bitte dieses Feld NICHT ausfüllen!
', '2018-12-07 19:24:34');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3194, 'What We Do
Free classroom projects
We design high-impact programmable electronics projects, freely documenting how to source, build and teach with them.                     Our builds are Arduino-compatible using cheap prototyping components on solderless breadboard. More about Projects
Kits for sale
We offer pre-bagged kits, to simplify the task of sourcing and preparing our projects for the classroom                     and to support the development of more @ShrimpingIt projects. We offer schools their first example of any kit at                     cost prices. More about Kits
Workshops on request
We run workshops for learners and teachers, introducing solderless breadboard and programming                     tools, with project kits to take away. We also design bespoke workshop series for a host''s                     subject-specific needs. More about Workshops
Projects Already Documented
Blink
Blink is the first build we complete in our workshops.                     It is the simplest possible Arduino-Uno-compatible circuit, made on                     solderless breadboard. You can attach it to your computer and program it to Blink its LED .                     Once you have Blink working, you can go on to make a whole universe of projects.
The @ShrimpingIt projects all follow on from Blink , with build                     diagrams, software, bagged kits and teaching materials. For example you can build                     a Persistence of Vision display to paint your name in the air, a Conductive Keyboard for interactive games driven playfully by                     touched objects just like a MakeyMakey ,                     an Alarm Clock to explore time-driven applications,                     or a Memory Game as a high-impact demonstrator of physical computing                     to wow friends and family.
Since our layout is binary-                     and pin-compatible with an Arduino Uno, you can also take advantage of the huge collection of projects already shared for free by the Arduino                     community, but in a cheaper and more educational way than using an official Arduino board.
Finally, learners can reinforce the success of any of their prototypes by completing a Stripboard build and deploying it permanently with its own power supply                     in a housing they have designed. More about this Project
Persistence of Vision
Once you''ve completed Blink , just another 7 LEDs and a battery pack and you can paint your name                     in the air with our Persistence of Vision project. After changing the text, a more advanced lesson plan incorporates learning binary                     and designing your own 8-pixel-high glyphs to replace characters in the original Commodore 64                     font provided with your own icon designs. More about this Project
', '2018-12-07 19:24:34');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3207, 'Who we are
Staff
Jake Harries (Director of Art & Innovation) is a media artist and composer whose work explores ideas of openness in skills/knowledge sharing and how technology might help facilitate this. Since 2007 he has been curating and developing the digital and participatory arts programme at Access Space. He has an independent practice with film maker Monika Dutta, A little piece of land , focusing on sustainable alternatives to current models of relating to the environment, including food growing and resource consumption. Jake has been involved with creative practice and music in Sheffield since the 1980s. He was a member of industrial/electronic band Chakk , and a partner in the influential FON Studios (Cabaret Voltaire, 808 State, artists from Warp Records and many others recorded there). Chakk was an early adopter of digital music technology owning one of the first samplers in South Yorkshire. In the 1990s and 2000s he was one of the “freestyle techno” trio Heights of Abraham  ( soundcloud ).
 
John Moseley (Refab Space Technician) has a professional background in electronics and media production, with qualifications in electronics, information technology and teaching. He particularly enjoys making robots and electronic musical instruments. John’s family came to Sheffield in 1898 to work as pattern makers and he hopes he continues a tradition of craft and skill sharing. @_jo_mo
 
Jonathan Cook (Administrator & Development Manager) has a background of working with community organisations promoting local empowerment and self help. He has strong financial expertise, and has helped to establish two local credit unions. He writes for local publications and is enthusiastic about the potential for digital technologies to give people a voice.
 
Toni Buckby is an artist working with textiles and interactive electronics. Her work focuses on the intersection between traditional craft techniques, hertiage and digital technologies. She is currently working on projects with the Derby Silk Mill Musuem and the V&A. Graduating from Sheffield Hallam University in 2014 with an MA in Fine Art, she has been with Access Space since 2013 as an administrator and workshop leader in digital making.  https://tonibuckby.com/
 
Joelle Salumu (Administrator) has a range of experience in working with community organisations in practical and office-based roles. Having originally joined Access Space as a volunteer in 2013, she now mainly focusses on grant application and administration. She is currently combining her work with Project Management study at postgraduate level.
 
Directors/Trustees
Mike Hodson has spent 13 years working in community arts and adult education, followed by 7 years in community support and engagement. Now retired. Mike has been associated with Access Space as supporter from 2005, joined the Board in 2010 and has held the position of Chair since 2014.
 
 
Monika Kostera is Professor Ordinaria and Chair of Management at the Jagiellonian University in Kraków, Poland, as well as Professor and Chair in Management at the Durham University, UK, and Guest Professor at Linnaeus University, Sweden. She holds several visiting professorships. She has authored and edited over 40 books in Polish and English, including her last book, Management in a Liquid Modern World with Zygmunt Bauman, Irena Bauman and Jerzy Kociatkiewicz (Polity), as well as and a number of articles published in journals including Organization Studies, Journal of Organizational Behavior Management and British Journal of Management. Her current research interests include organizational imagination, disalienated work, ethnography and critical organization studies. Her website is at: www.kostera.pl
 
Alex McLean (Research Director) has been active across the digital arts since the year 2000, developing a creative approach to computer programming, including live coding music. Alex co-founded the TOPLAP and Algorave movements, and has collaborated widely including as part of Slub, Canute, Aalleexx and xynaaxmue. He completed his PhD thesis on Programming Languages for the Arts at Goldsmiths, during which time he initiated the free/open source live coding language TidalCycles ( http://tidalcycles.org ), connecting code and music to create a rich approach to pattern making. Alex is based in Sheffield UK, and works as part of the research institute in the Deutsches Museum in Munich, using live coding techniques to explore the ancient thought processes of textile weavers, for the European project PENELOPE. He releases his solo music as Yaxu on the Computer Club label, including the EP Peak Cut and album Spicule. He curates the annual Festival of Algorithmic and Mechanical Movement ( http://algomech.com ), bringing together code and machines in the performing arts. Alex has worked with Access Space on major events since 2007 and joined the board of trustees in 2014.  http://slab.org/
 
Susanne Palzer is a cross-disciplinary artist, researcher and performer based in Sheffield. Her current practice investigates the intersection of digital and analogue forms, in particular the fusion of digital technology and physical performance, and linguistic aspects of both performance and technology. The intersection of language systems, especially technical terminology in art/performance and in computing/programming, including speech, physical gestures and movements, is where most of her current work finds its context and the freedom of deconstruction and re-interpretation resides. Susanne Palzer is the curator of OPEN PLATFORM/RAP(s)-TwT. , a micro event that explores the digital with physical means by asking performers to present digital work without using anything digital. Susanne has been involved with Access Space since 2009 and became a director/trustee in 2014.
 
Martyn Eggleton is a coder and digital maker who has been involved in community education and training for over 20 years. He was Technical Manager at Access Space from 2009-2013, leading the computer reuse and technical education. Martyn has built technical widgets and software for various arts projects over the last 10 years and is a regular Youth Zone Facilitator @ MozFest, and a web tech evangelist. He is currently a Skills Trainer at the Sheffield University Technical colleges, supporting employer led projects, classroom programming activities and GCSE Computer Science. Marytn has been a director/trustee since 2014.
 
Lucy Cheesman organises and facilitates inclusive workshops and events and is a founder member of SONA and the Yorkshire Sound Women Network. She recently acted as project lead on a year-long initiative to build a community of practice for women in creative technology and has contributed to research showing how community building approaches can have a significant impact on redressing inequality. Lucy also makes live coded music under the name Heavy Lifting using TidalCycles and Foxdot, collaborating with musicians over the internet and in real life. Along with Alex McLean Lucy co-founded Tidal Club; a monthly meet-up for TidalCycles users. Lucy joined the Access Space board in 2017.
 
Dan Sumption co-founded one of the UK’s first web design agencies, Hard Reality, in 1997, which merged with global ad network Leo Burnett in 2001. He established the arts magazine, FAD in 2002. He helped to create many pioneering online video platforms, including BBC iPlayer, ITV Player and YouView, and now builds cloud-based software at Autodesk. He is also known for his photographic coverage of Sheffield events and nightlife. Dan has been involved with Access Space (who published his photobook “Working Nights” in 2007) for many years, and became a trustee in 2017.
 
Our other board members are Bill Best (Treasurer) and Tom Reavey.
Search
Find Us
Address:
Access Space Network Ltd, 3-7 Sidney St, Sheffield, S1 4RG, UK and Access Space Labs, Fitzalan Square, S1 2AY Sheffield, S1 4RG, UK
Phone: 0114 249 5522
UK Charity No. 1103837, UK Co. Reg. 4560531
Twitter
', '2018-12-07 19:24:36');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3208, 'Hear Sheila Ceccarelli Talking about AccessArt
Proud to be Affiliated With
Many thanks for support from:
Two beautiful books to inspire creativity in children
Buy Signed Copies Direct.
New to AccessArt? Start Here!
Access our Distance Learning Courses
See all Contributors
Sheila, AccessArt: This one''s looking exciting - cutting through to see the pa…
Sheila, AccessArt: Tracing paper always useful for sketchbook exercises - we a…
Sheila, AccessArt: Dear Lise, Never too late to join the sketchbook group and …
Lise B: Some cut out doors to the next page...…
Lise B: I think I may be 4 years too late for this group!…
Sharon Gale: What a wonderful resource! I’ve done puppet making before…
Lucy C: I am so pleased to have found Access Art - what a brilliant…
Sheila, AccessArt: Hi Mar - I''m sorry to hear that - follow this link to find …
Mar: I really want to make a 3-d bird. I am looking for a tutori…
AccessArt Supports your Arts Award
', '2018-12-07 19:24:39');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3285, 'Home About Us
About Us
Fab Lab Connect supports the growth of the Fab Lab Network by building, accelerating and connecting Fab Labs and providing Project Replication Grants. As we worked with grassroot organizations, unmet needs emerged for more compact and less expensive labs.
Schools expressed an interest in introducing their students to digital design and fabrication with a program that fits their requirements. School Fab Lab offers a solution that is more accessible to a wider audience.
School Fab Lab is a STEAM, project-based, integrated education program based on the constructionism theory and focuses on digital design, digital fabrication and robotics. We deliver the program through three main components: teacher training, online platform and machines.
Fab Labs are breaking down boundaries between the digital and physical worlds, this will radically change not only how we interact with everything but also who we are.
Fab Lab Connect offers comprehensive solutions to BUILD  new Fab Labs, ACCELERATE them and CONNECT cross border knowledge transfer.
Join the movement, join the Fab network, join us!
137 Montague Street, Suite 197, Brooklyn NY 11201
Powered By NPOSTMediaGroup.com
', '2018-12-07 19:24:41');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3285, 'This website uses cookies to improve your experience. We''ll assume you''re ok with this, but you can opt-out if you wish. Accept Read More
Privacy & Cookies Policy
', '2018-12-07 19:24:41');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3335, 'Do you need to contact the bespoken team?
 
Whether it’s a new idea for bespoken, reporting spam, giving feedback on the site or anything else for that matter, this is the email to reach us on.
bespoken@blackwoodgroup.org.uk
', '2018-12-07 19:24:48');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3335, 'My Page
About Us
bespoken is an online community that seeks to share information, stimulate innovation and put people who have disabilities at the heart of the design process. We believe that the real experts on disabilities are the people who live with them. bespoken aims to bring together the collective experience and knowledge of its members to support and advise each other.
On bespoken you will find news updates from the world of assistive technology, independent product reviews by our members, as well as ample opportunity to contribute yourself either via our forums or your own personal blog.
 
Background on bespoken
 
bespoken was created by the Blackwood Foundation, a Scottish charity which in 2015 was integrated into its parent organisation Blackwood Homes and Care. Today bespoken is managed by Blackwood’s Research and Innovation department.
 
Born out of a series of workshops conducted in 2011, these demonstrated that people who have disabilities have an unparalleled knowledge and understanding of disability and independent living yet had nowhere to share advice and information.
 
It also became apparent that all too often, adaptations to enhance independent living are poorly suited to the job and the end users needed to be better included into the design process.
 
In both instances, bespoken aims to plug the gap.
 
Sounds great, but how can I get involved?
 
Step one is to sign up  to bespoken so you can receive the weekly bulletin with news and updates of products, concepts and adaptations to enhance mobility and independence.
 
Step two is to get involved in the discussions. You can even start your own! What disability aids do you think could be improved on? What adaptation hasn’t been invented yet but should be?
 
You can use the private message function to chat to the bespoken Team or even email us at bespoken@blackwoodgroup.org.uk and enquire about doing any of the following…
 
Design and focus groups
 
We’ve done a number of focus groups over the years where we’ve stimulated discussion into action by bringing people who have disabilities together with product designers. Are you working on an idea and need some constructive feedback to help shape your concept?
 
Reviews
 
Fancy doing a review of a product or a venue? Determine the suitability, functionality or accessibility of it? We’ve had volunteers help out in the past to review the Edinburgh Fringe as well as the “Harley” mobility scooter. Why not do your own revue?
 
Overview of Blackwood
 
Bespoken is wholly owned by Blackwood Homes & Care which is one of Scotland’s leading providers of housing and care services. Blackwood is a Scotland wide organisation providing social housing for rent and care and support services particularly for those with disabilities. Blackwood has over 1,600 rented homes for tenants across 29 Scottish local authority areas. Blackwood also runs four dedicated care homes in Aberdeen, Glasgow, Greenock and Stirling.
 
Throughout its existence Blackwood has always placed a strong emphasis on innovative design to boost independent living and quality of life for people who have disabilities.
 
', '2018-12-07 19:24:48');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3335, 'My Page
Share ''About Us''
bespoken is an online community that seeks to share information, stimulate innovation and put people who have disabilities at the heart of the design process. We believe that the real experts on disabilities are the people who live with them. bespoken aims to bring together the collective experience and knowledge of its members to support and advise each other. On bespoken you will fin…
You can share this page in two ways…
Share this link:
', '2018-12-07 19:24:48');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3335, 'My Page
Share ''Contact Us''
Do you need to contact the bespoken team?   Whether it’s a new idea for bespoken, reporting spam, giving feedback on the site or anything else for that matter, this is the email to reach us on.  bespoken@blackwoodgroup.org.uk
You can share this page in two ways…
Share this link:
', '2018-12-07 19:24:48');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3368, 'Your Message
Send your content to us
We are now actively looking for content for the archive and will always add content that you send to us. The archive is a labour of love and we update it in our spare time.
Explore the Archive
', '2018-12-07 19:24:54');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3368, 'About Us
Celebrating, Preserving and Sharing Birmingham’s Music Heritage.
We are now actively looking for content for the archive and will always add content you send us. The archive is a labour of love and we update it in our spare time so please bare with us if you regularly check the site and it looks the same – content is always being added.
Building the archive to inspire the future.
The Birmingham Popular Music Archive has been established by Jez Collins in order to recognise and celebrate Birmingham’s rich musical heritage. We want Birmingham to take pride in its musical heritage and to start shouting out about it. Other cities aren’t shy in celebrating their successes and neither should we.
We believe music provides us with memories, individual and shared experiences and self expression. For us, these memories and meaning can be stirred by a vast array of music ephemera, it could be a song, it might be a photograph or a ticket stub or it could be someone else’s recollections that make a connection with you and trigger your music experiences. And we aren’t just interested in the ‘star’ names. We want to hear about ALL the music activity in the city.
We are interested in hearing and sharing stories. Were you in a band? Where did you play? What venues did you go to and who did you sees? Did you work in a Record Shop? Or perhaps you ran a label or were associated with a Pirate Radio station. You may have worked in a rehearsal space, been an sound engineer to a DJ. All this and more, no matter how big or small you think it was. All those personal experiences and memories that surround and inform this vibrant city and it’s music is what we are after.
Our rather big ambition is to capture the entire history of popular music in and from the city.
So whether you were in a band, or were a regular gig-goer, we want to hear from you – Tell us what you know, tell us what you think!
If you want to find out about adding content to the archive, then please see our ‘Contact Us’ page and get in touch.
Send your content to us
We are now actively looking for content for the archive and will always add content that you send to us. The archive is a labour of love and we update it in our spare time.
Explore the Archive
', '2018-12-07 19:24:54');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3370, 'BCH 0.00060217
About Us
Fastest turn around time in the market. Same day settlement. Purchase up to 500BTC daily for personal or business use. Quick approval time. We sell Bitcoin, Litecoin, Ethereum, and Bitcoin Cash
<%=slide%>
Bitcoin of America (www.bitcoinofamerica.org) is a virtual currency exchange registered as a money services business with the United States Department of Treasury (FinCEN)(RegNum).  We operate directly with you, the public, without intermediaries, through a digital platform that facilitates the purchase and sale of virtual, digital currencies, such as Bitcoin.  Using a robust anti-money laundering transaction monitoring system, and superior user-friendly, payment features, we provide rapid and safe transactions with the best rates in the market. A customer can wire payments, make payments at a store location or use a Virtual Currency Kiosk, and get coins the same day.
Our policy is to do business with people whom we can identify, so we do require you to provide certain information, including your government-issued photo identification and any other information that might be needed to make us feel comfortable transacting with you. It’s not just our policy, it’s the law.
Experience doing transactions over the years allows us to know that, even more important than conducting daily transactions, is the support that we give to our clients. We offer full 24/7 support via email. You can email us using the contact page for any technical or transaction question.
    
', '2018-12-07 19:24:54');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3370, 'BCH 0.00060217
Contact us
Fastest turn around time in the market. Same day settlement. Purchase up to 500BTC daily for personal or business use. Quick approval time. We sell Bitcoin, Litecoin, Ethereum, and Bitcoin Cash
<%=slide%>
Just before you contact us , take a look at the most frequently asked questions and see if you can get a quick answer:
I didn''t manage to upload my files to get verified. What should I do?
Make sure the files are not larger than 4MB each. If you still don''t manage to upload them, please email us at helpdesk@bitcoinofamerica.org and we will assist you.
I completed my transfer and updated my details, now what?
We will send your bitcoins minutes after your transfer has cleared in our account. Please notice helpdesk@bitcoinofamerica.org works everyday daily to sell you bitcoins!
I uploaded my files for verification, how long will it take to get verified?
Verifying an account can take up to 6 hours during working hours though it usually takes much less.
I am not getting the SMS verification code?
If your not getting the SMS verification code from the site you most likely have a house phone. In that case you can pick up the phone and the SMS code will be verbally given.
Whats the maximum BTC I can buy?
Once your account is approved you can purchase 500BTC daily.
Your Enquiry
', '2018-12-07 19:24:54');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3370, 'BCH 0.00060213
Contact us
Fastest turn around time in the market. Same day settlement. Purchase up to 500BTC daily for personal or business use. Quick approval time. We sell Bitcoin, Litecoin, Ethereum, and Bitcoin Cash
<%=slide%>
Just before you contact us , take a look at the most frequently asked questions and see if you can get a quick answer:
I didn''t manage to upload my files to get verified. What should I do?
Make sure the files are not larger than 4MB each. If you still don''t manage to upload them, please email us at helpdesk@bitcoinofamerica.org and we will assist you.
I completed my transfer and updated my details, now what?
We will send your bitcoins minutes after your transfer has cleared in our account. Please notice helpdesk@bitcoinofamerica.org works everyday daily to sell you bitcoins!
I uploaded my files for verification, how long will it take to get verified?
Verifying an account can take up to 6 hours during working hours though it usually takes much less.
I am not getting the SMS verification code?
If your not getting the SMS verification code from the site you most likely have a house phone. In that case you can pick up the phone and the SMS code will be verbally given.
Whats the maximum BTC I can buy?
Once your account is approved you can purchase 500BTC daily.
Your Enquiry
', '2018-12-07 19:24:54');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3370, 'XRP / BCH0.0006021 BCH
About Us
Bitcoin of America (www.bitcoinofamerica.org) is a virtual currency exchange registered as a money services business with the United States Department of Treasury (FinCEN)(RegNum). We operate directly with you, the public, without intermediaries, through a digital platform that facilitates the purchase and sale of virtual, digital currencies, such as Bitcoin. Using a robust anti-money laundering transaction monitoring system, and superior user-friendly, payment features, we provide rapid and safe transactions with the best rates in the market. A customer can wire payments, make payments at a store location or use a Virtual Currency Kiosk, and get coins the same day. Our policy is to do business with people whom we can identify, so we do require you to provide certain information, including your government-issued photo identification and any other information that might be needed to make us feel comfortable transacting with you. It’s not just our policy, it’s the law.
Experience doing transactions over the years allows us to know that, even more important than conducting daily transactions, is the support that we give to our clients. We offer full 24/7 support via email. You can email us using the contact page for any technical or transaction question.
We are AMC/BLA complient
', '2018-12-07 19:24:54');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3370, 'What type of content do you plan to share with your subscribers?
Subscribe
Follow us on
© 2018. Bitcoin Of America. All Right Reserved.
Disclaimer: Last update: April 2018. Bitcoinofamerica.org takes legal and regulatory Compliance very seriously. Every effort is made to operate fully compliant with both Federal and State laws and regulations. Bitcoinofamerica adopts Anti- Money Laundering and Know Your Customer Policy to prevent and mitigate possible risk and violations. Currently we do not provide sales of virtual currency to residents from the following states: AK, CT, HI, IA, ID, ME, NY, NM, RI, UT, VT & WA State. We are in the process of reviewing bit-license requirements in those states and hope to be able to serve you in the near future.
Bitcoin Of America Bitcoin Of America 888-502-5003 support@bitcoinofamerica.org
', '2018-12-07 19:24:54');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3432, 'empirica Gesellschaft für Kommunikations– und Technologieforschung mbH
Directors: Werner B. Korte, Simon Robinson
Court of Registration: Amtsgericht Bonn
Number of Registration: HRB 4686
Sales Tax Identification Number according to §27a Umsatzsteuergesetz: DE 122113895
CAP4Access is a project funded by the European Commission in the 7th Framework Programme.
For interacting with user communities there is a second website at www.MyAccessible.EU
', '2018-12-07 19:24:58');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3432, ' 
Why we need to get active
Online maps offer great opportunities to indicate which places and ways are accessible and which ones are not. Locations and routes on maps can be enhanced with accessibility related data, commented, and visualised with photos. These possibilities are not yet widely used, however, and their full potential still needs to be reaped.
Everyone can contribute to online maps for accessibility by collecting, entering and validating information, and by joining others for taking action to tackle existing barriers. The map is then turning into a “collective awareness platform”. The CAP4Access project is part of a related initiative by the European Commission .
Recent developments in technology and society have opened up new sources of data that can contribute substantially to accessibility mapping. Automatic sensors in smartphones may collect data about, for instance, the effort needed to pass a certain way with a wheelchair. Public administrations have abundant data about features of the built environment – these are increasingly being made available as "open data". In both cases, research is required to unlock the potential for the benefit of citizens. This is what CAP4Access aims at.
 
What we want to achieve
The objective of CAP4Access is to develop and pilot-test methods and tools for collectively gathering and sharing information about accessibility of public spaces. Our tasks:
Documentation, annotation and discussion of places and routes about their accessibility.
Visualising the data in ways which are intuitive and highly attractive for target users.
Route planning and navigation.
Creating awareness and preparing effective measures for eliminating barriers.
Applications will be based on OpenStreetMap, the "Free Wiki World Map". We will make best use of established practice, in particular Wheelmap . All newly developed tools will be available for interested parties – such as city administrations – as open software under the GNU Lesser General Public License (LGPL).
Our four test sites are in Vienna (Austria), London (United Kingdom), Elche (Spain) and Heidelberg (Germany).
 
CAP4Access promotes innovation for the people by the people. Specifically, we address the following groups:
People with limited mobility and their representatives
Policy-makers and public administrators
', '2018-12-07 19:24:58');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3450, 'Skip to content
What we do
Catalyst is a housing association. We provide housing solutions and opportunities for those who can’t afford a home without our help. We fund our activities through building and selling homes, and through income from our rented properties. We are a not-for-profit, charitable organisation with a turnover of c. £170 million. In 2018 we made the Sunday Times Top 100 Best Companies to Work For (Not for Profit).
There are more than 600 of us, spread across multiple teams and locations, with our Head Office based in Ealing, London.
Whatever stage you’re at in your life and career, there’s plenty of opportunity at Catalyst for people with energy, commitment and a sense of purpose.
', '2018-12-07 19:25:00');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3458, 'Livinglab approach
A Living Lab is an ecosystem for experimentation and co-creation with real users in real life environments, where the end users together with researchers, firms and public institutions jointly explore, design and validate new and innovative products, services, solutions and business models.
 
', '2018-12-07 19:25:04');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3458, 'E-zavod, Zavod za celovite razvojne rešitve
E-Institute, Institute for Comprehensive Development Solutions
Čučkova ulica 5 Ptuj 2250 Slovenia
This email address is being protected from spambots. You need JavaScript enabled to view it.
+386 (0)2 749 32 27
', '2018-12-07 19:25:04');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3481, 'This field is for validation purposes and should be left unchanged.
This iframe contains the logic required to handle Ajax powered Gravity Forms.
', '2018-12-07 19:25:05');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3483, 'This iframe contains the logic required to handle Ajax powered Gravity Forms.
Already registered on the CITA website?
Please log in as an existing charity or volunteer
Not yet registered on the CITA website?
Please sign up as a new charity or volunteer
“Ian has been fantastic to work with. He has given his time happily and we have felt that he has our interests at heart. His dedication and commitment to helping the charity work to a solution has been first class. He has always been very enthusiastic and made us feel as important as his “day job”. We would have been utterly stuck without him.”
Director, The Ectopic Pregnancy Trust - 05/01/2017
', '2018-12-07 19:25:06');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3484, 'For general  information about the project:
Britt Ann Kåstad Høiskar: bah@nilu.no
Contact persons for the specific cities:
Oslo: Britt Ann Kåstad Høiskar ( bah@nilu.no )
Edinburgh: Joanne Crawford ( joanne.crawford@iom-world.org )
', '2018-12-07 19:25:06');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3492, 'Office Address: 23/F, Chun Wo Commercial Centre, 23-29 Wing
Wo Street, Central, Hong Kong.
Fax: (852) 3105 9713
Email: info@nullcivic-exchange.org
You can subscribe to our newsletter at this link . You can also unsubscribe via the newsletter itself, or by emailing us at the above address.
Want to send us a message?
Your Name (required)
', '2018-12-07 19:25:07');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3517, 'Attend EUDojo on October 17 in the European Parliament, Brussels!
27 July 2018 · by Nuala McHale
Hosted by Seán Kelly (MEP), this years EU Dojo is taking place on Wednesday 17 October in the European Parliament, Brussels to celebrate EU Code Week. At the event MEP’s will be taught their first lines of code by CoderDojo youth attendees! Would you like to attend the event in Brussels and teach the MEP’s a thing or two about coding? Get Involved We are looking for Mentors and Champions from all over Europe to bring young people from their Dojos to Brussels on 17 October. The theme of this years event is libraries! As a… Read More
What I’ve learned through my internship with CoderDojo
18 July 2018 · by Maimuna
My experience as a software engineering intern at the CoderDojo Foundation has been nothing short of rewarding. I have not only been able to grow in my knowledge of software development practices, principles, and information – but in confidence and knowledge of myself as a developer, employee, and student. What I have learned I have gained experience in various technologies during my time here working on the CoderDojo platform. I have been working a lot on the frontend part of the stack – learning vue and about css standardization practices, unit and e2e testing… Read More
A new, easier booking process and an exciting few months ahead!
4 July 2018 · by Conor Murphy
Conor, Product Manager. We’ve released a completely new simplified process for parents/guardians to book their children into a Dojo event on the CoderDojo platform (Zen). We hope that you find it as useful and as easy to use as we think you will. I want to take you through why and how we built it, and what it means for how we’re building the CoderDojo platform into the future. Why rebuild the booking process? We noticed several things that convinced us to remake the booking flow for Zen. The majority of champions/Mentors that had complaints about the Zen platform… Read More
Apply for a Travel Bursary to DojoCon 2018!
6 June 2018 · by Nuala McHale
DojoCon is the annual global CoderDojo conference specifically tailored for adult volunteers involved in Dojos worldwide. This flagship community event is a fantastic opportunity to connect with other generous, experienced and like minded people who play an instrumental part in the CoderDojo movement. This years community conference is set in the beautiful city of Kilkenny, Ireland and will take place from Friday 19th – Sunday 21st of October 2018. What can you expect? On Friday an opening ceremony will be held in the upstairs function room of our City’s award winning Left Bank bar, located across… Read More
Pete: Embarking on a new adventure
17 May 2018 · by Giustina Mizzoni
After four years,  on the 25th of May, Pete O’Shea will be finishing up with us at the CoderDojo Foundation to embark on a new adventure. Pete was one of the first people that I hired at the Foundation and its been a privilege to work with him  and watch him grow over this time. He has been a pivotal team member in growing the community team and the supports that we provide to the global community.   New Opportunity “During my time at the Foundation I have had the privilege to meet so many… Read More
What we discussed on our April Community Call!
17 May 2018 · by Nuala McHale
Did you miss out on our latest Open Community Call, on 24 April, facilitated by the CoderDojo Foundation? Don’t worry you can listen to call and see the resources that were mentioned below! The open call was hosted by Pete O’Shea, from the CoderDojo Foundation. Watch and listen to the call in it’s entirety Slides and call agenda Community Call – 24th April 2018… Read More
1
', '2018-12-07 19:25:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3517, 'CoderDojo Recommended Practice
4 June 2014 · by PeterOShea
Open Consultation Period As you may know, the CoderDojo Foundation is inviting the entire global CoderDojo Community for an Open Consultation Period (11th of June 2014 to the 13th of July 2014 at 12 midnight) during which they can give their feedback on Recommended Practice. We are happy to announce that this period has officially started and is open until the 13th of July 2014. During this time any Dojo can submit feedback, or arrange a call with the CoderDojo Foundation team to discuss their thoughts. The Foundation is committed to the CoderDojo community and value their input greatly on… Read More
', '2018-12-07 19:25:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3517, 'What we discussed on our September Community Call!
26 September 2017 · by Nuala McHale
Did you miss out on our latest Open Community Call, on 20 September, facilitated by the CoderDojo Foundation? Don’t worry you can listen to call and see the resources that were mentioned below! The open call was hosted by Pete O’Shea, from the CoderDojo Foundation. Watch and listen to the call in it’s entirety here: On the open call, we focused on the below discussion points; Giustina, the Foundation’s Executive Director discussed updates to the CoderDojo Charter and asked for the communities input. Translation of the charter was discussed. our first guest Dave Honess from the Astro Pi project explained how… Read More
Launching the 2017-18 European Astro Pi Challenge!
26 September 2017 · by Nuala McHale
Astro Pi is an annual science and coding competition where school-age youths code is run on the International Space Station! We want to inspire as many Ninjas as possible to further their interests in space science with code! The challenge is open to CoderDojo clubs and schools in all ESA member countries (most of Europe) including Canada and Slovenia. Some Dojos have already gotten involved, including Neffies Dojo in France, so why not get your code into space? Two missions are available; Mission Zero (submission closes on 26 November) A non-competitive mission for young people… Read More
Join our Open Community Call on September 20th!
11 September 2017 · by Nuala McHale
Our next Open Community Call will be on Wednesday 20 September at 11am UTC (which is 12 noon in the ROI/UK). The call will be facilitated by the CoderDojo Foundation. On this open call: Giustina, the Foundation’s Executive Director will discuss updates to the CoderDojo Charter and ask for the communities input our first guest Dave Honess from the Astro Pi project will explain how to get your ninjas code in space with Astro Pi. Philip / Ciara from the content team, will be sharing news on… Read More
Attend #EUDojo on Oct 17th @ the European Parliament, Brussels!
4 August 2017 · by Pete
Hosted by Seán Kelly, MEP from 12:30pm – 2:30pm on Tuesday 17th October 2017. The EU Dojo is taking place again this year on the 17th October in the European Parliament in Brussels to celebrate EU Code Week. At the event MEP’s will be taught their first lines of code by CoderDojo youth attendees! Would you like to attend the event in Brussels and teach the MEP’s a thing or two about coding? Get Involved We are looking for Mentors and Champions from all over Europe to bring young people from their Dojos to Brussels on… Read More
Launching Eventbrite Integration on the CoderDojo Community Platform
19 July 2017 · by Nuala McHale
The CoderDojo Community Platform (Zen) enables Champions, Co-Champions and those with Dojo Admin rights the ability to create ticketed events. However, some Dojos already use Eventbrite for helping them organise and manage their Dojo events and we want to make sure parents and young people can find and book into all events easily. Does your Dojo currently use Eventbrite for their events? If your Dojo is one of these you can now integrate your Dojo events created on Eventbrite with their Dojo page on the CoderDojo Community Platform! You still manage events directly on Eventbrite but… Read More
Community Story: CoderDojo Valencia at the CoderDojo Coolest Projects by Bernat
12 July 2017 · by Pete
“Ladies and gentlemen in a few minutes we will be landing at Dublin airport”. And so on Thursday 15th June last began our great adventure that brought 4 young people between 12 and 14 years from the programming club CoderDojo Valencia to the Coderdojo “Coolest Projects” Awards 2017 which took place on Saturday 17th June at the Royal Dublin Society (RDS). At this fantastic global programming event, hundreds of children between 7 to 17 years of age from 17 countries, members of more than one thousand Dojos from around the world, present their projects in a contest with cool… Read More
', '2018-12-07 19:25:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3517, 'What’s happening at DojoCon Japan 2017?
2 November 2017 · by Nuala McHale
On Saturday, November 4th DojoCon Japan 2017 will welcome over 300 CoderDojo Champions, Mentors and Ninjas from all over Japan to celebrate open source programming education. Though admission is free, tickets are already sold out! DojoCon Japan 2017, the second regional DojoCon held in the country, will take place at Ritsumeikan Future Plaza Event Hall in Ibaraki-city, Osaka, Japan from 10:30 to 6:30pm. Connect Dojo to Dojo The theme of this years event is “Connect (Tsunagaru / つながる) Dojo to Dojo”, and aims to create a space for Dojo members from all over Japan, to interact,… Read More
Job Opportunities at the CoderDojo Foundation
1 November 2017 · by Nuala McHale
CoderDojo is a global movement of free computer coding clubs for young people. The first Dojo was started six years ago, and there are now more than 1500 active Dojos in 75 countries, giving 45000 young people the opportunity to learn how to code in a fun, social environment. The CoderDojo movement is supported by the CoderDojo Foundation, a registered charity based in Dublin. In May 2017 the CoderDojo Foundation joined forces with the Raspberry Pi Foundation. We currently are looking for experienced and enthusiastic managers for the below positions to join our team. We are passionate about education and technology. Read More
We’re Hiring an Events Manager
26 October 2017 · by Nuala McHale
Location: Dublin, with travel to other sites and event venues Rate: €35000 – €45000, dependent on experience Basis: permanent; full-time, part-time or flexible Eligibility: you must be eligible to work in Ireland The role CoderDojo is a global movement of free coding clubs for young people between the ages of 7 and 17. Clubs (Dojos) running all over the world give young people the opportunity to learn to build websites, apps, programs, and games, and to explore technology. The first Dojo was started six years ago, and there are now more than 1500 active Dojos in 75… Read More
What I’ve learned and am still learning at the CoderDojo Foundation
25 October 2017 · by Nuala McHale
Graham is studying computing at Dublin City University. As part of his third year work experience he got the opportunity to work for six months as a software development intern with the CoderDojo Foundation. Below Graham notes the benefits of getting practical experience, what he has learned and how he continues to learn, as well as the main highlights of his time with the Foundation. I’ve been interning as a Software Engineer with the CoderDojo Foundation for six months now. My role has been focused on the development of Zen, the… Read More
We’re hiring a Partnerships Manager!
24 October 2017 · by Nuala McHale
Based: Dublin, with travel to other sites and event venues Rate: €35000 – €45000, dependent on experience Basis: permanent; full-time, part-time or flexible Eligibility: you must be eligible to work in Ireland   The Role CoderDojo is a global movement of free computer coding clubs for young people. The first Dojo was started six years ago, and there are now more than 1500 active Dojos in 75 countries, giving 45000 young people the opportunity to learn how to code in a fun, social environment. The CoderDojo movement is supported by the CoderDojo Foundation, a registered charity based… Read More
Join the Europe Code Week 2017 ?? celebration ? !
2 October 2017 · by Pete
The Europe Code Week celebration is here! During the month of October, millions of children, young people, adults, parents and teachers will come together at events, in classrooms and libraries across Europe to learn to create with code! EU Code Week is a grass-roots movement run by volunteers who promote coding in their countries, as Code Week Ambassadors. Each year more than half a million people participate in with coding events in countries in Europe and beyond.   Join the celebration and register your next Dojo event! Each year the CoderDojo movement has been involved in Code Week, you can… Read More
', '2018-12-07 19:25:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3517, 'Attend the BT Young Scientist with CoderDojo In January 2018!
6 December 2017 · by Pete
For the fith year in a row there will be an interactive CoderDojo Booth at the BT Young Scientist event (10th – 13th Jan 2018). We are looking for CoderDojo Ninjas from across the Republic of Ireland to join us on the day and showcase the awesome projects that they’ve made at CoderDojo for one or more of these days (either the 10th, 11th, 13th or 13th of January)! We will be looking for more people on the 10th and 11th to attend, so keep that in mind when filling out the applications. Showcase your cool Project! We are looking for CoderDojo Ninjas, living in Ireland, to apply to… Read More
How parent & Mentor, Wendy, created a space to support girls in her Dojo
4 December 2017 · by Nuala McHale
Wendy, a parent and Mentor at Clonakilty Dojo in Cork, shares her experiences of challenging gender disparity in her Dojo and how she developed a space to engage girls more: Our Dojo, which is in its 6th year is quite large. Approximately 60-75 children aged from 7–17 attend each session. In 2015 we had a heavy representation of boys compared to girls, with a 90:10 split. Each year, very few girls would return after the Christmas break. In addition, girls might come to hang with their brothers – but few would engage with the technology. To tackle the… Read More
Coolest Projects 2018 date announced!
29 November 2017 · by Nuala McHale
CoderDojo Coolest Projects is a world leading showcase that empowers and inspires the next generation of digital creators, innovators, changemakers and entrepreneurs. Young people from CoderDojo clubs around the world will descend on the RDS Simmonscourt, Dublin for the opportunity to present the projects they have designed and built at their local Dojo. Last year saw over 700 Ninjas from 16 countries including Australia, Argentina, United States, Romania, Ireland, Netherlands and Japan showcase their projects to more than 10,000 spectators at this one day event. Today we are delighted to announce that… Read More
Announcing the hosts of DojoCon 2018!
7 November 2017 · by Nuala McHale
DojoCon is the annual global conference for CoderDojo Community members. It is an opportunity for all those who volunteer their time to support, organise and Mentor at Dojos to meet, share their experiences, develop relationships and learn from each other. This years DojoCon saw Community members travelling to Warrington; from Belgium, Bulgaria, France, Ireland, Italy, the Netherlands, Poland, Slovakia, Spain and the UK to learn new insights and skills to bring back with them to their local areas. Who will host DojoCon 2018? We are delighted to announce that CoderDojo Kilkenny (Ireland) will host the event… Read More
CoderDojo Foundation 2017 Q3 Report
5 November 2017 · by Nuala McHale
Throughout the third quarter we focused on contacting community members about approaches and resources they use to engage girls in their Dojo sessions. From late August through to September we released resources and advice for Dojos returning back after the summer break as part of our #BackToDojo campaign. Additionally, the first online content hackathon was successfully organised in order to better engage volunteers wishing to give their time to translating and creating learning resources.   Download our Q3 Report as a PDF here. Read More
Get involved in our December 2nd Hackathon!
2 November 2017 · by Philip Harney
Last July we ran our first Content Hackathon, which focused on translating our learning resources and materials from English into other languages. It was a 24 hour effort, giving everyone a chance to participate for a few hours no matter where they were and involved CoderDojo Community members from all over the world. It was a great success, with opportunities for us to learn and improve on it. You can read more about what we produced and what we learned here. (p.s. This is the blog I refer to in the video!)… Read More
', '2018-12-07 19:25:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3517, 'Coolest Projects International Travel Bursary: Deadline Extended!
2 February 2018 · by Nuala McHale
We’re accepting applications for those seeking to receive a bursary for travelling to Coolest Projects International 2018 from across Europe. Last year young people traveled from 17 countries to attend the event, and this year we want to help even more young people travel to Ireland to participate! The 2018 Coolest Projects showcase will be held on May 26 in the Simmonscourt complex in the RDS, Dublin. Our goal is to have over 100 projects from outside of Ireland showcased at the event! Deadline for Applications Extended! Bursaries have been extended until midnight GMT 7 Febuary 2018 – to apply just enter… Read More
We’re Hiring a Community Coordinator for Ireland
31 January 2018 · by Nuala McHale
Location: Dublin, with travel to other sites and event venues Rate: €27500 – €38000, dependent on experience Basis: permanent; full-time, part-time or flexible Eligibility: you must be eligible to work in Ireland The Role CoderDojo is a global movement of free computer coding clubs for young people. The first Dojo was started six years ago, and there are now more than 1500 active Dojos in 75 countries, giving 45000 young people the opportunity to learn how to code in a fun, social environment. The CoderDojo movement is supported by the CoderDojo Foundation, a registered charity based in Dublin that is part of… Read More
Reaching the Stars: How Dojos are participating in Astro Pi
30 January 2018 · by Amy O''Meara
Many Ninjas in our global community got involved recently in  Astro Pi: Mission Zero; a project whereby young people have the opportunity to write code which is run onboard the International Space Station for 30 seconds. The code is displayed by two specially designed Raspberry Pis, called Astro Pis, which are each equipped with a camera and an LED display. They also have a range of environmental sensors for measuring humidity, pressure, and temperature. Astro Pi is an initiative of the Raspberry Pi Foundation, and aims to actively… Read More
How we’re building Zen in 2018
15 January 2018 · by Conor Murphy
Hey there.   It’s been 3 months since I joined CoderDojo and I guess it’s time to introduce myself properly. My name is Conor and I’m the new Product Manager here at the CoderDojo Foundation. First of all I want to say, in the few months I’ve been here I’ve been stunned at the huge ambition and enthusiasm of it all. We’re a community with a clear of focus of giving as many kids as possible the opportunity to build and learn about things they never thought possible. And I hope as the… Read More
Growing with the CoderDojo movement
21 December 2017 · by Pete
“2017 marks my 4th year here at the Foundation. This year the team has achieved so much, we released a load of new content, created the CoderDojo Girls guide, hosted the annual survey, supported many events and much more!  One highlight for me was attending the Bulgarian Coolest Projects and then seeing some of the ninjas travel with their projects to participate at the Coolest Projects in Dublin with the support of their mentors and champions!” The CoderDojo community has grown for a humble number of 200, back when I started, to over 1,600 Dojos in 85+ countries… Read More
Club Program Manager Opportunity in North America
12 December 2017 · by Nuala McHale
Based: Oakland, CA, with frequent travel to other sites and event venues Rate: $60,000–$70,000 per annum Basis: permanent Eligibility: you must be eligible to work in the United States The role The Raspberry Pi Foundation’s mission is to put the power of digital making into the hands of people all over the world. This is a deliberately bold statement of our intent. Since launching our first product in 2012, we have sold more than 15 million Raspberry Pi computers and helped kickstart a global movement to get more people involved in computing and digital making. We have trained thousands of teachers… Read More
', '2018-12-07 19:25:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3517, 'We’re hiring a Community Intern
3 April 2018 · by Nuala McHale
Location: Dublin Salary: €22500 Basis: full-time six-month internship (possibility of extension to twelve months) Eligibility: you must be eligible to work in Ireland About us The CoderDojo Foundation is a Dublin-based educational charity that works to give every child the opportunity to learn how to code and create with technology. We do this through supporting the volunteer-led CoderDojo movement. Founded in July 2011, the movement is based on open-source principles, meaning that anyone anywhere in the world can start a Dojo. Clubs (Dojos) run all over the world on a regular basis, giving young people between the ages of 7 and 17… Read More
CoderDojo Annual Survey Results 2017
26 March 2018 · by Nuala McHale
In November, we opened the CoderDojo Annual Survey. All Dojos running regularly across 75 countries were invited to share their experiences. The information from the Annual Survey helps us determine the impact that the CoderDojo movement is having and it allows the Foundation to receive feedback on what the community needs to guide our future strategy.   If you have any feedback please feel free to share your insights or ask questions about the survey results on our community forums. Thank you to all 520 Dojo champions and lead mentors who took the time to fill in this… Read More
Help by Volunteering at Coolest Projects International 2018
24 March 2018 · by Nuala McHale
In order to run an amazing event for the hundreds of children and teens who attend and present at Coolest Projects International in Dublin on Saturday May 26th, we need volunteers like you with a wide variety of skills, non-technical or technical! Without your help we literally couldn’t run Coolest Projects International. In 2017 we had over 250 people sign up and help at the event, many of whom have been with us for a number of years, and this year we will need even more volunteers to help everyone at the event (including the volunteers) to have an… Read More
Coolest Projects: Matei’s Lost World and the learning opportunity it created!
28 February 2018 · by Amy O''Meara
In the run-up to Coolest Projects International 2018 on 26 May, we will be sharing some inspiring stories of  participants in last year’s event to tell you about their unique journey: from initially conceiving their project idea, to developing it into a reality and presenting it on the day. Matei and ‘The Lost World’ Matei, aged 10,  is a Ninja at his local Dojo, Cluj-Napoca @ Academy+Plus in Romania. He kindly shared with us how he developed his project The Lost World, and told us about his… Read More
Win a Pi Zero and more by sharing #MyCoolestProject
25 February 2018 · by Nuala McHale
Ahead of Coolest Projects International (Dublin), and regional events in Belgium and the UK, we want to see what fun and useful ideas young people are working on at their local Dojo, and reward them for doing so! Great, how do I get involved? We want to learn more about how you created your project, any challenges you faced and how you are overcoming them. To participate share a video, photos and text about the process of making your idea a reality using the hashtag #MyCoolestProject either on Instagram, Facebook… Read More
Are your children prepared for the black box, digital and creative society?
3 February 2018 · by Nuala McHale
Chris, is a web developer and chairman for CoderDojo Nederland. In this role he is responsible for driving and supporting the work of the Dutch CoderDojo community. He has been the Champion of Rotterdam Dojo for the last four years and CoderDojo Leiden Champion for over a year. Today he wants to share his personal motivation to work for CoderDojo, and why he believes it is necessary. Black Box Society Do you remember the old MSN Messenger or MySpace profiles? With those you could go crazy customizing everything to your liking. You had a lot of control with HTML, color codes,… Read More
', '2018-12-07 19:25:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3517, 'CoderDojo Foundation Annual Report 2017
12 May 2018 · by Nuala McHale
We are delighted to release our Annual Report on the activities and success of the CoderDojo movement during 2017. We celebrated 6 years of CoderDojo, supported Dojos in 92 countries, launched the Girls initiative with the goal of increasing the number of girls in the movement towards gender parity, we released our second book, and our global conference Dojocon was held outside of Ireland for the first time.  In May we joined forces with the Raspberry Pi Foundation to create one of the largest sustained, global efforts to help young people learn computing and digital making. Overall, 2017 was a… Read More
We’re Hiring a Senior Community Manager and a Community Manager!
4 May 2018 · by Nuala McHale
Are you interested in using your skills to further the work and mission of the CoderDojo movement? Looking for a fast-paced rewarding role in a mission-focused organisation? Apply to one of our two Community vacancies today.   About Us The CoderDojo Foundation is a Dublin-based educational charity that works to give every child the opportunity to learn how to code and create with technology. We do this through supporting the volunteer-led CoderDojo movement. Founded in July 2011, the movement is based on open-source principles, meaning that anyone anywhere in the world can start a Dojo. Clubs (Dojos) run all over… Read More
Coolest Projects Belgium 2018
2 May 2018 · by Conor Murphy
On a beautiful sunny day on the 22nd of April I travelled to Belgium to visit Coolest Projects Belgium 2018. It was an amazing event with 91 kids building 64 projects. Nearly 100 volunteers ran a very fun and professional event which attracted as many as 3,000 visitors across 3 hours in the Technopolis Mechelen. It was so much fun to see all the amazing projects; from live chickens, water conservation, football playing robots, 3D animations to walking sticks that can sense the world for you. These very impressive projects were made even more impressive when the friendly and proud… Read More
How New Dojos in Ireland and the UK can avail of a Starter Pack!
2 May 2018 · by Nuala McHale
At the CoderDojo Foundation our mission is to give more young people the opportunity to code and create. To focus on this mission, our aims are twofold; to grow the amount of Dojos and to support and retain the clubs that are currently running. To achieve both these aims we are piloting a new initiative of CoderDojo starter packs. This helpful pack includes hardware, educational content and materials which will help you create a sustainable Dojo for years to come. From today, these are available to Dojos who are starting in the UK & Ireland. Read More
How to Join our Community Call on Tuesday 24 April
12 April 2018 · by Nuala McHale
Our next Open Community Call will be on Tuesday 24 April at 1:00pm UTC+1 (find your timezone here). The call, which will be facilitated by members of the CoderDojo Foundation, will include an interview with Dojo resource creators Philip and Ciara. They’ll be sharing what they’ve learned through developing our newest online course and the upcoming Champions handbook. We want all those involved with CoderDojo around the world to join, listen in, share their insights and ask questions! The Agenda: Time (ROI/UK)TopicWho 13:00-13:10Welcome & updates! Q&ARoss CoderDojo Foundation 13:10–13:20A new, easier booking process Maimuna… Read More
How our new free online course will help you start a Dojo!
9 April 2018 · by Nuala McHale
You can now sign-up to our new free online course — Starting a CoderDojo club — in which anyone can learn more about CoderDojo and how they can set up one of these free coding clubs for young people in their local area. With only two weeks until the course begins I wanted to highlight what will be included on the course content as well as the motivations behind it from the courses creator, so you know what you’ll gain by participating. Join the course!   What is CoderDojo? CoderDojo is… Read More
', '2018-12-07 19:25:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3517, 'How to Get Free .xyz Domain names and more!
23 June 2017 · by Nuala McHale
_____________________________________________________ This offer is now over. Stay up to date with future competitons and offers for Dojos on our Forums, Facebook Page or Twitter. ______________________________________________________ The awesome folks at .xyz are providing free access to their .xyz domain names and other services to all those involved in Dojos worldwide! For the next month, all members of the CoderDojo community can apply to get all of the below for FREE: A .xyz domain of your choice Weebly website builder Hosting WHOIS privacy protection Limited edition XYZ swag bag A chance to be interviewed for their #WebsiteWednesday series… Read More
Deadline Extended for Coolest Projects 2017 Entries!
21 May 2017 · by Nuala McHale
Our project applications are nearing capacity! But to let last minute projects register we have extended the project applications deadline for The CoderDojo Coolest Projects Showcase until midnight, next Sunday, May 28th. The Coolest Projects Showcase is both a competition and exhibition that supports a generation CoderDojo innovators, creators and entrepreneurs aged from 7 to 17. The Coolest Projects offers young people the opportunity to showcase the skills they have learned at their local CoderDojo, to an international audience. So register your Scratch, website, games & web games, mobile apps, hardware or evolution (for more complex, advanced or released) projects asap!… Read More
Our 5 latest improvements to the Community Platform!
15 May 2017 · by Nuala McHale
Software Engineers, Daniel and Guillaume, and Content Lead Philip have made some awesome design and feature improvements to the Community Platform recently including: 1. New “My Dojos” page design The new layout (see left) makes creating events, viewing & managing your members, editing your Dojo profile and finding resources easier for Champions, Co-Champions & Dojo Admins. 2. New “Contact Dojo Members” functionality Champions, Co-Champions & Dojo Admins can now email the members of their Dojo(s) (or filter so that you only email a specific group eg. mentors) from within the platform. Find out more about this really useful… Read More
How to email members of your Dojo from within the Platform
13 May 2017 · by Giustina Mizzoni
Last month, among designing the new Start a Dojo process, drafting specs for a new content platform and working on end-to-end test our software team pushed a new feature for the Community Platform (Zen) that I, among many community members had requested and were waiting for for in great anticipation!   Sure, the new My Dojo’s page that make creating events, viewing & managing your members, editing your Dojo profile and finding resources easier for those managing a Dojo, however the new feature is something a little smaller that directly benefits Champions and Mentors with Admin privileges… Read More
How Badges benefit your child
5 May 2017 · by Pete
Digital badges are becoming a popular way to acknowledge digital skills achieved by attendees at Dojos. They are almost like a Scout’s badge which are awarded once someone reaches a certain level or learns a particular skill except they are digital! How are CoderDojo Badges useful? CoderDojo digital badges allow youths to: verify their programming skills highlight event participation showcase their skills for employment or education They are a great method of validating digital and other skills which aren’t always very visible.   How… Read More
Rewards & Recognition: CoderDojo Digital Badges
2 May 2017 · by Pete
Meet Ruby, she is a CoderDojo attendee that has developed an interest in building websites and3D printing. She has been attending her Dojo, which is hosted in a MakerSpace, once a week for the past 3 years. During this time she has taught herself, with the support of mentors, how to fully create and host her own website. She has recently moved on to learning all about designing and 3D printing. All these skills she has learnt via self directed learning in the informal learning setting created and supported by  the mentors at her local CoderDojo.   Rewards… Read More
', '2018-12-07 19:25:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3517, 'CoderDojo Bulgaria and the Coolest Projects!
28 April 2017 · by Pete
CoderDojo Bulgaria Please meet Adriana and Vessela, who initially started the first Dojo in Sofia, Bulgaria. Since then they have been growing the community and inspiring others to start their own. Now there are 5 verified Dojos across Bulgaria with more on the way! To continue this journey by representing the CoderDojo Community in Bulgaria they recently became a new addition to growing list of official CoderDojo Regional Bodies, whose remit is to support and grow the CoderDojo Community in their country. CoderDojo Coolest Projects Bulgaria The first CoderDojo Coolest Projects was held by CoderDojo DCU in 2012 to reward… Read More
April: Focus on Online Safety & Dojo Recommended Practices
3 April 2017 · by Nuala McHale
Online Safety Tips & Advice: The internet has become an integral part of lives of young people around the world. Young people start using computers from an early age and are increasingly using the internet whether at home, in school, or on their mobile phones. This month one of our focuses is on online safety. Parents and volunteers should understand enough about the internet to keep young people safe from harm. It is equally important that we equip youths with skills and knowledge so they can utilise the internet positively and responsibly. The internet is a significant part of… Read More
Apply to receive Micro:bits for your Dojo! (UK)
14 March 2017 · by Nuala McHale
Through the CoderDojo partnership with the Micro:bit Foundation, we are happy to now open applications for you to get Micro:bit kits for your Dojo! Currently this offer is limited to those Dojo’s in England, Wales or Northern Ireland (who are registered on the CoderDojo Community platform). We are hoping to work up an offer for Dojos beyond these countries at a later date! Dojo’s in Scotland can contact the CoderDojo Scotland team directly to arrange if you haven’t already received your Micro:bits. We will be distributing Micro:bits via our friends at Pimorni and in batches of 10 devices per Dojo. Read More
Roland Horvath: Future Makers Awards Finalist
9 March 2017 · by Nuala McHale
During Europe Code Week 2016, the Future Makers Awards invited youngsters between 10-17 years old to ‘make the future’ and use their coding superpowers to bring about a positive social impact and change in their communities. We received applications from 13 countries! The projects were judged on their technological & coding complexity, their positive social impact and how innovative they are. Roland is one of the finalists in the 14-17 year old category who you can read more about below! Roland Horvath Roland is 15 years old and from Hungary. He has developed an app with interactive software for learning and practicing… Read More
Andrei Covaci: Future Makers Awards Finalist
3 March 2017 · by Nuala McHale
During Europe Code Week 2016, the Future Makers Awards invited youngsters between 10-17 years old to ‘make the future’ and use their coding superpowers to bring about a positive social impact and change in their communities. We received applications from 13 countries! The projects were judged on their technological & coding complexity, their positive social impact and how innovative they are. Andrei is one of the finalists in the 10-13 year old category who you can read more about below! Andrei Covaci Andrei is 12 years old and from Romania. He has attended CoderDojo Oradea since it started, 3 years ago. For… Read More
Katie Waters: Future Makers Awards Finalist
2 March 2017 · by Nuala McHale
During Europe Code Week 2016, the Future Makers Awards invited youngsters between 10-17 years old to ‘make the future’ and use their coding superpowers to bring about a positive social impact and change in their communities. We received applications from 13 countries! The projects were judged on their technological & coding complexity, their positive social impact and how innovative they are. We will focus in on each of the 10 finalists over the next two weeks, Katie is one of the finalists in the 10-13 year old category who you can read more about below! Katie Waters Katie is 11 years… Read More
', '2018-12-07 19:25:09');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3535, 'Hello,
Good to see you here!
We are Daniela and Merel and together we initiated the art project Museu da Crise in 2011. With this project we want to put ''the crisis'' into a museum to make it a concept of the past. We can talk about that forever, but we will save that for another time ;) If you want to know more about us now, visit www.museudacrise.org .
At work at the #commonKiosk in The Hague
Within the Commonfare project we are responsible for the Dutch pilot. We work in Amsterdam, Rotterdam and The Hague en try to involve as many people as we can in these three cities. For instance by organising design workshops to collaboratively shape and improve the design and functionalities of the commonfare.net platform. If you want to collaborate, send an email to merel@commonfare.net.
Apart from the workshops, we are also investigating what we call ''good practices''. People and projects that are already existing and that we think are important for Commonfare. We will report on who and what we come across here.
Let''s talk!
', '2018-12-07 19:25:11');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3549, 'Contact us
Contact us
For press information, requests for interview, images and media resources about COMRADES feel free to get in touch with us.
To contact COMRADES, please fill in the contact form below or send your email at This email address is being protected from spambots. You need JavaScript enabled to view it.
and we will come back to you soon!
All fields required.
I accept the terms of use for this website (*)
terms of use
 
This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 687847. This website reflects only the author''s view and the European Commission is not responsible for any use that may be made of the information it contains.
© Gov2u 2018 . All rights reserved.
Bootstrap is a front-end framework of Twitter, Inc. Code licensed under Apache License v2.0 .
Font Awesome font licensed under SIL OFL 1.1 .
Our website uses cookies to monitor how the site is used and help to provide you with information tailored to your individual preferences. If you continue to browse we will assume your permission to use cookies. To find out more and to learn how to change your settings visit our privacy and cookies policy. Learn more info
', '2018-12-07 19:25:13');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3549, 'Partners
About
COMRADES (Collective Platform for Community Resilience and Social Innovation during Crises) aims to empower communities with intelligent socio-technical solutions to help them reconnect, respond to, and recover from crisis situations.
Project Analysis:
Project Coordinator: Prof Harith Alani , Knowledge Media institute, Open University (UK)
Project Start: 1 January 2016
Duration: 36 months
Topic: ICT-10-2015: Collective Awareness Platforms for Sustainability and Social Innovation
Type of action: Research & Innovation Actions (RIA)
Goals of COMRADES
Community Resilience
Providing intelligent Information and Communication technologies to boost community resilience, by increasing the ability of a community to take collective actions, and to utilise available resources to self-organise and respond to crises.
Community Engagement
Platform will be designed and created bottom-up by communities. Project will run several co-design and engagement events with communities, along with theoretical research.
Informativeness of Citizen Reports
During crises, a very large number of messages are often posted on various social media platforms by using the hashtags dedicated to the crises at hand. COMRADES will produce a set of automatic filters, to efficiently and intelligently identify the messages of sufficient relevance and value.
Content and Source Validity Assessment
Falsified crises and emergency reports are amongst the biggest concerns of humanitarians and communities towards using social media content during crisis. COMRADES will develop methods to help responders with assessing the validity of content and its source and also provide responders with means for alerting the community through the COMRADES platform and social media of unreliable content and information sources currently circulating.
Crisis Event Modelling Detection
COMRADES will develop tools and algorithms for automatically and accurately detecting, modelling, and matchmaking emergency events.
Participatory Community Innovation during crises
The services and platform will be open source using open and linked data, thus facilitating the extension of the platform to other application areas. The COMRADES platform will be accessible over the Web and mobile devices.
What’s in it for me?
The platform will encourage community-wide participation, by enabling local (communities in crisis zones) and remote (digital activists and responders) individuals and communities to come together and share knowledge through their crises reports (community reporters), to produce and access filtered and quality collective information, and to be connected with others based on emergency needs and offers.
COMRADES will involve citizens at two levels:
By engaging multiple communities in the requirements, design, and evaluation tasks and
By producing a platform to be primarily used by the citizens, to help their communities and fellow citizens. The platform will be based on Ushahidi; a common platform for crises mapping, developed in collaboration with iHub.
COMRADES will engage with three communities that are core to effective resilience efforts;
Activists (platform deployers): individuals and groups of people who set up instances of the community platform.
Responders: communities that organise and coordinate resources and provide expertise when the platform is deployed.
Reporters: communities who report on crisis.
Partners
', '2018-12-07 19:25:13');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3578, 'Contact us
Contact us
Please fill in the form below and we will be in touch with you as soon as possible.
<br />
You can find us at:
About us
Innovating Cosmos is a global community of innovators and would-be innovators, innovating collaboratively to make the world better, richer, healthier, fairer for all.
', '2018-12-07 19:25:13');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3636, 'M60 0AS
By phone
Please note that Co-operatives UK is the trade body for co-operative businesses and does not, therefore, own or run any outlets or stores itself. If your query is intended for a specific co-operative organisation please direct your enquiry to them to ensure you get a quick response.
To contact Co-operatives UK use the form below or ring our switchboard on 0161 214 1750. All media enquiries should be directed to Dom Mills  or Leila O''Sullivan . Visit our newsroom for more information.
If your enquiry relates to being a member of Co-operative Group, we think you have gone to the wrong place. Try contacting The Co-op.
Your Name *
', '2018-12-07 19:25:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3636, 'Take your co-op to the next level
Contact us
“Co-operatives UK really helped us to get going with practical advice... My only regret really was not contacting them sooner!” Public Affairs Co-operative
Please use this form to contact us for more information about any of the products or services you have read about. You will hear back from us within two working days.
Name *
', '2018-12-07 19:25:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3637, 'Our Team Join us
OUR TEAM
We are constantly growing, our organisation is made of individuals from around the world who all share a vision of openness and data access for health. We’d love for you to join us on our journey .
OUR TRUSTEES
 
Fiona Nielsen, CEO of DNAdigest
Fiona Nielsen, founder and CEO of DNAdigest and Repositive, is a bioinformatics scientist turned entrepreneur. She used to work at Illumina developing tools for interpretation of next-generation sequencing data and analysing cancer and FFPE samples sequenced on the latest technology. There she realised how difficult it is to find and get access to genomics data for research, which led to DNAdigest being founded as a charity to promote best practices for efficient and ethical data sharing – aligning the interests of patients and researchers. In August 2014 Repositive Ltd was spun out of DNAdigest as an entity to develop and provide a novel software tools and mechanisms for sharing of data.
Dr Mohammad Al-Ubaydli,
CEO of Patients Know Best
Mohammad is founder and CEO of Patients Know Best and has over 15 years of experience in medical software. He trained as a physician at the University of Cambridge; worked as a staff scientist at the National Institutes of Health; and was a management consultant to US hospitals at The Advisory Board Company. He is the author of seven books, including Personal health records: A guide for clinicians and Streamlining Hospital-Patient Communication: Developing High Impact Patient Portals. He is also an honorary senior research associate at UCL medical school for his research on patient-controlled medical records. In 2012 he was elected an Ashoka Fellow as a social entrepreneur for the contributions he has made to patient care. He is the driving force behind the Patients Know Best patient portal software.
Read the interiew with Mohammad here .
Anthony Brookes,
Professor of Bioinformatics & Genomics
Anthony Brookes is a Professor of Bioinformatics & Genomics at the University of Leicester, working on ‘knowledge engineering’ to bridge the divide between research and healthcare. In this context he is Director of the ‘Data-2-Knowledge-4-Practice’ Facility, a joint initiative by the University and the University Hospitals Leicester NHS Trust. He is experienced in disease and population genetics, DNA variation analysis, and data management for biomedical information, and in these areas has published >170 peer reviewed articles and reviews. He served two 3-year terms on the HUGO Council, co-founded the Human Variome Project and the Human Genome Variation Society (now Vice-President), jointly designed the standard Observ-OM data model for gene-disease relationships, runs the world’s largest open GWAS database (GWAS Central), and recently created a new informatics platform (Cafe Variome) for federated discovery of samples, subjects and data. He contributes his wide experience to the Diagnostic Committee in IRDiRC, and to various Task Teams in the GA4GH (Co-Chairing one on ‘Automatable Discovery and Access’). Awarded several biotech patents, launched two bioscience SMEs, and participates (often in a leadership capacity) in a range of EU, IMI, and national consortia projects.
 
', '2018-12-07 19:25:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3658, '11 months ago
The DecarboNet research project has received funding from the European Union''s Seventh Framework Programme for research, technological development and demonstration under grant agreement no 610829.
By using this site you accept the setting of cookies to improve user experience. To learn more, please visit our privacy policy.
OK
', '2018-12-07 19:25:19');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3701, 'Toggle search navigation
About Us
Digital Enlightenment Forum (DigEnlight) is a value-driven non-profit association which aims to stimulate multi-stakeholder discussions and collaborations towards finding and proposing game-changing strategies concerning digitisation in society. Its foundation was based on the publication of the RISEPTIS report .
DigEnlight focuses on innovation and sustainable evolution of a society respecting human values.
Vision
At DigEnlight, we believe that people should enjoy the kind of autonomy and freedom online that they do offline. We believe in the value of human rights, democracy, privacy, transparency and accountability for our society.
By understanding how current and future digital technology can best be used to express our identities in the digital world, taking into account the core values we cherish, we can support the rights of the individual in society.
The spirit of the Enlightenment
The 18th century Enlightenment movement was a commitment to forward-thinking, egalitarian principles that caused a seismic intellectual and societal shift, and subsequently allowed innovation and creativity to flourish.
By recommitting to the principles below, we can help realise the potential that digital technology promises for the 21st Century.
Society flourishes when all its members, regardless of status, collaborate in its design & governance
Intellectual and scientific inquiry should be pursued in service to all
Knowledge should be shared as a public good, not hoarded as a private possession
People are capable of thinking for themselves
How we work
Leadership
By building an interactive community of experts and interested parties across domain, we provide collective thought leadership for the ethical development of the digital society, based on the values of democracy, human rights, freedom, privacy, transparency and accountability.
You can join our community or see an overview of our associates .
Debate
By bringing representatives of science, technology, policy, law and society together for outcome-focused debate, we propose principles, policy recommendations and activities at technical, legal, societal and market levels.
We create the space for debate on this online platform, as well as in-person events.
Action
We initiate and facilitate actions addressing the specific societal difficulties related to generational gaps in technical skills and knowledge.
DigEnlight Privacy Policy
', '2018-12-07 19:25:22');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3702, 'City, State, Zip
Phone Number
Digital Entrepreneurs is a community of online entrepreneurs that aims to run businesses entirely from their computer.
Your Custom Text Here
', '2018-12-07 19:25:22');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3702, 'About Us
About Us
Every story begins with a journey. Two friends explored two different corners of the world. A year in England and a year in South America. But both of us returned with empty pockets and a costly realization. Working for six months, traveling for six months, rinse and repeat, was not a life either of them wanted to live for ! How could this vicious cycle be broken? We had always dreamt of starting our own business but fear of the unknown, made us toe the line. This time, it was different. Mobilizing on the transformative power of the communal, we built an online community of entrepreneurs,freelancers and digital nomads. This community aims to offer advice,training and support to our members as well as simultaneously expand their networking horizons.
We are also dedicated to Blue Beach Web, a web development team who remain committed to fulfilling client needs in the most professional and friendliest way possible. Our vision is to support other entrepreneurs and we’re always exploring other ventures. We’re planning for a store catering to digital nomads, slated to open soon. 
 
', '2018-12-07 19:25:22');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3709, 'For all other help and support enquiries on the Knowledge Hub please contact us on support@khub.net
 
', '2018-12-07 19:25:25');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3709, ' 
About Knowledge Hub
Knowledge Hub boasts a proud heritage within public sector digital collaboration. Originated and developed for local government by local government, the Knowledge Hub and its predecessor Communities of Practice have been enabling public service practitioners to share knowledge online since 2006.
Since January 2016, Knowledge Hub has been an independent small company maintaining the original ethos of a free to use platform for public service professionals to share knowledge, experiences and learning, while also developing a commercial model that seeks to share income generated through private sector advertising with subscribing organisations.
Why choose Knowledge Hub?
Knowledge Hub is the place where you can connect, collaborate and communicate with members just like you across public service. Knowledge Hub provides its digital collaboration tools to a broad audience of over 500 public sector organisations within the UK and internationally. It is used extensively in central and local government, health, education, housing, police and fire services.
The growing Knowledge Hub community consists of over 150,000 public service professionals and their partners who use Knowledge Hub to exchange ideas and solutions and share good practice and innovation. Members need no longer reinvent the wheel, but can gain efficient, effective and reliable advice from their peers in a secure environment.
Knowledge Hub is hosted within private cloud facilities in our UK-based data centre, in the London area. Find out more about security on Knowledge Hub . 
What can Knowledge Hub help you with?
Discover knowledge to help you do your job.
Keep up to date with current thinking.
Enhance your skills and expertise.
Compare and share information and data.
Connect with people like you and experts to network with.
Develop and retain specialist knowledge.
Share what works with others.
Network and collaborate with peers.
Save time and money.
Have global conversations from your desk.
Save hours of research time by learning from others.
Pool resources.
Generate and incubate ideas to drive improvement.
Inspire innovation.
Create new ways of working.
Learn from leaders in your field.
Raise your profile and that of your organisation.
Get in touch with new and existing customers.
Build relationships.
Develop customer insight.
What services does Knowledge Hub provide?
Knowledge Hub’s technology is an enabler for public service organisations everywhere to share information and learn from each other within online groups. Knowledge Hub groups are flexible spaces that can be used for a variety of purposes, from large communities of practice, to small time-limited project groups, working groups, training programmes or recruitment campaigns. A Knowledge Hub group may be open, restricted or private.
Knowledge Hub digital networks also provide organisations with the opportunity to buy their own branded ‘slice’ of Knowledge Hub for use as an externally branded open network of groups (extranet), a private social intranet, or a mixture of the two. This allows you to seamlessly engage with employees, partners, suppliers and citizens all in one place without the need for multiple logins.
Check out our product and services page for full details of groups and networks.
Read the full terms and conditions for using Knowledge Hub.
Who are the team behind Knowledge Hub?
  Jason Fahy - Chief Executive
I am responsible for developing Knowledge Hub as ''the'' place to go to for public service and non-profit organisations.  Encouraging members to help each other through the simple exchange of skills and expertise.
Our vision is to connect the global public sector and to generate income for subscribing organisations by tapping into the digital advertising market.
My journey to Knowledge Hub came via the Royal Borough of Kensington & Chelsea Council, Capita and Liberata. I am also Chief Executive of PFIKS and a member of the Catapult Advisory Network, for Digital Catapult.
Please connect and let me know your views on how Knowledge Hub might support you and our sector.  You can also follow us on twitter @jasontfahy or @knowledgehub and @PFIKS .
  Liz Copeland - Head of Customer Insight & Engagement
My role at Knowledge Hub is varied, from facilitating individual communities of practice and driving member engagement, to managing the wider platform, business development, communications, getting member feedback and working closely with clients to help them build their digital networks.
I’m all about helping you - our members and customers - get what you need, so I’m always interested to hear your feedback about Knowledge Hub.
I’ve worked with Knowledge Hub since it went live in 2012 at the LGA and previous to that worked at the IDeA in workforce development and talent management, where I was also very involved in communities of practice.
Connect with me here on Knowledge Hub , on Twitter @LizCop and on LinkedIn .
  Dimple Rathod - Digital Community & Knowledge Manager
I have been working with Knowledge Hub since its creation in 2012, and prior to this I worked at the IDeA and LGA and was involved in communities of practice and e-government.
I enjoy working with our members and group facilitators to help them get the most of out of Knowledge Hub and use it in a way that best supports them and their needs. This includes providing guidance on group facilitation, responding to helpdesk queries, connecting members, promoting Knowledge Hub and group activity, and gathering user stories.
Connect with me: Knowledge Hub / @dimplerathod / LinkedIn
  Michael Norton - Digital Community & Knowledge Manager
Michael joined Knowledge Hub before Knowledge Hub was even thought about and has been supporting people in building online communities ever since.
Michael is an experienced knowledge professional who has been involved with Communities of Practice for many years and is well regarded in this field.
His particular focus is on community’ management incorporating Knowledge Management, Web 2.0 technologies and Social Media and he is heavily involved in supporting professional networking across local government and the public sector.
Connect with Michael here on Knowledge Hub , Twitter or Linkedin .
You can ask Michael about:
Nearly anything to do with sport
Star Wars and Jason Bourne
Common challenges community facilitators face and the best practices they use to conquer them.
', '2018-12-07 19:25:25');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3709, 'About Knowledge Hub
Knowledge Hub - where you go to work together
Knowledge Hub is the place where members exchange knowledge to improve public services and produce social value. As the UK''s largest platform for public service collaboration Knowledge Hub helps members and communities to freely connect, share knowledge, develop initiatives and share expertise in a secure environment.
Why choose Knowledge Hub?
Discover knowledge to help you do your job.
Keep up to date with current thinking.
Enhance your skills and expertise.
Compare and share information and data.
Connect with people like you and experts to network with.
Develop and retain specialist knowledge.
Share what works with others.
Network and collaborate with peers.
Save time and money.
Have global conversations from your desk.
Save hours of research time by learning from others.
Pool resources.
Generate and incubate ideas to drive improvement.
Inspire innovation.
Create new ways of working.
Learn from leaders in your field.
Raise your profile and that of your organisation.
Get in touch with new and existing customers.
Build relationships.
Develop customer insight.
In addition, Knowledge Hub prides itself on providing high quality support to its members. There is an experienced and established team of community managers behind the scenes who are always happy to help.
', '2018-12-07 19:25:25');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3709, 'User guide
The Public Service Transformation Academy is a not-for-profit social enterprise, with RedQuadrant as the lead partner and delivery partners including the Whitehall and Industry Group (lead delivery partner), OPM, NCVO, Browne Jacobson LLP, the E3M Bold Commissioners Club, TSIP, the Alliance for Useful Evidence, Local Gov Digital, Collaborate, Numbers for Good, and members of the Public Service Transformation Network.
The founders of this academy share some core beliefs: That most of the problems the public sector is dealing with have a significant underlying level of complexity. This complexity differs – it can arise from having to deal with multiple stakeholders, from complex citizen needs, from a need for cultural or behavioural change, or from a complex system of organisations that need to work together. Often, all these sources contribute to the complexity at the same time.
Governance
The PSTA operates with three main boards. The main board consists of RedQuadrant, WiG, two representatives of the members, a Cabinet Office representative and a Public Service Transformation Network representative, with one additional member representing the wider sector.
The Advisory board provides advice to the entity board along with subject matter knowledge.
The Design Authority provides quality assurance to all our work and has oversight of our accreditation for courses, trainers and certification.
In addition to this, we will have a cross-sector Development group for representatives of all those across central and local government with a shared interest in the development of commissioning.
', '2018-12-07 19:25:25');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3709, ' 
Collaboration is key to self-improvement
SSAT has spent 30 years building up the biggest and most active network of schools in England.  Now, this important community needs to be driving the self-improving system we all talk about - it’s time to mobilise the network!  And so, we are introducing new ways of connecting member schools and generating deeper, sharper, more accessible insights to stimulate collaboration. 
One of those new ways is to provide collaboration space here on the Knowledge Hub platform and offer teachers, headteachers and governors opportunities to work and learn together, thereby shaping solutions for the many challenges faced by schools in today’s shifting educational landscape.
Our KHub Network is only accessible to teachers in SSAT’s member schools and non-member teachers participating in specific professional development programmes.  Inside the network, groups are either ‘open access’, allowing members to engage freely in collaborative activity or ‘restricted’, because they support paid-for activities and specific collaboration.
Any questions that you have about our Network can be emailed to glyn.barritt@ssatuk.co.uk
', '2018-12-07 19:25:25');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3709, ' 
Purpose
The community is a network of like-minded people with an interest in project and programme delivery who want to learn and share experiences and knowledge. Our mission is to help build capability and grow talent so that we can transform project and programme delivery in government. With the help of this community, in 5 years we will have developed a project delivery profession that is brilliant at delivery and recognised for that in its own right.
Who is it for?
We aim to include everyone who contributes to the delivery of government projects, whether you work in a government department, an Arms Length Body or a project professional body. We welcome those of you who work in the private sector if you would like to participate in learning and sharing with others – we have one rule – no selling!
What’s in it for you?
Our research has told us that your major need is for a safe space where you can:
Build relationships that transcend organisational boundaries;
Collaborate with others to share ideas and solve problems;
Find people who know the answers to your questions because they have done it before; and
Share your experiences for others to learn from.
In short it will help you find - the Right Person with the Right Answer at the Right Time.
There are more tangible benefits too that will help both you and your organisation – some examples:
Saving time and money by finding information and answers sooner;
Keeping up to date with the most current thinking;
Progressing new ideas with a wider audience;
Sharing good practice and avoiding duplication; and
Rapid Induction - supporting new Project staff in their induction and development.
So what do you have to do now?
Register yourself in the groups that interest you and start building your profile – that’s all you need to do to get started. Later on you will be able to set up your own groups within the Government Project Delivery Community based on your own shared interests.
', '2018-12-07 19:25:25');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3735, 'Kim About Us Edits
DonorSearch: Innovation Since Day One
DonorSearch was founded in 2007 with one goal: to provide more accurate, more comprehensive, more actionable data to help nonprofits of all types achieve better fundraising and outreach results.
Headed by industry veterans, DonorSearch is a leading provider of prospect research to nonprofits of all types, including charities, healthcare organizations, fraternities/sororities, religious organizations and educators.
Using information from 25 databases, DonorSearch uses proprietary algorithms to help clients find the best philanthropic prospects. DonorSearch’s data can be easily integrated with most common donor management and general sales software, putting critical donor information at a client’s fingertips.
DonorSearch is the only prospect research company that:
Searches and analyzes wealth and philanthropy databases
Reviews key information manually for greater accuracy before you receive it
Can predict both capacity and propensity to give with confidence
Provides in-depth information to help development personnel formulate approach and ask strategies
Helps development offices allocate scarce resources most efficiently
About the CEO
Bill Tedesco is a well-known entrepreneur in the field of philanthropy with over 15 years of experience at the helm of companies serving the fundraising profession. He has personally conducted original research to identify markers of philanthropy and has developed modelling and analytical products that use those markers to accurately predict future giving.
Since 2007, he’s been the founder, CEO and Managing Partner of DonorSearch. DonorSearch is one of a small group of companies providing wealth screening, philanthropic reviews, and online prospect research tools exclusively to the nonprofit market.
Prior to DonorSearch, he was CEO of WealthEngine and Executive Vice President of Target America, which was later purchased by Blackbaud and folded into Blackbaud’s Target Analytics division. Mr. Tedesco was also a principal of BFTConnect/ContactReporter.
He also has four years of fundraising experience as Director of Development for the Fund for Educational Excellence, Baltimore’s public education fund, and as a major gift officer at the University Of Maryland School Of Medicine. He earned B.S. and M.P.A. degrees from Pennsylvania State University and is a frequent speaker at conferences around the United States.
meet the
', '2018-12-07 19:25:25');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3753, 'Please Contact us to our email: Contact@e-bookreadercomparison.com
Facebook
LinkedIn
About US
Welcome to e-bookreadercomparison.com, the home to all of your ebook reader information and product needs. We are here to help you decide which e-book reader is the best choice for you. Whether it is looking for an e-book reader with a lot of different features and the latest technology, to finding the best e-book reader for reading in direct sunlight and traveling. Discover the latest reviews and data on the top e-book reader brands on the market. We have it all here at e-bookreadercomparison.com
Support US
Making a website like e-bookreadercomparison is expensive, of course, if it wasn''t for our Followers, it might be impossible to keep my blog alive. For less  than a cost of coffe toast, you can help maintain the future of e-bookreadercomparison.My pledge For You, -all earnings you contribute will be utilized exclusively to build more contents. This consists of paying our regular server costs, bandwidth and other bills required to keep carefully the site running..
Recent Posts
', '2018-12-07 19:25:26');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3765, '88269 students are using the Uni Compare app
Contact University Compare
University Compare’s students are attracted to us from a social, text, email, school referrals, and organic searches. Our social reach sits at just shy of 1.3m users across social channels: Instagram, Twitter and Facebook.
We specialise in helping students engage with universities and more importantly, positioning the prospective student, so they are ready to apply.
University Compare is not your typical comparison website; we’re a tech-savvy socially addicted bunch that understand how students think.
We ARE the new generation of student media.
Physical Address:
Unit 8B Kingsdale Business Centre
Chelmsford
', '2018-12-07 19:25:28');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3765, 'Student Finance: Everything that You Need to Know
Student Finance: Everything that You Need to Know
University Advice
Student Finance: Everything that You Need to Know
Student Finance is one of the most difficult things for students new and even the old to get their heads around. There is a fair amount that you need to be aware of before you go to university and finance is a big part of that.
There are many different sides to student finhs burnance. Whether it be the student loan repayments after university, or it’s the overall budgeting of your money every month, there are always things to consider when exploring student finance. Student finance in England or  student finance in Wales  or anywhere else can be different depending on the governing laws, however, they can also change depending on the rules and laws laid out by the universities that you’re attending.
What Do Tuition Fees Cover?
Tuition Fees cover all the charges made by the University that you’re attending with everything from registration, tuition, examinations, Student Union services and all other university services if you ever find yourself asking  what is a students union , then be sure to visit our page to find out more and see what your money is going on, who knows, it may even help you to  settle into student life !
Unfortunately, these fees won’t contribute towards your housing or anything else, like that. These are some of the other things that tuition fees cover at university:
Access to the university library
Access to the university’s computer facilities
Student’s Union services
Support services offered by the university
Administration fees
Lectures
Examinations
Your tuition fees also cover other big expenses that the university is likely to be encountering, too, such as the general university upkeep, new facilities or new equipment for the university.
Do I have pay the university the tuition fees?
No. The tuition loan is paid from the student finance company directly to the university.
How Much Will I Be Paying at University?
Until around 1998, tuition fees were virtually non-existent in the United Kingdom, save for a couple of universities that charged their students, however, the legislation changed under prime minister John Major, who on the recommendation of  The Dearing Report , introduced the idea of tuition fees.
However, the  university fee caps  that the government introduced, have risen on an election basis for a number of years. Fees initially started at £1,000 and have risen to £3,000, £5,000 and £9,000 respectively. At the moment, £9,000 is the most that a university can legally charge students.
The rate of tuition fees can be different depending on which country in the United Kingdom, you happen to be studying in. In England and Wales, fees are set at a maximum of £9,000 for everyone. People born in that country, pay the same amount as those coming from Scotland or Northern Ireland.   Studying in Scotland  is free-of-charge, for those that are born in Scotland and other eligible EU Nations (However, given Britain’s exit from the European Union (Brexit), this can change), for those from elsewhere in the United Kingdom, the price is £9,000. The price in Northern Ireland is £9,000, but for those who are from Northern Ireland, the price is significantly less at £3,925.
Am I Able to Get a Cheaper Student Loan?
Unfortunately, you won’t be able to shop around. Applying for student finance or taking a student loan out, will not be the same as  comparing car insurance  (Maybe you have a  car to take to university , too!) or changing your phone provider (Not that that’s the easiest thing in the world to do, either). The prices that are set, are set by the government and are therefore unlikely to budge.
Costs can fluctuate depending whereabouts in the UK that you happen to be studying, but the change in money is unlikely to set the world on fire.
If you’re living in  London , then it will not doubt be more expensive than studying in  Londonderry  or anywhere similar, as the prices can often be dictated by the cost of living in the place that you’re living in, and also by your living arrangements. While it can be annoying to see, the prices will essentially stay the same, give or take a few places, obviously, accommodation can be even more difficult, if you’re  looking for accommodation after Clearing , too. 
However, the fees noticeably change when studying in other countries. For those living in England, the situation is a little more advantageous, as there is more of an incentive to look around. Fees in England are at a record high of £9,000 and are unlikely to go down, however, studying in Scotland will be cheaper. As we said above, Scotland will still charge English students the same basic fee that they are accustomed to in England, but you may save a little bit of money all the same as  Student Finance in Scotland , works very differently. Paying £7,500 would be a lot better for you than spending £9,000, even if it may only seem like a £1,500 differential, you are essentially saving nearly 17%, which could be all the difference between a comfortable and affordable education and a more expensive one!
Staying local to your home or even  living at home during university  will save you money in the long-run, but whichever way you look at it, the money involved with getting a degree, will be very expensive, regardless of how much you save. Going to university is an incredibly rewarding experience, but an expensive one too.
What is a Maintenance Loan?
A Maintenance Loan pays for your living costs. When receiving a  Maintenance Loan  you have to give the details of your household income and any other payments or direct debits or line of credits or any debts that you may have. The loan is then paid directly into your bank account at the start of every university term. On the off-chance that you don’t already have a bank account, check out the  best student bank accounts  for you.
Students that are in their first full-time higher education undergraduate course can receive a Maintenance Loan, however, the caps on a Maintenance Loan is also dependent on your current university situation as well. If you’re living at home, the cap will be significantly lower, than if you are living away from home. The loan will very much depend on your current financial status and on your family’s ability to help you, too. You can apply for a Maintenance Loan regardless of your income, but the amount that you will be able or eligible to borrow depends on your current income.
To claim a Maintenance Loan, you must have lived in the United Kingdom for at least three years before starting to study.
Maintenance Loans are different for students that are studying an NHS-funded course at degree level, in this instance, students can apply for a set rate of their maintenance loan to come in, coupled with their income-assessed  NHS Bursary , which often comes as standard.
This can be different if you apply for a   Maintenance Grant , which is aimed at students with a lower-income than other students, and whose circumstances require help, the amount of Maintenance Loan you receive will be slightly reduced, this too, will be dependent on the overall circumstances that you find yourself in. Students that receive a Maintenance Grant do not have to pay it back.
Maintenance grants and loans will not necessarily be enough for students to be able to survive on, so students will sometimes need to find a way to supplement their income, this can be by applying for a student job or by cutting their spending habits significantly, maybe by looking up some  student budgeting tips .  How to manage working and studying  is essential, not only for financial benefits but also for experience and to learn new skills.
How Much Money Can I Apply for From My Maintenance Loan?
This can depend on your circumstances. The amount that you can actually borrow will, of course, be different depending on what your current university circumstances are. We have some rough estimations for you below:
Living at Home: You can be entitled to around £6,000
Living Away from Home: Roughly £8,000
Living Away from Home (In London): Has been known to be around the £10,000 mark, depending on whereabouts in London you are.
Living Away for Home (Abroad): Dependent on the country you’re staying in.
Of course, these rates, like any other rate, are open to interpretation and can fluctuate accordingly, depending on your own personal circumstances, such as your personal income status as a family.
How Much Maintenance Loan Am I Entitled To?
A Maintenance Loan, like so many loans, will always be dependent on the amount of money that your personal circumstances dictate and the place that you’re actually studying, to sue the  Student finance calculator  to see how much you should be entitled too. However, if you look below, you can see a rough estimate of the amount that you’re likely to be entitled to:
Household Income of £25,000 or less, you will be entitled you to around £8,000
A Household Income of £30,000, you will be entitled to roughly £7,000
Household Income of £40,000 will entitle to around £6,000
A Household Income of £45,000 will entitle you to around £5,000
A Household Income of £55,000 can entitle you to around £4,000
Household Income of £62,000 or more will entitle you to roughly £3,000
These rates are of course interchangeable and can deviate depending on different circumstances.
Am I Entitled to Maintenance Grant?
This follows much of the same for above. The allowance afforded to students will very much rely on the present circumstances to those that are applying and of course, not everyone is eligible for a Maintenance Grant, as these are generally aimed at students where the household income isn’t enough to support the student while at university.
Those who are in a family whose income exceeds that of £45,000 will not be entitled to a Maintenance Grant at all, and those who are earning just south of £40,000 will only be entitled to £50. The rest is dependent on the pay that is coming into the family household, however, the most that a Maintenance Grant will cover is just over £3,000 and very rarely more than that. So your Maintenance Grant will cover between £50 – £3,000.
Can Bursaries or Scholarships Help Me with My Student Loan?
Bursaries, Grants and Scholarships  are an important part of university, as many of them are used as a way of being able to get academically exceptional pupils or those that are in a financially difficult position into university.
Bursaries and Scholarships are used in exceptional circumstances and are done for the two aforementioned reasons. If a student offers remarkable academic potential then a scholarship may be offered. These scholarships can range for covering your first year’s tuition fees, to covering your entire time at university, however, they are likely to have conditions that you will have to stick with; whether that be staying between a certain grade percentage or achieving a certain grade at the end of your first year or at the end of the overall university degree. And yes,  tuition fees really do matter !
A Bursary will allow students that come from a lower-income household, the opportunity to go to university. The requirements for a bursary can be different, depending on your household income, your living situation for the following year, the university that you’ll be studying at and the bursary requirements themselves. The bursary works as a lump fund for the university to help your survive when you’re there. This can be split across the terms that you’re studying in or across the year and it’s not obligatory for the students to pay the bursary back.
What is a DSA?
A DSA is the Disabled Student’s Allowance, not only are the loans different but there is a slight difference with  disability-based UCAS applications . The DSA provides extra funding to students that are  attending university with a learning difficulty . These include everything from physical disabilities, all the way through to general  mental health complaints at university , essentially, anything that prevents students from being able to work effectively at university.
As you can imagine, the burden of proof is on the student themselves; you will need to provide documentation to back up your claim for DSA and there is also a cash sum to pay for any specialist equipment, a helper, and any other help that you may need. The allowance that you are given by the university, is not included in any Student Finance that you receive, as it is a separate payment, another good thing too, is that it is not affected by any sort of household income and what’s even better, is that it doesn’t have to be paid back!
However, a word of warning, accepting any other disability related grants or any other financial incentives aimed towards helping with any disability support can affect your eligibility for DSA, so make sure that you check with your university, first.
What is a Hardship Fund and How is it Different from a Bursary?
Universities have a legal requirement to hold some money at the university that can help students that are struggling financially, this is a  Hardship Fund . This is different from a bursary, however, as this is reserved specifically for particular circumstances, like students that are coming from low-income backgrounds or if the student has any disabilities or if they have any dependants. Bursaries are there to help people that are struggling financially, however, a hardship fund is specifically made for those that as well as struggling for money, also have circumstances beyond their help, like the aforementioned disabilities or dependents.
If you need to apply for a hardship loan before or during university, then, talk to the university’s welfare team or to the university’s onsite student money advisor about any emergency grants or loans.
What are the Biggest Expenses at University?
This is a difficult one for students. The cost of the course and the  cost of a university degree  will, of course, be a tough pill to swallow for students, but that isn’t even the biggest expense. When factoring in all of the different expenses seen at university, the biggest expense for students is most likely going to be the cost of living.
Now, the cost of living will depend very much on your circumstances, are you  living at home during university , are you living at university or in accommodation –  choosing the right accommodation  is essential or are you working at the moment, are you studying/living in a big city etc… are all things to consider when at university. These are the factors that can change whether or not you have a large number of expenses, especially if you’re studying at a  top city university , a  top coastal university  or a  university with the best nightlife . 
When at university, it is incredibly important to keep an eye on your expenses, the things that you’re spending money on and the money that you have coming in. As we’ve said above, the best thing for a student to do is to supplement their income with a job of some sorts. Whether that be a part-time job in an office, doing the photocopies and living out the life of Peter Gibbons in Office Space, or just working behind the counter in your local Tesco, it makes no difference, you need to be able to bring some more money to keep your costs down and your incomings up.  Try to cut back on things that you don’t need, save your money where you need it and try and keep yourself away from things that are just going to bleed you dry.
Nights out are often a big expense at university and  the people you meet on night’s out  can be different, too, but that’s for another time. We recommend that you try and take part as much as you can, don’t try and avoid going out or being cautious about going out, just for the sake of money. Be smart about going out, but don’t just decide not to go out for no good reason. Remember, that university can be some of the best days of your life, don’t forget that you are allowed to have a life outside of work!
When Do I Start Repaying My Student Loan?
You won’t pay anything of your student loan back until you’re in a position to do so. Repaying student loans works very differently from any other kinds of direct debits that you’re likely to take on in your life.
Your  student loan repayments  work very similar to your  National Insurance or tax contributions . You don’t ever physically pay the money back, the money is automatically deducted for your paycheque. For instance, if you earn £23,000-a-year, that works out at roughly £1,900-a-year. When you receive your payslip for your employers, you will see your tax deduction, your National Insurance contribution,  Emergency Tax  (If you’ve been paying it) and your  university loan repayments .
However, you will not be paying your university fees back until you’re earning around £21,000 or more. When you are earning £21,000 or more, you will pay back at around a rate of 9%. The average university loan is around £48,000, which will take a long time to pay off, however, the payments will be so minimal you will hardly notice, and any loans that are no paid back within a thirty-year period, are automatically written-off. Most loans are paid back between a twenty year to twenty-five year period, which is roughly the same amount of time as the average mortgage.
What Percentage of My Salary Goes Towards My University Repayments?
This is an easy one to understand. Roughly 9% is contributed for your wages each month and is (as stated above) taken out of your wages, in much the same way that a mortgage repayment is made or your national insurance.
The repayments themselves can work out differently, depending on whereabouts in the UK that you happen to be studying. Repayments in Scotland are affected in a different way as the percentage changes, and this does not stay at a fixed rate like the rest of the UK. The percentage of repayment in Scotland can also depend on the amount that you earn.
In order to repay your loan in the rest of the UK, however, you need to be earning over a certain amount of money, which is roughly £18,000, although this figure has been known to change.
Will Missing a Student Loan Repayment Affect My Credit Rating?
Missing any repayments to your university does not affect your credit score at all. It is almost impossible to miss a student loan repayment, as they are taken for your paycheque before you even get the chance to spend it! As it works this way, this means that it is impossible to miss a repayment.
For any students that started higher education between 1990 and 1997, their student loan will impact on their credit score if they ever miss a payment, because the repayment system, is different to how the system is run today. The Student Loans Company will always write to late payers, which gives them 28 days to make contact with The Student Loan Company before the payment will actually show up on any credit records. However, if you started higher education between 1998 and the present day, your loan does not show up on credit reference agency’s databases
That being said, a Student Loan can have a small effect on any future mortgage applications. It’s nothing to panic about, it just means that your take-home each month, will be less than someone who is not paying a student loan, so mortgage lenders like to know what the amount of money coming into the house is.
When Do My Student Loan Repayments Stop?
This is dependent on whereabouts you studied. Not as in, which university, which country. In Northern Ireland, it is twenty-five years and in Scotland, it is thirty-five years. You also have to have been earning a certain amount of money in order to pay it back, and if you are unable to, then your debt is wiped from existence!
The more money that you earn, the more you will pay back and the less money you earn, the less you will payback. These are all dependent on the amount you earn and your present circumstances.
Does a Student Loan Entitle You to Tax Breaks?
In the words of Peep Show’s Mark Corrigan, “Chance would be a fine thing”. Unfortunately, your student loan doesn’t mean that you’re exempt for anything like  tax breaks . Many students are unaware of this, they believe that having an added cost on their salary will mean that they either get a lower tax code or will have their tax written off completely. Boy, are they in for a shock! You’ll pay income tax on your salary before your loan repayment comes even comes into play, which can annoy a lot of people!
For example, if your annual salary is said £50,000 any income tax that you owe, will be collected as usual. When your loan repayment is calculated and taken, it’s still worked out from your initial salary figure, regardless of your other payments. It’s unfortunate, we know, but the tax breaks are non-existent for students, no virtually, literally. If they can get Al Capone, they can get you too, so don’t try and get out of paying income tax. Make sure you know enough about  student tax  before you move on! 
Will I Pay Interest on My Student Loan?
Is the Pope Catholic? Get used to paying interest on a lot of loans in your life, and your student loan will be not different! In England and Wales, you get an added little goodie bonus, too! Your interest will be charged on top of the rate of inflation. Which is always…err…lovely. That means you’ll most likely be charged between whatever the going rate of inflation happens to be at that point, in time, plus whatever the actual percentage is on the loan.
It works differently when you’re at university, to when you leave university, as you can imagine:
Whilst at University: You will pay the rate of inflation (RPI) plus another whatever the percentage is on your loan. This will be the same until the April in the year that succeeds your graduation.
After You’ve Graduated: If you earn under the £21,000 threshold (See more above), your interest rate will be set at inflation (RPI), as with the old system, that you’re used to paying when you were at university. For every additional £1,000 that you earn, you will be charged an extra percentage per annum on your loan, which of course depends on your earning capabilities as you can imagine. Once you earn above the high-earning threshold, which is likely to be in the £41,000 region, you will likely incur an additional annual interest rate.
People Keep Talking About The Student Loan Company. What is The Student Loan Company?
The  Student Loans Company  (Often abbreviated to SLC) is an organisation established to provide financial services to those that are repaying their student loan. The company is responsible for a lot of the logistics involved with student loans and will also handle the majority of bursaries and the like.
The company is Government-run, therefore, it is not a separate entity to the rest of the country. Therefore, they will most likely have your best interests at heart and will be able to help you, should you ever run into any sort of financial issues.
If I Move Abroad, Will My Student Loan Stop?
Not so fast, slick! A common myth that is perpetuated by universities or by former students, is that if you move abroad for a certain amount of time, you will not have any debt to pay. That, unfortunately, is wrong. Very very wrong. You have to pay your student loan back regardless, and moving abroad won’t save you, unfortunately. Studying abroad won’t save you either. If you’re studying in America, the  student finance in America  is very different there, too, so be careful! The  cost of studying abroad  will obviously be drastically different, depending on the currency, too.
Sorry to burst the potential fee dodging bubble!
Can I Pay My Student Loan Back Early?
In the immortal words of Saul Goodman, “It’s never a bad thing to be early, except in death and taxes”, so of course, you can! So  should you repay your student loan early?  Whether or no you should is another question, however. Clearing your student debt, or choosing to make a larger contribution to getting your loan cleared will open up a couple of questions for you.
You’ll need to think about how much money you need to have as disposable income, it’s essential that you  understand student debt . While the idea of paying back a student loan early may seem very appealing, you have to remember that it is money that you will now no longer be able to claim back, so if you’re out of money at the end of the money, you’ll just have to, unfortunately, live with it. It requires a fair amount of forethought, if you still want to go ahead with it, then take a look at your account and see if your cash can take the hit.
', '2018-12-07 19:25:28');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3765, 'Student Tax: Everything You Need to Know
Student Tax: Everything You Need to Know
University Advice
Student Tax: Everything You Need to Know
Tax is an unfortunate construct of our society, and certainly the man who invented tax is enjoying his very own suite in the deepest circles of hell, however, that doesn’t stop it being one of the most important things in our society today.
The ramifications involved with the breaking of tax laws can be incredibly serious; many people find themselves running afoul of tax, in fact, Chicago mobster Al Capone was arrested and eventually imprisoned for his tax situation . In the immortal words of Saul Goodman, “If they can get Capone, they can get you”.
But tax is a complicated thing, and something that if you don’t understand, you need to speak to a professional, be it a member of your bank, or to an accountant, ignorance of the law will not wash, unfortunately. Luckily, we’ve got some facts and figures surrounding student tax for you to help you out, these can help you out even if you’re  studying an Accounting degree .
When Do I Start Paying Tax?
You’ll start paying taxes as soon as soon as you are earning over the personal allowance figure that is representative in the UK at that point. Everyone in the UK will have to pay tax at some point in their lives. The rates can change from year-to-year, with the rate depending on various socio-economic factors affecting the country at that time; however, at the moment, the minimum earning wage to pay tax in the UK is £11,500 – after that you will enter a higher tax bracket, if you earn over a certain amount, which at the moment is set at £33,500. If you earn over the £11,500 bracket, you will have to pay a basic tax rate on everything you earn over that amount.
While working a part-time job while studying at university or if you decide on getting a job whilst doing your GCSEs , you will be in a different tax bracket, so you will not be taxed. Although, if you work full-time over a period of time, perhaps over the summer when summer university term is in full flow, you may be taxed as you will be working full-time hours, likely to be earning a taxable income. Not to worry, you’ll be able to claim this tax back, which is explained below. It’s important how to manage working and studying; both are very important to you, so it is essential you find the right balance.
What is the Basic Tax Rate in the UK?
The basic tax rate in the UK  at the moment stands at 20%. Which means if you’re earning over £11,500 but under £33,500, you will pay 20%. For example: if you earn £22,700-a-year, you will pay £4,540 overall in tax that year, which will work out at £378.33-a-month. This will, of course, change depending on the wage bracket that you qualify for.
Is All Income in the UK Taxed?
No. Despite the horrible laws surrounding the boring world of tax, not everything is taxable. For instance, your wages, any state benefits you receive, some bank accounts and any Job Seeker’s Allowance that you happen to be claiming, will be taxed; however, your student finance package, any bursaries, grants, scholarships , Child Tax Credits or Disability Living Allowance or any ISA Allowances (See more below) that you have are non-taxable. A useful tip for you is that when applying for any means-tested funds, like student finance , you only need to declare taxable income!
Can I Reclaim Overpaid Tax or National Insurance Contributions?
Yes, you can, you lucky little so-and-so. To do this, you will need to keep an eye on your payslips, though. If you’ve noticed that you’ve been overcharged, then you will need to get in touch with Her Majesty’s Revenue and Customs and let them know or ask them to investigate. If you are entitled to money back, they will reimburse you as soon as possible.
What is a Tax Rebate?
While the idea of having children is certainly up there, a tax rebate is, of course, the greatest a person can experience. Okay, that might be a bit of an overstatement, but it is certainly a welcome surprise for everyone! A Tax Rebate is where the government reimburses your tax that you’ve overpaid, in the form of a cheque. You will often see that you will get rebates once or twice every two years or so if you’re changing jobs, but overall, you will see a tax rebate every so often, depending on your job and your contributions. It’s best to (As we said above) keep an eye on your payslips and see if you are due one, but the government will let you know if you aren’t.
Do I Still Have to Pay Tax If I am Self-Employed?
You will still have to pay tax, even if you’re self-employed. You can be entitled to pay less tax if you’re self-employed, but this is all dependent on a couple of factors for the business itself. Only profits can actually count towards your overall personal allowance. Profits, are obviously worked out by the money that the business (Or you) have earned and then take away the overall running costs of the business. Don’t try and be sneaky though, any money that you personally invest into your company is still very much your money. If you think that over-investing will help to save on tax, you’re wildly mistaken.
Why Does an ISA Help Me with My Tax?
Well, an ISA stands for Individual Savings Account. What you need to be aware of is that banks automatically tax interest on any and all bank accounts that you have, before any money even enters the account. The best thing to do is to open an ISA, which can allow up to £20,000 to go into the account and remain tax-free, year-after-year. It’s a perfect account for students, especially as it now means that you don’t have to pay any tax and it means that you can keep topping up your ISA all the time!  
If I Earn Money from Overseas, Do I Still Have to Pay Tax?
Pump your breaks there, slick! You will indeed have to pay tax on any money earned abroad. What’s more is that you won’t just pay tax in the UK, you’re also quite likely to pay tax in other countries, too! That’s always nice, isn’t it? You might want to do some research into any tax treaties first as well, just to make sure that you’re not paying tax twice.
Is Cash Tax-Free?
Not in this world, if you believe in the multiverse theory, there may be a universe where that’s true, but not this one. Employers cannot pay you in cash. Many employers will have employees sign contracts to say that they are self-employed, this removes the burden of sorting out the tax and national insurance contributions for the employees and leaves it up to them themselves, it will also most likely mean that you are paid in cash, if that is the case, you need to seek tax advice immediately, you don’t want to be caught out! A good idea is to keep a note of all of your cash incomings and outgoings and be prepared for the tax man to do some invasive digging.
So there you have it! The boring, tops-survey and frustratingly complicated world of tax. Remember to keep an eye on things when you’re at university, don’t miss out on payslips, always read them and don’t allow yourself to be caught out on your tax, you don’t want to be the next Al Capone, or be ridiculed like Jimmy Carr!
', '2018-12-07 19:25:28');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3765, 'Erasmus Programme: Everything You Need to Know
Erasmus Programme: Everything You Need to Know
University Advice
Erasmus Programme: Everything You Need to Know
The Erasmus Programme is a student exchange programme that works across Europe. Erasmus stands for European Region Action Scheme for the Mobility of University Students. The Erasmus Programme is one of the most popular exchange programmes in Europe today, the programme has been running since 1987 and has a number of parallel schemes that they run too.
How Long Can I Go Away For?
Unfortunately, there is a limit to the time scale that you can go away for. If you go to a university in the European Union, you have a minimum stay of three months and a maximum stay of twelve months. No matter how long you go away for, it is crucial that you have European health insurance !
When Can I Start Using the Erasmus Programme?
Slow it down there, slick! You can’t use Erasmus until you’re in the second year of university or later than that, depending on what course you happen to be studying.
Your course may have a year studying abroad at another university as mandatory. A lot of language courses will require you to go abroad, however, often going abroad will be an option rather than strictly mandatory, but this depends on the course that you’re doing and the university that you happen to be studying at.
How Can I Apply for the Programme?
You should speak to your university if you’re thinking of studying abroad .  Your university will most likely have an Erasmus office that will handle issues such as these. You can also speak to any lecturers that you have and they will be able to give some personal advice and the Student Advisor will also bring many benefits. Where do I want to study in Europe , I hear you scream! Be sure to research the different countries where you are able to study, all will have benefits and drawbacks, only you can evaluate and decide what is best for you.
Where Can I Go?
You’ve got a pretty long list if we’re being totally honest. Nearly one thousand institutes partner with the scheme and allow foreign students to visit the premises, although the list is likely to be affected by Britain’s recent exit from the European Union (Brexit, if you will). It is important to have a study abroad back up plan , so once you have researched the different possibilities, pick at least 3 options, don’t rely on just one institution.
Will I Have to Pay Any Additional Fees for Studying Abroad?
No you won’t. When studying abroad, you can actually do yourself a big favour in terms of your own student loan . If you study abroad for the full academic year, then you will only be required a maximum of fifteen percent of your tuition fees for that year. However, if you studied for less than twenty-four weeks, then you will have to pay the full amount.
Is There a Deadline for Applying for the Programme?
There is a deadline, yes. This can vary from university to university as it will all very much depend on what your university’s stipulations are; although the most common deadline usually happens to be around the winter term.
Can I Get Any Grants or Loans Towards My Stay Abroad?
Your university will often have a grant or a fund for this sort of thing, which very much depends on whereabouts you are going and the cost-of-living in that area too. The going rate for most universities is around €250-€300, which goes towards student accommodation and your living costs, although this can change depending on the university that you happen to be studying at. You can also get an additional €100 towards your stay if you have any special needs that need accommodating.
How Many Universities or Institutes Take Part in Erasmus?
At the time of this article going live, the programme has over a thousand institutions available for people to look into.
So there you have it! A small article, but an informative article. The Erasmus programme is a fairly straightforward programme to understand, but most of the information you will need to know will be handled by your university as they will most likely have separate information or separate rules that they adhere. From this, we recommend that you speak to your university if you have any other issues.
', '2018-12-07 19:25:28');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3765, 'Mature Students: Everything You Need to Know
Mature Students: Everything You Need to Know
University Advice
Mature Students: Everything You Need to Know
There are plenty of people who subscribe to the theory of “University is a young person’s game”, how wrong these people are. University is designed with the sole intention of giving anyone who wants to go to university, the opportunity to learn and to move forward with their lives in the direction that they want.
Plenty of mature students who go to university are often there for two reasons. Maybe they’re looking to learn more about a certain subject or are pursuing a change in career direction.
Another reason is that maybe the company that they work for has changed their job description that means that they have to have a degree. There are many different reasons as to why, but they will all be unique to each applicant. We’ve got some common questions for people that are applying to university as a mature student.
What is a Mature Student?
A mature student is someone who studies at university beyond the average starting point. Most university students start at around eighteen; however, a mature student is someone who starts university after the age of twenty-one. After the age of twenty-one, you are considered to be a mature student.
How do I apply to become a Mature Student?
You will need to apply through the UCAS website, this will allow you to pick the universities that you want to apply to and will allow you to sort out your options, your personal statement (use our Personal Statement examples and Personal Statement editor to help complete it to a university accepted standard.) and will also allow you to start assessing your options. Your UCAS application will be the same as anyone else applying, not matter how young or old that person is.
Can Mature Students still use Clearing or Adjustment?
Yes. What’s Clearing I hear you ask?… Clearing is used to help students to get into a university as a result of grades that are lower than they thought they would get and UCAS Adjustment is obviously the opposite of that, this will still apply to Mature Students that are looking to apply too.
Are there any benefits to becoming a Mature Student?
There is always a benefit to going to university, assuming you’re prepared to work and put the effort in, you will always see yourself benefiting from university. Obviously, the stock answers that you’ll most likely see are:
Learning new skills
Professional progression
New life experience
But you can always find a good reason to go to university, regardless of your age, there’s no reason to miss out on university just because you think you’re too old when it comes to education, age is just a number.
What qualifications do I need to become a Mature Student?
This is all dependent on the university that you’re applying to. If you are a student of around twenty-one, then you can just apply with your usual qualifications and grades, if you’re an older student, there may be a possibility that your university may not accept your grades or qualifications anymore, so they may ask you to take of the many  Open University courses , or ask you to an Access Course of some kind; this will allow them to assess you and your abilities and see if you will fit into the university’s way of life and ethics.
You can also use an APL, which is an Accreditation of Prior Learning, which allows universities to see the work that you have done previously, whether that be through work experience you have done or any private and self-taught work that you’ve done, however not all universities will accept one or all of these applications, so you will need to speak to the university in question to help you when you’re applying to university.
Are there Childcare facilities to help me when I’m a Mature Student?
This again will depend on the university that you’re applying for. If you have a small child that needs to be cared for in any way, then you should speak to your university and see if there is anything that is offered for students. Some universities will offer daycare facilities, some will offer a babysitting service of some kind, or they will have a grant system in place to help those with small children, it’s best to speak to your university and see what’s happening with this, if not, get used to having to use a lot of babysitters!
Are courses full-time or part-time for Mature Students?
It’s entirely up to you. If you choose to go to university and you’d like a full-time course , then you can choose that, however, if you’re balancing your course around your work life and home life, then you are more than welcome to opt in for a part-time degree if it’s easier for you, ultimately, your course is applicable to what you want to do.
Am I still entitled to a Maintenance Loan if I am a Mature Student?
Of course, you are, all student finance remains to same. A Maintenance Loan is available for all students regardless of race, gender, sexual orientation, age or what you’re studying; of course, the amount that you can borrow is very much all dependent on what it is that you’re studying, where you’re studying and how much money you have coming into your house at that time.
Are there any Grants, Bursaries or Scholarships offered for Mature Students?
There are a number of grants available for Mature Students, which will apply depending on the circumstances; also, there will be other grants and bursaries or scholarships that you would be entitled to as well, it depends on what you’re applying for (learn more – student grants, scholarships and bursaries) . The most common ones are Mature Student Scholarships, Access Entry Mature Students Bursary and Women Careers Foundation.
What is a Mature Student Scholarship?
Most universities offer this as a scholarship for students that have shown exceptional academic prospect, for the most part, the grant will cover around £3,000. Although, most universities won’t cover students below the age of twenty-five.
What is an Access Entry Mature Students Bursary?
Available for students over the age of twenty-one, this bursary is good for £1,000-a-year, which will keep the wolves away from the door. The bursary is all dependent on the university, however, as there may be one or two universities that don’t offer it at all.
What is a Women Careers Foundation?
This is a grant that is granted (See what we did there?) to female students that are over the age of twenty-one (Although exceptions have been known to happen for younger students that are studying a  Music degree or Dance degree (Or Musical Theatre degree ) at university), this is all dependent on the university, as many universities may not allow students to apply for these at their university, owing to university guidelines and the like, it’s best to do your research and to contact your university beforehand.
Do I still have to pay my loans back if I am a Mature Student?
Woah, slow down cowboy! You won’t be dodging any fees that easily, not on this government’s watch! All student loan repayments have to be paid back, and you won’t be able to dodge them, no matter how hard you try, you will have your debt wiped after around thirty years the same as everyone else, and you will only pay back your loan if you’re able to, don’t worry, the loans will always need to be paid back, which we imagine will bring you a great comfort.
So there you have it! Everything that you need to know about becoming a Mature Student! Universities will always have Mature Students on their books, and they will also have people that are willing to help Mature Students, remember that education knows no age, as long as you’re prepared to work, you’ll fit right in!
', '2018-12-07 19:25:28');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3765, 'Student Union: Everything You Need to Know
Student Union: Everything You Need to Know
University Advice
Student Union: Everything You Need to Know
Going to university for the first time can be a very scary thing to experience. The worries of leaving your friends and family behind, the problems that can arise when you’re feeling homesick at university or what to do when your friends are going to different universities or if you’re struggling with any mental health issues at university . Luckily for you, your Student Union can be a big help to you when adjusting to university!
But what does a Student Union do for students and how does it help students? Well, luckily we’ve compiled a useful guide for you to see how your Student Union can help you.
What is a Student Union?
A Student Union is an organisation that is set up by a university to represent the best interests of students.
A Student Union will help students to deal with any issues that they may have and to introduce students to university facilities. Your student union can also help you if you’re experiencing bullying, sexual harassment at university , homesickness or exam stress , there are plenty of things that your Student Union is able to help you with.
As part of a Student Union, you will also be given an NUS Card, which means you will be able to claim student discounts all around the UK!
Do all universities have a Student Union?
Yes, they do!
All universities have a Student Union and they all work the same way. It is a requirement for a university to have a Student Union. A Student Union represents the voice of the students and are a good way of gauging
Is my Student Union part of a larger group?
All Student Unions are all affiliated with the National Union of Students (NUS). The NUS represents everyone from undergraduates all the way through to Mature Students . However, there are three other bodies that represent student interests nationwide as well, such as:
Coalition of Higher Education Students in Scotland (CHESS)
Aldwych Group
These groups cover the same area as the NUS, however, they focus more on other areas. The Coalition of Higher Education Students in Scotland focuses on university students in Scotland and will help you with everything that you need at university, whether that be help with studies, issues surrounding bullying or student finance in Scotland .
The Aldwych Group purely covers Russell Group universities .
Does the NUS cover Northern Ireland?
Yes, it does. Northern Ireland used to have it’s own Student Union, called the National Union of Students-Union of Students in Ireland (NUS-USI), which apart from being an incredibly over-complicated and somewhat confusing name for a student union, became part of the NUS in 1972, which means you’re covered in Northern Ireland as well!
Am I entitled to any discounts through my Student Union?
This is probably one of the best things about being part of your university’s Student Union! As a student, you will be entitled to various student discounts and will be able to use your Student NUS Card (See below) to claim some discounts.
Many companies already offer student discounts for students, but you will need to have your NUS Card as a way of proving that you’re a student first.
Companies like ASOS and Amazon will already have incentives open to students, and being able to use your NUS Card means that you will be able to save money on equipment that you need for university.
What is a Student NUS Card?
A Student NUS Card is a card that allows you to take advantage of student discounts. A Student NUS Card costs £12 and will allow you to save money as a student on everything from food to cinema trips, so if you fancy seeing whatever Star Wars film Disney decide to make this year, then you can go with a nice little discount to help you out.
What is also useful, is that an NUS Card can actually help you with travel too! Although students can still claim a student travel grant , even with their NUS Card, some students still need to apply for various travel incentives, such as the 16-25 Railcard , which allows students to save 30% off of travel. With an NUS Card, you can save 11% off of the cost of the Railcard, talk about top savings, right!?
Will I meet my Student Union representatives at a University Open Day?
This really depends on the university, and the University Open Day itself. It’s unlikely that a university would not have a representative from their university there, they may even have several student ambassadors. Most universities give you the chance to visit the Student Union building on your open day as it is, and as a result, you may be able to go there and speak to them at your will.
Can I speak to my Student Union if I’m facing any difficulties at university?
Your Student Union will always be there for you. Many students encounter different issues at university, whether it be facing issues with bullying at university , sexual harassment, homesickness at university or general exam stress, and your Student Union can help you with all of them!
Your Student Union can help you with exam stress by helping you to draw up a study plan or to help you with revision, they can give you some helpful advice for those that are facing issues with being homesick, by introducing you to a number of different services for staying in touch with friends and family and can help you with any statements made to the police or the university, in cases of sexual harassment.
Your Student Union is one of the most important facilities open to students at university, and we cannot recommend it enough. A Student Union is there to offer you help and is there to provide you with the tools you need to survive university, whether that be an NUS Card for student discounts or helping you with any issues you’re having at university, your student union is always there for you.
', '2018-12-07 19:25:28');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3837, 'Us
How everything started
Fab Lab Limerick started in 2012 as an elective course at the School of Architecture, UL in which open source 3D printers, CNC routers and laser cutters were built. In 2014 this self-build equipment was moved to an empty building in the city centre owned by Limerick City Council.
Since then, Fab Lab Limerick has evolved into a fully functional digital fabrication laboratory that offers cultural, educational and research programmes on digital fabrication, bridging the gap between these technologies and creatives from all disciplines.
Meet the team
Thomas O''Brien
Volunteer
Former Fab Lab Limerick collaborators: Michael McLaughlin, Matteo Oppo, Stephen Bourke, Sean Collins, Barry Casey, Aoife Marnane, J. O''Riordan, Jim Murphy, Colin Dorgan, M. Mainardi, Gabriela Sartori, Rafael Melo, Jack Byrne, Luke Benson, Dave Hunt, Battista S., Isabelle Carvalho, Niccolo Nulchis, Hottmar Loch, Alejandro Sanchez, Haidê Neves, Luis Graff, Ahsan Iqbal, Andrêssa De Andrade Machado, Luiz Felipe Ferreira, Tamires Lourdes, Diego Augusto Tavares, Aoife Barry, Darren Simring, Andrew Tynan, Kris Kelly, Lucia Poliokova.
Become a Volunteer yourself! Check the guidelines
and contact us fablab(still at)saul.ie
Fab Lab Limerick in the media
Workshop to ''build'' ideas for living in Georgian Limerick
Sep 16 2016 · Limerick Leader
˝…Living Georgian, a community design workshop on the topic of living in Georgian Limerick, will be held in the FabLab Limerick… …The workshop is part of a series of community design workshops run b…˝
Citizens of Limerick: Javi BuronGarcia
Jul 15 2016 · Limerick2020
˝…In order to call yourself a Fab Lab you need to have digital fabrication tools, an open-door policy, create public events and activities, and you have to acknowledge and collaborate with other Fab…˝
Sem autonomia é impossível a criatividade
May 24 2016 · Unisul Hoje
˝…definiu a ideia central da Semana Criativa, promovida na Unidade Florianópolis, sob as temáticas Movimento Maker e Fabricação Digital. Na University of Limerick, o FabLab é um espaço dedicado a fa…˝
blueprint prepared to create better future for butter bui...
Nov 04 2015 · Irish Examiner
˝…the possible transformation of the former Butter Exchange in Shandon… …The project, which is based on similar hubs such as the Fab Lab in Limerick, is subject to funding.
Dress for the future causes lightning strike in Limerick
Oct 17 2015 · Limerick Leader
˝…Thanks to the support from ID2015 and Fab Lab Limerick (a space for makers and designers on Rutland Street in the city) Lucia Poliakova, Dave Hunt and Mariel Mazan Palacios held a series of worksh…˝
Mini-park springs up in city centre
Oct 07 2015 · Galway Independent
˝The local authority, along with the Woodquay Business and Residents Association, FabLab at School of Architecture University of Limerick and Galway Men''s Shed, designed and fabricated the street pa…˝
', '2018-12-07 19:25:30');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3851, 'About us
About us
Mission Statement | The Fab School provides students with the technical and hands-on skills required to become marketable candidates for employment in the metal fabrication and welding industry through classroom activities and practical shop experiences.
About The Fab School
The Fab School is a private, post-secondary, vocational school that specializes in the fundamentals of metal fabrication. The school has been educating students on the technical skills and theories of fabrication since 2005, providing hands-on training in a real world, working fabrication shop environment. The Fab School is located in Rancho Cucamonga, California.
Get Your Experience from the Experienced
The Fab School operates under the leadership of the school founder, Troy Johnson. Troy is a leader in the fabrication industry and has more than 25 years of experience. Troy has hand-picked some of the top industry professionals to join The Fab School as instructors. Each instructor has over 10 years of experience in the fabrication industry and possesses unique, insider knowledge of the complexity and scope of work involved in the fabrication industry today. Troy and the instructors at The Fab School take pride in passing their skills, expertise, and industry knowledge to the students.
Hands-on Fabrication Skill Development
The Fab School offers instruction on the fundamentals of metal fabrication. The program schedules are fast paced. The curriculum embraces the hands-on approach to learning, and the class size is designed to allow for group work as well as individualized attention. Students can complete the program in as little as 7 months.
WHY CHOOSE THE FAB SCHOOL?
The Fab School offers instruction on the fundamentals of metal fabrication. The curriculum embraces the hands-on approach to learning, and the class size is designed to allow for group work as well as individualized attention. With our fast paced program schedules, students can complete the program in as little as 7 months.
Hands on Training | The Fab School provides a hands on approach to learning. Our day and evening programs enables students to learn a full range of fabrication skills utilizing the most current technology.
Federal Financial Aid | The Fab School accepts federal financial aid through the U.S. Department of Education’s Title IV Program for those who qualify.
Job Placement Assistance | The Fab School has a career services department that is second to none, offering graduates job placement assistance in the metal fabrication and welding industry.
Veteran Approved | The Fab School also accepts all programs offered by the Department of Veteran Affairs and the tuition assistance program for active duty and reservists.
Call. 1-877-411-WELD
', '2018-12-07 19:25:31');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3866, '×
Buy some games
You added Feelif Gamer to your cart. Feelif gamer comes with preloaded demo games, of which you can choose two to fully activate. In addition to those two you can also buy more games.
', '2018-12-07 19:25:32');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3928, 'Questions about the project: team@futurict.eu
Problems with the website: website@futurict.eu
Posting news and events: social-media@futurict.eu
Project Chairs
Scientific Coordinator: Prof. Dr. Dirk Helbing, ETH Zürich, Switzerland: dirk.helbing@gess.ethz.ch
Coordination Action Work Package Leaders
WP1 – Co-evolution of ICT and Society
Prof. Dr. Paul Lukowicz, Universität Passau, Germany
WP2 – Interconnected observatories of society for a resilient and sustainable future
Dott.ssa Rosaria Conte, ISTC-CNR, Italy
WP3 – Designing an Innovation Accelerator
Prof. Dr. Dirk Helbing, ETH Zürich, Switzerland
WP4 – Management
Prof. Steven Bishop, UCL, UK
WP5 – Flagship infrastructure
Dr. Felix Reed-Tsochas, Oxford University, UK
WP6 – Dissemination and Stakeholder Liaison
JB McCarthy, University College Cork, Ireland
FuturICT National Representatives
Stefan Thurner, Medical University of Vienna, Austria: thurner@univie.ac.at
Vincent Blondel, Université Catholique de Louvain, Belgium: blondel@mit.edu , vincent.blondel@uclouvain.be
Nikolay Vitanov, Bulgarian Academy of Sciences, Bulgaria: vitanov@imbm.bas.bg
Pavel Kordik, Czech Technical University in Prague, Czechia: kordikp@fit.cvut.cz
Erik Mosekilde, Department of Physics, Technical University of Denmark, Denmark: erik.mosekilde@fysik.dtu.dk
Tarmo Soomere, Tallinn University of Technology, Estonia: tarmo.soomere@cs.ioc.ee
Jean-Pierre Nadal, “École Normale Supérieure and École des Hautes Études en Sciences Sociales, CNRS”, France: nadal@lps.ens.fr
Paul Lukowicz*, Passau University, Germany: paul.lukowicz@uni-passau.de
Panos Argyrakis, University of Thessaloniki, Greece: panos@physics.auth.gr
Janos Kertesz, Budapest University of Technology and Economics, Hungary: janos.kertesz@gmail.com
JB McCarthy*, Development Director Financial Services Innovation, University College Cork, Ireland: Jb.McCarthy@ucc.ie
Shlomo Havlin, Bar-Ilan University, Ramat Gan, Israel: havlin@ophir.ph.biu.ac.il
Rosaria Conte*, CNR, Roma  Italy: rosaria.conte@gmail.com
Atis Kapenieks, Director of Riga Technical University Distance Education Study Centre, Head of Latvian Delegation in ICTC: atis.kapenieks@rtu.lv
Arturas Kaklauskas, Professor of Vilnius Gediminas Technical University, Lithuania: arturas.kaklauskas@vgtu.lt
Eric Ras, CRP Henri Tudor, Luxembourg: eric.ras@tudor.lu
Cars Hommes, Department of Quantitative Economics, University of Amsterdam, Netherlands: C.H.Hommes@uva.nl
Ingve Simonsen, Norwegian University of Science and Technology, Norway: Ingve.Simonsen@ntnu.no
Janusz Holyst, Warsaw University of Technology, Poland: jholyst@if.pw.edu.pl
Jorge Louca, Lisbon University Institute, Portugal: jorge.l@iscte.pt
Carmen Costea, ASE Bucharest, Romania: cecostea@yahoo.com
Gregor Pipan, Mobility Researcher at University of Warwick and Xlab Slovenia, Slovenia: gregor.pipan@xlab.si
Maxi San Miguel, Campus Universitat Illes Balears, Spain: maxi@ifisc.uib-csic.es
Bjorn-Ola Linner, Water and Environmental Studies, Centre for Climate Science and Policy Research at Linköping University, Sweden: bjorn-ola.linner@liu.se
Steven Bishop*, Department of Mathematics, University College of London, United Kingdom: s.bishop@ucl.ac.uk
Dirk Helbing*, Department of Sociology, in particular Modelling, ETHZ, Switzerland: dirk.helbing@gess.ethz.ch
Hideaki Aoyama, Kyoto University, Japan: hideaki.aoyama@gmail.com
Ian Wilkinson, University of Sydney & Visiting Professor of Entrepreneurship and Relationship Management, University of Southern Denmark, Australia: I.Wilkinson@econ.usyd.edu.au
Ryan Murphy, Singapore Risk Center, Singapore: rmurphy@ethz.ch
The work leading to these results has received funding from the European Union Seventh Framework Programme (FP7/2007-2013) under grant agreement n° 284709 - project ''FuturICT'', a Coordination and Support Action in the Information and Communication Technologies activity area.
 
', '2018-12-07 19:25:35');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3930, 'Us
Who we are
Founded by corporate learning expert Jack Makhlouf, ELM was born to tackle Corporate America’s stale learning programs using a science based approach to learning, which asserts that people learn best when they are cognitively engaged in a learning experience.  In 2013, Jack joined forces with entrepreneurs Andrew Fayad and Paul Fayad, whose expertise in creating an engaged workforce and modern leadership principles has evolved ELM into the powerhouse it is today.  Cognitive science tells us that people learn best when they see relevance and meaning in an experience. That’s why we make learning easy, interactive, fun and even magical, delivering results that transform how leading businesses learn and grow, and inspiring enthusiasm for continuous learning. We listen. We strategize. We collaborate.  While we are passionate about our work, we are just as passionate about our people and the remarkable culture we foster together. We support one another’s efforts, value all ideas, get high on teamwork, and push every day to learn more, do more, and be the best that we can be. We are ELM.
Our clients
Our creative approach, scientific foundation, unparalleled artistry and commitment to collaboration set us apart and ensure that we deliver what clients want, and what learners need. Some of our clients include:
Want to work with us? Contact us today!
Our approach
We want to let you in on a little secret: neuroscience has been used in marketing for decades. It’s why you have an emotional reaction to every Disneyland commercial, and why you reach for a sweater on sale–whether you need it or not. Brands know how to use the brain to create emotional connections and a sense of urgency to influence your buying decisions.  While marketing has been ahead of the neuroscience curve, our founder, Jack Makhlouf, saw an opportunity for neuromarketing to be used as a force of good–in learning. If the marketing industry could harness cognition, emotion, and memory to make you buy a sweater you don’t need, he could use the same approach to shake up stale training and improve learner experiences.  The truth is that brains respond to stories; whether it’s a tearjerker animated movie or a sales module, the reaction and retention is the same. That’s because contextualized stories strengthen the neural pathways in our brains to create better recall. When a learner connects a story to an emotion, it’s almost impossible for them to forget the content. In fact, it’s the way our brains are meant to learn: from early man cave paintings to modern professionals viewing instructional videos, our genetics crave storytelling and visual communications.  The ELM approach to training was reborn as neurolearning: a perfect mixture of cognitive engagement and information retention. In short, we combine beautiful design, compelling stories, and smart content so your learners experience an entire spectrum of engagement, emotion, and focus.  Above all, we respect learners. By applying our neuroscientific research with cutting-edge design, we’ve created a full-service eLearning agency—leveraging everything from storytelling to microlearning, animation, and instructor-led training—to create the perfect environment for your learners. We’ve assembled a talented and innovative team of designers, animators, and learning architects to synchronize what we know about neuroscience with the way modern learners want to experience content.  Harnessing the power of neuroscience for the good of the learner: it’s the smart, beautiful way we’ve revolutionized the training industry.
Meet the Team
Andrew Fayad, Chief Executive Officer
Jack Makhlouf, Chief Learning Officer
Paul Fayad, Chief Financial Officer + Chief Culture Officer
Tori Signorelli, VP of Service Design
Ashley Hudson-Hines, VP of Solutions Design
Greg Kozera, VP of Creative Design
Brad Previti, VP of Accounts
Dori Malinowski, Head of Support Services
Katie Estes, Senior Art Director
Johnny Bustamante, Senior Art Director
Beth Epperson, Quality Assurance Lead
Sara Cardoza, Script Lead
Courtney Barnes, Senior Project Manager
Eunjae Kim, Lead Learning Experience Designer
Dawn Vitale, Project Management Lead
Candace Groff , Art Director
', '2018-12-07 19:25:36');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3930, 'Sign up for our newsletter:
© 2018 ELM Learning
', '2018-12-07 19:25:36');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (3968, 'A diverse team, a powerful story and a clear vision
 
"Strategy is a commodity, execution is an art."
- Peter Drucker
Recognized
From the launch of the first platform in Mexico to its current work facilitating far reaching international campaigns, GovFaces'' contributions to social engagement have been highlighted across the full range of media in more than 15 countries.
Trusted
Committed to the objective of improving public engagement, GovFaces has worked and maintains relationships with globally recognized institutions, groups and initiatives. 
Awarded
With its initial reception of the Karios 50 Award on the floor of New York Stock Exchange, GovFaces has been fortunate to receive honors for its work in providing innovative solutions to improving social and political discussion.
Jon Mark Walls
Having worked at the top levels of government, multi-lateral institutions and non-governmental organizations in Europe and North America, Jon Mark has led the GovFaces team in its work since 2012.
Tudor Mihailescu
Specializing in speechwriting and political rhetoric, Tudor blends a strong background in the nuances of political communication with strategy development, consultancy and grassroots organization.
Alexis Bourgeois
Serving as a focal point for business operations and execution, Alexis brings in depth knowledge of social and political systems and supports activity in the growth and development of client portfolios.
Juan Manuel Olea
Working across a range of technologies in designing and developing platforms, Juan Manuel oversees the coordination and maintenance of GovFaces platforms.
Javier Hernandez
Facilitating the creation of the design of GovFaces, Javier is dedicated to ensuring a smooth and rich user experience delivering the most value with each login.
Daniel Gomez
Founding GovFaces, Daniel has worked steadily to maintain the vision and operations of GovFaces as it has been implemented across countries and systems..
Our Team
As a team, we hold tightly to principles that have proven their worth over the course of our time together. Whether it is crafting a proposal or executing a project, we are committed to understanding the "why" before attacking the "what"; striving towards - but not being hindered by - the pursuit of perfect; and judging every keystroke, call and action by the metrics of quality, effectiveness and efficiency.
', '2018-12-07 19:25:37');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4058, 'NEWCOMER COMPANIES HELPER ABOUT US LOG IN
Meet the Team
We are a team of young people who believe in the potential of diversity and therefore want to actively shape tomorrow''s society.
Meet our team!
How did HIRE start?
HIRE started in fall 2015 as an innovation project of the entrepreneurial scholarship Manage&More at the UnternehmerTUM in Munich. We decided to team up for our shared motivation to give back and impact our society. The goal was to see how we can help in the current refugee crisis in Europe. 
In order find out about the problems more than 40 Newcomers have been interviewed in the early stage of the project. We learned that work is one of the main needs. The goal of HIRE to support the integration of Newcomers into workforce was born.
The team
Christopher Rehberg
Co-Founder & CEO
Raised in East-Berlin, equipped with an Original DDR (Eastern German) birth certificate, successfully migrated into Western Germany, BRD. Still struggling with Bavarian, though.
Johannes Geiger
Co-Founder & CEO
Was always warmly welcomed during his stay abroad in South Africa and Canada. He has been socially engaged with Rotaract for many years.
Timo Ludwig
Niklas Wolf
Co-Founder
Born into a standard-German speaking family and raised among Bavarians, Niklas first foreign language was Bavarian.
Robert Urban
Web Development
Robert''s family benefited of the German integration system in the late 1980s. He wants to help others to get the same chance as he did.
Alexander Dobler
Business Development
A real child of Munich. Now he lives part-time in England and therefore knows how important and appreciated support from locals is.
Felix Thiele
Web Development
Having just arrived in Munich himself, he hopes to help others arriving in Germany with HIRE.
Alexander Modes
Web Development
Loves to live in Germany. He wants to help that other people have the chance to feel like him.
Supporters
For some special things we do not have the needed skills or resources ourself... So we are very very thankful that we have the following people, who (did) help and support us:
Andreas Liebl
McKinsey-trained project mentor. Thank you, Andreas for your support!
Lena May
Thank you Lena , your efforts for HIRE were amazing!
Timo Friesland
Coach and entrepreneur. Timo , the team still needs you! ;)
Zoltan Elekes
Incredible designer and creator. Check out the LittleBlueBox -group!
Manuel Pfeifer
Thank you for the legal work. If you want to work with an amazing lawyer supporting social causes check him out!
Newsletter
', '2018-12-07 19:25:38');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4131, 'About us
Kaini Industries – Unlocking the hidden value in Hull’s economy
Kaini Industries was established in April 2014 as a result of a piece of research undertaken by Hull City Council that explored how the disruptive technology underpinning bitcoin could used as a local currency to support communities in Hull affected by poverty.
Hullcoin is a new way to unlock the hidden value in Hull’s economy. People who engage with charities and community groups across the city of Hull can earn Hullcoin by volunteering and undertaking activities that benefit themselves. Hullcoin can then be redeemed as discount at participating retailers. Activities that earn Hullcoin and offers where you spend Hullcoin can be found at our website join today!
Kaini Industries is a company registered by guarantee in England. Company No.: 09017003 Registered Office:  The Annex, Community Enterprise Centre, Cottingham Road, Hull HU5 2DH. HullCoin is a trading name of Kaini Industries.
COMPANY
', '2018-12-07 19:25:38');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4175, 'Equity Tower 38th Floor, Unit D
SCBD Lot 9
Jl. Jend. Sudirman Kav. 52-53
Jakarta 12190
', '2018-12-07 19:25:41');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4183, 'Home Challenges Solutions Crowdfunding Implementation Shop
Contact us
If you do not want to register yet, we would appreciate getting in contact with you. Prior to registration, if you have any general questions or about submitting a challenge or a solution, please contact us here.
Name
', '2018-12-07 19:25:42');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4188, 'Locations Overview
Community-focused with a national footprint
Headquartered in Denver, Colo., InnovAge offers PACE in Colorado, California, New Mexico, Pennsylvania, and Virginia. Our approximately 1,300 employees serve more than 4,000 seniors. In Colorado, we also offer home care services for seniors, and own two affordable senior housing communities in the Denver metro area.
The mission of InnovAge is to allow seniors to live life on their terms – by aging in place, in their own homes and communities, for as long as safely possible. The primary way we do this is through PACE. InnovAge is committed to expanding PACE to serve more seniors in more communities.  
', '2018-12-07 19:25:42');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4188, 'Please enter a valid email.
Please enter a valid email.
Please enter a valid email.
Phone Number *
Please enter a valid phone number.
Please enter a valid phone number.
Please enter a valid phone number.
Anything else we should know?
Tell us here, but please do not include any Protected Health Information (PHI), dates of birth, Social Security numbers, or any other confidential information.
Contact InnovAge
', '2018-12-07 19:25:42');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4222, 'Find a Remploy branch near you
We have branches nationwide. Use our branch finder to find a Remploy branch near you. 
', '2018-12-07 19:25:43');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4222, 'About us
About us
Remploy is a leading provider of specialist employment and skills support for disabled people and those with health conditions.
Our mission is to transform the lives of disabled people and those experiencing complex barriers to work.
We do this by supporting thousands of people every year across a range of programmes, with hundreds of delivery locations at the heart of the communities we serve.
We are the only specialist employment support provider in Britain operating major programmes across England, Wales and Scotland.
Remploy has a long and distinguished history, stretching back over seven decades, of supporting disabled people into work. We are expert in helping employers to create a workplace environment where all employees can thrive and to recognise the benefit to their business of employing disabled talent.
Innovation is at the heart of what we do. Remploy operates a unique online service allowing disabled people to access support from their home or wherever is convenient. We also work with like-minded organisations to develop innovative partnerships to improve the support we offer.
Our business is a partnership between MAXIMUS, a leading provider of employment and health related services to governments on four continents, and our employees who own 20 per cent of Remploy.
To find out more about our business click here .
You can also find out more about the programmes and contracts we deliver, by clicking here .
Find out more
', '2018-12-07 19:25:43');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4222, 'Get in touch with our Quality Matters team on:
Telephone: 0300 456 8119
Opening hours: Monday to Friday, 9am-5pm.
You can also complete our online form by telling us what you think.
Find out more
', '2018-12-07 19:25:43');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4254, '(you can always unsubscribe again. See Privacy Policy )
Kiron Open Higher Education gGmbH
Palais Am Festungsgraben
', '2018-12-07 19:25:45');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4270, 'Contact
Helper circles and volunteers
Contact us to find out more about the Lale platform. Request a demo or let us help you introduce Lale to your helper circle.
Ralf Schröder (Lale.help)
Thank you! Your submission has been received!
Oops! Something went wrong while submitting the form
Our platform is developed in open collaboration of digital workers all over the world. The more we are, the faster we can release updates to Lale and help volunteers making a difference for refugees. You can join our efforts in many different ways.
Developers and Programmers
Look at our github repository at  lale-help  to find our Ruby code. We have curated issues backlog which can be picked up and jointly worked on.
Creative Minds and UX Designers
In order to make Lale as intuitive as possible, we need the best UX design to put our functionality into the hands of the often not so tech savvy helper community. You can join our discussions on  lale.slack.com . 
', '2018-12-07 19:25:45');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4353, 'Posted May 18, 2018 by woolfini
We are looking for a Technical Web Developer.
At Made Open, we have been creating digital solutions and services for some time now. Mostly we have been working with associates and contractors on a freelance basis. Now, we are looking to bring technical web development skills in house.
The person we are looking for will take responsibility for our technical projects – developing code, ensuring quality control and taking on a technical leadership position as our digital team expands.
We have some exciting projects lined up (in particular developing our community platform), and so we are looking for someone with a proven track record who can hit the ground running. This will be a challenging role within our dynamic and creative company – based in beautiful Cornwall.
Initially, this will be a 12 month full time contract, based in our peaceful office.
The really important bits:
You will have worked in a fast-paced agency environment in a similar technical positin.
You know MySQL and PHP like the back of your hand.
Your JavaScript, JQuery and AJAX is pretty awesome.
You know how to produce technical documentation and service specifications.
You have a process for ensuring compatibility across browsers and devices.
You don’t break a sweat when accessibility is mentioned.
You have excellent written and verbal communication skills.
You can critically review the code of other developers.
You have an energetic and positive attitude.
You have an eye for detail – 1 pixel out is not good enough.
The bits that will set you apart from the rest:
You use Laravel
You can demonstrate that you use Agile / Scrum methodologies.
You can turn your hand to semantically marked up HTML5 and CSS if required.
You can demonstrate that you love exploring new technologies.
You have built cool things that use and display large volumes of open, linked data.
You have played with lots of APIs and maybe built your own.
What you get:
A 12-month full time contract on a competitive starting salary with a three month review.
A PC loaded with all the tools you need to hit the ground running.
The opportunity to make your mark in an exciting growing business.
Note to freelancers: if you are currently freelancing and are interested in a retainer whilst keeping your independence, we would open to a more flexible working arrangement for the right candidate.
How to apply:
Please send a cover letter with your availability, a CV with some references, a link to your online portfolio and anything else you think would help you stand out to robert@madeopen.co.uk .
No agencies please.
Here’s the job spec .
Platform
', '2018-12-07 19:25:47');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4387, 'Developing tomorrow''s entrepreneurs today!
Testimonials
An inspiring programme “The programme aimed to inspire rather than drill knowledge into our... Read more
Alinique Swart
AEP 2018 graduate
It´s perfect for people like me, starting from zero and asking the correct questions as well as... Read more
Cesar Quiero
Co-founder
Fantastic programme, that has a helluva lot on offer. Stimulates both youth entrepreneurs and older... Read more
Clint Davies
Professor
“Mashauri has been instrumental in helping me set up my coaching and career advice business.... Read more
Simon Bailey
Mashauri: one stop excellence centre for global entrepreneurs  
Kassim Badmus
Entrepreneur
The programme has opened my eyes to what I want to become and what my passion is. When I started... Read more
Luqmaan Samsodien
Founder
I expected the ABC of entrepreneurship and received that AND MORE. I am fired up. More aware. More... Read more
Mauricia Abdol
Founder
An eye opening experience, offering an insight on entrepreneurship of which I was not previously... Read more
Njabulo
Timeless and relevant sources helped me get to grips with the entrepreneurial journey.
Nonkosi Xaba
Founder
Great for knowing where you want to be and how to position yourself in the market, the drive,... Read more
Zimele Vezi
Founder
I have been using the Mashauri site for a while and love the short, easy-to-understand and highly... Read more
Mandy Conidaris
Founder
Mashauri gives you all the necessary tools you need to be a successful entrepreneur. Connecting... Read more
Lukhanyiso Nuno
Founder
I am looking forward to seeing how mashauri.org develops. I like the idea of a "Coursera" meets... Read more
Time 4 Goals
Founder
It was a great programme, excellent in all respects. It should be mandatory for every student to do... Read more
Zinzi Magoda
', '2018-12-07 19:25:47');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4416, 'You might have discovered MesInfos during MyData2017 conference. If you want to know all about the project, this blog post is for you !
MesInfos project
« If I (an organization) can use your data, you can too… however you please. » With this particular idea in mind, Fing and its partners launched the MesInfos project in 2012. The MesInfos project explores and implements the Self Data concept in France.
What would happen if organisations that gather personal data actually shared those datasets with the individuals concerned? From this starting point, MesInfos has set out to explore what could be a real paradigm shift in the digital economy. We call it Self Data: the collection, use and sharing of personal data by and for individuals, under their complete control and designed to fulfil their own needs and aspirations.
In 2016-2017, we decided to take actions so that the Self Data scenario arise from paper and experiments into reality. We have been leading the MesInfos Pilot since then – to gather organisations holding personal data, platforms/personal clouds, a territory, and an innovation ecosystem to pave the way for a concrete Self Data world (in which organisations are giving back data to their clients under no time limits). In 2017
- Each of the 3000 testers will get a Cozy Cloud (Personal Cloud), thanks to the insurance provider Maif.
- Several organisations (insurance, energy and telecom providers to start) will transmit the data they have on them directly to each of the Cozy Cloud, under their complete control.
- The testers will then be able to add more data (photo, calendar, files…) to their Cozy and enjoy services to help them in gaining use value from their data (for instance services to better manage their lives, improve one’s self-knowledge, …).
 
–>Want to know more about Self Data ? The Self Data FAQ is made for you .
–> You are asking yourself « The Pilot project was launched 1 year ago… but what have you been doing since then? », we wrote a blog post about this subject .
(All documents translated by Jianne Whelton: jianne.whelton@gmail.com)
 
MesInfos & MyData
Fing – through MesInfos project team – is one of MyData conference founders and organizers, along with Open Knowledge Finland, Aalto University and Tallinn University.
Our collective / cross-border journey began in 2015, when EU DG Connect organized a roundtable about PIMS topic. MesInfos project was invited; we met lots of people, projects, startups, researchers… in other words, this roundtable draw the first lines of a European Community on this subject. We met again and again in 2016 in different cities, helping OKF and Aalto University to design piece by piece the first event about MyData/Self Data model : MyData2016.
In 2017, the European Community took a step forward : it’s not only about building a 1 shot event, but about building a movement that will last and grow. If MyData conference founders are still totally committed in organizing the event, we are also working with other members of the community on another front : we are participating in building an actual MyData Organization, with its principles, goals, governance…
One of the first steps of this work is embodied into the MyData principles declaration . Here is an abstract of this declaration :
« We are entrepreneurs, activists, academics, listed corporations, public agencies, and developers. For years, we’ve been using different words for what we do – MyData, Self Data, VRM (Vendor Relationship Management), Internet of Me, PIMS (Personal Information Management Services) etc, while sharing a common goal: to empower individuals with their personal data, thus helping them and their communities develop knowledge, make informed decisions, and interact more consciously and efficiently with each other as well as with organisations. »
We are convinced that concrete pilot projects or experiments such as MesInfos (and others) can bring precious learnings that will help this movement to grow !
Laisser un commentaire
Votre adresse de messagerie ne sera pas publiée. Les champs obligatoires sont indiqués avec *
Nom *
Site web
Commentaire
Vous pouvez utiliser ces balises et attributs HTML  : <a href="" title=""> <abbr title=""> <acronym title=""> <b> <blockquote cite=""> <cite> <code> <del datetime=""> <em> <i> <q cite=""> <strike> <strong>
', '2018-12-07 19:25:48');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4494, 'I have read and accept the PRIVACY & COOKIES POLICY
×
', '2018-12-07 19:25:52');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4495, 'Open Data
Contact us!
The primary way of contacting the project is via the info [at] netcommons [dot] eu email, that is an alias to the project coordinator and the WP coordinator.
Else, you can contact us via twitter at the account netCommonsEU
', '2018-12-07 19:25:52');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4561, 'Next
About the Open Data Barometer
The Open Data Barometer aims to uncover the true prevalence and impact of open data initiatives around the world. It analyses global trends, and provides comparative data on countries and regions via an in-depth methodology combining contextual data, technical assessments and secondary indicators to explore multiple dimensions of open data readiness, implementation and impact.
This is the second edition of the Open Data Barometer, completing a two-year pilot of the Barometer methodology and providing data for comparative research. This report is just one expression of the Barometer, for which full data is also available, supporting secondary research into the progression of open data policies and practices across the world.
The Open Data Barometer forms part of the World Wide Web Foundation’s work on common assessment methods for open data .
You can contact the Barometer team by emailing: project-odb@webfoundation.org
Members of the media can download the press release , or request further information by emailing press@webfoundation.org
About the Web Foundation
The World Wide Web Foundation was established in 2009 by Web inventor, Sir Tim Berners-Lee. Our mission? To advance the open Web as a public good and a basic right.
Thanks to the Web, for the first time in history we can glimpse a society where everyone, everywhere has equal access to knowledge, voice and the ability to create. In this future, vital services such as health and education are delivered efficiently, access to knowledge unlocks economic value whilst access to information enhances transparency and strengthens democracy.
To achieve this vision, the Web Foundation operates at the confluence of technology, research and development, targeting three key areas: Access, Rights and Participation. Our work on open data connects across these themes, working to support inclusive approaches to open data impact across the globe.
Our work on open data covers:
Research - As part of the Open Data for Development Network, we support research and research capacity building across three continents. From 2013 – 2015 the Open Data in Developing Countries project has been exploring use and impacts of open data, and a new phase of this project will commence in early 2015, supporting regional research agendas in Africa and Asia.
Innovation - including building the first Open Contracting Data Standard , aimed at putting the $9 trillion that governments spend annually on procurement into the public domain. The project puts our values and research into practice, developing the standard through an open an inclusive approach, and keeping a focus on the participatory potential of open contracting data.
Training & capacity building - The Web Foundation’s Open Data Labs are experimenting with how open data can make a real difference in the Global South. By trying out new approaches, we want to accelerate progress and ensure open data rapidly becomes a vital tool to tackle practical problems in developing and emerging economies. Our first Open Data Lab is now open in Jakarta, and we will be announcing more soon.
Engagement - To encourage and support more governments to open up their data to citizens, we are co-chairing the Open Data Working Group of the Open Government Partnership, which brings together 80 governments and 120 civil society organisations to share practical know-how and promote good practices.
', '2018-12-07 19:25:57');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4566, '9/11: us upside down
About the author
Jeffrey C. Isaac is James H. Rudy Professor of Political Science and Director of the Center for the Study of Democracy and Public Life at Indiana University, Bloomington. His most recent book is Democracy in Dark Times (Cornell, 1998).
I am a supporter of the war. The terrorist attacks of 11 September, and their aftermath, have profoundly affected me psychologically as well as politically. I was horrified and outraged by these attacks. I have no doubt that the attackers are the enemies of the United States, of liberal democracy, and of universal values. I also have no doubt that these enemies are violent and that military force must be used to destroy their terror network; perhaps  if possible  to bring them to justice; and to provide some measure of security against the possibility of future terrorist acts.
I am a believer in human rights and in international law. I believe that the survival and flourishing of humankind requires us to develop, nurture, and support new forms of global responsibility, practices and institutions rooted in civil society, but also new forms of legal authority and international criminal enforcement. And yet I do not believe that current intimations of these new forms of responsibility are robust, effective, or possessed of global legitimacy. And so I believe that critics of war who appeal to international law are naïve, and indeed that their appeal is really a call to do nothing in the face of terror.
But as I have spent innumerable hours over the past six weeks thinking about current events, I have also been caused to think about the meaning of the very words with which I began this piece: I am a supporter of the war. This has not been an academic exercise. It is a form of self-reflection and indeed of political judgment, an effort to think hard about what I am doing with these words. For it seems to me that it is natural, and inevitable, in this time of crisis, fear, and war, that we are forced by circumstance to speak in such terms, declaring our support for  or, for some, opposition to  the war. And yet it also seems that at the same time that we are pressed to issue such declarations, we also should think about what such declarations mean.
Unpacking a declaration
The first thing to note about my statement is its beginning. I am. By using such a phrase we fix ourselves in time, as we must. At any given moment, each of us is likely to have an opinion, and, in declaring this opinion, at that moment, we are saying what we believe at that moment. But the simple statement I AM a supporter of the war leaves no room for the qualification that in fact is entailed by the very statement  that right now I am a supporter of the war, but this might change, that I am a thinking being who is constantly evaluating events and strategies and who may be caused, on reflection, to change his mind. A public discourse of simple declarative statements obscures the inherently dynamic character of our opinions. This is why is important to step back and interrogate our grammar.
The second thing to note about my statement is its concluding words  the war. What does this mean? Does it mean the general policy of military response to terrorism? Does it mean every aspect of the strategy and tactics behind the Bush administrations actual military response? The concluding two words of my statement do not answer these questions. But these questions are crucial. And, in point of fact, the ambiguity of the words the war accurately encapsulate my own deeply complex and ambivalent thoughts and feelings. For, while I know that I support a military response, and that I see no alternative to a military response, I also know that I have serious doubts about both the strategy and tactics of the current military response.
The third thing to note about my statement is what it does not say. It does not say anything about what I think about many other things  human rights; the requisites of true global responsibility; the limits of unilateral foreign policy; the importance of defending civil liberties in a time of war; the folly of attacking and dismantling the public sector, as conservatives have practiced and preached for the past two decades; the need, here at home and elsewhere, to attend seriously to problems of economic inequality, environmental degradation, racial injustice, and national chauvinism.
The above list is simply a sampling of what matters to me politically. Many of the issues listed relate, directly or indirectly, to the prosecution of the war. Many of them do not, and are no less important for that. At this moment many of these issues have taken a back seat to the question of war, for the nation but also for me. And this is perhaps warranted. But these questions are nonetheless important to me, and they raise questions of importance to our politics. And yet my simple declarative statement of support of the war, by its very nature, is silent about these things.
The fourth, and perhaps most important thing to note about my statement, is not a feature of its grammar but of its very character as a declarative statement: how situationally specific such a statement really is.
To declare is to act
We are inclined to think that political opinions are best expressed in the form of such declarative statements. A culture of polling and punditry surely helps to sustain the idea that we can and should state our opinions in simple, declarative form: I am in favor of X, I am against Y. But this is not wholly an artifact of certain unfortunate features of our political culture. It is a feature of political reality itself. There are occasions when it is necessary to formulate what one thinks in such terms. Such occasions are moments of singular decision. One such occasion is in the act of voting, where one must decide, simply, who or what to vote for, whether a candidate, a party, or a referendum item. Another such occasion is in the course of compliance with specific laws. The payment of taxes, for example, involves a decision to comply with the tax law. On the date that taxes are due, each individual must decide whether to make the payment in question.
One might decide, as did Thoreau, that one does not wish to comply with the payment of taxes that support a war. One may thus feel compelled to declare ones opposition to war in the course of such a decision. There is, finally, the decision to fight, whether as a conscript, an enlistee, or volunteer. Perhaps more than any other individuals, soldiers are pressed by circumstances to decide, in the starkest of terms, whether or not they support a war, by virtue of their decision whether or not to participate in the war, to place their lives on the line or willingly to take the lives of others.
Thinking openly, deciding provisionally
The above situations are not, to be sure, the only situations in which it makes sense to offer declarative statements about war. There are surely many other occasions  letters to politicians, public demonstrations, arguments with friends or adversaries  where one may reasonably feel moved to make such declarative statements.
But unlike voting, tax payment, and war fighting, such situations do not require that we make a decisive choice for or against. We are of course free to do so if we choose. But there is nothing about the situation that requires it. And there is even less about such situations that requires us to articulate our decisions or actions, whatever they may be, in simple declarative statements.
Indeed, for most of us, the general situation of being an individual and a citizen is rarely like the situation of the voting booth. It rarely places us in a position where we must think, and articulate our thoughts, in terms of clearcut declarative statements. We may sometimes choose to think and speak in such terms. But we also have the option of thinking, and speaking, in more complex terms. This is not a luxury that politicians and soldiers can typically afford. But it comprises, I would suggest, the main virtue of ordinary democratic citizenship.
The virtue I have in mind is intellectual complexity and openness, a willingness to think and speak with subtlety and depth, to listen to others, and to be attentive to changing circumstances and open to changing ones mind. What I am talking about is not indecisiveness. It is a certain kind of serious and intelligent civic responsibility. It involves an acknowledgment of the need to take positions and to offer support or, perhaps more typically, to refuse to oppose or to offer resistance. It involves an acknowledgment of the need to issue declarative statements. But it also acknowledges the limits of such statements, and the importance of also, simultaneously, thinking and talking in other ways, keeping questions and doubts open, keeping the possibility of change open.
The provisionality of democratic decisions is one of the things that distinguishes them from the decisions of non-democratic regimes and societies. In a democracy, political accountability and freedom of expression make all political decisions provisional. The government needs to make decisions. And we, as citizens, sometimes need to make our own decisions about whether to support or oppose these governmental decisions. But all of these decisions are provisional. All are subject to change. And it is communication about the issues, in all their complexity, that helps to ensure that political decisions are never dogmatic, never set in stone, never artifacts of coercion or inertia but instead are the products of public intelligence. Public intelligence is a precious feature of a democratic society, all too rarely appreciated, all too rarely achieved. It is never more needed than now, at a time of national crisis and war.
Wielding weapons, and words
I am a supporter of the war.
But what I think, and what I also say, is in fact much more complicated than this simple declarative statement.
In the weeks, months, and years ahead, it is important that each of us is very clear about what we are doing with our words. We will be pressed to make declarative statements. And such statements will have their place. But it is just as important to remember that the qualifications, and the questions, and the ambivalences have their place. We need to make sure that they have their place in our individual minds. And, even more important, we need to make sure that they have their place in our public culture.
The struggle against terrorism is a struggle on behalf of security and of life. But it is also a struggle on behalf of freedom and democracy. Right now the defence of our democracy requires us to be attentive to the things we do with weapons. But above all, democracy requires us to be supremely attentive to the things we do with words.
We encourage anyone to comment, please consult the
oD commenting guidelines if you have any questions.
Donate here→
Follow us
 
openDemocracy is an independent global media platform covering world affairs, ideas and culture which seeks to challenge power and encourage democratic debate across the world. Read more...
Highlights
', '2018-12-07 19:26:14');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4579, 'We’re ready to assist you.
Complete the form below and someone will be in touch with you shortly.
2780 Industrial Ln. Broomfield, CO 80020 United States
+1 720-415-4868
', '2018-12-07 19:26:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4579, 'About Us
OSBeehives
OSBeehives is on a mission to technify the beekeeping industry by building a global network of beekeepers and identifying causes and solutions for colony health deterioration. This mission is founded on the principles of citizen science, open data, and collaboration between beekeepers.
Our integrated hardware and software solutions make use of the latest deep neural network and digital signal processing technologies applied to beehive audio recordings and sensor measurements with the goal of detecting possible hive health problems. All of this can be done remotely and automatically, letting beekeepers know that something is going wrong so they can intervene and possibly save the colony. For example, colonies that are about to swarm have unique audio signatures and temperature changes that we can identify before the swarm itself occurs, giving the beekeeper a chance to capture the swarm or stop it entirely.
As Featured In
BuzzBox
Beehive Monitoring IoT Device
The BuzzBox is a solar-powered, Wi-Fi capable device that transmits audio, temperature, and humidity signals that are automatically analyzed using state of the art AI to predict the health of your hive. It also features theft alarms and an external weather station.
OSBeehives App
The best beekeeping app out there
The OSBeehives App is a free solution to remotely monitor hive measurements produced with a BuzzBox or any other device. The app also allows to record the sound produced by your beehive and analyze it using the same BuzzBox AI, with lots of exciting new features coming soon.
Beehives
Colorado Top Bar & the Barcelona Warre
The Colorado Top Bar & the Barcelona Warre beehives are open source, flat pack, snap fit hives that respect natural beekeeping principles to create ideal conditions for your bees. You can obtain the files for the hives for free in our Open Source page.
OSBeehives Team
', '2018-12-07 19:26:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4591, 'About openPetition
Start a petition and find supporters
openPetition supports petitioners in preparing their petition, collecting signatures and submitting the petition to the appropriate recipient. In addition, openPetition demands the opinion of parliamentarians independently of the formal petition process. Whether it concerns the preservation of the youth club, the decommissioning of nuclear power plants, changes in legislation or the construction of wind turbines: Every day users start petitions on our platform and push forward changes: locally, regionally, nationwide and bit by bit also in Europe. More than 6 million people use openPetition and become part of digital democracy.
The openPetition team
Jörg Mitzlaff studied computer sciences in Berlin and worked as a software developer and IT manager with companies including Infoseek, eBay and idealo. He became acitvely engaged with More Democracy before starting up the openPetition platform in 2010. He is a partner and managing director of the non-profit company openPetition,
Jonas Hantelmann - is software developer at openPetition. He previously trained as an IT specialist for application development and taught ICT at a school in Ghana.
Jessica Seip - Editing, social media and public relations are your hobbies. She studied cultural studies with a focus on social sciences/linguistics and lived in Spain for six months. Before that she was active in the radio landscape for several years.
Mirko Brunner - worked for over 12 years as a software developer in various agencies and implemented software solutions and smartphone apps. Now he is helping to program democracy, where data protection and security are of great concern to him.
Greta Linde - is a student assistant at openPetition and supports the editorial staff in press and public relations. She studies journalism/communications and literature at Freie Universität Berlin and is involved in unipolitics.
Rita Schuhmacher - ist studierte Medienwissenschaftlerin und fachpolitische Redakteurin. Sie engagiert sich unter anderem in der Gleichstellungspolitik und arbeitet bei openPetition als Campaignerin und Redakteurin im Bereich Presse- und Öffentlichkeitsarbeit.
Katrin Appel - ist Praktikantin bei openPetition und unterstützt die Redaktion bei Presse- und Öffentlichkeitsarbeit. Sie studierte Politikwissenschaft und Humangeographie an der Goethe Universität in Frankfurt und schrieb zuletzt ihre Abschlussarbeit über politische Partizipation bei Studierenden.
Ralf Engelmann - is an honorary system administrator for openPetition. He is particularly committed to data protection and data economy. He works full-time at an Internet price comparison portal.
The openPetition mission and how we want to achieve our goals
Make politics simpler - We advise and support petitioners so that each openPetition achieves maximum effect, i.e. reaches the relevant parliament and is processed there.
making policies more visible - We make sure that politicians know what moves citizens in their constituencies. For this purpose we have developed the Wahlkreisradar.
making policies more comprehensible - If a petition reaches the openPetition-quorum, we ask the respective parliament, i.e. all elected representatives of the region, for a statement.
Strengthen the right to petition - We are politically committed to a more effective petitions system. To this end, we talk to politicians and administration in the petitions committees in Germany at federal, state and European level.
Bringing petitions to parliaments across Europe - We want to open up the possibilities of the right of petition online in other European countries and help citizens to address their concerns to their political representatives through online participation.
Modernising parliamentary democracy - We want a modern online right of initiative and demand the right to be consulted and consulted on new, urgent citizens'' concerns in parliaments and specialist committees.
Values & principles
Neutrality and democratic values - openPetition is politically neutral: We do not intend to influence political opinion or support political parties unilaterally. All petition requests as well as recommendations for partner NGOs do not reflect our point of view, but show a colourful mixture of opinions - no matter where you come from or what you believe in. Because we want to promote constructive political debate, we pay attention to respecting our Terms of Use : Racism, misanthropy and discrimination have no place with us. We invite everyone to interfere under fair conditions. We see diversity of opinion as an enrichment for common, democratic discourses.
Privacy - Digital democracy can only work if personal data is safe from uncontrolled government access and selfish economic interests. If algorithms influence political opinion-forming, then they must be made transparent and easy to understand for everyone. Citizens must retain control of their data and the algorithms for processing the data at all times. The right to oblivion must be taken into account as early as the collection of data. As a non-governmental organisation for citizens'' concerns, we want to support the idea of a digital democracy and protect your political footprint on our platform. Please read our Privacy Policy .
We are non-profit.
openPetition was started as a voluntary project by software developper Jörg Mitzlaff in 2010. In 2012 he together with Campact e.V. founded the not- for-profit organisation openPetition gGmbH. It is recognized as a charitable organisation beause we are fostering education, democratic practice and civil society. It is listed in the register of companies of Local Court Berlin-Charlottenburg unter number HRB 144054 B. Our tax-exempt status is confirmed by finance office Finanzamt Berlin für Körperschaften I, most recent confimation dates from 12th of March 2015.
We are independent
openPetition is politically neutral. The topics on the platform cover the entire democratic political spectrum. openPetition does not adopt the demands and statements of the petitions - this also applies to petition recommendations.
In terms of acceptable content on our platform we are committed to our terms of use only. Our terms of use are based on democratic rights and values and are permanently being developed further in dialog with our users.
Our independence form our associates is stipulated in the Articles of Association.
More than 10,000 committed citizens already support the work of openPetition with a donation or a regular contribution.
Financial independence is important to us. Therefore, state funds or party donations are out of the question for us. So far we are financially supported by the Campact. e.V. campaign network and in return send out a partner mailing four times a year. Other NGOs and associations can also advertise their petition requests with us in individual cases and against payment. However, we are striving for 100% financing through small donations from our users.
Info pages
', '2018-12-07 19:26:17');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4594, 'Tweets by @OpenTnet
Subscribe to our newsletter
This project has received funding from the European Union’s Competitiveness and Innovation Framework Programme under grant agreement no. 620533
', '2018-12-07 19:26:21');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4594, 'Cookies help us deliver our services.
By using our services, you agree to our use of cookies.
 
The App evolution is a Map revolution!
Contact us for more info
This project has received funding from the European Union''s Competitiveness and Innovation Framework Programme under grant agreement no. 620533
', '2018-12-07 19:26:21');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4594, 'Cookies help us deliver our services.
By using our services, you agree to our use of cookies.
 
The App evolution is a Map revolution!
Kontaktujte nas pro vice informaci
Tento projekt je podporovan Ramcovym programem pro konkurenceschopnost a inovace (CIP) Evropske unie cislo 620533
', '2018-12-07 19:26:21');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4594, 'Cookies help us deliver our services.
By using our services, you agree to our use of cookies.
 
The App evolution is a Map revolution!
Contacteer ons voor meer info
Dit project ontving EU steun uit het "European Union''s Competitiveness and Innovation Framework Programma" onder grant agreement no. 620533
', '2018-12-07 19:26:21');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4632, 'Contact us
How can we help you?
Here are some frequently asked questions we receive (and their answers) which might solve your problem. You can explore our extensive FAQs or click on a question below. Still have a question? Please fill out the form below and we’ll get back to you soon.
', '2018-12-07 19:26:27');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4632, 'About us
We''ve partnered with 600,000+ people living with 2800+ conditions on 1 mission: to put patients first
Imagine this: a world where people with chronic health conditions get together and share their experiences living with disease. Where newly diagnosed patients can improve their outcomes by connecting with and learning from others who''ve gone before them. Where researchers learn more about what''s working, what''s not, and where the gaps are, so that they can develop new and better treatments.
It''s already happening at PatientsLikeMe. We''re a free website where people can share their health data to track their progress, help others, and change medicine for good.
"We started with the assumption that patients had knowledge we needed, rather than we had knowledge they needed. We didn''t have the answers, but patients had the insights that could help us collectively find them."
Jamie Heywood, Co-founder and Chairman
By actively involving people in their own care, we''re changing lives…
Sounds great, but how does it work? Let''s break it down:
People like you share symptoms, treatment info, and health outcomes.
PatientsLikeMe turns that into millions of data points about disease…
…and aggregates and organizes the data to reveal new insights.
We share back what we''ve learned with everyone — that''s our give data, get data philosophy.
Then, we share the patient experience with the industry so they can develop better products, services, and care.
…and igniting a digital health revolution
What makes and keeps us healthy, and what can we do daily to thrive?
These are the questions we''re striving to answer. And we''ve got a plan to get there — by creating a far more advanced network that combines even smarter, more comprehensive data to bring greater precision to healthcare decisions.
We''re working to bring you a new patient experience with more options, more information, and a roadmap to living better.
We''re unleashing the power of data for good…
Each time someone shares their experience, they''re helping the next person diagnosed learn what could really work for them, and helping researchers shorten the path to new treatments. We call it data for good — and here''s how it''s making a difference for patients and the healthcare industry:
PatientsLikeMe refuted the results of a clinical trial on the effects of lithium carbonate on ALS, marking the first time a peer-to-peer network was used to evaluate a treatment in real time.
Since 2016, our patient-reported data has been helping the FDA gain new insights into drug safety.
A study in Neurology shows that after using PatientsLikeMe, veterans living with epilepsy improve their self management and self efficacy.
…by empowering people to take control of their health…
But don''t take it from us — our members share in their own words how they learn, connect with others, and track their progress on PatientsLikeMe, and how that''s helped them live better, together.
"I researched other people who have gone through the clinical trials I was going to be doing, and I was able to make an informed decision… You feel less alone on the site, like you''re not the only one going through this."
Laura, living with IPF
"I''ve learned a lot from what people''s experiences are…It''s very helpful to share our stories and our data, because it''s the only way we''re going to find a cure."
Ed, living with Parkinson''s Disease
"I learned that every spring I have an episode…so now my doctor and I start preparing for whatever''s coming. It''s been a long time since I had any episodes in March because I''m prepared. I have that data to link all the pieces together."
Allison, living with bipolar
…because we believe real-world evidence can change the healthcare system
PatientsLikeMe members aren''t just transforming their own lives, they''re helping to transform the very system that serves them. We analyze their data and experiences to create a new kind of medical evidence and truer picture of human health — and then we bring that evidence to our partners to help them fill the gaps in how they treat their patients.
From pharmaceutical companies to research institutions, we team up with leaders in the industry to bring the patient perspective to their products, services, and care.
AstraZeneca uses PatientLikeMe''s global network to support patient-driven research initiatives.
Takeda Pharmaceuticals is partnering with PatientsLikeMe to find new ways to transform medical research and healthcare decision making to improve patients'' lives.
We''re teaming up to give patients and researchers a more complete picture of patients'' experiences with cancer treatments.
And it all started with Stephen — here''s our story…
PatientsLikeMe was born out of frustration. When Stephen Heywood was diagnosed with ALS in 1998 at the age of 29, his brothers Jamie and Ben tried to treat his symptoms and slow his disease as it progressed. But finding information to guide their decisions was time-consuming and difficult.
Jamie and Ben, along with family friend Jeff Cole, initially launched PatientsLikeMe to connect ALS patients, but it quickly expanded, and in 2011, we opened the website to all patients and all conditions. Today, 600,000+ people use PatientsLikeMe to report on their experience with 2,800 conditions.
They''ve generated 43 million data points about disease, creating one of the largest repositories of patient-reported, cross-condition data available today. It all started with Stephen, and now it''s helping many more improve their outcomes, and doing a whole lot of good.
', '2018-12-07 19:26:27');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4706, 'Contact us
Thanks for your interest in Provenance. Please fill out the form below and we’ll make sure your request finds its way to the right person.
Required fields are marked with an (*)
What would you like to contact us about? * 
Request a demo
', '2018-12-07 19:26:29');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4709, 'We’re Hiring
Northern, Central, and Southern NJ
Project 99 is currently seeking culturally competent professionals with expertise and experience, to provide In Home Counseling Services to children between the age of 5-18 with behavioral and emotional needs. Services are specific, outcome-oriented interventions designed to augment other mental/behavioral health services included in a child’s comprehensive plan of care. These interventions are designed to target specific behaviors that are interfering with a child/youth’s functioning. Services are provided in the beneficiary’s home or other community setting aimed at averting admission or readmission to residential mental/behavioral health treatment programs or inpatient psychiatric settings.
Project99 has an exciting, challenging and rewarding work environment that allows excellent potential for professional growth as well as an outstanding compensation and benefits package including performance incentives, health & dental insurance.
Positions Available
In Home Therapists: (Part Time & Full Time) NJ licensed therapists/counselor to provide in home therapy and evaluations. Must have demonstrated experience with child/adolescent behavioral health must have good work ethics, good communication skills and well organized; capable of producing comprehensive documentation.
For more information about how you can join the Project 99 team and impact your community please call (973) 565-9199.
All applicants must be 21 years of age or older and have no history of abuse, neglect, or exploitation of children or vulnerable adults. At least one year of documented experience with children, preferably with behavioral, emotional, or learning issues. Must have reliable transportation, valid driver’s license, and acceptable driving record within company insurance guidelines. Must complete criminal background checks.
', '2018-12-07 19:26:29');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4709, 'Now Hiring
About us
Project 99 was founded in January of 2004; by two dedicated men, Zachary Simmons-Glover and the late Patrick Pasteur, who graduated high school in the year 1999. Realizing the importance of success through leadership and positive partnerships these gentlemen made a vow that they would dedicate their lives to developing innovative programs and services that would bring change to disadvantaged communities. In keeping to their pledge: Project 99 has been marked as their initial stride toward uplifting communities one family at a time.
', '2018-12-07 19:26:29');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4709, 'ESSEX, HUDSON, MIDDLESEX, UNION, PASSAIC, BERGEN, MONMOUTH, CAMDEN & OCEAN
ABOUT US
Project 99 was founded in January of 2004; by two dedicated men, Zachary Simmons-Glover and the late Patrick Pasteur, who graduated high school in the year 1999. Realizing the importance of success through leadership and positive partnerships these gentlemen made a vow that they would dedicate their lives to developing innovative programs and services that would bring change to disadvantaged communities. In keeping to their pledge: Project 99 has been marked as their initial stride toward uplifting communities one family at a time.
 
', '2018-12-07 19:26:29');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4725, 'Join us
The Civio Foundation (Fundación Ciudadana Civio) is an independent, non-profit organization based in Spain which monitors public authorities, reports to all citizens and lobbies to achieve true and effective transparency within our institutions.
We work for the following: to achieve free access to the information generated by them; to understand how they make decisions; to have public policies based on evidence; to succeed in the public sector being held accountable; and to facilitate action and public participation.  The levers we use to achieve these changes are journalism and innovation.
Our mission is to bring an end to opacity in public affairs, informing citizens and using innovation to freely access the information that matters.
These challenges suppose profound changes within our institutions. Opacity puts their efficiency and democratic health at risk. There are certain changes which should take priority and, since the end of 2011, we have been working successfully to achieve them.
We are a multi-disciplinary team of 9 (journalists, computer programmers, and experts in communications and marketing, institutional relations and business development) with international patronage and an advisory board committed to improving democracy.  Although we do (increasingly) work on international projects, the focus of our activity is on Spain, and on our headquarters of Madrid.
What do we do?
The main areas we investigate and lobby on are:
Transparency , access to information generated by public institutions and how these are held accountable for the decisions they make.
Public procurement , its workings, irregularities and the measures necessary to halt corruption.
Networks of power and influence, conflicts of interest, revolving doors and good governance.
Access to healthcare and the relationship between pharmaceutical companies, doctors, associations and public entities.
The correct functioning of the justice system , its independence - or not - and inequality before the law, the granting of pardons and the strengthening of the State of Law.
Management of public spheres , the right to know what every single euro is spent on, and how the problem of forest fires is being dealt with.
How do we do this?
We undertake journalistic investigations and create innovative tools which give you access to unpublished data on the topics we specialize in.  Among other El BOE nuestro de cada día (Our Daily Official Gazette), Medicamentalia , El indultómetro (the Pardonometer), Quién cobra la obra (Who’s paid for the work?), Dónde van mis impuestos (where do my taxes go?), Quién manda (Who rules?), España en llamas (Spain in flames) or Tu derecho a saber (Your right to know).
What makes us different? Our credibility.  We only deal with facts which can be proven and in our specialized subject areas.
We view the information we generate as a service in the public''s interest and our main commitment is to the truth.  We bring you the most relevant information on governance with data, content and a highly specialized expertise.  We never publish anything that we can''t verify ourselves. Every year , almost one million people browse and use our projects to learn about the decisions and facts that affect them, and then act accordingly.  We are passionate about data that tell stories, and some of those we have told have received the top awards in international journalism , such as the Gabriel García Márquez prize for innovation, or the Best Investigation of the Year at the Data Journalism Awards.  We are proof that journalism can be done differently, brilliantly, and from the sidelines of the mainstream media. And more importantly: journalism that generates change.
The information we generate empowers citizens in their decision making and allows for the scrutiny of public administration.
Changes don''t happen on their own however, hence we lobby a lot . Our relationship with governments and public authorities is collaborative, transparent and of public knowledge, including our meetings to lobby for improvements to laws and policies. We have been key to the implementation of the first transparency policies in Spain and to the acknowledgement of the Right of Access to Information. Two of our current priorities are making the activity of lobbies more transparent, and public procurement in general, and we are already winning the fight.  Today more than ever, if people in our institutions are talking about how to publish the budgetary execution in the most detailed way possible, or how they can find out who senior officials are meeting with, who influences one law or another, of the opacity in the public procurement of medicines and vaccines , of the need to reform the Law on Pardons , among others, it is thanks to our hard work.
We are staunchly non-partisan . Any administration with a firm commitment to the disclosure of public information can count, if it so wishes, on our active collaboration. Over 20 public entities, of all political leanings, already do so. That is why around 17 million citizens can now find out what policies each euro of their taxes is spent on, thanks to our collaboration with the public sector .
More and more people are helping us to monitor and bring transparency to other blind spots in the system," This would not be possible without your support.
The sum of our work has given rise to a series of victories and changes of which we are particularly proud. We hold ourselves to the highest standards of ethics, transparency and good governance - we publish all o ur accounts, salaries and our annual report in detail. None of this would have been possible without the support of our donors , who make regular donations that allow us to continue to serve as an effective agent for change, for the benefit of all.
Civio has played a crucial role in this nascent phase of transparency in Spain. Now is the time to affect long- lasting change. There is much still to learn. The challenges are great and we are needed now more than ever. Join us .
We need your support
', '2018-12-07 19:26:31');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4725, 'Jacobo Elosua
Founding Trustee and President of Civio
The entrepreneur behind projects with a marked social angle such as Iris AI and Civio. After starting his working life at UBS Investment Bank where he became Executive Director, Jacobo returned to Spain in 2005 and has been involved in a large number of projects related to digital technology and citizen activism.  Prior to co-founding Civio, in his civic role he participated in Citizens’ Teams, Pro Bono Publico, Open Data of Spain, Cambiemos la Ley Electoral and Voota - an association that developed an innovative open platform to allow citizens to digitally connect with candidates and elected officials.
Jacobo has three degrees, in International Business Administration by ICADE (E-4), and in Economics and Law by UNED. In 2015 he was selected and received a scholarship to attend the Graduate Studies Program at Singularity University, at NASA Ames. Jacobo was born in Vigo in 1974 and apart from his native Spain, he has lived in the US, UK, Ireland and Germany.
David Cabo
Founding Trustee and Executive Director
BS in Computer Science and BA in Psychology, David founded Civio in 2012, and currently serves as Executive Director and lead developer. For 12 years he worked as a consultant, developer, and software architect for companies such as British Telecom, HM Revenues and Customs, Accenture, Ericsson and BBVA Global Markets. He is an expert in open data, public data analysis and budget data, among other fields.
David also lectures on the post-graduate courses offered by Spain’s leading media (El Mundo and El Pais), as well as in numerous workshops and conferences related to civic technology and data journalism. Before founding Civio, David co-organized the largest open data hackathon in Spain, Desafio Abredatos, launched the pro-transparency initiative #adoptaundiputado and collaborated with investigative journalists on the extraction and analysis of public data (Looting the Seas, ICIJ). He has also worked with Access Info Europe and mySociety in the development of the European web portal AsktheEU.org.
Rodrigo Tena
Boardmember
A notary of Madrid by exam, Rodrigo has served as an associate professor of Civil Law at the University of Zaragoza, Visiting Lecturer of Civil Law at the Universidad Complutense de Madrid and associate professor of Documentary Law at the Juan Carlos I University of Madrid (2000-2001). He is an editorial board member of the magazine El Notario del siglo XXI, patron of the Matritense Foundation of Notaries and the Coprodeli Foundation. He has published a large number of works (monographs, articles in specialized journals, jurisprudential publications, etc.) both in Civil, Mercantile, Mortgage and Notarial Law, alongside regular contributions to the newspapers El País, El Mundo, Cinco Días, Expansión and the magazine Claves de Razón Práctica. He is the author of the essay Ocho minutos de arco - Essay on the Political Importance of Moral Archetypes, published by Antonio Machado Books. He is co-editor of the blog ¿Hay Derecho?, was a member of UPyD’s board of directors, and provides technical advice to the Ciudadanos party in the ambit of ​​Justice.
Javier de la Cueva
Boardmember
Javier holds a degree in Law and a PhD in Philosophy from the Universidad Complutense de Madrid, and works as a lawyer. He has served as defense on numerous cases related to the use of licenses which are free from intellectual property rights, and of different technological platforms. He is a Professor of Intellectual Property on the Musical Creation degree course at the European University of Madrid, and of Information Law: Media Regulation on the Bachelor’s Degree in Communication at IE University.
Alongside his work as a lawyer, he is currently busy planning  various technological projects, lecturing and writing about his specialization. He has been a GNU / Linux user since 1998 and has been a system administrator for the operating system since 2003. He writes his scripts in Python and enjoys n3 notation for modeling semantic web ontologies.
Marisol García
Boardmember
Executive Director of the Hazloposible Foundation (“Make it Possible Foundation” in English) since 2007. She started her professional career as a consultant at Accenture where she carried out process reengineering and management software implementation projects for large companies. This vocation for social matters made her move into the nonprofit sector in the year 2000, joining Hazloposible in order to leverage Internet as a tool to facilitate volunteering in Spain. This is how Hacesfalta.org (“You are needed”, in English) was born. She later transferred the idea to the business world, creating a corporate volunteering program currently in place in companies representing 50% of the Ibex 35 market capitalization.
She holds a degree in Economics and Business Administration from the University of Alicante, an extraordinary end-of-career award and a postgraduate degree in leadership and innovation in NGOs from ESADE.
José Luis Marín
Boardmember
José Luis Marín holds an MSc in Telecommunications Engineering and a Diploma in Business Administration from the University of Valladolid. His career has been developed in the company Gateway S.C.S. (owner of EuroAlert.net), of which he is a partner and director. EuroAlert is currently building a pan-European platform capable of adding public tenders worth 18% of EU GDP, to create valuable services for companies and organizations around the world.
He has been an expert evaluator of Research and Innovation projects for the European Commission for more than 5 years and has also directed more than 10 R&D projects on Internet-associated technologies. He is the author of several publications such as the book “Web 2.0: A simple description of the changes we are living” and co-author of “Open Data: Reuse of public information”. He regularly participates in initiatives related to the promotion of open source software, innovation and free and open knowledge, such as those related to the Open Data movement.
Olivier Schulbaum
Boardmember
Co-founder of Platoniq , an organization of cultural producers and software developers, and vicepresident of the Fundación Goteo . From there, he co-directs Goteo.org , a social network for collective financing (crowdfunding) and distributed collaboration for projects whose aims are social, cultural, scientific, educational, technological or ecological.
Since 2001 he has carried out actions and projects in which technology is applied to the promotion of communication, self-training and citizen organization. His projects include Common Knowledge Bank, a knowledge and education exchange platform; the OpenServer public streaming server; the temporary media laboratory, Media Space Invaders; and the creation of the first copyleft license in the Spanish legal framework, Unconditional Air. In addition, Mr. Schulbaum has won several awards for his project Burn Station, a copyleft music copying station.
Advisory Board
Nathaniel Heller
Nathaniel is Executive Director at the Results for Development Institute , where he is responsible for the Good Governance program. Prior to joining R4D, Nathaniel co-founded and led Global Integrity , an NGO that adopted rigorous research and new technologies to foster transparency, good governance, anti-corruption and accountability. He was also one of the drivers of the OpenGov Hub , the first open co-working community among Open Government specialists, based in Washington DC and Kathmandu, Nepal. Previously, Nathaniel worked for the State Department, was a fellow of the Center for Public Integrity, and advised Senator Edward Kennedy on foreign policy.
With a degree in International Relations and Spanish Literature from the University of Delaware, and an MA from the School of Foreign Service at Georgetown University, Nathaniel is currently the representative for civil society on the Executive Committee of the Open Government Partnership .
@integrilicious
Víctor Lapuente
Victor Lapuente holds a PhD in Political Science from the University of Oxford and currently teaches and researches at the Government Quality Institute of the University of Gothenburg (Sweden).
His main interests are the analysis of public policies, the operation of administrations from a comparative point of view, and the causes and consequences of corruption. He advocates a thorough approach to these topics, one strictly based on data and evidence. He often writes about these subjects in publications such as El País.
@victorlapuente
Luis Martín Cabiedes
Luis Martín Cabiedes is one of Spain''s leading investors specializing in technological companies in the launch phase. With his vast experience and broad portfolio of operations, he is an icon in the field. First as ''business angel'' and later on with his own venture capital fund, Cabiedes & Partners, Luis has invested in over sixty Internet and technology firms, from Privalia to Blablacar, Trovit and many more .
Since 2008, Luis has been a part-time professor at IESE, where he teaches Entrepreneurship and Business Finance. For 12 years he co-chaired the Europa Press Group, which he joined in 1989. He has a degree in Philosophy from the Universidad Complutense de Madrid, an MBA from IESE, and is an accredited financial analyst of the CFA Institute.
@luismcabiedes
Momi Peralta
Angélica -Momi- Peralta Ramos ( @momiperalta ) is from Argentina and heads the Multimedia Development and Data Journalism unit of the prestigious newspaper La Nación . Her work in the specialized field of data-based research is internationally recognized and has been repeatedly awarded .
Momi is in charge of leading the transformation of La Nación into a multiplatform outlet, fostering innovation in digital formats, as well as areas of journalistic coverage with mobile devices, social networks and the specialized data unit ( LNData ). In this respect, Momi plays a key role in the internal training of newsrooms, the development of news apps (such as Open Affidavits , VozData or the Argentine Census Project ) and collaboration between data journalists, open data platforms and community transparency.
Momi holds a degree in Systems (CAECE), a Master’s Degree in Business Administration (IAE Universidad Austral), and a postgraduate degree in Project Management (UBA).
@momiperalta
Chris Taggart
Chris Taggart ( @countculture ) began his career as a journalist and editor on UK magazines. His move to consultancy and activism in open data and financial economic transparency began with OpenlyLocal , a platform to make local government data more transparent and accessible. Later, Chris founded OpenCharities with the aim of opening data for NGOs and charities. In 2010 he created OpenCorporates , his flagship project: the largest open database in the corporate world, with information on nearly 85 million companies in more than 70 jurisdictions.
A world-leading international expert in economic-financial transparency and accessibility of trade registers, Chris collaborates via OpenCorporates with governments and institutions, such as the World Bank, to increase the opening of corporate data and the quality of this. His database is a go-to resource for both journalists and researchers of the corruption phenomenon, as well as banking and the world of finance. Chris also advises governments on transparency, and was the driving force behind the Open Election Data Project .
@countculture
Helen Darbishire
A human rights activist with more than 20 years experience in promoting the right to access information at an international level, Helen has helped in the drafting and implementation of Access to Information laws in Europe, Latin America and Africa. Prior to founding Access Info Europe in 2006, Helen worked with Article 19 (London and Paris, 1989-1998) and the Open Society Institute (Budapest and New York, 1999-2005), and has acted as an advisor to several intergovernmental organizations, including UNESCO, the Council of Europe, the OSCE and the World Bank. She is the founder of the transnational organization Freedom of Information Advocates Network, which she directed from 2004 to 2010. She has a BA in History and Philosophy of Science from the University of Durham, UK.
@helen_access
François Derbaix
A Belgian entrepreneur based in Madrid since 2000, Françoisis an investor in web startups. An active defender of transparency, he believes that the best way to reform governance is to make conflicts of interest public, and to prevent corruption. In 2009 he was one of the creators of the Voota association (now defunct), among whose goals featured the promotion of citizens’; participation in politics, the strengthening of democracy in Spain, and the renewal of Spain’s current political system.
Derbaix graduated in Management Engineering from the Catholic University of Louvain and co-founded the holiday rental sites Toprural (2000) and Rentalia (2003), which were sold in 2012 to HomeAway, global market leader in the sector, and idealista.com respectively. Currently he participates in more than 15 initiatives, among others: 11870.com, Vinogusto.com, SeedRocket, the Internet Investors and Entrepreneurs Association (AIEI) and the venture capital fund Bonsai Venture Capital.
', '2018-12-07 19:26:31');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4745, 'Welcome to a world of co-creation  
Stop talking. Start acting.
ReDI School of Digital Integration was founded in February 2016 by a team convinced that technology can bring people together to build new solutions to old problems. 
​
We believe integration starts with a "Hello" - People meeting around a shared interest.  Indeed, the idea of ReDI School was sparked by a conversation on a warm August day in 2015 in a refugee home in Berlin. Here, we realized that amongst the newcomers there are incredible IT-talents eager to learn, who want to contribute to Germany''s society and who could help fill the 51.000 open IT-jobs in Germany.
 
From Insight to Action
From the beginning, our school has been developed through co-creation between the Berlin and München tech community and our students. Since then, we''ve grown into a non-profit social enterprise offering several 3-months IT-programs, workshops, corporate training projects as well as short term summer courses. We have also helped incubate 7 start-ups! 
​
Our aim is to provide our students with valuable digital skills and a strong network of tech leaders, students and alumni to help create new opportunities for all.
WANT TO SPREAD THE STORY?
Great! We can''t wait to talk to you. In the meantime, download our presskit and send us an email.
', '2018-12-07 19:26:31');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4760, 'What we do
What we do
We intend to provide computers and free Internet access in refugee accommodation facilities. In line with demand, we also provide training courses for the refugees about the usage of computers, smartphones and the Internet.
As our first project for 2015, we chose the refugee reception unit at the Fliegerhorst in Fuerstenfeldbruck. We set up a computer room and are offering courses there on subjects such as usage of computers and internet, tablets, smartphones etc. Our purpose here is to convey basic knowledge about these things, which should help refugees to use these technologies accordingly to their full potential. We do not follow a strict curriculum here, because in reception units like this, residents change constantly and each course, the previous knowledge of our students differs. As far as possible, we motivate qualified refugees to become a trainer. That way, we can sometimes offer courses in other languages than just in English.
 
 
Inside the previous casern, we built up a substantial wireless-LAN infrastructure, so the 1.600 residents have internet access on many central locations of the facility.
Our second project, a community home in Germering, started in May 2015. At this time there were around 100 refugees at this facility, now the number has grown to 300. The internet access has been up and running since the end of May and, together with AsylPlus, we set up a computer room with a few desktop computers. The access here – as well as in Fuerstenfeldbruck – is managed through a professional wireless-LAN hotspot system.
By now, we are actively working in around 160 facilities and each week, new projects are coming in. We always work jointly with the helpers associations on-site, who handle installation and organization, whereas we are responsible for provision and configuration of the wirless-LAN hotspot systems.
We have worked out several technical recommendations that we would like to pass on. Please use our  contact form for this purpose.
You may also require a copy of our annual report.“
', '2018-12-07 19:26:32');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4760, 'Who we are
Who we are
The association has been founded by people who care about the destiny of refugees in Germany. These people have decided to actively help the association by fundraising, collecting donated devices and supporting its activities by providing training courses to refugees.
Thank you very much for your help!
We believe that access to the Internet is essential for refugees to stay in touch with their families and get access to online information. It helps to cope with the situation being away from their home countries and their families.
Please allow us to introduce your contact persons:
Volker Werbus
Volker Werbus is an engineer (Communication Engineering) and currently he assumes the position of the Managing Director at Green Digit GmbH, a company providing engineering services in the field of Embedded Software. The company is located in Gilching, close to Munich.
Volker was Chief Technical Officer (CTO) with a commercial WiFi provider between 2003 and 2005 and thus he gained a lot of experience in the area of providing Internet access.
Helga Werbus
Helga Werbus was working in the food retailing industry before the two kids were born. She is supporting her husband in the charity association.
', '2018-12-07 19:26:32');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4830, 'Who are we?
Frank Kashner
I recently retired as a collaborative, critical, and community-action oriented psychotherapist on the North Shore of Boston, MA, USA , also working to organize Massachusetts independent mental health workers through Clinicians United – SEIU Local 509  and promoting the Says-Us project proposal .
In the 1960’s, I was part of the Wisconsin Draft Resistance Union, Students for a Democratic Society, and the (successful) Cambridge Rent Control Referendum Campaign.   In the 1970’s I became a machinist at General Electric and an activist in IUE Local 201 in Lynn, MA where I participated in successful efforts to end General Electric practices of open racism, to win greater worker safety, and to make the union leadership more democratic.   See “A Rank & File Strike at GE” in the 1978 issue of Radical America.
19 years in Massachusetts software engineering organizations, most of the time as a manager, allowed me to participate in the development of technology and witness its liberatory potential.  I practiced  collaborative leadership (linked-in profile)  while navigating and learning from organizational politics.
In 2010, while researching why there were so many problems with social service organizations, I was directed to jobvent.com by an unhappy employee of South Bay Mental Health .  On jobvent I saw the passionate complaints of almost 100K employees about almost 10K companies, and glassdoor’s subsequent betrayal of those employees. The use of jobvent, my activist history, the stories of my clients, and my belief in the liberatory potential of the web, led me to conceptualizing and promoting the need for says-us.
—————————————–
————————————–
Next Steps
 
“If Universal Health Services is treating the patients under its care with dignity and respect, then why would it prevent caregivers from talking to the media?’’ union executive vice president Veronica Turner said in a written statement. “It raises serious questions about what the company is trying to hide.’’   (see the post Frank Barnes and SEIU 1199)
Posts
', '2018-12-07 19:26:39');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4871, 'Share N Save is a crowdsourced map of initiatives and skills in South Australia that helps people use less resources through better sharing of what we already have.
About
', '2018-12-07 19:26:40');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4938, 'CONTACT US
SOCRATIC PLATFORM
SOCRATIC will be a Knowledge-based Internet Platform offering a set of tools and Services to support the whole Social Innovation Project life cycle from problem identification and awareness and crative solution ideas to collective decision-making, design and implementation of the best ideas.
An integrated solution for the innovation process rather than separate items or services, enabling agile social innovation processes. Each activity will be supported by a specific service in the platform. SOCRATIC will also implement a Global Observatory about Sustainability challenges.
The main goal of SOCRATIC project is to provide citizens and organizations a collaborative space where they can identify innovative solutions to achieve the Sustainable Development Goals set by the United Nations.
SOCRATIC – SOcial CReATive Intelligence for achieving Global Sustainability Goals
The main objective of SOCRATIC is to facilitate a platform so citizens and/or organisations can collaboratively identify specific innovative solutions for achieving the desired Global Sustainability Goals, as defined by the United Nations. SOCRATIC will also implement a Global Observatory on Sustainability Challenges with a double objective: (1) Measuring the impact of SOCRATIC actions on Global Sustainability Challenges by monitoring social networks, (2) Using the data about Global Sustainability Challenges gathered in social media as a source of information to launch challenges in the SOCRATIC platform.
The SOCRATIC project has a deep user-centric approach, implementing gamification techniques to engage users in the sustained use of the platform. The project involves one European NGO (CiberVoluntarios) and a group of Young Social Innovators from the “Experts in Team” program of the Norwegian University of Science and Technology (NTNU-EiT). Both collectives perform actions in different fields, but with a common tool, the use of IT to empower citizens and achieve specific sustainability goals. The pilots will be initially focused on three specific challenges: “Ensuring healthy lives and promote wellbeing for all at all ages (UN´s Goal 3)”, “Ensuring inclusive and equitable quality education and promote lifelong learning opportunities for all (UN’s Goal 4)”, and “Promoting sustained, inclusive and sustainable economic growth, full and productive employment and decent work for all (UN’s Goal 8)”.
The SOCRATIC’s platform will be used by citizens and organizations to
to propose new challenges oriented to solve specific sustainability issues.
to invite individuals or organizations to participate with innovative ideas that solve these issues.
to collectively select and implement the most promising ideas.
Challenges are the targets that United Nations has set in order to achieve its Global Sustainability Goals (GSG). This village will identify challenges and propose solutions regarding the UN’s sustainability goal number 3 , which is “Ensuring healthy lives and promote well-being for all at all ages”.
', '2018-12-07 19:26:44');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4945, 'What we will do
Report on practices and competences in dealing with bullying in school communities
This analysis will present a complete picture of school bullying (the literature and news items in school bullying, real life cases, best practices) in the participating countries and will outline the needed competences that professionals in school communities need to have so as to deal effectively with this problem.
Peer learning methodology for dealing with school bullying
The proposed project targets all involved actors of the school community that are affected by bullying. Its principal aims are to raise awareness, but also to create a safety net in the form of a rich repository of experiences that are exchanged by members of a community of peers. The project will invest in recording, classifying and making available the experience of the school community members, thus contributing to the formation of a collective memory and set of contextualized tools.
Learning objects
The peer learning of the participating actors will be achieved with the help of digital content developed in most part by the actors themselves. This content will serve as an online, continuously accessible repository of knowledge stemming from real life situations and experiences of the actors. The production of the learning objects constitutes the main peer learning activity. Moreover, access to the learning objects will be easy and direct using a tag cloud. All learning objects will be made available under Creative Common license.
Peer learning platform
This output is dealing with the design, creation, implementation and evaluation of the training platform that will provide teachers, teacher trainers, parents and all other school professionals and stakeholders access to training material on school bullying, as well as the means to publish their experiences and to comment and tag the experiences uploaded by their peers.
Exploitation Guide & tools
This output is an exploitation Guide which will present the exploitation results together with the main findings and conclusions of the project, as well the creation of project site which will act as the mail diffuser of the produced results.
Select Language:
', '2018-12-07 19:26:44');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4945, 'Hellenic Open University-(HOU)  , Greece
The Hellenic Open University (HOU) is a Public University founded by the Greek Government in 1992 and fully operational since 1997. Its prime mission is to offer university level education using distance learning methodology and to develop the appropriate material and teaching methods. An equally important aim is the promotion of scientific and applied research in the field of distance and lifelong learning. The Hellenic Open University (HOU) evolve around the following axes:
The creation (development, production, publishing), access, usage (retrieval and sharing) and maintenance (storage, archiving, adaptation) of educational content and material (in printed or digital format),
The development and evolution of distance learning methodologies, by exploiting the contemporary educational and pedagogical theories, as well as the technological developments that ensure their efficient application, and
The development or application of software and other Information Society Technologies (IST) applications to support education
The staff of HOU has participated in various R&D projects, in the context of which it has developed job profiles, training guidelines, platforms, numerous software tools, databases and educational content and material. As a result, they have developed state-of-the-art know how in distance and lifelong learning methodologies and technologies.
For more information you can visit the official website of Hellenic Open University www.eap.gr .
Initiatives for efficient training ( INFOREF)  , Belgium
INFOREF (Initiatives pour une Formation Efficace / Initiatives for efficient training) is a Belgian non-profit organisation whose aim is to promote an educational
use of ICT in education and training. The organisation has acquired experience participating in several European projects related to education and early school leaving,
including the project “I Am Not Scared” on school bullying.
Computer Technology Institute and Press-( CTI )  , Greece
The Computer Technology Institute and Press (CTI) was founded in 1985 in Patras. CTI is a research and technology organization focusing on research and development in Information and Communication Technologies (ICT). Particular emphasis is placed on education, by developing and deploying conventional and digital media in education and lifelong learning; publishing printed and electronic educational materials; administrating and managing the Greek School Network; and supporting the organization and operation of the electronic infrastructure of the Greek Ministry of Education and Religious Affairs and all educational units. Since its establishment in 1985, and in the past decades of rapid technological development, CTI has actively contributed to many of the advances that today are taken for granted.
The Directorate of Telematics and Applications for Regional development (abbrev. “Telematics Center”) of CTI, participating in this project, was established aimed at enhancing regional development through the effective use of ICT. CTI Telematics Center has significant past experience in the design and development of advanced e-learning services as well as in the effective customization of social networking tools so as to be applied in lifelong learning
Fondazione Mondo Digitale (FMD)  , Italy
Fondazione Mondo Digitale – FMD (www.mondodigitale.org) is a non-profit Foundation founded by the Municipality of Rome and 5 ICT companies. It works for an inclusive knowledge society by blending innovation, education, inclusion and fundamental values. Its mission is to promote social learning, social innovation and e-inclusion, with special attention to categories at risk of social exclusion. FMD promotes the inclusive use of new technology in different sectors: Education for life, Educational Robotics, Active Ageing and Intergenerational Solidarity, Social Innovation and Young Entrepreneurship and Integration of migrants & Refugees. It has promoted didactic innovation through the use of new technology in schools and digital literacy of elders via an intergenerational and peer learning approach since 2002.
FMD Headquarters are located at the Città Educativa in Rome (http://cittaeducativa.roma.it/), a center of best practices and social innovation including the first FabLab in Rome. FMD operates at local, national and international level with an action-research, development and implementation programmes combining theoretical academic work with the development of tools and projects for community building.
ABC – National Anti-Bullying Research and Resource Centre
Dublin City University , Ireland
ABC is a dedicated research facility located in the School of Education Studies at Dublin City University, Ireland. Founded in 1996 researchers at the Centre undertook the first national studies in school bullying, workplace bullying, homophobic bullying and cyberbullying among young people. In 2014, ABC hosted the first national conference on cyberbullying at Dublin Castle where representatives from Facebook, Twitter and other major social networks came together for the first time with representatives of government and researchers to consider issues related to cyberbullying and to develop appropriate responses. Staff at the Centre are currently involved in a number of research projects sponsored by the Government of Ireland and the European Union.
Select Language:
', '2018-12-07 19:26:44');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4946, 'reCAPTCHA: please fill the following text
Follow Sonetor Project
Co-funded by:
DISCLAIMER: The content of this website reflects the views only of the authors, and the Commission cannot be held responsible for any use which may be made of the information contained herein.
Project number:
', '2018-12-07 19:26:44');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4946, 'Contact us
Project description
SONETOR aims to enhance the knowledge, skills and competencies of Cultural Mediators through the development of innovative Vocational Education Training content and programmes. In addition to the formal modules the recognition of informal and non-formal learning will be a key element of the project.
It will firstly study the actual professional needs of Cultural Mediators and draft a job profile, listing the necessary, knowledge, skills & competences they should possess. Based on these, the project will develop a set of learning scenarios aimed at developing the Cultural Mediator in terms of continuing professional and personal development. A key element of the project is the concept of peer or shared learning and the project will facilitate participants with the opportunity to share real-life experience and adopt best practice.
To this end, the project will develop and deploy a special multi-lingual social networking platform, which will foster the development of a European Cultural Mediators community, by providing a universal focal point for the exchange of working experiences between the community members and other stakeholders. The platform will support peer learning via the realization of training scenarios tailored to the specific job needs and personal attributes required of Cultural Mediators. The project is committed to using techniques and learning strategies which are recognised to be in line with the best practice and principles of adult education and will make use of high quality digital training content, as well as of communication tools, such as forums, blogs and wikis, developed and maintained by the registered members of the community. The training services will be accessible via the Internet, thus supporting distance learning in the workplace or at home making the product a very flexible one.
SONETOR will become the vehicle of choice for improving the quality of services provided Cultural Mediators and their organisations through up-skilling and re-skilling. It will promote innovation in vocational training of Cultural Mediators through the social networking training platform. The primary products of the project (job profile, training content and scenarios, social networking platform) will ensure that the material is available to as wide an audience as possible in what is a fast changing educational landscape.
 
Follow Sonetor Project
Co-funded by:
DISCLAIMER: The content of this website reflects the views only of the authors, and the Commission cannot be held responsible for any use which may be made of the information contained herein.
Project number:
', '2018-12-07 19:26:44');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4957, 'Contact us!
Who is this for?
 You have gone through our Frequently Asked Questions (FAQ)  , Overview , and Help sections to find an answer to your question but to no avail!
Good news! We have other options to help you!
Tell us about your problem, querie, or suggestion using the following routes:
The form in this web page
Sending us an email at spatialhub@improvementservice.org.uk
Calling the Improvement Service business support at +44 (0) 1506 282012
If you are a human and are seeing this field, please leave it blank.
Full name *
Email *
Message *
Check below to indicate that you have read and agree to the terms of our privacy policy
I agree *
This website makes use of cookies to enhance browsing experience and provide additional functionality. We''ll assume you''re ok with this, but you can opt-out if you wish. Accept Reject Read More
Privacy & Cookies Policy
', '2018-12-07 19:26:44');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (4991, 'YOUR MESSAGE
SUBMIT
Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat
ABOUT US
Student Pulse is a discounted ticket and loyalty scheme for classical music concerts, run jointly by 11 of London''s best orchestras and venues. The program allows you to purchase cheap tickets and accumulate points which can then be redeemed for great rewards. The Student Pulse app, available for both iPhone and Android, as well as this website allow you to purchase tickets from a number of different organisations and venues, all from the same place. Read more.
', '2018-12-07 19:26:45');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (5060, 'Contact us
Contact info
If you have any questions about any of the training courses, please feel free to contact us using one of the methods below:
Tradelocks
Unit 1 Albert Close Trading Estate,
Whitefield,
', '2018-12-07 19:26:45');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (5060, 'About us
Who We Are
Based in Whitefield (just outside of Manchester), Tradelocks delivers hands-on intensive auto locksmith courses for those who wish to learn the craft themselves and become a qualified auto locksmith.
Our brand new dedicated locksmith training school will introduce you in the World of auto picking, opening, bypassing a variety of different car locks, as well as auto key cutting, key programming and Eeprom work.
If you attend a course, we will help you with your marketing side using our in house marketing team. This will include specialised leaflets and business cards (if you have a company logo we will put this onto the artwork for you), and we will give you all the artwork to take away and use. We will also give you a FREE guide on how to promote yourself online with websites, social media and paid for advertising.
If you book on the 8 day auto course we will even help you design a brand new company logo to help promote yourself, as well as print out 48 A5 leaflets and 50 business cards for you and provide a van sticker!
', '2018-12-07 19:26:45');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (5061, 'Sign In
The Ground_Up Project is the deal-sourcing platform for sustainability.
We are a financial technology company and a leading platform for impact ventures looking for funding under US $20M.
Join to assess your investment-readiness using the free Value Compass tool and to be seen by investors on the platform.
REGISTER NOW
ABOUT THE GROUND_UP PROJECT
The Ground_Up Project is a deal-sourcing platform for sustainability. As the leading platform for impact ventures looking for under $20M, we achieve scale through our proprietary Value Compass, which analyses and scores projects, and aggregates portfolios to match investor requirements.
WHAT WE DO
In order to maximize opportunity and impact for entrepreneurs, investors, and resource-providers, we have designed a platform to:
Standardize and validate information about small impact ventures and their impact
Provide insights into business risk and reward
Visualize trends and aggregated data
Create collaborations among users
WHY WE DO IT
While small and medium-sized enterprises (SMEs) might be individually small, collectively they represent more than 95% of the world''s businesses, and have a huge untapped potential to improve the environment and create sustainable livelihoods. That’s why we focus on small impact ventures. These are for-profit businesses with a clear, positive social and environmental impact in any sector, in need of under 20 miliion USD. For examples ranging from organic farming to clean energy, please see The Ground_Up Project Blog .
About The Ground_Up Project
The Ground_Up Project is the deal-sourcing platform for sustainability. We''re a financial technology company and a leading platform for impact ventures looking for funding under US $20M. We achieve scale through our proprietary Value Compass, which analyses and scores projects to match investor requirements.
Subscribe to Mailing List
', '2018-12-07 19:26:45');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (5061, 'Message
About The Ground_Up Project
The Ground_Up Project is the deal-sourcing platform for sustainability. We''re a financial technology company and a leading platform for impact ventures looking for funding under US $20M. We achieve scale through our proprietary Value Compass, which analyses and scores projects to match investor requirements.
Subscribe to Mailing List
', '2018-12-07 19:26:45');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (5165, 'Tomorrow is being built here and now. Log in
About us
The GROUPE SOS launched a new kind of social network dedicated to change-makers: UP Campus Supported by the UP community, it aims at creating links between people who share the idea that we can (re)invent tomorrow world. A specific network at last created for people who want to give a hand or just get inspired by innovative solutions. It works thanks to a matching system based on your needs and on the possibility to meet people attending to the same events as you. So now, play on: open the discussion and bring your ideas to live! UP Campus is the "must have" to speed up your projects, expand and build your network, and meet new contacts at our wide range of events: UP Conferences, UP Café, UP Conferences Dreamstorming, UP Pro, afterworks...
Come together
The first social network which enables change-makers to converge. A new way of creating encounters with people from different fields and backgrounds.
(Be) inspire(d)
Take part in our events, get the community involved in your ideas, discuss your projects. Be inspired by members and get them inspiration!
Be involved
You''re planning to start a project? Need a hand or want to lend one? Because you''re unique, UP Campus has developed a custom-built tool so that you can quickly find the right match for your needs.
Impelled in France by the GROUPE SOS,, UP community embraces a new vision of networking thanks to its heritage: a wide range of inspiring events that provide innovative solutions and bring change-makers together with their differing backgrounds. UP Conferences, UP Café, UP Pro, Afterworks... in 2014, more than 12 000 people have joined one of our events.
', '2018-12-07 19:26:48');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (5208, 'Women''s Fashion
About Us
VoucherMedia.com is managed and operated by London Voucher Company Ltd, a company registered in England and Wales with company number 9238718. vouchermedia.com excavates and finds amazing discounts and great deals from major merchants and delivers them to the fingertips of the customers. The Voucher Media website operates as a hub that hosts the elite promotional voucher codes discovered from the World Wide Web and direct household merchants.
Customers are able to use the service completely free with no hidden charges whatsoever. Revenue is accrued from advertising on our website and an agreed (low) commission from the merchant for the use of these codes on the Merchants website. Merchants are able to maximize their sales revenue through advertising that the Voucher Media Site provides whilst also attracting and maintaining new and existing customers to their business through exposure of the unique promotions and deals offered on the Voucher Media site
We are registered in England and Wales under registration number 9238718, and our registered office is London Voucher Company Ltd, 20-22 Wenlock Road, London, N1 7GU.
You can contact us by writing to our Mailing Address-
Voucher Media,16 Sherwood Gardens,London SE16 3JA, use our website contact form or by email to [email protected]
', '2018-12-07 19:26:49');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (5217, 'Wallspot is a system for managing legal walls for artistic interventions that use the public space as a means for creation.
Origin
Wallspot has sprung up from the need for a single, unified way to integrate and bring together all those engaged in urban art: artists, photographers, arts managers and promoters, the general public and the local government.   Committed to becoming an international platform aimed at creators and lovers of urban art, Wallspot’s goal is to bring together all types of professionals engaged in this art form, creating a global urban art community.
Philosophy
We take the public space to be a place for people to gather, to build collectively and for social transformation.  We’d like to foster a social dynamic that brings new cultural values to the city, bridging the gap between the artistic community and society.  Our goal is to support quality artistic creations, open to all types of discipli-nes, under one social philosophy that calls for the active participation of all sectors of society.
The team
We are a multidisciplinary team of pro-grammers, designers, creatives, sociologists, arts managers and curators. This broad professional reach means we can understand and approach projects from different points, providing for an all-round vision. We work with pioneering innovation that unites technology and urban art so that it can gain more momentum and recognition worldwide, creating new channels for public participation. Our goal: transform how we value public spaces from and for society through urban art.
Cultural managers
Rebobinart
Barcelona, España
Somos una plataforma de arte urbano, que creemos en el arte como herramienta de dinamización social y cultural del espacio público.
Incentivamos la participación ciudadana a través del arte en la calle como herramienta para favorecer la cohesión social. El arte se convierte así en una herramienta de mediación comunitaria, consiguiendo una ciudadanía más implicada, poniendo en contacto a grupos sociales diversos, trabajando para afrontar los conflictos vecinales de forma positiva. Apoyamos a los artistas locales, ofreciendo la oportunidad de trabajar de forma profesional y de promocionar sus carreras.
Realizamos proyectos y programas de intervención social a través de la creación artística. Algunos de los proyectos más destacados son Wallspot, una plataforma digital que gestiona espacios legales destinados a intervenciones artísticas, y Ús Barcelona, un festival de arte urbano y espacio público que tiene como objetivo dar nuevas funciones a espacios de la ciudad que están en desuso.
RMUTL
RMUTL
Chiang Mai, Thailandia
The Faculty of Arts and Architecture is one of four faculties that make up the Rajamangala University of Technology Lanna (RMUTL), an educational institution created for Northern Thailand that aims to produce active graduates who are experts in their vocation by integrating learning and teaching with real work experience.
The Faculty of Arts and Architecture offers eignt different undergraduate courses, with majors in Architectire, Interior Architectire, Textiles and Jewelry, Communication Design, Industrial Design, Visual Art, Ceramics, and Printing and Packaging Technology.
As part of the Printing and Packaging Technology program, studentsn are able to develop a full and comprehensive understanding of the processes of packaging design on packaging. The program also encourages students to develop the necessary skills needed to produce original packaging. In 2014, RMUTL''s packaging design students participated in the regional and international pakaging design competition ''AsiaStar Packaging Award'' in the Philippines and won nine separate awards under the category of ''Student Packages''.
NAAH
NotAnotherArtHub
Johannesburg, South Africa
The creation of Not Another Art Hub was to cultivate a market for the urban art culture. To open doors for urban artists to be able to partake in and be a part of art festivals and residencies, even creating our very own. Building a network of artists and connecting them to the transforming of public art in Johannesburg and South Africa. Our main focuses include producing urban art events, exhibitions, festivals and artist exchange residencies, we also aim to grow further into consulting and marketing for urban art and the artists themselves.
We love the idea of having the opportunity as a creative house to interact with other artists, ideas and cultures from around the world.
DAVE
Rewriters010
Rotterdam, The Netherlands
Rewriters is committed to fight for the comeback of street art (graffiti & street art) in Rotterdam. We provide the street artists of Rotterdam with a platform within their own city where they can display their own work outdoors. Some of the artists already gained popularity in several countries and have been asked to create wall paintings and exhibitions throughout the world. Nevertheless these artists are restricted to extremely small areas in Rotterdam whereby their talent remains unknown in their own city. We are here to change this by the start of numerous projects where the local artists can get their recognition in the open air of Rotterdam.
Throwupgallery
', '2018-12-07 19:26:54');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (5246, 'Registration
About us
We are a project team of students belonging to the university group Enactus KIT (Which also belongs to the Enactus Germany network). Enactus is worldwide known organisation, that is engaged in the development of social entrepreneurship. So was the start of our earlier projects that started in January 2014 where we were engaged in several refugee helping projects. Our partner project Flüchtlingszeit is also writing a novel which is a collection of stories and events of some refugees.
The idea for the Welcome2Work project was the result of rigorous brainstorming, of which the needs and support of refugees was highly underlined. The German economy will be heavily influenced in the upcoming years, depending on whether migrants and asylum seekers are integrated and provided with a job here in Germany. As a result, it has become our aim to help simplify and ease the process through which migrants can apply for a job.
The university group consists of around 100 members, fifteen of which who belong to Welcome2Work.
 
', '2018-12-07 19:26:55');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6710, 'Contact Us:
If you have always dreamed of owning your own farm, are interested in learning more about organic farming and local agriculture, or are looking for a great project to get involved with, this group, located in  the Seattle, Washington area, is a perfect place to start.
Voicemail: (206) 834-6179
Email: wannabe@scn.org
Web Site: http://www.scn.org/edu/wannabe/
To join our Listserve: send a message to majordomo@scn.org ,     leave subject line blank, type "subscribe farmers" in the message.
', '2018-12-07 19:27:04');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6710, 'Request a registration form from the Secretary of State .
Find out the deadlines for registering, and other important details.
You are more than TWICE as likely to actually vote if you get your ballot absentee .  Print out the form, check permanent request and mail it, and    you''ll never have to ask again.  Click here to see where to mail it and when.
', '2018-12-07 19:27:04');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6710, 'You can find out about Harvey Muggy Stonewall Democrats in several ways:
General email about this site or for more                 information to glbtdems@scn.org
Email Chair Ken Kadlec at kudd22@hotmail.com
Check back at this website
Write to 1122 E. Pike St. #1196, Seattle WA 98122
Call our voice message line, 206/903-9613
', '2018-12-07 19:27:04');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6710, 'To request a schedule of upcoming performances
and shows or to order an audio cassette please write:
Olga Sukhover
', '2018-12-07 19:27:04');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6710, ' 
 
The idea to organize a professional singing group came in November of 1995, when several Russian families gathered together for a traditional Holiday celebration. They sang popular folk songs, danced and played games.In order to keep Russian traditions and songs alive our Folk group was formed under the directorship of Olga Sukhover. Ms. Sukhover was awarded her Master''s degree in Folk Music from the Moscow Gnesin Institute where she studied Choir Conducting and Piano. She worked for several years with the Children''s Choir in Moscow before coming to the United States.
Now our Russian Folk Group "Khorovod" performs and shares the traditions, folk songs and dances from the different regions of Russia. We have been performing throughout the Puget Sound area. Venues have included the St. Spiridon Cathedral Orthodox Church in America, the Fremont Baptist Church, the Russian Consulate, the Russian club, Everett Community College, Seattle Central Community College, the University of Washington, the Capital Building in Olympia, the Sea-Tac International Festival 1997. Since 1997, we have also been participating in the Fremont Street Fair and the Northwest Folklife Festival.
It is becoming a tradition to celebrate a "Maslenitsa" every year with the students of the Slavic Department at the University of Washington. We are very proud to have their support.
', '2018-12-07 19:27:04');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6710, 'City Links and Our Neighbors
Our location
Westwood Neighborhood is the West Seattle neighborhood between SW Holden Street at the north and SW Roxbury Street in the south, SW 35th Avenue to the west and Delridge Way SW to east. We are our own little "town", with a library, high school, middle school, elementary school, fire department station, medical services, shopping center, Post Office, stores, restaurants, community center with a swimming pool, and athletic facilities (Sealth/Denny Field).
Who We Are
The Council is an open group of neighbors working together to improve and maintain the quality of life in the Westwood community.  Read our online brochure  to learn more about the Westwood Neighborhood Council.
Neighborhood Plan
We are currently early in the process with the City of Seattle working through various community meetings updating the 1999 Neighborhood Plan. Neighborhoods are often grouped together that share a common boundary. We share our neighborhood plan with our neighbors to the east in Highland Park . Neighborhood plans identify to the city our focus for development, traffic, pedestrians, zoning and other aspects.
', '2018-12-07 19:27:04');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6710, 'Contact Us
Learn more about support in the Seattle area at http://health.groups.yahoo.com/group/MPDSeattleSupport/ .
Sign on to the Yahoo Group so you can exchange emails with members of          the CBCs-R-Us Seattle Area Support Group and stay informed about          upcoming meetings.
 
', '2018-12-07 19:27:04');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6710, 'Headstart or ECEAP (free to low-income families) Name First:
Name Last:
', '2018-12-07 19:27:04');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6710, 'Home   >> About Us
About King County Seniors Online
This is our newly formatted web site, which has been running since September 1997. It is the only independent Network Resource for older residents in Seattle and King County. According to the latest estimates of the Area Aging and Disability Agency, there are about 233,000 ''senior'' residents living in this region. Unofficial estimates figure that about 6% own and or use computers and use the Internet today, with a growth rate figured at 5% a year.
Our Mission Statement
"To serve the senior population living in Seattle/King County and through the medium of the Internet to make living here easier. And, to encourage the exchange of ideas, opinions, experiences and knowledge to help us get more out of life."
One of our goals is to see that every senior center, SPICE site and other community resource has at least one Internet linked computer dedicated to the interests of older people.
Your feedback and comments are important. How can we serve you. Please send us an
or call us at (206) 935-8175 and leave a message and tell us what you would like to see.
We will be constantly adding to this site and welcome your suggestions and comments.   Please feel free to send us the names of organizations, community resources, etc. that you think should be included here.
SPECIAL MESSAGE TO ORGANIZATIONS & GROUPS
This web site is open to all groups and organizations to publish your meeting times and other information.  Please contact us at (206) 935-8175 for details.
 
', '2018-12-07 19:27:04');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6710, 'NCBI Seattle moves individuals and organizations toward a just, inclusive society through training and leadership development. NCBI Seattle is affiliated with NCBI International, which has over 50 Community Chapters and Affiliates across the United States, Canada, and Europe.  Each Chapter or Affiliate is composed of community-based leaders who share a commitment to bringing NCBI''s methodologies to their community.
To find out more about the members of the Seattle Chapter of NCBI, click on the names below:
', '2018-12-07 19:27:04');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6711, '×   Pasted as rich text.    Paste as plain text instead
  Only 75 emoticons maximum are allowed.
×   Your link has been automatically embedded.    Display as a link instead
×   Your previous content has been restored.    Clear editor
×   You cannot paste images directly. Upload or insert images from URL.
', '2018-12-07 19:27:06');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6719, 'About
What is Freenet?
I worry about my child and the Internet all the time, even though she''s too young to have logged on yet. Here''s what I worry about. I worry that 10 or 15 years from now, she will come to me and say ''Daddy, where were you when they took freedom of the press away from the Internet?
--Mike Godwin, Electronic Frontier Foundation
Freenet is free software which lets you anonymously share files, browse and publish "freesites" (web sites accessible only through Freenet) and chat on forums, without fear of censorship. Freenet is decentralised to make it less vulnerable to attack, and if used in "darknet" mode, where users only connect to their friends, is very difficult to detect.
Communications by Freenet nodes are encrypted and are routed through other nodes to make it extremely difficult to determine who is requesting the information and what its content is.
Users contribute to the network by giving bandwidth and a portion of their hard drive (called the "data store") for storing files. Files are automatically kept or deleted depending on how popular they are, with the least popular being discarded to make way for newer or more popular content. Files are encrypted, so generally the user cannot easily discover what is in his datastore, and hopefully can''t be held accountable for it. Chat forums, websites, and search functionality, are all built on top of this distributed data store.
Freenet has been downloaded over 2 million times since the project started, and used for the distribution of censored information all over the world including countries such as China and in the Middle East. Ideas and concepts pioneered in Freenet have had a significant impact in the academic world. Our 2000 paper "Freenet: A Distributed Anonymous Information Storage and Retrieval System" was the most cited computer science paper of 2000 according to Citeseer, and Freenet has also inspired papers in the worlds of law and philosophy. Ian Clarke, Freenet''s creator and project coordinator, was selected as one of the top 100 innovators of 2003 by MIT''s Technology Review magazine.
An important recent development, which very few other networks have, is the "darknet": By only connecting to people they trust, users can greatly reduce their vulnerability, and yet still connect to a global network through their friends'' friends'' friends and so on. This enables people to use Freenet even in places where Freenet may be illegal, makes it very difficult for governments to block it, and does not rely on tunneling to the "free world".
Written by Ian Clarke
The Philosophy Behind Freenet
A Disclaimer
There are many reasons why people get involved in the Freenet Project. Some share the views outlined in this document; some share variations of these views, which are also served by what we are trying to achieve; and some just enjoy the technical challenge. These are the ideas which motivated me to architect the system in the first place, but not necessarily the views that everyone involved in the Freenet Project holds.
Suggested prior reading
For this document to make sense, you should probably know what Freenet is. You can get a good overview from What is Freenet? .
The importance of the Free flow of information
Freedom of speech, in most western cultures, is generally considered to be one of the most important rights any individual might have. Why is the freedom to share ideas and opinions so important? There are several ways to answer this question.
Communication is what makes us human
One of the most obvious differences between mankind and the rest of the animal kingdom is our ability to communicate sophisticated and abstract concepts. While we constantly discover that animal''s communication ability is more sophisticated than previously assumed, it is unlikely that any other animal approaches our own level of ability in this area.
Knowledge is good
Most people, given the option of knowing something and not knowing something, will choose to have more information rather than less. Wars have been won and lost over who was better-informed. This is because being better-informed allows us to make better decisions, and generally improve our ability to survive and be successful.
Democracy assumes a well informed population
Many people today live under democratic governments, and those who don''t, probably want to. Democracy is an answer to the question of how to create leaders, while preventing them from abusing that power. It achieves this by giving the population the power to regulate their government through voting, yet the ability to vote does not necessarily mean that you live in a democratic country. For a population to regulate their government effectively it must know what their government is doing, they must be well informed. It is a feedback loop, but this loop can be broken if the government has the power to control the information the population has access to.
Censorship and freedom
Everyone values their freedom, in fact, many consider it so important that they will die for it. People like to think that they are free to form and hold whatever opinions they like, particularly in western countries. Consider now that someone had the ability to control the information you have access to. This would give them the ability to manipulate your opinions by hiding some facts from you, by presenting you with lies and censoring anything that contradicted those lies. This is not some Orwellian fiction, it is standard practice for most western governments to lie to their populations, so much so, that people now take it for granted, despite the fact that this undermines the very democratic principles which justify the government''s existence in the first place.
The solution
The only way to ensure that a democracy will remain effective is to ensure that the government cannot control its population''s ability to share information, to communicate. So long as everything we see and hear is filtered, we are not truly free. Freenet''s aim is to allow two or more people who wish to share information, to do so.
Isn''t censorship sometimes necessary?
Of course no issue is black and white, and there are many who feel that censorship is a good thing in some circumstances. For example, in some European countries propagating information deemed to be racist is illegal. Governments seek to prevent people from advocating ideas which are deemed damaging to society. There are two answers to this however. The first is that you can''t allow those in power to impose "good" censorship, without also enabling them to impose "bad" censorship. To impose any form of censorship a government must have the ability to monitor and thus restrict communication. There are already criticisms that the anti-racism censorship in many European countries is hampering legitimate historical analysis of events such as the second world war.
The second argument is that this "good" censorship is counter-productive even when it does not leak into other areas. For example, it is generally more effective when trying to persuade someone of something to present them with the arguments against it, and then answer those arguments. Unfortunately, preventing people from being aware of the often sophisticated arguments used by racists, makes them vulnerable to those arguments when they do eventually encounter them.
Of course the first argument is the stronger one, and would still hold-true even if you didn''t accept the second. Basically, you either have censorship, or you don''t. There is no middle-ground.
But why is anonymity necessary?
You cannot have freedom of speech without the option to remain anonymous. Most censorship is retrospective, it is generally much easier to curtail free speech by punishing those who exercise it afterward, rather than preventing them from doing it in the first place. The only way to prevent this is to remain anonymous. It is a common misconception that you cannot trust anonymous information. This is not necessarily true, using digital signatures people can create a secure anonymous pseudonym which, in time, people can learn to trust. Freenet incorporates a mechanism called "subspaces" to facilitate this.
And what of copyright?
Of course much of Freenet''s publicity has centered around the issue of copyright, and thus I will speak to it briefly. The core problem with copyright is that enforcement of it requires monitoring of communications, and you cannot be guaranteed free speech if someone is monitoring everything you say. This is important, most people fail to see or address this point when debating the issue of copyright, so let me make it clear:
You cannot guarantee freedom of speech and enforce copyright law
It is for this reason that Freenet, a system designed to protect Freedom of Speech, must prevent enforcement of copyright.
But how will artists be rewarded for their work without copyright?
Firstly, even if copyright were the only way that artists could be rewarded for their work, then I would contend that freedom is more important than having professional artists (those who claim that we would have no art do not understand creativity: people will always create, it is a compulsion, the only question is whether they can do it for a living).
Secondly, it could be questioned whether copyright is effective even now. The music industry is one of the most vocally opposed to enhancements in communication technology, yet according to many of the artists who should be rewarded by copyright, it is failing to do so. Rather it has allowed middle-men to gain control over the mechanisms of distribution, to the detriment of both artists and the public.
Alternatives to Copyright
Fortunately it won''t come to this. There are many alternative ways to reward artists. The simplest is voluntary payment. This is an extension of the patronage system which was frequently used to reward artists prior to copyright, where a wealthy person would fund an artist to allow them to create full-time. The Internet permits an interesting extension of this idea, where rather than having just one wealthy patron, you could have hundreds of thousands, contributing small amounts of money over the Internet.
We actually practice what we preach in this regard too, on the 15th of March 2001 the Freenet Project started taking donations, and within a week we had collected over $1000.
Current Contributors
Ian Clarke
Freenet is based on Ian''s paper "A Distributed Decentralised Information Storage and Retrieval System". Ian started the Freenet Project around July of 1999, and continues to coordinate the project. Learn more about Ian on his personal website .
Matthew Toseland
Matthew has been working on Freenet since before the 0.5 release. His work and that of others has resulted in dramatic improvements to the performance and stability of the network.
Oskar Sandberg
Oskar was also one of the earliest contributors to the Freenet Project, and has made some important theoretical breakthroughs that lead to the beginning of Freenet 0.7, see the papers page.
Florent Daignière
Since 2003, Florent has improved various aspects of the software and performed the project''s system administration. In his day job, he is the Technical Director of Matta Consulting , a boutique security consultancy firm and currently works on safepass.me , an Active Directory password filter.
Michael Rogers
Michael has mostly contributed detailed simulations as part of the Google Summer of Code. He has been helpful in designing the new transport layer .
Steve Dougherty
The current release manager. He joined in GSoC 2013 and has been a driving force behind tackling long standing issues in Freenet.
xor
The Developer of the Web of Trust and Freetalk. He worked on the Web of Trust in part-time for one year and is now working as volunteer again.
David (Bombe) Roden
The developer of the site insertion tool jSite and of Sone, the Social Network over Freenet.
Ximin Luo
A debian developer who currently works on packaging Freenet.
Bert Massop
Works on the Freenet core and wherever there is need.
TheSeeker
A long term contributor who, among other things, helps keep the contact between the core developers and users in active subgroups.
Tommy[D]
A Gentoo packager who untangled all the dependencies of Freenet and packaged it cleanly in Gentoo.
Arne Babenhauserheide
The current maintainer of pyFreenet and infocalypse. He also writes articles and tutorials for Freenet.
The translators
A dilligent team of people from various backgrounds who make it possible to ship Freenet and this website in many different languages.
Many more great hackers
This list is missing many freesite authors, plugin writers, and a host of other people who contributed in various ways.
Anonymous Contributors
Works on Freenet core and communicates via FMS.
Somedude
The developer of the Freenet-based Forum system FMS, of FreenetHG and of FLIP, chat over Freenet.
The folks from Frost
A group of users and programmers who use an old spammable Freenet-based forum system which has been abandoned by most of the core developers. They are active, however, and though it takes time for their contributions to reach to core development, they take part in Freenet development.
Previous Contributors
A dutch developer and statistic-enthusiast. He now works at Topicus.Education.
Scott Miller
Scott is responsible for the implementation of much of the cryptography elements within Freenet.
Steven Starr
Steven helps with administration of Freenet Project Inc, and is an advisor to the project on business and publicity matters.
Dave Baker
Dave''s main contribution has been Freemail , his Summer of Code project to build a working email-over-Freenet system, as well as some debugging and core work in various places.
Robert Hailey
Robert has helped improve the speed and security of Freenet by finding two major bugs, and has recently contributed some code.
David Sowder
David (Zothar) has helped the Freenet Project as time permits and interest directs, including configuration, statistics and peer management via FCP, the FProxy stats page and Node 2 Node Messages (N2NM/N2NTMs).
And hundreds of others, who either haven''t asked to be added here, who prefer to remain nameless, or who we just haven''t got around to thanking. Not to mention thousands of users, testers, and donors !
Papers
Measuring Freenet in the Wild: Censorship-resilience under Observation (PDF) Observations and measurements on the live Freenet network. Includes suggestions for improvement. This was submitted to PETS 2014.
The Dark Freenet (PDF) Detailed paper about the Freenet 0.7.5 network, as opposed to its routing algorithm, which is detailed in the below papers. Includes some new simulations. This has been submitted to PET 2010.
Video of Small World talk, Berlin, December 2005
This is a video of a talk given by Ian Clarke and Oskar Sandberg at the Chaos Computer Congress in Berlin, December 2005, describing the (then) new architecture for Freenet 0.7. You can also download the slideshow , and the source for the Java demo (requires Java 1.5).
Searching in a Small World (PDF) Oskar Sandberg''s licentiate thesis describing a simple decentralized mechanism for constructing small world networks that is inspired by Freenet''s original design. Part II of the thesis describes the basis for the new Darknet architecture.
Distributed routing in Small World Networks (PDF) A paper by Oskar Sandberg describing the theoretical basis for the new "Darknet" routing mechanism employed by Freenet 0.7.
Chaos Computer Congress Talk (slideshow)
This is a slideshow for a talk given at the Chaos Computer Congress on 30th Dec 2005 in Berlin, Germany by Ian Clarke and Oskar Sandberg. It described the new "darknet" approach to be employed in Freenet 0.7. A Java demonstration to accompany the talk is also available.
Switching for a small world (PDF) A thesis by Vilhelm Verendel exploring ways to optimise the swapping algorithm.
Protecting Freedom of Information Online with Freenet (PDF) An IEEE Internet Computing article describing the Freenet architecture circa 2002 - probably the best introduction to the theory behind Freenet.
FreeNet White Paper (PDF) Original white paper by Ian Clarke, Division of Informatics, University of Edinburgh 1999.
', '2018-12-07 19:27:10');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6719, 'About
Qu''est-ce que Freenet ?
I worry about my child and the Internet all the time, even though she''s too young to have logged on yet. Here''s what I worry about. I worry that 10 or 15 years from now, she will come to me and say ''Daddy, where were you when they took freedom of the press away from the Internet?
--Mike Godwin, Electronic Frontier Foundation
Freenet est un logiciel libre qui vous permet, de façon anonyme, de partager des fichiers, de parcourir et de publier des « sitesFree » (sites Web accessibles uniquement par Freenet), de clavarder sur des forums, le tout sans craindre la censure. Freenet est décentralisé afin de le rendre moins vulnérable aux attaques, et s''il est utilisé en mode « réseau invisible » qui implique que les utilisateurs se connectent seulement à leurs amis, il est très difficile à détecter.
Les communications des nœuds Freenet sont chiffrées et acheminées par d''autres nœuds afin qu''il soit extrêmement difficile de déterminer qui demande l''information et quel en est le contenu.
Les utilisateurs contribuent au réseau en donnant de la bande passante  et une partie de leur disque dur (appelée « magasin de données ») pour le stockage de fichiers. Les fichiers sont automatiquement conservés ou supprimés suivant leur popularité, les moins populaires étant détruits  pour faire place à du contenu plus récent ou plus populaire. Les fichiers sont chiffrés. L''utilisateur ne peut donc pas découvrir facilement ce que contient son magasin de données et, avec un peu de chance, ne peut pas en être tenu responsable. Les forums de discussion, les sites Web et la fonction de recherche s''appuient sur ce magasin de données distribué.
Freenet a été téléchargé plus de 2 millions de fois depuis le début du  projet et a été utilisé pour la diffusion d''informations censurées partout dans le monde, incluant des pays comme la Chine et au Moyen-Orient. Les idées et concepts précurseurs de Freenet ont eu un impact significatif dans le milieu universitaire. Notre article de 2000 intitulé « Freenet: A Distributed Anonymous Information Storage and Retrieval System (Freenet : un système de stockage et de récupération d''informations anonyme et distribué) » a été l''article scientifique le plus cité de 2000 d''après Citeseer, et Freenet a aussi inspiré des articles dans les milieux juridique et philosophique. Ian Clarke, créateur de Freenet et coordinateur du projet, a été choisi comme l''un des 100 premiers innovateurs de 2003 par le magazine « Technology Review » du MIT.
Le « réseau invisible » est un développement important récent que très peu d''autres réseaux possèdent : en ne se connectant qu''à des personnes de confiance, les utilisateurs peuvent grandement réduire leur vulnérabilité, tout en se connectant à un réseau mondial par l''intermédiaire des amis des amis de leurs amis, etc. Cela permet l''utilisation de Freenet en des lieux où Freenet pourrait être illégal, rend le blocage des gouvernements très difficile, et ne dépend pas d''une tunnellisation vers le « monde libre ».
Written by Ian Clarke
The Philosophy Behind Freenet
A Disclaimer
There are many reasons why people get involved in the Freenet Project. Some share the views outlined in this document; some share variations of these views, which are also served by what we are trying to achieve; and some just enjoy the technical challenge. These are the ideas which motivated me to architect the system in the first place, but not necessarily the views that everyone involved in the Freenet Project holds.
Suggested prior reading
For this document to make sense, you should probably know what Freenet is. You can get a good overview from What is Freenet? .
The importance of the Free flow of information
Freedom of speech, in most western cultures, is generally considered to be one of the most important rights any individual might have. Why is the freedom to share ideas and opinions so important? There are several ways to answer this question.
Communication is what makes us human
One of the most obvious differences between mankind and the rest of the animal kingdom is our ability to communicate sophisticated and abstract concepts. While we constantly discover that animal''s communication ability is more sophisticated than previously assumed, it is unlikely that any other animal approaches our own level of ability in this area.
Knowledge is good
Most people, given the option of knowing something and not knowing something, will choose to have more information rather than less. Wars have been won and lost over who was better-informed. This is because being better-informed allows us to make better decisions, and generally improve our ability to survive and be successful.
Democracy assumes a well informed population
Many people today live under democratic governments, and those who don''t, probably want to. Democracy is an answer to the question of how to create leaders, while preventing them from abusing that power. It achieves this by giving the population the power to regulate their government through voting, yet the ability to vote does not necessarily mean that you live in a democratic country. For a population to regulate their government effectively it must know what their government is doing, they must be well informed. It is a feedback loop, but this loop can be broken if the government has the power to control the information the population has access to.
Censorship and freedom
Everyone values their freedom, in fact, many consider it so important that they will die for it. People like to think that they are free to form and hold whatever opinions they like, particularly in western countries. Consider now that someone had the ability to control the information you have access to. This would give them the ability to manipulate your opinions by hiding some facts from you, by presenting you with lies and censoring anything that contradicted those lies. This is not some Orwellian fiction, it is standard practice for most western governments to lie to their populations, so much so, that people now take it for granted, despite the fact that this undermines the very democratic principles which justify the government''s existence in the first place.
The solution
The only way to ensure that a democracy will remain effective is to ensure that the government cannot control its population''s ability to share information, to communicate. So long as everything we see and hear is filtered, we are not truly free. Freenet''s aim is to allow two or more people who wish to share information, to do so.
Isn''t censorship sometimes necessary?
Of course no issue is black and white, and there are many who feel that censorship is a good thing in some circumstances. For example, in some European countries propagating information deemed to be racist is illegal. Governments seek to prevent people from advocating ideas which are deemed damaging to society. There are two answers to this however. The first is that you can''t allow those in power to impose "good" censorship, without also enabling them to impose "bad" censorship. To impose any form of censorship a government must have the ability to monitor and thus restrict communication. There are already criticisms that the anti-racism censorship in many European countries is hampering legitimate historical analysis of events such as the second world war.
The second argument is that this "good" censorship is counter-productive even when it does not leak into other areas. For example, it is generally more effective when trying to persuade someone of something to present them with the arguments against it, and then answer those arguments. Unfortunately, preventing people from being aware of the often sophisticated arguments used by racists, makes them vulnerable to those arguments when they do eventually encounter them.
Of course the first argument is the stronger one, and would still hold-true even if you didn''t accept the second. Basically, you either have censorship, or you don''t. There is no middle-ground.
But why is anonymity necessary?
You cannot have freedom of speech without the option to remain anonymous. Most censorship is retrospective, it is generally much easier to curtail free speech by punishing those who exercise it afterward, rather than preventing them from doing it in the first place. The only way to prevent this is to remain anonymous. It is a common misconception that you cannot trust anonymous information. This is not necessarily true, using digital signatures people can create a secure anonymous pseudonym which, in time, people can learn to trust. Freenet incorporates a mechanism called "subspaces" to facilitate this.
And what of copyright?
Of course much of Freenet''s publicity has centered around the issue of copyright, and thus I will speak to it briefly. The core problem with copyright is that enforcement of it requires monitoring of communications, and you cannot be guaranteed free speech if someone is monitoring everything you say. This is important, most people fail to see or address this point when debating the issue of copyright, so let me make it clear:
You cannot guarantee freedom of speech and enforce copyright law
It is for this reason that Freenet, a system designed to protect Freedom of Speech, must prevent enforcement of copyright.
But how will artists be rewarded for their work without copyright?
Firstly, even if copyright were the only way that artists could be rewarded for their work, then I would contend that freedom is more important than having professional artists (those who claim that we would have no art do not understand creativity: people will always create, it is a compulsion, the only question is whether they can do it for a living).
Secondly, it could be questioned whether copyright is effective even now. The music industry is one of the most vocally opposed to enhancements in communication technology, yet according to many of the artists who should be rewarded by copyright, it is failing to do so. Rather it has allowed middle-men to gain control over the mechanisms of distribution, to the detriment of both artists and the public.
Alternatives to Copyright
Fortunately it won''t come to this. There are many alternative ways to reward artists. The simplest is voluntary payment. This is an extension of the patronage system which was frequently used to reward artists prior to copyright, where a wealthy person would fund an artist to allow them to create full-time. The Internet permits an interesting extension of this idea, where rather than having just one wealthy patron, you could have hundreds of thousands, contributing small amounts of money over the Internet.
We actually practice what we preach in this regard too, on the 15th of March 2001 the Freenet Project started taking donations, and within a week we had collected over $1000.
Current Contributors
Ian Clarke
Freenet is based on Ian''s paper "A Distributed Decentralised Information Storage and Retrieval System". Ian started the Freenet Project around July of 1999, and continues to coordinate the project. Learn more about Ian on his personal website .
Matthew Toseland
Matthew has been working on Freenet since before the 0.5 release. His work and that of others has resulted in dramatic improvements to the performance and stability of the network.
Oskar Sandberg
Oskar was also one of the earliest contributors to the Freenet Project, and has made some important theoretical breakthroughs that lead to the beginning of Freenet 0.7, see the papers page.
Florent Daignière
Since 2003, Florent has improved various aspects of the software and performed the project''s system administration. In his day job, he is the Technical Director of Matta Consulting , a boutique security consultancy firm and currently works on safepass.me , an Active Directory password filter.
Michael Rogers
Michael has mostly contributed detailed simulations as part of the Google Summer of Code. He has been helpful in designing the new transport layer .
Steve Dougherty
The current release manager. He joined in GSoC 2013 and has been a driving force behind tackling long standing issues in Freenet.
xor
The Developer of the Web of Trust and Freetalk. He worked on the Web of Trust in part-time for one year and is now working as volunteer again.
David (Bombe) Roden
The developer of the site insertion tool jSite and of Sone, the Social Network over Freenet.
Ximin Luo
A debian developer who currently works on packaging Freenet.
Bert Massop
Works on the Freenet core and wherever there is need.
TheSeeker
A long term contributor who, among other things, helps keep the contact between the core developers and users in active subgroups.
Tommy[D]
A Gentoo packager who untangled all the dependencies of Freenet and packaged it cleanly in Gentoo.
Arne Babenhauserheide
The current maintainer of pyFreenet and infocalypse. He also writes articles and tutorials for Freenet.
The translators
A dilligent team of people from various backgrounds who make it possible to ship Freenet and this website in many different languages.
Many more great hackers
This list is missing many freesite authors, plugin writers, and a host of other people who contributed in various ways.
Anonymous Contributors
Works on Freenet core and communicates via FMS.
Somedude
The developer of the Freenet-based Forum system FMS, of FreenetHG and of FLIP, chat over Freenet.
The folks from Frost
A group of users and programmers who use an old spammable Freenet-based forum system which has been abandoned by most of the core developers. They are active, however, and though it takes time for their contributions to reach to core development, they take part in Freenet development.
Previous Contributors
A dutch developer and statistic-enthusiast. He now works at Topicus.Education.
Scott Miller
Scott is responsible for the implementation of much of the cryptography elements within Freenet.
Steven Starr
Steven helps with administration of Freenet Project Inc, and is an advisor to the project on business and publicity matters.
Dave Baker
Dave''s main contribution has been Freemail , his Summer of Code project to build a working email-over-Freenet system, as well as some debugging and core work in various places.
Robert Hailey
Robert has helped improve the speed and security of Freenet by finding two major bugs, and has recently contributed some code.
David Sowder
David (Zothar) has helped the Freenet Project as time permits and interest directs, including configuration, statistics and peer management via FCP, the FProxy stats page and Node 2 Node Messages (N2NM/N2NTMs).
And hundreds of others, who either haven''t asked to be added here, who prefer to remain nameless, or who we just haven''t got around to thanking. Not to mention thousands of users, testers, and donors !
Les articles
Measuring Freenet in the Wild: Censorship-resilience under Observation (PDF) Observations and measurements on the live Freenet network. Includes suggestions for improvement. This was submitted to PETS 2014.
The Dark Freenet (PDF) Detailed paper about the Freenet 0.7.5 network, as opposed to its routing algorithm, which is detailed in the below papers. Includes some new simulations. This has been submitted to PET 2010.
Video of Small World talk, Berlin, December 2005
This is a video of a talk given by Ian Clarke and Oskar Sandberg at the Chaos Computer Congress in Berlin, December 2005, describing the (then) new architecture for Freenet 0.7. You can also download the slideshow , and the source for the Java demo (requires Java 1.5).
Searching in a Small World (PDF) Oskar Sandberg''s licentiate thesis describing a simple decentralized mechanism for constructing small world networks that is inspired by Freenet''s original design. Part II of the thesis describes the basis for the new Darknet architecture.
Distributed routing in Small World Networks (PDF) A paper by Oskar Sandberg describing the theoretical basis for the new "Darknet" routing mechanism employed by Freenet 0.7.
Chaos Computer Congress Talk (slideshow)
This is a slideshow for a talk given at the Chaos Computer Congress on 30th Dec 2005 in Berlin, Germany by Ian Clarke and Oskar Sandberg. It described the new "darknet" approach to be employed in Freenet 0.7. A Java demonstration to accompany the talk is also available.
Switching for a small world (PDF) A thesis by Vilhelm Verendel exploring ways to optimise the swapping algorithm.
Protecting Freedom of Information Online with Freenet (PDF) An IEEE Internet Computing article describing the Freenet architecture circa 2002 - probably the best introduction to the theory behind Freenet.
FreeNet White Paper (PDF) Original white paper by Ian Clarke, Division of Informatics, University of Edinburgh 1999.
', '2018-12-07 19:27:10');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6724, 'Recurring monthly donations enable us to continue collecting and publishing environmental data. Contribute now!
Contact Us
The Japan office is located in the Loftwork building in Shibuya:
SAFECAST Japan (c/o Loftwork) 150-0043 Tokyo-to, Shibuya-ku Dogenzaka 1-22-7, Dogenzaka Pia 3F Japan
 
', '2018-12-07 19:27:14');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6731, 'Category: About
Hits: 58688
Welcome to Writing Commons, the open-education home for writers. Our mission is to help college students improve their writing, research, and critical thinking. We publish a variety of webtexts on writing pedagogy. Major topics include Academic Writing, Information Literacy, Evidence and Documentation, Research Methods & Methodologies, Style, New Media Communication, Professional and Technical Communication, and Creative Writing. Writing Commons also publishes a monthly newsletter, unCommon News.
Students in a variety of writing courses may use Writing Commons, including composition, business writing, STEM/technical writing, and creative writing. Widely adopted, Writing Commons is used worldwide by numerous institutions, including Georgia Institute of Technology , Ohio State University , Duke University , Malmö University , and the University of Tartu . Writing Commons sponsors the Aaron Swartz Best Webtext Award for the best original OER published by Writing Commons. Our 2015 winner was Angela Eward-Mangione for her webtext, ??.
We are fortunate to have a team of volunteers who work hard to make our commons project a popular, global resource. The Editorial Board and the Advisory Board , composed of distinguished academics, review submissions on a rolling basis. In the first round of peer review, submissions are reviewed by our Staff . Reviews are conducted by our Review Editors . Members of the Editorial Board serve primarily in an advisory capacity, yet they may also review submissions.
Original webtexts published by Writing Commons are licensed under a Creative Commons 3.0 NC ND. In addition, Writing Commons reprints works with permission and publishes some works under a CC 3.0 SA.
Writing Commons was founded in 2008 by Joe Moxley ( more ).
', '2018-12-07 19:27:27');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6731, 'Details
Students
Writing Commons is, first and foremost, a resource for students. We hope that you find the content you are searching for and that it was both comprehensive, easy to read, and beneficial to your coursework. If you would like to share any thoughts or ideas with us about how we can better serve you, please don''t hesitate to contact us through the form on this page.
Instructors
Aside from providing a free learning resource for students, we also like to provide syllabi and planned coursework to instructors who choose to use it. Is there a way we can better serve the needs of your students? Do you need something specific to your class that we do not yet have on Writing Commons? Please share your feedback with us.
For any marketing, PR, or commercial inquires, please use the form below and provide your preferred method of contact. We will be in touch with you as soon as possible.
Contact us
If you want more information, fill in this form. We will get back to you as soon as possible.
Name
', '2018-12-07 19:27:27');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6731, 'About
Details
Welcome to Writing Commons, the open-education home for writers. Our mission is to help college students improve their writing, research, and critical thinking. We publish a variety of webtexts on writing pedagogy. Major topics include Academic Writing, Information Literacy, Evidence and Documentation, Research Methods & Methodologies, Style, New Media Communication, Professional and Technical Communication, and Creative Writing. Writing Commons also publishes a monthly newsletter, unCommon News.
Students in a variety of writing courses may use Writing Commons, including composition, business writing, STEM/technical writing, and creative writing. Widely adopted, Writing Commons is used worldwide by numerous institutions, including Georgia Institute of Technology , Ohio State University , Duke University , Malmö University , and the University of Tartu . Writing Commons sponsors the Aaron Swartz Best Webtext Award for the best original OER published by Writing Commons. Our 2015 winner was Angela Eward-Mangione for her webtext, ??.
We are fortunate to have a team of volunteers who work hard to make our commons project a popular, global resource. The Editorial Board and the Advisory Board , composed of distinguished academics, review submissions on a rolling basis. In the first round of peer review, submissions are reviewed by our Staff . Reviews are conducted by our Review Editors . Members of the Editorial Board serve primarily in an advisory capacity, yet they may also review submissions.
Original webtexts published by Writing Commons are licensed under a Creative Commons 3.0 NC ND. In addition, Writing Commons reprints works with permission and publishes some works under a CC 3.0 SA.
Writing Commons was founded in 2008 by Joe Moxley ( more ).
', '2018-12-07 19:27:27');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6731, 'Assistant Professor of English
Kennesaw State University
E. Jonathan Arnett is an Assistant Professor in the Professional Writing Program at Kennesaw State University, where he teaches Technical Writing, Professional Editing, and First Year Composition. His research interests include Rhetoric of Science and Professional Editing Course Pedagogy.
Matt Barton
Associate Professor of English
St. Cloud State University
Matt Barton teaches Composition and Digital Media courses at the undergraduate and graduate level. His research focuses on Digital Media, Wikis, and Video Games. He is the co-editor of Wiki Writing: Collaborative Learning in the Classroom; co-author of Vintage Games: An Insider Look at the History of Grand Theft Auto, Super Mario, and the Most Influential Games of All Time; and author of Dungeons & Desktops: The History of Computer Role-Playing Games (A K Peters/CRC Press, 2008). He has published articles in the journals Computers and Composition, Technical Communications Quarterly, Game Studies, and several edited collections. He is also the producer of Matt Chat, a weekly YouTube program featuring interviews with game developers and historical retrospectives of classic games. Matt has an  academic website , a  blog , and a YouTube Channel .
 
Instructor and PhD Student
Ball State University
Matt Balk is a PhD student at Ball State University, where he teaches First Year Composition.  He is currently the Assistant Director of the Writing Center.  His research interests include Social Media Usage in the Composition Classroom, as well as Synchronous Online Communication Methods.
 
Assistant Professor of English
Cameron University
William Carney is an Assistant Professor and Director of the Composition Program at Cameron University in Oklahoma. In addition to Rhetoric and Composition, he has published on teaching English as a Foreign Language, varieties of English, and collaborative writing in venues such as Intercultural Communication Studies and the Journal of Language Teaching and Research. His current research interests all involve English as a Second Language and English as a Foreign Language, and he teaches classes in Technical Writing and Composition Pedagogy for English Education majors. He holds a doctorate from Texas Tech University and has earned Master’s Degrees in English and Organizational Behavior.
 
Associate Professor
University of Wisconsin
Joel Friederich is a poet and Associate Professor at the University of Wisconsin-Barron County in Northwestern Wisconsin. Blue to Fill the Empty Heaven, his full-length collection of his poetry, was published in 2009 by Silverfish Review Press after winning the Gerald Cable Award. He has also published two chapbooks, Without Us from Finishing Line Press and The Body We Gather from Kulupi Press. His poetry has won a Wisconsin Academy of Sciences, Arts, and Letters prize, and individual poems have appeared in journals such as Witness, Prairie Schooner, Sou''wester, The Paris Review, Beloit Poetry Journal, River Styx, and others.
 
PhD Candidate
Indiana University of Pennsylvania
A PhD candidate at Indiana University of Pennsylvania, Tamara Girardi teaches online and face-to-face for two community colleges in Pennsylvania. She holds a BA in English and Humanities (Jacksonville University, Florida) and an MLitt in Creative Writing (University of St. Andrews, Scotland). Her doctoral dissertation, It Can Be Acquired and Learned: Building a Writer-Centered Pedagogical Approach to Creative Writing, argues for a balance of practice and theory in daily assignments and attention to student writing and reading preferences.
Tamara writes young adult fiction, and her academic research interests include creative writing studies, online learning, student engagement, and writer-centeredness. Follow her on Twitter @TamaraGirardi
 
Professor of English/Director of Professional Writing
Barry University
Andrea Greenbaum teaches classes in professional writing, cultural studies, gender, multimedia writing, and screenwriting. She is also the Director of the Professional Writing Program and has served on the national editorial boards of College Composition and Communication and Florida English. Additionally, she has published four books: Judaic Perspectives on Rhetoric and Composition (Hampton Press, 2008), Jews of South Florida (Brandeis University Press, 2005), Emancipatory Movements: The Rhetoric of Possibility (SUNY Press, 2002), and Insurrections: Approaches to Resistance in Composition Studies (SUNY Press, 2001). Her articles and reviews have been published in numerous journals including, The Journal of Men’s Studies, Composition Forum, Writing on the Edge, American Studies, American Jewish History, Shofar, Humor: The International Journal of Humor Research, JAC, Film and History, Florida English, and the Journal of the Assembly of Expanded Perspectives on Learning.
 
Assistant Professor
University of Arkansas at Little Rock
Heidi Skurat Harris is an Assistant Professor of Rhetoric and Writing at the University of Arkansas at Little Rock where she teaches technical writing, grant writing, digital rhetoric and rhetorical theory and has developed a certificate in online writing instruction. Her research interests are online and digital pedagogy and online professional development. She also sits on the Conference on Composition and Communication''s Committee for Effective Practices in Online Instruction where she serves on the editorial board of the Online Writing Instruction Open Resource.
 
Stephanie Hedge
Assistant Professor of English
Stephanie Hedge is an Assistant Professor of English at SUNY Potsdam. She teaches writing courses to both undergraduates and graduate students, including digital writing, and courses on rhetoric and composition theory. Her research focuses on new media writing and digital literacies.
 
Assistant Creative Writing Editor of Writing Commons
Doctoral candidate (ABD) in the Composition and TESOL program
Indiana University of Pennsylvania
Mitch James was born and raised in Central Illinois, where he received a BA in English with a minor in Creative Writing from Eastern Illinois University. He received a Masters in Literature from Indiana University of Pennsylvania and has had fiction and poetry published in Decomp, Underground Voices, Kill Author Digital Americana and Blue Earth Review among others. Mitch is a doctoral candidate (ABD) in the Composition and TESOL program at Indiana University of Pennsylvania, where he''s both an instructor in the English Department and Assistant Coordinator of the English Writing Portfolio Placement Program.
Mitch''s latest scholarly article, "Tragedy, Plot, Fiction: A Study of Sameness and How You May Have Been Duped," was recently published in New Writing: The International Journal for the Practice and Theory of Creative Writing.
 
Lecturer
The University of Baltimore
Christopher Justice is a lecturer and teaches courses in Composition, Linguistics, and Literature. As a doctoral student in the University of Maryland, Baltimore County’s Language, Literacy, and Culture program, his interdisciplinary research focuses on Environmental Discourse, Ecocomposition and Ecoliteracy. He examines how people compose and write about the ecological "place" known as a fishery and how diverse, multimodal discourses—including literary, journalistic, cinematic, and scientific texts—influence how we conceptualize, regulate, and interact with fisheries, particularly those in the Chesapeake Bay. Other scholarly interests include Visual Rhetoric, Writing in the Disciplines, Writing Program Administration, Journalism, and Environmental Humanities. Additionally, he is a film scholar and has recently published chapters in Edgar G. Ulmer: Detour on Poverty Row, The Worlds of Back to the Future, The Films of Joseph H. Lewis, and The Cinema of Michael Haneke: Europe Utopia.
 
Director of the Writing Program & Associate Professor
University of Arizona
Dr. Amy C. Kimme Hea is Director of the Writing Program and an Associate Professor in the Rhetoric, Composition, and Teaching of English Program at the University of Arizona. Her research interests include Spatial Rhetoric, Hypertext Theory, Computers and Composition, Writing Program Administration and Assessment, and Professional and Technical Writing Theory and Practice. She is on the Executive Committee of the Consortium of Doctoral Programs in Rhetoric and Composition, and at the University of Arizona, she facilitates a Faculty Learning Community on Program Assessment. Her collection Going Wireless: A Critical Exploration of Wireless and Mobile Technologies for Composition Teachers and Researchers (Hampton Press, 2009) was nominated for the Computers and Composition Best Book Award, and she has published in a range of peer-reviewed edited collections and journals in the field.
 
Associate Professor
Lewis University
Bonnie Lenore Kyburz teaches writing, rhetoric, and digital media studies at Lewis University. A long time Sundance volunteer, film lover, performer, and rhetorician, kyburz makes short digital films that hope to resonate as entertaining, provocative arguments, especially for an evolving linguistic academic scene. Her work also appears in Composition Studies, College English, and other NCTE publications. She is currently working toward publication of her book Screen(ing) Rhetorics: Affective Digital Mediations Toward Film-Composition with the #writing series and West Virginia University Press.
 
Instructor
MMI Preparatory School
Jennifer Lee Novotney teaches English at MMI Preparatory School in Freeland, PA. She has taught writing courses at Pennsylvania State University and was Coordinator of Writing at Misericordia University in Dallas, PA, where she directed the Writing Center. She was selected as a national judge for the 2012 and 2013 National Council of Teachers of English Achievement Awards in Writing, and she judged the Norman Mailer College Poetry Awards in 2012 and 2013. She has been a consultant for Pearson in Composition and Literature, as well. A native of Los Angeles, CA, she earned a Bachelor of Arts in Journalism from California State University and a Master of Arts in English from Northern Arizona University. She is a published author of magazine articles, poetry, and short stories. Her novel, Winter in The Soul, was published in 2014.
 
Professor of Literature and Composition
Hillsborough Community College
Angela Eward-Mangione is a Full-Time Instructor in the English Department at Hillsborough Community College, where she teaches English Composition I and English Composition II. Her research interests include Adaptations, Cultural Studies, and Pedagogy for Composition and Literature Courses.
 
Assistant Professor of English
College of Saint Rose
Jennifer Marlow is Assistant Professor of English at The College of Saint Rose in Albany, NY where she teaches courses in Composition and New Media. Her work focuses on Educational Technology Software and its uses in the writing classroom. When she is not busy researching Innovative Digital Technologies that bring learning "outside the box," she makes documentaries with colleague, Megan Fulwiler, about how the labor conditions of higher education affect everything from academic freedom to student learning to how we implement and think about technology.
 
Assistant Professor of English & (S.T.E.P) faculty member
Northern Essex Community College & Bentley University
Patricia Portanova is an Assistant Professor at Northern Essex Community College and a Summer Transitional Education Program (S.T.E.P) faculty member at Bentley University where she teaches first-year writing, creative writing, technical and professional writing, and linked reading/writing learning communities. She also serves as chair of the Northeast Writing Across the Curriculum Consortium and co-chair of the CCCC Cognition and Writing Special Interest Group. She holds a doctorate in Composition Studies from the University of New Hampshire and a Master’s of Arts in English from Bridgewater State College. Her current research interests include media distractions and student writing, information design, reading development, and writing assessment theory and practice.
 
Assistant Professor of Composition and Rhetoric
University of Wisconsin-Stout
Daisy Pignetti’s passion for evaluating college-level writing blossomed as she worked as a teaching assistant during her M.A. and Ph.D. programs, and continues to grow in her current position as an Assistant Professor at the University of Wisconsin-Stout. While she was hired as a Rhetoric and Composition generalist, her teaching of the upper-level course, "Advanced Rhetoric," led to a more involved role in the Professional Communication and Emerging Media program and eventually the newly created Master of Science in Technical and Professional Communication program.
A proud New Orleans native, she has presented her research on selfless and successful social media use in the aftermath of the Hurricane Katrina disaster to the Oxford Internet Institute and Association of Internet Researchers in addition to the Computers and Writing community. She has been published in Computers and Composition Online and Reflections: A Journal of Writing, Service-Learning, and Community Literacy and her book chapter on blogs is part of Hampton Press’s New Dimensions in Computers and Composition series.
 
Assistant Professor of English
Elizabeth City State University
Abigail Scheg currently teaches at Elizabeth City State University. She holds a Bachelor of Arts in Creative Writing (California University of Pennsylvania), Master of Arts in Literature (Slippery Rock University) and a PhD in Composition (Indiana University of Pennsylvania). Her research interests include Online Teacher Training, Computers and Composition, and most recently, Online Assessment. Follow her on Twitter @Abigail_Scheg
 
Assistant Professor of Academic Writing
Pitzer College
Andrea Scott is Assistant Professor for Academic Writing at Pitzer College, where she also directs the Writing Center. Before joining the Claremont Colleges, she taught for five years in the Princeton Writing Program and served as the Associate Director for the Writing Seminars from 2010-2013. Her research and teaching interests include Writing Center Theory, Writing Across the Curriculum/Writing In the Discipline, and the International Turn in Writing Studies. Her most recent project focuses on the Transatlantic History of the Development of Writing Initiatives and Centers in Germany since the 1990s. By examining who is creating these programs and where they are situated within the university, her research contributes new ways of understanding how disciplinary knowledge and professional identities are constructed in writing studies outside North America. She holds an Master of Arts and PhD from the University of Chicago.
 
Assistant Professor of English
Wright State University
Lars Söderlund is an Assistant Professor of English at Wright State University, where he directs their Professional and Technical Writing program. His research interests include academic publishing, rhetorical theory, and professional writing program administration.
 
Assistant Professor of English
University of South Florida
Brogan Sullivan writes fiction, poetry and creative nonfiction and teaches creative writing and composition at the undergraduate level. His work has been published in Spry Literary Journal and First Inkling. His research interests include creative writing pedagogy, narratology, ethics, gender studies, and post-apocalyptic literature. He is an Assistant Professor of English at the University of South Florida.
 
Professor of English
University of North Carolina- Chapel Hill
Todd Taylor is a professor at the University of North Carolina at Chapel Hill. His research focuses "on how our notions of literacy are changing in response to emerging communications technologies such as the Internet" (full bio  here ). Todd served as the Director of the UNC Writing Program from July 2005 to July 2009, and in July 2009, he was appointed the Norman and Dorothy Eliason Distinguished Professor of English and Comparative Literature.
 
Business and Technical Writing Program Director
University of Alabama-Huntsville
Ryan Weber teaches technical writing courses at the University of Alabama in Huntsville, where he directs the Business and Technical Writing Program. His research focuses on NASA and science communication, social media, entrepreneurial rhetoric, and public rhetoric. His work appears in College Composition and Communication, Journal of Technical Writing and Communication, and Journal of Advanced Composition. He also runs uahtechcomm.com , a resource for technical writing practitioners and scholars. He received his Ph.D from Purdue University in 2009.
 
Assistant Professor of English
Auburn University
Susan A. Youngblood specializes in Technical and Professional Communication and is co-director of the Service Learning Opportunities in Technical Communication (SLOT-C) Database. Her current research largely examines how writing—including publicly published online writing—is crafted for individuals vulnerable to physical harm, cultural harm, and exclusion from access to information and services. She studies both the tensions and processes in that communication and how that writing could be improved.
', '2018-12-07 19:27:27');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6731, 'Welcome to Writing Commons,
a free, popular, global, peer-reviewed, open-education resource for college students (see About )
licensed by a CC BY-NC-ND 3.0 or CC BY-NC-SA 3.0 (excluding Common Comments ).
Writing Commons, https://writingcommons.org , helps students improve their writing, critical thinking, and information literacy. Founded in 2008 by Joseph M. Moxley , Writing Commons is a viable alternative to expensive writing textbooks. Faculty may assign Writing Commons for their composition , business , technical , and creative writing courses. We are currently crowdsourcing submissions via an academic, peer-review process (see Contribute ).
UnCommon News
', '2018-12-07 19:27:27');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6731, 'Hits: 125
Mitchell Ray James
Assistant Creative Writing Editor of Writing Commons Doctoral candidate (ABD) in the Composition and TESOL program, Indiana University of Pennsylvania
', '2018-12-07 19:27:27');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6731, 'A crowd-powered newsletter for a writing-centered community.
Issue 6, November, 2012
Hello friends,
When Governor Jerry Brown of California signed legislation that supports the creation of 50 free textbooks for common undergraduate courses, the Association of American Publishers (AAP) quickly critiqued the idea of free textbooks, suggesting it would costs “tens of millions of dollars to develop, distribute and maintain” these textbooks.
For Governor Brown as well as the AAP, we have some great news: You can cross one important textbook off of your list!  California, welcome to Writing Commons! 
We are pleased to announce that past issues of "unCommon News" are now available at our archive , and they are linked through Rich Haswell''s CompPile . We are honored to be anchored at CompPile, a quality crowd-powered academic resource.
In this issue we will explore:
Two Calls for Papers--one on academic argument, and one on Information Literacy
Writing Commons traffic report
A note from one of our users
Social Media
The Traffic Report
Traffic at Writing Commons is on a steady rise; in October alone we had more than 30,000 users. With around 1,000 users accessing Writing Commons every day, we look forward to making it 2,000.
 
What does more traffic at Writing Commons mean for you? Each user accessing Writing Commons is a vote for the relevance, quality, and scholastic value of every article published, and that article could be yours. So send in your work and enjoy the benefits of 30,000 readers a month.
 
User Report: Susan Youngblood
We''ve asked how you, our users, employ Writing Commons in your classrooms and this is what you had to say. This is how Susan Youngblood plans on using Writing Commons in her upcoming technical writing service courses:
I''ve selected a condensed book for the class, and I plan to use WC (Writing Commons) resources as supplements. In particular, I plan to add the following components:
managing group projects (for the group report assignment)
copyright and writing (for the instructions assignment because some students need the video)
some of the style entries
some pages on incorporating evidence (for the report assignment)
And I''ll incoprorate a set of resources I built from scratch before I learned about WC, resources that cover usage and other WC-type topics. 
I knew I could select a comprehensive textbook for the class, but students in the class are largely from other majors, especially nutrition & dietetics, wildlife biology, and building science. They seem intimidated by the large, dense chapters in most expensive writing textbooks and, for this and several other reasons, they often don''t read them. My service course students seem more likely to read bite-sized pieces about relevant topics, and both the condensed book and WC present information this way. Also, the comprehensive books are expensive (sometimes a few students put off buying then and fall behind in class) and heavy (many students are reluctant to tote them to class when they are carrying materials for a number of classes).
Were our audience for this class to shift significantly, I might change books and approaches, but I still like having short easy to read supplements.
We thank Susan for sharing her students'' thoughts on how Writing Commons helps bridge the gaps left behind by other course materials.
Do you have an anecdote or story about how you use Writing Commons in your classroom; maybe you just want to share some pedagogy? Either way, drop us a line; we want to hear from our users!
Calls for Papers
As an organic, digital, recursive text we are interested in receiving new and interesting articles and chapters to expand the breadth and depth of what we can offer our global community of writers.
While we welcome submissions covering all aspects of writing, we are especially in need of content focused on academic argument. In the past Writing Commons has partnered with the first-year composition program at the University of South Florida to produce a derivative work, a textbook used by that writing program--one that more specifically focuses on academic argument.
Those interested in submitting articles should contact Jason Carabelli and Brogan Sullivan at This email address is being protected from spambots. You need JavaScript enabled to view it.
for further details about recommended content and conventions.  Note: all submissions will be reviewed by editors at Writing Commons.
A list of submission topics is as follows:
Forms of Academic Writing
Writing is an Ongoing Conversation
Writing is a Process
', '2018-12-07 19:27:27');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6731, 'If you are unable to view the images in this newsletter,  click here .
unCommon News
A crowd-powered newsletter for a writing-centered community
October, 2013
Dear Friends, 
I hope your fall semester is getting off to a great start.
In response to last month''s newsletter, Qwo-Li Driskill asked me to share this opportunity for U.S. faculty who teach at tribally controlled colleges to receive financial support to attend the next Conference on College Composition and Communication:  https://www.ncte.org/cccc/awards/tribalcollegefellow .  
Speaking of reader feedback, I received a note from a reader who questioned whether our goal of being a global resource for writers suggests we seek to colonize Writing Studies, to presume that the U.S. model is the ideal model:
 
It is rather astonishing that even educators in the more dominant social and educational cultures don''t realize that even economically weak and sociopolitically less advanced communities around the world also have their own languages and communicative practices, their own unique conventions and needs for writing, their own local educational cultures, distinctive norms and values about what counts as knowledge, and unique standards and local expectations about how to write "well" (as they also have their own spiritual traditions in place).
In response, I acknowledge that by sharing our pedagogical resources we impose at some level our view of the pedagogy.  Thanks to theorists such as James Berlin, Paulo Freire, Lisa Delpit, or Henry Giroux, the discipline of Writing Studies is cognizant of the inherently ideological aspects of rhetoric.  However, I''d like to clarify our vision for Writing Commons: while we do seek to be a global, open-education home for writers, we do not wish to impose a single vision for writing pedagogy.  As rhetoricians and compositionists, we embrace linguistic and pedagogical diversity.  We aspire to celebrate and research context-based writing processes, genres, and methodologies. 
One way we hope to recognize diverse approaches to teaching writing is by publishing webtexts by faculty and writers in different countries/cultures.  Admittedly, nearly 100% of our webtexts are written by U.S. faculty and about 85% of our readers are tagged with U.S. IP addresses.  In the future, however, as the affordance of Internet technologies and access to these technologies enable us to talk with one another about our pedagogical goals and contexts, we are hopeful faculty from across the world will share their practices, research efforts, and theoretical approaches.  
Another way to explore our diverse teaching contexts is to publish alternative syllabi.  At the top navigational bar for Writing Commons (see  Adoptions ), we publish the syllabi of our users. Currently, our listing includes USF, Duke, Georgia Tech, and Ohio State.  Plus, to show how Writing Commons can be used beyond first-year composition courses, Sundus Alsharif has provided a syllabus for teaching Public Speaking. If you assign Writing Commons for your classes, we hope you''ll share your syllabi.  
A third way to celebrate diversity is to publish award-winning student texts by institution.  We''ve had discussions with several U.S. writing program administrators about publishing award-winning student texts. As a result, we''d like to add a "My Campus" tab that would enable users to view award winners by institution/country/state. Wouldn''t it be useful to refute the doom and gloom about student writing by publishing some great works?  Please let us know if you''d like to participate in this endeavor.
Lastly, I''m happy to report that our overall readership continued to grow during September, with over 194,870 visitors during September 2013.  Thank you for your ongoing support.  
Joe Moxley, This email address is being protected from spambots. You need JavaScript enabled to view it.
Executive Editor and Publisher
 Wanted: Teammates 
Calling All Team-Players!
Please help us grow Writing Commons so that its slogan is more than a slogan—so we truly are the open-education home for writers.  
Writing Commons especially needs to hire a Chief Technology Officer.  To beat back the spammers, we need help server-side.  The version of Joomla we use needs updating, and we would like to update our interface.  Unfortunately, because we lack funding, we cannot pay a salary to volunteers.
 This month we very happy to welcome new team members:
Daniel Richards, assistant professor, Old Dominion University, Senior Editor, Technical Writing
Kate Panteliedes, assistant professor, Eastern Michigan University, Review Editor
Cassandara Branham, UCF, Technical Writing Intern
Sundus Alsharif, USF, Social Media Intern.  
The Traffic Report
During September, traffic at Writing Commons hit new highs: over 194,870 users—i.e., 6495 visitors/day—visited 577,902 pages.  Overall, the server logged 5,553,512 hits.  
It''s amazing to us that in the last two months we had about 360,000 visitors.  We feel fortunate to be providing a free yet valued service to the open-education community.
 
 
 A Note from One of Our Review Editors
I have served as a review editor for Writing Commons for over a year and have enjoyed watching the website and articles take shape. The individuals involved in this project have been working tirelessly to develop the content and participation, and I believe in the necessity of this open-access material.
I have encouraged my students to use Writing Commons and have heard positive responses in the depth, breadth, and ease of access of the information. If my students like it and it helps them develop their writing skills, then it is most certainly a worthwhile cause to me. This semester I have challenged my upper-level composition students to develop an article for publication on the Writing Commons website and many expressed delight at the opportunity to become part of the learning tool that helped them. I look forward to working with my students to build their articles and hopefully I can provide a positive review of this process in a future newsletter. It’s never too late to get involved! #GoScholarGo 
Dr. Abigail G. Scheg, Elizabeth City State University
Recent Publications
This month, we have two original webtexts to celebrate:
"Diplomacy, Tone, and Emphasis in Business Writing" by Jessica McCaughey, George Washington University
In this  webtext , Jessica McCaughey mentions several strategies for how to write a professional e-mail. The author describes ten different strategies for achieving the right tone.  With each strategy she provides examples to illuminate her advice. The webtext closes with an activity students can complete in class or take home and work on.  Certainly, writing professional e-mails is a crucial activity many students need help with, both within the college setting and in their jobs; this webtext provides a useful guide to help them compose professional e-mails. 
"Creating Scienftic Posters" by Candice Welhausen, The University of Delaware
 In this  webtext , Candice Welhausen walks through the standard conventions and characteristics for composing scientific posters. She describes the common venues in which they are displayed, how to organize a poster in IMRaD format, the different expectations of expert and non-expert audiences, and quick tips about such considerations as spacing, color, and readibility of different fonts. Her multimedia text invovles instructional videos and an actual student poster, as an example. This webtext would be enormously helpful in a technical communication course. 
Please feel free to contact me if you have any questions regarding ideas for original webtexts. 
Quentin Vieregge, This email address is being protected from spambots. You need JavaScript enabled to view it.
Managing Editor
In the News
Thanks to Aaron Barlow, Faculty Editor, and Academe, sponsored by the American Association of University Professors, for publishing this account of the early history of Writing Commons:
“Open Textbook Publishing.”  Academe.  (Sept/Oct 2013): 40-43 <  https://www.aaup.org/article/open-textbook-publishing#.UkXER1Oi9GY >
USF M.A./Ph.D. Program in Rhetoric and Composition
The University of South Florida invites applications from promising undergraduate or MA students who are interested in graduate study in Rhetoric and Composition. 
USF''s Rhetoric and Composition program < https://english.usf.edu/graduate/concentrations/rc/degrees/ >
promotes direct contact between graduate students and faculty, as evidenced by co-authored books, articles, conference presentations, grants, and co-teaching.  
 Call for Papers
Teachers, please share with us your expertise.  You can find the most up-to-date submission information and Call for Papers (CFPs) at our  Contribute  page. We seek new and interesting webtexts to expand the breadth and depth of what we can offer our global community of writers. Our past CFPs have focused on  information literacy ,  professional and technical writing , and  creative writing .
Social Media
 Visit us at  Facebook  page. View newsfeeds regarding Writing Commons and updates about Open Education.
Don''t forget to connect with Writing Commons on  Twitter  using @writingcommons and #writingcommons. Writing Commons'' tweets consist of answers to students'' most common writing questions, such as "What''s a paragraph supposed to have?" and "What''s Rogerian argument?" Each tweet is hyperlinked to our Writing Commons blog where Writing Commons staff members provide succinct, accessible answers and helpful examples.
If you have received this newsletter in error or no longer wish to receive "UnCommon News," please use the following link to  Unsubscribe . 
', '2018-12-07 19:27:27');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6731, 'Hits: 3822
Amanda Wray
Instructor, Department of Literature and Language as well as Gender, Women, and Sexuality studies, University of North Carolina Asheville
', '2018-12-07 19:27:27');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6731, 'Editor of New Media Communication and Research Methods and Methodologies
Full-Time Instructor of Humanities and Communications, Embry-Riddle Aeronautical University, Daytona Beach, FL
', '2018-12-07 19:27:27');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6738, 'Posted on January 1, 2018 by Wendy Hanamura
This month we were powered by 75,000 donations big and small.  One supporter in Zimbabwe even sent us Twenty Billion dollars!  But what keeps us going is, quite simply, you.  Your words of encouragement and support remind us why we do what we do.  They make us want to do more.
We asked our supporters “Why do you donate to the Internet Archive?” Here’s a selection of recent replies:
I love the Archive and I love the Wayback Machine. What is true may not always be clear. But by looking at the past we can see what is and isn’t true. You enable that vital process to occur. Thank you! — Jack
I donated because, “we can only keep what we have by giving it away.”  — Joe
I have used your site many times. I was homeless and broke. Said if I ever had anything to give, I would be sure to do so. So here I am.  –Rebecca
Where would any of us be without the Wayback Machine? — Melissa
I’m a university professor and depend heavily on the Internet for my required readings. In this case, I am having my students read a book published in 1838. How else would I get them access to such a text?  – Madeleine
With the impending closure of Storify and the Library of Congress’s decision to stop archiving Twitter, I was reminded just how vital this service is.  — Lee
You are filling a serious need, the Internet isn’t a safe place for sensitive information and it’s good to know you have our backs.  — Tony
The most trusted name in knowledge for free. What Google wanted to be. I do hope you get the major foundational support you deserve. And support from Old Time Radio and Jazz aficionados like I am. Great literature as well. And film, cartoons.  Archive.org is the cultural library for “the rest of us.”  –Dennis
I’m an amateur historian and writer. I appreciate being able to find information on your site that would otherwise require trips around the country to dusty archives or libraries!  –Rita
Downloaded a few really great live shows from archive.org (especially from Songs: Ohia, Magnolia Electric Co & Cowboy Junkies). With my small donation I wanted to give a little something back to you.  –Roland
Love your old time radio & television programs. You are preserving our history while so many others are trying to change or erase it. I feel your preservation efforts are important to our culture & freedom. Those who forget (or erase) history are doomed to repeat it. Keep up the good work. Wish I could afford more. –-Randy
Why I donated? This pretty much sums it up for me:  “Fixed more than 3 million broken links in Wikipedia using the Wayback Machine.  Saved 200 terabytes of government data that might have disappeared.”  –JVK
Books from your archive have been a big help for my genealogical research. –Jim
It is a great initiative and I’ve saved 50 times the amount I donated with this site. I am more than happy to chip in where I can.  –Em
Hiding society’s collective knowledge away behind copyright paywalls is an enormous problem.  I want to thank you for making it more accessible, and support your effort.       –Jerry
For the love of this site over the years and the continual availability of great live music by incredible musicians…and their fans! — A Fan
Want to hear programs I’ve missed on KPFA Radio, especially Democracy Now!                –Jeanne
KBCA was a Los Angeles radio station in my youth.  Just being able to hear a program broadcast again is a rare, but welcome treat to me.  –Byron
Dear Internet Archive, I have been using this service for years and found all the classic scientific work from the leading scientists of the 20th century. I could not have found these books anywhere else, not even big university libraries.  Excellent efforts,  –Farooq
Your service is extraordinary, necessary and a gift.  Thank you. — Bella
A small gift can go a long way if everyone does so. –Richard
For some extremely obscure but historically important publications, I can now download, thanks to you and similar digitising archives, a digital copy straight to my computer from Melbourne, Australia. Only a decade or two ago, I would have needed to locate one of the few copies that exists, travel to the library in which the copy was held, and sit in a reading room taking notes, or taking one-off photocopies, still not keyword searchable.  Thanks so much.  –Kale
I believe knowledge needs to be preserved, and shared.  Censorship whether it is by government, corporations or individuals, through legislation, fascist threats or economic censorship (ie youtube)  is a detriment to all humankind.  — TD
I get both pleasure and (sometimes) insight from being able to read texts from the original editions, where the visual impact of the content is what the author most probably intended.  One cannot get this in any other way, apart from the very occasional good luck to be given (or to purchase) old editions. The Internet Archive is a wonderful resource.  — Jeremy
Thank you for keeping Venketaramana’s talk on Bhagavad Geetha…it changed my way of thinking. I am still growing. — Nunu
Born in the 1960s but stumbled on old radio as a kid—these shows are my Prozac.  Thank you.  –Julie
I’ve listened to Theatre Five, and MindWebs and other Old Time Radio for years now and couldn’t afford donate. I would push the donate button and turn back every time. I finally can now, and I will again.  –Derrick
I listen to Libra Vox and benefit from finding books online for free in family history work.  You are very valuable.  I appreciate donations that don’t just go to large overhead in top heavy charities.  Thank You for all you do.  –Janeal
I love learning and am so grateful to have a place to learn anything I want!  Thank you for this gift! –Judi
To the Internet Archive & Open Library Communities:  You are an amazing bunch!  Thank YOU for using knowledge in all its many forms to inspire, learn, and enjoy life more fully.  Here’s to growing,  improving, and sharing even more in 2018.
 
', '2018-12-07 19:27:32');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6741, 'About Thanks
About
This project automatically generates high-level information about the OSMF servers. Previously this was maintained on the wiki, but this was both time-consuming and error-prone.
This project takes the information that is gathered on each Chef run, and parses it to gain the relevant information for the pages.
The code to generate these pages can be found at https://github.com/gravitystorm/osmf-server-info
', '2018-12-07 19:27:34');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6742, 'Log in
Contact us
The meipi team thanks you for your comments and sugestions that you send us. This is the easiest way to communicate with us to report errors, translate the tool to other languages or comment anything.
 
Consult the FAQ to see if your problem has already been answered.
 
', '2018-12-07 19:27:34');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6753, '4chan Pass users can bypass this verification. [ Learn More ] [ Login ]
File
Please read the Rules and FAQ before posting.
You may highlight syntax and preserve whitespace by using [code] tags.
There are 14 posters in this thread.
05/04/17
New trial board added: /bant/ - International/Random
10/04/16
New board for 4chan Pass users: /vip/ - Very Important Posts
06/20/16
New 4chan Banner Contest with a chance to win a 4chan Pass! See the contest page for details.
', '2018-12-07 19:27:40');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6761, 'Your comments or questions are welcome! The point of contact is info at opensourceecology dot org.
Discussion Forum
You are encouraged to post your questions, ideas or suggestions on our discussion forum .
Doing so will ensure that many people will read it and you''ll have the chance to exchange ideas and experiences to help each other.
Please read the Vanilla Forums page to learn more
Sharing information
If you have some valuable information to share, you may want to add it to our wiki.
See the wiki instructions page to learn how.
Want to replicate a machine or help your community?
Then please consider discussing your ideas on our replication forum category . Perhaps you can find others who have the same intention living in the same country or city as you.
Don''t have time to build yourself?
Our tools are open, so you can simply build them yourself, using the open source instructions we provide for free.
But if you don''t have the time to build them, you also have the option to buy them instead .
See Also
', '2018-12-07 19:27:46');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6764, 'Contact Us
Contact Us
Here are our contact details if you have any questions relating to the store or products.  We are on Eastern time (UTC/GMT -5 hours) and operate between 9 AM to 5 PM weekdays.  Leave a message on the phone if we don''t pick up!!!  You can also send us an email using the form below.
MakerPlane Inc
', '2018-12-07 19:27:47');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6764, 'The Team
The MakerPlane team are passionate about aviation, open source and the maker movement!
Contributors and volunteers to the project come from all over the world proving that this is truly a global initiative.   Stay tuned as we update this page with bios of the leading contributors to the MakerPlane project.
 
John Nicol
John is the founder of MakerPlane.  A pilot, aircraft homebuilder and maker.  A former New Zealand Army Officer, he has also been in the high-tech industry as an executive in the Public Safety and Defence industries in New Zealand and Canada including as CEO of a Canadian Top 40 Defence company.  John left Lockheed Martin in 2012 as a Principal Engineer and was involved in the launch to market of the Lockheed Martin Prepar3D  flight simulation product.  John lives just north of Ottawa, Canada with his wife and son.
 
Jeffrey Meyer
Jeffrey is the lead aeronautical engineer responsible for the MakerPlane v1.0 LSA design and has over 40 years general engineering experience.  He has a BSc. Mechanical Engineering and MSc. Aeronautical Engineering from the Technion, Israel Institute of Technology.  Jeffrey is currently an adjunct lecturer in the Aerospace Engineering Facility of the Technion.   Jeffrey says, "If it flies I like it, but also dabble in electronics, software development, R/C modeling, and teaching kids and university students about aero modeling.  Private pilot and glider pilot licenses, long expired – too expensive and troublesome over here."
 
Vern Little
Vern Little comes from the semiconductor and networking equipment industry.  As a founder of one of Canada’s largest semiconductor companies, he has expertise in building successful companies including engineering, marketing, business development and venture fundraising in Canada and the US. Vern retired as a director of product research with ten US patents and has built two aircraft—an RV-9A and a Harmon Rocket II.  As the principal of Vx-Aviation, Vern pioneered the use of d-sub packages that contain complex avionics functions and generic wiring hubs that reduce aircraft wiring cost and complexity.  Vern is a technology business mentor in Victoria, BC.
 
Dave Covert
Dave is a software developer and business owner with a BSc. Computer Science and a BSc. Electrical Engineering from Texas A&M University. In the past twenty years Dave has worked for Microsoft and started two technology companies (sold one). He is a SEL pilot who once owned a Grumman AA-5, and with interests in Augmented Reality, CAD/CAM, 3D printing, and fly-by-wire technologies, now finds himself lusting after a LSA to try some ideas out on. His guiding principle in aviation is ‘Flying should be easy.’ Dave lives in the country about 30 miles south of Houston, Texas.
 
Phil Birkelbach
Phil has a degree in Mechanical Engineering Technology from Texas A&M University and is a Project Engineer for a company that designs and builds control systems for the offshore oil and gas production industry. He lives in Waller, a small town 30 miles NW of Houston Texas, with his lovely wife and way too many animals.  He learned to fly when I was 13 years old and soloed a glider at 14.  Phil has a PPL with SEL and Glider ratings. He built his RV-7 from 2001 thru 2005.
 
', '2018-12-07 19:27:47');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6767, '- where one entity designs a complete system for everybody
- which operate according to a hierarchical (vertical) model
Scroll down to learn more.
When we look at modular construction systems we can clearly distinguish two different models:
Within software constructions however we are witnessing the emergence of open modular systems.
Within current hardware constructions we observe the existence of various closed systems.
The OpenStructures (OS) project initiates a construction system where everyone designs for everyone.
It is an ongoing experiment that wants to find out what happens if people design objects according to a shared modular grid , a common open standard that stimulates the exchange of parts, components, experiences and ideas and aspires to build things together.
Scroll down to learn more
Open modular systems
Designer A designs modular system 1
Company B designs modular system 2
Although all these systems enjoy the benefits of modularity within their system,
they most of the time are completely incompatible with one another.
open knowledge sharing
', '2018-12-07 19:27:47');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6768, 'Our Team Join us
OUR TEAM
We are constantly growing, our organisation is made of individuals from around the world who all share a vision of openness and data access for health. We’d love for you to join us on our journey .
OUR TRUSTEES
 
Fiona Nielsen, CEO of DNAdigest
Fiona Nielsen, founder and CEO of DNAdigest and Repositive, is a bioinformatics scientist turned entrepreneur. She used to work at Illumina developing tools for interpretation of next-generation sequencing data and analysing cancer and FFPE samples sequenced on the latest technology. There she realised how difficult it is to find and get access to genomics data for research, which led to DNAdigest being founded as a charity to promote best practices for efficient and ethical data sharing – aligning the interests of patients and researchers. In August 2014 Repositive Ltd was spun out of DNAdigest as an entity to develop and provide a novel software tools and mechanisms for sharing of data.
Dr Mohammad Al-Ubaydli,
CEO of Patients Know Best
Mohammad is founder and CEO of Patients Know Best and has over 15 years of experience in medical software. He trained as a physician at the University of Cambridge; worked as a staff scientist at the National Institutes of Health; and was a management consultant to US hospitals at The Advisory Board Company. He is the author of seven books, including Personal health records: A guide for clinicians and Streamlining Hospital-Patient Communication: Developing High Impact Patient Portals. He is also an honorary senior research associate at UCL medical school for his research on patient-controlled medical records. In 2012 he was elected an Ashoka Fellow as a social entrepreneur for the contributions he has made to patient care. He is the driving force behind the Patients Know Best patient portal software.
Read the interiew with Mohammad here .
Anthony Brookes,
Professor of Bioinformatics & Genomics
Anthony Brookes is a Professor of Bioinformatics & Genomics at the University of Leicester, working on ‘knowledge engineering’ to bridge the divide between research and healthcare. In this context he is Director of the ‘Data-2-Knowledge-4-Practice’ Facility, a joint initiative by the University and the University Hospitals Leicester NHS Trust. He is experienced in disease and population genetics, DNA variation analysis, and data management for biomedical information, and in these areas has published >170 peer reviewed articles and reviews. He served two 3-year terms on the HUGO Council, co-founded the Human Variome Project and the Human Genome Variation Society (now Vice-President), jointly designed the standard Observ-OM data model for gene-disease relationships, runs the world’s largest open GWAS database (GWAS Central), and recently created a new informatics platform (Cafe Variome) for federated discovery of samples, subjects and data. He contributes his wide experience to the Diagnostic Committee in IRDiRC, and to various Task Teams in the GA4GH (Co-Chairing one on ‘Automatable Discovery and Access’). Awarded several biotech patents, launched two bioscience SMEs, and participates (often in a leadership capacity) in a range of EU, IMI, and national consortia projects.
 
', '2018-12-07 19:27:48');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6770, 'Sign in to see the full range of help available to OU students.
You are here:
', '2018-12-07 19:27:51');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6770, 'Contact us
Contact us
If you’re an employer and have enquiries about our apprenticeship programmes, staff development and learning solutions, training services and education partnerships, please complete the following form, providing your work contact details. For all other enquiries, including personal development, please visit the main Open University  contact us  page.
If you would like to become an apprentice, visit our  advice and guidance section  for more information.
Fields marked with * are mandatory.
Follow Open to Business on LinkedIn
Follow @OUforBusiness on Twitter
', '2018-12-07 19:27:51');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6770, 'You can leave a message using the contact form below.
Your name
Please tell us about the qualification you are interested in?
Qualification
Please provide details of your query
So that we can create an enquirer record for you please can you tell us:
Postcode
First line of your address
Your Personal Identifier (optional)
', '2018-12-07 19:27:51');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6770, 'Who we are and what we do
Home Library Information Who we are and what we do
Vision
Our vision is to be a trusted, leading-edge service at the heart of the Open University learning experience.
Mission
Library Services supports the teaching, learning and research of the University by ensuring that all staff and students have access to world-class resources. We also provide a centre of excellence for information management and preservation, and are focused on equipping students with the essential skills and capabilities to enable their self-directed learning and for them to succeed in the workplace.
Supporting students and staff
Information literacy and module production
Library Services staff in the Student and Academic Services team work closely with module teams to create opportunities for students to develop their digital and information literacy skills whilst studying.  Digital literacy refers to the skills of OU students in using digital technologies to achieve their personal, study and work-related goals. The team also work with faculties to help select online content to use in modules.
Advice and guidance
An important role of the Student and Academic Services team is to provide help and support to staff and students. The Library Helpdesk provides help and support to both students and staff, which is available 24 hours, 365 days of the year through webchat. Online training sessions also provide students with the opportunity to develop essential study skills in areas such as referencing and finding information for their assignment.
Research Support
The Library Research Support website offers guidance on Open Access Publishing, Research Data Management, Bibliometrics, ORCID and managing researcher visiblity.
Access to trusted educational material
Library Services connect students and staff to an accessible and seamless world of online content. This world of online content, managed by the Digital Content and Systems team, includes journal and magazine articles, newspapers, encyclopaedias and dictionaries. All resources can be accessed 24 hours a day.
University Archive
The University Archive holds historical materials of the Open University and unique research collections. The University Archive also manage the Open University Digital Archive (OUDA) that helps to manage and preserve digital and digitised OU archive content, as well as making materials more widely accessible.
Library projects
Library Services’ broad range of projects are focussed on supporting our organisational objectives as well as sharing our learning and findings with the broader community.
Want to find out more?
Take a look at the support and services for students or support and services for staff pages for more information.
Get in touch with us
If you’d like to get in touch with the OU Library please contact us .
Library Information
', '2018-12-07 19:27:51');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6771, 'One pager (PDF)
What?
Free education opportunities on the meaning, application, and impact of “openness” as it applies to education, science, research. We offer online courses, face-to-face workshops, and innovative training programs on topics such as Creative Commons licenses, open educational resources, and sharing creative works. Learn about the meaning, application, and impact of “openness” as it applies to education, science, research.
Why?
Universal access to and participation in research, education, and culture is made possible by openness, but not enough people know what it means or how to take advantage of it. We hear about Open Source Software, Open Educational Resources, and Open Access… But what are these movements, who are their communities, and how do they work? Most importantly—how can they help me?
Learning about “open.”
Whatever the field, we seek to help people do what they already do better with the aid of open resources and tools. For example, “open” can help: educators find free, useful resources online; scientists share research and data; artists find music and images for remix.
Who is involved?
We are a community of volunteers from organizations and regions all over the world. The School of Open community is coordinated by Creative Commons (CC) and the Peer 2 Peer University (P2PU).
Creative Commons is a globally focused nonprofit dedicated to making it easier for people to share and build upon the work of others, consistent with the rules of copyright. P2PU is a peer learning community for developing and running free online courses. All School of Open courses and resources are licensed to the public under a Creative Commons Attribution-ShareAlike or more open license.
', '2018-12-07 19:27:51');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6773, 'Contact
Connect with the OpenSpending community.
The best place for OpenSpending-related discussions is our forum , but you can interact with OpenSpending on your favorite social network by following the links below:
Open Knowledge Discussion Forum
', '2018-12-07 19:27:51');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6774, '4Editing
Step 4 : Editing
Throughout the Book Sprints process, your contributors read, restructure, edit, and reorganise the text. Our facilitators empower each contributor to make decisions in this process, yet ensure that editing is done respectfully. They may pair an expert with a non-expert to work together to make technical language accessible. A designated "target reader" or a small group may be asked to revise the entire book from beginning to end.
Step 5 : Producing the Book
Group
5Production
Step 5 : Producing the Book
Our team of copy editors and designers work alongside the contributors using our collaborative writing platform. Your contributors choose a title, a cover design, and illustrations. Based on the final version our team produces print-ready PDF and EPUB versions of the book. The book is then made available immediately to be distributed through your preferred channels, online, as print copies, or through a publishing house.
Step 1 : Conceptualising
', '2018-12-07 19:27:52');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6779, '©2018 University of California
 
SETI@home and Astropulse are funded by grants from the National Science Foundation, NASA, and donations from SETI@home volunteers. AstroPulse is funded in part by the NSF through grant AST-0307956.
Generated 3 Aug 2018, 11:52:41 UTC
', '2018-12-07 19:28:01');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6781, 'Press enquiries
We encourage conversations to take place via our forums . This helps keep things open and allows the whole network to follow along, participate, and stay informed. If you have an idea, comment or discussion point, please participate on our forums . This is our broadest and most open conversation.
If you’d like to get in touch about a specific project , event, or community activity please refer to those websites for details.
If you’re looking for a particular person on Open Knowledge International staff, please refer to our team page .
Please direct non-electronic communications to our postal address and registered office:
Open Knowledge International, 86-90 Paul Street, London, EC2A 4NE, United Kingdom
We''d love to discuss potential open data projects with you. Fill in the form below and we''ll get back to you as soon as possible.
Name
', '2018-12-07 19:28:04');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6781, 'About Us
“Liberating Knowledge for Opportunity”
Open Knowledge Nepal is a non-profit open network of mostly young open knowledge enthusiasts. It is the working group of Open Knowledge International in Nepal founded in February 2013. The group has been involved in research, advocacy, training, organizing meetups and hackathons, and developing tools related to Open Data, Open Government Data, Open Source, Open Education, Open Access, Open Development, Open Research and others. The family of the group contains professors, students, activists, journalists, government officers from various fields as its members and supporters. The group also helps and supports Open Data entrepreneurs and startups to solve different kinds of data related problems they are facing through counseling, training and by developing tools for them.
Community Objective
Promoting the vision of Open Knowledge.
Organizing the local and global program related to Open Knowledge.
Creating and running different Open Knowledge working group in Nepal like: Open Government Data, Open Science, Open Economic, Open Research etc.
Developing tools, project and resources related to Open Knowledge and sharing it with the world.
Doing research and survey to map the situation of Open Knowledge in Nepal.
Collaborating / Working with local and international open network to improve the situation of Open Knowledge in Nepal.
Core Team
Prakash Neupane | Strategic Officer
Prakash Neupane is the strategic leader of the organization. His role is to make plans, buildup strategy and implement, evaluate and adjust them as per the requirement. He started his journey of open knowledge by taking the role of Ambassador at an early stage. The team will be able to fulfill the core objectives with his visionary guidelines.
 
Nikesh Balami | Chief Executive Officer
Wanna be a Civic Entrepreneur, Lone Traveler, and forbearing Listener, Nikesh represents two non-profit organization – Open Knowledge Nepal and Code for Nepal, both working to improve digital literacy and open data sector of Nepal. He is a strong supporter of Open Source Software and believes in the vision of Open Knowledge to empower citizens regarding technology usage and Open Data. He serves different open communities of Nepal as a volunteer and his main mission is to create transparent and accountable society by using the potential of civic technology. He kills his frustration and enjoys his success by writing an article, essay, poem, and blogs. With gifted Event Management and Research skills, He loves Coordinating, Communicating, Team Building, Brainstorming, Blogging and Tweeting. He had completed Diploma in Computer Engineering and currently pursuing his bachelor degree from Purbanchal University. Nikesh is a Shuttleworth Flash Grantee 2016 and U.S. Embassy Youth Council member 2017/18.
Kshitiz Khanal | Chairperson
Kshitiz Khanal is the Chairperson of Open Knowledge Nepal. He has an academic background in mechanical engineering and sustainable energy planning. He is currently involved in research about using Open Data to achieve Sustainable Development Goals and also practices as a mechanical design consultant. He believes in solving pressing problems through open and interdisciplinary innovation. His major areas of interest in Open Knowledge include open data, open science hardware, and open education.
Sagar Ghimire | Chief Technical Officer
Sagar Ghimire is Chief Technical Officer at Open Knowledge Nepal. He is an IT professional and an open source enthusiast with an extensive experience in web development and passionate about using web technology and open data to make a better future for everyone. He has been working as a freelance web developer. Likewise, he is also a strong supporter of knowledge sharing and social entrepreneurship. To ensure to attain all the academic knowledge, he has completed diploma and currently an active student of B.E computer engineering.
 
Firoj Ghimire | Chief Marketing Officer
Firoj Ghimire is an enthusiastic promoter of open knowledge in Nepal. He has worked extensively in diverse open data projects. He is an outspoken advocate for openness and aims to revolutionize the society through its practice. He is currently pursuing an undergraduate degree in computer science and information technology.
 
Shubham Ghimire | Chief Operating Officer
Shubham Ghimire serves as the Chief Operating Officer of Open Knowledge Nepal and is responsible for the day-to-day operation and general management of the company. Shubham is a computer science student pursuing his undergraduate degree from Tribhuwan University. He is a strong advocate for the open data movement along with overall open programs campaign in Nepal. He considers himself as an Open Knowledge Activist who is also committed towards the Open source, open data awareness programs, and campaign. In fact, he oversees the project management for Open Knowledge Nepal while also being involved with various other open communities of Nepal. His efforts are dedicated to empowering the people by bringing accountability and transparency in governance. He also dedicates his time in various researches and is a competent programmer.
Categories
', '2018-12-07 19:28:04');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6786, 'Close
About SourceForge
SourceForge is an Open Source community resource dedicated to helping open source projects be as successful as possible. We thrive on community collaboration to help us create a premiere resource for open source software development and distribution.
With the tools we provide , developers on SourceForge create powerful software in over 500,000 projects ; we host millions of registered users. Our popular directory connects more than 33 million monthly users with all of these open source projects and serves more than 4 million downloads a day.
IT professionals come to SourceForge to develop, download, review, and publish open source software. SourceForge is the largest, most trusted destination for Open Source Software discovery and development on the web.
', '2018-12-07 19:28:11');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6791, 'What would you like to talk to us about?
I agree to receive information about Canonical’s products and services.
In submitting this form, I confirm that I have read and agree to Canonical’s Privacy Notice and Privacy Policy .
Submit
', '2018-12-07 19:28:17');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6791, 'Contact us
Contact Canonical
Just fill in the form below and a member of our team will be in touch within one working day.
About you
What would you like to talk to us about?
I agree to receive information about Canonical’s products and services.
In submitting this form, I confirm that I have read and agree to Canonical’s Privacy Notice and Privacy Policy .
Submit
Want a fully managed private cloud?
BootStack is your OpenStack private cloud with our experts responsible for design, deployment and availability.
Get BootStack ›
Get Ubuntu
The open source software platform that runs everywhere from the smartphone, the tablet and the PC to the server and the cloud.
', '2018-12-07 19:28:17');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6791, 'Tags: Desktop , Developer , Snap , snapcraft.io
Linux has long been a favourite platform with developers due to the rich array of languages and toolchains available. In this article we highlight 6 IDEs that can boost your productivity. Each IDE is just a Snap away so you can easily craft your complete development workstation in seconds. Here are six of the best IDEs every developer should know about and an additional 14 bonus IDEs mentioned throughout the article for you to discover.
1. Visual Studio Code
Snapcrafters
Visual Studio Code is a fast becoming the code editor of choice for many developers and combines the simplicity of a code editor with what developers need for the edit-build-debug cycle. Intellisense goes beyond syntax highlighting and autocomplete. Debug code right from the editor. Working with Git and other SCM providers is easy. Install extensions to add new languages, themes, debuggers, and to connect to additional services. Get Visual Studio Code from the Snap store or install it on the command-line with: snap install vscode
Atom is another extensible code editor available that provides a similar feature set. For frontend developers Brackets is worth a look or if you work extensively with Javascript and Javascript frameworks then WebStorm will certainly be of interest.
2. Sublime Text
Snapcrafters
Sublime Text is a much loved sophisticated text editor for code, markup and prose. Built from custom components that provide unmatched responsiveness, super fast syntax highlighting and limitless customisation. Get Sublime Text from the Snap store or install it on the command-line with: snap install sublime-text
Notepad++ is an extremely popular code editor for Windows and is now available for Linux via the Snap store for those of you seeking something familiar. If you prefer native Linux applications then notepadqq is Notepad++-like editor providing all you can expect from a general purpose text editor.
3. Android Studio
Snapcrafters
Android Studio provides the fastest tools for building apps on every type of Android device. World-class code editing, debugging, performance tooling, a flexible build system, and an instant build/deploy system all allow you to focus on building unique and high quality apps. Get Android Studio from the Snap store or install it on the command-line with:
snap install android-studio
For Java developers not just targetting mobile IntelliJ IDEA ( Ultimate editon is also available ) is a capable & ergonomic Java IDE for enterprise, web & mobile development supporting Java, Groovy, Kotlin , Scala, Android, JavaScript, SQL and lots of other languages and frameworks. Naturally the grand daddy of Java development environments, Eclipse , is also just a Snap away for those of you with years of muscle memory using this extensible Java IDE.
4. PyCharm
JetBrains
PyCharm Community Edition is a free and open-source IDE which is perfect for pure Python coding. PyCharm knows everything about your code. Rely on it for intelligent code completion, on-the-fly error checking and quick-fixes, easy project navigation, and much more. Get PyCharm CE from the Snap store or install it on the command-line with: snap install pycharm-community
PyCharm Pro is an IDE for professional Python development and provides all the tools you need for productive Python, Web and Scientific development while PyCharm EDU combines interactive learning to provide a platform for the most effective learning and teaching experience.
If Python is not your thing then JetBrains has you covered. RubyMine is a dedicated Ruby and Rails development environment, Goland is aimed at providing an ergonomic environment for Go development and PhpStorm is a PHP IDE that actually “gets” your code with on-the-fly error prevention, autocompletion, code refactoring, zero configuration debugging, and an extended HTML, CSS, and JavaScript editor.
5. Simply Fortran
Jeffery Armstrong
If you’re working with the language of supercomputers then Simply Fortran provides an integrated development environment for Fortran developers, featuring Fortran project and dependency management, advanced editting features, and integrated debugging capabilities. Get Simply Fortran from the Snap store or install it on the command-line with: snap install simplyfortran
If you prefer C/C++ then CLion natively supports C and C++, libc++ and Boost. For SQL data wranglers DataGrip is the multi-engine database environment which supports MySQL, PostgreSQL, Microsoft SQL Server, Microsoft Azure, Oracle, Amazon Redshift, Sybase, DB2, SQLite, HyperSQL, Apache Derby and H2.
6. Postman
Tobias Langendorf
Postman is the complete toolchain for API developers and makes working with APIs faster and easier by supporting developers at every stage of their workflow. Get Postman from the Snap store or install it on the command-line with: snap install postman
Insomnia is a powerful open source HTTP and GraphQL tool belt to debug APIs like a human by offering advanced authentication helpers, templating, and request chaining to help get things done faster. GitKraken is a great Git client that integrates with self-hosted repositories and popular services like GitHub, Bitbucket and GitLab to make working with your remote repositories easier.
Learn how the Ubuntu desktop operating system powers millions of PCs and laptops around the world.
Newsletter signup
Select topics you’re interested in
Cloud and server
', '2018-12-07 19:28:17');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6809, 'About
Who we are
The Internet Engineering Task Force (IETF) is a large open international community of network designers, operators, vendors, and researchers concerned with the evolution of the Internet architecture and the smooth operation of the Internet.
Support the IETF
Key Info
The Internet Engineering Task Force  (IETF)  is a large open international community of network designers, operators, vendors, and researchers concerned with the evolution of the Internet architecture and the smooth operation of the Internet. It is open to any interested individual. The IETF Mission Statement is documented in  RFC 3935 . 
The technical work of the IETF is done in its working groups , which are organized by topic into several areas (e.g., routing, transport, security, etc.). Much of the work is handled via  mailing lists . The IETF holds meetings three times per year. 
The IETF working groups are grouped into areas, and managed by Area Directors, or ADs. The ADs are members of the Internet Engineering Steering Group ( IESG ). Providing architectural oversight is the Internet Architecture Board, ( IAB ). The IAB also adjudicates appeals when someone complains that the IESG has failed. The IAB and IESG are chartered by the Internet Society  (ISOC)  for these purposes. The General Area Director also serves as the chair of the IESG and of the IETF, and is an ex-officio member of the IAB.
Bibliography
[1] RFC 3935
A Mission Statement for the IETF
This memo gives a mission statement for the IETF, tries to define the terms used in the statement sufficiently to make the mission statement understandable and useful, argues why the IETF needs a mission statement, and tries to capture some of the debate that led to this point.  This document spe...
Harald T. Alvestrand
', '2018-12-07 19:28:47');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6815, 'Commotion is led by the Open Technology Institute at the New America Foundation.
1899 L St., N.W., Suite 400
Washington, DC 20036
Fax: 202-986-3696
For questions, comments, support or other inquiries, email us: support at commotionwireless dot net
For general discussion on using the Commotion Wireless software, join the Commotion Discuss mailing list. For in-depth technical discussion or questions, or for discussing the development of the software, join the Commotion Dev mailing list.
', '2018-12-07 19:28:47');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6825, 'Thank you for your interest in Joomla! Community Magazine.
If you would like to contact us regarding:
', '2018-12-07 19:28:52');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6831, 'Text
Buttons
Occupy Sandy is a grassroots disaster relief network that emerged to provide mutual aid to communities affected by Superstorm Sandy.  We''re also a fiscally sponsored project of the Alliance for Global Justice so all donations are tax deductible. Donate Volunteer Contact Us
Email subscription
', '2018-12-07 19:28:54');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6838, 'About Us
In Brief
This is the project site for Evergreen, highly-scalable software for libraries that helps library patrons find library materials, and helps libraries manage, catalog, and circulate those materials, no matter how large or complex the libraries.
Evergreen is open source software, licensed under the GNU GPL, version 2 or later.
The Evergreen Community
The Evergreen Project develops an open source ILS (integrated library system) used by more than 2,000 libraries around the world. The software, also called Evergreen, is used by libraries to provide their public catalog interface as well as to manage back-of-house operations such as circulation (checkouts and checkins), acquisition of library materials, and (particularly in the case of Evergreen) sharing resources among groups of libraries.
The Evergreen Project was initiated by the Georgia Public Library System in 2006 to serve their need for a scalable catalog shared by (as of now) more than 275 public libraries in the state of Georgia. After Evergreen was released, it has since been adopted by a number of library consortia in the US and Canada as well as various individual libraries, and has started being adopted by libraries outside of North America.
The Evergreen development community is still growing, with about eleven active committers and roughly 80 individuals who have contributed patches (as of April, 2016). However, the Evergreen community is also marked by a high degree of participation by the librarians who use the software and contribute documentation, bug reports, and organizational energy. As such, Evergreen is very much about both the developers *and* the users.
Because of the nature of ILSs, Evergreen has an interesting mixture of functionality. For example:
Evergreen is a metadata search engine
Evergreen is a transaction processing engine
Evergreen is just another web application
Evergreen is based on a robust, scalable, message-passing framework – OpenSRF
Getting to know Evergreen
To learn more about Evergreen:
Start with the Frequently Asked Questions and the Evergreen Roadmap
Browse the public demos of Evergreen’s online catalog.
Visit live implementations of Evergreen in libraries large and small, public and academic
Explore developer versions of the Evergreen catalog. These exhibit all the latest & greatest features we’re working on currently.
Note: The development site may be unstable, experimental, missing data, etc.
Download and install the staff client, the server, or a VMWare image of the server
Getting Involved with Evergreen’s Communities
Say hi on our Facebook page
Subscribe to the Evergreen mailing lists and introduce yourself once you’re on board
Or if you’re shy, send an email to a few community volunteers at feedback@evergreen-ils.org
Stop by our Internet Relay Chat (IRC) channels at #Evergreen on the Freenode IRC network.
Note: Most but not all activity is on and off daytime through evening, EST.
Browse the Evergreen blog and comment on the posts
About Evergreen
This is the project site for Evergreen, a highly-scalable software for libraries that helps library patrons find library materials, and helps libraries manage, catalog, and circulate those materials, no matter how large or complex the libraries.  © 2008-2017 GPLS and others. Evergreen is open source software, freely licensed under GNU GPLv2 or later. The Evergreen Project is a member of Software Freedom Conservancy.
Community Links
', '2018-12-07 19:28:57');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6841, 'Automatticians in Santa Cruz, California
At WordPress.com, our mission is to democratize publishing one website at a time.
Open source WordPress is the most popular online publishing platform, currently powering more than 28% of the web. We wanted to bring the WordPress experience to an even larger audience, so in 2005 we created WordPress.com.
We’re a hosted version of the open source software. Here, you can start a blog or build a website in seconds without any technical knowledge.
Overall, the WordPress.com network welcomes more than 409 million people viewing more than 15.5 billion pages each month. Our users publish about 41.7 million new posts and leave 60.5 million new comments each month.
All of us at Automattic  constantly work on improving WordPress.com. We roll out updates almost every day, and develop other services, like Gravatar and Simplenote , to enhance your experience on the web. We want to build products, features, and themes you will love using, so don’t hesitate to  leave us your feedback .
Almost everything on WordPress.com is free, and what’s currently free will remain so in the future. We keep your sites free by offering upgrades for things like Plans  and custom domains , as well as products like  anti-spam software Akismet  and  VIP hosting partnerships with major media outlets.
Whether you’re a blogger or a website owner, we know you have many places where you can pitch your online tent. If you’re a current user, thanks for choosing us — we love having you around. If you’re looking to build your online presence and haven’t decided where to drop anchor, give us a try. We’d love to become your home on the web.
Get Started
', '2018-12-07 19:29:03');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6841, 'LinkedIn
Ready to get started?
No matter where you are in the planning process, we’re happy to help, and we’re actual humans here on the other side of the form.
We’re here to discuss your challenges and plans, evaluate your existing resources or a potential partner, or even make some initial recommendations. And, of course, we’re here to help any time you’re in the market for some robust WordPress awesomeness.
', '2018-12-07 19:29:03');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6851, ' April 15, 2018   fiona
Earth Day Festival “Earth Fest”
Special Earth Day Festival to learn about compost, recycling, energy efficiency, and much more. Play recycling games, learn how to compost, and create upcycled art. Stations include representatives from AIRE, Virginia Cooperative Extension, Environmental Services, Remove Invasive Plants, Creative Resources, Fitness, and more. This activity takes place when school is out, and all ages are invited.
Purpose:
This event is to raise awareness on conservation for environmental protection. Themes include reduce, reuse, recycle. Exhibitors should focus on themes surrounding Earth Day.
Kate Sweet, Freecycle Moderator was present at the event, she says:
‘all the stuff on the table was free and when folks selected an item I told them they did their first freecycle and offered to sign them up on the spot!!!!’
Bath Chronicle: Seven of the weirdest things on Freecycle available to Bath bargain hunters
 February 14, 2018   fiona
Lavender hair dye, a batch of carrier bags and a slightly broken Hammond organ are some of the unusual things currently listed on Freecycle in Bath.
If you’re not aware of the website, it’s a non-profit organisation where people can post their unwanted items for free.
You can get a great bargain, like a three-piece Harrods suite, and it helps stop more reusable items ending up in landfills.
So, here’s a list of some of the strangest, quirkiest things being listed right now.
', '2018-12-07 19:29:19');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6853, 'Do not send support requests via e-mail. You will not receive a response.
If you have a question about Zotero, see Getting Help .
If you have a question about your Zotero storage subscription, or institutional storage plans, you can contact storage@zotero.org .
Developers interested in contributing code, using the Zotero APIs, or otherwise extending Zotero can post to the zotero-dev mailing list . Note that the dev list is not for user support or feature requests.
Members of the press, independent journalists, bloggers, or anyone else who would like to cover Zotero and needs further information can contact press@zotero.org .
Security issues with Zotero or zotero.org that can''t be reported publicly can be reported to security@zotero.org .
For all other issues, please use the Zotero Forums , where you''ll generally receive a response quickly from a Zotero developer or expert community member.
contact_us.txt
· Last modified: 2018/06/05 05:57 by
dstillman
', '2018-12-07 19:29:23');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6853, 'We’re in the process of updating the documentation for Zotero 5.0 . Some documentation may be outdated in the meantime. Thanks for your understanding.
Zotero에 문의하기(Contact Zotero)
이메일을 통해 문의사항을 보내지 마세요. 답변을 받으실 수 없습니다.
Zotero에 대해 문의사항이 있는 경우, “ Getting Help ” 메뉴를 먼저 확인하세요.
Zotero에 관심이 있는 개발자들의 경우 “Zotero APIs”를 사용하시고, 메일링 리스트( zotero-dev mailing list )를 참고하세요. 이 리스트는 사용자 문의응답이나 기능 요청용이 아닙니다
Zotero를 널리 알리고 매뉴얼을 만들고자 하는 사서, 교육기술자, 교수, 대학교직원이나 단체 등은 이곳을 참고하시거나 campus-reps@zotero.org 구글 그룹( Zotero Evangelists Google Group )에 참여해주세요.
언론사, 언론인, 블로거 등 Zotero에 대해 다루고 싶거나 더 많은 정보가 필요한 경우 이곳을 통해 연락주세요. press@zotero.org .
Zotero나 zotero.org과 관련한 보안 이슈는 security@zotero.org 로 연락주세요.
ko/contact_us.txt
· Last modified: 2017/11/12 19:53 (external edit)
Show pagesource
', '2018-12-07 19:29:23');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6853, 'We’re in the process of updating the documentation for Zotero 5.0 . Some documentation may be outdated in the meantime. Thanks for your understanding.
联系Zotero
', '2018-12-07 19:29:23');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6855, 'About
What''s this page about?
The task of the documentation team is the writing of a new manual       for current and future generations of GIMP . The GIMP User Manual is       a newly written User Manual, written for the GIMP Help Browser,       but able to produce the Help pages for other media as well.
Goals?
', '2018-12-07 19:29:26');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6865, 'Home
How to Contact Us
Many developers and some users frequently are available on IRC.  This is often the fastest way to get in contact and get feedback to problems.  You can use any standard IRC client (such as Pidgin ) and connect to any of  the IRC servers of the Freenode network and join us in the #gnunet channel.  If you do not want to download a real client, you can also use Freenode''s web interface at http://webchat.freenode.net/ .
Note that at the moment most developers reside in Europe, don''t expect immediate answer in the middle of the night. The channel is logged however, so there''s a good chance that you will get some answer hours later (you can also check that log yourself to see if someone answered).
If you want to report bugs, read the FAQ and then report the issue to our bugtracker .  Alternatively, you can use the
bug-gnunet mailinglist, but we prefer reports to be tracked via the website.
If you want to receive release-related announcements, you should subscribe to the moderated info-gnunet mailinglist.  It has very little traffic.
For end-user discussions, we''ve created help-gnunet and for developers gnunet-developers .  Both mailinglists are not seeing much traffic right now since most discussions happen in person or on IRC.
Finally, if you want to keep up-to-date with every single bit of development, there is gnunet-svn , an automated, moderated mailinglist for all git commits to the GNUnet project (including also GNU libmicrohttpd and GNU libextractor).  This mailinglist has heavy traffic, at peak times you may see 100 commits a day and typically several commit e-mails every day.
', '2018-12-07 19:29:41');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6871, 'Bottle is a fast, simple and lightweight WSGI micro web-framework for Python. This blog covers new ideas, technical details and other Bottle and web development related stuff. Never miss a release again!
About
Want to learn more about the Bottle Web Framework ?  This blog covers new ideas, technical details and other Bottle and web development related stuff. Never miss a release again!
About Bottle
Bottle is a fast, simple and lightweight WSGI micro web-framework for Python . It is distributed as a single file module and has no dependencies other than the Python Standard Library .
', '2018-12-07 19:29:49');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6874, 'Who are we?
Who are we?
LibreOffice is community-driven and developed software, and is a project of the not-for-profit organization, The Document Foundation .
LibreOffice is developed by users who, just like you, believe in the principles of Free Software and in sharing their work with the world in non-restrictive ways. At the core of these principles are the four essential freedoms and the tenets of The Document Foundation''s Next Decade Manifesto [pdf].
We believe that users should have the freedom to run, copy, distribute, study, change and improve the software that we distribute. While we do offer no-cost downloads of the LibreOffice suite of programs, Free Software is first and foremost a matter of liberty, not price. We campaign for these freedoms because we believe that everyone deserves them.
We seek to eliminate the digital divide and empower all as full citizens, support the preservation of mother tongues, and avoid proprietary software and format lock-in. We work to attain our goals by
providing unfettered access to our office productivity tools at no cost
encouraging the translation, documentation, and support of our software in one''s own language
promoting and actively participating in the creation and development of open standards and Free Software via open and transparent peer-review processes
Though the members of our community hail from many different backgrounds, we all value personal choice and transparency, which translates practically into wider compatibility, more utility, and no end-user lock-in to a single product. We believe that Free Software can provide better-quality, higher-reliability, increased-security, and greater-flexibility than proprietary alternatives.
The community behind LibreOffice is the heart of the project, without which we would not have the resources to continue developing our software. The passion and drive that every individual brings to the community results in collaborative development that often exceeds our own expectations. With dozens of different essential roles in the project, we invite everyone to join us in our work and help us to make LibreOffice known and accessible to all.
The Statutes of the Document Foundation, developed by our own community members, guide the way we work and encourage new members to contribute in a way which benefits both the whole community as well as themselves. Through the use of copyleft licenses such as the GNU Lesser General Public License , Mozilla Public License , and Creative Commons Attribution-ShareAlike License , we commit to protecting your rights as developers and content creators.
Interested in seeing who has already contributed to the LibreOffice project? Please visit our Credits page. We hope to see your name there soon!
To learn more about how the community protects your rights and how the non-profit TDF shepherds projects like LibreOffice, please see our Privacy Policy  and L egal Information .
Despre noi
', '2018-12-07 19:29:51');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6874, 'Who are we?
Who are we?
LibreOffice is community-driven and developed software, and is a project of the not-for-profit organization, The Document Foundation .
LibreOffice is developed by users who, just like you, believe in the principles of Free Software and in sharing their work with the world in non-restrictive ways. At the core of these principles are the four essential freedoms and the tenets of The Document Foundation''s Next Decade Manifesto [pdf].
We believe that users should have the freedom to run, copy, distribute, study, change and improve the software that we distribute. While we do offer no-cost downloads of the LibreOffice suite of programs, Free Software is first and foremost a matter of liberty, not price. We campaign for these freedoms because we believe that everyone deserves them.
We seek to eliminate the digital divide and empower all as full citizens, support the preservation of mother tongues, and avoid proprietary software and format lock-in. We work to attain our goals by
providing unfettered access to our office productivity tools at no cost
encouraging the translation, documentation, and support of our software in one''s own language
promoting and actively participating in the creation and development of open standards and Free Software via open and transparent peer-review processes
Though the members of our community hail from many different backgrounds, we all value personal choice and transparency, which translates practically into wider compatibility, more utility, and no end-user lock-in to a single product. We believe that Free Software can provide better-quality, higher-reliability, increased-security, and greater-flexibility than proprietary alternatives.
The community behind LibreOffice is the heart of the project, without which we would not have the resources to continue developing our software. The passion and drive that every individual brings to the community results in collaborative development that often exceeds our own expectations. With dozens of different essential roles in the project, we invite everyone to join us in our work and help us to make LibreOffice known and accessible to all.
The Statutes of the Document Foundation, developed by our own community members, guide the way we work and encourage new members to contribute in a way which benefits both the whole community as well as themselves. Through the use of copyleft licenses such as the GNU Lesser General Public License , Mozilla Public License , and Creative Commons Attribution-ShareAlike License , we commit to protecting your rights as developers and content creators.
Interested in seeing who has already contributed to the LibreOffice project? Please visit our Credits page. We hope to see your name there soon!
To learn more about how the community protects your rights and how the non-profit TDF shepherds projects like LibreOffice, please see our Privacy Policy  and L egal Information .
About Us
', '2018-12-07 19:29:51');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6876, 'Try Sakai
Thanks to our commercial affiliates , you can get a cloud-hosted trial Sakai site set up in minutes.
Ready to make the transition to Sakai? You can take advantage of our affiliates’ hosted options or download Sakai’s source code to host yourself.
© Apereo Foundation 2014
', '2018-12-07 19:29:53');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6878, 'Releases: We are what we repeatedly do
Releases: We are what we repeatedly do
Published
Tuesday, October 25, 2016 - 15:49
Written by
totten
At the CiviCons and developer meetings this year, we''ve had several conversations about release strategy. The topic is a bit abstract -- touching on a web of interrelated issues of technology and scheduling and business-process. I''ve been searching for a way to explain this topic to people who don''t eat and breathe code in CiviCRM''s git repos -- an analysis which is a bit simpler and more transcendent.
The best analysis predates us by a few years -- Will Durant attributed the idea to Aristotle''s Nicomachean Ethics, paraphrasing:
We are what we repeatedly do. Excellence, then, is not an act, but a habit.
All of us -- users, developers, administrators, core, contributors -- want excellence in CiviCRM. Does that mean we need one grand release to prove our excellence? No, excellence merely means that we review every patch thoughtfully. Excellence means that we have mastery over our use-cases, and that we affirmatively and repeatedly demonstrate the quality of all our systems. Excellence means that compatibility and usability are upfront requirements. Excellence means that every release is production-ready -- because our thinking and our tools and our processes and our values habitually make them so.
As examples, here are some of the habits which help make us excellent:
Study problems or codes before acting on them.
Read and comment on other people''s work. This makes their work better -- and it makes you smarter.
Define clear tests (either manual or automated) -- regardless of whether you work on patches or extensions or deployments.
Do your testing before the release -- not after the release.
Prioritize your testing on the things which seem most important or most risky for you.
Treat compatibility and usability and migration as upfront requirements.
If something requires a break, then make a new space where the breakage is safe. If you absolutely must have breakage in a common space, then coordinate and communicate and schedule proactively.
Don''t be surprised by mistakes. Instead, correct them.
Take care of yourself. Don''t try to fix every problem at once -- focus on a few topics which you can manage well over time.
Leap by Extension. Iterate by Month. (LExIM)
Aristotle and Durant are well-and-good, but they offer high-minded ideals which leave room for a lot of interpretation. More concretely, we''ve been working on a rule of thumb. When planning an improvement for CiviCRM, a developer should ask a simple question: does this idea represent a major leap forward or an iterative improvement?
If a change represents a major leap forward in functionality, it should be developed as an extension. Taking a leap is risky. There can be bugs and edge-cases and small discrepancies between the old way and the new way. Only a few people will be ready to take the leap at first -- but the folks who are ready to take a leap will want to take it quickly. Leaps should generally be prepared as CiviCRM extensions which can be downloaded and enabled separately. The extensions can stabilize on their own schedule -- without posing any risk to existing users. Leaping in this way also encourages modularization.
If a change represents an incremental fix or improvement, it should develop iteratively -- as a carefully-reviewed patch for the existing code. We should take concrete, upfront steps to check the quality of the change and ensure that it still meets the original requirements. This review process should follow a clear schedule so that multiple stakeholders have the opportunity to participate in a timely way.
Consider a few examples:
Example 1: Generating PDF and Word Documents
Discussion: CiviCRM has long included support for using mail-merge tokens to generate PDF documents. But what if you want to generate Word documents instead? The screens and fields and validations are pretty much the same in either case -- we just need a small option to toggle between pdf and doc output.
Solution: Iterate by month. This is a small change to an existing feature. Converting the feature to an extension would be much harder than patching it directly (and the conversion would actually create additional risks). Since we patch the code directly and release the new code to all users, we have strong responsibility to review it carefully from multiple angles.
Example 2: Mosaico
Discussion: CiviMail uses the "CKEditor" library for writing emails with headings, paragraphs, and basic text styling. However, it does not support drag/drop layout of blocks or columns -- and it lacks optimizations for robust display across email clients. The "Mosaico" library does provide these features -- but the user-experience and data-structures are very different from "CKEditor".
Solution: Integration between CiviCRM and Mosacio should be developed as an extension. Users who are most excited about this functionality should install it on their own. After a large number of users have been working with it successfully, we should enable the extension by default.
These two examples are relatively clean. Some situations don''t cleanly break down one way or another, but we can still resolve them if we stay focused on the goals of continuity, compatibility, and reasonable transitions. Here are few more challenging examples:
Example 3: CSS Theming
Discussion: CiviCRM comes with one built-in "look and feel". This determines font-sizes, button colors, visual spacing, etc. However, this "look and feel" has grown outdated. The look-and-feel can be changed by replacing the file "civicrm.css", but doing so would be a very visible disruption (firstly, disrupting users who were trained in the old "look and feel"; secondly, disrupting customizations which tuned-in to the old "look and feel"), and it can take time for a new "look and feel" to reach maturity.
Solution: This involves both a leap and an iteration: the new "look and feel" is a leap which should be delivered as an extension. However, there''s little precedent for "theme extensions" -- because this requires some new APIs. These new APIs must be added incrementally to the main application (without disrupting the old "look and feel").
Example 4: PHP 5, PHP 7, and mysqli
Discussion: PHP 7 made a change to the PHP standard library which required converting CiviCRM from php-mysql to php-mysqli . This posed a risk of breaking existing installations, but there was no reasonable way to package this as an extension.
Solution: Iterate by (three) month(s). The patches were prepared and submitted as part of the monthly release cycle, but they were held-over for extra time. In that period, we did extra research to assess impacts on existing users, tested more thoroughly, revised the patches, held discussions on the mailing-list and blog, and created an in-app advisory to help admins get ahead of the issue.
LExIM is only a rule of thumb. If you can frame your project as a leap/extension or as a small iteration, then you''re on the right track. If it''s not possible, then break it down and focus on these guidelines:
The next release should be compatible with existing installations and customizations. Users who understand the old release should be effortlessly comfortable in the new release. Take affirmative/positive measures to control this.
If it is unavoidable that an important change causes a break, then work to understand it throughly, communicate it, and prepare affected users or systems.
Areas of Improvement
Adopting habits which make for an excellent product requires work. To be sure, we''ve made a lot of progress in recent years, but there are still more areas where we can improve:
We need to strengthen our community-of-practice (skills, documentation, tools, etc) around testing. Tests aren''t just for core -- they''re for extensions and sites, too.
We need to get better about directing information manageably. As the scope of our systems and community have grown, the firehoses of JIRA, Github, etal, have grown too. Developing thematic working-groups and thematic release-cycles enables us to meet with like-minded people and deliver more impact.
Regressions can and will happen -- when they do, we need better feedback processes to manage them more effectively, to act more quickly, to prevent recurrences, and to reduce harms.
If you have other thoughts on how to improve -- how to ensure that every change and every release is something reliable which we can all be proud of -- please feel free to share.
Filed under
', '2018-12-07 19:29:54');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6885, 'Search:
Contact Us
If you have a question or query that is not answered elsewhere on this website, you can contact us. We provide numerous means of contact, depending on your interest in GNOME.
User help
If you have a problem with GNOME software, it is recommended that  you seek help through your distribution. The GNOME Project does provide a number of ways to get support using its software, however:
User documentation: the GNOME Project has an extensive library of help documentation written to help you use our software. You can view this online or use the built in GNOME help browser.
Bug reporting: if you think that you are experiencing an error with our software, you can search and file reports in our bug database .
Contact users, enthusiasts and developers: the GNOME Project  provides several ways to make contact with users, enthusiasts and  developers, including IRC chat and email mailing lists . The #gnome IRC channel and GNOME List mailing list are the most general in scope: use these if you are unsure which list or channel to use.
Partner organisations, sponsors and legal matters
The GNOME Foundation serves as the interface between the GNOME Project and our partner companies and organisations. See the GNOME Foundation site for further details.
Press
See the press releases page for recent announcements.
Website matters
If you have discovered a problem with the GNOME web-site, or wish to help improve our web-pages, you can email the GNOME Website Development list
Reporting security issues
', '2018-12-07 19:29:57');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6885, 'Search:
We promote software freedom
GNOME brings companies, volunteers, professionals and non-profits together from around the world. We make GNOME 3: a complete free software solution for everyone.
Independent
GNOME is led by the non-profit GNOME Foundation. Our board is democratically elected, and technical decisions are made by the engineers doing the work. We are supported by many organizations; employees from over a hundred companies have contributed since the project began.
Free
We believe that software should be developed in the open. Our development infrastructure and communication channels are public, and our code can be freely downloaded, modified and shared with others. All our contributors have the same rights.
Connected
Our project is an important part of the Free Software ecosystem. We work with other free projects to create high-quality solutions that span the entire software stack.
People-focused
Our software is translated into many languages and comes with built in accessibility features. This means that it can be used by anyone, regardless of the language they speak or their physical abilities.
', '2018-12-07 19:29:57');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6885, 'Foundation Announce mailing list  (Posts are held for moderator’s approval)
Telephone
Voice message for the foundation board and administrators can be left at +1-617-206-3947.
Fraudulent checks
The Foundation rarely sends out checks, and never without prior discussion with the recipient. Those checks are usually for travel or other reimbursements for GNOME contributors. If you have received a check in the mail from the GNOME Foundation that you were not already expecting, it is likely fraudulent.
Corporate sponsorship
The GNOME Foundation welcomes contributions from companies and other organizations that share our goals. This is primarily done through sponsorship of our conferences or by becoming a member of the advisory board . Please note that as a 501(c)3, we have limits on the level of attribution we can provide. To avoid donations being classed as advertising, we do not provide links to specific product pages, and all our links are “nofollow”.
Address
', '2018-12-07 19:29:57');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6888, 'Business Learn more about hiring developers or posting ads with us
This site uses cookies to deliver our services and to show you relevant ads and job listings.  By using our site, you acknowledge that you have read and understand our Cookie Policy , Privacy Policy , and our Terms of Service .  Your use of Stack Overflow’s Products and Services, including the Stack Overflow Network, is subject to these policies and terms.
', '2018-12-07 19:30:03');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6908, 'Contact us
How can we help you?
Here are some frequently asked questions we receive (and their answers) which might solve your problem. You can explore our extensive FAQs or click on a question below. Still have a question? Please fill out the form below and we’ll get back to you soon.
', '2018-12-07 19:30:16');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6908, 'About us
We''ve partnered with 600,000+ people living with 2800+ conditions on 1 mission: to put patients first
Imagine this: a world where people with chronic health conditions get together and share their experiences living with disease. Where newly diagnosed patients can improve their outcomes by connecting with and learning from others who''ve gone before them. Where researchers learn more about what''s working, what''s not, and where the gaps are, so that they can develop new and better treatments.
It''s already happening at PatientsLikeMe. We''re a free website where people can share their health data to track their progress, help others, and change medicine for good.
"We started with the assumption that patients had knowledge we needed, rather than we had knowledge they needed. We didn''t have the answers, but patients had the insights that could help us collectively find them."
Jamie Heywood, Co-founder and Chairman
By actively involving people in their own care, we''re changing lives…
Sounds great, but how does it work? Let''s break it down:
People like you share symptoms, treatment info, and health outcomes.
PatientsLikeMe turns that into millions of data points about disease…
…and aggregates and organizes the data to reveal new insights.
We share back what we''ve learned with everyone — that''s our give data, get data philosophy.
Then, we share the patient experience with the industry so they can develop better products, services, and care.
…and igniting a digital health revolution
What makes and keeps us healthy, and what can we do daily to thrive?
These are the questions we''re striving to answer. And we''ve got a plan to get there — by creating a far more advanced network that combines even smarter, more comprehensive data to bring greater precision to healthcare decisions.
We''re working to bring you a new patient experience with more options, more information, and a roadmap to living better.
We''re unleashing the power of data for good…
Each time someone shares their experience, they''re helping the next person diagnosed learn what could really work for them, and helping researchers shorten the path to new treatments. We call it data for good — and here''s how it''s making a difference for patients and the healthcare industry:
PatientsLikeMe refuted the results of a clinical trial on the effects of lithium carbonate on ALS, marking the first time a peer-to-peer network was used to evaluate a treatment in real time.
Since 2016, our patient-reported data has been helping the FDA gain new insights into drug safety.
A study in Neurology shows that after using PatientsLikeMe, veterans living with epilepsy improve their self management and self efficacy.
…by empowering people to take control of their health…
But don''t take it from us — our members share in their own words how they learn, connect with others, and track their progress on PatientsLikeMe, and how that''s helped them live better, together.
"I researched other people who have gone through the clinical trials I was going to be doing, and I was able to make an informed decision… You feel less alone on the site, like you''re not the only one going through this."
Laura, living with IPF
"I''ve learned a lot from what people''s experiences are…It''s very helpful to share our stories and our data, because it''s the only way we''re going to find a cure."
Ed, living with Parkinson''s Disease
"I learned that every spring I have an episode…so now my doctor and I start preparing for whatever''s coming. It''s been a long time since I had any episodes in March because I''m prepared. I have that data to link all the pieces together."
Allison, living with bipolar
…because we believe real-world evidence can change the healthcare system
PatientsLikeMe members aren''t just transforming their own lives, they''re helping to transform the very system that serves them. We analyze their data and experiences to create a new kind of medical evidence and truer picture of human health — and then we bring that evidence to our partners to help them fill the gaps in how they treat their patients.
From pharmaceutical companies to research institutions, we team up with leaders in the industry to bring the patient perspective to their products, services, and care.
AstraZeneca uses PatientLikeMe''s global network to support patient-driven research initiatives.
Takeda Pharmaceuticals is partnering with PatientsLikeMe to find new ways to transform medical research and healthcare decision making to improve patients'' lives.
We''re teaming up to give patients and researchers a more complete picture of patients'' experiences with cancer treatments.
And it all started with Stephen — here''s our story…
PatientsLikeMe was born out of frustration. When Stephen Heywood was diagnosed with ALS in 1998 at the age of 29, his brothers Jamie and Ben tried to treat his symptoms and slow his disease as it progressed. But finding information to guide their decisions was time-consuming and difficult.
Jamie and Ben, along with family friend Jeff Cole, initially launched PatientsLikeMe to connect ALS patients, but it quickly expanded, and in 2011, we opened the website to all patients and all conditions. Today, 600,000+ people use PatientsLikeMe to report on their experience with 2,800 conditions.
They''ve generated 43 million data points about disease, creating one of the largest repositories of patient-reported, cross-condition data available today. It all started with Stephen, and now it''s helping many more improve their outcomes, and doing a whole lot of good.
', '2018-12-07 19:30:16');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6943, 'Creating our future all together
The reward-based Italian crowdfunding platform for creative talents, startups and companies
Share your idea, seek out the web''s support to realise it, and discover and pre-order exclusive products and services: all this is Eppela, a meeting place for creative talents that aspire to emerge and their communities.
Share your idea, seek out the web''s support to realise it, and discover and pre-order exclusive products and services: all this is Eppela, a meeting place for creative talents that aspire to emerge and their communities.
65%Successful projects out of the total of those published
4.000 +Funded projects
3.500.000€ +Co-financing provided by mentors
25 MLN +Pledged turnover
Crowd Runner:It''s worth supportingCrowdfunding is changing the market. Check out the video!
It''s simple, free and merit-based
It''s simple, free and merit-based Present your idea and give life to a crowdfunding campaign at no cost. Right from the beginning of your journey, a personal advisor will give you the most useful advice for structuring a successful campaign.
Your community, your strength ''Web people'' are the most important resource for realising your project. Involve them, make them feel an integral part of a story, make them part of your adventure. Propose rewards which are attractive to your sponsors. Success is just around the corner.
Support kitfor your campaignDownload now!
Sponsor an idea and decide on the future
Rewards and zero risks Supporting a crowdfunding project means revolutionising market rules, believing in a creator''s idea and ensuring that they get the success they deserve. Eppela uses stripe.com for payments, guaranteeing maximum data and transaction security.
Preorder exclusive products The products and/or services present on Eppela are original and not yet on the market. They are a sort of market test whose success depends on the enthusiasm of the creator''s community.
Promote the project and conquer the net
People have the power Promoting your project is essential for achieving the desired results. From your closest friends, to acquaintances, and sharing your project across the web, remember: your success will be theirs too.
Thank them and amaze them! Reward those who have believed in you. Thank every sponsor, whatever their contribution - all contributions are vital. Make your project special, add new rewards or create live initiatives during your crowdfunding campaign
', '2018-12-07 19:30:39');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6944, 'Webinar
Manufacturing 101: Everything You Need to Know About Manufacturing
Get the knowledge and insights you need from electronics manufacturing experts through this free webinar hosted by Arrow & Indiegogo.
Learn tips and tricks on how to navigate the challenges of the manufacturing process, and advice on finding solutions that work. We’ll answer all of your questions about production timeline, costs, sourcing, and more.
Speaker: Michael Murphy, Vice President of Global Manufacturing Services, Arrow Electronics
When: Thursday, May 17, 2018
Time: 10:00am to 11:00am PST
Together, Arrow & Indiegogo support tech startups from ideation through production via the Arrow Certification Program. Whether it’s engineering design tools, discounts on parts or funding, Arrow and Indiegogo will match you with the resources and experts you need. Learn more and join the Arrow Certification Program here . 
Share
', '2018-12-07 19:30:39');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6945, 'Subscribe to our monthly newsletter for the latest TechHub news, updates and opportunities!
Subscribe
Privacy
Copyright © TechHub Europe Ltd,
We use cookies to improve your experience on our website. You can withdraw consent or change your cookie choices at any time through your browser. Accept Read More
Privacy & Cookies Policy
', '2018-12-07 19:30:40');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6945, 'Contact Us
About Us
TechHub is the global community for tech entrepreneurs and startups. We support the growth of over 750 companies across the world, whether they''re founders getting started, or fast-scaling 50 to 100-person teams.
TechHub''s approach is different to that of an accelerator - we don’t take equity, invest in or impose success metrics or membership time limits on our startups.
We work exclusively with tech product startups and scale-ups by understanding the process they go through and the needs they have at each stage. We support startups across all stages of their development - from ideation to exit.
TechHub members build enterprise and consumer technology product businesses within a wide variety of industries and verticals including fintech, adtech and media, security, retail, edtech, entertainment, health and medtech, sport and many more.
Our members benefit from the flexibility and freedom to build their businesses and develop their products how and when they want, with the consistent support of our dedicated teams and a community of like-minded peers.
Read more below to learn about our story and the ways in which we support startups.
2010
TechHub launches . Our first community space opens in London''s Silicon Roundabout, with Google as a founding sponsor.
2011
TechHub launches its second location worldwide in Riga , Latvia.
2012
TechHub announces partnership with Campus London, Google for Entrepreneurs'' first Campus community.
TechHub announces the launch of its third location worldwide: Bucharest , Romania.
2013
TechHub announces global membership at all locations within the TechHub network.
Facebook acquires TechHub member Monoidics.
TechHub announces partnership with British Telecom and the launch of the BT Infinity Labs programme.
TechHub launches second UK location in Swansea , Wales.
2014
TechHub launches its first location for scale-ups in London''s Silicon Roundabout, with London Mayor Boris Johnson attending the offical opening .
Twitter acquires TechHub members Second Sync .
TechHub members Divide, who raised Series B investment over their TechHub membership, are acquired by Google.
TechHub expands to Asia, launching its first location in Bangalore, India.
2015
TechHub opens its largest worldwide location for scale-ups , in London, UK.
TechHub launches in Spain , extending its partnership with Google for Entrepreneurs to Campus Madrid.
TechHub launches in Poland , extending its partnership with Google for Entrepreneurs to Campus Warsaw.
2016
TechHub announces global partnership with Google for Entrepreneurs, giving all TechHub members worldwide access to Google programmes and support.
TechHub alumni Nexmo (who grew from 3 to 70+ and raised a $18M funding round while at TechHub) are acquired by US-based Vonage.
TechHub alumni SwiftKey (who raised Series B funding over their time as TechHub members) are acquired by Microsoft.
2017
TechHub member Wercker is acquired by Oracle .
Callsign raises $35 million , the largest-ever Series A investment closed by a TechHub member.
', '2018-12-07 19:30:40');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6945, 'Contact Us
About Us
TechHub is the global community for tech entrepreneurs and startups. We support the growth of over 750 companies across the world, whether they''re founders getting started, or fast-scaling 50 to 100-person teams.
TechHub''s approach is different to that of an accelerator - we don’t take equity, invest in or impose success metrics or membership time limits on our startups.
We work exclusively with tech product startups and scale-ups by understanding the process they go through and the needs they have at each stage. We support startups across all stages of their development - from ideation to exit.
TechHub members build enterprise and consumer technology product businesses within a wide variety of industries and verticals including fintech, adtech and media, security, retail, edtech, entertainment, health and medtech, sport and many more.
Our members benefit from the flexibility and freedom to build their businesses and develop their products how and when they want, with the consistent support of our dedicated teams and a community of like-minded peers.
Read more below to learn about our story and the ways in which we support startups.
2010
TechHub launches . Our first community space opens in London''s Silicon Roundabout, with Google as a founding sponsor.
2011
TechHub launches its second location worldwide in Riga , Latvia.
2012
TechHub announces partnership with Campus London, Google for Entrepreneurs'' first Campus community.
TechHub announces the launch of its third location worldwide: Bucharest , Romania.
2013
TechHub announces global membership at all locations within the TechHub network.
Facebook acquires TechHub member Monoidics.
TechHub announces partnership with British Telecom and the launch of the BT Infinity Labs programme.
TechHub launches second UK location in Swansea , Wales.
2014
TechHub launches its first location for scale-ups in London''s Silicon Roundabout, with London Mayor Boris Johnson attending the offical opening .
Twitter acquires TechHub members Second Sync .
TechHub members Divide, who raised Series B investment over their TechHub membership, are acquired by Google.
TechHub expands to Asia, launching its first location in Bangalore, India.
2015
TechHub opens its largest worldwide location for scale-ups , in London, UK.
TechHub launches in Spain , extending its partnership with Google for Entrepreneurs to Campus Madrid.
TechHub launches in Poland , extending its partnership with Google for Entrepreneurs to Campus Warsaw.
2016
TechHub announces global partnership with Google for Entrepreneurs, giving all TechHub members worldwide access to Google programmes and support.
TechHub alumni Nexmo (who grew from 3 to 70+ and raised a $18M funding round while at TechHub) are acquired by US-based Vonage.
TechHub alumni SwiftKey (who raised Series B funding over their time as TechHub members) are acquired by Microsoft.
2017
TechHub member Wercker is acquired by Oracle .
Callsign raises $35 million , the largest-ever Series A investment closed by a TechHub member.
Want more TechHub info, bios and stats? Head on over to our:
', '2018-12-07 19:30:40');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6945, '3rd Floor Salarpuria Business Centre
4th B Cross Road, 5th Block, Koramangala Industrial Layout
Bengaluru, Karnataka 560095
', '2018-12-07 19:30:40');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6952, 'Asylos is a pan-European network of volunteers
researching information for asylum claims.
 
Meet the Asylos team
Our vision
The Asylos network’s volunteer researchers work to ensure that every asylum seeker has a fair opportunity to claim his or her right to protection. Evidence to prove persecution or to support the credibility of an individual testimony is a vital element in decisions taken to grant or withhold asylum. Still, many refugees and their legal representatives cannot access this information. As a result, our legal system fails to protect them.
To redress this imbalance and protect the right to asylum we work to:
ensure that asylum seekers and their legal counsel have access to crucial sources and data to substantiate their claim
We believe that refugees and their counsel should have equal access to information and sufficient resources to conduct the needed research. Those helping to defend asylum seekers’ rights face great difficulties when researching information: it often requires understanding local languages and experience in researching complex human rights issues, as well as access to expert evidence in the countries of origin. However, asylum procedures, which have been drastically shortened in many European countries, and limited legal aid budgets don’t allow resources or time for sufficient research.
ensure that the asylum procedure is evidence-based and unbiased
We believe that decisions on individual claims should be fair and based on proper evidence instead of preconceptions. Country-of-origin information reports produced by public administrations should meet the highest standards of our legal system and be driven by respect for human rights. Information compiled by public authorities should be accessible to the public. Asylum courts should rule based on the best available evidence and data. Officials with decision-making power regarding asylum claims should have received sufficient training to be able to make evidence-based decisions.  They need to be aware of the latest developments in the countries of origin of the asylum seekers.
How we work
To access very detailed information from remote areas, and often from conflict zones, we use investigative research techniques. Collectively, Asylos’s volunteers can conduct research in more than 20 languages. After consulting relevant publications from human rights NGOs, governments, international organisations and international and local media, we can fill information gaps by conducting interviews with experts, such as NGO professionals in the countries of origin, academics, or journalists. In addition, we leverage online tools and social media for obtaining evidence for asylum claims: for example, we can use social network analysis to obtain information about events or people, satellite imagery analysis to prove the existence of specific places, and domain registration records to identify website owners.
In addition to using existing datasets, we’ve built internal search engines to more effectively search sources we trust, including sources in local languages, and to categorise and share information through bookmarking services.
 
Researching case-specific information
Our research is free of charge and conducted at the request of asylum lawyers and NGOs assisting asylum seekers with their claims. Our reports detail human rights violations in specific countries or investigate specific facts relating to the claimant’s testimony.
 
 
Producing new sources
In addition to our case-specific work, we collaborate with other NGOs to produce new sources to fill gaps in country-of-origin information.
 
 
Promoting better procedures
We also gather data about the use and impact of evidence in asylum courts. Learning what works in court enables us to advocate for better procedures and fairer decision making on asylum claims.
 
Our impact
During the past six years, we have produced nearly 300 research notes and helped many individuals claim their right to asylum. Lawyers tell us that they would not have had the resources to conduct thorough country-of-origin information research without our help. Our research notes are quoted in court decisions and have been used by UNHCR to inform UN eligibility guidelines. Through our research we have helped counter misinformation and stereotypes in asylum courts and administrations.
  
', '2018-12-07 19:30:44');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6957, 'Vérificatrice des comptes: Michèle Tilman
CommunityForge pays special attention to various aspects of its services:
A constantly evolving platform by a constant development of our own tools. A team that is committed to facing new challenges and to maintaining IT safety. Your data is secured and protected by our regularly updated safety measures.
Confidentiality of data: Yourdata protected on our platforms are not transmitted to third parties
Transparency: An accounting team that reports, on the site or during the General Meeting, about the association’s accounts, “gifts”, “expenses”, “cost evolution.”
Free services: Our “software, hosting, support”services are proposed for free, we subsist thanks to the gifts from certain communities that we host.
The proximity: Evenif we are separated by great distances, we are close to you in our actions, our support and our assistance.
CommunityForge support France   the French branch of CommunityForge, created to ensure the proximity and the follow up that our French friends needed in order to facilitate contacts, meetings, and to allow gifts made in checks.
Matthieu Fémel
', '2018-12-07 19:30:44');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6959, 'About Us
Background
The CMA is a non-profit making organisation founded in 1983 to support community radio – and our remit has expanded to include community television and community-based Internet projects. The CMA represents the community media sector to Government, industry and regulatory bodies.
Our membership brings together established organisations, aspirant groups and individuals within the sector. The CMA provides a range of advice, information and consultancy, offering support to anyone with an interest in the sector.
Working for the Community Media Sector
Much of the CMA’s work has a strategic emphasis and the organisation has been intensely involved in liaison on behalf of the community broadcasting sector with Government, the regulator Ofcom and other strategic bodies regarding actual or proposed legislation and regulation of:
Community Radio
', '2018-12-07 19:30:45');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6959, '0114 279 5219 – office hours 10am – 6pm Monday to Friday excluding Bank Holidays.
By email
Our general email contact address is info@commedia.org.uk . To contact one of our staff members directly, please use the email addresses listed on the Our Staff page.
Find us
Our offices are located in The Workstation , in Sheffield city centre, just a short walk from the city’s railway station. The map below shows where we are in Sheffield.
', '2018-12-07 19:30:45');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6959, 'Ofcom has published details of giving existing community radio licensees an opportunity to apply to Read more +
Sign up for our monthly newsletter
Sign up for our monthly newsletter and view the archives here:
© 2018 Community Media Association  |  15 Paternoster Row, Sheffield, S1 2BX, UK
A not-for-profit limited by guarantee company registered in England and Wales as the Community Media Association.
Tel: 0114 279 5219  |  Fax: 0114 279 8976  |  Email: info@commedia.org.uk | membership@commedia.org.uk
Company No: 02798488 | VAT No: 945 3988 70
Send to Email Address
', '2018-12-07 19:30:45');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6959, 'Community Media Association > What We Do
What We Do
As the UK representative body for the community broadcasting sector, the Community Media Association (CMA) is committed to promoting access to the media for people and communities. We enable people to establish and develop community-based communications media for empowerment, cultural expression, information and entertainment. The CMA represents the community media sector to Government, industry and regulatory bodies.
Our objectives are to increase recognition of community radio and other forms of community media (including online radio and television, free to air community and local television and community film makers) as an important delivery agent for local aspirations on increasing volunteering, education and employment opportunities; improving local accountability and democracy; increasing community cohesion, safety and understanding and inter-cultural and inter-generational communication; reducing the digital divide through providing access to ICT and media production skills; and support and to increase the ability of the sector to respond to the needs of their communities.
Share this:
', '2018-12-07 19:30:45');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6974, 'About Browse RSS
About
Freesound Labs is a directory of projects, hacks, apps, research and other initiatives that use content from Freesound or use the Freesound API .  Our aim is to keep on updating this directory as soon as we come across more and more of Freesound powered projects. Nevertheless, we’ll probably miss a lot of interesting things so if you know of something that should be listed in our directory and it is not here, please feel free to tell us by sending us an email (freesound at freesound dot org).
Some of the projects listed in this site are, in fact, hosted in Freesound Labs. Do you have a cool web-based project using Freesound and want to host it here? Please, send us an email explaining the project and the hosting requirements and we’ll try to do our best to make it happen. The simpler the project, the more chances you have to get it hosted, e.g., static pages are simple to host ;)
Freesound Labs is run by the Freesound Team at the Music Technology Group of Universitat Pompeu Fabra . Feel free to contact us if you have any questions, comments or suggestions.
', '2018-12-07 19:30:57');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6975, 'Legal
About us
In the early years of the new millennium, there was a limited set of options for anyone who wanted to enjoy music online: downloading illegally from P2P file-sharing services, or spending money on digital downloads that you could only use on one specific device.
In the rise of more permissive models and movements such as Open Source and the FreeCulture Movement, new ideas on how to digitally share creative works came to life. Creative Commons brought an alternative to the automatic “all-rights reserved” copyright, eventually leading a small group of people in Luxembourg to found in 2004 the pioneering website Jamendo.com, the first platform to legally share music for free from any creator under Creative Commons licenses.
Jamendo is all about connecting musicians and music lovers from all over the world. Our goal is to bring together a worldwide community of independent music, creating experience and value around it.
On Jamendo Music, you can enjoy a wide catalog of more than 500,000 tracks shared by 40,000 artists from over 150 countries all over the world. You can stream all the music for free, download it and support the artist: become a music explorer and be a part of a great discovery experience!
Jamendo’s mission is to offer the perfect platform for all independent artists wishing to share their creations as easily as possible, reaching new audiences internationally. Our philosophy is that any artist in the world is entitled to share his music and should have a chance to be heard by a greater number of people around the world.
We also believe that any artist should benefit from sharing their music. That’s why we help artists make money by licensing their songs for commercial use. Thanks to Jamendo Licensing, we answer the needs of anyone searching for any style of music to be used immediately in a film, a TV program, a commercial, an online video or as background music for commercial space; all at a fair price. With such use of their songs, artists find new sources of revenue while getting wider exposure.
Jamendo is a whole world of music to discover. Come and enjoy the best of the independent music community, use it, or contribute to it!
Jobs
We are a team of 20+ people based in Luxembourg, Paris and Brussels. Come and join us!
', '2018-12-07 19:31:00');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6975, 'Contact us
Before contacting us, please visit our FAQs , you are likely to find an answer to your question!
Username
', '2018-12-07 19:31:00');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6983, 'Things you need to know about Santiago before PoplusCon
16 Apr 2014 by Catalina Margozzini
Here’s some important information about Santiago you should read before coming to PoplusCon!
Santiago: Getting around.
During the days of the conference we will probably be moving around in groups. Nevertheless, it’s useful to know how to get around Santiago.
Buses
In order to get around in buses - or micros - you will need a Tarjeta Bip (this card is the only accepted payment method and is the same for metro), which you can buy and charge at any Metro Station. In www.transantiago.cl you can find all buses routs in Santiago.
Metro (subway)
Santiago has a slick, clean and efficient metro system that is always being expanded so more citizens can reap the benefits. Many already do - nearly a million passengers travel daily. Santiago''s metro system has a few separate lines that interlink. For destinations along these lines, it''s far quicker to take the metro than a bus. The metro operates 6:30am to 10:30pm Monday to Saturday, and 8am to 10:30pm Sundays and holidays. The trains are clean and frequent, but can get busy in rush hour. If you''re in Santiago for more than a few days, get a Multivía card (Tarjeta Bip). It costs US$1.70, but you can charge it up as much or little as you want, and each journey costs slightly less than if you buy them individually. Fares vary depending on the time of day. The normal rate (US$0.55) is available 6:30am to 7:15am, 9am to 6pm and 7:30pm to 10:30pm; the peak-hour rate (US$0.70) applies 7:15am to 9am and 6pm to 7:30pm. Metro map here
Connecting Metro and bus rides
While travelling by bus or metro you are able to connect up to 3 times within a 2-hours period with no extra cost. For instance, you might need to take 1 metro ride and 2 bus rides in order to get some places, you''ll have to check your TarjetaBip 3 times but it will be charged just once (the first time).
Taxi
Santiago has abundant metered taxis, all black with yellow roofs. It costs about US$0.35 to start the meter, and about US$0.17 per 200m. Most Santiago taxi drivers are honest, courteous and helpful, but a few will take roundabout routes. A handful have ''funny'' meters. Always carry cash when travelling in taxi.
Bicycle
Rampaging micros mean that Santiago''s streets are not that safe for bikes, but recent cycle lanes have improved the situation. There are also a lot of public paths that can be used, as well as quieter roads, so don''t be put off. Santiago is certainly compact enough to get around by bike and the climate is ideal for it - even if the smog isn''t.
Health
Here’s a list of medical services: Clínica Santa María - This medical center is closest to the conference venue. and very close to the hotel.  (http://www.clinicasantamaria.cl/ Tel 56220190000, Av. Santa María 0500)
Clínica Alemana (562 22101111, www.alemana.cl; Vitacura 5961, Vitacura)
Clínica Universidad Católica (562 23696000; www.clinicauc.cl in Spanish; Lira 40, Centro; Universidad Católica)
Posta Central (562 26341650; Av Portugal 125, Centro; 24hr; Universidad Católica) Santiago''s main emergency room is here.
Money
You''re never far from an ATM in Santiago: in any bank or subway station. Exchange houses are clustered on Agustinas between Bandera and Ahumada. Cambios Afex (56226881143; Agustinas 1050, Centro; Universidad de Chile) is efficient. There is also a whole bunch of cambios (money exchangers) on Av Pedro de Valdivia in Providencia, although some will not change traveler''s checks apart from US dollars.
1USD = $550 chilean pesos aprox. £1 = $920 chilean pesos aprox.
Safety
Santiago is quite a safe city, but just like in any large city in the world, you should look after your belongings (i.e. don’t count your cash in public, don’t leave your cel phone or laptop unattended).
Contact & emergency Numbers
Here’s a few contact numbers from Ciudadano Inteligente staff in Chile:
Felipe Heusser +56995779645 María Luisa Sotomayor +56992379372
Ciudadano Inteligente office: +56224192770
The police are called “Carabineros”. They are dressed in green. Police Emergency number: 133
Important to know. What to do in case of an earthquake
Don’t panic. Chile is a seismic country, but there’s nothing to worry about. Earth movements are very common on a daily bases, but all buildings have to be constructed according to very strict international seismic regulations, so movements are barely felt by people. Nevertheless, if there’s any strong earth movement during your stay, you should: 1) Keep calm and stay where you are. 2) Move away from falling objects. 3) Move away from large glass windows. 4) Open the door if you’re inside a room. 5) Stay inside.
You should not: 1) Panic 2) Go down the stairs 3) Run 4) Go outside during the earth movement.
Weather & what to pack
Weather is quite nice this time of year in Chile. Mid autumn means having a high pick of 24 Celsius (75 Fahrenheit), though mornings can be a bit chilly (around 7 Celsius, or 44 Fahrenheit),as can nights, so you should pack warm clothes as well. There’s also a saying in Chile: “Abril, lluvias mil”, which means in April we can get very rainy days, so don’t forget your umbrella.
It’s important to note that we will be doing some walking around, so you should definitely pack comfortable shoes for the conference.
No special clothing will be necessary.
Food, special allergies
Dietary requirements will be considered in all meals during the conference. Nevertheless it’s important to note that not all restaurants in Santiago accommodate all dietary requirements, so if you go out, do ask in advance at the restaurant.
Customs
Chile is a very strict country regarding anything entering or leaving the country. At the airport, right next to customs you’ll find the country’s agricultural and livestock service, and it’s very important that you declare any goods with animal or vegetable origin. In this form you’ll find more information. If goods are found and have not been declared, fines can be very high.
', '2018-12-07 19:31:06');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6983, 'What is PopIt?
PopIt is a tool that just exists to do one task, well.
That task is the business of making and maintaining lists of politicians and other public figures. List like this:
Every politician in the South African Parliament
Every politician in the Finnish Parliament
Why do lists of politicians deserve a special tool?
Before it is possible to know what politicians are doing, and whether they are doing a good job, we must first know who they are. This data is often far harder to get, and far more inefficiently shared than you would ever imagine.
Bad data on who politicians are is especially problematic if you are trying to build websites or apps to help with the business of tracking them. And, as of today, bad data, or no data, is the global norm.
How does PopIt Help?
PopIt makes it easy to make lists of politicians, and keep them up to date. It provides an easy user interface that makes the task simple, an interface suitable for campaigners, journalists, or indeed government officials. We hope very much that governments and assemblies start to use PopIt to publish official lists of politicians.
Crucially, though, PopIt converts this data into a format that makes it easy for programmers and computers to use the data for other purposes. So if you’re a coder wanting to build a civic or democratic site, you want people in your country or city to use PopIt so that you don’t have to build and maintain your own constantly-changing databases. Just suck in the JSON every day and stop worrying.
PopIt is Open Source and Open Standards
We want every assembly in the world, national or local, to have good quality, up to date data about members. We think that it will be easiest to achieve this by making a tool that is free, and based on open standards. There’s no cost, and no lock in. PopIt also functions as both a hosted service and as a self-hosted tool, so there’s no questions about data security or making it compatible with your systems.
So, what’s the formal mission of PopIt?
“PopIt is being built to lower the costs and speed the delivery of democracy, accountability and civic websites and apps. It will do this by dramatically improving the process by which lists of people and institutions are created and maintained.”
And how do we think it will deliver on this mission?
By lowering the cost and increasing the speed of building and maintaining lists of people and institutions.
By filling the internet with highly re-usable lists of people and institutions on top of which people can build services both predictable and unpredictable.
By providing a tool which allows non-technical enthusiasts and activists to create real value for programmers, allowing them to work together more productively.
By powering some of mySociety’s own tools, both reducing our long run costs and acting as exemplars.
By offering instant, free, hosted lists for getting started quickly, as well as  a deployable, open source codebase for people who want complete control.
', '2018-12-07 19:31:06');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (6990, 'Interaction and communication in learning communities Open forum
The DEHub has been formed in 2009, with the purpose of establishing an Australian research institute for open and distance learning.
The Distance Education Community theme considers the need to provide a sense of inclusion and community for distance education students within the Australian tertiary education sector, and the need for distance education courses to engage with broader social and cultural community groups around the world. A central online agency would enable professionals to access, understand, and implement world’s best practises in developing new, and supporting existing, communities for distance education courses, units, and programs. The focus on this theme could seek to simplify the ICT learning and management requirements of both staff and students, whilst also improving and streamlining the delivery of distance education programs.
Of the three key themes explored by DEHub, Distance Education Community has the broadest scope, is relevant to the largest number of people associated with distance education, and is fast being recognised as of central importance for the continued success of distance education programs. Across the Australian tertiary education sector, distance education communities take on a wide range of official and unofficial shapes and forms: online discussion post forums, online chat forums, e-mail groups, Facebook groups, Second Life groups, MySpace groups, and many more. At other times distance education programs relate to a broader, less structured understanding of community. For example, institutional stakeholders may be monitoring the progress of a new and innovative program, or there may be local non-institutional assistance and involvement in the delivery and/or receipt of program material. There are also the more distant residential-community relationships with distance education providers. For example, the University of New England’s symbiotic relationship with the city of Armidale and the New England region, or Charles Sturt University’s research-based relationship with the viticulture and equine community of Australia.    ‘Community of learners’
With regards to staff-student communities centred on tertiary education programs, a central online agency could make available resources to assist in the design, construction, and management of online and interactive ‘classrooms’, ‘tutorials’, and discussion groups. This could facilitate the access, understanding, and implementation of best practices to enable patrons to create and support their own virtual communities within which staff and students to engage each other as a ‘community of learners’ (UNESCO 2000, Learning Development Institute 2001, 2005 [Moore, p. 640]).
As technologies evolve, so too do technological trends. As these trends change, so too must distance education systems change to facilitate the shifting needs, desires, and learning styles of students. Historically, the increased availability of the home PC enabled a greater number of electronically typed papers; the increased availability of the internet enabled increased access to online knowledge systems for use in these papers; and the increased availability of laptops made both electronically typed papers and the accessibility of online knowledge systems a ‘portable’ activity. Furthermore, as these technologies and trends change, so too do the cultures that support them. Wireless hot spots in café’s and parks allow students to engage in active learning in a range of physical environments, whilst the proliferation of software has supported an almost equally diverse range of ‘online’ or ‘virtual’ environments (such as Facebook, MySpace, and Second Life).
To remain connected to the ‘community of learners’ students often need to feel engaged and involved with others. Wenger (1998 [Moore p. 573]) argued that learning is a form of social participation; in a distance education environment educators must ensure that they provide a space where healthy levels of ‘social participation’ can be encouraged and maintained. Woudstra and Adria added that ‘individuals engage in and contribute to the practices of their communities’ [Moore p. 573]. In order to ‘engage in and contribute’, individuals must first feel they are a valued member of a valued community, and furthermore, that their contributions will be valued. Geographical distances should not be allowed to disrupt intellectual and social engagement. Thus, in the absence of the traditional ‘physical’ learning community environment (ie: the classroom), educators must be able to provide an appropriate ‘virtual’ learning community environment. Again, as technological trends change, these virtual environments must also change. To remain fixed on one idea for too long is to remain fixed on the past, and outside the cultural and intellectual interest of the increasingly technologically savvy student body of today.
Providing a ‘space’ for a community to develop is not enough in itself. A lecturer would not simply place a group of students in a classroom and expect them to begin to drill down to the core issues of the subject (they are more likely to discuss the weekends social outings). Ryan and Fitzgerald (Handbook of Research on Social Software and Developing Community, p. 171) noted that ‘we have not built into our programmes even the simplest of ways to engender a learning group ethos’. The open and visible nature of online learning communities makes the distraction of social discussions less likely, but some basic direction is still required. A million students on a million typewriters left in a room for an infinite time may produce the collected works of William Shakespeare, but it will not be done in a single day, or even a single teaching semester. To assist in guiding the process (be it producing the works of Shakespeare, or discussing the set weekly readings), educators need to engage with this community and provide direction.
Educators also need to personally connect with these learning communities to continue to make education accessible to the changing student body. Accessible not only in terms of being able to find and physically ‘access’ the material, but also, and importantly, of being able and willing to engage and intellectually ‘access’ the material and the modes of delivery (for ‘intellectual access’ see Doiron and Davies, 1998, pp. 14, 44). It is one thing to set up the facilities for an online learning community environment; it is another thing to ensure that students feel valued and are intellectually engaging within this community.
A central agency could also support these communities by facilitating the smooth transition to, or across, virtual educational environments. By focusing on the priorities of ‘ease-of-access’, ‘ease-of-understanding’, and ‘ease-of-implementation’, this agency could enable both staff and students to quickly engage and interact in these new environments with a minimum of fuss. Consideration should be given to the range of educational needs in the Australian tertiary sector, and provision should be made for a corresponding range of educational support systems and pedagogical approaches. Of primary importance is assisting tertiary education providers in the management of these communities.
Community Engagement
The Distance Education Community theme also considers broader engagement between tertiary education institutions and other non-university communities. The distance education experience can be enhanced, and the strengths of the host educational institution improved through innovative engagement (both direct and indirect) with broader community associations. The US-based Carnegie Foundation for the Advancement of Teaching classified US tertiary institutions based on their type and level of community engagement. Amy Driscoll noted in a paper on the resulting community engagement classifications that:
One of the major strengths of the institutions that were classified as engaged with their communities was a compelling alignment of mission, marketing, leadership, traditions, recognitions, budgetary support, infrastructure, faculty development, and strategic plans – the foundational indicators of community engagement (Driscoll, p.39).
With regards to academic programs, courses, and units, the benefits of community engagement to students and staff include the enhancement of learning outcomes ‘through curricula that are relevant to community issues and priorities’, the ‘development of increased opportunities for student experiential learning and internships’, and improved funding opportunities for scholarships, fellowships, and grants as research partnerships develop (AUCEA Position Paper, p. 3). Driscoll also argues that the exact nature of community engagement can take a wide range of forms: ‘cooperative education and extension coursework, learning centers, institutional resource-sharing (libraries, technology, and cultural offerings), student volunteerism, and professional-development centers’ (Driscoll, p. 41). On a similar note, Buys and Bursnell also listed some of the benefits of successful university-community partnerships, including new insights and learning, better informed community practice, career enhancement for individuals involved with the partnership, improvement in the quality of teaching and learning, increased opportunity for student employment, additional funding and access to information, more frequent and higher-quality publications, and more rapid speed of internationalisation (Buys and Bursnell, p. 74).
At the most basic teaching level, in the classroom, lecture theatre, or online discussion thread, the benefits of such engagement include the variation of the weekly teaching routine and interaction between students and members of the related community. Such variation and interaction introduces new teaching staff, new knowledge backgrounds, new perspectives, and new approaches to teaching (Buys and Bursnell, pp. 77-78). Community figures often present ‘real-world’ experiences, insight, and perhaps even a broader level of ‘expertise’ than can be offered by a single lecturer alone. Chickering argues that:
Pedagogical practices need to call for behaviors that are consistent with our desired outcomes and that generate learning that lasts: collaborative and problem-based learning, case studies, learning teams and research teams, socially responsible learning contracts, criterion-referenced evaluation. These pedagogical practices need to incorporate concrete experiences and reflection, applying and testing academic concepts, principles, and theories in real-life situations such as service learning (Chickering, p. 93).
A range of concrete experiences and reflection, application and testing of academic concepts, principles, and theories in real life situations can be conveyed to students through academic engagement with broader non-university communities. These partnerships enhance learning and teaching and present students with ‘cutting edge’ research by, as Buys and Bursnell argue, ‘embedding it in the real world’ (Buys and Bursnell, p. 80).
A central agency could also assist in fostering closer collaboration and engagement between tertiary institutions and external communities by providing a central resource repository on best practices in university engagement with communities. This could consider the different levels within institutions at which community engagement occurs, the different reasons why community engagement occurs, and the different types of collaboration and cooperation required. As with the other DEHub themes, the priorities of ‘ease-of-access’, ‘ease-of-understanding’, and ‘ease-of-implementation’ could also apply to the material available on community engagement to enable both tertiary institutions and community partners to recognise and access the full mutual benefits of closer cooperation.
DEHub Open Learning
DEHub’s ‘Distance Education Community’ theme could also explore Open Learning within Australia in an effort to review and improve teaching practices, technological innovations, and student approaches within Open Learning environments.
Open Learning styles demand the development of new skill sets amongst students. Traditional views of learning within a lecture, tutorial, or seminar environment have been rapidly transformed with the advent and widespread proliferation internet-hosted communities and multimedia tools. The potential now exists for a class of students to sit within the comfort of their respective lounge room, kitchen, bedroom, bathroom, back yard, local coffee house, or local beach to ‘conference meet’ with a tutor or lecturer and a dozen or so other students online. Students can easily complete an entire course/degree without having to ever see another human being.
From an educational standpoint, the advantages of Open Learning have long been expounded: a more diverse and larger student body, greater access to education for disadvantaged students, more flexible learning environments, and the increased ability to manage long term careers and full time employment with study. From an institutional standpoint, the advantages extend to increased flexibility in course delivery, greater diversity of course content, further opportunities to expand internationally, and greater opportunities for inter-university and institution-community financial, educational, research, and institutional cooperation and collaboration. DEHub is positioned to explore best practises in Open Learning for the Australian higher education sector.
', '2018-12-07 19:31:18');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7005, 'Contact
Contact
If you are interested in joining the ongoing discussion about FreedomBox with users and developers, sign up for our Mailing list . For technical support, please refer to the same mailing list. If you would like to contact the FreedomBox Foundation directly, our contact information is below.
Email
Press inquires press@freedomboxfoundation.org
If you are interested in supporting the FreedomBox project''s efforts, either as a developer or future user, please write to us and let us know. join@freedomboxfoundation.org
If you are interested in making a financial contribution to the Foundation, please write to donate@freedomboxfoundation.org
', '2018-12-07 19:31:36');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7007, 'SENSORICA is an open, decentralized, and self-organizing value network.
SENSORICA thrives on open innovation.
We design, produce, and distribute open hardware.
SENSORICA is open, why compete with us when you can join us?
SENSORICA is a commons-based peer production system.
Stop looking for a job and start working now! Join SENSORICA!
Your revenue is based on your contribution within SENSORICA.
SENSORICA is NOT a power-based structure, but a value-based structure.
We bet on speed rather than protection, we thrive on open innovation.
SENSORICA is a growing pool of know-how in sensing technology.
SENSORICA is an institution designed for the know-how economy.
Know-how and production capacity are the key to the new economy.
What you know and what you can do are more important that who you are.
Share and be social and you’ll be fine...
All revenue is redistributed among members based on individual contributions.
Be autonomous and show initiative! There is no boss within SENSOICA!
A new economical paradigm… 
', '2018-12-07 19:31:42');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7007, 'SENSORICA is an open, decentralized, and self-organizing value network.
SENSORICA thrives on open innovation.
We design, produce, and distribute open hardware.
SENSORICA is open, why compete with us when you can join us?
SENSORICA is a commons-based peer production system.
Stop looking for a job and start working now! Join SENSORICA!
Your revenue is based on your contribution within SENSORICA.
SENSORICA is NOT a power-based structure, but a value-based structure.
We bet on speed rather than protection, we thrive on open innovation.
SENSORICA is a growing pool of know-how in sensing technology.
SENSORICA is an institution designed for the know-how economy.
Know-how and production capacity are the key to the new economy.
What you know and what you can do are more important that who you are.
Share and be social and you’ll be fine...
All revenue is redistributed among members based on individual contributions.
Be autonomous and show initiative! There is no boss within SENSOICA!
A new economical paradigm… 
Home ‎ > ‎
What We Do
SENSORICA is committed to the design and deployment of intelligent sensors and sensemaking systems (IoT + AI + p2p infrastructures), which allow our communities to optimize interactions with our physical environment and realize our full human potential.
What we do can be classified in the following categories:
', '2018-12-07 19:31:42');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7007, 'What is SENSORICA and value networks
 Sensorica''s structure and economic model 
commons-based peer-production  network
SENSORICA is partially a market-based economy and partially a gift economy . For the first part, we find solutions to problems and exchange these solutions (products) on the market for revenue. For the second part, individuals and organizations are allowed to initiate projects that are mostly passion-driven, without expecting any extrinsic reward (financial compensation or other). SENSORICA offers the right balance between passion and duty, between freedom and constraint. SENSORICA is for benefit, NOT for profit (it is not a profit maximizing organization).
Two tracks of innovation in SENSORICA
Sensing technology : Integrating open source into ethical and open products. In other words, we design and manufacture open products (sensor devices and applications) to be exchanged on the market ( see products ).
The  open value network model : designing and implementing the  open value network (OVN)  model, a new organizational and business model. 
 As an organization, SENSORICA is
designed to facilitate large-scale co-creation and exchange of value
facilitates large-scale coordination and collaboration
secures transactions among affiliates and reduces transaction costs
facilitates resource management
allows multi-dimensional value accounting and fair redistribution of revenue
open: easy access to participation
horizontal: non-hierarchical governance, autonomy of affiliates, structure shaped by value-based relations rather than power-based relations
decentralized: bottom up allocation of resources, no central planning, no collective budget
emergent and adaptive structure, constantly changing with respect to internal and environmental conditions
 
an ethical and humane organization, by its nature
a locus of practical knowledge about  sensing
an organization tuned for the know how economy
an organization producing  Commons , we share our knowledge
an organization optimized for speed to market and flexibility (as opposed to protection)
an economic entity that thrives on open (source) innovation through a network-to-teams structure
an elastic organization that builds new capacity by network affiliation / association
we are only limited by our ability to coordinate our global network.
we use a contribution-based compensation model.
Our online community is growing. Join us in our mission!
Geographical distribution of our network of Sensorica team members
View  SENSORICA  on a larger map
More about us
SENSORICA is a new type of organization tuned for the p2p economy, for commons-based peer production. It is 
an expanding open enterprise, or an open value network : 
Open knowledge and standards
', '2018-12-07 19:31:42');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7024, '×
The road to a better world.
Zipcar is the world’s leading car-sharing network. We provide on-demand access to cars by the hour or the day in cities and campuses around the globe.
The Story of Zipcar
Our history
It’s been close to two decades since our founders, Robin Chase and Antje Danielson, sat in a café in Cambridge, Massachusetts and decided to bring the European car-sharing idea to North America. Once the wheels were in motion, it was only a matter of time before some major changes helped grow our little car-sharing company into the world’s leading car-sharing network. Today, thanks to a member-driven user experience, a focus on innovative technology, and an amazing team of hands-on car-sharing enthusiasts, we are redefining the way the world thinks about alternative transportation.
Car sharing seems like a simple enough idea, but there’s a reason Zipcar has become the leader of cars on demand. It’s not just about fewer cars and reducing congestion and pollution; it’s about finding sustainable solutions. This idea is bigger than all of us, and we’re committed to our mission: to enable simple and responsible urban living.
Our mission
We''re on a mission to enable simple and responsible urban living—a future filled with more car-sharing members than car owners in major cities across the globe.
We do this every day by:
  Delivering on-demand vehicles that support environmental sustainability.
  Helping members save time, hassle and money in their everyday transportation.
 
  Freeing up city space through strategies that consider campus, urban, residential, commercial and city planning needs.
  Becoming a premier employer for talented, passionate people who thrive on changing the world for good.
  Living our core values, as a company and as individual team members from the Big Apple to Big Ben
A message from our president
A message from our president
“Zipcar doesn’t just start with the member experience, we live it. We sit atop an impressive history of innovation, risk taking, ingenuity, and hard work. And we’re still driven (pun intended) by the same core mission: to enable simple and responsible urban living. And it all grows as we get closer and closer to our vision of a world where car sharing outnumbers car ownership.”
–Tracey Zhen, President
Causes we support
Environmental impact
Since 2000, our mission has been to create better neighborhoods. So we set out to cut carbon emissions, reduce traffic and promote multi-modal travel. Thanks to our over one million members, we proudly lower CO2 emissions by 1.6 billion lbs per year and take away the need for more than 415,000 personally owned cars.
Embracing equality
Equality shouldn’t be up for debate. That’s why Zipcar was an early supporter of LGBTQ organizations and equal rights. We continue to show our support at Pride events and in how we do business every day. We believe people can overcome anything on their own—but we’ll always be stronger together.
Zipcar grant program
Zipcar helps non-profits do more with simple access to on-demand transportation. Through our Students with Drive program with Ford, over $1 million has been donated to more than 400 student organizations at universities across the United States. And our grant program donates hundreds of thousands in driving credit to non-profit groups across North America, supporting a variety of causes. Need some help with your charity organization? Tell us how we can help and apply for a grant.
Other Products
', '2018-12-07 19:31:51');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7027, 'BLOG
Press and Media information
When you want to visit Auroville for filming, writing or photographic purposes, you will have to get in touch with Auroville’s OutreachMedia, the Auroville service in charge of facilitating visits of journalists and film/video makers. Our particular aim is to ensure that all journalists and filmmakers get correct, up-to-date information or relevant/representative footage from the best sources so that generally their visit is a fruitful and constructive one.
This includes intermediating with authorities in order to obtain permission to film (specially relevant to foreign film crews); collaborating on tentative scenarios and film schedules beforehand so that when a film crew or reporter arrives no loss of time occurs; setting up interviews with appropriate people; escorting crews around the township; and helping book accommodation and arrange transport, as well as possibly helping with the final product where ‘finishing touches’ may be required. A financial contribution may be asked for the above services including administrative charges/fees.
Objective and non-proselytising
“Auroville belongs to humanity as a whole,” says its Charter, and we are aware that the very existence of Auroville gives hope and encouragement to many people. Consequently, when it comes to making the existence, aims and ideals of Auroville known to a wider audience, the OutreachMedia team will give every assistance to ensure that the information is imparted in an objective, non-proselytising manner.
Thank you for approaching OutreachMedia
', '2018-12-07 19:31:53');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7027, '1st Floor, TownHall Building, Auroville
0413-2622098
', '2018-12-07 19:31:53');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7031, 'KvK: 55901964
VAT: 851904270B01
Please note: our repair centre is located elsewhere, if you need information on this subject or would like to submit a return or repair request, get in touch !
Together we can change the way products are made
Want to get updates on how we’re changing the electronics industry?*
This iframe contains the logic required to handle Ajax powered Gravity Forms.
We ask for your name and e-mail so that you can receive our newsletter for awesome project updates. You can withdraw permission at any time.
We use MailChimp as our email platform. By clicking subscribe, you acknowledge that the information you provide will be transferred to MailChimp for processing in accordance with their Privacy Policy and Terms .
', '2018-12-07 19:31:55');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7031, '250,000 community members on Facebook, Twitter and the Fairphone forum combined
Business as a means for social change
We believe business and social impact should go hand-in-hand, and we want to show that commitment by becoming a B Corp-certified social enterprise. Read the B Corp report card on our social and environmental performance.
Learn more
Awards
We’re not doing it for the prizes, but we don’t mind a bit of praise. Here are a few of our favorite achievements from the past few years.
Won
German Environment Foundation German Environmental Award 2016 for our founder Bas van Abel
The Lovie Awards 2016 Lovie Emerging Entrepreneurs
European Business Awards for the Environment (EBAE) 2016 The International Business Cooperation Award
Sustainia 2014 Community Award
UN Momentum for Change Award 2015 ICT Solutions
d&ad 2016 White Pencil in Service Innovation and Graphite Pencil in Product Design
The Next Web 2015 Tech5 - Fastest Growing Tech Startup in Europe
GreenTec Awards 2015 ProSiebenSat.1 Accelerator Start Up Prize
Finalist
SXSW Interactive Innovation Awards 2016 New Economy
Koning Willem I Plaquette Prijs 2016 KW I Plaquette voor Duurzaam Ondernemerschap
London Design Museum Designs of the Year 2014
Together we can change the way products are made
Want to get updates on how we’re changing the electronics industry?*
This iframe contains the logic required to handle Ajax powered Gravity Forms.
We ask for your name and e-mail so that you can receive our newsletter for awesome project updates. You can withdraw permission at any time.
We use MailChimp as our email platform. By clicking subscribe, you acknowledge that the information you provide will be transferred to MailChimp for processing in accordance with their Privacy Policy and Terms .
', '2018-12-07 19:31:55');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7039, 'Regional Support
Contact us
BMC understands how important it is to be available to customers and provide the right support as quickly as possible. Should you have any queries, the Open Research Support Team is here to help 7 days a week so please feel free to contact us and we will get back to you very soon.
If you would like to check whether an answer is already available before getting in touch, we invite you to check our Support FAQs .
Areas covered include general questions about open access, login, how to check the status of a manuscript and paying an article-processing charge. 
If you can’t find the answer to your question, please contact us using the options available below.
Online Support:
', '2018-12-07 19:31:58');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7039, 'BMC management team
Rachel Burley - Publishing Director
Rachel has responsibility for all of BioMed Central and SpringerOpen’s editorial and commercial activity. She joined Springer Nature in 2016.
Rachel was previously Vice President and Director of Open Access at Wiley, where she led the strategic planning and development of Wiley’s open access initiatives. Prior to that, Rachel spent seven years at Nature Publishing Group where she developed several new journals including Nature Methods.
Dr Louisa Flintoft – Executive Editor, BMC in-house journals
Louisa has editorial responsibility for the in-house editorial teams at BMC, including the BMC series and the flagship journals (which include Genome Biology and Genome Medicine, as well as BMC Biology and BMC Medicine). She joined BMC in 2014.
Previously, Louisa was Chief Editor of Genome Biology for three years and Chief Editor of Nature Reviews Genetics for five years. She has a PhD in Genetics from Imperial College, London, and carried out postdoctoral research at King’s College, London.
Toby Charkin - Global Head of Physical Sciences
Toby is Global Head of Physical Sciences for academic journals within the Open Research Group at Springer Nature. 
Prior to joining Springer Nature in October 2016, he was Executive Publisher for Behavioural and Cognitive Neuroscience journals at Elsevier, where he spearheaded several group-wide initiatives on topics such as pre-registration, open data, and reproducibility. He previously also held journals publishing positions at Informa and Current Science. As well as a background in journals, he has also worked as a Web Editor and Group Digital Media Manager for Incisive Media and Informa, and helped to launch a start-up in online reference publishing.
Amye Kenall - Global Head, Life Sciences
Amye is Global Head of Life Sciences for academic journals within the Open Research Group at Springer Nature. She joined BioMed Central (part of the Springer Nature group) in 2013, heading up data publishing and initiatives across BioMed Central. 
Previously she was at Taylor & Francis working on management and acquisitions for the Behavioral Science and Neuroscience titles. In the past she has held positions across production and editorial for both books and journals across the sciences and social sciences. Amye has worked on various projects, working groups and publications related to data sharing, OA, open data and the role of publishers in reproducible research.
Advertisement
', '2018-12-07 19:31:58');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7042, 'About
Who we are
The Internet Engineering Task Force (IETF) is a large open international community of network designers, operators, vendors, and researchers concerned with the evolution of the Internet architecture and the smooth operation of the Internet.
Support the IETF
Key Info
The Internet Engineering Task Force  (IETF)  is a large open international community of network designers, operators, vendors, and researchers concerned with the evolution of the Internet architecture and the smooth operation of the Internet. It is open to any interested individual. The IETF Mission Statement is documented in  RFC 3935 . 
The technical work of the IETF is done in its working groups , which are organized by topic into several areas (e.g., routing, transport, security, etc.). Much of the work is handled via  mailing lists . The IETF holds meetings three times per year. 
The IETF working groups are grouped into areas, and managed by Area Directors, or ADs. The ADs are members of the Internet Engineering Steering Group ( IESG ). Providing architectural oversight is the Internet Architecture Board, ( IAB ). The IAB also adjudicates appeals when someone complains that the IESG has failed. The IAB and IESG are chartered by the Internet Society  (ISOC)  for these purposes. The General Area Director also serves as the chair of the IESG and of the IETF, and is an ex-officio member of the IAB.
Bibliography
[1] RFC 3935
A Mission Statement for the IETF
This memo gives a mission statement for the IETF, tries to define the terms used in the statement sufficiently to make the mission statement understandable and useful, argues why the IETF needs a mission statement, and tries to capture some of the debate that led to this point.  This document spe...
Harald T. Alvestrand
', '2018-12-07 19:32:01');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7043, 'more
NEC is very pleased to be part of the CTI Technical Committee and continues to drive CTI adoption with industry partnerships to benefit customers. NEC believes that threat intelligence standards are crucial for proactively countering the cyber threat. We are excited about the formation of CTI TC and support its efforts through its contributing to and promotion of this global standard.
Kozo Matsuo
General Manager, Cyber Security Strategy Division
NEC Corporation
We have been advocates of STIX, TAXII and CybOx for some time. OASIS as an international standards checkpoint will undoubtedly improve threat intelligence sharing amongst partners by facilitating the exchange of computer-readable threat information.
David Maxwell
', '2018-12-07 19:32:03');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7043, 'I want to:
About Us
OASIS is a nonprofit consortium that drives the development, convergence and adoption of open standards for the global information society.
OASIS promotes industry consensus and produces worldwide standards for security, Internet of Things, cloud computing,  energy, content technologies, emergency management, and other areas. OASIS open standards offer the potential to lower cost, stimulate innovation, grow global markets, and protect the right of free choice of technology.
OASIS members broadly represent the marketplace of public and private sector technology leaders, users and influencers. The consortium has more than 5,000 participants representing over 600 organizations and individual members in more than 65 countries .
OASIS is distinguished by its transparent governance and operating procedures. Members themselves set the OASIS technical agenda, using a lightweight process expressly designed to promote industry consensus and unite disparate efforts. Completed work is ratified by open ballot. Governance is accountable and unrestricted. Officers of both the OASIS Board of Directors and Technical Advisory Board are chosen by democratic election to serve two-year terms. Consortium leadership is based on individual merit and is not tied to financial contribution, corporate standing, or special appointment.
SGML Open
OASIS was founded under the name "SGML Open" in 1993. It began as a consortium of vendors and users devoted to developing guidelines for interoperability among products that support the Standard Generalized Markup Language (SGML). The consortium changed its name to "OASIS" (Organization for the Advancement of Structured Information Standards) in 1998 to reflect an expanded scope of technical work.
Nonprofit Status
Documents related to OASIS'' nonprofit status are available in all suitable public resources and by written request to the address shown at https://www.oasis-open.org/org/offices
', '2018-12-07 19:32:03');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7043, 'OASIS representatives [3] are located in Beijing [4], Amsterdam, Geneva, Dallas, Los Angeles, New York, San Francisco, Sarasota, and Washington, D.C.
Related links: 
', '2018-12-07 19:32:03');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7045, 'Tweet @GuardianProject
Contact Us
If you’d like to learn more about Guardian from the team directly or have a proposal for us, please let us know using of the methods below:
Guardian-Dev Discussion List : If you are a developer, designer, power user, hacker, MOD’r, cryptophreak or just anyone interested in following the development side, this is for you!
Follow us us on Twitter: @guardianproject
Email us
Apps and Software Support:  support@guardianproject.info
Jobs and Internship Interest:  jobs@guardianproject.info
PGP-Encrypted mail to Guardian Project Director
', '2018-12-07 19:32:07');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7053, 'We''ll get back to you as soon as we can via e-mail.
Need help with your game?
Many form submissions we receive ask specific questions about a game   or ask questions that have already been answered on our forums or in Stencylpedia .
Customers: Please seek technical support through our customer-only forums .
Contact Form
', '2018-12-07 19:32:18');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7076, 'Text
CAPTCHA
This question is for testing whether or not you are a human visitor and to prevent automated spam submissions.
', '2018-12-07 19:32:31');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7077, '….
Childcare at Common House
We have a Kids Cupboard in the main space that is a home for toys, games, arts and craft materials, a changing mat, rugs for the floor. We are also 5mins walk from the Childhood Museum and there are plenty of parks near by. The Common House is a child friendly space and we encourage all groups to organise childcare where necessary for their events and meetings.
…..
Disability Access
The Common House is located on the ground floor of an old textiles factory – there are no steps inside the Common House venue and the building is wheelchair accessible via the side door. Currently the toilet is not wheelchair accessible. For more detailed accessibility information see here: https://www.commonhouse.org.uk/book-the-common-house/accessibility-information/
The Common House is a collectively managed space for radical groups, projects and community events
Location
Bethnal Green, London E2 9QG
Subscribe to our mailing list
Login for Regular Useres
', '2018-12-07 19:32:31');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7077, 'The Common House is a collectively managed space for radical groups, projects and community events
Location
Bethnal Green, London E2 9QG
Subscribe to our mailing list
Login for Regular Useres
', '2018-12-07 19:32:31');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7180, 'Nous rejoindre
Who are we ?
We are a network of social digital factories in France and abroad. We have trained more than 2000 trainees in the digital sector since 2013.
We are a social business. We want to make the digital sector a place of inclusion and to reveal talents among groups underrepresented in this field.
We also advise companies in their digital transformation and  create their websites and applications.
Our convictions
The digital sector is everywhere, but its language is not always natural or innate. The fracture between those who speak and manipulate it and those who don’t is striking. That’s why we want to make coding accessible to everybody.
 
Today our trainees live in the Meuse, Boulogne-sur-Mer, Aulnay, or even Romania. Our trainees are refugees, adolescents marginalized by the school system, children, long term job-seekers, or employees in reconversion.
 
And tomorrow they will be web developers, trainers, digital project managers, entrepreneurs, etc.
A brief story
Introduction
The great Simplon adventure began in 2013 with Frédéric Bardeau, Andrei Vladescu-Olt, and Erwan Kezzar. Inspired by the first Bootcamps blooming all over the Silicon Valley, they dreamed of a more inclusive and French version with more and more recruits from different backgrounds and territories. The local Montreuil factory was quickly requisitioned and refurbished and the first cohort, reunited 30 people from 17 nationalities, began in October.
2014-2015
Simplon began developing outside of Montreuil and factories started to grow in priority in rural areas of France thanks to a successful first round of financing and investing. We were then authorized to issue the Professional Qualification Certificate “New Technology Developer”.
At the same time, Simplon expanded its activities with the creation of Simplon Prod, the digital manufacturing solidarity workshop for ESS shareholders and the Simplon foundation, its philanthropic branch accommodated by Face .
2016-2017
This is the beginning of our international development. After the opening of the Romania factory in 2014, we then expanded to Brussels, Lebanon, Ivory Coast, and South Africa. Shortly after, France Active (via SIFA), la Caisse des Dépôts et Consignations (CDC), Phitrust , INCO , Aviva , Crédit Coopératif (via ESFIN), Amundi , and Mirova became financial partners of Simplon. 
With our new partners, we expanded our activities and began doubling our workforce.
Our engagements
Social and Solidarity
We are a social business with strong commitments in terms of limited profitability, participating governance, and wage differentials. Here, the employees at the top cannot earn more than 10 times the SMIC, and 7 times the SMIC of the 5 highest salaries. Simplon.co supports associations, solidarity project promoters, or NGOs to develop and digitalize. We are also members of the group Sociétés Coopératives d’Intérêt Collectif (SCIC) carrying out solidarity projects such as le label Emmaüs , the online shop of the Emmaus network, or MedNum , the cooperative actors of digital mediation who work daily for an evermore inclusive digital society. Our ESS ecosystem also includes:
Ashoka : member of the network since 2015, we work to enrich cooperation between network members, to promote the use of digital services in the network, and to build together a “French Tech of social innovation”.
La France S’Engage : winners of the first promotion in 2014, we work to make sure that the foundation is recognized as being a public utility and we are now on the project selection committee.
Mouves : in addition to being a member since its creation, Frédéric Bardeau, the president and co-founder of Simplon, is a member of this bureau.
A participatory governance
a Stakeholders Council bringing together Simplon.co’s key partners from local communities, businesses, state services, professional branches and their OPCAs, Simplon.co factory project leaders, employee representatives, and alumnis;
an Enlarged Executive Community (COMEX) which brings together all employees of Simplon.co (Impact Units, centers of expertise, and the whole team) with the executive co-founders and general managers.
A Strategic Committee composed of Simplon.co shareholders; external actors not involved in operational or shareholder management, such as the Accompanying Committee (CODAX) and the Enlarged Executive Community (COMEX) of the Simplon Foundation.
To stay up-to-date with our news, subscribe to the newsletter
* indicates required
First Name
Last Name
The information collected on this form is stored in a computer file by Simplon.co for census information requests. They are kept for 10 years and are intended for Simplon.co’s territorial divisions in France and abroad, as well as the Training, Engineering and Partnerships department. In accordance with French digital law, you can exercise your right to access your data and have it rectified by contacting: Simplon.co – 55 Rue de Vincennes – 93 100 Montreuil
SIMPLON.CO
', '2018-12-07 19:32:38');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7181, '312 South Beverly Drive, #3887
Beverly Hills, CA, 90212, USA.
 
#6, Knowledge House, Kasthuri Ranga Road, 
Alwarpet, Chennai 600018, Tamil Nadu,
India.
', '2018-12-07 19:32:38');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7227, 'About
We are an award-winning education technology movement which prepares young people for the real world.
Working alongside educators Apps for Good offers a free, flexible course framework that infuses digital learning with teamwork, creativity and entrepreneurship.
Students find a problem they want to solve and apply new skills to making a real life technology product. They explore the full product development cycle from concept, to coding, to launch in a way that brings the classroom to life.
', '2018-12-07 19:32:38');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7227, 'We are an award-winning education technology movement which prepares young people for the real world.
We are an award-winning education technology movement which prepares young people for the real world.
Follow us on Twitter
', '2018-12-07 19:32:38');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7285, 'Participating Services
Contact
Contact us for questions about how to get started, to be connected with partners, or information about joining or supporting the Open Badges movement.
Send us an email!
', '2018-12-07 19:32:41');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7331, 'You are here: Home > Web Wheel > Contact Us
Contact Us
Senior Psychologist, National Educational Psychological Service (NEPS)
National Coordinator for Guidance, Counselling and Psychological Services in Youthreach and Community Training Centres
Address:
', '2018-12-07 19:32:46');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12680, 'WHAT WE DO
Real Junk Food Manchester is a not-for-profit community interest company.
We tackle environmental and social problems through a mix of practical action and campaigning. We intercept food that would otherwise go to waste, and transform it into amazing meals for everyone and anyone, on a pay-as-you-feel basis.
In the short term we aim to reduce as much food waste as possible by feeding bellies not bins. In the long term we aim to campaign and raise awareness to stop food waste from happening, and make our food system more sustainable and fairer for all.
Find out more about the restaurant
We source perfectly edible food that would otherwise go to waste from supermarkets, wholesalers, and other food businesses, transform it into lovely meals, with a focus on health and nutrition, and serve them to anyone and everyone on a pay-as-you-feel basis.
', '2018-12-07 19:32:46');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12680, 'WHY WE DO IT
To stamp out food waste
Around 15 million tonnes of food is wasted every year in the UK alone, and the vast majority of that food is perfectly edible. The lowest carbon, most sustainable thing we can do with food that is going to go to waste is put it in a belly, any belly, rather than a bin, any bin (landfill, anaerobic digestion, animal feed, or compost). For every tonne of food that we save from going to waste, we save about 4.2 tonnes of CO2e from being emitted, and around 10% of the UK’s carbon footprint come from food that is never eaten.
To provide access to good food for all
At Real Junk Food Manchester (RJFM), absolutely all of our meals are served on a pay-as-you-feel basis. Pay-as-you-feel offers an innovative, inclusive model that deliberately bucks the recent trend towards food banks. We aim to offer an inclusive space where people from all backgrounds and of any social / economic status are welcome. Customers are invited to decide what they consider to be the value of the meal they enjoy, and to support the project with their time, energy, skills, or a financial donation if they would prefer, or are able. This is a conscious choice to work against the unfortunate consequence of a food bank model, where the poorest and most vulnerable in our society are segregated, often being offered little dignity or choice.
Using wasted food ingredients, focusing a model on cooking and serving great meals, and offering meals on a pay-as-you-feel basis means that RJFM also reverses the trend of the poorest in our society having the worst diets, and having severely limited access to healthy food. One of the highest areas of food waste, at farm, wholesale, and supermarket level, is fresh vegetables and fruits. By sourcing food that would go to waste, the project intercepts a huge quantity of basic, healthy ingredients, offering access to good food, and further bucking the trend of the most vulnerable within our society being offered heavily processed, tinned or dried meals.
To raise awareness about the huge amount of food that goes to waste, why it is a problem, and how it can be stopped
The long-term aim of the project is very simple; to put ourselves “out of business” by reducing avoidable food waste to the point where we can no longer sustain our project, and to influence the development of a food system that is environmentally sustainable, socially conscious and accessible.
We do this by using our work to raise awareness among consumers, food businesses and local and regional government about the scale and senselessness of food waste, it’s huge environmental and social impacts, and how everyone can help to make our food system better.
In the long run we would like to see a system where the high proportion of avoidable food waste is stopped before it happens, and the remaining, much smaller amount, of unavoidable waste food is diverted to those in need, with a focus on healthy food and cooked meals.
We recognise that this is a mammoth task with a number of different threads including (but not limited to); reducing farm level waste and the refusal of supermarkets to accept cosmetically imperfect produce, removal of best before dates from food items, pushing larger food businesses to effectively record and report their waste, regulatory and legislative changes, consumer education about more sustainable food choices, reduction in food packaging and moves to more biodegradable packaging… and many more.
To upskill, educate and support people to further level social inequalities
Real Junk Food Manchester is about valuing people and planet before profits, and this thread feeds through everything we do. Our project aims to support people to reduce food waste and have better nutrition in their homes, by offering recipes and information, and we hope ion the future, basic cookery courses. We also aim to support people in getting back into work by offering placements and training to people who might otherwise struggle to find work.
Operate a financially sustainable business model
In recent years the community and charity sector has seen huge reductions in the amount of funding available. This has led to reductions in support for our most vulnerable citizens, and in some cases a competitive rather than collaborative attitude between funded groups doing similar work. Real Junk Food Manchester aims to develop a financially sustainable business model based on the pay-as-you-feel concept that allows customers to support our activities in a way that suits them, and supports us in doing some good in the world!
Every tonne of food that we rescue from going to waste can…
Save 4.2 tonnes of CO2
FROM BEING OMMITTED INTO THE ATMOSPHERE
Feed over 1000 people
WITH A HOT, NUTRITIOUS MEAL
Find out more about Food Waste …
and its impact on our environment
', '2018-12-07 19:32:46');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12847, 'About Us
Who are the participants?
53 teams consisting of representatives from a municipality and a local school from all over Latvia took part in the SFL initiative “Change opportunities for schools”. They all together looked for new, creative and effective ways of taking advantages of the existing resources to change and enliven schools so that they can become a meeting and activity place for all local community members.
More information on individual projects you can find in the Project part on this website.
The team of the initiative “Change opportunities for schools”:
Aija Tūna, coordinator of the SFL initiative „Change opportunities for schools”, who is always happy to hear and tell others the stories of good experience and helps to convert challenges into new possibilities; she is also a consultant and a lecturer whose specialty is support to families, children and teachers and civic involvement as a key to better life.
For further communication aija.tuna@sfl.lv
Liesma Ose, former director of SFL Civic Rights and Ethnic Conformity program, co-author and the first coordinator of the initiative “Change opportunities for schools”.
Sanita Upīte, SFL administrative assistant was instrumental in building effective and pleasant link between people implementing the project, and the rest of the team, provided support in administrative and all the other matters.
Consultants – experts:
Ausma Pastore is excellent at involving people in thinking and decision making processes, in finding out people’s needs and their talents; she knows how to generate ideas to implement the needs and can motivate everybody to be ready to face up serious decisions.
Jānis Baltačs, having always been active in social and local development projects now shares his experience and encourages the people implementing projects into action; he is excellent at bringing round people to use modern technologies and talking them into starting entrepreneurship by taking stock of their own ideas and skills first, not by doing a course in accountancy.
Valdis Kudiņš, considers that the quality of each place depends on strong community and the community improvement is an integral part of the country development; he has great experience which allows him to address and support others.
What is happening?
As a reaction to the consequences of global financial crisis in Eastern Europe and Middle Asia, George Soros granted extraordinary 100 million dollars within a period of two and a half years to countries which had been affected by the crisis most badly. The decisions on programs to be maintained and the amount of finances to be granted are made by the Advisory Board of the Open Society Foundations.
The Soros Foundation – Latvia has successfully submitted and gained acceptance for several initiatives, including the initiative “Change opportunities for schools” supporting educational and social needs of small schools and local community members. 1.3 million EUR have been granted for implementing this initiative which allows create patterns for successful maintenance of small schools or re-structuring them by widening the spectrum of their activities, more actively serving the needs of local communities and putting the principles of lifelong learning into practice.
The aim of the initiative “Change opportunities for schools” is to promote restructuring of small schools into multi-functional education, culture and social centers developing partnerships between administrative, educational, entrepreneur and non-governmental sector organizations.
A call for projects was announced, and 323 projects were submitted. 151 projects were proposed for the second stage, and 53 of these were confirmed for implementation. An essential criterion in evaluating the projects was analysis performed by local governments and their understanding of social and educational needs of local people, ability to propose effective and sustainable solutions, as well as promoting involvement of various local community groups.
Both, local and foreign experts have declared that small schools in Latvia have high potential of being competitive and providing high quality education if they react to local community needs and change alongside with time. Taking into consideration performing different functions local community needs, small schools have a potential for development as they are closer to community people, thus are able to respond to their demands and changes in conditions. Small number of students gives a possibility to work more personally with children and young people at school, which together with assistance to teachers and possibilities offered by new technologies adds value to schools like this.
The initiative “Change opportunities for schools” is directed to taking advantage of the potential present at small schools and communities by helping and encouraging them to be transferred into community centers which react to the needs of every single community member.
53 projects were started in the end of 2009 and will be implemented until the end of 2010. During this year one of the tasks and aims of the projects is to find the best forms of activities, cooperation partners and to create general preconditions for successful sustainability of the projects in the future.
', '2018-12-07 19:32:55');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12878, 'What we do
 
At Fryshuset we value and recognize young people’s voices, ideas and actions. We are convinced that it contributes to a creative and solution-oriented social climate in which it’s allowed to think differently and find innovative solutions.
 
Youth-driven activities are the key component in realizing Fryshusets vision of enabling youth to change the world through their passions. The activities we offer young people are based on the motivation that comes from within, driven simply out of young peoples desires and pleasures.
By involving youth in different processes and provide them with tools and knowledge we promote young peoples empowerment in helping them shape their own future. In this way, our work stays always relevant and attractive. We strive to achieve this in the following areas:
We are bridge builders for mutual understanding and respect between young people, authorities, decision makers and other individuals. We work to strengthen young people’s knowledge about society and different cultures. We promote and support democratic values, knowledge and skills in order to increase young people’s active participation in democratic processes.
All of Fryshuset operations are based on the same values, mission and methods, but are locally adapted to the conditions, needs of young people and their driving force.
Would you like to visit and meet us? That’s possible.
Come and experience our dynamic operations firsthand! Click on image for more info>>>
Get in touch
Whether you’re looking for more information on our projects and activites, would like to collaborate with us, or just want to visit us, you’re more than welcome to contact us right here. We’ll respond as soon as possible!
Opening Hours:
', '2018-12-07 19:32:57');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12878, 'Who we are
 
Fryshuset is a dynamic and multifaceted movement for young people’s development. We enable young people to change the world through their passions.
 
We are a non profit organization independent of any political ideology or religion. We promote empowerment and social inclusion of youth with a special focus on those who are at risk or already face exclusion.
We provide opportunities for young people to develop their innate abilities and discover their passion in order to help them realise their full potential and find their way into society.
We believe that everybody can succeed and are worth to be given a chance to make a new beginning.
We strive to achieve this by jointly with young people create environments for holistic learning and personal development within the following fields of work; youth culture, social projects, schools, labour market and entrepreneurship.
Through these areas of work we give young people opportunities to practice their passionate interests, create and implement youth-driven activities, join projects and attend our schools. Furthermore, we create and provide various educational, leadership, labour market and entrepreneurship programs.
All components are equally important for all young people.
Would you like to visit and meet us? That’s possible.
Come and experience our dynamic operations firsthand! Click on image for more info>>>
Get in touch
Whether you’re looking for more information on our projects and activites, would like to collaborate with us, or just want to visit us, you’re more than welcome to contact us right here. We’ll respond as soon as possible!
Opening Hours:
', '2018-12-07 19:32:57');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12955, 'NK Zagreb 041
Aim and short description of activities (info about the project):
During the projectQuality integration solutions for refugees plans are made to empower and train the socially endangered group – refugees in Croatia, so they can independently start with the process of employment and/or development of the idea of social entrepreneurship with the goal of achieving better integration into society.
Aims of this project are:
empowering the skills for employability of refugees through training and supporting innovative initiatives of social entrepreneurship
advocacy of social inclusion and politics of refugee employment
networking of civil initiatives in providing social services and inclusion of refugees into society
The project consists of:
activities of informing and empowering persons that received refugee status (asylum or subsidiary protection) or that are living as foreigners in Croatia
media campaign and documentary film about refugee integration in Croatia
cooperation with organisations of civil society in providing support to refugee integration     
The total value of the project and EU share in financing the project
The total value of the projec tQuality integration solutions for refugees is 118.581,35 EUR. The total share of EU financing in this project is 99.999,65 EUR (84,33 %). 
Croatia’s government Office for Cooperation with NGOs financed this project with 13.007,19 EUR (10,96%).
Project implementation period: 01.03.2014 - 28.02.2015.
Tastes
', '2018-12-07 19:33:01');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12956, 'This foodbank is run in partnership with local churches, facilitated by Praise Gate Temple. Registered charity number 1077422 | Registered in England and Wales
By continuing to use the site, you agree to the use of cookies. More information
Accept
The cookie settings on this website are set to "allow cookies" to give you the best browsing experience possible. If you continue to use this website without changing your cookie settings or you click "Accept" below then you are consenting to this.
', '2018-12-07 19:33:01');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12965, 'Triff andere Alumni beim Jahrestreffen und online in unserem Netzwerk
Sie sind hier
Startseite » Über uns › About us
About us
The German non-profit organisation ArbeiterKind.de encourages and helps pupils from families without higher education experience to become First Generation university students. ArbeiterKind.de and its more then 6,000 volunteer mentors continue to support these students throughout their studies right up until successful graduation.
ArbeiterKind.de gives university students from so called non-academic families an identity and a voice. Through ArbeiterKind.de, the students meet peers who are facing similar challenges. As a group, these students are encouraged and gain in confidence. ArbeiterKind.de also strives to attract national attention and to raise public awareness of an often unacknowledged, yet systematic, discrimination.
ArbeiterKind.de helps pupils and university students gain access to and understand information about higher education. The website www.arbeiterkind.de provides all the information needed for a successful start at university with content tailored to the individual student, hands-on advice about financial support options, career guidance, and exam preparation. The website has created great interest among both current and potential university students. More than 30.000 people have been logging on every month.
Since 2008, ArbeiterKind.de has supported young people who have entered higher education as the first of their family. Over the steadily grown network of now more than 6,000 volunteers, ArbeiterKind.de informed and encouraged pupils, students and parents seeking advice from study entry until the successful completion of studies. The volunteers are organized nationwide in more than 75 local groups and efficiently addressed. The volunteers of ArbeiterKind.de are available during consultation hours and open meetings. They also hold information events at schools and a presentation of funding opportunities, scholarships, study selection and organization of studies. Additionally, the ArbeiterKind.de Info phone hotlline offers free initial information on four days a week.
ArbeiterKind.de is a nonprofit organization that relies on donations for the stabilization and the further expansion of support for first generation students. 
For further information contact www.arbeiterkind.de/spenden .
"Über uns"
', '2018-12-07 19:33:02');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12968, 'Mia
12 years old
It is great that everything is explained so thoroughly in bettermarks! In addition, it is really cool to be able to see what you have already succeeded at through your collection of coins and stars.
Studies
Learning outcome proven
Effectiveness and acceptance
1,000 students and 52 teachers from 34 schools took part in the study conducted by the Transfer Center for Neuroscience and Learning (ZNL) at the University of Ulm. The five-week test investigated the effectiveness and acceptance of bettermarks by teachers and students. The result: even in this short time, a significant positive effect was seen across all types of school. The classes that worked with the online learning system showed a more rapid increase in learning and had better test results.
Top in international comparisons
New online learning platforms for mathematics are being developed worldwide. However, bettermarks is clearly the best according to the 2012 international comparison study Eva-CBTM (Evaluation of Computer Based Training Programs for Mathematics). Among the German and English language products tested, bettermarks was the clear winner in all of the evaluation criteria. The most convincing aspect of the product is the evaluation and assistance system – the heart of any adaptive learning system. This is what enables students to be taught on an individual level.
Improve marks with bettermarks
120 Students of the European Richard von Weizsäcker high school in Thale (Saxony-Anhalt, Germany) tested software including bettermarks for the Intel® Education Alliance program from January to June 2012. The result: students who studied with bettermarks performed better by an entire class level on average than students who did not take part in the program. The students learned more quickly and the software reduced the amount of work for the teachers.
', '2018-12-07 19:33:03');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12968, '„… the most innovative e-Learning company in Europe.“
Digita 2011 & 2012
„…for the outstanding quality of the appealing and sensitively designed platform.“
Comenius
„… for the high educational quality and use of media.“
GiGA MAUS
„Diverse, full of variety, educationally successful.“
Classroom insights
Students helping students
Allowing students to participate more actively in the school day with control over their own activities encourages independence, a stronger group dynamic and a greater sense of belonging to the school as an institution. This was the goal of the “students helping students” project, developed by bettermarks in conjunction with the Heinrich-Mann school in Berlin.
Involving parents successfully
The Marie-Curie high school has been using bettermarks since 2011. Beginning with three classes in the first year, the school regularly adds new ones. This wealth of experience has given the teachers important insights into how to make the transition to digital teaching material as seamless as possible.
', '2018-12-07 19:33:03');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12968, 'Joining forces to revolutionize learning
Macmillan and bettermarks
Macmillan Education has enjoyed continuity in India’s educational market for over 120 years. Being one of the world''s leading publishers of school and higher education curriculum materials, Macmillan Education works with teachers, students, institutions, educational authorities and the Ministries of Education to develop high quality course books and supplementary materials to suit the needs of teachers and students at all levels. In the past few years the digital revolution has changed the face of school classrooms — instructional teaching modules, video clips and animations all leading to an interactive teaching learning process.  Macmillan prides itself on its willingness to innovate and employ visionary people with the creative and business sense to develop and implement new ideas. This focus has successfully led to the expansion of the group into new business areas and an ability to innovate in the digital space.
The bettermarks online solution helps students learn maths efficiently at the primary and secondary level (K12). Teachers and parents are actively integrated in the learning process through a comprehensive set of features such as performance and know­led­ge gap reports for an individual and a class. By integrating all of these features, bettermarks helps students achieve "better marks".
The content created by bettermarks
Provides state-of-the-art interaction and unparalleled logic.
Offers a learning experience that goes beyond "correct" or "incorrect." The system analyzes all answers in depth and detects the root cause of errors.
Proposes alternative solution paths and improvements, thus guiding each student individually and adaptively.
Has more than 100,000 exercises and support material documents (online maths textbooks, glossaries, etc.) available, which offer a motivating, intuitive and comprehensive learning experience.
Experience Reports
Classroom insights
Students helping students – a participation project
Allowing students to participate more actively in the school day with control over their own activities encourages independence, a stronger group dynamic and a greater sense of belonging to the school as an institution. This was the goal of the “students helping students” project, developed by bettermarks in conjunction with the Heinrich-Mann school in Berlin.
Starting off on the right foot: involving parents successfully
The Marie-Curie high school has been using bettermarks since 2011. Beginning with three classes in the first year, the school regularly adds new ones. This wealth of experience has given the teachers important insights into how to make the transition to digital teaching material as seamless as possible.
', '2018-12-07 19:33:03');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12968, 'We are happy to help. Questions, feedback or comments are welcome
 
', '2018-12-07 19:33:03');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12968, 'Any questions? We are happy to help.
Personal consultation
Please write us an email.
Online seminars
We offer online seminars for teachers free of charge. Click the button on the right to contact us.
', '2018-12-07 19:33:03');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12968, 'Any questions? We are happy to help.
Personal consultation
Please write us an email .
Online seminars
We offer online seminars for teachers free of charge. Click the button on the right to contact us.
', '2018-12-07 19:33:03');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (12968, 'Any questions? We are happy to help.
Personal consultation
Please write us an Email .
Online seminars
We offer online seminars for teachers free of charge. Click the button on the right to contact us.
', '2018-12-07 19:33:03');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13012, 'Who we are
 
Ateliere Fara Frontiere (AFF) is a Romanian non-profit association, Work Integration Social Enterprise creating jobs for disadvantaged people in social and solidarity workshops in order to prepare them for complete social and professional reintegration on the labor market. Our 3 missions are:
Fight against exclusion
Environmental protection
Solidarity with education and community development
Ateliere Fara Frontiere has launched, in may of 2009, Bucharest’s work integration workshop, which provides to people in great difficulty who are employed there:
An employment contract – maximum 24 months
A productive work experience, in one of the following activities: recycling, refurbishing, logistics, packing, merchandise handling, cleaning, transport, sewing
An individual follow up: counselling, orientation, training, job placement – in partnership with Non profits, institutions and companies
Designed as a training period with a career launch at the end, the social and professional program has the following objectives:
(re)building self confidence and confidence in others
(re)gaining autonomy
(re)building and consolidating social and family ties
(re)integration on the labor market and society as citizens with full rights
What do we mean by „people in great difficulty” ?
The employees undergoing the work integration program at Ateliere Fara Frontiere are people that accumulate a large number of difficulties: financial, family or justice related, without education or experience, homeless, who suffer medical, behavioral or discrimination problems, long term unemployment, who with addictions, disabilities, single women with kids or victims of violence, etc.
Despite having a wrong start or a life accident, making it difficult for them to find and keep a job, they are motivated and working towards rebuilding their lives regardless of the obstacles presented by discrimination and other difficulties they are confronted with.
They are recommended to Ateliere Fara Frontiere by our social partners who collaborate with us closely.
Throughout the program we work on integration, training and career counseling while the social partners keep following up on social, medical and psychological matters. In 2014 we have worked with 12 trusted partners: Samusocial in Romania, Caia Obregia, Casa Ioana, Concordia, The Probation Service, DGASMB, DGASPC sector 1, DGASPC sector 3, the APEL Service, Touched Romania, ACSIS, and the Solwodi Association.
Ateliere Fara Frontiere’s mission:
Creation and development of a work integration program
Ateliere Fara Frontiere employs tens of young people and adults from vulnerable groups and accompany them towards integration in society, the first step being a job in the real economy.
Fight against waste and pollution, for a sustainable development
By collecting, reusing and recycling waste from companies, Non profits and institutions, Ateliere Fara Frontiere takes part in waste reduction and implements a sustainable waste management option
Solidarity with education and community development programs
Ateliere Fara Frontiere provides Non profits and schools from undeserved communities all over the country with refurbished computers for free or at a very low cost. We also organize community work sites building playgrounds and other places dedicated to general interest.
Ateliere Fara Frontiere’s values
Man is at the center of the project
We must regard the people’s qualities and build from them, make them grow, instead of considering their weaknesses or their difficulties. People undergoing the work integration program are not only beneficiaries of Ateliere Fara Frontiere’s action, but actors of a common project. We always make things together with the people, never in their place.
Always be open to partnership
Ateliere Fara Frontiere’s action is based on the involvement of all economic and social stakeholders, and implies sustainable and quality partnerships with companies, institutions and the civil society.
Share experience and knowledge
Ateliere Fara Frontiere fights for causes that don’t belong to us we don’t consider ourselves being in competition with other organizations. In this regard, we share our input with all the actors that fight for the same causes and share our values.
Adauga un comentariu
', '2018-12-07 19:33:12');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13019, 'Yes, I accept the privacy policy and agree to the processing of my personal data.
Don''t fill this field!
Alpine Pearls – Green Travel with Mobility Guarantee
“Alpine Pearls” – for over 10 years the name for first-class quality when it comes to “soft mobility” in the Alps! Twenty-fore Alpine Pearls form a unique network of villages offering green mobility for your holidays in six Alpine countries. They offer special car-free adventures perfectly fitted for your needs while simultaneously guaranteeing full mobility at your holiday destination. This mobility starts on your train and/or bus trip to the Pearl.
 
At each Pearl, numerous shuttle services, hikers’ and ski buses, taxicab services, e-cars, bicycles and e-bikes make sure that you get around easily, yet without adversely affecting the environment. To complete this emphasis on environmentally friendly mobility, our Pearls offer Guest & Mobility Cards, which allow free access to local public transportation. Make your next trip a gently mobile adventure in an Alpine Pearl and travel green! The ultimate comfort and service of this “new type of traveling” may just be what you were looking for.
', '2018-12-07 19:33:12');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13019, 'Holiday bliss like no other
100 % relaxation, 100 % mobility – guaranteed
Green travel – the way of the future
Climate-friendly holiday adventures
Let yourself be spoilt with soft mobility
Naturally! Natural green travel to the “Alpine Pearls”
Alpine Pearls
Network of Charming Villages in the Alps Offering Green Mobility
What does “Alpine Pearls” stand for? It stands for car- and carefree holidays. It stands for restful, stress-free and fun leisure time in 25 unique villages in den Alps. And it stands for exceptional environmental friendliness and green mobility.
 
Like a necklace of pearls, these Alpine Pearl villages are strung across the entire Alpine area of Germany, France, Austria, Italy, Slovenia and Switzerland. This necklace includes well-known names of winter and summer sports destinations along with lesser known names of quaint resorts and insiders’ tips. What they all have in common is a concern for the environment and a method to protect it.
Our Member Villages, the Alpine Pearls - Villages Offering Delightful, yet Sustainable Holidays
The umbrella organization Alpine Pearls joins together 25 of the most gorgeous Alpine villages in their quest for gentle mobility and climate-friendly holidays. Guests at these villages will enjoy carefully chosen environmentally friendly mobility solutions like nowhere else. These handpicked villages provide a variety of mobility options ensuring your ability to get around in ways that do not adversely affect the environment. Guaranteed! You can pay attention to what matters to you: experiencing action-packed, pleasurable, relaxing holidays. At the same time you are making an important contribution to climate protection and you do your part in leaving this earth in good shape for future generations.
 
When you use the train and/or bus for your trip to a Pearl, you will take advantage of our typical Alpine Pearl comfort right from the start throughout your entire stay here.  This gentle way of combining recreation with environmental awareness is up for grabs right now! Why wait? Book your trip to an Alpine Pearl-affiliated host right now.
 
Every single Pearl is worth a trip. – Where to go first? South Tyrol at the foot of the Dolomites, Bad Reichenhall with its mild climate or the tiny hamlet of Chamois overshadowed by the Matterhorn? What these and all other Pearls have in common are spectacular mountain views, breath-taking beauty and authentic Alpine customs. No matter what time of year, these Alpine Pearls offer an abundance of recreational activities that will convince even the most demanding pleasure seeker. – They deliver on their promise to create unforgettable, climate-friendly and fun-filled holidays. And there is a special Pearl waiting for you whether you are looking for active outdoor adventures in the crisp mountain air or  would rather spend some relaxing days enjoying wellness & beauty treatments. Whatever your goal, you’re sure to find the ideal Alpine Pearl destination for your individual desires.
Limone Piemonte, Italy
Val Cenis Termignon, France
Premium Pearls: Specializing in Creating the Perfect Alpine Trip for your Wishes
Our Alpine Pearl villages offer numerous premium qualities with a wealth of different holiday packages. You want environmentally friendly activities, outdoor fun and regional culinary delights wrapped around adventure, variety and quality, right? Just key in your individual wishes for a perfect holiday and you’ll find the specialists offering exactly what you’re looking for. That’s the easy way to discover your perfect destination, your very own Premium Pearl, which provides everything you’ve ever dreamed of.
Pick your favourite Pearl:
Be active:
Hiking, walking, Nordic walking, mountaineering, climbing, bicycling, mountain biking, swimming, rowing, water sports, horseback riding, horse-drawn carriages, paragliding, cross-country skiing, biathlon, Alpine skiing, snowboarding, ski touring, ski mountaineering, ice skating, snowshoeing, sledding.
Soft mobility for you: Traveling by train, e-mobility, mobility solutions for day-trippers
Environment, nature & culture: Environmental quality, nature & enjoying the outdoors, wellbeing, local culinary arts
We Promise You a Perfect Holiday
The Pearls of the Alps are…
    25 villages, which invite you to experience gentle eco-holidays
    25 villages, which are easily accessible by train and bus
    25 villages, which guarantee all-round car-free mobility with numerous public transportation options
    25 villages, which invite you to experience new, environmentally friendly recreational/mobility options with top “Premium Quality” guarantee
    25 villages, where pedestrians can enjoy carefree times – far away from traffic noise and exhaust fumes
    25 villages, which offer outstanding services – such as information and consulting – to help you plan softly mobile holidays with ease.  Book your trip with one of the certified Alpine Pearl-affiliated hosts!
    25 villages, which preserve their regional, culinary, and cultural heritage  
Origins of the Alpine Pearls
In 2006, Alpine Pearls was established by 17 member villages, the “Pearls of the Alps”. The Association was the result of two successive EU projects (Alps Mobility and Alps Mobility II). Both of these projects originated in an initiative by the Austrian Ministry of Agriculture, Forestry, Environment, and Water Management. The idea behind the Alpine Pearls was to create innovative tourist packages that protect the environment. The results of these EU projects were implemented by creating the transnational umbrella organization Alpine Pearls for the entire Alpine region.
From 2007 to 2016, new members were added, all of which support eco-tourism, climate protection, and sustainable holidays.  Today, 25 Pearls of the Alps in six countries help you enjoy carefree holidays in one of the most spectacular regions of the world, while at the same time being mindful of the environment.
Let yourself be inspired by the Pearls of the Alps. Leave your car at home. Hop on a train or bus and take the time to discover the natural beauty of the Alps, the hospitality of the people and the rugged quality of the regions. Have a fantastic trip!
Alpine Pearls – Green Travel with Mobility Guarantee
“Alpine Pearls” – for over 10 years the name for first-class quality when it comes to “soft mobility” in the Alps! Twenty-fore Alpine Pearls form a unique network of villages offering green mobility for your holidays in six Alpine countries. They offer special car-free adventures perfectly fitted for your needs while simultaneously guaranteeing full mobility at your holiday destination. This mobility starts on your train and/or bus trip to the Pearl.
 
At each Pearl, numerous shuttle services, hikers’ and ski buses, taxicab services, e-cars, bicycles and e-bikes make sure that you get around easily, yet without adversely affecting the environment. To complete this emphasis on environmentally friendly mobility, our Pearls offer Guest & Mobility Cards, which allow free access to local public transportation. Make your next trip a gently mobile adventure in an Alpine Pearl and travel green! The ultimate comfort and service of this “new type of traveling” may just be what you were looking for.
', '2018-12-07 19:33:12');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13024, 'Delhi Metro Rail Corporation Ltd.
Metro Bhawan
Fire Brigade Lane, Barakhamba Road,
New Delhi - 110001, India
Board No. - 23417910/12
Fax No. - 23417921
Nearest Metro Station to reach Metro Bhawan is Barakhama Road Metro Station on blue line of the Dwarka-Noida city center/ Vaishali corridor (Line 3 and 4).
The address of office of the Claims Commisioner
Claims Commisioner, Delhi Metro Rail Corporation Limited,
Room No.1, Block B, Ground Floor,
Delhi Metro IT Park, East Approach Road,
Shastri Park, Delhi-110053.
', '2018-12-07 19:33:14');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13024, 'rajib_3441[at]dmrc[dot]org
Mahender Singh
', '2018-12-07 19:33:14');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13024, 'हिन्दी
About Training Institute
Since inception, DMRC has appreciated the need of competency building in rail                                                                                    based urban transportation systems. In its efforts to impart world class                                                                                    training a dedicated Training Institute was set up & inaugurated on 19 July 2002                                                                                    in SHASTRI PARK TRAIN DEPOT of DMRC. The Training Institute is ISO 9001:2015                                                                                    accredited for design, development and delivery of training programmes. Training                                                                                    Institute is equipped with state of art training infrastructure, most advanced                                                                                    simulators for training in the field of operations and maintenance of rail based                                                                                    urban transportation systems. We have distinction of training not only DMRC                                                                                    employees but also employees from other metro systems such as Bangaluru Metro,                                                                                    Chennai Metro, Jaipur Metro, Mumbai Metro and Kochi Metro. We are also                                                                                    equiped to train staff of neighboring countries in the fild of urban                                                                                    rail trasportation.
Training Institute-Mission                                                                                    statement
Corporate Culture:‘DMRC is a Learning Organization and it will strive to improve                                                                                    in whatever it does’.
1. To be the most preferred ‘total training solution’ provider throughout the                                                                                    community it serves.
2. To promote conducive environment for ‘individual and organisational learning’                                                                                    by building ‘right facilities’ and human resources.
3. To harmonize and control training processes to develop competence for various                                                                                    job profiles, enhance effectiveness, by keeping abreast of the latest development of technology and working environment.
4. To inculcate essence of professionalism and organisational culture amongst                                                                                    trainees to ensure that business is conducted in accordance with the ethical                                                                                    standards.
Quality Policy
Delhi Metro Rail Corporation Training Institute is committed                                                              to harmonise and control training processes in order to                                                              enhance overall productivity, develop competence for                                                              various job profiles and to provide training keeping                                                               abreast with changes in technology and continuously                                                                improving training processes.
.
', '2018-12-07 19:33:14');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13027, '© Copyright 2012 Medical Diagnostech (Pty) Ltd. All rights reserved.
Monday -  Friday     8:00 am - 5:00 pm
 
', '2018-12-07 19:33:14');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13050, 'Startseite
Help without Insurance
More and more people fall through the cracks oft he socal net and are wtihout insurance coverage. According to Armutskonferenz"Over 100000 Immigrants, Asylum seekers and Austrian citizens, men women and children have no health insurance. Many of our patiens come from other cultures and do not speak german well. Their attitude towards health, sickness and treament is usually different. Because of their legal status, they have no access to public healthcare and live in poor conditions. They cannot afford medicine and treatment.
Project Amber was launched on January 12th trhough the Diakonie Refugee Service. At first the program treated primarily the many homeless and the uninsured asylum seekers; Over time uninsured Austrians as well as immigrants also came to ambermed tobe insured. Since August 2006, Amber has been cooperating with the Austrian Red Cross and the Diokonie Refugee Service under the name AmberMed.
AmberMed relies heavily on external financial support In order to maintain its operations. Part of our funding comes from the public funds of the Vienna Health Insurance the Vienna Social Fund and the Federal Ministry of Health. The MEDUNA project is also co-financed by the Fund for a health Austria. However the biggest portion of our funding comes from private donations, which help to insure the continued existence of AmberMed
The AmberMed Team thanks you on behalf of our patients.
© 2018 AmberMed | HalfPixel.sk
AmberMed ist ein Kooperationsprojekt der Diakonie und des Österreichischen Roten Kreuzes
Oberlaaer Strasse 300 - 306 , 1230 Wien,  T:  01/589 00 - 847,   F:  01/253 303 330 23,  amber@diakonie.at
', '2018-12-07 19:33:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13050, 'Home
Help without Insurance
More and more people fall through the cracks oft he socal net and are wtihout insurance coverage. According to Armutskonferenz"Over 100000 Immigrants, Asylum seekers and Austrian citizens, men women and children have no health insurance. Many of our patiens come from other cultures and do not speak german well. Their attitude towards health, sickness and treament is usually different. Because of their legal status, they have no access to public healthcare and live in poor conditions. They cannot afford medicine and treatment.
Project Amber was launched on January 12th trhough the Diakonie Refugee Service. At first the program treated primarily the many homeless and the uninsured asylum seekers; Over time uninsured Austrians as well as immigrants also came to ambermed tobe insured. Since August 2006, Amber has been cooperating with the Austrian Red Cross and the Diokonie Refugee Service under the name AmberMed.
AmberMed relies heavily on external financial support In order to maintain its operations. Part of our funding comes from the public funds of the Vienna Health Insurance the Vienna Social Fund and the Federal Ministry of Health. The MEDUNA project is also co-financed by the Fund for a health Austria. However the biggest portion of our funding comes from private donations, which help to insure the continued existence of AmberMed
The AmberMed Team thanks you on behalf of our patients.
© 2018 AmberMed | HalfPixel.sk
AmberMed ist ein Kooperationsprojekt der Diakonie und des Österreichischen Roten Kreuzes
Oberlaaer Strasse 300 - 306 , 1230 Wien,  T:  01/589 00 - 847,   F:  01/253 303 330 23,  amber@diakonie.at
', '2018-12-07 19:33:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13203, 'Groundswell are based in Brixton, London at the following address:
Groundswell
', '2018-12-07 19:33:24');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13204, 'Contact us
Contact Us
You may contact us by filling in this form any time you need professional support or have any questions. You can also fill in the form to leave your comments or feedback.
Name:
', '2018-12-07 19:33:24');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13211, '> About Us
The Time Credits model works simply:
for each hour that an individual contributes to their community or service, they could earn a Time Credit. This Time Credit can then be spent on accessing an hour of activity, such as local attractions, training courses or leisure, or gifted to others.
HOW WE WORK
We work in partnership with local authorities, Clinical Commissioning Groups, Health Boards, housing providers, health and social care providers, schools, voluntary organisations and businesses to fund, co-design and deliver each Time Credits programme.
> Find out more about local Time Credits programmes and their objectives
Time Credits programmes build connections at a local level by joining up the public, private and voluntary sectors in a community. Individuals earn Time Credits through a network of local community organisations, charities and services that we engage and support to reach new people and thank existing volunteers with Time Credits.
We develop spend partnerships with public, private and voluntary sectors that enable individuals to access a wide range of positive activities. Spend opportunities across our UK network can be accessed by members from any programme. For example, someone might earn some Time Credits by volunteering their time for a community group in Lancashire, and choose to spend them on a visit to one of the many London attractions that accept Time Credits in exchange for entry.
OUR VISION
Since we started in 2008, Spice has established Time Credits networks in six regions of the UK, and over half a million Time Credits have been earned. 
Our vision is for strong, connected communities where everyone’s time is valued. We want to work with more communities in England and Wales – enabling more people to contribute to their local community, and be recognised for the time that they give.
To achieve this, we will use Time Credits to engage new people in communities and in shaping local services, improving health and wellbeing for all.
> Find out more about developing a Time Credits programme .
WHO WE ARE
Our charity, Spice, was founded to support and strengthen disconnected communities using Time Credits. ‘Time Credits’ was initially developed at The Wales Institute for Community Currencies (WICC), which explored ways that alternative currencies could be used to help rebuild the ex-mining communities of South Wales.
Our model builds on the concept of Time Banking: a currency system that uses time as the unit of exchange. This concept was theorised and popularised by human rights lawyer Edgar Cahn in 1986.
We have a fantastic team, headed up by our CEO Ian Merrill. 
Our Board members are :
', '2018-12-07 19:33:25');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13213, 'Register for YE Online
WHAT WE DO
Young Enterprise is the UK’s leading enterprise and financial education charity reaching over 250,000 young people every year.
 
What we do:
We make the connection between school and the world of work, enabling young people to develop the knowledge and attitudes they need to succeed, building on 8 key skills:
Communication, Confidence, Financial Capability, Initiative, Organisation, Problem-solving, Teamwork and Resilience.
 
What we offer:
We offer something for every year of education from our Primary Programmes  to our Secondary Programmes (including our year-long  Company Programme ) to the university-level  Start-up .
Every year we work with over 250,000 young people, support over 9,000 teachers in both enterprise and financial education, with the help of more than 7,000 volunteers and 3,500 businesses.
 
Our Impact:
Since we were established, over 4 million young people have taken part in Young Enterprise programmes in the UK. One million of these have run businesses for a year on our Company Programme. We take evaluation very seriously and that’s why we measure the impact of everything we do.
', '2018-12-07 19:33:26');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13213, 'You can search for the right region for you by entering your full postcode here:
 
Get in touch with Young Enterprise:
 
', '2018-12-07 19:33:26');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13215, 'What We Do
Programmes
UpRising  Programmes  help young adults to reach their full potential by developing their knowledge, skills, confidence and networks (our Theory of Change).  
 
When they graduate, we see real results such as, massively raised aspirations, boosted employability, the increased likelihood of getting into leadership roles, better educational chances and strengthened communities through their social action activity.
 
Watch our ‘Bridges to Power’ video and hear from our UpRisers and supporters. You can also download our brochure here .
 
We believe that Leadership and employability can’t be taught in a classroom. 
 
Our Programmes provide a unique learning journey for our young adults - far removed from anything they experience in their normal day-to-day lives.  To do this, UpRising engages with companies across all sectors to ensure that our young leaders have access to a wide selection of the very best Role Models.
 
Across our 2016/17 Programmes a staggering 1,334 hours of time were donated, which equals to just over 55 days worth of expertise, knowledge sharing by our Role Models in just one year.
 
Our UpRisers have benefitted from the mentorship, talks and visits to the likes of,
 
Media / Cultural BBC, ITV, The Guardian, Buzzfeed, Bloomberg, Sky News, FACT, HOME
Business KPMG, PwC, EY, Siemens, Accenture, Brother UK, IBM, DONG Energy, AstraZeneca, Barclays, International Festival for Business, Deloitte, Mayer Brown, Co-Op, Bruntwood
Education Cardiff, Manchester / Met, Aston, Liverpool, John Moores, Birmingham City, Queen Mary, Bedfordshire, BPP, Salford and Bolton Universities and regional colleges / sixth forms.
Third Amnesty, Canals and Rivers Trust, Change.org, City Year, NSPCC, The Real Junk Food Project, Media Trust, Factory Youth Zone
Public Job Centre +, National Careers Service, Local Police & Fire Services, NHS Trust, Media Trust
Government / Policy IPPR North, CLES, Local & Combined Authorities, European Parliament, local MP’s, Cabinet / Foreign and Commonwealth Office, all major political parties, HM Treasury, Palace of Westminster, Ministry of Justice, NESTA
 
Find out how you can get involved  here .
Find out more
', '2018-12-07 19:33:26');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13215, 'About Us
 
UpRising is a national youth leadership development organisation. We are recognised at Government-level as a pioneering charity championing the critical issues surrounding diversity, social mobility and equality.
Our important work provides routes to leadership and employment for 16-25 year olds who have talent, but lack opportunity. The mission is to break the cycle of unrepresentative power in the UK, by developing new, community-minded and socially-conscious leaders; so that our future decision-makers truly represent our diverse communities.
We want to change the face of power in this country - quite literally.
Our Programmes run in London , Luton , West Midlands , Cardiff City Region and Greater Manchester .  We have also worked in Merseyside, Bedford, Tamworth, Lichfield, Stoke-on-Trent and Blackburn.
We have a growing movement of over 3,300 UpRising Alumni, who importantly, remain involved in their community and with our work.
 
', '2018-12-07 19:33:26');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13247, 'I consent to The Mandometer Clinics collecting my details through this form.
Send
We use cookies to improve the way our website works. By using this website, you agree to our use of cookies and also our Privacy Policy.  We at the Mandometer Clinics take the necessary steps to ensure that personal data are handled in accordance with the requirements of the GDPR. OK Read more
', '2018-12-07 19:33:32');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13248, 'I consent to The Mandometer Clinics collecting my details through this form.
Send
We use cookies to improve the way our website works. By using this website, you agree to our use of cookies and also our Privacy Policy.  We at the Mandometer Clinics take the necessary steps to ensure that personal data are handled in accordance with the requirements of the GDPR. OK Read more
', '2018-12-07 19:33:33');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13253, 'Do you have any questions for us? bos@bos.rs Masarikova 5/16 Belgrade, Serbia 381 11 3065 800
Belgrade open school
BOS develops human resources, improves public policies and strengthens the capacities of the civil, public and business sectors in order to develop a better society based on freedom, knowledge, and innovation.
© 2016 Belgrade open school
', '2018-12-07 19:33:36');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13253, 'Quality traineeship
About us
The Belgrade Open School Improvement of Employability programme sees its mission in supporting individuals in their finding creative answers to the challenges of finding, getting, keeping and advancing in both their professional and personal lives. In the era of a dynamic global labour market, common economic and social crisis, we believe that it is important to affect the development of career management skills, lifelong learning and as painless a transition as possible from education onto the labour market (or vice versa when the career development demands it).
So our team has, starting from 2003, been encouraging the individual''s continuous development, lifelong learning and  overall economic development of our society by contributing to the establishment of the career guidance and counseling system and by strengthening the connection between education and the labour market.
Our goals are:
Further development and quality improvement of the career guidance and counseling system;
Building the capacities of all participants in the career guidance and counseling process
Conceptualization, creation and promotion of innovative career development models
Supporting education reforms which will enable acquring competencies that are in accordance with the needs of the labour market
Promotion of the concept of quality traineeships
More effective employment and economic growth
The Improvement of Employability programme realizes its activities through two complementary areas: Career Guidance and Counseling and Linking Education and the Labour Market . Activities realized:
Developing strategic documents (models, policies, strategies);
Supporting institutions in the further development of the career guidance and counseling system and practice;
Developing and promotion of using innovative techniques/tools and instruments for career guidance and counseling;
Raising the capacities of those who provide career guidance and counseling services, as well as individuals, by organizing trainings and seminars;
Career informing of youth and their parents;
Individual and group career counseling of youth;
Online career counseling and informing of individuals via BOS Career ;
Research in the Career Guidance and Improvement of Employability area;
Publishing works in the Career Guidance and Improvement of Employability area;
Organizing traineeships, mentorships, visits and other forms of connecting unemployed young people and employers;
Establishing a network of interested parties, connecting employers, education representatives, employment, youth policy actors;
Monitoring the potentials, needs and cooperation between the education system and the labour market;
Supporting young people in the process of employment, self-employment and entrepreneurship ventures;
Promoting perspective occupations, entrepreneurship and good practice examples.
Latest news
Do you have any questions? puz@bos.rs Masarikova 5/16 Belgrade, Serbia 381 11 30 61 341
Improvement of employability
', '2018-12-07 19:33:36');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13253, 'Latest news
Do you have any question for us? bos@bos.rs Masarikova 5/16 Belgrade, Serbia 381 11 30 65 577
Civil Society
BOS strengthens human resources, improves work of public institutions and organisations, develops and advocates public policies in order to develop better society based on freedom, knowledge and innovation.
Participate in building civil society!
© 2016 Belgrade Open School
', '2018-12-07 19:33:36');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13260, 'Join our email list! SIGN UP
Go4Life is a registered trademark of the U.S. Department of Health and Human Services.
', '2018-12-07 19:33:38');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13322, 'Want to talk? Get in touch
General Info:
', '2018-12-07 19:33:52');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13325, 'About us
About us
creACTive is a youth association for support of creativity and active citizenship of young people. Formed in 2007, creACTive has a main aim to assist young people’s development by providing them with a variety of non-formal education activities.
creACTive currently has one Office in Kavadarci. More information about their work is available under the “ ACTions ” section.
creACTive implements various youth projects, on both local and international level. While the local activities range from workshops, concerts, volunteer actions, seminars and sport competitions to various performances and other events, creACTive’s international work includes youth exchanges and training courses organized in Macedonia and in other countries, mainly in the framework of the Eramus + Programme of the European Commission. creACTive also sends and hosts volunteers through the European Voluntary Service.
Feel free to contact us for more detailed information about our work. If interested, you are always welcome to join our activities!
Find us on Facebook
', '2018-12-07 19:33:53');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13326, 'By doing what’s right.
By doing what’s needed.
By doing what works.
What we do
How Action for Children works
From before they are born until they are into their twenties, we help disadvantaged children across the UK.
We help them through fostering or adoption – and by intervening early to stop neglect and abuse. We make life better for children with disabilities. We influence policy and advocate for change. Our 7,000 staff and volunteers operate over 522 services, improving the lives of 301,000 children, teenagers, parents and carers every year. We succeed by doing what’s right, doing what’s needed, and doing what works for children.
 
We always demonstrate what we do, and show how it works.
From our origins running children''s homes, our work has spanned three centuries, developing services for children that are proven to work. We work in partnership with other charities, local authorities and health services, and are supported by a team of experts and specialists. Evidence drives everything we do.
', '2018-12-07 19:33:55');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13326, 'Find out more about our history,
ambassadors and services.
About us
When Methodist minister Thomas Bowman Stephenson saw children living rough under the arches of Waterloo station in London, he decided he had to act. And his first action was to listen.
He paid children the respect of hearing their stories, before working out the most practical way to help. 
A century and a half later, the spirit of our founder remains our inspiration. We take action on behalf of 301,000 children, young people, parents and carers across the UK. Our mission is to do what’s right, do what’s needed, and do what works for children.
', '2018-12-07 19:33:55');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13326, 'Or write to us at:
Action for Children
Watford
WD18 8AG
If you have a concern about our Fundraising activities, you can find further information by reading our Fundraising Complaints Procedure . Our fundraising promise can be found here.
If you would like to know more about making a complaint about our services to children, young people and families, you can read further about our Complaints Procedures - Services .
I have concerns about a child
We cannot undertake investigations into specific cases of child abuse. If you have concerns about a child you should contact either social services or the police. For more information read Worried about a child .
Any messages we receive that raise child protection issues will be forwarded to the police or social services.
More information on how to report child abuse can be found on the government website:  https://www.gov.uk/report-child-abuse
', '2018-12-07 19:33:55');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13326, 'Get in touch
Contact our team
If you have a query just fill in the form below and we''ll get back to you. Or you can call us for a chat on 0300 123 2112 (open 9.00am to 5.00pm, Monday to Friday).
* indicates required field
What is your relationship to Action for Children?*
Type of enquiry*
', '2018-12-07 19:33:55');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13344, 'Department of Justice (DOJ)
Federal Partners in Bullying Prevention
StopBullying.gov coordinates closely with the Federal Partners in Bullying Prevention Steering Committee, an interagency effort co-led by the Department of Education and the Department of Health and Human Services that works to coordinate policy, research, and communications on bullying topics. The Federal Partners include representatives from the U.S. Departments of Agriculture, Defense, Education, Health and Human Services, the Interior, and Justice, as well as the Federal Trade Commission and the White House Initiative on Asian Americans and Pacific Islanders.
Sharing Information
Unless otherwise noted, text and documents on the StopBullying.gov website are in the public domain and may be copied and distributed. Please credit StopBullying.gov as the source of the material. 
Many of the images used on this website are licensed stock photography images that feature model(s). The models have no affiliation with the Department of Health and Human Services (HHS) or StopBullying.gov and are for illustrative purposes only. The stock photos are not in the public domain and may not be utilized by others under the HHS or StopBullying.gov license.
Some checklists and planning materials are available in PDF format so you can easily make copies. Please note, if you adapt or modify the materials in anyway, all StopBullying.gov citations and logos must be removed. If copyrighted content, documents, images, or other materials appear on StopBullying.gov, it will be noted, and you must contact the copyright holder before you can reproduce that material.
You are welcome to direct or link to www.StopBullying.gov in your materials, but including this link does not constitute an endorsement or partnership by StopBullying.gov or the Departments represented on the Editorial Board.
We encourage website managers to link to StopBullying.gov. Please identify the site as providing one-stop access to U.S. Government information on bullying topics.
Content last reviewed on August 14, 2018
A federal government website managed by the
U.S. Department of Health and Human Services
200 Independence Avenue, S.W. Washington, D.C. 20201
', '2018-12-07 19:33:56');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13373, '700 Traditional                      paddy varieties conserved
8000 Farmers are networked
1500 acres cultivating paddy varieties
Millet Diversity
68 Traditional millet varieties                      conserved
300 farmers are networked
250 acres cultivating millet varieties
Objectives of Sahaja
To promote sustainable agriculture and create                                awareness on the need to conserve natural resources                                and traditional knowledge systems
To conduct on-farm research and standardize sustainable                                agriculture practices
To capacitate Farmers, Panchayat representatives,                                Non government organisations, Government Officials                                and Policy planners on sustainable agriculture and                                natural resource management
To assist in implementation of sustainable farming                                techniques to farmers and grassroots organizations,                                in the process of converting their land into organic                                farms,
To disseminate information, research outcome, knowledge and thought on sustainable agriculture                                and natural resource management through publications                                and audio/visual materials
To facilitate procurement, marketing and sales                                of organically produced products
 
', '2018-12-07 19:33:57');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13535, 'We built a business that incentivizes conservation.
 
Our founders saw that traditional models of wildlife conservation weren’t working. Telling people not to poach didn’t address the root cause of their action – rural poverty. Instead of punishing people for poaching, which only exacerbated issues of hunger and impoverishment when a family member went to jail, COMACO designed a system that instead rewards people for conserving their natural resources. We ask everyone we work with – poachers and farmers – to take a Conservation Pledge, agreeing to abide by a set of community-decided principles designed to safeguard the health of their soils, forests, and wildlife. In exchange, we offer extensive trainings, support, and the means to secure a substantial, reliable income through farming. With the economic driver removed, incidents of poaching have dramatically decreased. Instead, villagers are busy tending to their fields, producing high yields of nutritious food crops, which we purchase at premium prices and turn into quality products sold across Zambia under the brand It’s Wild!  Our systems approach is working: incomes are rising, nutrition is improving, and wildlife is returning to the Luangwa Valley.
15 years of operation in Eastern Zambia
Working with 179,000 farmers
Established and partnered with 80 community cooperatives
Community Partnership
With over 15 years of operation in Eastern Zambia, we have built deep and lasting relationships with community leaders across the region. The mutual trust and respect between COMACO’s staff and our local partners sets us apart, and makes our work possible.
Conservation Dividend
At the end of each year we conduct an audit that scores our partner communities on how well they’ve complied with their conservation standards. High scoring communities are paid a substantial Conservation Dividend, that supports community development projects and further farmer support services.
Sustainable Development
Unlike traditional aid programs, our operational costs are partially offset by the sale of our food products. This market-driven approach to conservation is designed to be self-sustaining, improving the lives of thousands and restoring wildlife to the Luangwa Valley for the foreseeable future.
Our Approach
COMACO employs over 300 Zambians. Meet a selection of our team.
Dale Lewis
', '2018-12-07 19:34:12');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13543, 'Signal House,  595 Andries ST,  Pretoria,  0001
 
Sifiso is passionate about community development and has over 10 years’ experience in Programme Administration and HR Administration. She has worked for international NGOs and the private sector and is looking forward to contributing to the growth of IY.
Simone is originally from Cape Town, but has lived in Johannesburg for the last few years (and more recently spent a few months working in Vietnam). In Johannesburg,she worked as a consultant for Ernst and Young as part of their People and Organisational Change team. While Simone greatly enjoyed her experience there, it has been great to be back in Cape Town and to be part of an organisation doing such incredible work to uplift the youth.
Zukile is an IkamvaYouth ex-learner, tutor, Branch Assistant, Branch Coordinator, Schools Coordinator for the Year Beyond Project and currently fills the role of Branch Coordinator at Gugulethu Comprehensive. He has a degree in information systems and has been based at the Makhaza branch for 10 years. Zukile has substantial experience working with learners from the Khayelitsha community and is highly motivated to put learners from disadvantaged backgrounds on the path to earning a dignified living.
Nokubonga is an ex-learner from the class of 2009. She is an activist and a feminist and has been doing advocacy work at various organisations since 2008. Prior to joining IkamvaYouth, she was involved with Equal Education, Women''s Legal Center, Grassroots Boxing Academy, #UniteBehind, and was recently confirmed to be a mentor in the Mandela Washington Fellowship. She is the Co-founder of Equal Education''s society at UCT and has initiated a psycho-social group called No More Victims. She is driven by social justice and creating change in socio-economic issues especially education and gender issues.
Portia Siphokazi Ntlati
siphokazi@ikamvayouth.org
Nazeema Isaacs Library, Cnr Landsdowne & Cekeca Road, Makhaza, 7784
Siphokazi is the Makhaza Branch Assistant. She graduated from Cape Peninsula University of Technology with a National Diploma: Office Management and Technology. Siphokazi joined IkamvaYouth in 2016 towards the end of the year, in October to be specific as an intern.
Zindi Mankayi
zindi@ikamvayouth.org
New Eisleben High School, Corner Stulo & New Eisleben Road, Crossraods, Cape Town 7755
Zindi holds a BA Degree in Humanities. She is currently pursuing her BA Honours Degree in Development Studies. Zindi hopes to use her knowledge to add value to IkamvaYouth and the community at large. She has spent most of her life in Cape Town and is grateful to have the opportunity to have a positive impact on the lives of the youth in her community. Zindi has a passion for reading, education and self-empowerment.
Wendy joined IkamvaYouth in 2017 as a Year Beyond volunteer. She holds a BSc Degree in Environmental and Water Science from UWC. She has just finished a short course from CPUT in Human Resources Management. She believes that as a person, you will always be a piece of a puzzle needed to complete someone else’s life, and in this case she is the piece required for the learners that she works with. She has a passion for youth development, and wants to see South African youth empowered, educated and out of poverty.
Nicolas Commeignes
nicolas@ikamvayouth.org
1522 Mothapeng Road, Masiphumelele / Fish Hoek, Cape Town 7975
Nicolas is interested in social development topics and has been involved with IkamvaYouth since April 2009. He started as the Winter School Coordinator at the Makhaza office before coordinating the Masiphumelele Branch till March 2013. He is back in this position since January 2015. In the past Nicholas has been involved in various youth development projects in France, Latvia and Romania. He also managed youth centres and facilitated numerous leadership trainings.Nicolas graduated with a State certification in social organisation coordination. His involvement at IkamvaYouth can be summarised by saying that he wants to encourage Youth from Masiphumelele to discover their own potential, so they may bring about positive change for themselves, their family, their community and South Africa.
Lungile grew up in Cape Town, Makhaza. He has been involved in the after school space for more than 5 years. Lungile holds 3 leadership courses from Stellenbosch University and currently furthering his studies at Unisa in Project Management.  Youth development is his passion, seeing young people from disadvantaged background (townships) reaching their fullest potential and becoming positive role model in their communities brings smiles to him.
Yanga Totyi
yanga@ikamvayouth.org
Proteus Technical High School, Reygersdal Dr, Atlantis, 7349
Yanga is an aspiring Social Entrepreneur who is passionate about making an impact in the world, especially in areas that involve education. She is currently completing her studies at Unisa, pursuing a BSc in Mathematics and Statistics. She has aspirations of becoming an Analyst especially for NGOs and schools.
Andrea Pietersen was born and raised in Atlantis where she is currently based as the Branch Assistant at Atlantis. She aims to complete her studies in business administration at Cape Peninsula University of Technology. She has always been involved in community outreach programmes and these programmes have influenced who she is today. She believes in personal and professional development in any form. She is very proud of the work that she is doing at IkamvaYouth and believes that education is power.
Phelela Mokwana
phelela@ikamvayouth.org
Kuyasa Public Library, Walter Sisulu Avenue, Khayelitsha
Phelela is originally from the Eastern Cape in Tsolo village but lives in Cape Town, Khayelitsha. She is a Retail Business Management graduate from CPUT. Phelela was once a learner at Ikamva during my high school days from 2006-2008. She is a Programme Coordinator at Kuyasa branch. Phelela''s experience working at Ikamva has been amazing. She started working for IY during winter school 2016, it just reminded her of the days when she was a student. Seeing learners engaging themselves in the tutoring space and the commitment of tutors has just reminded her of why she came back to work for Ikamva.
I am an Applied Communication Management graduate from University of Fort Hare. I have an experience in Communication field and have worked as a Desktop publisher. In July last year I joined Ikamvayouth as an intern at Kuyasa Branch where I learnt that collaboration and peer-peer support is very important when working with different kinds of personalities, now I am a Branch Assistant at Kuyasa Branch and it feels good to be a part of the IY Family. Being at IY reminds me of Les Brown’s words ‘’ Help others achieve their dreams and you will achieve yours
Nosi is our Gugs Comp Branch Coordinator. She completed her degree in Political studies and her honours in political communication. My interest in South African politics led me to become involved with work on the ground rather than policy. Prior to joining IkamvaYouth, she volunteered at many different non-profit organisations. She loves working with young people and loves the energy they bring.
Zintle Nyathi
zintle@ikamvayouth.org
Intshukumo Comprehensive Secondary/ Gugulethu Comprehensive, 50 Ny, Gugulethu, 7750
Zintle is an IkamvaYouth alumnus, having matriculated with the class of 2013. She is keen on community development and positively impacting the youth. Zintle''s goals have kept her working hard against difficult odds and the time she gets to spend with the learners has filled her life with joy and the desire to do even more. Being part of Ikamvayouth has taught Zintle a lot through her time in high school, now she is paying it forward by assisting others.
Ezekiel is a young, vibrant, hardworking individual from Pretoria, South Africa who is passionate about driving change in the communities through education. He is currently pursuing a Mechanical Engineering degree. Having worked for some time in the after school space as a volunteer he believes that his role at IkamvaYouth gives him an opportunity to effectively contribute to the youth of South Africa so that they can also have a chance of pulling themselves out of poverty.
Lunika graduated at CPUT with a Financial Accounting Diploma, prior to that she studied a number of courses at college in Financial Management, Business Management Intro, Human Resource Management and Computers. She is continuing her studies and is doing her second year in a BCom degree, she is also an intern at Year Beyond Gugs Comp.
Buyisiwe Khumalo
', '2018-12-07 19:34:12');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13549, 'What We Do
What We Do
Tourism is the world’s largest job creator and Open Africa stimulates rural development by leveraging communities’ collective tourism assets. It does this by connecting remote areas with travel markets in a competitively sustainable and innovative way. It starts with identifying products and clustering entrepreneurs into branded networks that strengthen their combined power and market appeal.
Entrepreneurs are connected to customers through the Open Africa website and other marketing platforms. It turns conservation into a wealth creator and builds capacity through sharing knowledge and experience in a network that embraces them continuously thereafter.
 
We Work With Communities To
Link existing community-based tourism businesses (e.g. accommodation providers, tour guides, local artisans) into off-the-beaten track, self-drive routes, clustering travel attractions in an area for travellers to explore;
Identify enterprise opportunities along a route and match them with potential or existing entrepreneurs willing to take these up;
Help develop the business concept, product development and branding, as needed;
Build capacity through training workshops and mentorship programmes using specialised partners as needed; training varies per project, but can include the development of technical, entrepreneurial and life skills;
Create linkages to access markets using Open Africa routes as platforms;
Market routes by transferring marketing know-how and skills to local entrepreneurs and showcasing their offering on an innovative travel portal and through other marketing initiatives;
Install monitoring and evaluation tools to track route challenges and outcomes; and
Enhance conservation through monitoring of a flagship plant or animal species along a travel route, educating local people about their environment as a potential wealth creator.
 
About Open Africa
Open Africa is a social enterprise that works with small businesses to establish rural tourism routes that offer travellers authentic experiences, while generating income and jobs for local people.
Link to Open Africa
Click here to see how you can add a link to Open Africa on your website.
Featured Routes
', '2018-12-07 19:34:14');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13549, 'Founder
Noel De Villiers
... founded Open Africa. A farmer''s son who started out as a commercial entrepreneur, he founded Avis Rent a Car in Southern Africa. Later entered the corporate world as Managing Director of the Security, Travel, Transport, and Tourism interests of the Rennies Group and thereafter served as Chairman and Managing Director of a similar division in the Freight Services Group.
Founded Prime Leasing in partnership with Nedfin, founded SAVRALA (The Southern Africa Vehicle Rental and Leasing Association); and served as deputy chairman of SATOUR. Member of the IUCN''s World Commission on Protected Areas (WCPA), and founder member of the Peace Parks Foundation.
Board of Directors
Alastair Collins
... Irish by birth, is married (to Katrine), has two daughters (Kerry & Clare) and has recently returned from London to reside in Cape Town. He commenced his career in South Africa with Roberts Construction and probably is still the only Irish graduate of Free State University.
He went on to serve as a Partner in Davis Langdon (the world''s largest construction consultancy) for some 30 years, first in Southern Africa and then in London. He served on the firm''s Board from 1995 and in the five years from 2003 to 2008 as its Chief Executive and International Managing Partner.
As Chief Executive he led a multi-cultural international team which successfully motivated, developed and delivered innovative strategies and initiatives, as business plans, within complementary budget parameters. Much of his sustained success came from nurturing internal and external relationships, ensuring effective communications, together with sound financial risk management and reporting.  Beyond commercial concerns, a further element of his brief was to harness the firm''s international ''corporate social responsibility'', one output from which was the direct financing, design and delivery, to the local community, of an orphanage school in Aceh, in the aftermath of the 2004 Tsunami.
He also established and administered an international bursary fund, focused on tertiary education, which enabled successful applicants to realise their academic potential.
Extramurally, Alastair has been a frequent author of articles and speaker at conferences on a broad range of themes relative to the built environment. He Chairs the South African chapter of the Council on Tall Buildings and Urban Habitat, which focuses on the creation of sustainable city environments for the future. He also advises ‘Super Neighbourhoods’, a charitable enterprise dedicated to uplifting the physical environments of underprivileged communities. He is a Director of an energy drinks company and of a company specialising in the production of life saving auto-disabling syringes for Africa.
Charmaine Groves
... cum laude UCT MBA - founder of SABRI (SA Business Resources Institute).
Charmaine has wide experience across a number of disciplines and serves on the boards of Pro-active Health Solutions and the Small Enterprise Foundation, as well as undertaking mentorship among Old Mutual women in middle management.  Charmaine’s CV reads like a first class in everything, which is why Open Africa is especially privileged to have the benefit of her wisdom and experience. She lives in Johannesburg and frequently commutes between there and her other office in Cape Town.
Lehotlo Ramokgopa
... is a BSc graduate in Computer Science and Geology with an MBA. After a career in information technology (IT) consulting and banking, Lehotlo became a partner at KPMG until recently, when she decided to pursue her own entrepreneurial interests. She is now managing director of her own consulting company, Evolut Consulting, which focuses on BEE legislation, small business development, rural development and strategy consulting.
The work she has done in the recent past was for state-owned and private entities where BEE assessments were done to evaluate processes that needed to be put in place in order to facilitate the incorporation of BEE into the business operations and to become BEE-competent organisations.
Lehotlo''s core skills are in strategy formulation and implementation, information technology, project and programme management, small business development, project funding, performance measurement, risk management, BBBEE and transformation, all subjects germane to Open Africa''s initiative.
Francois Viljoen
... is a trained geographer with significant experience in rural and community development. Francois joined Open Africa in 2005 as a route developer before moving into the role of operations manager and finally, general manager in July 2012. He has an MA in geography and environmental science, a BA (Hons) in environmental spatial planning and analysis, a BA in information science and is currently completing his MBA with the University of Stellenbosch.
Staff
Francois Viljoen, Managing Director
"Initially, I was drawn to work here because it allowed me to travel. What I didnt anticipate was how much I would fall in love with the participants. Africa is home to so many extraordinary people, all of whom have wonderful stories to tell. I love Open Africa because it gives me the opportunity to help those people tell their stories."
A trained geographer with significant experience in rural development, Francois joined Open Africa in 2005 as a route developer before moving into the role of operations manager and finally, general manager in July 2012. He has an MA in geography and environmental science, a BA (Hons) in environmental spatial planning and analysis, a BA in information science and is currently completing his MBA with the University of Stellenbosch. Like most of the Open Africa team, Francois enjoys the outdoors. He tries to spend every available weekend hiking the mountains of Cape Town with his two Labrador retrievers.
Josephine Mukwendi, Finance & Administrative Manager
"Seeing others at a disadvantage makes my heart bleed, and knowing that I have helped them gives me strength every day."
With a Chartered Institute of Secretaries and Administration (CIS) qualification, an MBA and more than 20 years of experience, Josephine is instrumental in executing Open Africa’s high governance and financial controls. Previous experience includes stints as financial manager at Maitland’s Zimbabwe and reconciliation manager at Trust Bank. Josephine is quite a woman. Having lost both parents as an infant, she has always been inclined to help people through hardship, dedicating her life to supporting others – including her own three children and her deceased brother’s five children. Josephine enjoys playing with her four-year-old grandson, who lives with her, and going to church. She considers Open Africa her second home.
Maryna Viljoen, Website Administrator
"John Wooden says that "You can''t have a perfect day without doing something for someone who''ll never be able to repay you".  This how Open Africa inspires me, and why I love working here. Open Africa identifies a need to develop tourism in Africa and it allows me to see the great and positive effects it has on people everywhere. And I just love working with the participants!"
Maryna grew up in the small town of Napier, near the Southernmost tip of Africa. She moved to Fish Hoek in 2006, and came on board with Open Africa doing administration and managing the website. She loves living near the sea and one of her favourite things is to stroll through craft markets. She shares a dream with many Open Africa staff to travel and explore more of Africa. She has a heart for helping others and a real passion for animals, her two labrador retrievers being part of her family.
Genevieve Maasdorp
"I was drawn to Open Africa because of their attention to detail and the unique and individual approach to each tourism product. I am a keen traveller with strong community roots and I love to work on projects that have a real impact on rural areas and allow the hidden gems to be showcased."
A well trained tourism practitioner, with more than 10 years’ experience in tourism development, Genevieve joined the Open Africa team as a project manager. She has experience with tourism infrastructure development, tourism routes and SMME support. Genevieve loves the diversity of culture, the wide open spaces and regards it as an adventure to live on the African continent.  She loves to explore the hidden gems of the Northern Cape with her family.
Vasti Hannie
"I''ve always been a traveller; always moving and never homesick. There are many benefits to travel, too many to go into, and I believe it should be accessible to everyone. Open Africa has a unique approach to tourism, and I was attracted to their inclusive attitude and development focus."
With 15 years of experience working in Youth Development, Vasti pours her passion and energy into building the South Africa she wants. She has a Bachelor of Social Science degree, with majors in English Literature and Social Anthropology, from the University of Cape Town. She started at Open Africa in 2017 remains excited about the opportunity to work in rural areas of Africa. Vasti has worked with a number of NGOs in the Cape Town area including SHAWCO, Catholic Welfare and Development and the Edmund Rice Camps in the Western Cape. She has lived in New Zealand; taught English in South Korea and currently resides in Johannesburg with her husband. She is interested in using the latest online tools to organise and present information in a creative way for the benefit of small businesses.
 
', '2018-12-07 19:34:14');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13551, 'RSS feed
Globally there is a critical shortage of healthcare workers although the need is unequally distributed.
Every year 3.2 million babies are stillborn and up to 40 percent of newborns die in the first 2 days after birth which are linked to complications during birth.
Only 10 percent of the world''s health research focuses on the health problems of the developing world.
Over 80 percent of infant deaths are due to an acute illness including children with pneumonia or newborn complications including preterm birth or asphyxia.
Of the 7.6 million childhood deaths that occur annually, 99 percent are in the developing world.
', '2018-12-07 19:34:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13551, '"Those who need care the most are the least likely to receive it”
(The Inverse Care Law – Tudor-Hart 1971).
Of the 7.6 million childhood deaths that occur annually, 99% are in the developing world.
3.1 million children die in the first month of life 1.
Every year 3.2 million babies are stillborn and up to 40% of newborns die in the first 2 days after birth which are linked to complications during birth 2.
Many complications during birth are preventable with safe birth practices. A critical measure to identify which babies are at risk is the fetal heart rate during labour.
Over 80% of infant deaths are due to an acute illness including children with pneumonia or newborn complications including preterm birth or asphyxia 3 , many of whom are hypoxic and should receive oxygen therapy.
Globally there is a critical shortage of healthcare workers although the need is unequally distributed. A healthcare worker in Guinea is expected to meet the needs of 7,143 people. In contrast, a healthcare worker in Norway will be responsible for only 53 people4 .
Sub-Saharan Africa has only 10% of the healthcare workers for its population that Europe has.
Only 10% of the world’s health research focuses on the health problems of the developing world, even though those countries are burdened with 90% of the world’s disease.
While health systems, healthcare workers, infrastructure and supply of drugs receive much attention, only recently has there has been recognition of a critical gap in need-driven technology and equipment in providing quality care.
United Nations Inter-agency Group for Child Mortality Estimation. 2011.Levels & Trends in Child Mortality. United Nations Children Fund, New York.
Lawn,J.E., Lee, A.C.C., Kinney, M. 2009. Two million intrapartum-related stillbirths and neonatal deaths: Where, why, and what can be done? International Journal of Gynecology and Obstetrics; 107: S5-S19
Black, R.E., Cousens, S., Johnson, H.L., et al. 2010. Global, regional, and national causes of child mortality in 2008: a systematic analysis.
Watt, P., Brekci, N., Brearley, L. and Rawe, K. 2011. No child out of reach, time to end the health worker crisis. Save the Children UK, London. 
Problems with medical equipment in developing countries
Most hospitals in developing nations have a “graveyard” of unused medical technology because they are too complex and require specialized training, or are not working because of lack of maintenance or replacement parts.
Development of medical devices is driven towards increasingly complex, consumable, expensive equipment, targeted towards a global market where 78% of spending is for countries that carry only 2% of the global burden of disease.
There is consistent evidence that much of the medical equipment which is currently employed in primary and secondary health care settings in poor countries is inappropriate and unfit for purpose.
Specific problems with existing medical technology include:
Too fragile – unable to cope with robust user behaviour, high temperatures, humidity, dust, etc.
Too complex – most health professionals have no training in the use of advanced medical technology
Too sophisticated – inappropriate design specifications for low resource settings
Dependent on uninterrupted mains power – even large hospitals may have frequent power outages and small facilities may have no mains power
Dependent on consumables such as probes, sensors, replacement batteries etc. which are expensive and unavailable in many low resource countries
No reliable support services for maintenance, repair, recalibration and updating
Too expensive
The net effect is that there is a gross shortage of working and suitable medical devices across hospital and clinic services in most poor countries.
Background
PET advocates and stimulates the development of appropriate, low-cost, robust, and power-independent medical devices, together with learning materials, to aid life-saving decisions and help frontline healthcare workers meet the challenges of health care in under-resourced settings.
', '2018-12-07 19:34:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13551, 'Our People
Professor John Wyatt (BSc, MBBS, FRCP, FRCPCH, DCH)
John Wyatt is Professor of Ethics & Perinatology at UCL. He has a clinical background as an academic neonatologist working on the mechanisms, consequences and prevention of brain injury in critically ill newborn infants. His work is now concentrated on ethical issues raised by advances in reproductive and medical technology at the beginning of life, research ethics and governance and the philosophical basis of medical practice. He is lead for the Clinical Ethics network at UCLH, a member of the ethics committees of the Royal College of Physicians and the Royal College of Paediatrics and Child Health, and a board member of Biocentre.
Dr Joy Lawn (B MedSci, MBBS, MRCP (Paeds), MPH, PhD)
Joy Lawn is an African-born paediatrician and perinatal epidemiologist. She has 20 years newborn health experience especially in Africa, including 4 years as a lecturer and clinician in Ghana. Following roles at the WHO Collaborating Center, CDC Atlanta, USA, and the Institute of Child Health, London, UK she is now Director Global Evidence and Policy with the Gates-funded Saving Newborn Lives program of Save the Children-US. She co-leads the Child Health Epidemiology Reference Group’s Neonatal Group which developed the first cause-of-death estimates for 4 million neonatal deaths each year, published in The Lancet Neonatal series and WHO WHR 2005. She works with governments and partners to integrate, scale up and evaluate newborn care, particularly in Africa, including a network of 6 large-scale effectiveness trials. She co-led a 60 author team from 14 organizations for the book “Opportunities for Africa’s Newborns” and then 7 country teams for African Science Development Initiative’s report “Science in Action – Saving the lives of Africa’s mothers, newborns and children”.
Professor David Woods (MBChB, MD, FRCP, DCH)
Dave Woods is an Emeritus Associate Professor of Neonatal Medicine at the School of Child and Adolescent Health, University of Cape Town. During a 35 year career of caring for newborn infants at Groote Schuur Hospital in Cape Town, he actively engaged in the training of nurses and doctors working with mothers and infants in rural hospitals and clinics. He is chairman of the Perinatal Education Trust and Eduhealthcare, both organisations which develop and promote distance learning for health care workers in the fields of maternal, infant, child, HIV and TB care. He has a particular interest in developing appropriate medical technology to supplement self-help courses in primary and secondary level health care.
Dr Francois Bonnici (MBChB, MSc, DLSHTM, MBA, MA)
Dr François Bonnici is currently at the Department of Paediatrics at the University of Cape Town. He is also a Senior Advisor to the Schwab Foundation for Social Entrepreneurship and a Global Leadership Fellow Alumnus of the World Economic Forum in Switzerland, where he was the Associate Director of the Global Health Initiative. Originally trained as a physician in South Africa, he also read for a Masters degree in Epidemiology and Public Health (London) and an MBA (Oxford) as a Rhodes Scholar. He has worked in clinical paediatrics, managed public health programs, worked as a consultant to UNICEF, foundations, country governments, and social enterprises, and as a researcher in the Clinical Trials Unit at Oxford University. He also provides strategic advice to non-profits and social enterprises and investment advice to social venture funds and foundations in on their blended value investments on the African continent.
Mrs Anneke Jagau (BA Midwifery)
Mrs Anneke Jagau is PET’s research midwife.  She qualified with a bachelor in midwifery from Leuven University College, Belgium.  She has worked in midwifery units in Uganda and Kenya and has trained health care workers in both of these countries.  At PET Anneke is involved with developing, setting up and conducting research with a specific focus on the quality of intrapartum care and fetal heart rate monitoring.
Mrs Sarah Crede (BSc Physiotherapy, MPH)
Mrs Sarah Crede has recently joined PET as the organization’s research manager.  Originally trained as a physiotherapist, she also read for a Masters degree in Epidemiology and Public Health (University of Cape Town).  Sarah has worked in clinical settings in both South Africa and Europe and more recently in academic public health with a focus on women’s health.  Her role at PET is to manage the research outputs of PET and to co-ordinate PET activities.
Background
PET advocates and stimulates the development of appropriate, low-cost, robust, and power-independent medical devices, together with learning materials, to aid life-saving decisions and help frontline healthcare workers meet the challenges of health care in under-resourced settings.
', '2018-12-07 19:34:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13551, 'About us
Background
PET,  based in South Africa, was established as a not-for-profit organization by key experts in newborn and child health (see the Our People) .
PET aims to develop and make fit-for-purpose essential life-saving medical technologies accessible to impact newborn, child, and maternal survival in high-burden countries.
The long-term goal is to develop a range of essential medical devices and to establish distribution and support partnerships to ensure that they are available and affordable for frontline healthcare workers in low- and middle-income countries, especially sub-Saharan Africa.
Through the generous support of the Sir Halley Stewart Trust in the UK, PET was able to establish a partnership with Freeplay Energy, a commercial company with world-leading expertise in human-powered technology.
Promise
PET advocates and stimulates the development of appropriate, low-cost, robust, and power-independent medical devices, together with learning materials, to aid life-saving decisions and help frontline healthcare workers meet the challenges of health care in under-resourced settings.
We aim to fill the gaps in health care delivery with innovative technologies that assist in providing critical care, particularly in low resource settings, thereby impacting newborn, child, and maternal survival in high-burden countries.
Our goal is to develop a range of essential medical devices and ensure that they are available and affordable for frontline workers in low- and middle-income countries, especially sub-Saharan Africa.  We are therefore establishing and seeking new partnerships to collaborate particularly on manufacturing support and distribution.
Background
PET advocates and stimulates the development of appropriate, low-cost, robust, and power-independent medical devices, together with learning materials, to aid life-saving decisions and help frontline healthcare workers meet the challenges of health care in under-resourced settings.
', '2018-12-07 19:34:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13554, 'You are here: Home › Contact Us
Contact Us
Should you have any queries of wish to know more about Shonaquip, please complete your details below and we will be in touch soon.
Alternately if you are interested in one of our devices or products - to ensure the device is the best match for the user’s body, environment and budget, we recommend that an assessment is done by a qualified therapist before the purchase is made. The therapist can then advise you about all the different options available and or customization that are required.
So please fill in your details and a therapist will call you back promptly.
Send Us Your Details:
Yes, I would like to book an assessment
Please let us know what city you are in so we can better assist you:
Message:
Please leave this field empty.
FOLLOW US:
Medical Aid and financial queries:
Shonaquip Tender queries:
45 Lester Road, Wynberg, Cape Town, South Africa
Tel: +27 (0)21 797 8239
Alternate Tel (Telkom line problems): +27 (0)76 838 2107
Email:
45 Lester Road, Wynberg, Cape Town, South Africa
Tel: +27 (0)21 797 8239
Alternate Tel (Telkom line problems): +27 (0)76 838 2107
Email:
For queries, contact the Cape Town office.
Contact person is Zoe Brain
Tel: +27 (0)21 797 8239
Email:
Gauteng Clinical Team @Baby Therapy Centre, Pretoria:
347 Elizabeth Grove, Lynnwood, Pretoria.
For queries and appointments contact Zoe Brain based in Cape Town: Tel: +27 (0)21 797 8239
Alternate Tel (Telkom line problems): +27 (0)76 838 2107
Appointments are held on Monday, Tuesday and Wednesday afternoons from 13h00 to 16h00.
Gauteng Clinical Team @Malamulele Onward, Johannesburg:
Children’s Memorial Institute, Gate 10, 13 Joubert Street Ext, Braamfontein.
For queries and appointments contact Zoe Brain based in Cape Town: Tel: +27(0)21 797 8239
Alternate Tel (Telkom line problems): +27 (0)76 838 2107
Botswana Clinical Team:
Tel: +267 71 776 6454
Email:
', '2018-12-07 19:34:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13556, 'Tel: +27 21 531 2134
Johannesburg
50 Tsessebe Crescent, Corporate Park South,
Randjespark, Midrand
Tel: +27 11 314 0306
Paarl
Cnr Jan van Riebeeck Rd
& Driebergen Str, Paarl
Tel: +27 21 868 0036
Durban
', '2018-12-07 19:34:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13558, 'CID’s – We work closely with the various CID’s who all have field workers. Some also have social workers
Local Clinics and police
How can I help a homeless person and know that I am making things better?
The first thing to keep in mind is: a homeless person is a person. They need love, acceptance and human connections as much as any other individual.
Once you have established a connection, the best way to help a homeless person is to make it possible for him/her to access organisations like ours or the ones mentioned in the above FAQ.
Our organisations are equipped with professional support services for long-term, phased interactions. If you live in the U-turn Service Center catchment area, ie the Southern Suburbs, you can support individuals living on the street with a U-turn voucher , which provides access to food and clothing, and most importantly access to our support services.
If the person you want to assist does not stay in the U-turn Service Center catchment area, you can support the individual by buying him/her a weekly train ticket to access U-turn services. You’re welcome to contact our Service Center to check on the individual’s attendance and progress .
Should I give money or not?
We have all experienced the heartache of giving assistance to a person only to realize later that our best efforts actually just made things worse. Question is, how do you help in ways that will actually make a lasting difference? In trying to answer this we have really appreciated a model that was given in a book called  “When helping hurts”.
The model teaches us that there are three different types of help that we can give ( relief, rehab and development ) and these have very different characteristics and intended outcomes. We know that if we get it wrong we often do more harm than good. We are also very aware that to get it right is difficult yet to do nothing is inexcusable. Welcome to our world!
Having grappled with this since 2006, we are very happy to share some insights that we have picked up along the way so feel free to invite us (+27 21 674 6119) to interact with your small group / community group / work colleagues about good ways to help the poor or if you have the chance, read the book.
Do homeless people have to pay for sheltered accomodation?
Yes, however, they are not charged on a night-by-night basis.
For example, to access shelter at the Haven Night Shelter (the largest network of shelters in the Western Cape), a person is screened and assessed. If successful, the person can then stay for free for up to 10 days, after which they pay R84 per week if they are unemployed or R280 per week if employed or receiving a grant.*
Sheltered accommodation is a temporary solutions, and so individuals are required to find alternative accommodation within 3-6 months. Individuals are also generally not allowed to make use of this solution more than once.
While the Havens are the largest network of shelters in the Western Cape, there are also other shelters that work according to their own intake criteria and support structure. Most of them require some form of payment but the fee structures differ.
We would suggest that if you do want to support any individual with the cost of shelter, that it would be best to make payment to the shelter directly via EFT rather than giving the individual cash in hand, as giving money to a person on the street can fuel addiction and dependency. You are able to make regular contact with the shelter to ensure that the person still lives there.
*Please note: these amounts may vary. For accurate rates and in-take procedures, please contact The Haven or relevant shelter directly.
', '2018-12-07 19:34:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13559, 'Contact Us SuccinctNate 2016-04-06T11:37:20+00:00
Contact Us
', '2018-12-07 19:34:15');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13573, '  
How to support us
The New Hermopolis project has, until now, been financed by its founder’s own resources. The project aims                 in the future to become a self-sufficient and self-sustainable social enterprise. However, the project                 is still in its infancy and your support is certainly be needed. In fact, your contribution will help us                 make this a model for development in the future in a country that heavily depends on tourism for much of                 its income, proving that it is possible for entrepreneurs to have a business but still live up to the                 ideals of caring for heritage, environment and people. Any donation large or small will go a long way                 towards achieving this goal.
For a one time donation of of $50 you can have an olive tree named after you or someone you love, and in                 this way, your legacy will grow along with the project. For $500 donation you can have a Sycamore or                 Palm tree instead.
If you are in the position to donate $1000 your name will be engraved on the wall of cartouches in our                 reception room, where you will be able to inspire other visitors and imbue the village with the spirit                 of generosity.
At the core of the vision of The New Hermopolis project is the dream of it being a community, a place                 where people can come together and create something beautiful. If you would like to be a part of this                 enterprise and see it grow, not matter where you are, please consider donating or helping in any other way.
For your donations, you may use Pay Pal or use any of the following bank accounts.
The New Hermopolis - Egypt
', '2018-12-07 19:34:16');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13574, '@RachelMason247 @CommCatsRhys @chriswatson101 Big thanksAbout 13 hours ago
RT @CommCatsRhys : @RachelMason247 @CommCats @chriswatson101 Yes, yes it has!!  All very exciting..... and from experience I can say it is T…About 14 hours ago
@Ermintrude2 Thanks for reflecting and sharing!About 14 hours ago
RT @changepeople_ : Hi! Thanks so much for including us,it looks really interesting & useful.We’ve been working with partners across Europe…About 14 hours ago
Call us on:
', '2018-12-07 19:34:16');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13592, 'You are here: Home / Contact
Contact
This field is for validation purposes and should be left unchanged.

Sign up to receive email updates and to hear what''s going on with our organization.
First Name
', '2018-12-07 19:34:18');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13618, 'Network info
2018-08-16
9:00 - We are currently having a problem with our rental system for casual users as well as those using the BIXI app, Transit app or OPUS card. Members with a BIXI key are not affected. We will keep you posted.
15:23 - The network is now restored.
2018-08-15
- (Island / Centre) station is temporarily removed for work purpose.
-
- Milton / Durocher station reinstalled today.
2018-08-13
- Jeanne Mance / du Mont-Royal station is moved to the intersection of de l''Esplanade / du Mont-Royal.
2018-08-09
- Clark / Prince-Arthur station has been reinstated today.
- Alexandra / Waverly station has been reinstated today.
- Milton / du Parc station has been reinstated today.
2018-08-08
- Wurtele / de Rouen station is temporarily removed for work purpose.
2018-07-17
- Métro Beaubien (de Chateaubriand / Beaubien) station reinstated today.
2018-07-16
- Casgrain / Mozart has been removed.
-
- (Alexandra / Waverly) sation has been removed.
2018-07-13
- de Bleury / Mayor station has been reinstated today.
2018-07-12
- Cartier / Marie-Anne station is temporarily removed for work purpose.
2018-07-11
- Quesnel / Vinet station reinstated today.
- Ste-Catherine / Dézéry station is moved to the intersection of Darling / Ste-Catherine
2018-07-09
- Roy / St-Hubert station has been removed.
2018-07-05
- de Chateaubriand / Beaubien station is temporarily removed for work purpose.
2018-07-03
- Métro Berri-UQAM (St-Denis / de Maisonneuve) station has been removed.
-
- Ste-Catherine / St-Denis station has been removed.
-
- Tupper / du Fort station was installed today.
2018-06-29
- Due to the Canada day event, several stations must be temporarily removed: de la Cathédrale / René-Lévesque, Ste-Catherine / St-Marc, Tupper / du Fort.
2018-06-28
- Métro Place-des-Arts 2 (de Maisonneuve / de Bleury) station is temporarily removed for work purpose.
- Marquette / des Carrières was installed today.
2018-06-27
- Duke / Brennan station has been removed.
-
- Hochelaga / Chapleau station was installed today.
2018-06-26
- Cartier / St-Joseph has been removed.
-
- du President-Kennedy / Robert Bourassa station was installed.
-
- des Érables / Rachel station has been removed.
-
- St-Charles / St-Sylvestre station was installed today.
2018-06-22
- Cartier / Masson is temporarily removed for work purpose
- Parc Plage station reinstated today.
2018-06-21
- Gauthier / Parthenais station has been removed.
2018-06-20
- Roy / St-Hubert station has been removed.
2018-06-15
- du President-Kennedy / Robert Bourassa station is temporarily removed for work purpose
2018-06-13
- Peel / avenue des Canadiens de Montréal was installed.
2018-06-12
- Cypress / Peel station was installed.
-
- St-Alexandre / Carmichael station has been removed.
-
- Métro Lionel-Groulx (Atwater / Lionel-Groulx) station was installed.
2018-06-11
- Metcalfe / de Maisonneuve station was installed.
-
- Quesnel / Vinet station has been removed.
2018-06-08
- Hochelaga / Chapleau station is temporarily removed for work purpose.
2018-06-07
- Metcalfe / de Maisonneuve station is temporarily removed for work purpose.
- 9e avenue / Dandurand station is temporarily removed for work purpose.
- Métro Frontenac (Ontario / du Havre) station has been removed.
2018-06-05
- Parthenais / Laurier  station was installed.
2018-06-04
- Basin / Richmond station has been removed.
-
- Notre-Dame / Chatham station has been removed.
-
- Cypress / Peel station has been removed.
2018-05-31
- Jeanne-Mance / St-Viateur station reinstated today.
- de Marseille / Viau station is moved to the Centre Pierre-Charbonneau
- Louis-Colin / Willowdale station is moved to the intersection of Louis-Colin / McKenna
2018-05-30
- de l''Église / Bannantyne station was installed today.
-
- de l''Église / de Verdun station was installed today.
2018-05-29
- Peel / avenue des Canadiens de Montréal station has been removed.
-
- Alexandra / Jean-Talon station has been removed.
2018-05-28
- de Bleury / Mayor station is moved to the intersection of Maisonneuve /de Bleury.
-
- de Chateaubriand / Beaubien station reinstated today.
2018-05-24
- Metcalfe / du Square Dorchester station is moved to the intersection of de la Cathédrale / René-Lévesque
- Clark / St-Cuthbert station is moved to the intersection St-Cuthbert / St-Urbain
2018-05-23
- Parthenais / Laurier station has been removed.
2018-05-22
- Duke / Brennan station was installed today.
-
- d''Outremont / Ogilvy station was installed today.
-
- Henri-Julien / du Carmel station was installed today.
2018-05-18
- Metcalfe / Ste-Catherine station is moved to the intersection of Mansfield / René-Lévesque
- Pierre-de-Coubertin / Aird station is moved to the intersection of Pierre-de-Coubertin / Sicard
2018-05-17
- Métro Université de Montréal station reinstated today.
- Jean-Brillant / McKenna station reinstated today.
2018-05-16
- St-Alexandre / Ste-Catherine (6081) station was installed today.
-
- St-Marc / Sherbrooke station was installed today.
- Ontario / Clark station is moved to the intersection of Ste-Catherine / Clark
2018-05-15
- Station Fullum / Sherbrooke station was installed today.
2018-05-14
- Jeanne-Mance / St-Viateur station has been removed.
-
- Laurier / 15e avenue station is temporarily closed for work purpose.
-
- St-Antoine / St-François-Xavier station installed today.
-
- St-Nicolas / Place d''Youville station installed today.
-
- Wurtele / Ontario station is moved to the intersection of Wurtele / de Rouen
2018-05-11
- Boyer / Beaubien station reinstated today
- Métro Lucien-L''Allier (Lucien l''Allier / Argyle) station has been removed.
- Mackay / René-Lévesque station has been removed.
2018-05-10
- St-Dominique / Gounod station reinstated today.
- St-Antoine / St-François-Xavier station is temporarily removed for work purpose.
2018-05-09
- Métro Berri-UQAM (St-Denis / de Maisonneuve) station reinstalled today.
-
- St-Urbain / Beaubien station is temporarily closed for work purpose.
- Eadie / Dubois station reinstated today.
- Square Sir-Georges-Étienne-Cartier / St-Ambroise station reinstated today.
2018-05-08
- Jogues / Allard station installed today
-
- de Chateaubriand / Beaubien station has been removed.
-
- Atwater / Ste-Catherine station installed today.
2018-05-07
', '2018-12-07 19:34:19');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13689, 'Contact
Contact
For more information or questions on our guidelines, international education and international collaboration, please contact us by e-mail:
', '2018-12-07 19:34:21');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13696, 'Don''t have an account yet? Register Now!
×
Sign in to your account
Username *
', '2018-12-07 19:34:21');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13696, 'About us
The Foundation Forming Futures (FFF) is a non-profit organization based in the city of Cali, Colombia.
It is registered in the chamber of commerce in Cali under the register n° 708 of Book I, with TIN 805.026.664-3 and was established in 2003. The FFF works with youngsters leaving the public protection programme, or teenagers that are still in protection and highly vulnerable.
During our 14 years of experience we have been accompanying the transition process of young people that didn’t have the opportunity to grow up in a stable family environment from the protection programme to an autonomous adult life. It especially dedicates to teenagers and young adults of the ICBF system in the region of the Cauca Vally and is constituted in accordance with national and international standards. The foundation uses different methods in their work with institutionalized teenagers and young adults and regularly publishes material, such as videos, reports and other publications.
History
Through the voluntary work in a protection center in Cali in 1998 our founder Tanya Manuell learned about the problems of young adults leaving the public protection system. These youngsters all grow up in conditions which request high adaptability and hardly ever enjoy a safe family situation.
That motivated Tanya to develop a pedagogic model that was implemented for the first time on a group of young men leaving the ICBF protection. None of them had parental networks or a profile that guaranteed their auto-sustainability, respectively their socio-occupational inclusion.
On the basis of this successful trial, the constitution of the foundation was created in 2003.
The youngsters benefit a lot from the opportunity the foundation is giving them. They finish their studies, they prepare for the work life and they are encouraged to build a professional network. Furthermore we stay in contact with our graduates, pointing out their effort and improvement of their present life quality.
140 youngsters already completed the residential programme and another 1.500 benefited from the non-residential programme in our reference point.
Mission
The mission of the foundation is to support and orientate youngsters that are still in public protection or leaving the protection system, as well as other vulnerable adolescents, in order to encourage their potential and to prepare them for an autonomous life.
Vision
The vision of the foundation is to develop visibility in the social context of Colombia as a generator of social programmes for young people and as an expert in the field of institutionalized protected youngsters.
RECONOCIMIENTOS
2009 FFF represented Colombia in the global Youth Summit on invitation of the World Bank.
2010 FFF was elected as the best Project of Terpel CIS Occidente on a Latin-American level.
2014 FFF wins price at Valor Social de CEPSA.
2009 to 2011 Project of social responsibility of the organization Terpel CIS Occidente.
© 2016 Fundación Formación d´Futuros. All Rights Reserved. Designed By ApelTM
', '2018-12-07 19:34:21');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13711, 'ANBI
Contact
Do you have a question about our work, want to get involved or just want to say hello? Drop us a line and #joinouralliance.
', '2018-12-07 19:34:22');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13727, 'More
About us
Social Cooperative Humana Nova Čakovec encourages the employment of the disabled and other socially excluded persons through the production and selling of quality and innovative textile products made from ecological and recycled fabrics for the needs of the domestic and the foreign markets. Our products are the response to the actual needs of users. Each fiber of a product embeds the satisfaction and benefit of workers, cooperatives, Nature and community. In this way our Cooperative actively contributes to the sustainable development of the local community, diminishing of poverty and nature conservation.
Social Cooperative Humana Nova Čakovec is one in a series of the social enterprises that Autonomous Centre – ACT has been running and developing in the area of the County of Međimurje.
Vision
Social Cooperative Humana Nova Čakovec is a leader of the social entrepreneurship and has been recognized in the wider regional scope. The work of the cooperative has actively and directly contributed to the establishing of a tolerant society and has in turn helped the socially excluded persons and their families in enhancing their self-reliability and the quality of life.
Values
Togetherness, cooperation and mutuality / Quality and professional quality/ Leadership/Courage/ Sustainability (economical, social, ecological)/ Acquisition of new skills /Pleasant and motivating workplace
Why social cooperative? In line with the Croatian Act on Cooperatives, a cooperative is an economy subject in which, for the purpose of the own development, an individual entrepreneur associate with other individuals or legal entities and they thus contribute to their own and to their mutual development. The associating of the individuals with the same or similar visions in order to achieve a better-thought marketing of products in the more and more demanding market is one of the basic leading ideas in the Cooperative. The specific nature of Humana Nova reflects in the fact that its members are associations and individuals that have recognized the value of the incentives and potentials of the process of engaging the disabled and other socially excluded persons into the production of the quality and widely demanded products. The members are ready – by means of their contribution, work and financial investments- to help the cooperative to be started, to start working and to develop; they thus help the local community as well.
Why social enterprise? A social enterprise is a company that is owned by its employees and/or local community members, a company that is lead by both social and commercial objectives and that is managed commonly and on the democratic principles. (Freer Spreckley, 1981.)
The direct influence of the operation of the social companies can be seen in the economic development of a local community or a region by means of the following:
By means of their operations, they supplement the public interest services (for example, social services) that cannot be performed to the satisfactory level of quality by the public institutions or privately-owned profit companies;
By means of a contribution to a balanced use and allocation of the resources at disposal in favour of the local community;
By means of generating new jobs in their realms of operating; some social companies are, in turn, specially oriented towards the integration of the people who have been out of work for a long time in the labour market;
By means of encouraging the social cohesion and contribute to the growth and development of the social capital;
By means of providing support to the institutionalizing of the informal entrepreneurial activities within the private profit sector etc.
Newsletter
Enter email
', '2018-12-07 19:34:22');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13750, '1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
Sharing the Lessons Learned
One of our aims is to share with the wider community the lessons learned during the first fifteen years of creating and managing Ireland’s first ecovillage.
The Coach House
Located at the pedestrian entrance to Cloughjordan Ecovillage, off Main Street.
Cloughjordan Ecovillage
Pa Finucane welcomes all overnight visitors to his comfortable inexpensive accommodaIon
Cloughjordan Ecovillage
Eco Houses
With good design and great insulaIon, it’s no surprise that we have 6% of Ireland’s A-­rated homes -­‐ and almost all the rest are rated at B1.
Cloughjordan Ecovillage
Walk our seven circle Celtic labyrinth – and reflect on the newly planted woodland around you.
Cloughjordan Ecovillage
The Cloughjordan Community Farm is operated on a Community Supported Agriculture basis
Cloughjordan Ecovillage
Hedgerows
Our boundary hedges are a rich natural habitat, which is home to thousands of plant and animal species.
Cloughjordan Ecovillage
What our visitors say about us
“It has been a privilege to be in this learning community. I’m excited and inspired by what you’re creating.”
Deborah F. Boston Mass. USAhttp://www.wikipedia.at – Maxmanc Inc
“Thank you for your time. I have been wanting to visit for some time & haven’t been disappointed.”
Julie H.Cork, Irelandhttp://www.wikipedia.at – FunkyInc
“Incredible project! Great progress in the last three years. A place to be – cool.”
Charlie A.Victoria, BC Canadahttp://www.wikipedia.at – Zax
“I loved how the village is connected to the old town.”
Diarmuid E.Limerick, Irelandhttp://www.wikipedia.at – O.S
“Impressive undertaking given the size of the estate. The district heating idea is very innovative.”
Matt R. Ireland
“A very interesting & innovative complex looking to the future. A great model for future development of communities.”
Phil L.Western Australia
“You are an inspiring and very creative bunch of settlers. Good luck for your future!”
G.H.Ecovillage of Sieben Linden, Germany
“Great to see such variety of building techniques in progress & amazing progress since last visit.”
Gary DIreland
Registered charity  - number 20041182
Tags
', '2018-12-07 19:34:24');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13765, 'By Sharri Xhiha on July 2, 2013 in News
A business plan is a formal document based on a business idea, which describes in details the nature of the business and the direction it is aiming for. The business plan outlines the goals of the future business, the reasons why these goals are considered achievable and the methods to attain these objectives. The business plan is the instrument used to translate promising ideas and hypothesis into specific business concepts, which are then broken down into clear operational actions. It may also be considered as the first test that challenges the plausibility and feasibility of the business idea.
Why write a business plan?
Professional investors, strategic partners and, not least, potential customers only support projects that are founded on a sound business plan.
Benefits of the business plan:
Forces the company founders to systematically think through their business idea.
Shows knowledge gaps and helps to fill these efficiently and in a structured way.
Servers as the central means of communication between the various partners.
Gives an overview of the resources required and reveals holes in the plan.
Is the rehearsal for the real thing: and it doesn’t cost anything if a potential crash-landing is identified during the business planning, whereas, later, the consequences for the entrepreneur, the investors and the employees could be serious.
How is a business plan structured?
Executive summary : brief, concise presentation of the business..
Product Idea : An easily understandable description of the product or service, including customer benefit, sales argument. Its objective is to convince the reader of the benefit of the product or service.
Company team: : a description of the individual team members, their skills and experience with regard to founding a company and building up the business. Usually, the team is the most critical success factor. Therefore, investors will read this section of the business plan with particular interest and care.
Marketing (sales organization): statements about the size of the market, the customer target segment as well as a description of the product or service’s price and advertising strategy and details of sales and distribution.
Business system and organization : description of the activities of the company, the production concept (e.g., own manufacturing vs. outsourcing) and the organization.
Realization schedule : the current status of the implementation and realization milestones.
Risks: an overview of risks and financial consequences shown in various scenarios.
Financing: calculation of the investment requirement, return on capital expectations for potential investors, balance sheet, (P&L) account and cash flow calculation.
Sources:
', '2018-12-07 19:34:24');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13765, 'Return to Content
OUR MISSION
Encouraging Young Entrepreneurs is a private company established in March 2012 in Prishtina aiming to stimulate and support young entrepreneurs in Kosovo and Diaspora to build new successful companies. With our experience in managing growth companies and our financial resources we have created an ideal environment to support young entrepreneurs to exploit opportunities and build new successful companies, with a strong focus on social and environmental responsibility. We believe that in the mid-term we will help generate a reasonable number of successful companies with a substantial number of new jobs. This will be our contribution in mitigating the problem of high youth unemployment in Kosovo.
OUR STRATEGY
We will accomplish our mission, by organizing a yearly contest eye Venture , designed to promote young entrepreneurs with outstanding ideas, from Kosovo and the Diaspora, and assist them, with financial resources and expertise, through the business planning process and in the initiation of their business.
We will provide our candidates with:
·      Assistance in the translation of business concepts into real companies
·      Coaching in the creation of business plans
·       Tutoring and consultancy for management teams
·      Networking and cooperation opportunities with our partners
·      Start up capital, managerial expertise and further financial support for the new ventures
OUR TEAM
Vllaznim Xhiha
Chairman
Vllaznim Xhiha, has graduated from the Swiss Technology Institute (ETHZ) of Zurich in 1976, whereas in 1980 he accomplished his post-graduate studies at the same Institute in the field of electronics and automatic control.
Drini Xhiha
Project Coordinator – Diaspora
In 2009 Xhiha graduates from the University of Zurich obtaining a Master’s of Arts in Economics.
After finishing his studies he moved to Vienna where he currently lives. In 2010 he joined the Austrian Post and worked as
Key Account Manager in the International Sales
Team for two years.
', '2018-12-07 19:34:24');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13785, 'Thursday, 12 March 2015 17:09
Netherlands Embassy wrote about us
Rate this item
5
(0 votes)
On International Human Rights Day - 10 December 2013 -, a Human Rights Symbol was unveiled by Minister of Social Welfare and Youth, Mr Erion Veliaj,and Dutch Ambassador Martin de la Beij. The Embassy supported the creation
of a Human Rights Symbol bringing into art the universal logo of human rights. Carved with quotes of different groups voicing importance of human rights, the Symbol echoes respect of human rights every single day. The symbol is made from used wood and produced by marginalized people.
In respect of the commitment and daily contribution enhancing the human rights situation in Albania, the Symbol is hosted at the Ministry of Social Welfare and Youth.
Special gratitude to the involved human rights defenders: OMSA, Pink Embassy, Help for Children Foundation, Albanian Coalition for Child Education, Albanian Institute for Public Affairs, Counselling Line, Mjaft Movement and the producer Design by Pana, for making it happen.
Please read the full article  HERE  (English)
Download the full article here in PDF format:  DOWNLOAD
', '2018-12-07 19:34:24');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13788, 'Cleaning services are provided on regular contractual basis and also by orders.
YAPS Cleaning operators are in approximate 60
YAPS can ensure up to 5 teams of cleaners witch can work separately for the general cleaning services.
WASH THE WINDOWS at any height
We offer a perfect service for glass cleaning of offices or your home.
Washing is performed in different ways like!!!!?
Glass washing done with professional equipments such as scale, scaffolding, cranes etc.
Washing windows professionally done by tow, glass knife for windows and dust repellent.
Contact Us
Str. “Don Bosko”, in front of  Ferrari Hotel
069 20 93 689
04 240 58 67, 04 240 57 51
04 240 58 40
', '2018-12-07 19:34:24');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13855, 'About us
Who we are
Quartal is created by young talents with the idea of making a helping hand for our partners, that is focused on accounting, business development services and startup consulting, but also taking care about the needs of our partners in the way of putting their clients in focus.
To keep up a certain standard for business is a crucial point for us whether it comes to customer service, professional financial advice and other means of help that will make way for a prosperous progress.
Our co-workers have broad experience with accounting, law, finance, marketing and business development. We have put down countless hours on administrative work as well as work on the field. For this reason, we understand how demanding each project can be to begin, run and develop. That is why we have much respect for entrepreneurs that have made the choice of creating their own sailing ship and making way for their own economy!
 
', '2018-12-07 19:34:33');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13861, 'Everything you need to know about GDPR compliance and email security
Posted on
by Andy Yen
Encrypted email can help you comply with privacy laws, limit the risk of hacks and data breaches, and improve your company’s overall online security strategy.
New regulations always create compliance-induced headaches for companies. But in this case, the European Union’s General Data Protection Regulation (the GDPR) presents an enormous opportunity for businesses to improve their digital security.
Encryption is one of the data protection measures specifically recommended in the GDPR. Under the new law, organizations that suffer a data breach and have not taken appropriate measures to protect their users’ data can be hit with enormous fines. It is relatively simple, however, to mitigate your liability, and in this article we will explain how. Note, the recommendations here are not exhaustive and we also recommend speaking with an attorney to get more information.
A brief background on the GDPR
As of May 25, 2018, any organization that collects, stores, or uses the personal data of people in the EU must adhere to strict requirements that give individuals more control over their data. You can find the full text here . It’s worth reading over the documents with your lawyer to learn the many ways the GDPR could affect your business.
Broadly speaking, the GDPR aims to give people (“data subjects”) more control over who can access their personal information and how it is used. To accomplish this, data subjects now have certain protected rights. For example, they must be allowed to see what information about them is being stored, and they can ask to have it deleted. The GDPR also introduces a requirement known as “data portability,” which gives people the right to obtain their data in a standard format. This gets at one of the central ideas of the GDPR: personal data belong to the data subjects, not businesses. The GDPR requires “data controllers” (i.e. the organizations handling personal data) to set up procedures to honor these rights.
Data controllers also have new responsibilities to protect data more rigorously. No longer should data breaches compromise users’ online security and privacy. The GDPR compels data controllers to use additional security measures to render data files more harmless in the event of a data breach: pseudonymization, anonymization, or encryption. We’ll look closer at some of these concepts below.
This legislation has serious teeth. If you fail to adequately protect users and their data, it could cost you 4 percent of your global annual revenue or €20 million, whichever is higher. In determining the severity of the penalties, the authorities take into account what steps the data controller has taken, such as the use of encryption, to mitigate damage to data subjects.
The GDPR applies to everyone
Any organization that handles the personal data of EU residents or citizens must comply with the GDPR, including companies that are not based in the EU. Third-party services used by your organization must also be compliant. That includes your email provider. So, for example, if your company communicates with EU-based customers through email, then your email service provider, regardless of the location of its headquarters or servers, must comply with GDPR.
How to comply with the GDPR
It’s useful to think about approaching compliance in three broad steps:
Start by identifying the personal data in your organization’s possession. Understand where it is, how it is collected, and who has access.
Create new systems to manage these data. The GDPR requires data controllers to respond quickly to requests from data subjects, to identify breaches and report them within 72 hours, to limit data access within your organization, to establish a lawful basis for having the data, and to make privacy the default stance (e.g. you should not collect data you do not need, and data subjects must opt-in to collection), among many other requirements. This law will likely necessitate comprehensive new internal procedures and technical updates.
Finally, the GDPR requires data controllers to take active measures to protect the personal data they possess and to mitigate the potential damage in case of a breach. This includes data stored anywhere within your organization, including in emails.
 
GDPR Compliant Email
Encryption is a key data protection component of the GDPR. It is referred to as an example of an “appropriate measure” to keep personal data secure, it ensures “data protection by design” covered in Article 25, and it mitigates your liabilities in the event of a data breach under Article 34.
The encryption we use at ProtonMail satisfies these requirements while giving organizations total control over their data. Unlike other cloud email services, you can be sure that neither we nor anyone else can see the contents of your emails — even if there is a breach of our servers. We can make this guarantee thanks to our implementation of end-to-end encryption , which protects your organization’s internal email communications, and zero-access encryption , which protects all your external email communications.
Privacy regulations aside, encrypted email is a common-sense tool that more businesses and individuals are adopting to defend against cyber attacks and to keep sensitive information safe. By combining email encryption with a cloud hosted service, ProtonMail provides the best of both worlds. You can benefit from the reliability and cost savings of the cloud, while simultaneously maintaining control over your data. From the user’s perspective, ProtonMail works just like an unencrypted email service, with modern inbox design and secure mobile apps. There’s no learning curve because all the encryption takes place automatically behind the scenes.
It’s important to work with trustworthy and security-conscious service providers to limit your liability under the GDPR, and in this regard ProtonMail can help protect your organization and your customers. Now more than ever, customers want to know that you are taking the appropriate steps to protect their data, and encrypted email helps reduce the risk of being fined or worse: being in the headlines for a catastrophic data breach .
GDPR Data Processing Agreement
For organizations using ProtonMail to comply with GDPR, we provide a Data Processing Agreement that helps you comply with GDPR requirements. To properly comply with GDPR, you must also ensure any third parties (e.g. subcontractors, cloud services, etc.) handling your customers’ data are also compliant. To satisfy this obligation, you are expected to have in place a Data Processing Agreement with all services that may process customer data, in order to establish the rights and obligations of each party under the GDPR.
If you have additional questions about GDPR compliance and email security, please contact us .
Best Regards,
ProtonMail provides  free encrypted email accounts to the public.
We also provide a  free VPN service  to protect your privacy.
Share This!
', '2018-12-07 19:34:33');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13861, 'Everything You Need to Know About Buying Bitcoin
Posted on
by Irina M
Nowadays, almost everyone has heard of Bitcoin. In this article, we provide a step by step guide for buying and storing Bitcoins securely.
Editor’s Note: Since Bitcoin has surged to record prices recently (reaching $2700 on Thursday), we have had more and more users asking us about Bitcoin. To answer some of these questions, we’re publishing this guest post from a Bitcoin expert from our community.
Bitcoin prices have risen by a record amount in 2017.
What is Bitcoin?
Here are the main points that you need to understand before buying Bitcoin.
1) Bitcoin is a form of digital money, protected by cryptography, which makes bitcoin transactions secure. That is why it is called cryptocurrency.
2) Bitcoin exists only in a digital form so that it is never printed, which means that bitcoin is a form of digital information.
3) Bitcoin is decentralized. Meaning there is no specific place where bitcoins are released and even no central server that runs the system. Bitcoin infrastructure is distributed among all users, meaning their devices run the system. Thus, you do not buy Bitcoins from any central bank.
4) The amount of bitcoins is limited. You can make an analogy with gold: the amount is also limited. In other words, compared to other currencies like the US dollar or Swiss Franc, the Bitcoin supply is limited, and cannot be increased. Therefore, Bitcoin generally does not have inflation. Inelastic supply is one of the reasons that Bitcoin prices have tended to rise over time (but there are many other factors which play a role in determining Bitcoin price). $100 in Bitcoins purchased 5 years ago would today be worth millions.
Advantages and disadvantages of using Bitcoin
Advantages:
1) Decentralization, remember? This not only means that there is no central server, but also that there is no “Central Bank”.
Bitcoins are produced through the process called mining. In the bitcoin environment mining means verifying all the transactions that take place within the system. As a reward for transaction validation, system “gives” miners bitcoins. Bitcoins can be mined by anyone, having the right computer equipment and expertise. Because Bitcoins are digital, they are “mined” digitally using computers.
2) Lower Fees. There is no specific bank or service like, for example PayPal, that conducts and controls all the transactions. Generally, this means that Bitcoin transaction fees are much much lower. This also means that there is very little extra costs involved in obtaining Bitcoin other than the cost of buying the Bitcoins themselves.
3) Transactions can be conducted quickly, no matter whether you send bitcoins to your neighbor, or to a business partner that is on the other side of the world. Generally, the longest time it takes for a Bitcoin purchase to be verified is about an hour, instead of days for traditional money transfers.
 
Disadvantages:
1) Since Bitcoin is a completely new and non-governmental international currency, there is still a lot of uncertainty regarding the legal status of Bitcoin. While bank deposits are often insured and protected by law, this type of protection doesn’t exist for Bitcoins that you purchase.
2) Bitcoin is intangible, it cannot be touched or seen like gold. This is a reason why some people are wary about buying Bitcoin, as you can never “see” your purchase.
3) It has a high volatility. Throughout its history, bitcoin price has experienced both dramatic ups and downs. So no one knows for sure whether it will follow an upward trend or not. The volatility also makes it hard to conduct business with it since Bitcoin prices often vary 10% or more per day. In other words, buying bitcoins is not without risk.
 
How to buy Bitcoins
New Bitcoin buyers typically have three questions: How to purchase Bitcoins, how to store them, and how to spend them.
 
Bitcoin Wallets
If you want to purchase bitcoins, the first thing you need to do is to create a wallet. Bitcoin wallet is a special program, where users store their private data necessary to manage their bitcoins and make bitcoin transactions. There are different types of wallets, the major are: an offline wallet (a software that you download on your PC, that keeps your bitcoins), an online wallet (websites that allow you to store bitcoins), a hardware wallet (removable media). For the beginning, you can start with an online wallet, because it is the easiest to use.
Blockchain is one of the online wallets. It is time-tested and has been used by hundreds of thousands of people without major security issues. Moreover, it is perfect for beginners, because it is not overloaded with information and contains only the most important functionality. Xapo is another one and it’s gaining more and more popularity. They can even provide you with a debit card for wider usability of your Bitcoins. If you are storing large amounts if bitcoins however, a hardware wallet is recommended. It is also advisable to distribute your bitcoins over several wallets so you don’t have all your eggs in one basket.
If you go on the Blockchain website the registration process is very easy, you only need to write your name and confirm the email. From this very moment your wallet is done and you can use it, but it is highly recommended to complete the Security Center section (it will take you only 5 minutes), and make your wallet better protected. For extra security, you might want to consider using a ProtonMail encrypted email account to open your online wallet account.
In order to purchase bitcoins, you need to have an address – wallets provide you with one. An address is a randomly generated set of numbers and letters that you share with those you’re going to transact.
 
Exchanges
When you already obtained a bitcoin wallet the next step is to buy bitcoins. If you are a newcomer to Bitcoin, Bitcoin exchanges are the best option. International Bitcoin exchanges include Coinbase ,  CEX.IO , Bitstamp , Poloniex and Kraken . There are many exchanges out there so it is important to pick a reputable one. The process of buying bitcoins is easier than purchasing goods on the internet because you don’t need to provide a shipping address, just your bitcoin address.
 
Purchasing Bitcoins from an exchange involves three steps: registration, input your card data, write your digital wallet address (be careful, a typo could send your Bitcoin to the wrong address, the best way is to copy/paste your bitcoin address right from your wallet). Registering using a ProtonMail email address is also a good idea for additional security.
In addition to exchanges, there are many other options to purchase bitcoins. There are also some broker websites, providing a trading platform for people to exchange their bitcoins directly. ATMs is another option. Bitcoin ATMs are very similar to those maintained by banks and there are hundreds of them all over the world. You can also mine your own Bitcoin, but this is becoming more and more difficult for ordinary people to do.
 
How to use Bitcoin
Making purchases
When bitcoins first appeared in the market, you could only find private individuals who were willing to sell their stuff for bitcoins. For instance, at the dawn of bitcoin history, one bitcoin enthusiast paid 10,000 bitcoins for a couple of pizzas. But now everything has changed and more and more companies accept bitcoins. For instance, you can buy a Dell laptop using bitcoins or any Microsoft products as well. You can even pay for ProtonMail upgrades with bitcoin. There are some Bitcoin wallets that provide mobile apps which make it even easier to spend your Bitcoins.
The idea of bitcoin is clever and revolutionary. It’s a currency that is not influenced by anything except by supply and demand, and under the control of no government or organization. It belongs to no one specifically and could very well be the future of money. If this sounds exciting, now you know everything you need to become a Bitcoin user.
 
About the Author
Mary Ann Callahan is an UK based freelance journalist who specializes on Bitcoin-related topics. Currently, she writes for CEX.io , a multi-functional cryptocurrency exchange. She writes articles related to blockchain security, bitcoin purchase guides or bitcoin regulations in different countries. Previously, she worked for Boston Globe Media and holds a Master’s degree in Journalism from Columbia University.
 
Want to keep your Bitcoins safe? Then use an encrypted email account to open your Bitcoin related online accounts. You can get a free secure email account from ProtonMail here .
Learn why millions of people around the world have chosen to protect their communications with ProtonMail.
Share This!
', '2018-12-07 19:34:33');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13862, '                                                                                                        E.
Together for a Greener Future
B.E.S. AB was established in early 2017 in Stockholm with the main purpose of introducing, connecting and coordinating bio-energy producers in Europe and by-products base material suppliers around the globe.
​
As of today, Biotisk Energier Sweden AB successfully provided over 20,000 MT of sustainable products to the European energy market.
​
In September 2017, B.E.S. AB was awarded the ISCC-EU certificate and started trading sustainable products, providing feedstocks from the Middle East, Egypt and China.
Our network includes 12 global suppliers from China, Kuwait and Egypt holding the International Sustainability & Carbon System (ISCC-EU) certification, allowing us to provide the most secured terms and conditions in the market, along with the best service and quality.
​
A Way to Make an Impact
​
Our vision is to live in a a fossil-fuel free society.
​
Our mission is to contribute with positive environmental impact by being one of the top by-products providers for the European bio-energy industry.
Biotisk Energier Sweden AB
', '2018-12-07 19:34:34');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (13867, 'Note: Please fill out the fields marked with an asterisk.
Our collaborators
We believe that good stories are born from real life experiences. Wherever you are in the world, if you have a story to make children''s         life better, drop us a message, and we can guide you or even work together.
', '2018-12-07 19:34:34');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7639, 'VE Vision Education GmbH
Learn where your passion is
Vision Education is changing the way people feel about learning by focusing on motivation as the key to success and a joyful life long learning.
Education ist the new currency and our children are our hope and future. Our ambitious goal is to globally give access to learning and education to children (and adults) who normally would not have the opportunity.
Learning almost happens automatically when embedded in a playful and emotional context like football - this is, what our app "LearnMatch" does - building communities via education, gamification and sports. By working with international sponsors and NGOs, we want to create a meaningful connection between economy and social life.
What Is Impact Hub?
We believe a better world is created through the combined accomplishments of compassionate, creative, and committed individuals focused on a common purpose.
An innovation lab. A business Incubator. A social enterprise community centre. Impact Hub offers you a unique ecosystem of resources, inspiration, and collaboration opportunities to grow the positive impact of your work. Joining our diverse community of members and collaborators will inspire, connect, and enable you to develop your best work every step of the way.  From Amsterdam to Johannesburg, Singapore to San Francisco, Impact Hub has evolved into a rapidly expanding, diverse global network of over 15000 members in 80+ locations.
86
', '2018-12-07 19:34:35');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7639, 'Hub Ops
What is Impact Hub Phnom Penh?
Impact Hub Phnom Penh is a co-working space, a business incubator, a social enterprise builder and above all, a community of like-minded people who believe they can make the world a better place.
When we started 5 years ago, we found there wasn''t any meaningful support systems in place for entrepreneurs, innovators and organisations who want to have a positive impact in Cambodia. We met so many people doing incredible things, but with no common community uniting them. This is how our journey began, and since then we have created communities surrounding social causes, innovation, and entrepreneurship with hundreds of members.
One of the first communities we created was Social Enterprise Cambodia, a platform that connects social enterprises in Cambodia. Check out the website here .
Impact Hub Phnom Penh aligns itself and all its activities with the global Sustainable Development Goals, and we proudly display them in our space to inspire our community to do the same. Our community manifesto helps members to see the values we stand by and the kind of people you can expect to meet at Impact Hub Phnom Penh.
What are Impact Hubs?
Coworking spaces that support and develop social enterprises, based around the world. Acting locally, connected globally.
We are part of a global network of connected communities that enable collaborative ventures while supporting the passion to create sustainable impact. Impact Hubs unite people from every profession, background and culture with imagination and drive to pursue enterprising ideas for the world.  From Amsterdam to Johannesburg, Singapore to San Francisco, Impact Hub has evolved into a rapidly expanding, diverse global network. Each community is a wealth of innovative programs, events, and cutting -edge content. Step into any one of our Impact Hubs worldwide and immerse yourself in the local experience of a global movement.
Newsletter Subscription
', '2018-12-07 19:34:35');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7639, 'Hub Ops
What is Impact Hub Stockholm?
Impact Hub is the world''s largest network of social entrepreneurs. Our mission is to enable entrepreneurial action for positive impact. Through our core values of Trust, Courage and Collaboration; we engage in collective action with the vision of achieving a better world.
Impact Hub is a global movement. In 2015, over 2000 startups were founded by our members. We created 3,500+ new jobs and over 54 million people were impacted by our members last year.
Diversity is key for social innovation. But it''s not enough. It takes collaboration and community, and a space that fosters the right kind of environment. That''s what we provide at Impact Hub. Together we can create businesses with far-reaching positive social impact to make our world a better place, not just for us today, but for the future generations of tomorrow.
We invite you to join our global community of social entrepreneurs. For members of Impact Hub Stockholm, we have created a home away from home. A creative and central space at Sveavägen 44 where you can work alongside innovators and visionaries to expand your perspectives. Through the inspiring strength of our community, both in Stockholm and abroad, we can provide your business with the resources you need to succeed.   As a global network, Impact Hubs around the world unite people from every profession, background and culture. With imagination and drive, together we pursue enterprising ideas for a better world. At Impact Hub Stockholm, you will find the people with drive and passion to create new visions for the future.
Learn more
We believe a better world is created through the combined accomplishments of compassionate, creative, and committed individuals focused on a common purpose.
From Amsterdam to Johannesburg, Singapore to San Francisco, Impact Hub has evolved into a rapidly expanding, diverse global network of over 17,000 members in 100+ locations. Each community is a wealth of innovative programs, events, and cutting-edge content. Step into any one of our Impact Hubs worldwide and immerse yourself in the local experience of a global movement.  Part innovation lab, part business incubator, and part community centre; we offer you a unique ecosystem of resources, inspiration, and collaboration opportunities to grow your impact. Joining our diverse community of members and collaborators will inspire, connect, and enable you to develop your best work every step of the way.
', '2018-12-07 19:34:35');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (5442, 'CONTACT US
SOCRATIC PLATFORM
SOCRATIC will be a Knowledge-based Internet Platform offering a set of tools and Services to support the whole Social Innovation Project life cycle from problem identification and awareness and crative solution ideas to collective decision-making, design and implementation of the best ideas.
An integrated solution for the innovation process rather than separate items or services, enabling agile social innovation processes. Each activity will be supported by a specific service in the platform. SOCRATIC will also implement a Global Observatory about Sustainability challenges.
The main goal of SOCRATIC project is to provide citizens and organizations a collaborative space where they can identify innovative solutions to achieve the Sustainable Development Goals set by the United Nations.
SOCRATIC – SOcial CReATive Intelligence for achieving Global Sustainability Goals
The main objective of SOCRATIC is to facilitate a platform so citizens and/or organisations can collaboratively identify specific innovative solutions for achieving the desired Global Sustainability Goals, as defined by the United Nations. SOCRATIC will also implement a Global Observatory on Sustainability Challenges with a double objective: (1) Measuring the impact of SOCRATIC actions on Global Sustainability Challenges by monitoring social networks, (2) Using the data about Global Sustainability Challenges gathered in social media as a source of information to launch challenges in the SOCRATIC platform.
The SOCRATIC project has a deep user-centric approach, implementing gamification techniques to engage users in the sustained use of the platform. The project involves one European NGO (CiberVoluntarios) and a group of Young Social Innovators from the “Experts in Team” program of the Norwegian University of Science and Technology (NTNU-EiT). Both collectives perform actions in different fields, but with a common tool, the use of IT to empower citizens and achieve specific sustainability goals. The pilots will be initially focused on three specific challenges: “Ensuring healthy lives and promote wellbeing for all at all ages (UN´s Goal 3)”, “Ensuring inclusive and equitable quality education and promote lifelong learning opportunities for all (UN’s Goal 4)”, and “Promoting sustained, inclusive and sustainable economic growth, full and productive employment and decent work for all (UN’s Goal 8)”.
The SOCRATIC’s platform will be used by citizens and organizations to
to propose new challenges oriented to solve specific sustainability issues.
to invite individuals or organizations to participate with innovative ideas that solve these issues.
to collectively select and implement the most promising ideas.
Challenges are the targets that United Nations has set in order to achieve its Global Sustainability Goals (GSG). This village will identify challenges and propose solutions regarding the UN’s sustainability goal number 3 , which is “Ensuring healthy lives and promote well-being for all at all ages”.
', '2018-12-07 19:34:35');
INSERT INTO edsi.ProjectTexts (Projects_idProjects, Text, DateObtained) VALUES (7619, '11 months ago
The DecarboNet research project has received funding from the European Union''s Seventh Framework Programme for research, technological development and demonstration under grant agreement no 610829.
By using this site you accept the setting of cookies to improve user experience. To learn more, please visit our privacy policy.
OK
', '2018-12-07 19:34:36');