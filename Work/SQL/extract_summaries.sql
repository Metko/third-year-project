select * from AdditionalProjectData where Projects_idProjects= 2132;

# Get long and short descriptions side by side
select a.Value as 'Long Text', b.Value as 'Short Text', a.Projects_idProjects, p.ProjectName as 'name' from AdditionalProjectData a
left join AdditionalProjectData b on a.Projects_idProjects = b.Projects_idProjects
left join Projects p on a.Projects_idProjects = p.idProjects
where a.FieldName = 'Long Description' and a.Value != ''
#and a.Value not like '%<%'
and char_length(a.Value) > 500
and b.FieldName = 'Short Description' and b.Value != '';

# Average short description length
select avg(char_length(VALUE)) as 'Short Description Length' from AdditionalProjectData
where FieldName = 'Short Description' and Value != '';

select count(Projects_idProjects) from AdditionalProjectData where FieldName = 'Short Description' and char_length(Value) > 50;

# Average long description length
select avg(char_length(VALUE)) as 'Long Description Length' from AdditionalProjectData
where FieldName = 'Long Description' and Value != '';

# 48xx projects in total, 2216 long and short descriptions, 1934 marked as description only
select count(id) from AdditionalProjectData where FieldName = 'Description';


select value from AdditionalProjectData where Projects_idProjects = 3193;


select Projects_idProjects as id from AdditionalProjectData where FieldName = 'Long Description' and value like (select value from AdditionalProjectData where FieldName = 'Short Description' and Projects_idProjects = id);


select FieldName, Value from AdditionalProjectData where Projects_idProjects = 3261;

# check number of keyword present
select Value from AdditionalProjectData where FieldName = 'Keyword' and Value != '';


select * from Projects where Lower(ProjectName) like '%food%';
select FieldName, Value from AdditionalProjectData where Projects_idProjects = 12680;


select count(*) from AdditionalProjectData ad
inner join ProjectCrawls pc on pc.Projects_idProjects = ad.Projects_idProjects
where lower(ad.FieldName) like '%description%' and ad.Value != '';



Select * from TypeOfSocialInnotation where SourceModel like '%v14%';

select distinct ProjectWebpage from  Projects P
inner join TypeOfSocialInnotation TOSI on P.idProjects = TOSI.Projects_idProjects
inner join ProjectCrawls PC on PC.Projects_idProjects = P.idProjects
inner join AdditionalProjectData APD on P.idProjects = APD.Projects_idProjects
where CriterionObjectives = 1 and CriterionActors = 1 and CriterionInnovativeness = 1 and SourceModel like '%v14%'
and PC.CrawledText like '%=============%';

select a.Value as 'Long Text', b.Value as 'Short Text', a.Projects_idProjects, p.ProjectName as 'name', p.ProjectWebpage from AdditionalProjectData a
left join AdditionalProjectData b on a.Projects_idProjects = b.Projects_idProjects
left join Projects p on a.Projects_idProjects = p.idProjects
where a.FieldName = 'Long Description' and a.Value != ''
#and a.Value not like '%<%'
and char_length(a.Value) > 500
and b.FieldName = 'Short Description' and b.Value != '';



select distinct ad.Value as 'summaries', pc.CrawledText as 'doc' from AdditionalProjectData ad
inner join ProjectCrawls pc on pc.Projects_idProjects = ad.Projects_idProjects
where lower(ad.FieldName) like '%description%' and ad.Value != '' and char_length(ad.Value) > 300;


select distinct pc.CrawledText as 'doc' from AdditionalProjectData ad
inner join ProjectCrawls pc on pc.Projects_idProjects = ad.Projects_idProjects
where pc.Projects_idProjects not in (select aa.Projects_idProjects from AdditionalProjectData aa
                                    where lower(aa.FieldName) like '%description%' and aa.Value != '')
and pc.CrawledText != ''


select distinct p.idProjects, p.ProjectName, p.URL_Reported, ad.FieldName, ad.Value from Projects p
inner join AdditionalProjectData ad on ad.Projects_idProjects = p.idProjects
#inner join TypeOfSocialInnotation tosi on p.idProjects = tosi.Projects_idProjects
where p.URL_Reported is not NULL
  and p.URL_Reported != ''
  and lower(ad.FieldName) like '%description%'
  and ad.Value != ''
  and ad.Value is not null
#   and tosi.CriterionObjectives = 1
#   and tosi.CriterionActors = 1
#   and tosi.CriterionInnovativeness = 1
#   and tosi.SourceModel like '%v14%'
;



delete from AdditionalProjectData where FieldName = 'Text';

select distinct p.idProjects, p.ProjectName, p.URL_Reported, ad.Value as 'Text', ads.Value as 'Description', pc.CrawledText from Projects p
inner join AdditionalProjectData ad on ad.Projects_idProjects = p.idProjects
inner join AdditionalProjectData ads on ads.Projects_idProjects = p.idProjects
inner join ProjectCrawls pc on pc.Projects_idProjects = p.idProjects
where p.URL_Reported is not NULL
  and p.URL_Reported != ''
  and lower(ad.FieldName) like '%text%'
  and ad.Value != ''
  and ad.Value is not null
  and ads.FieldName = 'Description';



select distinct Projects_idProjects from ProjectTexts;



SELECT GROUP_CONCAT(pt.Text SEPARATOR ' ') as 'doc', ad.Value as 'summaries' FROM ProjectTexts pt
inner join AdditionalProjectData ad on ad.Projects_idProjects = pt.Projects_idProjects
where ad.FieldName like  'Description'
and ad.Value != ''
and ad.Value is not null
GROUP BY pt.Projects_idProjects, ad.Value;


SET group_concat_max_len=100000;

select distinct Value from AdditionalProjectData where FieldName = 'Description';


delete from ProjectSummaries where document = '' or summary = '';

# AVG Topic Similiarity between text and given summary - 74.5 %
select avg(topics_similiarity) from ProjectSummaries where char_length(document_topics) != 0 and char_length(summary_topics) != 0 and topics_similiarity != 0;




select ps.Projects_idProjects as 'id', document as 'doc', CONCAT(document_keywords, ", ", document_topic_keywords) as 'keywords', summary as 'summaries', pl.Country as 'location' from ProjectSummaries ps
left join ProjectLocation pl on pl.Projects_idProjects = ps.Projects_idProjects
where document != '' and summary != ''