## Implementation Of SummaRuNNer

### Model Diagram
<div  align="center">
<img src="hpzhao_implementation/images/RNN_RNN.jpg" width = "350" height = "350" align=center />
</div>

## Pretrained Models
1. `data/SIPSummaRuNNer.pt` - Trained on **not** *pre-processed* KNOWMAK dataset
2. `data/SIPSummaRuNNerPretrained.pt` - Trained on *pre-processed* KNOWMAK dataset
3. `data/SummaRuNNer.pt` - Trained on DailyMail dataset

### Setup

The whole implementation is done in the `sumarunner-implementation` notebook. 

### Usage  

```shell
# run
python3 script/main.py -model 1 -input input.json -output output.json
```
There are 3 parameters that the script takes:
1. `model` - Select one of the pretrained models (1, 2, 3)
2. `input` - This is a *json* file with the texts that you want to summarize. Its format should be:
```json
[
    {"document" : "..."},
    {"document" : "..."},
    ...
]
```
3. `output` - This is the *json* file that would be generated and it would contain the summaries:
```json
[
    {"document" : "...", "summary" : "..."},
    {"document" : "...", "summary" : "..."},
    ...
]
```
If you need to change the length of the summary that is being generated please look at `config.py`.

### Download Data:  

+ Google Driver:[data.tar.gz](https://drive.google.com/file/d/1JgsboIAs__r6XfCbkDWgmberXJw8FBWE/view?usp=sharing)

+ Source Data:[Neural Summarization by Extracting Sentences and Words](https://docs.google.com/uc?id=0B0Obe9L1qtsnSXZEd0JCenIyejg&export=download)


