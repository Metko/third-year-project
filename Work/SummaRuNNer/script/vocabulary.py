from config import UNK_TOKEN

class Vocab:
    def __init__(self, word2id):
        self.word2id = word2id or dict()
        self.id2word = { value: key for key, value in word2id.items() }
        
    def __len__(self):
        return len(self.word2id)
    
    def idx2word(self, idx):
        return self.id2word[idx]
    
    def word2idx(self, word):
        if word in self.word2id:
            return self.word2id[word]
        else:
            return UNK_TOKEN