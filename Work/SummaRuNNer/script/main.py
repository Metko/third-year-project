import argparse
from config import *
import numpy as np
import torch
from torch.autograd import Variable
import pandas as pd
from helpers import load_embeddings, get_model, add_sentences_split_symbol
from model import SummaRuNNer
from preprocess import *


parser = argparse.ArgumentParser(description='SummaRuNNer')
parser.add_argument('-model', type=int, default=1)
parser.add_argument('-output', type=str, default='output.json')
parser.add_argument('-input', type=str, default='input.json')
args = parser.parse_args()

model = SummaRuNNer(WORD_EMBEDDING_INPUT_SIZE, 
                    WORD_EMBEDDING_DIM, 
                    HIDDEN_LAYER_SIZE, 
                    POS_INPUT_SIZE, 
                    SEG_INPUT_SIZE, 
                    POS_EMBED_DIM
                   )
model.load(DATA_FOLDER + '/' + get_model(args.model))

def summarize(text):
    text = preprocess_text(text)
    if SPLIT_TOKEN not in text:
        text = add_sentences_split_symbol(text)
    sents = text.split(SPLIT_TOKEN)
    if len(sents) == 1: 
        return sents[0]
    sents = sents[:min(sents_per_doc_max, len(sents))]
    doc_len = len(sents)
    tokenized_sents = tokenize_sents(sents)
    encoded_sents = encode_and_pad_tokenized_sents(tokenized_sents)
    sents = Variable(torch.LongTensor(encoded_sents))

    if USE_CUDA:
        sents = sents.cuda()
        
    probs = model(sents, [doc_len])
    
    if USE_RELATIVE_LENGTH:
        summary_len = int(doc_len * RELATIVE_LENGTH) + 1
    else:
        summary_len = min(doc_len, SUMMARY_SENTENCES)
    topk_indices = probs.topk(summary_len)[1].cpu().data.numpy()
    topk_indices.sort()
    doc = text.split(SPLIT_TOKEN)[:doc_len]
    result = [doc[index] for index in topk_indices]
    return ' '.join(result)

df = pd.read_json(args.input)
df['summary'] = df.apply(lambda x: summarize(x.document), axis=1)

with open(args.output, 'w') as f:
    f.write(df.to_json(orient='records'))

print('Summaries written to output file successfully!')