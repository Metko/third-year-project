import torch
from vocabulary import Vocab
from config import *
from helpers import load_word2id

vocab = Vocab(load_word2id(DATA_FOLDER + '/' + 'word2id.json'))

# Trims each document and its corresponding labels to a certain size
def trim_doc_and_labels(doc_str, label_str):
    sents = doc_str.split(SPLIT_TOKEN)
    labels = label_str.split(SPLIT_TOKEN)
    # cast labels to integer
    labels = [int(label) for label in labels]
    max_sent_num = min(sents_per_doc_max, len(sents))
    return sents[:max_sent_num], labels[:max_sent_num]

# Gets all entries from a batch and concatenates them in a single list
# (one list of sents, one for labels and one storing document lengths)
def combine_entries(batch):
    all_doc_sents, all_doc_labels, doc_lens = [], [], []
    for doc_str, label_str in zip(batch['doc'], batch['labels']):
        sents, labels = trim_doc_and_labels(doc_str, label_str)
        all_doc_sents += sents
        all_doc_labels += labels
        doc_lens.append(len(sents))
    return all_doc_sents, all_doc_labels, doc_lens

def tokenize_sents(sents):
    tokenized_sents = []
    for sent in sents:
        words = sent.split()
        # Truncate sentence
        if len(words) > words_per_sent_max:
            words = words[:words_per_sent_max]
        tokenized_sents.append(words)
    return tokenized_sents

# Replaces words with their corresponding ids
def encode_and_pad_tokenized_sents(sents):
    encoded_sents = []
    for sent in sents:
        pad_length = words_per_sent_max - len(sent)
        encoded_sent = [vocab.word2idx(word) for word in sent] + [PAD_TOKEN for _ in range(pad_length)]
        encoded_sents.append(encoded_sent)
    return encoded_sents

def process_batch(batch):
    sents, labels, doc_lens = combine_entries(batch)
    tokenized_sents = tokenize_sents(sents)
    encoded_sents = encode_and_pad_tokenized_sents(tokenized_sents)
    
    sents = torch.LongTensor(encoded_sents)
    labels = torch.LongTensor(labels)
    summaries = batch['summaries']
    return sents, labels, doc_lens, summaries    

import re
from nltk.tokenize import sent_tokenize, word_tokenize
import spacy
nlp = spacy.load('en_core_web_sm')

def remove_string_special_characters(s):
    # Replace special character with ' '
    stripped = re.sub('[^\w.!?%\s]', '', s)
    stripped = re.sub('_', '', stripped)

    # Change any whitespace to one space
    stripped = re.sub('\s+', ' ', stripped)

    # Remove start and end white spaces
    stripped = stripped.strip()

    return stripped


def remove_html_tags(text):
    return re.sub('<[^<]+?>', '', text).replace('\n', ' ')


def check_sentence_has_verb(tokens):
    for token in tokens:
        if token.pos_ == 'VERB':
            return True
    return False

def check_sentence_for_consecutive_nouns(tokens):
    nouns_counter = 0
    for token in tokens:
        if token.pos_ == 'NOUN':
            nouns_counter += 1
            if nouns_counter >= MAX_CONSECUTIVE_NOUNS:
                return True
        else:
            nouns_counter = 0
    return False


def check_sentence_is_valid(sentence):
    tokens = nlp(sentence)
    return check_sentence_has_verb(tokens) and not check_sentence_for_consecutive_nouns(tokens)


def preprocess_text(text):
    text = remove_string_special_characters(text)
    text = remove_html_tags(text)
    sentences = sent_tokenize(text)
    sentences = [sent for sent in sentences if len(word_tokenize(sent)) > MIN_SENT_WORDS_LEN and len(word_tokenize(sent)) < MAX_SENT_WORDS_LEN and check_sentence_is_valid(sent)]
    return '\n'.join(sentences)