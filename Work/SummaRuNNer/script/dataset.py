import torch.utils.data as data

class Dataset(data.Dataset):
    def __init__(self, entries):
        super(Dataset, self).__init__()
        self.entries = entries
        
    def __getitem__(self, idx):
        return self.entries[idx]
    
    def __len__(self):
        return len(self.entries) 