import numpy as np
import torch
import json
from config import SPLIT_TOKEN
from nltk import sent_tokenize

def add_sentences_split_symbol(text):
    sentences = sent_tokenize(text)
    return SPLIT_TOKEN.join(sentences)

def load_embeddings(file_path='embedding.npz'):
    file = np.load(file_path)
    return torch.Tensor(file['embedding'])

def load_word2id(file_path='word2id.json'):
    word2id = dict()
    with open(file_path) as file:
        word2id = json.load(file)
    return word2id

def get_model(type):
    if type == 1:
        return 'SIPSummaRuNNer.pt'
    elif type == 2:
        return 'SIPSummaRuNNerPretrained.pt'
    else:
        return 'SummaRuNNer.pt'
