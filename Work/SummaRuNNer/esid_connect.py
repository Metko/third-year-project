import pymysql

def fetch_projects(number = 50):
    conn = pymysql.connect(host='localhost', port=8889, user='root', password='root', db='edsi', cursorclass=pymysql.cursors.DictCursor)
    connCursor = conn.cursor()
    sql = '''
select a.Value as 'long_text', b.Value as 'short_text', a.Projects_idProjects as 'id', p.ProjectName as 'name' from AdditionalProjectData a
left join AdditionalProjectData b on a.Projects_idProjects = b.Projects_idProjects
left join Projects p on a.Projects_idProjects = p.idProjects
where a.FieldName = 'Long Description' and a.Value != ''
and char_length(a.Value) > 500
and b.FieldName = 'Short Description' and b.Value != ''
limit {number}
'''.format(**vars())
    connCursor.execute(sql)
    result = connCursor.fetchall()
    connCursor.close()
    conn.close()
    return result

