from joblib import load
import numpy as np
from preprocess import preprocess_text
import argparse
import pandas as pd
from stemmed_vectorizer import StemmedCountVectorizer

parser = argparse.ArgumentParser(description='SVM Summarizer')
parser.add_argument('-type', type=int, default=1)
parser.add_argument('-output', type=str, default='output.json')
parser.add_argument('-input', type=str, default='input.json')
parser.add_argument('-sents_per_topic', type=int, default=2)
args = parser.parse_args()

MODEL_FOLDER = '../models/'
SPLIT_TOKEN = '\n'
SENTS_PER_TOPIC = args.sents_per_topic

def extract_summary(predicted, all_sents):
    summary = list()
    for index, predict in enumerate(predicted):
        if int(predict) == 1:
            summary.append(all_sents[index])
    return ' '.join(summary)

def get_binary_summary(text):
    svm = load(MODEL_FOLDER + 'binary_svm.joblib')
    sents = text.split(SPLIT_TOKEN)
    predicted = svm.predict(sents)
    return extract_summary(predicted, sents)

def get_binary_big_data_summary(text):
    svm = load(MODEL_FOLDER + 'binary_svm_big_dataset.joblib')
    sents = text.split(SPLIT_TOKEN)
    predicted = svm.predict(sents)
    return extract_summary(predicted, sents)

def convert_to_labels(sent_probs):
    probs = sent_probs.copy()
    probs[::-1].sort()
    for index, prob in enumerate(sent_probs):
        if prob in probs[:SENTS_PER_TOPIC]:
            sent_probs[index] = 1
        else:
            sent_probs[index] = 0
    return sent_probs

def load_topic_models():
    objective_svm = load(MODEL_FOLDER + 'objective_svm.joblib')
    actor_svm = load(MODEL_FOLDER + 'actor_svm.joblib')
    innovation_svm = load(MODEL_FOLDER + 'innovation_svm.joblib')
    output_svm = load(MODEL_FOLDER + 'output_svm.joblib')
    return objective_svm, actor_svm, innovation_svm, output_svm

def get_topics_summary_by_prob(text):
    objective_svm, actor_svm, innovation_svm, output_svm = load_topic_models()
    sents = text.split(SPLIT_TOKEN)
    objectives = convert_to_labels(objective_svm.predict_proba(sents)[:, 1])
    actors = convert_to_labels(actor_svm.predict_proba(sents)[:, 1])
    innovations = convert_to_labels(innovation_svm.predict_proba(sents)[:, 1])
    outputs = convert_to_labels(output_svm.predict_proba(sents)[:, 1])

    predicted = np.logical_or(objectives, actors)
    predicted = np.logical_or(predicted, innovations)
    predicted = np.logical_or(predicted, outputs)

    return extract_summary(predicted, sents)


def get_topics_summary(text):
    objective_svm, actor_svm, innovation_svm, output_svm = load_topic_models()
    sents = text.split(SPLIT_TOKEN)

    objectives = objective_svm.predict(sents)
    actors = actor_svm.predict(sents)
    innovations = innovation_svm.predict(sents)
    outputs = output_svm.predict(sents)

    predicted = np.logical_or(objectives, actors)
    predicted = np.logical_or(predicted, innovations)
    predicted = np.logical_or(predicted, outputs)

    return extract_summary(predicted, sents)

def summarize(document):
    text = preprocess_text(document)
    if args.type == 1:
        return get_binary_summary(text)
    elif args.type == 2: 
        return get_topics_summary(text)
    elif args.type == 3:
        return get_topics_summary_by_prob(text)
    else:
        return get_binary_big_data_summary(text)

df = pd.read_json(args.input)
df['summary'] = df.apply(lambda x: summarize(x.document), axis=1)

with open(args.output, 'w') as f:
    f.write(df.to_json(orient='records'))

print('Summaries written to output file successfully!')