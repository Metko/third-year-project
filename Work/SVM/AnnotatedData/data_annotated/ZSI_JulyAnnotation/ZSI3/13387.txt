Un camper per i diritti - Florence - Medici per i Diritti Umani
===============================

   <![endif] 
 [if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif] 
 0 
 Shopping cart   
 No products in the cart. 
  end product list  
 Medici per i Diritti Umani 
   facebook_account   twitter   flicker_account   youtube_account   
 English 
 Italiano 
   
 + 
 Navigation Home 
 About us 
 Projects 
 National Projects 
 International Projects 
 Press release 
 Report 
 How to participate 
 Volunteer with us 
 Donations 
 
					Un camper per i diritti &#8211; Florence  
                 
 Promoting psychosocial health care for the homeless in Florence 
 The context 
 In Florence, the homeless population is variously composed of 2200 individuals according to several official reports by 
local public and non-government agencies. The project stems from the identification of an urgent need. From the health care and social point of view, homeless people often find themselves with no support and they are unable to express their needs to a world far away from their reality. 
The idea for the project “Un Camper per i Diritti” (A Camper for Human Rights) stems from the possibility to combine one of the main goals of Medici per i Diritti Umani (Doctors for Human Rights) &#8211; i.e. to guarantee the right to health and social inclusion for vulnerable groups &#8211; and the need for the city to have more tools to tackle these issues. 
The mobile street unit is conceived as a basic low threshold level service. It aims at achieving the aforementioned goals and it represents a valuable aid in epidemiological surveillance operations carried out in a setting where the population is often difficult to reach by the initiatives for the prevention and control of diseases implemented by National Health Service. 
 The project “Un Camper per i Diritti” is active in Florence since 2006. 
 The project targets at least 4 segments of the population in precarious settlements: 
 &#8211; Refugees rejected by shelters for lack of space; 
&#8211; EU citizens who do not meet administrative conditions required to regularize their status; 
&#8211; Foreigners residing legally and illegally in Italy; 
&#8211; Romani citizens residing in spontaneous unauthorized settlements; 
&#8211; Italian citizens in precarious condition; 
 In the latter three groups at least 25% of the citizens are minors. 27% of refugees are victims of torture or violence at an international level. 
 The project 
 The mobile unit of Florence is a first aid service which offers counseling about social and health care services. It targets the homeless population of the city and of neighboring towns. The service will act on a complementary function to the Regional Health Service, which will retain its central role and its function to guarantee access to health services for the general population. 
The areas and the modalities of intervention are selected periodically after an accurate and continuous monitoring of the territory, giving priority to situations characterized by greater uncertainty and a lack or absence of access to social and health care services. 
During these years, the Camper per i Diritti visited many different contexts, either for the their and for the type of residents, e.g. buildings occupied by refugees or beneficiaries of international protection (viale Guidoni, Cascine, the warehouses of former Hospital Meyer, via Slataper), buildings occupied by Italian citizens and foreigners with different nationalities and administrative status (Viale Matteotti, Poggio Secco, former sanatorium Luzzi Pratolino), railway stations, spontaneous settlements of Roma between Florence and Sesto Fiorentino. 
Through the provision of a basic first aid service, MEDU operators are able to build a relationship of trust with users in order to start a gradual rapprochement with public health facilities. 
This will occur through the activation of a series of procedures aimed at regularizing the administrative position of foreign citizens (by obtaining a TPF card [Temporarily Present Foreigner, or STP, Straniero Temporaneamente Presente] or by registering as an NHS patient [for those eligible]) and at increasing regular use of health care and social services (by guiding or accompanying the most vulnerable cases). 
A customized medical chart and a data sheet will be prepared for each patient and an epidemiological fact sheet will be created for each settlement in order to assess their sanitary conditions. The project team is composed of different professionals health care and non-health care professionals (doctors, nurses, midwives, psychologists, anthropologists, lawyers), who work on a voluntary basis. 
 The expected results are as follows: 
 &#8211; the beneficiary population receives first aid services; 
&#8211; the beneficiary population receives adequate information on how to access the National Health Service, on the right to health and on other fundamental rights; 
&#8211; the beneficiary population benefits from an appropriate guidance on how to access the NHS facilities, shelters and protection institutions. 
 The information and awareness-raising activities play a crucial role in this project. In addition to direct assistance,the project aims at informing and raising awareness amongst public opinion and institutions on the social reality of particularly vulnerable segments of the population and on the respect of their fundamental rights, with special emphasis on the access to heath care. Finally, it is crucial to advocate for the right to health and other social rights in relevant institutional fora and to demand legislative and operational interventions for expanding the access to public health care and social services for the most vulnerable segments of population. 
 Further Information 
 Città Senza Dimora &#8211; INDAGINE SULLE STRADE DELL’ESCLUSIONE (Infinito edizioni 2011) 
 Rapporto 2009 sulle attività di assistenza sanitaria su strada a Roma &#8211; &#8220;Un camper per i diritti&#8221; 
 Rapporto 2008 sulle attività di assistenza sanitaria su strada a Roma e Firenze &#8211; &#8220;Un camper per i diritti&#8221; 
 Un camper per i diritti 2007 Rapporto sulle attività di assistenza sanitaria su strada a Roma e Firenze 
   
 SUPPORT OUR MOBILE CLINIC NOW 
 
ITALY 
 Infant mortality 
4,4 %o 
 Life expectancy at birth 
78,8 
 Human Development Index 
0,867; posizione 21/177 
 GDP per capita ($) 
25600 
 The Beneficiary population: 
Direct beneficiary population: the homeless population in Florence (almost 2000 people) 
Indirect beneficiary population: residents in the areas of intervention will be the indirect beneficiaries of publichealth initiatives, epidemiological surveillance, preventive healthcare and health promotion implemented by the project. 
 Volunteers: 
45 
 Project financiers: 
Tavola Valdese 
 Budget: 
2015: 26.480,00 Euro for activity in  Rome and Florence 
 Contact Us 
 Send message 
   
 Addresses   ROMA 
Via dei Volsci, 101 
00185 Roma 
tel. e fax 06/97844892 cell. 334/3929765
   
 FIRENZE 
Via Monsignor Leto Casini, 11 
50135 Firenze 
tel. 335/1853361 
 Informativa sui cookies 
 Privacy policy 
   
 Flickr   
   
 Medici per i diritti umani 2015 © tutti i diritti sono riservati  P.I. 10248871005 C.F.: 97328850587 
 Home 
 About us 
 Projects 
 Press release 
 Report 
 Donations 
   
  Italy Cookie Choices  " Informativa\r\n \r\nQuesto sito o gli strumenti terzi da questo utilizzati si avvalgono di cookie necessari al funzionamento ed utili alle finalit\u00e0 illustrate nella cookie policy. Se vuoi saperne di pi\u00f9 o negare il consenso a tutti o ad alcuni cookie, consulta la cookie policy.\r\nChiudendo questo banner, scorrendo questa pagina, cliccando su un link o proseguendo la navigazione in altra maniera, acconsenti all\u2019uso dei cookie.<\/b><\/div>" la pagina sull\'informativa dei cookies 
Un camper per i diritti - Florence - Medici per i Diritti Umani
===============================

   <![endif] 
 [if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif] 
 0 
 Shopping cart   
 No products in the cart. 
  end product list  
 Medici per i Diritti Umani 
   facebook_account   twitter   flicker_account   youtube_account   
 English 
 Italiano 
   
 + 
 Navigation Home 
 About us 
 Projects 
 National Projects 
 International Projects 
 Press release 
 Report 
 How to participate 
 Volunteer with us 
 Donations 
 
					Un camper per i diritti &#8211; Florence  
                 
 Promoting psychosocial health care for the homeless in Florence 
 The context 
 In Florence, the homeless population is variously composed of 2200 individuals according to several official reports by 
local public and non-government agencies. The project stems from the identification of an urgent need. From the health care and social point of view, homeless people often find themselves with no support and they are unable to express their needs to a world far away from their reality. 
The idea for the project “Un Camper per i Diritti” (A Camper for Human Rights) stems from the possibility to combine one of the main goals of Medici per i Diritti Umani (Doctors for Human Rights) &#8211; i.e. to guarantee the right to health and social inclusion for vulnerable groups &#8211; and the need for the city to have more tools to tackle these issues. 
The mobile street unit is conceived as a basic low threshold level service. It aims at achieving the aforementioned goals and it represents a valuable aid in epidemiological surveillance operations carried out in a setting where the population is often difficult to reach by the initiatives for the prevention and control of diseases implemented by National Health Service. 
 The project “Un Camper per i Diritti” is active in Florence since 2006. 
 The project targets at least 4 segments of the population in precarious settlements: 
 &#8211; Refugees rejected by shelters for lack of space; 
&#8211; EU citizens who do not meet administrative conditions required to regularize their status; 
&#8211; Foreigners residing legally and illegally in Italy; 
&#8211; Romani citizens residing in spontaneous unauthorized settlements; 
&#8211; Italian citizens in precarious condition; 
 In the latter three groups at least 25% of the citizens are minors. 27% of refugees are victims of torture or violence at an international level. 
 The project 
 The mobile unit of Florence is a first aid service which offers counseling about social and health care services. It targets the homeless population of the city and of neighboring towns. The service will act on a complementary function to the Regional Health Service, which will retain its central role and its function to guarantee access to health services for the general population. 
The areas and the modalities of intervention are selected periodically after an accurate and continuous monitoring of the territory, giving priority to situations characterized by greater uncertainty and a lack or absence of access to social and health care services. 
During these years, the Camper per i Diritti visited many different contexts, either for the their and for the type of residents, e.g. buildings occupied by refugees or beneficiaries of international protection (viale Guidoni, Cascine, the warehouses of former Hospital Meyer, via Slataper), buildings occupied by Italian citizens and foreigners with different nationalities and administrative status (Viale Matteotti, Poggio Secco, former sanatorium Luzzi Pratolino), railway stations, spontaneous settlements of Roma between Florence and Sesto Fiorentino. 
Through the provision of a basic first aid service, MEDU operators are able to build a relationship of trust with users in order to start a gradual rapprochement with public health facilities. 
This will occur through the activation of a series of procedures aimed at regularizing the administrative position of foreign citizens (by obtaining a TPF card [Temporarily Present Foreigner, or STP, Straniero Temporaneamente Presente] or by registering as an NHS patient [for those eligible]) and at increasing regular use of health care and social services (by guiding or accompanying the most vulnerable cases). 
A customized medical chart and a data sheet will be prepared for each patient and an epidemiological fact sheet will be created for each settlement in order to assess their sanitary conditions. The project team is composed of different professionals health care and non-health care professionals (doctors, nurses, midwives, psychologists, anthropologists, lawyers), who work on a voluntary basis. 
 The expected results are as follows: 
 &#8211; the beneficiary population receives first aid services; 
&#8211; the beneficiary population receives adequate information on how to access the National Health Service, on the right to health and on other fundamental rights; 
&#8211; the beneficiary population benefits from an appropriate guidance on how to access the NHS facilities, shelters and protection institutions. 
 The information and awareness-raising activities play a crucial role in this project. In addition to direct assistance,the project aims at informing and raising awareness amongst public opinion and institutions on the social reality of particularly vulnerable segments of the population and on the respect of their fundamental rights, with special emphasis on the access to heath care. Finally, it is crucial to advocate for the right to health and other social rights in relevant institutional fora and to demand legislative and operational interventions for expanding the access to public health care and social services for the most vulnerable segments of population. 
 Further Information 
 Città Senza Dimora &#8211; INDAGINE SULLE STRADE DELL’ESCLUSIONE (Infinito edizioni 2011) 
 Rapporto 2009 sulle attività di assistenza sanitaria su strada a Roma &#8211; &#8220;Un camper per i diritti&#8221; 
 Rapporto 2008 sulle attività di assistenza sanitaria su strada a Roma e Firenze &#8211; &#8220;Un camper per i diritti&#8221; 
 Un camper per i diritti 2007 Rapporto sulle attività di assistenza sanitaria su strada a Roma e Firenze 
   
 SUPPORT OUR MOBILE CLINIC NOW 
 
ITALY 
 Infant mortality 
4,4 %o 
 Life expectancy at birth 
78,8 
 Human Development Index 
0,867; posizione 21/177 
 GDP per capita ($) 
25600 
 The Beneficiary population: 
Direct beneficiary population: the homeless population in Florence (almost 2000 people) 
Indirect beneficiary population: residents in the areas of intervention will be the indirect beneficiaries of publichealth initiatives, epidemiological surveillance, preventive healthcare and health promotion implemented by the project. 
 Volunteers: 
45 
 Project financiers: 
Tavola Valdese 
 Budget: 
2015: 26.480,00 Euro for activity in  Rome and Florence 
 Contact Us 
 Send message 
   
 Addresses   ROMA 
Via dei Volsci, 101 
00185 Roma 
tel. e fax 06/97844892 cell. 334/3929765
   
 FIRENZE 
Via Monsignor Leto Casini, 11 
50135 Firenze 
tel. 335/1853361 
 Informativa sui cookies 
 Privacy policy 
   
 Flickr   
   
 Medici per i diritti umani 2015 © tutti i diritti sono riservati  P.I. 10248871005 C.F.: 97328850587 
 Home 
 About us 
 Projects 
 Press release 
 Report 
 Donations 
   
  Italy Cookie Choices  " Informativa\r\n \r\nQuesto sito o gli strumenti terzi da questo utilizzati si avvalgono di cookie necessari al funzionamento ed utili alle finalit\u00e0 illustrate nella cookie policy. Se vuoi saperne di pi\u00f9 o negare il consenso a tutti o ad alcuni cookie, consulta la cookie policy.\r\nChiudendo questo banner, scorrendo questa pagina, cliccando su un link o proseguendo la navigazione in altra maniera, acconsenti all\u2019uso dei cookie.<\/b><\/div>" la pagina sull\'informativa dei cookies 

 "Un Camper per i diritti" is a mobile unit providing a first aid service. It offers counseling about social and health care services and targets the homeless population of the city of Florence and of neighbouring towns. 

 "Un Camper per i diritti" is a mobile unit providing a first aid service. It offers counseling about social and health care services and targets the homeless population of the city of Florence and of neighbouring towns.
 WHOLE PROJECT MARK