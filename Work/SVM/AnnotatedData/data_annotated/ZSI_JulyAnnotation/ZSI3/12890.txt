Developing informal (neighborhood) care networks | zorg
===============================

 <![endif] 
 Jump to navigation 
 Home 
 Care Living Labs Structure of the Care Living Labs 
 About Program Office 
 Scientific Consortium (KIO) 
 Sounding Board Committee 
 Platforms AIPA - Ageing in place Aalst 
 Active Caring Neighborhood 
 CareVille Limburg - Moving Care 
 InnovAGE 
 LiCalab - Living &amp; Care lab 
 Online Neighborhoods 
 Projects 
 News 
 Events 
 Knowledge Base Reports 
 SIL 
 Congress papers, abstracts and posters, presentations 
 Contact 
 Nederlands 
 Vous avez des questions? cialis  cialis pharmacie  Laissez nous un message! 
 Developing informal (neighborhood) care networks 
 PROJECT PLATFORMS Active Caring Neighborhood CONTACT DETAILS Nathalie Dillen AzoB Platform Coordinator/Head Coordinator nathalie.dillen@zorgbedrijf.antwerpen.be zorgproeftuin@zorgbedrijf.antwerpen.be +32 (0)3 292 85 59 +32 (0)499 80 26 30 
 TAGS Informal neighborhood care network Social isolation Home living Informal care PROJECT PARTNERS 
   
 Developing informal (neighborhood) care networks The “Informal (neighborhood) care networks” project wants, through a broader vision of care, to develop a working model that makes informal care visible and creates, facilitates, supports and valorizes informal (neighborhood) care networks. By deploying informal (neighborhood) care networks, the quality of life of seniors increases and their independence rises, so that they can continue to live at home in a quality manner and are removed from their social isolation. 
 Additionally, with this project we wish to better align informal and formal support to what elderly people needing care and their informal caregivers can still do and to use the strength of the neighborhood and local residents. Care and welfare indeed should start closer to the citizens; care and service providers are thereby supportive, facilitating and binding. 
 The development of informal care networks in the context of a large city is a particular challenge: the social fabric is under pressure there, people move frequently, new residents (whether of foreign origin or not) move in, and others leave the neighborhood. Spontaneous help for neighbors has become less obvious than before and informal care is under pressure. 
 Approach   
 The project will be developed in Antwerp as well as Brussels. In Antwerp (Old Merksem neighborhood) we will start from the method of 'activating home visits'. A group of volunteer, informal caregivers regularly visit elderly people in order to remove them from their social isolation and/or to let them get acquainted with the offering of activities for seniors (mainly in two local service centers). 
 In the Brussels neighborhoods, small informal care networks will be developed whereby family, children, grandchildren, neighbors, shop owners... assume tasks so that seniors can remain living at home independently as long as possible. The activating home visit is seen as one of the activities that can be taken over by the informal network. The project will be tested in two different neighborhoods: the one neighborhood is a predominantly multicultural and disadvantaged neighborhood; the other neighborhood is a mixed neighborhood where the residents are generally in a less precarious situation. 
 Partners   
 In Antwerp we will start from cooperation between the urban services/Zorgbedrijf and Ziekenzorg (Healthcare). The home service of Zorgbedrijf Antwerp is responsible for guiding elderly people who need care and are vulnerable. The City of Antwerp actively participates in recruiting and monitoring volunteers and functions as a contact point for the Antwerp partners. 
 In Brussels, EVA npo and the neighborhood house Chambéry npo are the leaders. The Knowledge Center Home Care Brussels will take responsibility for coordinating the platform. The most important local partners for developing the project are the Maison BILOBA House and the Aksent Local Services Center. An iterative process of co-creation will be the working method during the entire project. Together with the elderly people concerned, informal caregivers and key actors in the neighborhood, the informal networks will be developed step by step. There is special attention to innovative methods such as a digital communication platform, need detection of (still difficult to reach) residents of foreign origin and knowledge development and exchange between the projects in Antwerp and Brussels. In this way the partners ensure a consolidated method description that can also be rolled out elsewhere. 
   
 Main menu Home 
 co-creatiesessie 
   
 z 
 About Flanders Care Living Labs 
 19 September 2013 marked the official launch of the Flanders Care Living Labs. Flanders Care Living Labs aims at facilitating the innovation of new care and help processes or products related to the care of the elderly. 
 Participating platforms, each with their own specific focus, include: 
 'Actief Zorgzame Buurt' in Antwerp and Brussels 
 'Ageing in Place' (AIPA) in the Aalst region 
 'InnovAGE' in the Leuven region 
 'Living and Care Lab' (LiCalab) in de Kempen region 
 'CareVille Limburg, Moving Care' in de Genk &amp; Hasselt regions 
 'Online Buurten' in West-flanders 
 TAGS 
   Thuis wonen     
   Home living     
   Technologie     
   Technology     
   Prevention     
   Medication     
   Complexe behoefte     
   Medicatie     
   Mantelzorg     
   Complex need     
   Sociaal isolement     
   Preventie     
   Therapietrouw     
   Therapy compliance     
   Informal care     
 2018 Zorgproeftuinen Contact 
Developing informal (neighborhood) care networks | zorg
===============================

 <![endif] 
 Jump to navigation 
 Home 
 Care Living Labs Structure of the Care Living Labs 
 About Program Office 
 Scientific Consortium (KIO) 
 Sounding Board Committee 
 Platforms AIPA - Ageing in place Aalst 
 Active Caring Neighborhood 
 CareVille Limburg - Moving Care 
 InnovAGE 
 LiCalab - Living &amp; Care lab 
 Online Neighborhoods 
 Projects 
 News 
 Events 
 Knowledge Base Reports 
 SIL 
 Congress papers, abstracts and posters, presentations 
 Contact 
 Nederlands 
 Vous avez des questions? cialis  cialis pharmacie  Laissez nous un message! 
 Developing informal (neighborhood) care networks 
 PROJECT PLATFORMS Active Caring Neighborhood CONTACT DETAILS Nathalie Dillen AzoB Platform Coordinator/Head Coordinator nathalie.dillen@zorgbedrijf.antwerpen.be zorgproeftuin@zorgbedrijf.antwerpen.be +32 (0)3 292 85 59 +32 (0)499 80 26 30 
 TAGS Informal neighborhood care network Social isolation Home living Informal care PROJECT PARTNERS 
   
 Developing informal (neighborhood) care networks The “Informal (neighborhood) care networks” project wants, through a broader vision of care, to develop a working model that makes informal care visible and creates, facilitates, supports and valorizes informal (neighborhood) care networks. By deploying informal (neighborhood) care networks, the quality of life of seniors increases and their independence rises, so that they can continue to live at home in a quality manner and are removed from their social isolation. 
 Additionally, with this project we wish to better align informal and formal support to what elderly people needing care and their informal caregivers can still do and to use the strength of the neighborhood and local residents. Care and welfare indeed should start closer to the citizens; care and service providers are thereby supportive, facilitating and binding. 
 The development of informal care networks in the context of a large city is a particular challenge: the social fabric is under pressure there, people move frequently, new residents (whether of foreign origin or not) move in, and others leave the neighborhood. Spontaneous help for neighbors has become less obvious than before and informal care is under pressure. 
 Approach   
 The project will be developed in Antwerp as well as Brussels. In Antwerp (Old Merksem neighborhood) we will start from the method of 'activating home visits'. A group of volunteer, informal caregivers regularly visit elderly people in order to remove them from their social isolation and/or to let them get acquainted with the offering of activities for seniors (mainly in two local service centers). 
 In the Brussels neighborhoods, small informal care networks will be developed whereby family, children, grandchildren, neighbors, shop owners... assume tasks so that seniors can remain living at home independently as long as possible. The activating home visit is seen as one of the activities that can be taken over by the informal network. The project will be tested in two different neighborhoods: the one neighborhood is a predominantly multicultural and disadvantaged neighborhood; the other neighborhood is a mixed neighborhood where the residents are generally in a less precarious situation. 
 Partners   
 In Antwerp we will start from cooperation between the urban services/Zorgbedrijf and Ziekenzorg (Healthcare). The home service of Zorgbedrijf Antwerp is responsible for guiding elderly people who need care and are vulnerable. The City of Antwerp actively participates in recruiting and monitoring volunteers and functions as a contact point for the Antwerp partners. 
 In Brussels, EVA npo and the neighborhood house Chambéry npo are the leaders. The Knowledge Center Home Care Brussels will take responsibility for coordinating the platform. The most important local partners for developing the project are the Maison BILOBA House and the Aksent Local Services Center. An iterative process of co-creation will be the working method during the entire project. Together with the elderly people concerned, informal caregivers and key actors in the neighborhood, the informal networks will be developed step by step. There is special attention to innovative methods such as a digital communication platform, need detection of (still difficult to reach) residents of foreign origin and knowledge development and exchange between the projects in Antwerp and Brussels. In this way the partners ensure a consolidated method description that can also be rolled out elsewhere. 
   
 Main menu Home 
 co-creatiesessie 
   
 z 
 About Flanders Care Living Labs 
 19 September 2013 marked the official launch of the Flanders Care Living Labs. Flanders Care Living Labs aims at facilitating the innovation of new care and help processes or products related to the care of the elderly. 
 Participating platforms, each with their own specific focus, include: 
 'Actief Zorgzame Buurt' in Antwerp and Brussels 
 'Ageing in Place' (AIPA) in the Aalst region 
 'InnovAGE' in the Leuven region 
 'Living and Care Lab' (LiCalab) in de Kempen region 
 'CareVille Limburg, Moving Care' in de Genk &amp; Hasselt regions 
 'Online Buurten' in West-flanders 
 TAGS 
   Thuis wonen     
   Home living     
   Technologie     
   Technology     
   Prevention     
   Medication     
   Complexe behoefte     
   Medicatie     
   Mantelzorg     
   Complex need     
   Sociaal isolement     
   Preventie     
   Therapietrouw     
   Therapy compliance     
   Informal care     
 2018 Zorgproeftuinen Contact 

 The "Informal (neighborhood) care networks" project aims to develop a model that makes informal care visible and creates, facilitates, supports and valorises informal (neighborhood) care networks. 

 The "Informal (neighborhood) care networks" project aims to develop a model that makes informal care visible and creates, facilitates, supports and valorises informal (neighborhood) care networks.
 WHOLE PROJECT MARK