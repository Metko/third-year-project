PACEMaker International | Changemakers
===============================

 
  Preloader  
  <div id="preloader">
	<div id="status"> </div>
</div>  
 Skip to main content 
 About 
 Challenges 
 Projects 
 Learning 
 Blog 
   
   
 Changemakers 
 English Español Português Français 
   
 Search form 
 Search  
   
 Log in   
 FAQs 
   
 You are here Home 
 PACEMaker International 
   Future Forward: Youth Innovations for Employment in Africa     
   
   Congratulations! This Entry has been selected as a finalist.     
   
   
   
 PACEMaker International: Youth In Action are questioning youth inaction in Kenya&#039;s education system. 
   Nairobi, Kenya Nairobi, Kenya   Peggy Mativo <div class="founder">Founder</div> 
   
   
    Year Founded:
   
 
    2013   
 Organization type:  nonprofit/ngo/citizen sector   
    Project Stage:
   
 
    Growth   
 Budget:  $10,000 - $50,000   
    Website:
   
 http://www.pacemakerinternational.org   
 Twitter:  https://twitter.com/Pacekenya Facebook:  https://www.facebook.com/PacePromotingAccessToCommunityEducationKenya 
 Corporate social responsibility   
 Education   
 Youth development   
 Youth leadership   
 Project Summary Elevator Pitch   
    Concise Summary: Help us pitch this solution! Provide an explanation within 3-4 short sentences.   
 PACE places Kenyan youth as teaching assistants in understaffed public schools for 6 month periods. We offer training in employment relevant skills coupled with an opportunity for youth to deeply impact the lives of younger students in low-income communities. We turn youth into active changemakers. 
 
    WHAT IF - Inspiration: Write one sentence that describes a way that your project dares to ask, &quot;WHAT IF?&quot;   
 
    What if young people could gain skills while ending the shortage of teachers in Kenya?    
 About Project   
    Problem: What problem is this project trying to address?   
 
    PACE seeks to unlock an additional 216 million hours of educational support for students from slums and rural areas. We target two key problems in Kenya: the shortage of 70,000+ teachers which disproportionately affects public schools and the lack of structured work opportunities for the 400,000 Kenyans who graduate from high school annually and college students on long vacations. We reposition these problems as solutions to each other.    
 
    Solution: What is the proposed solution? Please be specific!   
 
    We recruit, train and send youthful volunteers into understaffed schools as teaching assistants where they help fill the human resource gap in public schools. By improving the learning outcomes of our students today, we prepare them to escape poverty tomorrow. As the youthful teaching assistants serve in schools, they gain invaluable work experience and begin to see themselves as leaders who can make a difference in difficult, complex everyday situations. 
As a motivator for service, we uniquely couple in-school service with training so as to build specific skills that young people need for employment, but do not get from the current school system: leadership, first aid, teamwork, problem solving, job-readiness and communication skills. 
   
 Impact: How does it Work   
    Example: Walk us through a specific example(s) of how this solution makes a difference; include its primary activities.   
 
    Eneki, 18 years,just graduated from high school.She joins PACE &amp; goes through training on how to support learning for young students. She makes new friends who are passionate about making a difference and begins to work through her nervousness about public speaking. In her first week, Eneki is placed in a classroom with 45 students. She forms a special connection with Beryl, an intelligent class 8 student who is struggling to with English and needs a big sister to guide her. Eneki and Beryl meet for tutoring &amp; mentoring during lunch and tea breaks and over time, Beryl&#039;s performance improves. Eneki realizes that the school has a weak reading culture. She decides solve the problem &amp; initiate a fun weekly reading club for Beryl &amp; her friends.    
 
    Impact: What is the impact of the work to date? Also describe the projected future impact for the coming years.   
 
    PACE volunteers have worked in public schools for 20000 hours. This translates to Kenyan youth donating time that is valued at approximately 2.5 million shillings since January 2013.
165 youth have gained work experience and employment-relevant training and skills through the PACE volunteer program. 
5600 students have benefited from additional grading, tutoring and mentoring led by volunteer teaching assistants. Evidence is seen through increase in mean scores e.g. North Highridge Primary KCSE national exam mean moved up by 26 points last year, and Farasi Lane English mean doubled over the past year. 
After graduation, PACE volunteers have chosen to go into fulltime teaching as a profession, to start businesses that supply schools with exam prep materials, to earn incomes as paid tutors, to author books for primary school students &amp; to continue giving back as motivational speakers
   
 
    Spread Strategies: Moving forward, what are the main strategies for scaling impact?   
 
    PACE will scale up through a setting up branches in Kisumu and Mombasa where Kenya&#039;s population is concentrated. These branches will serve as regional volunteer hubs, retain core elements and allow adjustments to the model to improve regional success e.g. altering contents of volunteer training to fit the local context.
We will create placement and high quality work experience opportunities for 10000 youth in 10 years, who will be seeds of changemaking &amp; will directly reach 450,000 younger students. These young people will have tangible impact, develop problem solving skills and be job-ready   
 Sustainability   
    Financial Sustainability Plan: What is this solution’s plan to ensure financial sustainability?   
 
    We will get sustainable through donations from individuals and corporations, engaging in profitable partnerships e.g. entering job creation contracts with local govt. &amp; leveraging the skills the volunteers develop within their 6-month period of service to public schools. After the 6 months, we will offer volunteers an opportunity to earn a shared-income through paid peer tutoring that targets families from middle and high-income environments.    
 
    Marketplace: Who else is addressing the problem outlined here? How does the proposed project differ from these approaches?   
 
    15 churches serve the 400,000 graduates who complete high school every year current with mostly religious education programming and 26 colleges that offer diplomas at a cost of about $USD 500, most of them located in the city of Nairobi. Our program has the distinct advantage of being free for volunteers to join, offering them an opportunity to earn work experience while making a difference. We are currently the first volunteer program that offers volunteers both the learning and impact benefits, along with certification through placement in a high-needs, problem-rich school environment.   
 Team   
    Founding Story   
 
    At 22, I was volunteering in a school rural Tanzania as a HIV/AIDS educator when I realized that the teachers in the school were much younger than me. The headteacher told me that when govt. teachers were posted to her school, they would leave because the area was full of hardships and lacked public transportation. She hired her graduating form 4s to fill the gap left by the teachers. It hit me that there was a way to use this method to ensure personal development and skill building for young people, reward them for service and recognize them through certificates and recommendation letters. I wrote the idea down, and began sharing with peers on Facebook, and overtime, we built the PACE program.   
 
    Team   
 
    Our team has two full-time people, our executive director &amp; program administrator. Our two part-time members support the accounting and resource mobilization dockets. Our current board is a working board, with advisors like Mrs. Muchiri and Mr. Oluoch,both teachers. We will transition over the coming 2 years into a board with higher net worth individuals and people with influence in order to attract the resources &amp; investments needed to scale.   
 About You About You First Name Peggy 
 Last Name Mativo 
 Twitter URL Facebook URL About Your Project Organization Name How long has your organization been operating? Project Organization Country , NA, Nairobi 
 Country where this project is creating social impact , NA, Nairobi 
 What awards or honors has the project received? Funding: How is your project financial supported? Friends and family, Individuals, NGOs, Businesses, Customers. Supplemental Awards: What awards or honors has your solution received, if any?  Clinton Global Initiative University- Education Commitment of the Year 2012 
Dell Social Innovation Challenge- Promising Project 2013 
Harvard Graduate School of Education- Bridge Pitch Competition Winner 2014 
Transform Kenya Awards- Finalist 
 Tell us about your partnerships 1) Nation Media Group- Nation provides newspapers as reading materials, we provide volunteers. Together we train young people on leading reading interventions. Our volunteers run reading clubs and make them fun for the students. This is a win-win partnership, meeting both the financial bottom-line for Nation, and providing alternative resources for students in our schools to learn from.  
 2) Akili Dada-  Akili Dada connects PACE with high-achieving young African women from underprivileged backgrounds who are passionate about social change and have recently graduated from high school. During their post-high school gap year, PACE provides diverse training and skills-building opportunities for these young leaders, as well as a platform for them to have impact on younger students. We help track and monitor the impact that the students have, so that in the end, they have documented evidence of the change they were responsible for. This is a win-win partnership because we&#039;re able to extend and amplify Akili Dada&#039;s investment in the education sector, while providing a tangible benefit to the students in the program.  
 3) 8 Public Primary Schools in Nairobi- Public schools in Kenya are facing a shortage of 70000 teacher. PACE partners with these schools as implementation sites to alleviate the burden of being understaffed. After we train volunteers on how to be effective as teaching assistants, these schools provide an enabling environment for the teaching assistants to act as change-makers. As the teaching assistants serve in class, the academic performance of the students improves. So does the overall discipline of the students. The teaching assistants benefit by having tangible work experience for the 6 months we place them within the schools. 
 Challenges: What challenges might hinder the success of your project and how do you plan to overcome them? Two key challenges face our project that might dampen our success. First, we need to develop stronger networks through which we can access expertise and share ideas beyond what is immediately available within our team. Secondly, in terms of capacity building, our team needs to grow in terms of resource mobilization so as strengthen the resources backing our work. To address these two challenges, we&#039;re reaching out to other peer organizations and using their platforms to pollinate our ideas and attract mentors as well as build our credibility as an organization. We&#039;re also developing our own internal storytelling capacity so as to be able to more effectively attract resources and share the impact that our volunteers experience. 
 How does your idea help young people create bright futures and improve opportunities for meaningful and long-term careers?  In Kenya, very few employers are willing to take the risk and employ pre-university students fresh out of high school because they lack work experience. PACE provides a platform where young people can gain work experience, skills and credibility in a very professional context before they enter the job market. We also increase the confidence that young people have in their own abilities as doers. They benefit from the mindset shift where they begin to see themselves as active change-makers, rather than passive beneficiaries. We believe that to change Africa&#039;s future, you must change the mindset of African youth.  
 Our pre-field trainings are focused on imparting skills in classroom management and planning, giving feedback to children and using peer influence to positively affect student behavior in the classroom.Each of our volunteers does a teach-back session which help strengthen public speaking and presentation skills after which they receive feedback from other volunteers and facilitators. Our pre-field trainings also focus on mentoring and tutoring skills- focusing on areas such as metacognition and scaffolding.  
 Our in-service learning components involve weeklong assignments that emphasize problem solving(identification, analysis, brainstorming and action planning),  communication skills (verbal and non-verbal), decision making skills &amp; teamwork (through team teaching in classrooms as well as working in teams to lead reading clubs and other extra curricular activities), job-readiness skills (we go through the mechanics of resume writing, interviewing and opportunity identification), entrepreneurship skills, and first aid skills Volunteers really develop their planning, prioritizing and organizing skills during because they have to plan and account for their time in the schools as well as the value they provide to the students.  
 At the end of their service experience, through the timecards and weekly reports that volunteers submit as part of the service documentation, they are each able to account for their time and impact within the schools. This gives them even more credibility because they are able to demonstrate tangible, measurable results from their experience. The PACE certificates and recommendation letters work as a physical evidence for employers and universities that are looking to admit young people who have solid work ethic, strong leadership and communication skills and think outside the box when it comes to addressing the complex problems in our day-to-day lives in Kenya and Africa. 
 Target Age Group(s): What age group(s) do(es) your solution target through it's programming? 13-17, 18-35. Intervention Focus: Identify which of the following best explain key parts of your solution Job Creation, Job Search, Education - Curricular, Education - Extracurricular, Professional Development, Entrepreneurship, Training/Skill Development, Online Learning. Do you have separate programs or initiatives that target the following types of populations? Boys/Men, Girls/Women. Impact - Reach: How many people did your project directly engage in programmatic activities in the last year? 1,001 to 10,000 Number of People Directly Employed by Your Organization Fewer than 10 Number of Volunteers 101-250 Does your project utilize any of the innovative design principles below? Select a theme below that most applies to your work. If none of them do, no problem, you can skip this question. Create New Industries: recognize opportunities in the marketplace to build new professions or new markets Is your project targeted at solving any of the following key barriers? Select a theme below that most applies to your work. If none of them do, no problem, you can skip this question. Education isn't preparing young people for the evolving job market   
 Log in  to post comments Printer-friendly version Download PDF 
 Give Feedback 
 Your insights will be sent privately to this project to help them improve. Rate This Project 
 Overall  * 
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Innovation  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Impact  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Sustainability  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Write a Private Feedback  * 
 Strength Need Improvement   
 Clarity of model 
   
 Financial Sustainability 
   
 Idea Originality 
   
 Impact Measurement 
   
 Impact Potential 
   
 Partnerships 
   
 Scalability 
   
 Team 
   
 Understanding of Marketplace 
   
   
   
   
   
   
 © 2016-2017 Ashoka Changemakers. 
 Terms  &amp;  Privacy Policy  |  Feedback 
   
 What We Do 
 What You Can Do Here 
 Our Team 
 Careers 
 FAQs 
 Partner With Us 
 Contact Us 
   
   
   
   
PACEMaker International | Changemakers
===============================

 
  Preloader  
  <div id="preloader">
	<div id="status"> </div>
</div>  
 Skip to main content 
 About 
 Challenges 
 Projects 
 Learning 
 Blog 
   
   
 Changemakers 
 English Español Português Français 
   
 Search form 
 Search  
   
 Log in   
 FAQs 
   
 You are here Home 
 PACEMaker International 
   Future Forward: Youth Innovations for Employment in Africa     
   
   Congratulations! This Entry has been selected as a finalist.     
   
   
   
 PACEMaker International: Youth In Action are questioning youth inaction in Kenya&#039;s education system. 
   Nairobi, Kenya Nairobi, Kenya   Peggy Mativo <div class="founder">Founder</div> 
   
   
    Year Founded:
   
 
    2013   
 Organization type:  nonprofit/ngo/citizen sector   
    Project Stage:
   
 
    Growth   
 Budget:  $10,000 - $50,000   
    Website:
   
 http://www.pacemakerinternational.org   
 Twitter:  https://twitter.com/Pacekenya Facebook:  https://www.facebook.com/PacePromotingAccessToCommunityEducationKenya 
 Corporate social responsibility   
 Education   
 Youth development   
 Youth leadership   
 Project Summary Elevator Pitch   
    Concise Summary: Help us pitch this solution! Provide an explanation within 3-4 short sentences.   
 PACE places Kenyan youth as teaching assistants in understaffed public schools for 6 month periods. We offer training in employment relevant skills coupled with an opportunity for youth to deeply impact the lives of younger students in low-income communities. We turn youth into active changemakers. 
 
    WHAT IF - Inspiration: Write one sentence that describes a way that your project dares to ask, &quot;WHAT IF?&quot;   
 
    What if young people could gain skills while ending the shortage of teachers in Kenya?    
 About Project   
    Problem: What problem is this project trying to address?   
 
    PACE seeks to unlock an additional 216 million hours of educational support for students from slums and rural areas. We target two key problems in Kenya: the shortage of 70,000+ teachers which disproportionately affects public schools and the lack of structured work opportunities for the 400,000 Kenyans who graduate from high school annually and college students on long vacations. We reposition these problems as solutions to each other.    
 
    Solution: What is the proposed solution? Please be specific!   
 
    We recruit, train and send youthful volunteers into understaffed schools as teaching assistants where they help fill the human resource gap in public schools. By improving the learning outcomes of our students today, we prepare them to escape poverty tomorrow. As the youthful teaching assistants serve in schools, they gain invaluable work experience and begin to see themselves as leaders who can make a difference in difficult, complex everyday situations. 
As a motivator for service, we uniquely couple in-school service with training so as to build specific skills that young people need for employment, but do not get from the current school system: leadership, first aid, teamwork, problem solving, job-readiness and communication skills. 
   
 Impact: How does it Work   
    Example: Walk us through a specific example(s) of how this solution makes a difference; include its primary activities.   
 
    Eneki, 18 years,just graduated from high school.She joins PACE &amp; goes through training on how to support learning for young students. She makes new friends who are passionate about making a difference and begins to work through her nervousness about public speaking. In her first week, Eneki is placed in a classroom with 45 students. She forms a special connection with Beryl, an intelligent class 8 student who is struggling to with English and needs a big sister to guide her. Eneki and Beryl meet for tutoring &amp; mentoring during lunch and tea breaks and over time, Beryl&#039;s performance improves. Eneki realizes that the school has a weak reading culture. She decides solve the problem &amp; initiate a fun weekly reading club for Beryl &amp; her friends.    
 
    Impact: What is the impact of the work to date? Also describe the projected future impact for the coming years.   
 
    PACE volunteers have worked in public schools for 20000 hours. This translates to Kenyan youth donating time that is valued at approximately 2.5 million shillings since January 2013.
165 youth have gained work experience and employment-relevant training and skills through the PACE volunteer program. 
5600 students have benefited from additional grading, tutoring and mentoring led by volunteer teaching assistants. Evidence is seen through increase in mean scores e.g. North Highridge Primary KCSE national exam mean moved up by 26 points last year, and Farasi Lane English mean doubled over the past year. 
After graduation, PACE volunteers have chosen to go into fulltime teaching as a profession, to start businesses that supply schools with exam prep materials, to earn incomes as paid tutors, to author books for primary school students &amp; to continue giving back as motivational speakers
   
 
    Spread Strategies: Moving forward, what are the main strategies for scaling impact?   
 
    PACE will scale up through a setting up branches in Kisumu and Mombasa where Kenya&#039;s population is concentrated. These branches will serve as regional volunteer hubs, retain core elements and allow adjustments to the model to improve regional success e.g. altering contents of volunteer training to fit the local context.
We will create placement and high quality work experience opportunities for 10000 youth in 10 years, who will be seeds of changemaking &amp; will directly reach 450,000 younger students. These young people will have tangible impact, develop problem solving skills and be job-ready   
 Sustainability   
    Financial Sustainability Plan: What is this solution’s plan to ensure financial sustainability?   
 
    We will get sustainable through donations from individuals and corporations, engaging in profitable partnerships e.g. entering job creation contracts with local govt. &amp; leveraging the skills the volunteers develop within their 6-month period of service to public schools. After the 6 months, we will offer volunteers an opportunity to earn a shared-income through paid peer tutoring that targets families from middle and high-income environments.    
 
    Marketplace: Who else is addressing the problem outlined here? How does the proposed project differ from these approaches?   
 
    15 churches serve the 400,000 graduates who complete high school every year current with mostly religious education programming and 26 colleges that offer diplomas at a cost of about $USD 500, most of them located in the city of Nairobi. Our program has the distinct advantage of being free for volunteers to join, offering them an opportunity to earn work experience while making a difference. We are currently the first volunteer program that offers volunteers both the learning and impact benefits, along with certification through placement in a high-needs, problem-rich school environment.   
 Team   
    Founding Story   
 
    At 22, I was volunteering in a school rural Tanzania as a HIV/AIDS educator when I realized that the teachers in the school were much younger than me. The headteacher told me that when govt. teachers were posted to her school, they would leave because the area was full of hardships and lacked public transportation. She hired her graduating form 4s to fill the gap left by the teachers. It hit me that there was a way to use this method to ensure personal development and skill building for young people, reward them for service and recognize them through certificates and recommendation letters. I wrote the idea down, and began sharing with peers on Facebook, and overtime, we built the PACE program.   
 
    Team   
 
    Our team has two full-time people, our executive director &amp; program administrator. Our two part-time members support the accounting and resource mobilization dockets. Our current board is a working board, with advisors like Mrs. Muchiri and Mr. Oluoch,both teachers. We will transition over the coming 2 years into a board with higher net worth individuals and people with influence in order to attract the resources &amp; investments needed to scale.   
 About You About You First Name Peggy 
 Last Name Mativo 
 Twitter URL Facebook URL About Your Project Organization Name How long has your organization been operating? Project Organization Country , NA, Nairobi 
 Country where this project is creating social impact , NA, Nairobi 
 What awards or honors has the project received? Funding: How is your project financial supported? Friends and family, Individuals, NGOs, Businesses, Customers. Supplemental Awards: What awards or honors has your solution received, if any?  Clinton Global Initiative University- Education Commitment of the Year 2012 
Dell Social Innovation Challenge- Promising Project 2013 
Harvard Graduate School of Education- Bridge Pitch Competition Winner 2014 
Transform Kenya Awards- Finalist 
 Tell us about your partnerships 1) Nation Media Group- Nation provides newspapers as reading materials, we provide volunteers. Together we train young people on leading reading interventions. Our volunteers run reading clubs and make them fun for the students. This is a win-win partnership, meeting both the financial bottom-line for Nation, and providing alternative resources for students in our schools to learn from.  
 2) Akili Dada-  Akili Dada connects PACE with high-achieving young African women from underprivileged backgrounds who are passionate about social change and have recently graduated from high school. During their post-high school gap year, PACE provides diverse training and skills-building opportunities for these young leaders, as well as a platform for them to have impact on younger students. We help track and monitor the impact that the students have, so that in the end, they have documented evidence of the change they were responsible for. This is a win-win partnership because we&#039;re able to extend and amplify Akili Dada&#039;s investment in the education sector, while providing a tangible benefit to the students in the program.  
 3) 8 Public Primary Schools in Nairobi- Public schools in Kenya are facing a shortage of 70000 teacher. PACE partners with these schools as implementation sites to alleviate the burden of being understaffed. After we train volunteers on how to be effective as teaching assistants, these schools provide an enabling environment for the teaching assistants to act as change-makers. As the teaching assistants serve in class, the academic performance of the students improves. So does the overall discipline of the students. The teaching assistants benefit by having tangible work experience for the 6 months we place them within the schools. 
 Challenges: What challenges might hinder the success of your project and how do you plan to overcome them? Two key challenges face our project that might dampen our success. First, we need to develop stronger networks through which we can access expertise and share ideas beyond what is immediately available within our team. Secondly, in terms of capacity building, our team needs to grow in terms of resource mobilization so as strengthen the resources backing our work. To address these two challenges, we&#039;re reaching out to other peer organizations and using their platforms to pollinate our ideas and attract mentors as well as build our credibility as an organization. We&#039;re also developing our own internal storytelling capacity so as to be able to more effectively attract resources and share the impact that our volunteers experience. 
 How does your idea help young people create bright futures and improve opportunities for meaningful and long-term careers?  In Kenya, very few employers are willing to take the risk and employ pre-university students fresh out of high school because they lack work experience. PACE provides a platform where young people can gain work experience, skills and credibility in a very professional context before they enter the job market. We also increase the confidence that young people have in their own abilities as doers. They benefit from the mindset shift where they begin to see themselves as active change-makers, rather than passive beneficiaries. We believe that to change Africa&#039;s future, you must change the mindset of African youth.  
 Our pre-field trainings are focused on imparting skills in classroom management and planning, giving feedback to children and using peer influence to positively affect student behavior in the classroom.Each of our volunteers does a teach-back session which help strengthen public speaking and presentation skills after which they receive feedback from other volunteers and facilitators. Our pre-field trainings also focus on mentoring and tutoring skills- focusing on areas such as metacognition and scaffolding.  
 Our in-service learning components involve weeklong assignments that emphasize problem solving(identification, analysis, brainstorming and action planning),  communication skills (verbal and non-verbal), decision making skills &amp; teamwork (through team teaching in classrooms as well as working in teams to lead reading clubs and other extra curricular activities), job-readiness skills (we go through the mechanics of resume writing, interviewing and opportunity identification), entrepreneurship skills, and first aid skills Volunteers really develop their planning, prioritizing and organizing skills during because they have to plan and account for their time in the schools as well as the value they provide to the students.  
 At the end of their service experience, through the timecards and weekly reports that volunteers submit as part of the service documentation, they are each able to account for their time and impact within the schools. This gives them even more credibility because they are able to demonstrate tangible, measurable results from their experience. The PACE certificates and recommendation letters work as a physical evidence for employers and universities that are looking to admit young people who have solid work ethic, strong leadership and communication skills and think outside the box when it comes to addressing the complex problems in our day-to-day lives in Kenya and Africa. 
 Target Age Group(s): What age group(s) do(es) your solution target through it's programming? 13-17, 18-35. Intervention Focus: Identify which of the following best explain key parts of your solution Job Creation, Job Search, Education - Curricular, Education - Extracurricular, Professional Development, Entrepreneurship, Training/Skill Development, Online Learning. Do you have separate programs or initiatives that target the following types of populations? Boys/Men, Girls/Women. Impact - Reach: How many people did your project directly engage in programmatic activities in the last year? 1,001 to 10,000 Number of People Directly Employed by Your Organization Fewer than 10 Number of Volunteers 101-250 Does your project utilize any of the innovative design principles below? Select a theme below that most applies to your work. If none of them do, no problem, you can skip this question. Create New Industries: recognize opportunities in the marketplace to build new professions or new markets Is your project targeted at solving any of the following key barriers? Select a theme below that most applies to your work. If none of them do, no problem, you can skip this question. Education isn't preparing young people for the evolving job market   
 Log in  to post comments Printer-friendly version Download PDF 
 Give Feedback 
 Your insights will be sent privately to this project to help them improve. Rate This Project 
 Overall  * 
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Innovation  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Impact  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Sustainability  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Write a Private Feedback  * 
 Strength Need Improvement   
 Clarity of model 
   
 Financial Sustainability 
   
 Idea Originality 
   
 Impact Measurement 
   
 Impact Potential 
   
 Partnerships 
   
 Scalability 
   
 Team 
   
 Understanding of Marketplace 
   
   
   
   
   
   
 © 2016-2017 Ashoka Changemakers. 
 Terms  &amp;  Privacy Policy  |  Feedback 
   
 What We Do 
 What You Can Do Here 
 Our Team 
 Careers 
 FAQs 
 Partner With Us 
 Contact Us
 WHOLE PROJECT MARK