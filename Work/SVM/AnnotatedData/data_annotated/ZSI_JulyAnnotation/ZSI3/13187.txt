Ghana Venskab: School for Life
===============================

 
 DK 
  Brand and toggle get grouped for better mobile display  
 Toggle navigation VIS MENU 
 About Funding Activities Program Empowerment for Life programme CBO CLIP School for Life Background and history Danish School for Life Committee GDCP ACE Info Contact 
 Empowerment for Life programme CBO GDCP CLIP School for Life 
   
     
   
 Empowerment for Life programme CBO CLIP School for Life Background and history Danish School for Life Committee GDCP ACE 
 School for Life -a success story 
 Functional Literacy Programme for Children 
 The surplus of the Christmas calendar for development aid in 1994 started School for Life in 1995. After four phases as a single project SfL has now become one of the implementing organisations under the Empowerment for Life programme.   
 During the first phases the primary objective of SfL was to give children aged 8 to 14 years, who had missed the opportunity to go to the formal school an opportunity to get education through SfL courses. Later strengthening civil society, improving the formal school, replication and advocacy have become objects in focus. During the last two years SfL has focussed on advocating for GES implementing the SfL Complementary Basic Education approach, so that all children will go to school.   
 &quot;A role model for future changes in the primary schools of  Ghana .&quot;  That is how the Ghana Ministry of Education has described the Danish NGO supported program in Northern Ghana called School for Life. The program not only manages to reach out to several thousands of the many children, who are not in school because their parents want them to work instead. In School for Life the children are taught for only three hours every afternoon after they have finished working. Furthermore the children are taught in their mother tongue and not English which makes it possible for them to learn to read and write very quickly: One third are able to read fluently after the nine months that School for Life lasts. The graduates can then enter the formal school system. 
 You can read the 2007 Impact Assessment  The Leap to Literacy  here.   
 School for Life has given the children a new option. 8 to 14 year-old children go to school in the afternoon, so that they can help their parents in the morning. They learn to read, write and calculate in their mother tongue. 80% of them continue in the formal school P2, P3 or P4. This also means that the parents attitude to going to school is changed, so that they now see the advantage of sending their children to school.   
 After 17 years activity more than 150, 000 children have gone to school. A US supported NGO has replicated the SfL approach and has added 4000 children every year. Since 2009 a British organisation DFID gives funding to the activity monitored by SfL.   
 SfL uses voluntary untrained teachers. They are supported for a teachers’ training later and in the Alliance for Change in Education there is cooperation with GES to use these teachers in the small classes as teachers of the mother tongue.   
 In connection with the Empowerment for Life programme school for Life has become a Learning and Development centre for basic education, which can offer consultation to both authorities and private organisations.  A few districts will be kept for demonstration purposes.   
 Head Office of School for Life 
 P.O.Box 787, Tamale, Ghana Tel: +233 372022023 Fax: +233 372023815                        Homepage  
 e-mail:  sfl_(at)_4u.com.gh        www.schoolforlifegh.org   
 Situated at Naa Luro Estate, House no. 60 Choggu block VI, near NORBISCO  
 See and listen to the film below .  English speaking and Danish subtitles. 
 GHANA VENSKAB  - KLOSTERPORT 4C 3. - DK-8000 ÅRHUS C - TEL 61 41 71 52 -  INFO@GHANAVENSKABSGRUPPERNE.DK 
Ghana Venskab: School for Life
===============================

 
 DK 
  Brand and toggle get grouped for better mobile display  
 Toggle navigation VIS MENU 
 About Funding Activities Program Empowerment for Life programme CBO CLIP School for Life Background and history Danish School for Life Committee GDCP ACE Info Contact 
 Empowerment for Life programme CBO GDCP CLIP School for Life 
   
     
   
 Empowerment for Life programme CBO CLIP School for Life Background and history Danish School for Life Committee GDCP ACE 
 School for Life -a success story 
 Functional Literacy Programme for Children 
 The surplus of the Christmas calendar for development aid in 1994 started School for Life in 1995. After four phases as a single project SfL has now become one of the implementing organisations under the Empowerment for Life programme.   
 During the first phases the primary objective of SfL was to give children aged 8 to 14 years, who had missed the opportunity to go to the formal school an opportunity to get education through SfL courses. Later strengthening civil society, improving the formal school, replication and advocacy have become objects in focus. During the last two years SfL has focussed on advocating for GES implementing the SfL Complementary Basic Education approach, so that all children will go to school.   
 &quot;A role model for future changes in the primary schools of  Ghana .&quot;  That is how the Ghana Ministry of Education has described the Danish NGO supported program in Northern Ghana called School for Life. The program not only manages to reach out to several thousands of the many children, who are not in school because their parents want them to work instead. In School for Life the children are taught for only three hours every afternoon after they have finished working. Furthermore the children are taught in their mother tongue and not English which makes it possible for them to learn to read and write very quickly: One third are able to read fluently after the nine months that School for Life lasts. The graduates can then enter the formal school system. 
 You can read the 2007 Impact Assessment  The Leap to Literacy  here.   
 School for Life has given the children a new option. 8 to 14 year-old children go to school in the afternoon, so that they can help their parents in the morning. They learn to read, write and calculate in their mother tongue. 80% of them continue in the formal school P2, P3 or P4. This also means that the parents attitude to going to school is changed, so that they now see the advantage of sending their children to school.   
 After 17 years activity more than 150, 000 children have gone to school. A US supported NGO has replicated the SfL approach and has added 4000 children every year. Since 2009 a British organisation DFID gives funding to the activity monitored by SfL.   
 SfL uses voluntary untrained teachers. They are supported for a teachers’ training later and in the Alliance for Change in Education there is cooperation with GES to use these teachers in the small classes as teachers of the mother tongue.   
 In connection with the Empowerment for Life programme school for Life has become a Learning and Development centre for basic education, which can offer consultation to both authorities and private organisations.  A few districts will be kept for demonstration purposes.   
 Head Office of School for Life 
 P.O.Box 787, Tamale, Ghana Tel: +233 372022023 Fax: +233 372023815                        Homepage  
 e-mail:  sfl_(at)_4u.com.gh        www.schoolforlifegh.org   
 Situated at Naa Luro Estate, House no. 60 Choggu block VI, near NORBISCO  
 See and listen to the film below .  English speaking and Danish subtitles. 
 GHANA VENSKAB  - KLOSTERPORT 4C 3. - DK-8000 ÅRHUS C - TEL 61 41 71 52 -  INFO@GHANAVENSKABSGRUPPERNE.DK
 WHOLE PROJECT MARK