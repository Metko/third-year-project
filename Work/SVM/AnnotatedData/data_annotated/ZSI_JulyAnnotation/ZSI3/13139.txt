Green Living Movement
===============================

 
 
        Menu
     
 Home 
 Our Work 
 Organisation 
 Take Action 
 Donate 
 News 
 Contact 
 English 
 suomi 
   
 Green Living Movement Zambia 
 GLM Zambia supports sustainable use of natural resources and community livelihoods in rural Zambia. GLM Zambia&#8217;s work focuses on sustainable agriculture, environmental awareness, capacity building, empowerment and advocacy. 
 Learn more 
 Green Living Movement Swaziland 
 GLM Swaziland promotes sustainable community development to reduce poverty and to protect the environment. GLM Swaziland utilises the participatory working methods developed in Zambia. 
 Learn more 
 Green Living Movement Finland 
 GLM Finland is a volunteer-based group which supports and strengthens the work of GLM Zambia and Swaziland at the grassroots level by assisting in communication, fundraising and project planning. 
 Learn more 
 Recent news 
 Cooperation with a board game Norsun muisti (Memory Of The Elephant) 
 19/12/2017 
 GLM Finland and GLM Zambia have launched a cooperation with a finnish board game Norsun muisti (Memory Of The Elephant). [&hellip;] 
 Web pages published again 
 19/12/2017 
 Due to some unfortunate technical challenges our web pages were down for some time. Now the pages are published again [&hellip;] 
 
						Read all news						
					 
 Partners 
 Civil Society Environment Fund 
 Edinburgh Global Partnerships 
 Huussi &#8211; Global Dry Toilet Association of Finland 
 Super Analytics 
 KEPA Finnish Centre for Development Cooperation 
 ETVO Finnish Volunteer Programme 
 Networks 
 Civil Society Biofuel Forum 
 PELUM Zambia 
 The Earth Charter Initiative 
 Zambia Climate Change Network 
 Zambia Land Alliance 
 Social 
 Facebook 
 Flickr 
 Green Living Movement &copy; 2012. All rights Reserved. 
Green Living Movement
===============================

 
 
        Menu
     
 Home 
 Our Work 
 Organisation 
 Take Action 
 Donate 
 News 
 Contact 
 English 
 suomi 
   
 Green Living Movement Zambia 
 GLM Zambia supports sustainable use of natural resources and community livelihoods in rural Zambia. GLM Zambia&#8217;s work focuses on sustainable agriculture, environmental awareness, capacity building, empowerment and advocacy. 
 Learn more 
 Green Living Movement Swaziland 
 GLM Swaziland promotes sustainable community development to reduce poverty and to protect the environment. GLM Swaziland utilises the participatory working methods developed in Zambia. 
 Learn more 
 Green Living Movement Finland 
 GLM Finland is a volunteer-based group which supports and strengthens the work of GLM Zambia and Swaziland at the grassroots level by assisting in communication, fundraising and project planning. 
 Learn more 
 Recent news 
 Cooperation with a board game Norsun muisti (Memory Of The Elephant) 
 19/12/2017 
 GLM Finland and GLM Zambia have launched a cooperation with a finnish board game Norsun muisti (Memory Of The Elephant). [&hellip;] 
 Web pages published again 
 19/12/2017 
 Due to some unfortunate technical challenges our web pages were down for some time. Now the pages are published again [&hellip;] 
 
						Read all news						
					 
 Partners 
 Civil Society Environment Fund 
 Edinburgh Global Partnerships 
 Huussi &#8211; Global Dry Toilet Association of Finland 
 Super Analytics 
 KEPA Finnish Centre for Development Cooperation 
 ETVO Finnish Volunteer Programme 
 Networks 
 Civil Society Biofuel Forum 
 PELUM Zambia 
 The Earth Charter Initiative 
 Zambia Climate Change Network 
 Zambia Land Alliance 
 Social 
 Facebook 
 Flickr 
 Green Living Movement &copy; 2012. All rights Reserved.
 WHOLE PROJECT MARK