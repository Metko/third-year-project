Kavar Basin Rural Development Project | Hüsnü M. Özyeğin Vakfı
===============================

 
 Türkçe English 
   
    .logo / 
 Home Page 
 About Us 
 Hüsnü M. Özyeğin Vakfı 
 Organizaton 
 Hüsnü M. Özyeğin 
 Institutions 
 Education 
 Okullar 
 Yurtlar 
 Sağlık 
 Kültür 
 Scholarships 
 Yurtiçi Burslar 
 Yurtdışı Burslar 
 Programs 
 Support Programs for Grantees 
 Genç Kızlar Programı 
 Genç Kızlar Güçleniyor Eğitim Programı 
 Bilim Kahramanları Projesi 
 Genç Fikirler Projesi 
 Kırsal Kalkınma Programı 
 Rural Development Model 
 Kapasite Geliştirme Çalışmaları 
 Implementation Areas 
 Kavar 
 Ravanda 
 Eğil 
 Program Goals 
 Saha Ofisleri 
 News 
 Contact Us 
   Go to... Home Page About Us  - Hüsnü M. Özyeğin Vakfı  - Organizaton  - Hüsnü M. Özyeğin Institutions  - Education  - Okullar  - Yurtlar  - Sağlık  - Kültür Scholarships  - Yurtiçi Burslar  - Yurtdışı Burslar Programs  - Support Programs for Grantees  - Genç Kızlar Programı  - Genç Kızlar Güçleniyor Eğitim Programı  - Bilim Kahramanları Projesi  - Genç Fikirler Projesi  - Kırsal Kalkınma Programı  - Rural Development Model  - Kapasite Geliştirme Çalışmaları  - Implementation Areas  - Kavar  - Ravanda  - Eğil  - Program Goals  - Saha Ofisleri News Contact Us    .main-nav / 
  #header / 
 Kavar Basin Rural Development Project 
 Kavar Project was launched in November 2008 in Tatvan, located in southeastern Bitlis Province, which is among the most impoverished areas of Turkey according to socio-economic indicators. The project area includes 6 villages, Kolbaşı, Yassıca, Düzcealan, Tokaçlı, Dibekli, Bolalan, and 5 hamlets. The results of the baseline household surveys conducted in 2008, shows that Kavar Basin has a total population of 1,800, and average number of people per household is 8.3 while median monthly income per person is 82 TL. According to TUİK statistics, hunger threshold in the country is 80TL. 
 Economic Infrastructure Social Women&#8217;s Empowerment Environment Partnerships 
 Agriculture and Animal Husbandry 
 A total of 116 walnut orchards, 4 cherry orchards, and 5 apple orchards have been established. All fruit producers have received training and technical support. All existing and newly planted trees are routinely checked, disinfected and maintained. Local experts have been trained in tree pruning. Tree nurseries have been set up. Leaf and soil analyses have been conducted. Drip irrigation has been introduced to the farmers. An artificial pond has been created to irrigate 3 orchards. For the first time in Kavar, 13 farmers have adopted the low-tunnel system to grow green beans. 27 farmers have planted maize for silage in 163 square meters of land. 
 Farmers, whose main income comes from animal husbandry, have received extensive training on cattle farming including important topics such as reproduction, animal diseases, barn hygiene and record keeping. 14 Kavar farmers attended a comprehensive training program at Sütaş’s Karacabey cattle and milk production facilities in Bursa. Artificial insemination is performed to encourage transition to hybrid livestock. Cow magnets have been used to reduce and prevent animal diseases. Water ducts have been built on grazing land for animals to have access to drinking water. Stocked medicine cabinets have been installed in barns to effectively combat animal diseases and emergencies. Each barn in the project villages have been disinfected against external parasites. 21 barns have been renovated and modernized. Automatic water fountains have been established. In April 2010, a modern, two -ton capacity, Milk Collection Center was built. The Center is operated by the Kavar Agricultural Development Cooperative.
		 
 A modern communal bread oven has been built to alleviate the workload of women and prevent smoke-induced poisonings. A Community Center has been built with funding from the Japanese Embassy in 2012 and opened to the use of people in the basin. The Center is used for Cooperative meetings, trainings and various workshops for children and young people. 5 village rooms— social spaces for villagers to come together—have been built in 5 project villages. The roads and sewage systems have been renovated and repaired. Two playgrounds and two kindergartens have been established. 
 Kavar project includes sub-projects and activities solely dedicated to the social welfare and health of children and youth. Families have received free general health exams while students received free dental exams and health kits. A much-needed, 10-classroom elementary school, Deniz Süren Elementary School, was built to increase school enrolment in Kavar. The school meets its enegy needs partially through the solar panels installed on its roof, which makes it an environmentally friendly building. The school has a high technology classroom, which enables students from Kavar to connect with Özyegin University and participate in distance learning. 
 A teacher’s dormitory has been built in Kavar. Village primary schools have been supported through donations of learning materials. 75 students have been registered for distance learning programs to increase school enrolment rates. 9 successful university students in Kavar receive scholarships to be able to continue their education. Reading rooms and libraries have been set up for young people. University preparation courses and computer literacy training have been organized. AÇEV’s “Father Support Program&#8221; has been implemented for fathers and father candidates in the project villages. “Empowering Young Girls” is a program designed specifically to address issues that young girls face in their everyday lives. 
 A children&#8217;s choir has been created in the Paşaelmalı hamlet. The choir took the stage at Özyeğin University on 26th of January, 2012 and gave a joint concert with the famous band Kardeş Türküler.  Please  click here  to watch the concert video. 
 Students from Tokaçli village in Kavar have become “pen pals” with children from the low-income Uptown neighborhood of Chicago through a joint project titled “Bridge: Chicago-Kavar Pen Pal Project.” The children exchange letters to form cross-cultural friendships. Moreover, the photographs taken during the project have been turned into an art exhibition. Please click the links below to download “Bridge:  Chicago-Kavar Pen Pal Project” e-book as a PDF document:  Part I ,  Part II .  Here is the link  to the short film about the project. 
 Over the course of the 5-year Kavar Project, children have participated in art, music and theater classes. A toy library has been established in one of the villages. Children have received training on human rights and democracy. In cooperation with ACEV, an early childhood education module called “Learning With Fun” has been implemented in project villages. Kavar Spring Festival has become an annual event where the whole Kavar community comes together to celebrate local art and culture. “The calf beauty pageant” as well as the concerts, children’s art workshops, and dancing have become indispensable components of the festival.
		 
 Women Beekeepers Project:  The pilot bee-keeping project, which started in 2011 with only 3 brave women, has reached the capacity to provide a stable income for 40 women in Kavar as of 2014. The women have received training as well as all the necessary equipment, and have been producing honey under the constant supervision of a full-time bee keeping expert. As the second most important source of income for Kavar after cattle raising, the beekeeping project will continue to expand in 2014 with the participation of new farmers. 
 Decorative Flower Production:  With the establishment of two greenhouses in two villages, Kavar Women’s Decorative Flower Project has enabled 22 young women to earn an income through floricultural production. The main goal of the project is to increase rural women’s social and economic capacities by encouraging them to come together and work as a team for the management of the greenhouses and marketing of their end products locally. 
 Handicrafts Project:  50 women from Kavar have produced handmade purses, greeting cards and scarves to be sold at Marks &amp; Spencer and GAP stores in Istanbul and Ankara. 
 AÇEV Trainings:  More than 90 women have participated in AÇEV’s Rural Women’s Education Program, which includes classes on communication with children, women&#8217;s reproductive health, nutrition, and women’s rights. The classes are supported by educational video screenings. Furthermore, female literacy in the basin has increased by 25 % with the support of AÇEV adult literacy programs. 
 20 women participated in Entrepreneurship Training. 39 women attended field trips outside of their villages. 5 female farmers attended classes on dairy farming at Sütaş Karacabey Facilities. 3 women studied cooperatives at a training organized by the Ministry of Agriculture and German Cooperatives Union. 
 Environmental protection and sustainable use of natural resources is an integral part of the Kavar Project. Protection of grazing lands and pastures, trainings for local shepherds, workshops for raising environmental awareness in children, tree planting and seed festivals are some of the examples of environmental protection activities implemented to date in the project villages. 
 Özyeğin Foundation believes that sustainability of the project depends on the fact that the beneficiaries have a say in the process of formulation and implementation of project activities. It is this participatory process that ensures the ownership of the project by the local community. The Kavar Agricultural Development Cooperative, which was established after extensive community discussions, field trips to see best practices in other regions, and various trainings, is a typical successful example of this participatory approach. 
 Over the years, Kavar Cooperative has become an important actor in the socio-economic development of the whole basin. With the support of the Özyeğin Foundation, the Cooperative built a Milk Collection Center. In 2012, it became an official member of the Association of Cattle Breeders in Bitlis. It has reached the capacity to collect, store and market the milk that is collected from each household in the project villages. Thus, for the first time in Kavar history, milk producers of Kavar have started to earn a living through selling their daily produce. 
 It is the first and only Coop. in the region, which has announced its sale of milk to dairy factories through an open call for price offers. It has contributed significantly to the development of animal husbandry in the villages by purchasing wholesale fodder crops and distributing it to its members at a lower price. It has expanded its agricultural equipment with the purchase of a silage machine, a seed drill and a hoeing machine all co-financed by Özyeğin Foundation and the Ministry of Agriculture. Finally it has started to build a storage unit for the communal agricultural equipment and fodder stocks. For more information about the Kavar Cooperative please visit:  http://kavarkooperatifi.com/ 
  .entry / 
 eğil   hmov   husnu m ozyegin   hüsnü özyeğin   hüsnü özyeğin vakfı   kavar   kırsal kalkınma   kırsalda umut var   ravanda   vakıf 
 Share ! 
 tweet   
    .share-post  
  .post-inner  
  .post-listing  
 Related Articles 
  post-thumbnail / 
 Capacity Building 
 02 September 2014 
  post-thumbnail / 
 Field Offices 
 04 January 2013 
  post-thumbnail / 
 Özyeğin Foundation Rural Development Model 
 04 January 2013 
  post-thumbnail / 
 Rural Development Program Goals 
 04 January 2013 
  .content  
   
  .container / 
 
				P: +90 (216) 564 97 40 
F: +90 (216) 564 90 53
E-mail: info@hmov.org			 
 
				Özyeğin Üniversitesi Çekmeköy Kampüsü
Nişantepe Mah. Orman Sok. 34794 Çekmeköy - İSTANBUL 
All Rights Reserved to Husnu M. Ozyegin Foundation			 
  .Container  
  .Footer bottom  
  .Wrapper  
 Scroll To Top 
Kavar Basin Rural Development Project | Hüsnü M. Özyeğin Vakfı
===============================

 
 Türkçe English 
   
    .logo / 
 Home Page 
 About Us 
 Hüsnü M. Özyeğin Vakfı 
 Organizaton 
 Hüsnü M. Özyeğin 
 Institutions 
 Education 
 Okullar 
 Yurtlar 
 Sağlık 
 Kültür 
 Scholarships 
 Yurtiçi Burslar 
 Yurtdışı Burslar 
 Programs 
 Support Programs for Grantees 
 Genç Kızlar Programı 
 Genç Kızlar Güçleniyor Eğitim Programı 
 Bilim Kahramanları Projesi 
 Genç Fikirler Projesi 
 Kırsal Kalkınma Programı 
 Rural Development Model 
 Kapasite Geliştirme Çalışmaları 
 Implementation Areas 
 Kavar 
 Ravanda 
 Eğil 
 Program Goals 
 Saha Ofisleri 
 News 
 Contact Us 
   Go to... Home Page About Us  - Hüsnü M. Özyeğin Vakfı  - Organizaton  - Hüsnü M. Özyeğin Institutions  - Education  - Okullar  - Yurtlar  - Sağlık  - Kültür Scholarships  - Yurtiçi Burslar  - Yurtdışı Burslar Programs  - Support Programs for Grantees  - Genç Kızlar Programı  - Genç Kızlar Güçleniyor Eğitim Programı  - Bilim Kahramanları Projesi  - Genç Fikirler Projesi  - Kırsal Kalkınma Programı  - Rural Development Model  - Kapasite Geliştirme Çalışmaları  - Implementation Areas  - Kavar  - Ravanda  - Eğil  - Program Goals  - Saha Ofisleri News Contact Us    .main-nav / 
  #header / 
 Kavar Basin Rural Development Project 
 Kavar Project was launched in November 2008 in Tatvan, located in southeastern Bitlis Province, which is among the most impoverished areas of Turkey according to socio-economic indicators. The project area includes 6 villages, Kolbaşı, Yassıca, Düzcealan, Tokaçlı, Dibekli, Bolalan, and 5 hamlets. The results of the baseline household surveys conducted in 2008, shows that Kavar Basin has a total population of 1,800, and average number of people per household is 8.3 while median monthly income per person is 82 TL. According to TUİK statistics, hunger threshold in the country is 80TL. 
 Economic Infrastructure Social Women&#8217;s Empowerment Environment Partnerships 
 Agriculture and Animal Husbandry 
 A total of 116 walnut orchards, 4 cherry orchards, and 5 apple orchards have been established. All fruit producers have received training and technical support. All existing and newly planted trees are routinely checked, disinfected and maintained. Local experts have been trained in tree pruning. Tree nurseries have been set up. Leaf and soil analyses have been conducted. Drip irrigation has been introduced to the farmers. An artificial pond has been created to irrigate 3 orchards. For the first time in Kavar, 13 farmers have adopted the low-tunnel system to grow green beans. 27 farmers have planted maize for silage in 163 square meters of land. 
 Farmers, whose main income comes from animal husbandry, have received extensive training on cattle farming including important topics such as reproduction, animal diseases, barn hygiene and record keeping. 14 Kavar farmers attended a comprehensive training program at Sütaş’s Karacabey cattle and milk production facilities in Bursa. Artificial insemination is performed to encourage transition to hybrid livestock. Cow magnets have been used to reduce and prevent animal diseases. Water ducts have been built on grazing land for animals to have access to drinking water. Stocked medicine cabinets have been installed in barns to effectively combat animal diseases and emergencies. Each barn in the project villages have been disinfected against external parasites. 21 barns have been renovated and modernized. Automatic water fountains have been established. In April 2010, a modern, two -ton capacity, Milk Collection Center was built. The Center is operated by the Kavar Agricultural Development Cooperative.
		 
 A modern communal bread oven has been built to alleviate the workload of women and prevent smoke-induced poisonings. A Community Center has been built with funding from the Japanese Embassy in 2012 and opened to the use of people in the basin. The Center is used for Cooperative meetings, trainings and various workshops for children and young people. 5 village rooms— social spaces for villagers to come together—have been built in 5 project villages. The roads and sewage systems have been renovated and repaired. Two playgrounds and two kindergartens have been established. 
 Kavar project includes sub-projects and activities solely dedicated to the social welfare and health of children and youth. Families have received free general health exams while students received free dental exams and health kits. A much-needed, 10-classroom elementary school, Deniz Süren Elementary School, was built to increase school enrolment in Kavar. The school meets its enegy needs partially through the solar panels installed on its roof, which makes it an environmentally friendly building. The school has a high technology classroom, which enables students from Kavar to connect with Özyegin University and participate in distance learning. 
 A teacher’s dormitory has been built in Kavar. Village primary schools have been supported through donations of learning materials. 75 students have been registered for distance learning programs to increase school enrolment rates. 9 successful university students in Kavar receive scholarships to be able to continue their education. Reading rooms and libraries have been set up for young people. University preparation courses and computer literacy training have been organized. AÇEV’s “Father Support Program&#8221; has been implemented for fathers and father candidates in the project villages. “Empowering Young Girls” is a program designed specifically to address issues that young girls face in their everyday lives. 
 A children&#8217;s choir has been created in the Paşaelmalı hamlet. The choir took the stage at Özyeğin University on 26th of January, 2012 and gave a joint concert with the famous band Kardeş Türküler.  Please  click here  to watch the concert video. 
 Students from Tokaçli village in Kavar have become “pen pals” with children from the low-income Uptown neighborhood of Chicago through a joint project titled “Bridge: Chicago-Kavar Pen Pal Project.” The children exchange letters to form cross-cultural friendships. Moreover, the photographs taken during the project have been turned into an art exhibition. Please click the links below to download “Bridge:  Chicago-Kavar Pen Pal Project” e-book as a PDF document:  Part I ,  Part II .  Here is the link  to the short film about the project. 
 Over the course of the 5-year Kavar Project, children have participated in art, music and theater classes. A toy library has been established in one of the villages. Children have received training on human rights and democracy. In cooperation with ACEV, an early childhood education module called “Learning With Fun” has been implemented in project villages. Kavar Spring Festival has become an annual event where the whole Kavar community comes together to celebrate local art and culture. “The calf beauty pageant” as well as the concerts, children’s art workshops, and dancing have become indispensable components of the festival.
		 
 Women Beekeepers Project:  The pilot bee-keeping project, which started in 2011 with only 3 brave women, has reached the capacity to provide a stable income for 40 women in Kavar as of 2014. The women have received training as well as all the necessary equipment, and have been producing honey under the constant supervision of a full-time bee keeping expert. As the second most important source of income for Kavar after cattle raising, the beekeeping project will continue to expand in 2014 with the participation of new farmers. 
 Decorative Flower Production:  With the establishment of two greenhouses in two villages, Kavar Women’s Decorative Flower Project has enabled 22 young women to earn an income through floricultural production. The main goal of the project is to increase rural women’s social and economic capacities by encouraging them to come together and work as a team for the management of the greenhouses and marketing of their end products locally. 
 Handicrafts Project:  50 women from Kavar have produced handmade purses, greeting cards and scarves to be sold at Marks &amp; Spencer and GAP stores in Istanbul and Ankara. 
 AÇEV Trainings:  More than 90 women have participated in AÇEV’s Rural Women’s Education Program, which includes classes on communication with children, women&#8217;s reproductive health, nutrition, and women’s rights. The classes are supported by educational video screenings. Furthermore, female literacy in the basin has increased by 25 % with the support of AÇEV adult literacy programs. 
 20 women participated in Entrepreneurship Training. 39 women attended field trips outside of their villages. 5 female farmers attended classes on dairy farming at Sütaş Karacabey Facilities. 3 women studied cooperatives at a training organized by the Ministry of Agriculture and German Cooperatives Union. 
 Environmental protection and sustainable use of natural resources is an integral part of the Kavar Project. Protection of grazing lands and pastures, trainings for local shepherds, workshops for raising environmental awareness in children, tree planting and seed festivals are some of the examples of environmental protection activities implemented to date in the project villages. 
 Özyeğin Foundation believes that sustainability of the project depends on the fact that the beneficiaries have a say in the process of formulation and implementation of project activities. It is this participatory process that ensures the ownership of the project by the local community. The Kavar Agricultural Development Cooperative, which was established after extensive community discussions, field trips to see best practices in other regions, and various trainings, is a typical successful example of this participatory approach. 
 Over the years, Kavar Cooperative has become an important actor in the socio-economic development of the whole basin. With the support of the Özyeğin Foundation, the Cooperative built a Milk Collection Center. In 2012, it became an official member of the Association of Cattle Breeders in Bitlis. It has reached the capacity to collect, store and market the milk that is collected from each household in the project villages. Thus, for the first time in Kavar history, milk producers of Kavar have started to earn a living through selling their daily produce. 
 It is the first and only Coop. in the region, which has announced its sale of milk to dairy factories through an open call for price offers. It has contributed significantly to the development of animal husbandry in the villages by purchasing wholesale fodder crops and distributing it to its members at a lower price. It has expanded its agricultural equipment with the purchase of a silage machine, a seed drill and a hoeing machine all co-financed by Özyeğin Foundation and the Ministry of Agriculture. Finally it has started to build a storage unit for the communal agricultural equipment and fodder stocks. For more information about the Kavar Cooperative please visit:  http://kavarkooperatifi.com/ 
  .entry / 
 eğil   hmov   husnu m ozyegin   hüsnü özyeğin   hüsnü özyeğin vakfı   kavar   kırsal kalkınma   kırsalda umut var   ravanda   vakıf 
 Share ! 
 tweet   
    .share-post  
  .post-inner  
  .post-listing  
 Related Articles 
  post-thumbnail / 
 Capacity Building 
 02 September 2014 
  post-thumbnail / 
 Field Offices 
 04 January 2013 
  post-thumbnail / 
 Özyeğin Foundation Rural Development Model 
 04 January 2013 
  post-thumbnail / 
 Rural Development Program Goals 
 04 January 2013 
  .content  
   
  .container / 
 
				P: +90 (216) 564 97 40 
F: +90 (216) 564 90 53
E-mail: info@hmov.org			 
 
				Özyeğin Üniversitesi Çekmeköy Kampüsü
Nişantepe Mah. Orman Sok. 34794 Çekmeköy - İSTANBUL 
All Rights Reserved to Husnu M. Ozyegin Foundation			 
  .Container  
  .Footer bottom  
  .Wrapper  
 Scroll To Top 

 A project that promotes better practices in agriculture and animal husbandry by providing infrastructure and education. 

 A project that promotes better practices in agriculture and animal husbandry by providing infrastructure and education.
 WHOLE PROJECT MARK