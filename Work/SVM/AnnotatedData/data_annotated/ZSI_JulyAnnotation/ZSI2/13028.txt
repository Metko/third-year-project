The Robot Factory  | Changemakers
===============================

 
  Preloader  
  <div id="preloader">
	<div id="status"> </div>
</div>  
 Skip to main content 
 About 
 Challenges 
 Projects 
 Learning 
 Blog 
   
   
 Changemakers 
 English Español Português Français 
   
 Search form 
 Search  
   
 Log in   
 FAQs 
   
 You are here Home 
 The Robot Factory  
   The Re-imagine Learning Network     
   
   Congratulations! This Entry has been selected as a semifinalist.     
   
   
   
 The Robot Factory : Using STEM to solve environmental problems through play and exploration. 
   Alpena, United States Alpena, United States     
   
 Robert Thomson <div class="founder">Founder</div> 
   
   
   
   
   
    Year Founded:
   
 
    2012   
 
    Project Stage:
   
 
    Growth   
 Budget:  $1,000 - $10,000   
    Website:
   
 http://www.alpenaschools.com/   
 Facebook:  https://www.facebook.com/therobotfactory.urtworovteam?fref=ts 
 Education   
 Play   
 Youth development   
 Youth leadership   
 Project Summary Elevator Pitch   
    Concise Summary: Help us pitch this solution! Provide an explanation within 3-4 short sentences.   
 In order for students to develop a sustained attraction to science and for them to appreciate the many ways in which it is pertinent to their daily lives, classrooms and after school programming in science needs to connect students to their own interests and experiences. Learning requires play. 
 
    WHAT IF - Inspiration: Write one sentence that describes a way that your project dares to ask, &quot;WHAT IF?&quot;   
 
    What if a group of fifth graders discover a lost shipwreck using a robot they built to discover their future by looking for the past.   
 About Project   
    Problem: What problem is this project trying to address?   
 
    The future job market requires students that are innovators and problem solvers. Classrooms are driven by mandated curriculum and students have little to no time to explore self-interest or to create and play on a regular basis. Personal interest, experience, and enthusiasm are critical to children’s learning of science at school or in other settings. Students need time to develop their curiosity and time to explore their own interest.   
 
    Solution: What is the proposed solution? Please be specific!   
 
    The Robot Factory was developed to provide students the time and support necessary to engage in self-interest. The Robot Factory allows students that want to build and play with robotics an opportunity to do so through classroom outreach and after school programming. The Robot Factory reaches students through a curriculum driven research program that uses STEM to solve local environmental issues. Classrooms participate in water testing, data collection, and robotics. Classroom students interested in continuing and doing more after school can participate through the Robot Factory. The Robot Factory provides students with materials and time to build underwater or land based robots.   
 Impact: How does it Work   
    Example: Walk us through a specific example(s) of how this solution makes a difference; include its primary activities.   
 
    The success of our underwater robotics program and our partnership with the Thunder Bay National Marine Sanctuary and NOAA helped bring the International Underwater Robotic Competition to our town. The competition brought thousands of people to our small town filling hotels for a fifty mile radius. The Robot Factory participated in the competition and  allowed our students the opportunity to engage with world leaders in robotic careers and work with fellow students from around the world.        
 
    Impact: What is the impact of the work to date? Also describe the projected future impact for the coming years.   
 
    Currently the project is participating with twenty-five local classroom and the after school program has over eighty students participating at different levels. We are helping two separate school districts develop K-12 underwater robotic programs.  During the summer of 2014, the robot Factory host two week long engineering camps. Posttesting showed that the students had an eight percent content retention rate.     
 
    Spread Strategies: Moving forward, what are the main strategies for scaling impact?   
 
    The Robot Factory has sustained steady growth and has moved twice to accommodate the space required to allow more students to participate. In moving forward, the Robot Factory would provide classrooms a field trip destination in which teachers can bring their classroom to a facility that would engage students&#039; curiosity and let them play and discover.  Teachers would be provided professional development to foster play and exploration in their own classrooms.    
 Sustainability   
    Financial Sustainability Plan: What is this solution’s plan to ensure financial sustainability?   
 
    Our first step in showing sustainability is by increasing teacher recruitment and professional development. By providing a serves to the local school districts were are able to maintain classroom participation and student recruitment in after school programming.   By having this element in place, we can use it to prove sustainability that will help us secure grants for parts, equipment and consumables.    
 
    Marketplace: Who else is addressing the problem outlined here? How does the proposed project differ from these approaches?   
 
     Our success in sustaining our project has come through fulfilling our promises to our community partners and showing up ready to get something done. Our willingness to be open and fluent to our community stewardship needs has allowed our project to evolve. By maintaining growth, commitment, and filling a community need, we can prove our project is a worthy investment to the community. No one in our area has the partnerships, participation level, or the resources to provide the service that we bring to the community and students of our area.   
 Team   
    Founding Story   
 
    As elementary students left my program, there was no robotics program beyond the elementary level. I spent several years working to recruit secondary teachers to host robotic teams at school, but was not able to get teachers to take on the program and become mentors. There were several junior high students and high school students that wanted a way to continue with robotics, but no place for them to go. We had numerous secondary students wanting to participate, but no place to dot it. This is why I created the Robot Factory.   
 
    Team   
 
    Currently the Robot Factory is hosted by the Thunder Bay National Marine Sanctuary. Our team is made of volunteers and no one is paid. 

Board Members:
Robert Thomson (Elementary educator and engineer: Coordinator)
Rick Fluharty (DTE energy: outreach)
Sarah Waters (NOAA Educational Outreach Coordinator: Mentor)
Harriet Smith (NOAA Educational Specialist: logistics)
David Cummins (Alpena Community College: Tech. Adviser)   
 About You Organization:   Alpena Public Schools About You First Name Robert 
 Last Name Thomson 
 Twitter URL http://twitter.com/#!/ 
 Facebook URL http://www.facebook.com/#!/pages/Thunder-Bay-River-Water-Shed-Project/15... 
 About Your Project Organization Name Alpena Public Schools 
 How long has your organization been operating? Project Organization Country , MI, Alpena 
 Country where this project is creating social impact , MI, Alpena 
 What awards or honors has the project received? Funding: How is your project financial supported? Individuals, Foundations, Regional government, Other. Supplemental Awards Marine Advanced Technical Education Center Underwater Robotics Competition.  The Robot Factory has produced The Great Lakes Regional Winner every year it has participated. The Chevrolet Green Education Award and ING&#039;s Unsung Heroes Award for Educational Programming. 
 Primary Target Age Group 6 - 12, 13 - 17. Your role in Education Administrator, After-School Provider, Coach, Parent, Teacher. Please specify which of the following best applies: I am applying on behalf of a particular program or initiative. The type of school(s) your solution is affiliated with (if applicable) Public (tuition-free), Private (tuition-based), Home-School. Intervention Focus Extracurricular, Professional Development, Community. Does your project utilize any of the innovative design principles below? Putting Children in Charge: Giving children a voice and cultivating agency via experiential learning, project-based learning, and civic engagement. Is your project targeted at solving any of the following key barriers? Trapped Between Competing Pressures: Educators face a lack of capacity to re-imagine and restructure educational settings. Need Funding Sources 
 Offer Start-up Tips 
 What key learning outcomes does your work seek to improve? Creating self-actuating members of the community. 
 Secondary Form PROGRAM DESIGN CLARITY: We are hungry to know more about what exactly your model consists of. Please succinctly list a) what main activities are you doing with your beneficiaries, b) where you carry out the activities? c) how often? d) for how many hours? e) who delivers the services? and f) any other brief details  The Robot Factory connects four major community and national partners to offer students opportunities to work with world leaders and industries in the marine technologies.  The Factory is hosted by NOAA and has its own building which provides time for students to build after school and on Saturdays. 
 INSPIRATION: What do you consider the most important trends or evidence that inspire you to believe the world is ready to Re-imagine Learning? Please elaborate. In today&#039;s world economy, employers need problem solvers and employees that can be innovators. Current educational programming is slowly turning that directions, but not fast enough to meet the demands.  The exciting part is that the students are ready and excited to learn. This is what inspires me to push for educational reform. 
 LEARNING THROUGH PLAY: What does “learning through play” mean to you and why it is a must-have, instead of a nice to have?  Learning through play is the foundation of our learning process. When we play, we are doing the things we love. We learn to be creative problem solvers because we have a passion for what we love. That passion is what drives us to reach further and to push through the hurdles that get in our way. Without the love of play, there would be no passion for discovery. 
 SUSTAINABILITY: Please list a quick breakdown of your funding, indicating the percentage that comes from each source.  Fifty percent of our funding comes through the Thunder Bay National Marine Sanctuary in both direct founding and and in-kind. The other fifty percent of our founding comes through foundation grants such as the Besser Foundation, the Great Lakes Stewardship Initiative, and personal donations. 
 MODEL: How does your mission relate to your business model (i.e. Non-profit, for-profit or hybrid)? Our mission focuses on developing a passion for discovery in our students and our model for a successful program has to have that same passion for discovery as our foundation. Worker as a non-profit requires everyone to be opportunistic and that we are always one step away from the next opportunity. 
 FUNDING PRIORITIES: If your organization were given $20K in unrestricted funding today, how would you use it?  Why?   It would be used as seed money to invest in recruiting and training more teachers, developing more student lead community projects, and growing the program through out-reach in other communities. Our success in sustaining our programming comes form giving back to our community. 
 PARTNERSHIPS: Tell us about your partnerships that enhance your approach. We have built relationships with forty-five regional partners and community agencies. Our largest is with the Thunder Bay National Marine Sanctuary and NOAA. NOAA provides us with building space, volunteers, and staffing in order to organize outreach programming and technical resources so students have opportunities to work with scientist like Dr. Robert Ballard. 
 COLLABORATIONS: Have you considered or initiated partnerships with any of the other Challenge Pacesetters?  If so,  please share. I have read through several pacesetter projects and I am inspired to continue to push. I am working with three high school teachers from outside our community to consider the opportunity to develop a team of student underwater explorers to seek out undiscovered shipwrecks in the Great Lakes. 
 VISION: If you had unlimited funding, and you could fast forward 15 years to when your program has been able to achieve wild success - what will it have achieved?  Our program will have become a K-12 institute dedicated to discovering the unknown. We would have our own research institute that would host world renowned  researchers and fleet of research vessels that would allow teams of students to travel the world as research ambassadors teaching, discovering, and advocating education through play. 
 IMPACT - KEY METRICS: Please list the key data points that you would cite as evidence that you are able to achieve lasting learning outcomes. Please also share one data point for which you most hope to see better results over time One key data point that we have had some early success with is the retention rates of students over the last three years. We have been able to  increase our participation level by 100%, but also retain eighty percent of the students from our previous years. 
 IMPACT - REPORTING SAMPLE: Please attach any examples of your impact reporting. [optional]:  underwater_research_robot_tech_report.2014_int.pdf RESEARCH AND EVIDENCE: Please link or attach any research or evidence resource you are open to sharing more widely [optional]. Building research and evidence is a key aim of this initiative, and the resources you share may be chosen for listing in the  Center for Education Innovations library :  student_survey_results_thomson.doc SOURCE: If applicable - who created the research or evidence you are choosing to share? :  IMPACT - REACH: How many people did your project directly engage in programmatic activities in the last year?  0 to 500 STUDY: Has an external evaluation or study been conducted of your organization? In progress Other (please specify) Number of Employees: Fewer than 10 Number of Volunteers: 10-100 APPROACHES: Given the complexity of play, it is not surprising that there have been numerous research attempts to categorize the different types and approaches! Please indicate which of the following your project focuses on. Creating a Supportive Socio-Emotional Environment, Providing a Range of Opportunities (providing the equipment and materials needed for various types of play), Educational Structuring (developing playful projects within educational contexts). Other (please specify) AFFILIATION: Please specify if your organization has any existing affiliations with the LEGO Group.   
 Log in  to post comments Printer-friendly version Download PDF 
 Give Feedback 
 Your insights will be sent privately to this project to help them improve. Rate This Project 
 Overall  * 
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Innovation  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Impact  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Sustainability  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Write a Private Feedback  * 
 Strength Need Improvement   
 Clarity of model 
   
 Financial Sustainability 
   
 Idea Originality 
   
 Impact Measurement 
   
 Impact Potential 
   
 Partnerships 
   
 Scalability 
   
 Team 
   
 Understanding of Marketplace 
   
   
   
   
   
   
 © 2016-2017 Ashoka Changemakers. 
 Terms  &amp;  Privacy Policy  |  Feedback 
   
 What We Do 
 What You Can Do Here 
 Our Team 
 Careers 
 FAQs 
 Partner With Us 
 Contact Us 
   
   
   
   
The Robot Factory  | Changemakers
===============================

 
  Preloader  
  <div id="preloader">
	<div id="status"> </div>
</div>  
 Skip to main content 
 About 
 Challenges 
 Projects 
 Learning 
 Blog 
   
   
 Changemakers 
 English Español Português Français 
   
 Search form 
 Search  
   
 Log in   
 FAQs 
   
 You are here Home 
 The Robot Factory  
   The Re-imagine Learning Network     
   
   Congratulations! This Entry has been selected as a semifinalist.     
   
   
   
 The Robot Factory : Using STEM to solve environmental problems through play and exploration. 
   Alpena, United States Alpena, United States     
   
 Robert Thomson <div class="founder">Founder</div> 
   
   
   
   
   
    Year Founded:
   
 
    2012   
 
    Project Stage:
   
 
    Growth   
 Budget:  $1,000 - $10,000   
    Website:
   
 http://www.alpenaschools.com/   
 Facebook:  https://www.facebook.com/therobotfactory.urtworovteam?fref=ts 
 Education   
 Play   
 Youth development   
 Youth leadership   
 Project Summary Elevator Pitch   
    Concise Summary: Help us pitch this solution! Provide an explanation within 3-4 short sentences.   
 In order for students to develop a sustained attraction to science and for them to appreciate the many ways in which it is pertinent to their daily lives, classrooms and after school programming in science needs to connect students to their own interests and experiences. Learning requires play. 
 
    WHAT IF - Inspiration: Write one sentence that describes a way that your project dares to ask, &quot;WHAT IF?&quot;   
 
    What if a group of fifth graders discover a lost shipwreck using a robot they built to discover their future by looking for the past.   
 About Project   
    Problem: What problem is this project trying to address?   
 
    The future job market requires students that are innovators and problem solvers. Classrooms are driven by mandated curriculum and students have little to no time to explore self-interest or to create and play on a regular basis. Personal interest, experience, and enthusiasm are critical to children’s learning of science at school or in other settings. Students need time to develop their curiosity and time to explore their own interest.   
 
    Solution: What is the proposed solution? Please be specific!   
 
    The Robot Factory was developed to provide students the time and support necessary to engage in self-interest. The Robot Factory allows students that want to build and play with robotics an opportunity to do so through classroom outreach and after school programming. The Robot Factory reaches students through a curriculum driven research program that uses STEM to solve local environmental issues. Classrooms participate in water testing, data collection, and robotics. Classroom students interested in continuing and doing more after school can participate through the Robot Factory. The Robot Factory provides students with materials and time to build underwater or land based robots.   
 Impact: How does it Work   
    Example: Walk us through a specific example(s) of how this solution makes a difference; include its primary activities.   
 
    The success of our underwater robotics program and our partnership with the Thunder Bay National Marine Sanctuary and NOAA helped bring the International Underwater Robotic Competition to our town. The competition brought thousands of people to our small town filling hotels for a fifty mile radius. The Robot Factory participated in the competition and  allowed our students the opportunity to engage with world leaders in robotic careers and work with fellow students from around the world.        
 
    Impact: What is the impact of the work to date? Also describe the projected future impact for the coming years.   
 
    Currently the project is participating with twenty-five local classroom and the after school program has over eighty students participating at different levels. We are helping two separate school districts develop K-12 underwater robotic programs.  During the summer of 2014, the robot Factory host two week long engineering camps. Posttesting showed that the students had an eight percent content retention rate.     
 
    Spread Strategies: Moving forward, what are the main strategies for scaling impact?   
 
    The Robot Factory has sustained steady growth and has moved twice to accommodate the space required to allow more students to participate. In moving forward, the Robot Factory would provide classrooms a field trip destination in which teachers can bring their classroom to a facility that would engage students&#039; curiosity and let them play and discover.  Teachers would be provided professional development to foster play and exploration in their own classrooms.    
 Sustainability   
    Financial Sustainability Plan: What is this solution’s plan to ensure financial sustainability?   
 
    Our first step in showing sustainability is by increasing teacher recruitment and professional development. By providing a serves to the local school districts were are able to maintain classroom participation and student recruitment in after school programming.   By having this element in place, we can use it to prove sustainability that will help us secure grants for parts, equipment and consumables.    
 
    Marketplace: Who else is addressing the problem outlined here? How does the proposed project differ from these approaches?   
 
     Our success in sustaining our project has come through fulfilling our promises to our community partners and showing up ready to get something done. Our willingness to be open and fluent to our community stewardship needs has allowed our project to evolve. By maintaining growth, commitment, and filling a community need, we can prove our project is a worthy investment to the community. No one in our area has the partnerships, participation level, or the resources to provide the service that we bring to the community and students of our area.   
 Team   
    Founding Story   
 
    As elementary students left my program, there was no robotics program beyond the elementary level. I spent several years working to recruit secondary teachers to host robotic teams at school, but was not able to get teachers to take on the program and become mentors. There were several junior high students and high school students that wanted a way to continue with robotics, but no place for them to go. We had numerous secondary students wanting to participate, but no place to dot it. This is why I created the Robot Factory.   
 
    Team   
 
    Currently the Robot Factory is hosted by the Thunder Bay National Marine Sanctuary. Our team is made of volunteers and no one is paid. 

Board Members:
Robert Thomson (Elementary educator and engineer: Coordinator)
Rick Fluharty (DTE energy: outreach)
Sarah Waters (NOAA Educational Outreach Coordinator: Mentor)
Harriet Smith (NOAA Educational Specialist: logistics)
David Cummins (Alpena Community College: Tech. Adviser)   
 About You Organization:   Alpena Public Schools About You First Name Robert 
 Last Name Thomson 
 Twitter URL http://twitter.com/#!/ 
 Facebook URL http://www.facebook.com/#!/pages/Thunder-Bay-River-Water-Shed-Project/15... 
 About Your Project Organization Name Alpena Public Schools 
 How long has your organization been operating? Project Organization Country , MI, Alpena 
 Country where this project is creating social impact , MI, Alpena 
 What awards or honors has the project received? Funding: How is your project financial supported? Individuals, Foundations, Regional government, Other. Supplemental Awards Marine Advanced Technical Education Center Underwater Robotics Competition.  The Robot Factory has produced The Great Lakes Regional Winner every year it has participated. The Chevrolet Green Education Award and ING&#039;s Unsung Heroes Award for Educational Programming. 
 Primary Target Age Group 6 - 12, 13 - 17. Your role in Education Administrator, After-School Provider, Coach, Parent, Teacher. Please specify which of the following best applies: I am applying on behalf of a particular program or initiative. The type of school(s) your solution is affiliated with (if applicable) Public (tuition-free), Private (tuition-based), Home-School. Intervention Focus Extracurricular, Professional Development, Community. Does your project utilize any of the innovative design principles below? Putting Children in Charge: Giving children a voice and cultivating agency via experiential learning, project-based learning, and civic engagement. Is your project targeted at solving any of the following key barriers? Trapped Between Competing Pressures: Educators face a lack of capacity to re-imagine and restructure educational settings. Need Funding Sources 
 Offer Start-up Tips 
 What key learning outcomes does your work seek to improve? Creating self-actuating members of the community. 
 Secondary Form PROGRAM DESIGN CLARITY: We are hungry to know more about what exactly your model consists of. Please succinctly list a) what main activities are you doing with your beneficiaries, b) where you carry out the activities? c) how often? d) for how many hours? e) who delivers the services? and f) any other brief details  The Robot Factory connects four major community and national partners to offer students opportunities to work with world leaders and industries in the marine technologies.  The Factory is hosted by NOAA and has its own building which provides time for students to build after school and on Saturdays. 
 INSPIRATION: What do you consider the most important trends or evidence that inspire you to believe the world is ready to Re-imagine Learning? Please elaborate. In today&#039;s world economy, employers need problem solvers and employees that can be innovators. Current educational programming is slowly turning that directions, but not fast enough to meet the demands.  The exciting part is that the students are ready and excited to learn. This is what inspires me to push for educational reform. 
 LEARNING THROUGH PLAY: What does “learning through play” mean to you and why it is a must-have, instead of a nice to have?  Learning through play is the foundation of our learning process. When we play, we are doing the things we love. We learn to be creative problem solvers because we have a passion for what we love. That passion is what drives us to reach further and to push through the hurdles that get in our way. Without the love of play, there would be no passion for discovery. 
 SUSTAINABILITY: Please list a quick breakdown of your funding, indicating the percentage that comes from each source.  Fifty percent of our funding comes through the Thunder Bay National Marine Sanctuary in both direct founding and and in-kind. The other fifty percent of our founding comes through foundation grants such as the Besser Foundation, the Great Lakes Stewardship Initiative, and personal donations. 
 MODEL: How does your mission relate to your business model (i.e. Non-profit, for-profit or hybrid)? Our mission focuses on developing a passion for discovery in our students and our model for a successful program has to have that same passion for discovery as our foundation. Worker as a non-profit requires everyone to be opportunistic and that we are always one step away from the next opportunity. 
 FUNDING PRIORITIES: If your organization were given $20K in unrestricted funding today, how would you use it?  Why?   It would be used as seed money to invest in recruiting and training more teachers, developing more student lead community projects, and growing the program through out-reach in other communities. Our success in sustaining our programming comes form giving back to our community. 
 PARTNERSHIPS: Tell us about your partnerships that enhance your approach. We have built relationships with forty-five regional partners and community agencies. Our largest is with the Thunder Bay National Marine Sanctuary and NOAA. NOAA provides us with building space, volunteers, and staffing in order to organize outreach programming and technical resources so students have opportunities to work with scientist like Dr. Robert Ballard. 
 COLLABORATIONS: Have you considered or initiated partnerships with any of the other Challenge Pacesetters?  If so,  please share. I have read through several pacesetter projects and I am inspired to continue to push. I am working with three high school teachers from outside our community to consider the opportunity to develop a team of student underwater explorers to seek out undiscovered shipwrecks in the Great Lakes. 
 VISION: If you had unlimited funding, and you could fast forward 15 years to when your program has been able to achieve wild success - what will it have achieved?  Our program will have become a K-12 institute dedicated to discovering the unknown. We would have our own research institute that would host world renowned  researchers and fleet of research vessels that would allow teams of students to travel the world as research ambassadors teaching, discovering, and advocating education through play. 
 IMPACT - KEY METRICS: Please list the key data points that you would cite as evidence that you are able to achieve lasting learning outcomes. Please also share one data point for which you most hope to see better results over time One key data point that we have had some early success with is the retention rates of students over the last three years. We have been able to  increase our participation level by 100%, but also retain eighty percent of the students from our previous years. 
 IMPACT - REPORTING SAMPLE: Please attach any examples of your impact reporting. [optional]:  underwater_research_robot_tech_report.2014_int.pdf RESEARCH AND EVIDENCE: Please link or attach any research or evidence resource you are open to sharing more widely [optional]. Building research and evidence is a key aim of this initiative, and the resources you share may be chosen for listing in the  Center for Education Innovations library :  student_survey_results_thomson.doc SOURCE: If applicable - who created the research or evidence you are choosing to share? :  IMPACT - REACH: How many people did your project directly engage in programmatic activities in the last year?  0 to 500 STUDY: Has an external evaluation or study been conducted of your organization? In progress Other (please specify) Number of Employees: Fewer than 10 Number of Volunteers: 10-100 APPROACHES: Given the complexity of play, it is not surprising that there have been numerous research attempts to categorize the different types and approaches! Please indicate which of the following your project focuses on. Creating a Supportive Socio-Emotional Environment, Providing a Range of Opportunities (providing the equipment and materials needed for various types of play), Educational Structuring (developing playful projects within educational contexts). Other (please specify) AFFILIATION: Please specify if your organization has any existing affiliations with the LEGO Group.   
 Log in  to post comments Printer-friendly version Download PDF 
 Give Feedback 
 Your insights will be sent privately to this project to help them improve. Rate This Project 
 Overall  * 
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Innovation  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Impact  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Sustainability  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Write a Private Feedback  * 
 Strength Need Improvement   
 Clarity of model 
   
 Financial Sustainability 
   
 Idea Originality 
   
 Impact Measurement 
   
 Impact Potential 
   
 Partnerships 
   
 Scalability 
   
 Team 
   
 Understanding of Marketplace 
   
   
   
   
   
   
 © 2016-2017 Ashoka Changemakers. 
 Terms  &amp;  Privacy Policy  |  Feedback 
   
 What We Do 
 What You Can Do Here 
 Our Team 
 Careers 
 FAQs 
 Partner With Us 
 Contact Us
 WHOLE PROJECT MARK