Evidence-Based Programs | EPISCenter
===============================

 
 
Search: 
 Departments  |
 Penn State 
    end of search div  
   
 EPISCenter Facebook  
   
 EPISCenter Twitter  
   
 EPISCenter YouTube  
 About Us Annual Report 
 Job Openings 
 Contact Us 
 News » 2009 News Archive 
 2010 News Archive 
 2011 News Archive 
 2012 News Archive 
 2013 News Archive 
 Recognitions &amp; Features 
 Staff 
 Resources Opioid Epidemic 
 EPISCenter Mapping Project 
 General Resources 
 Research 
 Tools 
 Training Materials 
 Join the EPISCenter Mailing List 
 PA Youth Survey PAYS How-to Guide &amp; Workbook 
 PAYS Webinar Series 
 Submit a Question 
 Communities That Care CTC Phases 
 CTC Resources » CTC Implementation Tools 
 CTC Trainings 
 CTC Regional Meetings 
 Evidence-based Programs EBP 101 Assessing Community Needs 
 Selecting EBPs 
 Strong Implementation 
 Strong Evaluation 
 Sustainability Planning 
 Grant Writing 
 VDPP Funding Announcement 
 SPEP SPEP Score 
 Preparing for SPEP 
 Research Behind SPEP 
 SPEP Contact Information 
 SPEP-Related Presentations 
 SPEP Resources 
 EPISCenter Training Institute EBP Learning Center 
 Comprehensive Prevention Planning Learning Center 
 SPEP Learning Center 
 Sustainability Learning Center 
 Research &amp; Data Learning Center 
   
 You are here Home  » Evidence-Based Programs   
 Evidence-Based Programs   
   
 Pennsylvania has purposefully chosen to invest in disseminating proven effective evidence based programs targeting youth problems such as  violence ,  delinquency ,  substance use , and  school failure . 
 What do we mean when we say a program is evidence based?    This one page resource explains what we mean when we refer to a program as evidence-based.  The menu of  evidence-based programs  listed below have each undergone  rigorous evaluation  and been recognized as effective by federal and state agencies and prevention science organizations. These programs have been shown to be effective in preventing and reducing delinquency and  promoting positive youth development.   
 Before choosing programs to implement, agencies and coalitions examine the risk and protective factors underlying the problems they wish to target in their community.  This matrix shows the risk and protective factors addressed by each of the programs supported by the EPISCenter . 
 Aggression Replacement Training 
 School and community-based intervention program for delinquent and aggressive youth 
 Target ages 12-17 
   
 Big Brothers Big Sisters 
 Community-based prevention/mentoring program 
 Target ages 6-18 
   
 Familias Fuertes 
 Community-based prevention program for Spanish-speaking parents/caregivers and their youth 
 Target ages 10-14 
 This is the Spanish adaptation of SFP 10-14 
   
 Family Bereavement Program 
 School and community-based intervention program for delinquent and aggressive youth 
 Target ages 12-17 
   
 Functional Family Therapy 
 Short-term family-based treatment intervention program 
 For youth with moderate to severe anti-social and behavioral problems and at-risk for delinquency 
 Target ages 10-18 
   
 LifeSkills Training Program 
 Middle School-based universal prevention curriculum 
 Promotes social emotional learning to prevent adolescent tobacco, alcohol, and marijuana use, and violence 
 Target grades 6 through 9, depending on school structure 
   
 Multisystemic Therapy 
 Intensive family and community-based treatment intervention program 
 For youth with serious anti-social and behavioral problems, and at risk for delinquency 
 Target ages 12-17 
   
 Olweus Bullying Prevention Program 
 School climate focused universal prevention program 
 Designed to reduce and prevent school bullying through broad trainings of school staff and weekly class meetings 
 Target grades Elementary, Middle, and High School 
   
 Positive Action 
 School-based universal prevention curriculum 
 Improves school climate and decreases youth problem behavior through Thoughts-Actions-Feelings Circle 
 Target grades K-8 (elementary and middle school) 
   
 Positive Parenting Program 
 Family-based prevention/intervention program for parents/caregivers 
 Comprehensive program with five (5) separate levels of intervention 
 Target ages birth-16 
   
 Project Towards No Drug Abuse 
 High School-based universal prevention curriculum 
 Improves youth knowledge, corrects misperceptions, and builds SEL skills to prevent youth substance use and violence 
 Target ages 14-19 
   
 Promoting Alternative THinking Strategies 
 Pre-school and Elementary School-based universal prevention program 
 Reduces aggression and behavior problems though classroom based social emotional learning lessons 
 Target grades Pre-K to 5/6 
   
 Strengthening Families Program: For Parents &amp; Youth 10-14 
 Community-based prevention program for parents/caregivers and their youth 
 Target ages 10-14 
   
 Strong African American Families 
 Community-based prevention program for parents/caregivers and their youth 
 Culturally sensitive curriculum for African-American families 
 Target ages 10-14 
   
 Trauma-Focused Cognitive Behavioral Therapy 
 Family-based treatment/intervention program for moderate to high-risk youth who have experienced trauma and their families 
 Target ages 3-18 
   
 The Incredible Years 
 Basic Parent Training &ndash; a prevention/intervention program for parents of children ages 3-12 
 Advance Parent Training &ndash; a follow-up prevention program from the Basic Parent Training 
 Dina Classroom &ndash; school-based prevention program for children ages 3-8 
 Small Group &ndash; school or community-based intervention program for children ages 4-8 
   
 Treatment Foster Care Oregon (formerly Multidimensional Treatment Foster Care) 
   
    /.block  
  /.region  
    /#main  
 Search form 
 Search  
   
    /.block  
  /.region  
 The EPISCenter is a collaborative partnership between the Pennsylvania Commission on Crime and Delinquency (PCCD), the Pennsylvania Department of Human Services (DHS), and the Bennett Pierce Prevention Research Center, College of Health and Human Development, Penn State University. The EPISCenter is funded by DHS and PCCD. This resource was developed by the EPISCenter through PCCD grant VP-ST-24368. 
   
    /.block  
  /.region  
 EPISCenter  206 Towers Building, University Park, PA 16802 |  Directions  Phone: (814) 863-2568  |   EPISCenter@psu.edu 
 Privacy and Legal Statement  |  Copyright  © 2015 The Pennsylvania State University  Direct questions to the  webmaster . 
Evidence-Based Programs | EPISCenter
===============================

 
 
Search: 
 Departments  |
 Penn State 
    end of search div  
   
 EPISCenter Facebook  
   
 EPISCenter Twitter  
   
 EPISCenter YouTube  
 About Us Annual Report 
 Job Openings 
 Contact Us 
 News » 2009 News Archive 
 2010 News Archive 
 2011 News Archive 
 2012 News Archive 
 2013 News Archive 
 Recognitions &amp; Features 
 Staff 
 Resources Opioid Epidemic 
 EPISCenter Mapping Project 
 General Resources 
 Research 
 Tools 
 Training Materials 
 Join the EPISCenter Mailing List 
 PA Youth Survey PAYS How-to Guide &amp; Workbook 
 PAYS Webinar Series 
 Submit a Question 
 Communities That Care CTC Phases 
 CTC Resources » CTC Implementation Tools 
 CTC Trainings 
 CTC Regional Meetings 
 Evidence-based Programs EBP 101 Assessing Community Needs 
 Selecting EBPs 
 Strong Implementation 
 Strong Evaluation 
 Sustainability Planning 
 Grant Writing 
 VDPP Funding Announcement 
 SPEP SPEP Score 
 Preparing for SPEP 
 Research Behind SPEP 
 SPEP Contact Information 
 SPEP-Related Presentations 
 SPEP Resources 
 EPISCenter Training Institute EBP Learning Center 
 Comprehensive Prevention Planning Learning Center 
 SPEP Learning Center 
 Sustainability Learning Center 
 Research &amp; Data Learning Center 
   
 You are here Home  » Evidence-Based Programs   
 Evidence-Based Programs   
   
 Pennsylvania has purposefully chosen to invest in disseminating proven effective evidence based programs targeting youth problems such as  violence ,  delinquency ,  substance use , and  school failure . 
 What do we mean when we say a program is evidence based?    This one page resource explains what we mean when we refer to a program as evidence-based.  The menu of  evidence-based programs  listed below have each undergone  rigorous evaluation  and been recognized as effective by federal and state agencies and prevention science organizations. These programs have been shown to be effective in preventing and reducing delinquency and  promoting positive youth development.   
 Before choosing programs to implement, agencies and coalitions examine the risk and protective factors underlying the problems they wish to target in their community.  This matrix shows the risk and protective factors addressed by each of the programs supported by the EPISCenter . 
 Aggression Replacement Training 
 School and community-based intervention program for delinquent and aggressive youth 
 Target ages 12-17 
   
 Big Brothers Big Sisters 
 Community-based prevention/mentoring program 
 Target ages 6-18 
   
 Familias Fuertes 
 Community-based prevention program for Spanish-speaking parents/caregivers and their youth 
 Target ages 10-14 
 This is the Spanish adaptation of SFP 10-14 
   
 Family Bereavement Program 
 School and community-based intervention program for delinquent and aggressive youth 
 Target ages 12-17 
   
 Functional Family Therapy 
 Short-term family-based treatment intervention program 
 For youth with moderate to severe anti-social and behavioral problems and at-risk for delinquency 
 Target ages 10-18 
   
 LifeSkills Training Program 
 Middle School-based universal prevention curriculum 
 Promotes social emotional learning to prevent adolescent tobacco, alcohol, and marijuana use, and violence 
 Target grades 6 through 9, depending on school structure 
   
 Multisystemic Therapy 
 Intensive family and community-based treatment intervention program 
 For youth with serious anti-social and behavioral problems, and at risk for delinquency 
 Target ages 12-17 
   
 Olweus Bullying Prevention Program 
 School climate focused universal prevention program 
 Designed to reduce and prevent school bullying through broad trainings of school staff and weekly class meetings 
 Target grades Elementary, Middle, and High School 
   
 Positive Action 
 School-based universal prevention curriculum 
 Improves school climate and decreases youth problem behavior through Thoughts-Actions-Feelings Circle 
 Target grades K-8 (elementary and middle school) 
   
 Positive Parenting Program 
 Family-based prevention/intervention program for parents/caregivers 
 Comprehensive program with five (5) separate levels of intervention 
 Target ages birth-16 
   
 Project Towards No Drug Abuse 
 High School-based universal prevention curriculum 
 Improves youth knowledge, corrects misperceptions, and builds SEL skills to prevent youth substance use and violence 
 Target ages 14-19 
   
 Promoting Alternative THinking Strategies 
 Pre-school and Elementary School-based universal prevention program 
 Reduces aggression and behavior problems though classroom based social emotional learning lessons 
 Target grades Pre-K to 5/6 
   
 Strengthening Families Program: For Parents &amp; Youth 10-14 
 Community-based prevention program for parents/caregivers and their youth 
 Target ages 10-14 
   
 Strong African American Families 
 Community-based prevention program for parents/caregivers and their youth 
 Culturally sensitive curriculum for African-American families 
 Target ages 10-14 
   
 Trauma-Focused Cognitive Behavioral Therapy 
 Family-based treatment/intervention program for moderate to high-risk youth who have experienced trauma and their families 
 Target ages 3-18 
   
 The Incredible Years 
 Basic Parent Training &ndash; a prevention/intervention program for parents of children ages 3-12 
 Advance Parent Training &ndash; a follow-up prevention program from the Basic Parent Training 
 Dina Classroom &ndash; school-based prevention program for children ages 3-8 
 Small Group &ndash; school or community-based intervention program for children ages 4-8 
   
 Treatment Foster Care Oregon (formerly Multidimensional Treatment Foster Care) 
   
    /.block  
  /.region  
    /#main  
 Search form 
 Search  
   
    /.block  
  /.region  
 The EPISCenter is a collaborative partnership between the Pennsylvania Commission on Crime and Delinquency (PCCD), the Pennsylvania Department of Human Services (DHS), and the Bennett Pierce Prevention Research Center, College of Health and Human Development, Penn State University. The EPISCenter is funded by DHS and PCCD. This resource was developed by the EPISCenter through PCCD grant VP-ST-24368. 
   
    /.block  
  /.region  
 EPISCenter  206 Towers Building, University Park, PA 16802 |  Directions  Phone: (814) 863-2568  |   EPISCenter@psu.edu 
 Privacy and Legal Statement  |  Copyright  © 2015 The Pennsylvania State University  Direct questions to the  webmaster . 

 OBPP is a program addressing problems of bullying at schools, operating at four levels: school wide, classroom, individual, and community. 

 OBPP is a program addressing problems of bullying at schools, operating at four levels: school wide, classroom, individual, and community.
 WHOLE PROJECT MARK