Kolektiv
===============================

 
 
            We, members of the Kolektiv, promise that we will always strive to be the best version of ourselves, because it is the only way we can grow as individuals, colleagues, company and partners. Unconventional  and free spirited as we are, we're fanatically devoted to the work that we live. Instead of products that will meet the form, we want to build stories that will give meaning. Stories that will be a reminder to always go further and always try better. A reminder that nothing is impossible and that limits do not exist. Under this roof we decide to write a different future.
           
 Close 
 
                Kolektiv
               
 Meni 
 About us 
 What we do 
 Projects 
 Team 
 Clients 
 Contact 
 <ul class="right">
            <li><a href="#">BHS</a></li>
          </ul> 
 Up 
 About us 
 What we do 
 Projects 
 Team 
 Clients 
 Contact 
 Down 
 
	                KOLEKTIV was first established in 2001 and since then has become a leader in international recruitment. Today KOLEKTIV is a new generation Recruitment Shared Services Centre helping clients recruit from and across the Balkans, the Baltics, Central & Eastern Europe, the DACH countries and Turkey. Sat in the heart of Europe, our multilingual Centre of Excellence is home to accredited Sourcing professionals, expert Research & Insight teams - and award winning Employer Brand and Recruitment Marketing professionals. 
  <a href="javascript:void(0);" class="md-trigger" data-modal="modal-1">Our Pledge</a>  
  Place to add additional text 
 Continue reading... 
 15 
 Years old 
 40 
 Employees 
 1 
 Mission 
 150% 
 Engaged 
 WHAT WE DO 
 
                We work together for common goals. All good. 
               
 Employment solutions 
 Headhunting & Staffing solutions 
 Branding solutions 
 Recruitment 
 Campaign & event managment 
 Software development 
 Outsourcing 
 HR consulting 
 Project managment 
 Our projects 
 
                In 2004, we created first Bosnian job portal, Posao.ba. The project experienced explosive growth and became a leading job portal in Bosnia and Herzegovina. Soon we understood that our knowledge and skills could have a further reach, and so several new ideas, all  connected by common goal, were born and developed into their own companies...
               
 Bizoo 
 Bizoo, Business Innovation Zoo is a startup accelerator and startup event organiser with a wide network of local and international mentors and angel investors, located in Sarajevo, BiH. 
                 
                Our vision is to grow and build a sustainable startup ecosystem. We offer resources, know-ledge, cutting edge mentors and access to the latest technology. 
                 
                Apply to one of our programs and gain access to more exciting opportunities in Sarajevo and abroad.
                 
 WWW.BIZOO.BA 
 Most attractive employer 
 Project “Most attractive employer” was initiated in 2007, when the portal Posao.ba organized the first research on the companies for which portal users would like to work. In this way, insights were gained about the perception of bh citizens what they think about the companies as employers and the factors that influence their perception.
                     
 WWW.POSLODAVAC.BA 
 VideoKliker 
 Entertaining and inspiring trainings designed for companies, entrepreneurs and individuals, who want to be educated and motivated, productive and happy. Video Kliker solutions cover wide range of business and interpersonal skills: communication, sales, management and entrepreneurship.
                     
 WWW.VIDEOKLIKER.BA 
 Posao.ba 
 Posao.ba portal is the first and most visited website for employment in BiH. It was launched with the aim to include BiH in international labour stock exchange trends, as well as to directly connect candidates with companies and help them make the best match.
                   
                  Posao.ba was nominated the best web site in BiH, receiving GRAND prix WebAward for design, content, functionality and inovation. According to Gemius Audience, Posao.ba is among 10 most visited web pages in Bosnia and Herzegovina.  
                  Marketing experts have nominated us with prestigious Superbrands 2008 and Superbrands 2009 award. 
                  Portal.ba publishes round 1000 job ads daily and has more than 15.000 domestic and international companies, with more than 200.000 registered users in our database.
   
 WWW.POSAO.BA 
 Plata.ba 
 Plata.ba is the bigest and only survey of salary in BiH which can give comparison for over 300 work positions. This unique portal allows you to get comparison of salaries of Bosnia & Herzegovina, Slovakia, Hungary, Czech Republic, Serbia and Croatia. 
                     
 WWW.PLATA.BA 
 edukacija.posao.ba 
 Portal which promotes formal and informal educational trainings, seminars, educational programs and schools, scholarships, fellowships, courses, seminars, courses for undergraduate and postgraduate students, lectures and retraining courses, instructions. 
                     
 WWW.EDUKACIJA.POSAO.BA 
 DRIVE 
 DRIVE is the leading regional leadership and talent conference intended to all people whose work involves leading and development of employees and to professionals with different levels of knowledge and experience. In the past few years, this great event titled „You are Who You Hire“ has gathered more than 1000 participants and presented some of the leading world speakers. 
	                 
	               WHO SHOULD ATTEND DRIVE?
	                 
	•             Directors, managers and company owners 
	
	•             HR Managers 
	
	•             Head of departments and team leaders 
	
	•             All of you whose work involves leading and development of employees 
	
	•             Private and public companies 
	
	•             Companies in the field of selection, recruitment and human development management 
	
	•             Public administration companies 
	
	•             Everyone who is interested in HRM
	                 
 WWW.DRIVE.BA 
 Economic Empowerment of Srebrenica (EES) 
 EES project ("Economic Empowerment of Srebrenica") is implemented by Posao.ba in cooperation with the municipality of Srebrenica and the Association " Friends of Srebrenica ", with the support of the UK Government.
	                 
	                The goal of the project is to support employment and self-employment in the region, as well as to support innovative business ideas of young people.
	               
 MarketMakers 
 MarketMakers is a programme supported by the Swiss Agency for Development and Cooperation (SDC). The programme is part of the Swiss contribution to the transition of BiH towards a socially inclusive market economy and a decentralised, democratic political system, with the longer-term perspective of European integration.
	                 
	                SDC has assigned the implementation of MarketMakers to a consortium consisting of Helvetas Swiss Intercooperation and Kolektiv Ltd. (Posao.ba), combining international development expertise and experience with the context-specific knowledge of a national partner. MarketMakers stimulates private sector growth in selected markets leading to the creation of new job opportunities for young women and men.
	                 
 WWW.MARKETMAKERS.BA 
 TEAM MEMBERS 
 
                We grew from “one man” IT company to a media group which encompasses technology platforms, online bussinesses, events and entrepreneurship. We love to invest in startups and projects that inspire innovation and cultivates work culture, creativity and freedom. We are Kolektiv.
                 
 
                Meet the people who make us unique.
               
 Sanel Kličić 
 Employment Advisor 
 Erna Karišik 
 Client Support 
 Amila Kusturica 
 Sales Lead Generator 
 Bojan Divčić 
 Brand and Marketing Manager 
 Davor Odobašić 
 Executive Director 
 Mersiha Mehić 
 Chief Operating Officer 
 Amela Musagić 
 Employment Advisor 
 Vildana Mandalović 
 Head of development project department 
 Sulejman Kličić 
 Head of IT department 
 Merdina Mostarlić 
 Recruitment Specialist 
 Antonio Frankić 
 Head of finance and administration 
 Ayla Kutlay 
 Recruitment Specialist 
 Vedad Tokača 
 Sales and Employment Consultant 
 Jelena Golijanin 
 Client / Customer Support 
 Nina Hadžić 
 Recruitment Specialist & Psychologist 
 Anes Suljić 
 Programmer 
 Edin Mehić 
 Founder and CEO 
 Alma Hadžić 
 Head of HR Recruitment Agency 
 Jasmina Malja 
 Office Manager 
 Nermana Ajanović 
 Head of project department 
 Nadio Pašalić 
 Programmer 
 Mersiha Čelik 
 Client Support 
 Selma Turulja Borčak 
 HR Generalist 
 Irma Turković 
 HR Specialist 
 Sanjin Šehić 
 Designer 
 Ajla Ahmić 
 Accountant 
 Semina Husomanović 
 Project coordinator / EDS 
 Alma Berberović 
 Temporary Staffing Consultant 
 CLIENTS 
 Our clients sit at the core of our offerings and services. Our goal is to provide them with specific value. 
 Al Jazeera Balkans 
 American Embassy 
 British Council 
 Visoko sudsko tužilačko vijeće 
 UniCredit Group 
 Robert Bosch 
 Authority Partners 
 Marbo 
 KPMG 
 Reckitt Benckiser 
 Nestle 
 Atlantic Group 
 Deichmann 
 Lufthansa 
 Imperial Tobacco 
 Gazprom 
 Roche 
 GlaxoSmithKline 
 L'oreal 
 BH Telecom 
 Philip Morris 
 Zara 
 Agrokor Group 
 HT Eronet 
 Elektroprivreda BiH 
 Microsoft 
 MARS 
 Carlsberg 
 Violeta 
 dm drogerie markt 
 Messer Tehnoplin 
 HENKEL 
 UNDP 
 HP 
 Holdina 
 Qatar Airways 
 JYSK 
 Coca Cola 
 Orbico 
 Bosnalijek 
 Swiss Embassy 
 OBI 
 Austrian Airlines 
 USAID 
 Ernst & Young 
 Helvetas 
   
   
 GET IN TOUCH 
 
                Customer Care Line: 
 
                  + 387 33 204 592
                 
                8am - 17pm 
                Mondays to Fridays 
 
                E-mail: 
 info@kolektiv.ba 
 
                Street  address: 
                Kolektiv d.o.o. 
                Zmaja od Bosne 74, 71 000 Sarajevo 
                Bosna i Hercegovina 

               
 Kolektiv 
 FB 
 TW 
 YT 
 About us 
 What we do 
 Projects 
 Team 
 Clients 
 Contact 
 &copy; 2014 Kolektiv LTD. All rights reserved. 
  [CHECK#58245667]  
Kolektiv
===============================

 
 
            We, members of the Kolektiv, promise that we will always strive to be the best version of ourselves, because it is the only way we can grow as individuals, colleagues, company and partners. Unconventional  and free spirited as we are, we're fanatically devoted to the work that we live. Instead of products that will meet the form, we want to build stories that will give meaning. Stories that will be a reminder to always go further and always try better. A reminder that nothing is impossible and that limits do not exist. Under this roof we decide to write a different future.
           
 Close 
 
                Kolektiv
               
 Meni 
 About us 
 What we do 
 Projects 
 Team 
 Clients 
 Contact 
 <ul class="right">
            <li><a href="#">BHS</a></li>
          </ul> 
 Up 
 About us 
 What we do 
 Projects 
 Team 
 Clients 
 Contact 
 Down 
 
	                KOLEKTIV was first established in 2001 and since then has become a leader in international recruitment. Today KOLEKTIV is a new generation Recruitment Shared Services Centre helping clients recruit from and across the Balkans, the Baltics, Central & Eastern Europe, the DACH countries and Turkey. Sat in the heart of Europe, our multilingual Centre of Excellence is home to accredited Sourcing professionals, expert Research & Insight teams - and award winning Employer Brand and Recruitment Marketing professionals. 
  <a href="javascript:void(0);" class="md-trigger" data-modal="modal-1">Our Pledge</a>  
  Place to add additional text 
 Continue reading... 
 15 
 Years old 
 40 
 Employees 
 1 
 Mission 
 150% 
 Engaged 
 WHAT WE DO 
 
                We work together for common goals. All good. 
               
 Employment solutions 
 Headhunting & Staffing solutions 
 Branding solutions 
 Recruitment 
 Campaign & event managment 
 Software development 
 Outsourcing 
 HR consulting 
 Project managment 
 Our projects 
 
                In 2004, we created first Bosnian job portal, Posao.ba. The project experienced explosive growth and became a leading job portal in Bosnia and Herzegovina. Soon we understood that our knowledge and skills could have a further reach, and so several new ideas, all  connected by common goal, were born and developed into their own companies...
               
 Bizoo 
 Bizoo, Business Innovation Zoo is a startup accelerator and startup event organiser with a wide network of local and international mentors and angel investors, located in Sarajevo, BiH. 
                 
                Our vision is to grow and build a sustainable startup ecosystem. We offer resources, know-ledge, cutting edge mentors and access to the latest technology. 
                 
                Apply to one of our programs and gain access to more exciting opportunities in Sarajevo and abroad.
                 
 WWW.BIZOO.BA 
 Most attractive employer 
 Project “Most attractive employer” was initiated in 2007, when the portal Posao.ba organized the first research on the companies for which portal users would like to work. In this way, insights were gained about the perception of bh citizens what they think about the companies as employers and the factors that influence their perception.
                     
 WWW.POSLODAVAC.BA 
 VideoKliker 
 Entertaining and inspiring trainings designed for companies, entrepreneurs and individuals, who want to be educated and motivated, productive and happy. Video Kliker solutions cover wide range of business and interpersonal skills: communication, sales, management and entrepreneurship.
                     
 WWW.VIDEOKLIKER.BA 
 Posao.ba 
 Posao.ba portal is the first and most visited website for employment in BiH. It was launched with the aim to include BiH in international labour stock exchange trends, as well as to directly connect candidates with companies and help them make the best match.
                   
                  Posao.ba was nominated the best web site in BiH, receiving GRAND prix WebAward for design, content, functionality and inovation. According to Gemius Audience, Posao.ba is among 10 most visited web pages in Bosnia and Herzegovina.  
                  Marketing experts have nominated us with prestigious Superbrands 2008 and Superbrands 2009 award. 
                  Portal.ba publishes round 1000 job ads daily and has more than 15.000 domestic and international companies, with more than 200.000 registered users in our database.
   
 WWW.POSAO.BA 
 Plata.ba 
 Plata.ba is the bigest and only survey of salary in BiH which can give comparison for over 300 work positions. This unique portal allows you to get comparison of salaries of Bosnia & Herzegovina, Slovakia, Hungary, Czech Republic, Serbia and Croatia. 
                     
 WWW.PLATA.BA 
 edukacija.posao.ba 
 Portal which promotes formal and informal educational trainings, seminars, educational programs and schools, scholarships, fellowships, courses, seminars, courses for undergraduate and postgraduate students, lectures and retraining courses, instructions. 
                     
 WWW.EDUKACIJA.POSAO.BA 
 DRIVE 
 DRIVE is the leading regional leadership and talent conference intended to all people whose work involves leading and development of employees and to professionals with different levels of knowledge and experience. In the past few years, this great event titled „You are Who You Hire“ has gathered more than 1000 participants and presented some of the leading world speakers. 
	                 
	               WHO SHOULD ATTEND DRIVE?
	                 
	•             Directors, managers and company owners 
	
	•             HR Managers 
	
	•             Head of departments and team leaders 
	
	•             All of you whose work involves leading and development of employees 
	
	•             Private and public companies 
	
	•             Companies in the field of selection, recruitment and human development management 
	
	•             Public administration companies 
	
	•             Everyone who is interested in HRM
	                 
 WWW.DRIVE.BA 
 Economic Empowerment of Srebrenica (EES) 
 EES project ("Economic Empowerment of Srebrenica") is implemented by Posao.ba in cooperation with the municipality of Srebrenica and the Association " Friends of Srebrenica ", with the support of the UK Government.
	                 
	                The goal of the project is to support employment and self-employment in the region, as well as to support innovative business ideas of young people.
	               
 MarketMakers 
 MarketMakers is a programme supported by the Swiss Agency for Development and Cooperation (SDC). The programme is part of the Swiss contribution to the transition of BiH towards a socially inclusive market economy and a decentralised, democratic political system, with the longer-term perspective of European integration.
	                 
	                SDC has assigned the implementation of MarketMakers to a consortium consisting of Helvetas Swiss Intercooperation and Kolektiv Ltd. (Posao.ba), combining international development expertise and experience with the context-specific knowledge of a national partner. MarketMakers stimulates private sector growth in selected markets leading to the creation of new job opportunities for young women and men.
	                 
 WWW.MARKETMAKERS.BA 
 TEAM MEMBERS 
 
                We grew from “one man” IT company to a media group which encompasses technology platforms, online bussinesses, events and entrepreneurship. We love to invest in startups and projects that inspire innovation and cultivates work culture, creativity and freedom. We are Kolektiv.
                 
 
                Meet the people who make us unique.
               
 Sanel Kličić 
 Employment Advisor 
 Erna Karišik 
 Client Support 
 Amila Kusturica 
 Sales Lead Generator 
 Bojan Divčić 
 Brand and Marketing Manager 
 Davor Odobašić 
 Executive Director 
 Mersiha Mehić 
 Chief Operating Officer 
 Amela Musagić 
 Employment Advisor 
 Vildana Mandalović 
 Head of development project department 
 Sulejman Kličić 
 Head of IT department 
 Merdina Mostarlić 
 Recruitment Specialist 
 Antonio Frankić 
 Head of finance and administration 
 Ayla Kutlay 
 Recruitment Specialist 
 Vedad Tokača 
 Sales and Employment Consultant 
 Jelena Golijanin 
 Client / Customer Support 
 Nina Hadžić 
 Recruitment Specialist & Psychologist 
 Anes Suljić 
 Programmer 
 Edin Mehić 
 Founder and CEO 
 Alma Hadžić 
 Head of HR Recruitment Agency 
 Jasmina Malja 
 Office Manager 
 Nermana Ajanović 
 Head of project department 
 Nadio Pašalić 
 Programmer 
 Mersiha Čelik 
 Client Support 
 Selma Turulja Borčak 
 HR Generalist 
 Irma Turković 
 HR Specialist 
 Sanjin Šehić 
 Designer 
 Ajla Ahmić 
 Accountant 
 Semina Husomanović 
 Project coordinator / EDS 
 Alma Berberović 
 Temporary Staffing Consultant 
 CLIENTS 
 Our clients sit at the core of our offerings and services. Our goal is to provide them with specific value. 
 Al Jazeera Balkans 
 American Embassy 
 British Council 
 Visoko sudsko tužilačko vijeće 
 UniCredit Group 
 Robert Bosch 
 Authority Partners 
 Marbo 
 KPMG 
 Reckitt Benckiser 
 Nestle 
 Atlantic Group 
 Deichmann 
 Lufthansa 
 Imperial Tobacco 
 Gazprom 
 Roche 
 GlaxoSmithKline 
 L'oreal 
 BH Telecom 
 Philip Morris 
 Zara 
 Agrokor Group 
 HT Eronet 
 Elektroprivreda BiH 
 Microsoft 
 MARS 
 Carlsberg 
 Violeta 
 dm drogerie markt 
 Messer Tehnoplin 
 HENKEL 
 UNDP 
 HP 
 Holdina 
 Qatar Airways 
 JYSK 
 Coca Cola 
 Orbico 
 Bosnalijek 
 Swiss Embassy 
 OBI 
 Austrian Airlines 
 USAID 
 Ernst & Young 
 Helvetas 
   
   
 GET IN TOUCH 
 
                Customer Care Line: 
 
                  + 387 33 204 592
                 
                8am - 17pm 
                Mondays to Fridays 
 
                E-mail: 
 info@kolektiv.ba 
 
                Street  address: 
                Kolektiv d.o.o. 
                Zmaja od Bosne 74, 71 000 Sarajevo 
                Bosna i Hercegovina 

               
 Kolektiv 
 FB 
 TW 
 YT 
 About us 
 What we do 
 Projects 
 Team 
 Clients 
 Contact 
 &copy; 2014 Kolektiv LTD. All rights reserved. 
  [CHECK#58245667] 

 BIZOO supports entrepreneurship and employment of youth in Bosnia and Herzegovina and strengthens the competitiveness of SMEs (Small and Medium Sized Enterprises) through the efficient use of ICT. 

 BIZOO supports entrepreneurship and employment of youth in Bosnia and Herzegovina and strengthens the competitiveness of SMEs (Small and Medium Sized Enterprises) through the efficient use of ICT.
 WHOLE PROJECT MARK