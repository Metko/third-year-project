ABOUT ELMI | Elmi Project
===============================

   <![endif] 
  BEGIN head  
  BEGIN body  
  BEGIN #header-wrapper  
  BEGIN #header-border  
  BEGIN #header-top  
 Facebook 
 (021) 440-11-48 / (+04) 0757-022-382 
 asoc.habilitas@yahoo.com 
  END #header-top  
  BEGIN #header-content-wrapper  
  END #header-content-wrapper  
  BEGIN #main-menu-wrapper  
 ABOUT ELMI 
 About the project 
 Background 
 Aims 
 PARTNERSHIP 
 Presentation of partners 
 Habilitas Association 
 ANS 
 Caritas Romania 
 Romanian Alzheimer Society 
 E.N.T.E.R. 
 ULO 
 E.R.A. 
 EVENTS 
 RESULTS 
 Results 
 Dissemination 
 Partners meetings 
 Products 
 WHAT IS ECVET 
 CONTACT US 
 FORUM 
  END #main-menu-wrapper  
  END #header-border  
  END #header-wrapper  
  BEGIN .page-header  
 ABOUT ELMI 
   Home  &raquo; ABOUT ELMI    
  END .page-header  
  BEGIN .content-wrapper  
  BEGIN .main-content  
  BEGIN .inner-content-wrapper  
 Description 
 The project ”ENHANCING LABOUR MARKET INTEGRATION OF ELDERLY FAMILY CARERS THROUGH SKILLS IMPROVING” (ELMI) is a Leonardo da Vinci Transfer of Innovation project and is part of the Lifelong Learning Programme. 
 Duration 
 The duration of the ELMI project is of 2 years: 01.03.2014 – 28.02.2016 
 Financing 
 The project is financed by the European Commission through the National Agency for Communitary Programmes in the Field of Education and Professional Training in Romania. 
  BEGIN #respond-wrapper  
 Leave a Comment  Cancel Reply To Comment 
 Name  (required) 
 Email  (required) 
 Website  
 Comment   
  Checkbox by Newsletter Sign-Up Checkbox v2.0.3 - http://wordpress.org/plugins/newsletter-sign-up/  
  
				Sign me up for the newsletter			 
  / Newsletter Sign-Up  
  #respond  
  END #respond-wrapper  
  END .inner-content-wrapper  
  END .main-content  
 Română 
 English 
 Events 
  BEGIN .event-wrapper  
 Jun 
 17 
 Enhancing Labour Market Integration of elderly family carers through skills improving &raquo; 
 9:00 
  END .event-wrapper  
  BEGIN .event-wrapper  
 Jun 
 17 
 ELMI workshop agenda &raquo; 
 9:30 
  END .event-wrapper  
  BEGIN .event-wrapper  
 Oct 
 05 
 Elmi Workshop on project outcomes &raquo; 
 17.00 - 18.30 
  END .event-wrapper  
 Quick Links About ELMI 
 Partnership 
 Events 
 Products 
 What is ECVET 
 Contact 
   
  END .content-wrapper  
  BEGIN #footer-wrapper  
  BEGIN #footer  
  BEGIN #footer-bottom  
 © Elmiproject.eu All rights reserved. 
 Top 
  END #footer-bottom  
  END #footer  
  END #footer-wrapper  
  END body  
ABOUT ELMI | Elmi Project
===============================

   <![endif] 
  BEGIN head  
  BEGIN body  
  BEGIN #header-wrapper  
  BEGIN #header-border  
  BEGIN #header-top  
 Facebook 
 (021) 440-11-48 / (+04) 0757-022-382 
 asoc.habilitas@yahoo.com 
  END #header-top  
  BEGIN #header-content-wrapper  
  END #header-content-wrapper  
  BEGIN #main-menu-wrapper  
 ABOUT ELMI 
 About the project 
 Background 
 Aims 
 PARTNERSHIP 
 Presentation of partners 
 Habilitas Association 
 ANS 
 Caritas Romania 
 Romanian Alzheimer Society 
 E.N.T.E.R. 
 ULO 
 E.R.A. 
 EVENTS 
 RESULTS 
 Results 
 Dissemination 
 Partners meetings 
 Products 
 WHAT IS ECVET 
 CONTACT US 
 FORUM 
  END #main-menu-wrapper  
  END #header-border  
  END #header-wrapper  
  BEGIN .page-header  
 ABOUT ELMI 
   Home  &raquo; ABOUT ELMI    
  END .page-header  
  BEGIN .content-wrapper  
  BEGIN .main-content  
  BEGIN .inner-content-wrapper  
 Description 
 The project ”ENHANCING LABOUR MARKET INTEGRATION OF ELDERLY FAMILY CARERS THROUGH SKILLS IMPROVING” (ELMI) is a Leonardo da Vinci Transfer of Innovation project and is part of the Lifelong Learning Programme. 
 Duration 
 The duration of the ELMI project is of 2 years: 01.03.2014 – 28.02.2016 
 Financing 
 The project is financed by the European Commission through the National Agency for Communitary Programmes in the Field of Education and Professional Training in Romania. 
  BEGIN #respond-wrapper  
 Leave a Comment  Cancel Reply To Comment 
 Name  (required) 
 Email  (required) 
 Website  
 Comment   
  Checkbox by Newsletter Sign-Up Checkbox v2.0.3 - http://wordpress.org/plugins/newsletter-sign-up/  
  
				Sign me up for the newsletter			 
  / Newsletter Sign-Up  
  #respond  
  END #respond-wrapper  
  END .inner-content-wrapper  
  END .main-content  
 Română 
 English 
 Events 
  BEGIN .event-wrapper  
 Jun 
 17 
 Enhancing Labour Market Integration of elderly family carers through skills improving &raquo; 
 9:00 
  END .event-wrapper  
  BEGIN .event-wrapper  
 Jun 
 17 
 ELMI workshop agenda &raquo; 
 9:30 
  END .event-wrapper  
  BEGIN .event-wrapper  
 Oct 
 05 
 Elmi Workshop on project outcomes &raquo; 
 17.00 - 18.30 
  END .event-wrapper  
 Quick Links About ELMI 
 Partnership 
 Events 
 Products 
 What is ECVET 
 Contact 
   
  END .content-wrapper  
  BEGIN #footer-wrapper  
  BEGIN #footer  
  BEGIN #footer-bottom  
 © Elmiproject.eu All rights reserved. 
 Top 
  END #footer-bottom  
  END #footer  
  END #footer-wrapper  
  END body
 WHOLE PROJECT MARK