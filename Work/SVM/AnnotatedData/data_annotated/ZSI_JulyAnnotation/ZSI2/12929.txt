The Dilemma Workshop | Cities of Migration
===============================

 
  <div class="wpml-ls-sidebars-header-language-selector wpml-ls wpml-ls-legacy-list-horizontal">
	<ul>  
 English 
 Français 
 Deutsch 
 Español 
 	</ul>
</div>  
 
				    Search:  
 Cities of Migration 
 Good Ideas 
 Overview 
 Themes > 
 Work 
 Live 
 Learn 
 Connect 
 Plan 
 Municipal 
 Refugee Portal 
 Living Together 
 View All Good Ideas 
 What is a Good Idea in Integration? 
 Good Idea Index 
 Building Inclusive Cities 
 Overview 
 Dimensions > 
 Civic Inclusion 
 Cultural Inclusion 
 Economic Inclusion > 
 Employment Inclusion 
 Entrepreneurship Inclusion 
 Financial Inclusion 
 Educational Inclusion 
 Health Inclusion 
 Political Inclusion > 
 Public Space 
 Role of Media 
 Social Inclusion 
 Spatial Inclusion 
 Welcome Ability 
 Conversations in Integration 
 Current Issue 
 Thought Leadership 
 Monthly Archive 
 Newsletter 
 Publications 
 Learning Exchange 
 Overview 
 Past Webinars 
 Special Events > 
 COM 2016 
 COM 2014 
 COM 2010 
 Arrival City: The Bookclub 
 About 
 Overview 
 Cities of Migration Team 
 Partners 
 &rArr; Menu Good Ideas 
 &ndash; Overview 
 &ndash; &ndash; Work 
 &ndash; &ndash; Live 
 &ndash; &ndash; Learn 
 &ndash; &ndash; Connect 
 &ndash; &ndash; Plan 
 &ndash; Municipal 
 &ndash; Refugee Portal 
 &ndash; Living Together 
 &ndash; View All Good Ideas 
 &ndash; What is a Good Idea in Integration? 
 &ndash; Good Idea Index 
 Building Inclusive Cities 
 &ndash; Overview 
 &ndash; &ndash; Civic Inclusion 
 &ndash; &ndash; Cultural Inclusion 
 &ndash; &ndash; Economic Inclusion > 
 &ndash; &ndash; &ndash; Employment Inclusion 
 &ndash; &ndash; &ndash; Entrepreneurship Inclusion 
 &ndash; &ndash; &ndash; Financial Inclusion 
 &ndash; &ndash; Educational Inclusion 
 &ndash; &ndash; Health Inclusion 
 &ndash; &ndash; Political Inclusion > 
 &ndash; &ndash; &ndash; Public Space 
 &ndash; &ndash; Role of Media 
 &ndash; &ndash; Social Inclusion 
 &ndash; &ndash; Spatial Inclusion 
 &ndash; &ndash; Welcome Ability 
 Conversations in Integration 
 &ndash; Current Issue 
 &ndash; Thought Leadership 
 &ndash; Monthly Archive 
 &ndash; Newsletter 
 &ndash; Publications 
 Learning Exchange 
 &ndash; Overview 
 &ndash; Past Webinars 
 &ndash; Special Events > 
 &ndash; &ndash; COM 2016 
 &ndash; &ndash; COM 2014 
 &ndash; &ndash; COM 2010 
 &ndash; &ndash; Arrival City: The Bookclub 
 About 
 &ndash; Overview 
 &ndash; Cities of Migration Team 
 &ndash; Partners 
   
 Home    >  Good Ideas  >  Work   > The Dilemma Workshop 
 Work 
 
				    Botkyrka, Sweden				     
 Location 
 Population 
 % Foreign Born 
 % Migrants 
 Year 
 Botkyrka 
 86,274 
 39.00% 
 0.00% 
 2012 
 Sweden 
 9,340,682 
 14.30% 
 0.00% 
 2012 
 City website:   Botkyrka Kommun 
 Source: 
 Statistics Sweden 
 The Dilemma Workshop  
 Botkyrka Kommun 
 
            October 31, 2013             close class="ko-date" 
 Living and working together in the intercultural city means challenging discrimination and implicit bias one person at a time 
 A teenager takes his younger sister to the library. The librarian looks at him, sees his Middle Eastern background and wonders whether the girl is “oppressed.” The siblings pick up on the tension as they check out books. Has the librarian rushed to judgment? Has the library ceased to be a comfortable place for them? 
 This “intercultural dilemma” is the kind of experience discussed within an innovative workshop model designed for city staff in  Botkyrka , a district within the larger municipal region of Stockholm. 
 Botkyrka is Sweden’s most ethnically diverse municipality, with the highest percentages of first and second generation immigrants in the country. More than 50% of Botkyrka’s population has roots outside of Sweden. In one neighbourhood, 90% of inhabitants come from a migrant-origin background. The urban-rural split in the region’s diversity is especially striking, “so different and distant that they might as well be on different planets.” 
 These disparities became a matter of concern to Botkyrka’s civic leadership when they learned that many residents with a migrant background did not identify with Botkyrka as “home” but as a “place of transit.” Overcoming this divide was essential to the district’s future outlook. The city needed an  integrated strategy  that respected diversity and could ensure that all Botkyrka residents felt part of the same community and could work together to realize its vision of equality and opportunity for all “boys and girls, women and men who have Botkyrka as their home, regardless of social and ethnic background.”  The district-wide strategy must also recognize the relative youth of Botkyrka’s population as an important asset (average age 36) within the larger regional development plan. 
 Bringing interculturalism to Botkyrka 
 Botkyrka has a history of forward thinking. It established the first “one stop shop” for government services in Sweden. And it elected Sweden’s first foreign-born mayor in the 1970s. In 2010, Botkyrka joined the  Council of Europe’s Intercultural Cities  movement to bring its intercultural strategy home under the slogan, “Botkyrka, far from average.” 
 Yet introducing  interculturalism  would be no easy matter. One of Botkyrka’s challenges was to translate its aspirations into an action plan that could deliver results. A 15-year plan was developed with short- and long-term goals at each five-year marker. Importantly, the plan begins with the city government itself. As first responders and service delivery agents, the district’s administration employees and managers had to buy into the intercultural strategy in order to apply it effectively. City authorities recognized it is one thing to introduce an intercultural approach and another to have residents and civic employees understand it. 
 “One of the goals of the Intercultural Strategy is to narrow, and not increase, the distance between people with immigrant backgrounds and people with traditional Swedish backgrounds,” says Helena Rojas, Director, Division for Democracy, Human Rights &amp; Intercultural Development. 
 Playing games with intercultural intelligence 
 The Botkyrka Intercultural Strategy’s first goal was to incorporate a non-discriminatory and intercultural approach as a core competency for district managers and employees. To make sure the strategy was understood within the civil service, the district offered “Intercultural Dilemma Workshops” where participants could analyze situations of intercultural conflict and learn how to overcome them. The aim is to break down stereotypes and accept differences within the workplace while developing the intercultural ‘intelligence’ needed to respond appropriately to one’s own implicit bias in new or unexpected situations. 
 “We have an expectation from our employees,” says Rojas, “You can’t bring your preconceived ideas about people to work.” For Rojas, this means finding ways for the librarian to understand her biases. Botkyrka’s Dilemma Workshops offer city employees a trusted space and trained professionals with whom to build the intercultural awareness and competencies they need to do their job. 
 Launched in mid-2011 across all eight divisions of the public service, each half-day Dilemma Workshop begins with an introduction to the concept of interculturality. The main exercise is to discuss a number of real-life case studies from within municipal departments, including examples offered by participants themselves. Participants are placed into groups where they are encouraged to walk through the case study scenario or describe their own experience, and then analyze and discuss how intercultural skills, or a lack thereof, affects the outcome. Next, workshop facilitators help participants look at the impact of “structural conditions,” such as language barriers, cultural differences or institutional culture on the situation as well as explain what needs to change for the situation to improve. The aim is to create a safe space for intercultural learning that includes room for debate on the pros and cons of different solutions. The question governing any proposed solution is: how will this outcome reflect on Botkyrka and its services? Will it help the city achieve its goals? Finally, the group plans coming steps (short-term and long-term) and how to support individual employees. 
 Essential to the success of the workshop is the ability of the facilitator to provide a trusting environment. Participants need to be able to speak freely about situations they have experienced where misunderstandings, biases or prejudices may have had a negative impact on communication with colleagues or the public. Without that sense of safety, tough questions and issues, such as the case of the library encounter, would remain unanswered. 
 “One of the keys to the success of the dilemma workshop is to create trust, and an environment where people can share their experiences without feeling like ‘I am not good.’ You have to create that atmosphere for people to tell their stories and not be ashamed of these things,” says Rojas. 
 Success 
 From the individual employee and section head to the wider organization and public, the intercultural workshop aims to develop a continuous support structure that results in systemic change. “The individual always has responsibility [for him- or herself] but the district also has a responsibility to support the individual. That’s the Dilemma Workshop. Everyone has a share of the responsibility,” says Rojas. 
 In the case of the librarian, the department was able to start a working group where employees could bring such issues up for discussion with colleagues. The Dilemma Workshops have proven to be one the key tools to incorporate interculturalism within the district government: 12 workshops have already been held within 8 departments. Botkyrka’s Dilemma Workshop model has been recognized as a best practice by the Council of Europe’s Intercultural Cities program. 
 Making it Work for You: 
 Managing interpersonal conflict in the workplace requires both sensitivity and clear guidelines about rights, responsibilities and forms of redress. 
 A successful “dilemma workshop” model includes a trusting environment where participants are able to discuss situations where they felt uncomfortable, wronged or may have acted unfairly towards a colleague or client. 
 Developing cultural competencies means all parties – newcomers and the  status quo  – have to be prepared to learn, adapt and possibly change the way they interact and do their work. 
   
 Tags:  anti-discrimination ,  Botkyrka ,  communication skills ,  intercultural competencies   
 <div class="categorylisting">
				    <p>
				    :
				    				    </p>
				</div> 
 
				Themes:  Democratic institutions ,  Diversity ,  Employers and buyers of goods and services ,  Human resource management ,  Training ,  Work   
 Back to Top 
 

	<a href="javascript:window.print();"><img src="http://citiesofmigration.ca/wp-content/themes/citiesofmigration/images/printer.png" alt="" border="0"  />Print</a>
	<a href="https://secure.campaigner.com/CSB/Public/Form.aspx?fid=1351270" target="_blank"><img src="http://citiesofmigration.ca/wp-content/themes/citiesofmigration/images/email.png" alt="" border="0"  />E-Newsletter Sign Up!	</a>
    </div>
    <div id="bottom-widgets">
	<table cellspacing="0" cellpadding="0" border="0">
	    <tr>
		<td width="40%" valign="top">
			<a href="http://citiesofmigration.ca/feed/rss/"><img src="http://citiesofmigration.ca/wp-content/themes/citiesofmigration/images/rss.gif" alt="RSS" border="0" style=""  /> Rss</a></td>
		<td align="left">
			<div id="SendAFriendMsg" style="display: inline; position: static;">
				<a href="javascript:ToggleLoginBox()"><img border="0" alt="" src="/wp-content/themes/citiesofmigration/images/friend.gif" class="sendAfriendImg">Email to a friend</a>
			</div>
		</td>
	    </tr>
	</table>
	    <script type="text/javascript">function ToggleLoginBox() { 

						ToggleDisplayBox = document.getElementById('SendAFriendBox'); 

						ToggleDisplayMsg = document.getElementById('SendAFriendMsg');

						

						

						

						if(ToggleDisplayBox.style.display == 'none'  ) { 

						

						ToggleDisplayBox.style.display = 'block'; 

						ToggleDisplayBox.style.position = 'inline'; 

						ToggleDisplayMsg.style.display = 'none'; 

						ToggleDisplayMsg.style.position = 'relative'; 

						bitDisplay= 0; 

						} 

						else if(ToggleDisplayBox.style.display == 'inline' || ToggleDisplayBox.style.display == 'block') { 

						ToggleDisplayBox.style.display = 'none';  

						ToggleDisplayBox.style.position = 'relative'; 

						ToggleDisplayMsg.style.display = 'inline'; 

						ToggleDisplayMsg.style.position = 'static'; 

		

					}

					}</script><div id="SendAFriend" style=" margin:0; padding:0; display:block;"><div style="position:static;display:none;" id="SendAFriendBox"><div  class="top"></div><div class="middle" ><form action="?" method="post" name="SendToAFriendFrom" id="SendToAFriendFrom" style="margin:0; padding:0;"><div class="formLabels"><strong>Your Name:</strong> <span class="validation">*</span></div><div class="formFields"><input type="text" id="fromName" name="fromName" value="" maxlength="255" size="20"  /></div><div style="clear:left"></div><div class="formLabels"><strong>Quick message to your friend (100 characters max ):</strong> </div><div class="formFields"><textarea id="sendingWhy" name="sendingWhy" rows="3" cols="23" wordwrap></textarea></div><div style="clear:left"></div><div class="formLabels"><strong>Friend's Email:</strong> <span class="validation">*</span></div><div class="formFields"><input type="text" id="strEmail" name="strEmail" value="" maxlength="255" size="20"  /></div><div style="clear:left"></div><input type="hidden" name="xaction" value="sendToaFriend"> <input type="hidden" name="token" value="f741a6870ab2032e2a41fa4752155999"><div class="clr"></div><input class="btn" type="submit" value="Send"><input class="btn" type="button" onclick="javascript:ToggleLoginBox()" value="Cancel"><p id="formInst">All fields marked with an asterisk <span class="validation">*</span> are mandatory.</p></form></div><div id="news-bottom"></div></div></div>	    
	     
 Newsletter 
 Events 
 Twitter 
 Facebook 
 Youtube 
   
 Submit a Good Idea 
   
  start: contacts  
 For this Good Idea contact: 
 
				    Helena Rojas, Botkyrka Kommun 				    Director, Division for Democracy, Human Rights & Intercultural Development, 				    Munkhättevägen 45 				    Botkyrka, 				    				    Sweden, 				     147 85 				    				     helena.rojas(at)botkyrka.se				     http://www.botkyrka.se   
  end: contacts  
 Webinars: 
 Webinar: Immigrants Wanted! Smart Strategies for Advancing Welcoming Communities and Regional Growth 
  end: media  
  start : lib items  
 For further reading : 
 Strategy for an Intercultural Botkyrka 
 Municipality of Botkyrka: Intercultural Profile 
 Go to library 
  end: lib items  
 Copyright &copy; Cities of Migration 
 Contact Us 
 |  Disclaimer 
 |  Site Map 
 |  Privacy Policy 
 Maytree 
The Dilemma Workshop | Cities of Migration
===============================

 
  <div class="wpml-ls-sidebars-header-language-selector wpml-ls wpml-ls-legacy-list-horizontal">
	<ul>  
 English 
 Français 
 Deutsch 
 Español 
 	</ul>
</div>  
 
				    Search:  
 Cities of Migration 
 Good Ideas 
 Overview 
 Themes > 
 Work 
 Live 
 Learn 
 Connect 
 Plan 
 Municipal 
 Refugee Portal 
 Living Together 
 View All Good Ideas 
 What is a Good Idea in Integration? 
 Good Idea Index 
 Building Inclusive Cities 
 Overview 
 Dimensions > 
 Civic Inclusion 
 Cultural Inclusion 
 Economic Inclusion > 
 Employment Inclusion 
 Entrepreneurship Inclusion 
 Financial Inclusion 
 Educational Inclusion 
 Health Inclusion 
 Political Inclusion > 
 Public Space 
 Role of Media 
 Social Inclusion 
 Spatial Inclusion 
 Welcome Ability 
 Conversations in Integration 
 Current Issue 
 Thought Leadership 
 Monthly Archive 
 Newsletter 
 Publications 
 Learning Exchange 
 Overview 
 Past Webinars 
 Special Events > 
 COM 2016 
 COM 2014 
 COM 2010 
 Arrival City: The Bookclub 
 About 
 Overview 
 Cities of Migration Team 
 Partners 
 &rArr; Menu Good Ideas 
 &ndash; Overview 
 &ndash; &ndash; Work 
 &ndash; &ndash; Live 
 &ndash; &ndash; Learn 
 &ndash; &ndash; Connect 
 &ndash; &ndash; Plan 
 &ndash; Municipal 
 &ndash; Refugee Portal 
 &ndash; Living Together 
 &ndash; View All Good Ideas 
 &ndash; What is a Good Idea in Integration? 
 &ndash; Good Idea Index 
 Building Inclusive Cities 
 &ndash; Overview 
 &ndash; &ndash; Civic Inclusion 
 &ndash; &ndash; Cultural Inclusion 
 &ndash; &ndash; Economic Inclusion > 
 &ndash; &ndash; &ndash; Employment Inclusion 
 &ndash; &ndash; &ndash; Entrepreneurship Inclusion 
 &ndash; &ndash; &ndash; Financial Inclusion 
 &ndash; &ndash; Educational Inclusion 
 &ndash; &ndash; Health Inclusion 
 &ndash; &ndash; Political Inclusion > 
 &ndash; &ndash; &ndash; Public Space 
 &ndash; &ndash; Role of Media 
 &ndash; &ndash; Social Inclusion 
 &ndash; &ndash; Spatial Inclusion 
 &ndash; &ndash; Welcome Ability 
 Conversations in Integration 
 &ndash; Current Issue 
 &ndash; Thought Leadership 
 &ndash; Monthly Archive 
 &ndash; Newsletter 
 &ndash; Publications 
 Learning Exchange 
 &ndash; Overview 
 &ndash; Past Webinars 
 &ndash; Special Events > 
 &ndash; &ndash; COM 2016 
 &ndash; &ndash; COM 2014 
 &ndash; &ndash; COM 2010 
 &ndash; &ndash; Arrival City: The Bookclub 
 About 
 &ndash; Overview 
 &ndash; Cities of Migration Team 
 &ndash; Partners 
   
 Home    >  Good Ideas  >  Work   > The Dilemma Workshop 
 Work 
 
				    Botkyrka, Sweden				     
 Location 
 Population 
 % Foreign Born 
 % Migrants 
 Year 
 Botkyrka 
 86,274 
 39.00% 
 0.00% 
 2012 
 Sweden 
 9,340,682 
 14.30% 
 0.00% 
 2012 
 City website:   Botkyrka Kommun 
 Source: 
 Statistics Sweden 
 The Dilemma Workshop  
 Botkyrka Kommun 
 
            October 31, 2013             close class="ko-date" 
 Living and working together in the intercultural city means challenging discrimination and implicit bias one person at a time 
 A teenager takes his younger sister to the library. The librarian looks at him, sees his Middle Eastern background and wonders whether the girl is “oppressed.” The siblings pick up on the tension as they check out books. Has the librarian rushed to judgment? Has the library ceased to be a comfortable place for them? 
 This “intercultural dilemma” is the kind of experience discussed within an innovative workshop model designed for city staff in  Botkyrka , a district within the larger municipal region of Stockholm. 
 Botkyrka is Sweden’s most ethnically diverse municipality, with the highest percentages of first and second generation immigrants in the country. More than 50% of Botkyrka’s population has roots outside of Sweden. In one neighbourhood, 90% of inhabitants come from a migrant-origin background. The urban-rural split in the region’s diversity is especially striking, “so different and distant that they might as well be on different planets.” 
 These disparities became a matter of concern to Botkyrka’s civic leadership when they learned that many residents with a migrant background did not identify with Botkyrka as “home” but as a “place of transit.” Overcoming this divide was essential to the district’s future outlook. The city needed an  integrated strategy  that respected diversity and could ensure that all Botkyrka residents felt part of the same community and could work together to realize its vision of equality and opportunity for all “boys and girls, women and men who have Botkyrka as their home, regardless of social and ethnic background.”  The district-wide strategy must also recognize the relative youth of Botkyrka’s population as an important asset (average age 36) within the larger regional development plan. 
 Bringing interculturalism to Botkyrka 
 Botkyrka has a history of forward thinking. It established the first “one stop shop” for government services in Sweden. And it elected Sweden’s first foreign-born mayor in the 1970s. In 2010, Botkyrka joined the  Council of Europe’s Intercultural Cities  movement to bring its intercultural strategy home under the slogan, “Botkyrka, far from average.” 
 Yet introducing  interculturalism  would be no easy matter. One of Botkyrka’s challenges was to translate its aspirations into an action plan that could deliver results. A 15-year plan was developed with short- and long-term goals at each five-year marker. Importantly, the plan begins with the city government itself. As first responders and service delivery agents, the district’s administration employees and managers had to buy into the intercultural strategy in order to apply it effectively. City authorities recognized it is one thing to introduce an intercultural approach and another to have residents and civic employees understand it. 
 “One of the goals of the Intercultural Strategy is to narrow, and not increase, the distance between people with immigrant backgrounds and people with traditional Swedish backgrounds,” says Helena Rojas, Director, Division for Democracy, Human Rights &amp; Intercultural Development. 
 Playing games with intercultural intelligence 
 The Botkyrka Intercultural Strategy’s first goal was to incorporate a non-discriminatory and intercultural approach as a core competency for district managers and employees. To make sure the strategy was understood within the civil service, the district offered “Intercultural Dilemma Workshops” where participants could analyze situations of intercultural conflict and learn how to overcome them. The aim is to break down stereotypes and accept differences within the workplace while developing the intercultural ‘intelligence’ needed to respond appropriately to one’s own implicit bias in new or unexpected situations. 
 “We have an expectation from our employees,” says Rojas, “You can’t bring your preconceived ideas about people to work.” For Rojas, this means finding ways for the librarian to understand her biases. Botkyrka’s Dilemma Workshops offer city employees a trusted space and trained professionals with whom to build the intercultural awareness and competencies they need to do their job. 
 Launched in mid-2011 across all eight divisions of the public service, each half-day Dilemma Workshop begins with an introduction to the concept of interculturality. The main exercise is to discuss a number of real-life case studies from within municipal departments, including examples offered by participants themselves. Participants are placed into groups where they are encouraged to walk through the case study scenario or describe their own experience, and then analyze and discuss how intercultural skills, or a lack thereof, affects the outcome. Next, workshop facilitators help participants look at the impact of “structural conditions,” such as language barriers, cultural differences or institutional culture on the situation as well as explain what needs to change for the situation to improve. The aim is to create a safe space for intercultural learning that includes room for debate on the pros and cons of different solutions. The question governing any proposed solution is: how will this outcome reflect on Botkyrka and its services? Will it help the city achieve its goals? Finally, the group plans coming steps (short-term and long-term) and how to support individual employees. 
 Essential to the success of the workshop is the ability of the facilitator to provide a trusting environment. Participants need to be able to speak freely about situations they have experienced where misunderstandings, biases or prejudices may have had a negative impact on communication with colleagues or the public. Without that sense of safety, tough questions and issues, such as the case of the library encounter, would remain unanswered. 
 “One of the keys to the success of the dilemma workshop is to create trust, and an environment where people can share their experiences without feeling like ‘I am not good.’ You have to create that atmosphere for people to tell their stories and not be ashamed of these things,” says Rojas. 
 Success 
 From the individual employee and section head to the wider organization and public, the intercultural workshop aims to develop a continuous support structure that results in systemic change. “The individual always has responsibility [for him- or herself] but the district also has a responsibility to support the individual. That’s the Dilemma Workshop. Everyone has a share of the responsibility,” says Rojas. 
 In the case of the librarian, the department was able to start a working group where employees could bring such issues up for discussion with colleagues. The Dilemma Workshops have proven to be one the key tools to incorporate interculturalism within the district government: 12 workshops have already been held within 8 departments. Botkyrka’s Dilemma Workshop model has been recognized as a best practice by the Council of Europe’s Intercultural Cities program. 
 Making it Work for You: 
 Managing interpersonal conflict in the workplace requires both sensitivity and clear guidelines about rights, responsibilities and forms of redress. 
 A successful “dilemma workshop” model includes a trusting environment where participants are able to discuss situations where they felt uncomfortable, wronged or may have acted unfairly towards a colleague or client. 
 Developing cultural competencies means all parties – newcomers and the  status quo  – have to be prepared to learn, adapt and possibly change the way they interact and do their work. 
   
 Tags:  anti-discrimination ,  Botkyrka ,  communication skills ,  intercultural competencies   
 <div class="categorylisting">
				    <p>
				    :
				    				    </p>
				</div> 
 
				Themes:  Democratic institutions ,  Diversity ,  Employers and buyers of goods and services ,  Human resource management ,  Training ,  Work   
 Back to Top 
 

	<a href="javascript:window.print();"><img src="http://citiesofmigration.ca/wp-content/themes/citiesofmigration/images/printer.png" alt="" border="0"  />Print</a>
	<a href="https://secure.campaigner.com/CSB/Public/Form.aspx?fid=1351270" target="_blank"><img src="http://citiesofmigration.ca/wp-content/themes/citiesofmigration/images/email.png" alt="" border="0"  />E-Newsletter Sign Up!	</a>
    </div>
    <div id="bottom-widgets">
	<table cellspacing="0" cellpadding="0" border="0">
	    <tr>
		<td width="40%" valign="top">
			<a href="http://citiesofmigration.ca/feed/rss/"><img src="http://citiesofmigration.ca/wp-content/themes/citiesofmigration/images/rss.gif" alt="RSS" border="0" style=""  /> Rss</a></td>
		<td align="left">
			<div id="SendAFriendMsg" style="display: inline; position: static;">
				<a href="javascript:ToggleLoginBox()"><img border="0" alt="" src="/wp-content/themes/citiesofmigration/images/friend.gif" class="sendAfriendImg">Email to a friend</a>
			</div>
		</td>
	    </tr>
	</table>
	    <script type="text/javascript">function ToggleLoginBox() { 

						ToggleDisplayBox = document.getElementById('SendAFriendBox'); 

						ToggleDisplayMsg = document.getElementById('SendAFriendMsg');

						

						

						

						if(ToggleDisplayBox.style.display == 'none'  ) { 

						

						ToggleDisplayBox.style.display = 'block'; 

						ToggleDisplayBox.style.position = 'inline'; 

						ToggleDisplayMsg.style.display = 'none'; 

						ToggleDisplayMsg.style.position = 'relative'; 

						bitDisplay= 0; 

						} 

						else if(ToggleDisplayBox.style.display == 'inline' || ToggleDisplayBox.style.display == 'block') { 

						ToggleDisplayBox.style.display = 'none';  

						ToggleDisplayBox.style.position = 'relative'; 

						ToggleDisplayMsg.style.display = 'inline'; 

						ToggleDisplayMsg.style.position = 'static'; 

		

					}

					}</script><div id="SendAFriend" style=" margin:0; padding:0; display:block;"><div style="position:static;display:none;" id="SendAFriendBox"><div  class="top"></div><div class="middle" ><form action="?" method="post" name="SendToAFriendFrom" id="SendToAFriendFrom" style="margin:0; padding:0;"><div class="formLabels"><strong>Your Name:</strong> <span class="validation">*</span></div><div class="formFields"><input type="text" id="fromName" name="fromName" value="" maxlength="255" size="20"  /></div><div style="clear:left"></div><div class="formLabels"><strong>Quick message to your friend (100 characters max ):</strong> </div><div class="formFields"><textarea id="sendingWhy" name="sendingWhy" rows="3" cols="23" wordwrap></textarea></div><div style="clear:left"></div><div class="formLabels"><strong>Friend's Email:</strong> <span class="validation">*</span></div><div class="formFields"><input type="text" id="strEmail" name="strEmail" value="" maxlength="255" size="20"  /></div><div style="clear:left"></div><input type="hidden" name="xaction" value="sendToaFriend"> <input type="hidden" name="token" value="535f55931684f50d923f6fbb64f69681"><div class="clr"></div><input class="btn" type="submit" value="Send"><input class="btn" type="button" onclick="javascript:ToggleLoginBox()" value="Cancel"><p id="formInst">All fields marked with an asterisk <span class="validation">*</span> are mandatory.</p></form></div><div id="news-bottom"></div></div></div>	    
	     
 Newsletter 
 Events 
 Twitter 
 Facebook 
 Youtube 
   
 Submit a Good Idea 
   
  start: contacts  
 For this Good Idea contact: 
 
				    Helena Rojas, Botkyrka Kommun 				    Director, Division for Democracy, Human Rights & Intercultural Development, 				    Munkhättevägen 45 				    Botkyrka, 				    				    Sweden, 				     147 85 				    				     helena.rojas(at)botkyrka.se				     http://www.botkyrka.se   
  end: contacts  
 Webinars: 
 Webinar: Immigrants Wanted! Smart Strategies for Advancing Welcoming Communities and Regional Growth 
  end: media  
  start : lib items  
 For further reading : 
 Strategy for an Intercultural Botkyrka 
 Municipality of Botkyrka: Intercultural Profile 
 Go to library 
  end: lib items  
 Copyright &copy; Cities of Migration 
 Contact Us 
 |  Disclaimer 
 |  Site Map 
 |  Privacy Policy 
 Maytree 

 Creating a "safe space" for intercultural exchange through group dialogue on real-life case studies across municipal departments and with effective outreach and inclusion of the community. 

 Creating a "safe space" for intercultural exchange through group dialogue on real-life case studies across municipal departments and with effective outreach and inclusion of the community.
 WHOLE PROJECT MARK