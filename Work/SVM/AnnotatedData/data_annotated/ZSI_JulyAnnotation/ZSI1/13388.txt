Discover C4H, a social innovation project funded by the EU
===============================

 
 
                    
                    This project has received funding from the European Union&rsquo;s Seventh Framework Programme for research, technological development and demonstration under grant agreement no 602386 
  jQuery  
Credits4Health | The program that helps you following a healthy lifestyle
===============================

 
  Body  
  Header  
   
   
 This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no 602386 
 Home About C4H Background  The C4H Concept Objectives The C4H Approach How it works The Credits System Consortium’s Partners Events Press Review Contacts 
  Begin Content  
 Healthy Life-style 
 CREDITS 4 HEALTH  is a project funded by the  European Commission      under the  FP7 Framework Programme      and run by a Consortium consisting of national and local government bodies, enterprises, non-profit organizations, universities and research centres. It is a Social Innovation and Health Promotion Project aimed at finding out and testing a system effective in engaging, nurturing and keeping people committed in the adoption of personalised wellness paths and healthy life-styles. The main goal of the C4H project is to develop a sustainable system that encourages people living in Euro-Mediterranean Countries to enhance their level of physical activity and adopt healthy eating habits by means of a person-centric approach and a variety of incentives. The model will be tested in three European Countries (Greece, Italy, and Spain), with the long-term objective of validating the C4H approach and extending it to all of Europe. 
   
 PRESS REVIEW 
 INDUSTRIAL PARTNER 
 
<div class="bottone-footer bottone-uno captione"><a class="link-bottoniera" href="/deliverables-and-result"><img src="/images/bottoniera/c4h_documents.png" alt=""  /><span>DELIVERABLES<br  />AND RESULTS</span></a></div>
 
  <a href="http://www.credits4health.es" target="_blank">  
  <a href="http://www.credits4health.gr" target="_blank">  
 The research leading to these results has received funding from the European Union’s Seventh Framework Programme Managed by UNIFI Università degli studi di Firenze, FP7/2007-2013 under Grant Agreement no. 602386. Copyright © 2013-2016 
  End Content  
  Footer  
 
					Torna su				 
 	<p>
				&copy; 2018 C4h_EU_Rifacimento			</p>  
Industrial Partner
===============================

 
  Body  
  Header  
   
   
 This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no 602386 
 Home About C4H Background  The C4H Concept Objectives The C4H Approach How it works The Credits System Consortium’s Partners Events Press Review Contacts 
  Begin Content  
 
				C4H Call for partnership			 
 The Credits4Health Consortium is searching for enterprises to enter into a commercial partnership in the year 2016. 
 Commercial Partners are enterprises that produce and/or sell products and services which can be used for rewarding the users of the C4H Platform, that in 2016 will reach a maximum of 2,100 people. These users are located in the following areas: city of Florence (Italy), Salento region (Italy), city of Girona (Spain), Kalamata and Pylos Nestoras (Greece). 
 Requirements 
 The partners are required to offer discounts on a selection of their products/services to the end-users of the C4H platform. 
Examples of the typology of products/services that would suit the offer are as follows: 
 Healthcare and wellbeing 
 Sport and Physical activity Industry 
 Food industry 
 Entertainment and social interactions 
 Household and Personal products 
 Information technology and electronics 
 Telecommunication services 
 Consumer durables and apparel 
 Charity donations 
 In order to become C4H commercial partner, the enterprise has to meet the following requirement: The products and services offered are to be in line with the health objectives pursued by C4H and its end-users, facilitate the progress versus a healthier life-style and wellbeing of the users, and never harm their health status. 
 This fundamental requirement will be specifically assessed by the C4H Board. 
 The value of being a C4H Commercial Partner 
 The participation in the C4H initiative would offer the Commercial Partners advantageous opportunities related to: 
 Reputation and brand image 
 Stock turnover 
 Increased market retail 
 Increased sales 
 Advertising 
 In particular, the Commercial Partner would be offered: 
 The opportunity to be officially named as a partner of the European Commission funded project Credits4Health. 
 A proper visibility to the company within the dissemination activities related to the C4H project (e.g. the sponsor area in the official C4H website; the local C4H websites where the enterprise will provide its services; the local dissemination campaign designed to recruit the participants, in Italy, Greece, and Spain; proper visibility of the enterprise’s product/services in the C4H platform). 
 An option on a future partnership in case of a further follow-up of Credits4Health, after the end of the project (August 2016). 
 In case your enterprise is interested, please contact the C4H staff by sending a mail to  Questo indirizzo email è protetto dagli spambots. È necessario abilitare JavaScript per vederlo. 
 Become a partner of the Credits4Health project!   
 PRESS REVIEW 
 INDUSTRIAL PARTNER 
 
<div class="bottone-footer bottone-uno captione"><a class="link-bottoniera" href="/deliverables-and-result"><img src="/images/bottoniera/c4h_documents.png" alt=""  /><span>DELIVERABLES<br  />AND RESULTS</span></a></div>
 
  <a href="http://www.credits4health.es" target="_blank">  
  <a href="http://www.credits4health.gr" target="_blank">  
 The research leading to these results has received funding from the European Union’s Seventh Framework Programme Managed by UNIFI Università degli studi di Firenze, FP7/2007-2013 under Grant Agreement no. 602386. Copyright © 2013-2016 
  End Content  
  Footer  
 
					Torna su				 
 	<p>
				&copy; 2018 C4h_EU_Rifacimento			</p>  
A health preventive system based on new technologies
===============================

 
  Body  
  Header  
   
   
 This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no 602386 
 Home About C4H Background  The C4H Concept Objectives The C4H Approach How it works The Credits System Consortium’s Partners Events Press Review Contacts 
  Begin Content  
 
				Objectives			 
 CREDITS 4 HEALTH  is a Social Innovation and Health Promotion Project aimed at finding out a system effective in engaging, nurturing and keeping people committed in the adoption of personalised wellness paths and healthy life-styles. Its main goal is to develop a sustainable and scalable system based on the “credits for health” concept that encourages people living in Euro-Mediterranean Countries to  enhance their level of physical activity  and  adopt healthy eating habits . The model designed to obtain these results will be tested in three European Countries (Greece, Italy, and Spain), with the long-term objectives of validating the C4H approach and extending it to all of Europe. 
 C4H is based on in-depth and specific analysis carried out on similar projects, with the aim of capitalising best practices in the Health Sector, especially methodologies and standards for the clinical, social and methodological outcomes of field tests.   
 PRESS REVIEW 
 INDUSTRIAL PARTNER 
 
<div class="bottone-footer bottone-uno captione"><a class="link-bottoniera" href="/deliverables-and-result"><img src="/images/bottoniera/c4h_documents.png" alt=""  /><span>DELIVERABLES<br  />AND RESULTS</span></a></div>
 
  <a href="http://www.credits4health.es" target="_blank">  
  <a href="http://www.credits4health.gr" target="_blank">  
 The research leading to these results has received funding from the European Union’s Seventh Framework Programme Managed by UNIFI Università degli studi di Firenze, FP7/2007-2013 under Grant Agreement no. 602386. Copyright © 2013-2016 
  End Content  
  Footer  
 
					Torna su				 
 	<p>
				&copy; 2018 C4h_EU_Rifacimento			</p>  
An informed path against harmful habits
===============================

 
  Body  
  Header  
   
   
 This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no 602386 
 Home About C4H Background  The C4H Concept Objectives The C4H Approach How it works The Credits System Consortium’s Partners Events Press Review Contacts 
  Begin Content  
 
				The C4H Concept			 
 We all know that, in the long term, smoking, over-consuming alcohol, eating unhealthy food, and staying inactive, pose serious threats to our health and well-being: so why is it so difficult to convince people to turn harmful rooted habits into wellness preserving actions? 
 The C4H concept is based on one fundamental game-changing idea that needs to be implemented in order to foster the transition to a new approach towards health and well-being: people engagement or, in other words,  people empowerment . This represents a radical shift from a one-way delivered healthcare to a multi-stakeholder approach in which people play the pivotal role, being actively involved in maintaining and improving their health status. The C4H vision is to have people directly acting for their health and wellbeing, thus contributing to the prevention of chronic diseases and enhancing the quality of their lives.   
 PRESS REVIEW 
 INDUSTRIAL PARTNER 
 
<div class="bottone-footer bottone-uno captione"><a class="link-bottoniera" href="/deliverables-and-result"><img src="/images/bottoniera/c4h_documents.png" alt=""  /><span>DELIVERABLES<br  />AND RESULTS</span></a></div>
 
  <a href="http://www.credits4health.es" target="_blank">  
  <a href="http://www.credits4health.gr" target="_blank">  
 The research leading to these results has received funding from the European Union’s Seventh Framework Programme Managed by UNIFI Università degli studi di Firenze, FP7/2007-2013 under Grant Agreement no. 602386. Copyright © 2013-2016 
  End Content  
  Footer  
 
					Torna su				 
 	<p>
				&copy; 2018 C4h_EU_Rifacimento			</p>  
Regular exercise and a healthy diet in the interest of the health 
===============================

 
  Body  
  Header  
   
   
 This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no 602386 
 Home About C4H Background  The C4H Concept Objectives The C4H Approach How it works The Credits System Consortium’s Partners Events Press Review Contacts 
  Begin Content  
 
				Background			 
 The world’s health is undergoing an unprecedented transition on several fronts, particularly concerning epidemiological, nutritional and demographic issues. Many researches confirm the looming pandemic of  Non Communicable Diseases and Chronic Diseases  (NCD, CD), which represent the top cause of death worldwide, killing more than 36 million people in 2008. 
 More than 9 million of these deaths occurred before the age of 60, and could have been largely prevented, tackling risk factors like physical inactivity, raised blood pressure, overweight and obesity, raised cholesterol, tobacco smoking, and raised blood glucose. 
 This situation is very well schematised by the  3-four-50 model , which points out how three behaviours (poor diet, physical inactivity, and tobacco use) contribute to four chronic diseases (cancer, heart disease and stroke, type 2 diabetes, and pulmonary diseases such as asthma) that cause over 50% of all deaths worldwide. This clearly points out how important wellness is to individuals and countries as a whole. Therefore a crucial issue our societies have to address is fighting the increasing prevalence of lifestyle related diseases . 
   
 PRESS REVIEW 
 INDUSTRIAL PARTNER 
 
<div class="bottone-footer bottone-uno captione"><a class="link-bottoniera" href="/deliverables-and-result"><img src="/images/bottoniera/c4h_documents.png" alt=""  /><span>DELIVERABLES<br  />AND RESULTS</span></a></div>
 
  <a href="http://www.credits4health.es" target="_blank">  
  <a href="http://www.credits4health.gr" target="_blank">  
 The research leading to these results has received funding from the European Union’s Seventh Framework Programme Managed by UNIFI Università degli studi di Firenze, FP7/2007-2013 under Grant Agreement no. 602386. Copyright © 2013-2016 
  End Content  
  Footer  
 
					Torna su				 
 	<p>
				&copy; 2018 C4h_EU_Rifacimento			</p>  
Discover C4H, a social innovation project funded by the EU
===============================

 
 
                    
                    This project has received funding from the European Union&rsquo;s Seventh Framework Programme for research, technological development and demonstration under grant agreement no 602386 
  jQuery  
C4H is a project managed by distinguished international organizations
===============================

 
  Body  
  Header  
   
   
 This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no 602386 
 Home About C4H Background  The C4H Concept Objectives The C4H Approach How it works The Credits System Consortium’s Partners Events Press Review Contacts 
  Begin Content  
 
				Consortium’s Partners			 
 Università degli Studi di Firenze (Italy)
		 
 Istituto Scientifico Biomedico Euro-Mediterraneo (Italy)
		 
 Fundaciòn para la Investigaciòn Nutricional (Spain)
		 
 Ludwig Maximilians University (Germany)
		 
 University of Ioannina (Greece)
		 
 FIRMO - Fondazione Italiana per la Ricerca sulle Malattie Ossee (Italy)
		 
 Hellenic Health Foundation (Greece)
		 
 Freie Universit&auml;t Berlin (Germany)
		 
   
 Labor Srl (Italy)
		 
 ATB - Institut für angewandte Systemtechnik Bremen GmbH (Germany)
		 
 Looking for Value Srl (Italy)
		 
   
 Ministero della Salute (Italy)
		 
 Azienda Sanitaria Regionale Regione Puglia (Italy)
		 
   
 Adnkronos Comunicazione Srl (Italy)
		 
 C3 - Collaborating for Health (United Kingdom)
		 
   
 Generalitat de Catalunya Girona (Spain)
		 
 Generalitat de Catalunya (Spain)
		 
 Escola Universitària de la Salut i l'Esport (Spain)
		 
 Municipality of Kalamata (Greece) 
 Volunteer Committee of Municipality of Kalamata (Greece)
		 
   
 PRESS REVIEW 
 INDUSTRIAL PARTNER 
 
<div class="bottone-footer bottone-uno captione"><a class="link-bottoniera" href="/deliverables-and-result"><img src="/images/bottoniera/c4h_documents.png" alt=""  /><span>DELIVERABLES<br  />AND RESULTS</span></a></div>
 
  <a href="http://www.credits4health.es" target="_blank">  
  <a href="http://www.credits4health.gr" target="_blank">  
 The research leading to these results has received funding from the European Union’s Seventh Framework Programme Managed by UNIFI Università degli studi di Firenze, FP7/2007-2013 under Grant Agreement no. 602386. Copyright © 2013-2016 
  End Content  
  Footer  
 
					Torna su				 
 	<p>
				&copy; 2018 C4h_EU_Rifacimento			</p>  
Events
===============================

 
  Body  
  Header  
   
   
 This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no 602386 
 Home About C4H Background  The C4H Concept Objectives The C4H Approach How it works The Credits System Consortium’s Partners Events Press Review Contacts 
  Begin Content  
 Events 
  Modulo nuovo  
 Credits4Health Sport Village Florence, 7 July 2016 
 Credits4Health in Florence: a great success! 
 A 'health village' gave the opportunity to many Florentines to practice outdoor sports from group cycling, to Nordic Walking and other related activities. The event, called “A Firenze per la salute con Credits4Health”, organized by Unifi and F.I.R.M.O. to promote the culture of a healthy lifestyle, took place last July 7th at the Piazzale Michelangelo. 
 In the stands set up, doctors and nurses have carried out free MOC and Calcium Calculator exams, suggesting to participants the basic rules for a correct lifestyle and a healthy diet. The C4H (Credits4Health) project of social innovation, funded by the European Union, is designed to raise awareness and encourage people to undertake and maintain a healthy lifestyle. 
 "The customized platform Credits4health studies the psycotype of the person connecting - says Prof. Maria Luisa Brandi president of FIRMO Foundation - and awards those who behave well. The European population is thankfully too old, and C4H represents a unique example to help people in choosing a correct lifestyle". 
 "Finally we have an event totally focused on health and prevention - says Sara Funaro in charge of Welfare and Health for the Florence Municipality - we think that the policies that affect health must always be centered more on actions such as these." "An event that promotes the sport through a unique setting that only Florence can offer - comments Andrea Vannucci responsible for Sports of the Florence Municipality – always keeping a keen eye to all message carried out through sports." 
  FINE Modulo nuovo  
  Modulo nuovo  
 Girona per la Salut amb Credits4Health Girona, 21 May 2016 
 A healthy Fair in the city of Girona. 
 As part of the Credits4Health Girona project, a Community Health Fair was organized in the city of Girona. 
 The event “Girona per la Salut amb Credits4Health” (Girona for Health with C4h), was held on 21 May 2016 from 10:00 to 15:00h in the Girona City Hall Sports Complex “Pavelló de Fontajau”. Participants and accompanying friends or family from all three pilots were invited. However, it was an event that was open to the general public from Girona and the surrounding areas. 
 It was organized by the Nutrition Research Foundation (FIN) in collaboration with the third parties Agència de Salut Pública de Catalunya de la Generalitat and EUSES, as well as the support of Generalitat Girona and the Girona City Hall (Ajuntament de Girona) and the collaboration of 15 private businesses and 3 associations from the areas of health, fitness and nutrition. Volunteers were also recruited to collaborate with setting up and running the activities. These individuals were identified by wearing a T-shirt with the C4H logo to assist attendees throughout the event. 
 The C4H health fair included a variety of activities and workshops related to nutrition, physical activity and health. Most targeted the general public but specific activities were programmed especially for children. A Master of Ceremonies was hired to engage and inform the public about the different activities that were programmed and encourage them to participate. 
 Poster of the event 
 Program of the event 
  FINE Modulo nuovo  
 C4H at the Physical Activity Workshop Glasgow, Scotland 10 March 2016 
   Credits4Health is one of the 5 projects promoting physical activity in the EU 
 On 10 March, a workshop entitled “Public Health research - exploring physical activity for health and fun” was held in Glasgow (Scotland). 
 The EU promoted the day through the Directorate General for Research &amp; Innovation E3 - Fighting infectious diseases and advancing public health. 
 The European Union decided to join together the 5 projects, which it is fully funding. Their aims implement physical activity, for an exchange of experiences and of methodologies. 
 It comes as no surprise that the workshop was held in Glasgow’s stadium: the introduction to the works by Rachida Ghalouci from the DG for Research and Innovation in fact underscored how the European Union’s objectives include having 100 million more citizens active in sport and physical activity by 2020. 
 This ambitious goal is pursued through attention to legislative proposals and to the management of public mobility, by developing and supporting policies in favour of sport and physical activity. 
 All the projects go in the direction recommended by the DG for Health, which determines the priority strategies on nutrition and health risks connected with weight and obesity, promoting a balanced diet and active lifestyle, particularly for children, the elderly, and socially disadvantaged citizens. 
 Each of the 5 projects was presented by the coordinator and followed by sessions examining in greater depth the methodologies used and applied in the various countries in the EU. 
  FINE Modulo nuovo  
 A promotional tour for C4H Salento area, November 2015 
 Credits4Health went inside salento’s theaters 
 The  cultural association "Li Satiri"  is very well known locally in the Salento Area. They contributed to the recruitment of participants by promoting the C4H project with various activities. After their Theatre Performances they did direct promotion describing the project to the audience, while they distributed leaflets within schools, gyms, working places, fairs, parishes etc. to attain the largest possible number of people. 
  FINE Modulo nuovo  
 A cooperation between C4H and Girona Municipality Girona, 30 November 2015 
 Generalitat and Ajuntament de Girona with C4H 
 La Generalitat (Municipality) of Girona and the l’Ajuntament de Girona prensented on the occasion of a  press conference  the cooperation with the Credits4Health project. The C4H project was described during the press conference by spanish partner FIN, represented by Prof. Lluis Serra Majem. 
 <a rel="glasgov" class="jcepopup" title="" href="/images/eventi/big/Glasgow-03_big.jpg">
				<img class="jcepopup" src="/images/eventi/small/Glasgow-03_small.jpg" alt=""  />
			</a> 
 			<a rel="glasgov" class="jcepopup" title="" href="/images/eventi/big/Glasgow-04_big.jpg">
				<img class="jcepopup" src="/images/eventi/small/Glasgow-04_small.jpg" alt=""  />
			</a> 
  FINE Modulo nuovo  
 C4H at the the local saint Narcis festivities in Girona Girona, 25 October 2015 
 A tent for promoting C4H during the huge event of Girona 
 In Girona, during  the local saint Narcis festivities  (saint patron of Girona) when a great part of the population is on the streets, a lots of materials about C34H project have been distributed to recruit participants. 
 Under a tent with logos and posters of the project, the students gave information about the project and collected forms of persons interested that were subsequently contacted by Fin. 
  FINE Modulo nuovo  
 Credits4Health al “Bright” Florence, 25 September 2015 
 Prof. Maria Luisa Brandi to be on hand at European Researchers’ Night 
 The night of 25 September will be illuminated by the light of research. With  Bright2015  Tuscany will celebrate  European Researchers’ Night , an initiative promoted by the European Commission in 300 cities in 24 countries to spread the culture of science and knowledge of research professions. 
For  Bright2015 , Prof. Brandi, the coordinator of Credits4Health, will present the project named " Stili di vita e prevenzione. Dal concetto Nudge al progetto C4H " ("Lifestyles and prevention. From the concept of ‘nudge’ to the C4H project"). Her contribution is scheduled to start at 5:40 PM at the University of Florence’s Botanical Garden, within the setting of the thematic section entitled “Salute!” (“Health!”).  
Prof. Brandi will describe the project’s “State of the Art,” commenting upon the  results obtained  based on the first two trial phases and introducing the third and final phase set to kick off during these days in Italy, Spain, and Greece, which will involve  more than 2,100 participants .
 
 For more information, visit the  official website .
 
 "Ethics and Food" Conference Milan, 19 June 2015 
 Initial data of the Credits4Health project presented at the "Ethics and Food” Conference 
 On 19 June 2015, the  seventh conference of the cultural entrepreneurship series, entitled "Ethics and Food," , sponsored by the Scientific Committee of Expo Milano 2015 and organized by the   Berlin’s Cultural Entrepreneurship Institute  was held in Milan. 
 Prof. Maria Luisa Brandi was invited to speak at this important event and, as coordinator of the Credit4Health project, discussed the project’s status, presenting the first results regarding the statistics of the participants in C4H. In particular, she illustrated the gender and age breakdown of the volunteers recruited in the Florence and Salento areas in Italy; Pylos-Nestoras in Greece; and Girona in Spain, stressing how out of about 900 volunteers, the highest percentage is represented by women between 38 and 45 years of age (see  presentation slides ). 
 In concluding, Maria Luisa Brandi announced the start, for September 2015, of the third phase of trialling, which will end in mid 2016 and whose main objective is to finalize the trial by recruiting 2,100 more participants and asking everyone to visit the  project’s official website .
 
 Download the C4H presentation at "Ethics and Food" 
 Press conference for the launch of the project Credits4Health Rome, 9 December 2014 
  <h2><a href="http://www.credits4health.eu/" target="_blank"><strong>Credits4Health presented in Rome: the first pilot study begins</strong></a></h2>  
 Credits4Health presented in Rome: the first pilot study begins 
 The first pilot study of Credits4Health was presented in Rome, in a press conference attended by the project coordinator Maria Luisa Brandi, professor of endocrinology at the University of Florence, and the Italian Health Minister Beatrice Lorenzin. 
 "Prevention - said the minister Lorenzin - has been a cornerstone of the EU semester. We all realize that the fight against chronic disease prevention can stand if it becomes part of our life. It must be a mantra, but you must intervene on bands of the adult population. This is an interesting project, there are no medications and has an inductive part thanks to the Internet and to correct information. the other aspect is that it becomes part of a community, which helps to change lifestyle, a difficult task. " 
 "In this first phase we want to understand how people react to our platform - said Maria Luisa Brandi, coordinator of the project - because even socialization is crucial in lifestyles. Variables may be important loans, or know to have individualized advice. All information collected in the first two phases of the project will be fundamental to achieve the controlled clinical trial, the third and final phase Credits4Health ". 
 European Health Forum Gastein 1-3 October 2014 
 Credits4Health at the European Health Forum Gastein 2014 
 Credits4Health has been invited by the European Commission to take part to the  17th edition of the EHFG , the leading health policy event in the EU. 
 The project coordinator, Maria Luisa Brandi, presented the Credits4Health project in the framework of the Forum “Moving for Health and Well-being”, aimed at identifying best practices for prevention-oriented interventions in range of European health policies. 
 The coordinator also took part to the break-out session focused on intersectoral and interdepartmental collaboration, contributing to the discussion with the audience in order to pinpoint the main barriers preventing from physical activity and to highlight successful co-operation models. 
 See the Gastein C4H Presentation 
   
 PRESS REVIEW 
 INDUSTRIAL PARTNER 
 
<div class="bottone-footer bottone-uno captione"><a class="link-bottoniera" href="/deliverables-and-result"><img src="/images/bottoniera/c4h_documents.png" alt=""  /><span>DELIVERABLES<br  />AND RESULTS</span></a></div>
 
  <a href="http://www.credits4health.es" target="_blank">  
  <a href="http://www.credits4health.gr" target="_blank">  
 The research leading to these results has received funding from the European Union’s Seventh Framework Programme Managed by UNIFI Università degli studi di Firenze, FP7/2007-2013 under Grant Agreement no. 602386. Copyright © 2013-2016 
  End Content  
  Footer  
 
					Torna su				 
 	<p>
				&copy; 2018 C4h_EU_Rifacimento			</p>  
Plan and monitor your physical activity and nutrition with C4H
===============================

 
  Body  
  Header  
   
   
 This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no 602386 
 Home About C4H Background  The C4H Concept Objectives The C4H Approach How it works The Credits System Consortium’s Partners Events Press Review Contacts 
  Begin Content  
 
				How it Works			 
 The project leverages on three main pillars to enhance the quality of peoples’ lives:  the reduction of sedentary behaviour, active participation to social life, and the adoption of a healthier diet . The proposed action consists of recruiting people living in four cities in Greece, Italy, and Spain, who will be involved in personalized paths to enhance their physical activity and adopt a healthier diet. The following steps will be taken: 
 preliminary assessment of the medical, psychological and social conditions of each person; 
 definition of personal goals relating to nutrition and physical exercise; 
 use of a web-based platform providing a number of functionalities (personal profile, dietary and exercise suggestions, nutrition plans, etc.) and personalized plans aimed at giving psychological and social support to help participants reach their goals. The platform will enable participants to track their compliance with the health-regime agreed with the C4H Professional Staff who will follow their results on a regular basis. 
   
 PRESS REVIEW 
 INDUSTRIAL PARTNER 
 
<div class="bottone-footer bottone-uno captione"><a class="link-bottoniera" href="/deliverables-and-result"><img src="/images/bottoniera/c4h_documents.png" alt=""  /><span>DELIVERABLES<br  />AND RESULTS</span></a></div>
 
  <a href="http://www.credits4health.es" target="_blank">  
  <a href="http://www.credits4health.gr" target="_blank">  
 The research leading to these results has received funding from the European Union’s Seventh Framework Programme Managed by UNIFI Università degli studi di Firenze, FP7/2007-2013 under Grant Agreement no. 602386. Copyright © 2013-2016 
  End Content  
  Footer  
 
					Torna su				 
 	<p>
				&copy; 2018 C4h_EU_Rifacimento			</p>  
Press Review
===============================

 
  Body  
  Header  
   
   
 This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no 602386 
 Home About C4H Background  The C4H Concept Objectives The C4H Approach How it works The Credits System Consortium’s Partners Events Press Review Contacts 
  Begin Content  
  Zona RASSEGNA STAMPA  
 Press Review 
 Updated in October, 2016 
 Spain 
   
 Italy 
     
 Greece 
       
 International 
       
  FINE zona RASSEGNA STAMPA    
 PRESS REVIEW 
 INDUSTRIAL PARTNER 
 
<div class="bottone-footer bottone-uno captione"><a class="link-bottoniera" href="/deliverables-and-result"><img src="/images/bottoniera/c4h_documents.png" alt=""  /><span>DELIVERABLES<br  />AND RESULTS</span></a></div>
 
  <a href="http://www.credits4health.es" target="_blank">  
  <a href="http://www.credits4health.gr" target="_blank">  
 The research leading to these results has received funding from the European Union’s Seventh Framework Programme Managed by UNIFI Università degli studi di Firenze, FP7/2007-2013 under Grant Agreement no. 602386. Copyright © 2013-2016 
  End Content  
  Footer  
 
					Torna su				 
 	<p>
				&copy; 2018 C4h_EU_Rifacimento			</p>  
C4H helps you make informed choices towards a healthy lifestyle
===============================

 
  Body  
  Header  
   
   
 This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no 602386 
 Home About C4H Background  The C4H Concept Objectives The C4H Approach How it works The Credits System Consortium’s Partners Events Press Review Contacts 
  Begin Content  
 
				The C4H Approach			 
 The C4H concept deals with the identification of the right stimuli necessary to ignite a behavioural change in people, leading them to knowingly choose healthier life-styles. Inspired by the  “nudge” concept    , the C4H approach aims to support people in choosing what is best for them, trying to influence their motivations and decision making without any imposition, and preserving their ability to make a different choice. Since generalised approaches have proven to be largely ineffective in driving behavioural changes, we want to prove that the key is individualising the experience around the peculiarities of each and every person. This will help the participants find their own personalised path towards the achievement of a healthy life-style.   
 PRESS REVIEW 
 INDUSTRIAL PARTNER 
 
<div class="bottone-footer bottone-uno captione"><a class="link-bottoniera" href="/deliverables-and-result"><img src="/images/bottoniera/c4h_documents.png" alt=""  /><span>DELIVERABLES<br  />AND RESULTS</span></a></div>
 
  <a href="http://www.credits4health.es" target="_blank">  
  <a href="http://www.credits4health.gr" target="_blank">  
 The research leading to these results has received funding from the European Union’s Seventh Framework Programme Managed by UNIFI Università degli studi di Firenze, FP7/2007-2013 under Grant Agreement no. 602386. Copyright © 2013-2016 
  End Content  
  Footer  
 
					Torna su				 
 	<p>
				&copy; 2018 C4h_EU_Rifacimento			</p>  
An incentive system to support the motivation towards lifestyle change
===============================

 
  Body  
  Header  
   
   
 This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no 602386 
 Home About C4H Background  The C4H Concept Objectives The C4H Approach How it works The Credits System Consortium’s Partners Events Press Review Contacts 
  Begin Content  
 
				The Credits System			 
 People’s active involvement in the achievement of a healthy life-style will be rewarded through a credit system: participants will receive credits when they comply with their periodic goals in terms of physical activity and diet. Credits can be converted into discount vouchers for the purchase of products and services provided by our industrial partners. 
 The aim of the C4H approach is to provide a well-balanced mix of incentives (credits, information, personalized paths), which will enable participants to gradually develop further motivation towards a better lifestyle. 
   
 PRESS REVIEW 
 INDUSTRIAL PARTNER 
 
<div class="bottone-footer bottone-uno captione"><a class="link-bottoniera" href="/deliverables-and-result"><img src="/images/bottoniera/c4h_documents.png" alt=""  /><span>DELIVERABLES<br  />AND RESULTS</span></a></div>
 
  <a href="http://www.credits4health.es" target="_blank">  
  <a href="http://www.credits4health.gr" target="_blank">  
 The research leading to these results has received funding from the European Union’s Seventh Framework Programme Managed by UNIFI Università degli studi di Firenze, FP7/2007-2013 under Grant Agreement no. 602386. Copyright © 2013-2016 
  End Content  
  Footer  
 
					Torna su				 
 	<p>
				&copy; 2018 C4h_EU_Rifacimento			</p>  
Contacts
===============================

 
  Body  
  Header  
   
   
 This project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement no 602386 
 Home About C4H Background  The C4H Concept Objectives The C4H Approach How it works The Credits System Consortium’s Partners Events Press Review Contacts 
  Begin Content  
 
				Contacts			 
 Project Coordinator:  Prof. Maria Luisa Brandi (MD Ph.D, Full Professor of Endocrinology) 
 Address:  Department of  Surgery and Translational Medicine, University of Florence, Viale Pieraccini 6 - 50139 Florence, Italy 
 Email:  Questo indirizzo email è protetto dagli spambots. È necessario abilitare JavaScript per vederlo. 
   
 PRESS REVIEW 
 INDUSTRIAL PARTNER 
 
<div class="bottone-footer bottone-uno captione"><a class="link-bottoniera" href="/deliverables-and-result"><img src="/images/bottoniera/c4h_documents.png" alt=""  /><span>DELIVERABLES<br  />AND RESULTS</span></a></div>
 
  <a href="http://www.credits4health.es" target="_blank">  
  <a href="http://www.credits4health.gr" target="_blank">  
 The research leading to these results has received funding from the European Union’s Seventh Framework Programme Managed by UNIFI Università degli studi di Firenze, FP7/2007-2013 under Grant Agreement no. 602386. Copyright © 2013-2016 
  End Content  
  Footer  
 
					Torna su				 
 	<p>
				&copy; 2018 C4h_EU_Rifacimento			</p> 

 The FP7 project aimed at finding out and testing a system effective in engaging, nurturing and keeping people committed in the adoption of personalised wellness paths and healthy life-styles. 

 The FP7 project aimed at finding out and testing a system effective in engaging, nurturing and keeping people committed in the adoption of personalised wellness paths and healthy life-styles.
 WHOLE PROJECT MARK