Youth.inc - Aġenzija Żgħażagħ

===============================

 
 Search 
 Join Us 
 Home 
 News 
 Press 
 Media 
 Downloads 
 Contact 
 About Us Calls Youth Policy Youth Worker&#39;s Warrant Application Youth Empowerment Programme Youth Information Support to Youth Organisations Gozo Youth Services Youth.inc Register your Youth Organisation European Youth Card No Hate Speech Movement Youth Activity Centres Youth Cafés Youth Hubs Youth Stop Youth Research EU Funded Projects Freedom of Information Publications Useful Links 
 
                    
    You are here:  &raquo;  Youth.inc 
 Youth.inc 
 Who we are? 
 
	Youth.inc is an inclusive education programme, based on applied learning, for young people between the age of 16 and 21. The aim of the programme is to help young people to improve their standard of education and gain more knowledge, values and skills to enter the labour market or gain qualifications to continue in further education and/or training.  
 
	Youth.inc is under the remit and management of Aġenzija Żgħażagħ, which adopts a more youth-centred approach and seeks to strengthen the complementary role of formal and non-formal learning. 
 Validation and Certification 
 
	Youth.inc has two different levels of entry in accordance with the Malta Qualifications Framework. The programme is based on assisting the young person to gain key competences, sectoral skills and underpinning knowledge at the different levels. 
 
	Included in the learning are skills that are transferable to the next stages of education and training: 
 
	&bull; Applying theory to work-related challenges 
	&bull; Acquiring knowledge 
	&bull; Gaining basic skills 
	&bull; Embracing values 
	&bull; Working in a team 
	&bull; Being creative and innovative 
 
	Youth.inc is currently being offered as full-time Level 1, Level 2 and Level 3 programmes at Aġenzija Żgħażagħ complex in Santa Venera.  
 Entry to Youth.inc   
 
	The entry requirement for Level 1 or Level 2 is completion of full time compulsory education. The entry age is 16 years and young people who register for the programme are required to sit for an assessment which determines their participation in either Level 1 or Level 2 depending on their competences and skills. Young people entering Level 1 will progress to Level 2 upon successful completion of Level 1.  
 Maintenance Grants 
 
	Young people attending the Youth.inc programme on a full time basis are entitled to receive a stipend as stipulated in Legal Notice 327.178 Students&rsquo; Maintenance Grants Regulations. 
 How to apply 
 
	You can apply online in  English  or  Maltese . For any queries, send us an email on  agenzija.zghazagh@gov.mt  or call on 22586700 and ask for Mr. Bryan Magro. 
 Share 
 LGBTIQ 
 
	Aġenzija Żgħażagħ 
	St Joseph High Road 
	St Venera SVR 1012, Malta 
 
	Tel: +356 2258 6700 
	Email:  agenzija.zghazagh@gov.mt 
 Privacy Policy Disclaimer Copyright Accessibility Sitemap 
 Government of Malta 
 Website development by Abakus 

	Youth.inc - Aġenzija Żgħażagħ

===============================

 
 Search 
 Join Us 
 Home 
 News 
 Press 
 Media 
 Downloads 
 Contact 
 About Us Calls Youth Policy Youth Worker&#39;s Warrant Application Youth Empowerment Programme Youth Information Support to Youth Organisations Gozo Youth Services Youth.inc Register your Youth Organisation European Youth Card No Hate Speech Movement Youth Activity Centres Youth Cafés Youth Hubs Youth Stop Youth Research EU Funded Projects Freedom of Information Publications Useful Links 
 
                    
    You are here:  &raquo;  Youth.inc 
 Youth.inc 
 Who we are? 
 
	Youth.inc is an inclusive education programme, based on applied learning, for young people between the age of 16 and 21. The aim of the programme is to help young people to improve their standard of education and gain more knowledge, values and skills to enter the labour market or gain qualifications to continue in further education and/or training.  
 
	Youth.inc is under the remit and management of Aġenzija Żgħażagħ, which adopts a more youth-centred approach and seeks to strengthen the complementary role of formal and non-formal learning. 
 Validation and Certification 
 
	Youth.inc has two different levels of entry in accordance with the Malta Qualifications Framework. The programme is based on assisting the young person to gain key competences, sectoral skills and underpinning knowledge at the different levels. 
 
	Included in the learning are skills that are transferable to the next stages of education and training: 
 
	&bull; Applying theory to work-related challenges 
	&bull; Acquiring knowledge 
	&bull; Gaining basic skills 
	&bull; Embracing values 
	&bull; Working in a team 
	&bull; Being creative and innovative 
 
	Youth.inc is currently being offered as full-time Level 1, Level 2 and Level 3 programmes at Aġenzija Żgħażagħ complex in Santa Venera.  
 Entry to Youth.inc   
 
	The entry requirement for Level 1 or Level 2 is completion of full time compulsory education. The entry age is 16 years and young people who register for the programme are required to sit for an assessment which determines their participation in either Level 1 or Level 2 depending on their competences and skills. Young people entering Level 1 will progress to Level 2 upon successful completion of Level 1.  
 Maintenance Grants 
 
	Young people attending the Youth.inc programme on a full time basis are entitled to receive a stipend as stipulated in Legal Notice 327.178 Students&rsquo; Maintenance Grants Regulations. 
 How to apply 
 
	You can apply online in  English  or  Maltese . For any queries, send us an email on  agenzija.zghazagh@gov.mt  or call on 22586700 and ask for Mr. Bryan Magro. 
 Share 
 LGBTIQ 
 
	Aġenzija Żgħażagħ 
	St Joseph High Road 
	St Venera SVR 1012, Malta 
 
	Tel: +356 2258 6700 
	Email:  agenzija.zghazagh@gov.mt 
 Privacy Policy Disclaimer Copyright Accessibility Sitemap 
 Government of Malta 
 Website development by Abakus
 WHOLE PROJECT MARK