Biomass Energy for Employment and Energy Security in Bosnia and Herzegovina | UNDP in Bosnia and Herzegovina
===============================

 
   Around the world 
     BHS 
             |  English 
 Bosnia and Herzegovina 
 Sustainable Development Goals 
 Our focus 
 more 
 content starts 
  breadcrumbs start   
 Home Operations Projects Energy and Environment Biomass Energy for Employment and Energy Security in Bosnia and Herzegovina 
  breadcrumbs end   
 Biomass Energy for Employment and Energy Security in Bosnia and Herzegovina 
 Green Economic Development (GED) Technology transfer for climate resilient flood management in Vrbas River Basin Biomass Energy for Employment and Energy Security in Bosnia and Herzegovina Strengthen Bosnia and Herzegovina Decision-Making Towards Becoming a Party to the Minamata Convention and Build Capacity for Implementation of Future Provisions Regulatory Framework for Tariff Setting in Water Supply and Sewerage Services in BiH South East Europe Urban Resilience Building Action Network (SEE URBAN) Interlinking Disaster Risk Management (IDRM) in Bosnia and Herzegovina Disaster Risk Reduction Initiative in Bosnia and Herzegovina Third National Communication of Bosnia and Herzegovina under the United Nations Framework Convention on Climate Change 
 Biomass Energy for Employment and Energy Security in Bosnia and Herzegovina 
 What is the project about? 
 Assistance provided for the facilitation of wood biomass marketing and promotional activities within BiH 
 More than 53% of the territory of Bosnia and Herzegovina is covered with forests and forest land. However, despite of the significant potentials for production of energetic sources based on wood, its is still predominantly used as fuelwood for individual households heating. 
 
Furthermore, insufficient activity of private sector characterizes renewable energy sector. Limited incentives for renewable energy infrastructure projects from wood biomass represent a significant impediment to renewable energy sector development, job creations in BiH and export increase.    
 
With aim to support sustainable use of wood biomass and development of renewable energy sector in BiH, since 2016  Czech Development Agency  (CzDA) has been financing three-years long project „Biomass Energy for Employment and Energy Security in Bosnia and Herzegovina“ that has been implemented by UNDP. 
 
Partners in the realization of activities are  Ministry of foreign trade and economic relations of BiH ,  Embassy of the Czech Republic in BiH ,  Federal ministry of agriculture, water management and forestry ,  Ministry of agriculture, forestry and water management of Republika Srpska  and  Government of Brcko District of BiH . 
 What we do? 
 The overall objective of the Project is long-term reduction of CO2 emissions and improvement of local population living standard by supporting the sustainable use of wood biomass through strategic action, establishment of market value chain framework and awareness rising of general public on positive aspects of utilization of this energy source.    
 
Project strive to contribute to the overall tCO2 emission reduction per capita in BiH for 3% by the year 2030, based on the globally accepted baseline of the year 1990 (emissions of 8.97 tCO2 per capita). According to the  Emission Database for Global Atmospheric Research  (EDGAR), total emission per capita in BiH in 2014 amounted to 6,18 tCO2. 
 Project contributes to the following: 
 Enhancement of secure energy supply 
 Foster the development of enterprises for processing locally available wood biomass 
 Set up sustainable partnerships, that contribute to the economic development of micro-regions. 
 The project has been implementing through following three components: 
 Development of polices for sustainable wood biomass utilization  - reviewing of legislation in forestry and energetic sectors, support to the preparation of adoption of appropriate legislative framework, revision of Renewable energy action plans, strengthening the capacities of decision-makers, private sector representatives and local communities in the segment of wood biomass in BiH. 
 Improve the quality and availability of the wood biomass, as an energy carrier for heating purposes  – establishment of an multi-disciplinary coordination mechanism of relevant institutions from forestry and energetic sectors in BiH, mapping and quantification wood biomass exploitation, support to the establishment of forest governance mechanisms to increase utilization of wood biomass for energy purposes, awareness rising of general public on positive aspects of utilization of wood biomass in BiH. 
 Realization of renewable energy infrastructure projects  – development of business models and financial mechanisms for financing infrastructural wood biomass fuel switch projects in public buildings, establishment of the wood biomass Innovation Centre, installation of new heating systems on wood biomass in four (4) public buildings in BiH. 
 Expected Project results 
 Study on identification of policy gaps within the energy and forestry sectors in segment of wood biomass in Bosnia and Herzegovina prepared. 
 Study on comprehensive overview of potential policy and legislative framework amendment recommendations of forestry and energetic sectors in segment of wood biomass and Guide on best practices and operational methods to increase utilization of wood biomass prepared. 
 Study tour to Czech Republic for key policy decision makers in segment of wood biomass in BiH organized. 
 Trainings modules on wood biomass business models for representatives of 10 local communities and private sector in field of energy efficiency in BiH organized. 
 Working group on wood biomass in BiH with representatives of relevant institutions from forestry and energetic sectors established. 
 Study on potentials and quantification of wood biomass in BiH prepared. 
 Study on recommendations for adoption/revision of forest governance. mechanisms in the Federation of BiH and Republika Srpska to increase. utilization of wood biomass for energy production prepared. 
 At least 4,500 individuals informed about potentials of wood biomass in BiH. 
 Study on recommendations for introduction of financial mechanisms for wood biomass fuel switch projects in public buildings within Fund for environmental protection of Federation of BiH and Fund for environmental protection and energy efficiency of Republika Srpska prepared. 
 Wood Biomass Innovation Centre within the existing Biomass Association in BiH established. 
 Installation of new heating systems on wood biomass in four (4) public buildings in BiH. 
 What have we accomplished so far? 
 Implemented activities: 
 Working group on wood biomass in BiH with representatives of relevant institutions from forestry and energetic sectors established. 
 Study tour to Czech Republic for key policy decision makers in segment of wood biomass in BiH organized. 
 Report on analysis of wood biomass quality situation in BiH's market at initial phase of the Project prepared. 
 Study on identification of policy gaps within the energy and forestry sectors in segment of wood biomass in Bosnia and Herzegovina prepared and presented. 
 Ongoing activities: 
 Awareness rising  of general public on positive aspects of utilization of wood biomass in BiH. 
 Establishment of the Wood Biomass Innovation Centre within the existing Biomass Association in BiH 
 Installation of new heating systems on wood biomass in four (4) public buildings in BiH. 
 Preparation of the Study on recommendations for introduction of financial mechanisms for wood biomass fuel switch projects in public buildings within Fund for environmental protection of Federation of BiH and Fund for environmental protection and energy efficiency of Republika Srpska. 
 Preparation of the Study on recommendations for adoption/revision of forest governance mechanisms in the Federation of BiH and Republika Srpska to increase utilization of wood biomass for energy production.   
 Who Finances It? 
 Since 2009, Energy and Environment sector of UNDP in BiH has been implementing robust portfolio of activities in the segment of wood biomass mainly through its GEF Biomass energy for employment and energy security and Green economic development projects. 
 Since 2013, Ministry of Foreign Affairs of the Czech Republic had been co-financing the implementation of previous wood biomass project while current Project represent continuation of successful cooperation between UNDP in BiH and the Czech Development Agency. 
   
 Donor Name 
 Amount contributed per year 
 2009 
 Global Environmental Facility (GEF)   
 $     1,600.00 
 2010 
 GEF   
 $  165,000.00 
 2011 
 GEF   
 $  195,000.00 
 2012 
 GEF   
 $  240,000.00 
 2013 
 GEF   
 $  200,000.00 
 Ministry of Foreign Affairs of Czech Republic 
 $    50,000.00 
 2014 
 GEF   
 $  165,250.00 
 Ministry of Foreign Affairs of Czech Republic 
 $  100,000.00 
 2016-2019 
   
 Czech Development Agency  
 $ 993,273 
 UNDP 
 $ 448,430 
 Sub header 
 Project Overview 
 Status: 
 Active 
 Project start date:  
 10/21/2009 
 Estimated end date:  
 31/08/2019 
 Geographic coverage:  
 Bosnia and Herzegovina 
 Focus Area:  
 Energy and Environment 
 Project Manager: 
 Amila Selmanagić Bajrović 
 Partners:  
 Ministry of foreign trade and economic relations of BiH, Embassy of Czech Republic in BiH, Ministry of agriculture, water management and forestry of FBIH, Ministry of agriculture, forestry and water management, Government of Brcko District BiH 
  end of #mod-stats  
 Donors 
 
	    Czech Development Agency  
 Video - BIOMASS 
   
 content ends  
 Our focus 
 Response to Floods Rural and Regional Development Social Inclusion and Democratic Governance Energy and Environment Justice and Security 
 Explore 
 Projects Stories Publications Press center Newsletters 
 More 
 About us Contact us Jobs Procurement Funding and delivery 
 
           
          &copy; 
          2018
           
          United Nations Development Programme 
 Report fraud, abuse, misconduct 
 Submit social or environmental complaint 
 Scam alert 
 Terms of use 
 BHS 
             |  English 
   
 Home 
 Sustainable Development Goals 
 Our focus 
 Response to Floods 
 Energy and Environment 
 Justice and Security 
 Social Inclusion and Democratic Governance 
 Rural and Regional Development 
 About us 
 Press Center 
 Around the world 
 &times; 
 Response to Floods 
 Energy and Environment 
 Justice and Security 
 Social Inclusion and Democratic Governance 
 Rural and Regional Development 
 About us 
 Press Center 
 UNDP  Around the world 
 You are at UNDP Bosnia and Herzegovina  
 Go to  UNDP Global 
 &times; 
 A 
 Afghanistan 
 Albania 
 Algeria 
 Angola 
 Argentina 
 Armenia 
 Azerbaijan 
 B 
 Bahrain 
 Bangladesh 
 Barbados 
 Belarus 
 Belize 
 Benin 
 Bhutan 
 Bolivia 
 Bosnia and Herzegovina 
 Botswana 
 Brazil 
 Burkina Faso 
 Burundi 
 C 
 Cambodia 
 Cameroon 
 Cape Verde 
 Central African Republic 
 Chad 
 Chile 
 China 
 Colombia 
 Comoros 
 Congo (Dem. Republic of) 
 Congo (Republic of) 
 Costa Rica 
 Côte d'Ivoire 
 Croatia 
 Cuba 
 Cyprus 
 D 
 Democratic People's Republic of Korea 
 Djibouti 
 Dominican Republic 
 E 
 Ecuador 
 Egypt 
 El Salvador 
 Equatorial Guinea 
 Eritrea 
 Ethiopia 
 G 
 Gabon 
 Gambia 
 Georgia 
 Ghana 
 Guatemala 
 Guinea 
 Guinea-Bissau 
 Guyana 
 H 
 Haiti 
 Honduras 
 I 
 India 
 Indonesia 
 Iran 
 Iraq (Republic of) 
 J 
 Jamaica 
 Jordan 
 K 
 Kazakhstan 
 Kenya 
 Kosovo (as per UNSCR 1244) 
 Kuwait 
 Kyrgyzstan 
 L 
 Lao PDR 
 Lebanon 
 Lesotho 
 Liberia 
 Libya 
 M 
 Madagascar 
 Malawi 
 Malaysia 
 Maldives 
 Mali 
 Mauritania 
 Mauritius & Seychelles 
 Mexico 
 Moldova 
 Mongolia 
 Montenegro 
 Morocco 
 Mozambique 
 Myanmar 
 N 
 Namibia 
 Nepal 
 Nicaragua 
 Niger 
 Nigeria 
 P 
 Pacific Office 
 Pakistan 
 Panama 
 Papua New Guinea 
 Paraguay 
 Peru 
 Philippines 
 Programme of Assistance to the Palestinian People 
 R 
 Russian Federation 
 Rwanda 
 S 
 Samoa (Multi-country Office) 
 São Tomé and Principe 
 Saudi Arabia 
 Senegal 
 Serbia 
 Sierra Leone 
 Somalia 
 South Africa 
 South Sudan 
 Sri Lanka 
 Sudan 
 Suriname 
 Swaziland 
 Syria 
 T 
 Tajikistan 
 Tanzania 
 Thailand 
 The former Yugoslav Republic of Macedonia 
 Timor-Leste 
 Togo 
 Trinidad and Tobago 
 Tunisia 
 Turkey 
 Turkmenistan 
 U 
 Uganda 
 Ukraine 
 United Arab Emirates 
 Uruguay 
 Uzbekistan 
 V 
 Venezuela 
 Viet Nam 
 Y 
 Yemen 
 Z 
 Zambia 
 Zimbabwe 
 Regional presence 
 Africa 
 Arab States 
 Asia and the Pacific 
 Europe and Central Asia 
 Latin America and the Caribbean 
 Representation offices 
 Denmark 
 European Union 
 Finland 
 Geneva 
 Norway 
 Sweden 
 Tokyo 
 Washington D.C. 
 Global policy centres 
 Istanbul 
 Nairobi 
 Oslo 
 Rio de Janeiro 
 Seoul 
 Singapore 
 &times; 
Biomass Energy for Employment and Energy Security in Bosnia and Herzegovina | UNDP in Bosnia and Herzegovina
===============================

 
   Around the world 
     BHS 
             |  English 
 Bosnia and Herzegovina 
 Sustainable Development Goals 
 Our focus 
 more 
 content starts 
  breadcrumbs start   
 Home Operations Projects Energy and Environment Biomass Energy for Employment and Energy Security in Bosnia and Herzegovina 
  breadcrumbs end   
 Biomass Energy for Employment and Energy Security in Bosnia and Herzegovina 
 Green Economic Development (GED) Technology transfer for climate resilient flood management in Vrbas River Basin Biomass Energy for Employment and Energy Security in Bosnia and Herzegovina Strengthen Bosnia and Herzegovina Decision-Making Towards Becoming a Party to the Minamata Convention and Build Capacity for Implementation of Future Provisions Regulatory Framework for Tariff Setting in Water Supply and Sewerage Services in BiH South East Europe Urban Resilience Building Action Network (SEE URBAN) Interlinking Disaster Risk Management (IDRM) in Bosnia and Herzegovina Disaster Risk Reduction Initiative in Bosnia and Herzegovina Third National Communication of Bosnia and Herzegovina under the United Nations Framework Convention on Climate Change 
 Biomass Energy for Employment and Energy Security in Bosnia and Herzegovina 
 What is the project about? 
 Assistance provided for the facilitation of wood biomass marketing and promotional activities within BiH 
 More than 53% of the territory of Bosnia and Herzegovina is covered with forests and forest land. However, despite of the significant potentials for production of energetic sources based on wood, its is still predominantly used as fuelwood for individual households heating. 
 
Furthermore, insufficient activity of private sector characterizes renewable energy sector. Limited incentives for renewable energy infrastructure projects from wood biomass represent a significant impediment to renewable energy sector development, job creations in BiH and export increase.    
 
With aim to support sustainable use of wood biomass and development of renewable energy sector in BiH, since 2016  Czech Development Agency  (CzDA) has been financing three-years long project „Biomass Energy for Employment and Energy Security in Bosnia and Herzegovina“ that has been implemented by UNDP. 
 
Partners in the realization of activities are  Ministry of foreign trade and economic relations of BiH ,  Embassy of the Czech Republic in BiH ,  Federal ministry of agriculture, water management and forestry ,  Ministry of agriculture, forestry and water management of Republika Srpska  and  Government of Brcko District of BiH . 
 What we do? 
 The overall objective of the Project is long-term reduction of CO2 emissions and improvement of local population living standard by supporting the sustainable use of wood biomass through strategic action, establishment of market value chain framework and awareness rising of general public on positive aspects of utilization of this energy source.    
 
Project strive to contribute to the overall tCO2 emission reduction per capita in BiH for 3% by the year 2030, based on the globally accepted baseline of the year 1990 (emissions of 8.97 tCO2 per capita). According to the  Emission Database for Global Atmospheric Research  (EDGAR), total emission per capita in BiH in 2014 amounted to 6,18 tCO2. 
 Project contributes to the following: 
 Enhancement of secure energy supply 
 Foster the development of enterprises for processing locally available wood biomass 
 Set up sustainable partnerships, that contribute to the economic development of micro-regions. 
 The project has been implementing through following three components: 
 Development of polices for sustainable wood biomass utilization  - reviewing of legislation in forestry and energetic sectors, support to the preparation of adoption of appropriate legislative framework, revision of Renewable energy action plans, strengthening the capacities of decision-makers, private sector representatives and local communities in the segment of wood biomass in BiH. 
 Improve the quality and availability of the wood biomass, as an energy carrier for heating purposes  – establishment of an multi-disciplinary coordination mechanism of relevant institutions from forestry and energetic sectors in BiH, mapping and quantification wood biomass exploitation, support to the establishment of forest governance mechanisms to increase utilization of wood biomass for energy purposes, awareness rising of general public on positive aspects of utilization of wood biomass in BiH. 
 Realization of renewable energy infrastructure projects  – development of business models and financial mechanisms for financing infrastructural wood biomass fuel switch projects in public buildings, establishment of the wood biomass Innovation Centre, installation of new heating systems on wood biomass in four (4) public buildings in BiH. 
 Expected Project results 
 Study on identification of policy gaps within the energy and forestry sectors in segment of wood biomass in Bosnia and Herzegovina prepared. 
 Study on comprehensive overview of potential policy and legislative framework amendment recommendations of forestry and energetic sectors in segment of wood biomass and Guide on best practices and operational methods to increase utilization of wood biomass prepared. 
 Study tour to Czech Republic for key policy decision makers in segment of wood biomass in BiH organized. 
 Trainings modules on wood biomass business models for representatives of 10 local communities and private sector in field of energy efficiency in BiH organized. 
 Working group on wood biomass in BiH with representatives of relevant institutions from forestry and energetic sectors established. 
 Study on potentials and quantification of wood biomass in BiH prepared. 
 Study on recommendations for adoption/revision of forest governance. mechanisms in the Federation of BiH and Republika Srpska to increase. utilization of wood biomass for energy production prepared. 
 At least 4,500 individuals informed about potentials of wood biomass in BiH. 
 Study on recommendations for introduction of financial mechanisms for wood biomass fuel switch projects in public buildings within Fund for environmental protection of Federation of BiH and Fund for environmental protection and energy efficiency of Republika Srpska prepared. 
 Wood Biomass Innovation Centre within the existing Biomass Association in BiH established. 
 Installation of new heating systems on wood biomass in four (4) public buildings in BiH. 
 What have we accomplished so far? 
 Implemented activities: 
 Working group on wood biomass in BiH with representatives of relevant institutions from forestry and energetic sectors established. 
 Study tour to Czech Republic for key policy decision makers in segment of wood biomass in BiH organized. 
 Report on analysis of wood biomass quality situation in BiH's market at initial phase of the Project prepared. 
 Study on identification of policy gaps within the energy and forestry sectors in segment of wood biomass in Bosnia and Herzegovina prepared and presented. 
 Ongoing activities: 
 Awareness rising  of general public on positive aspects of utilization of wood biomass in BiH. 
 Establishment of the Wood Biomass Innovation Centre within the existing Biomass Association in BiH 
 Installation of new heating systems on wood biomass in four (4) public buildings in BiH. 
 Preparation of the Study on recommendations for introduction of financial mechanisms for wood biomass fuel switch projects in public buildings within Fund for environmental protection of Federation of BiH and Fund for environmental protection and energy efficiency of Republika Srpska. 
 Preparation of the Study on recommendations for adoption/revision of forest governance mechanisms in the Federation of BiH and Republika Srpska to increase utilization of wood biomass for energy production.   
 Who Finances It? 
 Since 2009, Energy and Environment sector of UNDP in BiH has been implementing robust portfolio of activities in the segment of wood biomass mainly through its GEF Biomass energy for employment and energy security and Green economic development projects. 
 Since 2013, Ministry of Foreign Affairs of the Czech Republic had been co-financing the implementation of previous wood biomass project while current Project represent continuation of successful cooperation between UNDP in BiH and the Czech Development Agency. 
   
 Donor Name 
 Amount contributed per year 
 2009 
 Global Environmental Facility (GEF)   
 $     1,600.00 
 2010 
 GEF   
 $  165,000.00 
 2011 
 GEF   
 $  195,000.00 
 2012 
 GEF   
 $  240,000.00 
 2013 
 GEF   
 $  200,000.00 
 Ministry of Foreign Affairs of Czech Republic 
 $    50,000.00 
 2014 
 GEF   
 $  165,250.00 
 Ministry of Foreign Affairs of Czech Republic 
 $  100,000.00 
 2016-2019 
   
 Czech Development Agency  
 $ 993,273 
 UNDP 
 $ 448,430 
 Sub header 
 Project Overview 
 Status: 
 Active 
 Project start date:  
 10/21/2009 
 Estimated end date:  
 31/08/2019 
 Geographic coverage:  
 Bosnia and Herzegovina 
 Focus Area:  
 Energy and Environment 
 Project Manager: 
 Amila Selmanagić Bajrović 
 Partners:  
 Ministry of foreign trade and economic relations of BiH, Embassy of Czech Republic in BiH, Ministry of agriculture, water management and forestry of FBIH, Ministry of agriculture, forestry and water management, Government of Brcko District BiH 
  end of #mod-stats  
 Donors 
 
	    Czech Development Agency  
 Video - BIOMASS 
   
 content ends  
 Our focus 
 Response to Floods Rural and Regional Development Social Inclusion and Democratic Governance Energy and Environment Justice and Security 
 Explore 
 Projects Stories Publications Press center Newsletters 
 More 
 About us Contact us Jobs Procurement Funding and delivery 
 
           
          &copy; 
          2018
           
          United Nations Development Programme 
 Report fraud, abuse, misconduct 
 Submit social or environmental complaint 
 Scam alert 
 Terms of use 
 BHS 
             |  English 
   
 Home 
 Sustainable Development Goals 
 Our focus 
 Response to Floods 
 Energy and Environment 
 Justice and Security 
 Social Inclusion and Democratic Governance 
 Rural and Regional Development 
 About us 
 Press Center 
 Around the world 
 &times; 
 Response to Floods 
 Energy and Environment 
 Justice and Security 
 Social Inclusion and Democratic Governance 
 Rural and Regional Development 
 About us 
 Press Center 
 UNDP  Around the world 
 You are at UNDP Bosnia and Herzegovina  
 Go to  UNDP Global 
 &times; 
 A 
 Afghanistan 
 Albania 
 Algeria 
 Angola 
 Argentina 
 Armenia 
 Azerbaijan 
 B 
 Bahrain 
 Bangladesh 
 Barbados 
 Belarus 
 Belize 
 Benin 
 Bhutan 
 Bolivia 
 Bosnia and Herzegovina 
 Botswana 
 Brazil 
 Burkina Faso 
 Burundi 
 C 
 Cambodia 
 Cameroon 
 Cape Verde 
 Central African Republic 
 Chad 
 Chile 
 China 
 Colombia 
 Comoros 
 Congo (Dem. Republic of) 
 Congo (Republic of) 
 Costa Rica 
 Côte d'Ivoire 
 Croatia 
 Cuba 
 Cyprus 
 D 
 Democratic People's Republic of Korea 
 Djibouti 
 Dominican Republic 
 E 
 Ecuador 
 Egypt 
 El Salvador 
 Equatorial Guinea 
 Eritrea 
 Ethiopia 
 G 
 Gabon 
 Gambia 
 Georgia 
 Ghana 
 Guatemala 
 Guinea 
 Guinea-Bissau 
 Guyana 
 H 
 Haiti 
 Honduras 
 I 
 India 
 Indonesia 
 Iran 
 Iraq (Republic of) 
 J 
 Jamaica 
 Jordan 
 K 
 Kazakhstan 
 Kenya 
 Kosovo (as per UNSCR 1244) 
 Kuwait 
 Kyrgyzstan 
 L 
 Lao PDR 
 Lebanon 
 Lesotho 
 Liberia 
 Libya 
 M 
 Madagascar 
 Malawi 
 Malaysia 
 Maldives 
 Mali 
 Mauritania 
 Mauritius & Seychelles 
 Mexico 
 Moldova 
 Mongolia 
 Montenegro 
 Morocco 
 Mozambique 
 Myanmar 
 N 
 Namibia 
 Nepal 
 Nicaragua 
 Niger 
 Nigeria 
 P 
 Pacific Office 
 Pakistan 
 Panama 
 Papua New Guinea 
 Paraguay 
 Peru 
 Philippines 
 Programme of Assistance to the Palestinian People 
 R 
 Russian Federation 
 Rwanda 
 S 
 Samoa (Multi-country Office) 
 São Tomé and Principe 
 Saudi Arabia 
 Senegal 
 Serbia 
 Sierra Leone 
 Somalia 
 South Africa 
 South Sudan 
 Sri Lanka 
 Sudan 
 Suriname 
 Swaziland 
 Syria 
 T 
 Tajikistan 
 Tanzania 
 Thailand 
 The former Yugoslav Republic of Macedonia 
 Timor-Leste 
 Togo 
 Trinidad and Tobago 
 Tunisia 
 Turkey 
 Turkmenistan 
 U 
 Uganda 
 Ukraine 
 United Arab Emirates 
 Uruguay 
 Uzbekistan 
 V 
 Venezuela 
 Viet Nam 
 Y 
 Yemen 
 Z 
 Zambia 
 Zimbabwe 
 Regional presence 
 Africa 
 Arab States 
 Asia and the Pacific 
 Europe and Central Asia 
 Latin America and the Caribbean 
 Representation offices 
 Denmark 
 European Union 
 Finland 
 Geneva 
 Norway 
 Sweden 
 Tokyo 
 Washington D.C. 
 Global policy centres 
 Istanbul 
 Nairobi 
 Oslo 
 Rio de Janeiro 
 Seoul 
 Singapore 
 &times; 

 The project aims for the reduction of greenhouse gas emissions, by installing or retrofitting biomass boilers. 

 The project aims for the reduction of greenhouse gas emissions, by installing or retrofitting biomass boilers.
 WHOLE PROJECT MARK