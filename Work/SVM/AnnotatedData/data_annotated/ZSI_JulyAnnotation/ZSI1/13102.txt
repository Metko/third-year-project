Older people's housing in Newcastle | Newcastle City Council
===============================

   <![endif] 
 Skip to main content 
 .page  
 .l-header  
 From 1 June 2018, older browsers such as Internet Explorer 7 and 8 will no longer be able to access council websites and other sites which use standard SSL secure technology. This is because older browsers do not properly support the essential security and privacy standards.  Find out which browsers will be unsupported 
 Get updates on Twitter 
 Get updates on Facebook 
   
 MyCouncil 
 <div class="help"><a href="#">What is MyAccount?</a></div> 
 MyNeighbourhood 
 
		<div class="payment">
			<div class="payment-container">
				<a href="#">Make a payment</a>
			</div>
		</div>
		 
 .top-bar  
 <li class="name"><h1></h1></li> 
 Menu 
 Enter search criteria 
 MyNeighbourhood 
 MyAccount 
 Services The Council   
 Search form 
 invisible  
 Search 
   
  /#main-menu  
 /.top-bar  
  Title, slogan and menu  
  End title, slogan and menu  
 You are here Home Housing Housing advice and Homelessness Older people&#039;s housing in Newcastle   
 Print 
 Bookmark 
 <div class="large-12 stages">
			<ul></ul>	 		
		</div> 
 /.l-header  
   
 Older people&#039;s housing in Newcastle 
 As you get older there a number of housing options available to you, whether you are looking to move or prefer to stay in your existing home. 
 If you are finding it difficult to manage in your home but would rather not move there are services which may be able to help. These include:  
 Aids and adaptations to your home such as stairlifts and level access showers 
 Home improvement services  
 Advice on energy efficiency and cavity wall and loft insulation  
 A Community Care Alarm Service with access to a 24-hour response centre.  
 Eligibility for these services will depend on your needs, income and whether you own or rent your home.    If you are thinking about moving you may wish to consider options such as Sheltered or Extra Care housing which are designed specifically to meet the requirements of older people. When choosing accommodation it is best to consider your current circumstances and whether you think your needs will change in the years ahead.    Tyne and Wear Homes  offers a housing options advice service. For further information about the options available to you as you get older and how to access them please  visit their website .    For advice on care services please contact  Adult Social Care Direct .      Our  Older People's Housing Delivery Plan 2013-2018  (pdf, 342Kb)was approved in March 2013. It sets out how we will respond to the requirements of our older residents and meet the challenges of an aging population. 
 Page last updated:  
 17 July 2014 
 Was this page useful? 
 Was this page useful?  * 
   Yes  
   No  
 Why could you not find the information you required?  * 
 - Select - Unclear information Information not available Page not found (Please include the page you tried to access in the description) Report a fault (Please include the page you tried to access in the description) Out of date information Unable to find the information required Wrongly transferred or signposted from officer or other website Other 
 Please describe the problem in more detail below  
 Leave this field blank  
 Submit 
 /.l-main region  
 Housing advice and Homelessness 
 Extra care housing in Newcastle 
 Small Change or Real Change? 
 What to do if you need housing advice or are homeless 
 Supported tenancies 
 What to do if you see someone sleeping rough 
 Information for professionals 
 Older people's housing in Newcastle 
 Apply for Housing Benefit 
   
 /.l-main 
 .l-footer 
 Website information 
 Contact the council Browsealoud and Accessibility Help and feedback Press Office and News Privacy Notice Terms and conditions 
 © Newcastle City Council 
   
   
 Social Media 
 Follow us @NewcastleCC 
   Follow us on Facebook 
   <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>  
 /.footer 
 /.page  
 [if lte IE 8]><link rel="stylesheet" type="text/css" href="/sites/all/themes/ncctheme2014_zurb/css/ie.css"><script src="/sites/all/themes/ncctheme2014_zurb/js/ie.js"></script><![endif] 
   
<script type="text/javascript" src="//www.browsealoud.com/plus/scripts/ba.js"></script>
 
Older people's housing in Newcastle | Newcastle City Council
===============================

   <![endif] 
 Skip to main content 
 .page  
 .l-header  
 From 1 June 2018, older browsers such as Internet Explorer 7 and 8 will no longer be able to access council websites and other sites which use standard SSL secure technology. This is because older browsers do not properly support the essential security and privacy standards.  Find out which browsers will be unsupported 
 Get updates on Twitter 
 Get updates on Facebook 
   
 MyCouncil 
 <div class="help"><a href="#">What is MyAccount?</a></div> 
 MyNeighbourhood 
 
		<div class="payment">
			<div class="payment-container">
				<a href="#">Make a payment</a>
			</div>
		</div>
		 
 .top-bar  
 <li class="name"><h1></h1></li> 
 Menu 
 Enter search criteria 
 MyNeighbourhood 
 MyAccount 
 Services The Council   
 Search form 
 invisible  
 Search 
   
  /#main-menu  
 /.top-bar  
  Title, slogan and menu  
  End title, slogan and menu  
 You are here Home Housing Housing advice and Homelessness Older people&#039;s housing in Newcastle   
 Print 
 Bookmark 
 <div class="large-12 stages">
			<ul></ul>	 		
		</div> 
 /.l-header  
   
 Older people&#039;s housing in Newcastle 
 As you get older there a number of housing options available to you, whether you are looking to move or prefer to stay in your existing home. 
 If you are finding it difficult to manage in your home but would rather not move there are services which may be able to help. These include:  
 Aids and adaptations to your home such as stairlifts and level access showers 
 Home improvement services  
 Advice on energy efficiency and cavity wall and loft insulation  
 A Community Care Alarm Service with access to a 24-hour response centre.  
 Eligibility for these services will depend on your needs, income and whether you own or rent your home.    If you are thinking about moving you may wish to consider options such as Sheltered or Extra Care housing which are designed specifically to meet the requirements of older people. When choosing accommodation it is best to consider your current circumstances and whether you think your needs will change in the years ahead.    Tyne and Wear Homes  offers a housing options advice service. For further information about the options available to you as you get older and how to access them please  visit their website .    For advice on care services please contact  Adult Social Care Direct .      Our  Older People's Housing Delivery Plan 2013-2018  (pdf, 342Kb)was approved in March 2013. It sets out how we will respond to the requirements of our older residents and meet the challenges of an aging population. 
 Page last updated:  
 17 July 2014 
 Was this page useful? 
 Was this page useful?  * 
   Yes  
   No  
 Why could you not find the information you required?  * 
 - Select - Unclear information Information not available Page not found (Please include the page you tried to access in the description) Report a fault (Please include the page you tried to access in the description) Out of date information Unable to find the information required Wrongly transferred or signposted from officer or other website Other 
 Please describe the problem in more detail below  
 Leave this field blank  
 Submit 
 /.l-main region  
 Housing advice and Homelessness 
 Extra care housing in Newcastle 
 Small Change or Real Change? 
 What to do if you need housing advice or are homeless 
 Supported tenancies 
 What to do if you see someone sleeping rough 
 Information for professionals 
 Older people's housing in Newcastle 
 Apply for Housing Benefit 
   
 /.l-main 
 .l-footer 
 Website information 
 Contact the council Browsealoud and Accessibility Help and feedback Press Office and News Privacy Notice Terms and conditions 
 © Newcastle City Council 
   
   
 Social Media 
 Follow us @NewcastleCC 
   Follow us on Facebook 
   <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, autoDisplay: false}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>  
 /.footer 
 /.page  
 [if lte IE 8]><link rel="stylesheet" type="text/css" href="/sites/all/themes/ncctheme2014_zurb/css/ie.css"><script src="/sites/all/themes/ncctheme2014_zurb/js/ie.js"></script><![endif] 
   
<script type="text/javascript" src="//www.browsealoud.com/plus/scripts/ba.js"></script> 

 Street maps for the elderly are specially designed for older people with support needs. With this maps they wiil be able to easily identify the marked locations on the map without being lost. 

 Street maps for the elderly are specially designed for older people with support needs. With this maps they wiil be able to easily identify the marked locations on the map without being lost.
 WHOLE PROJECT MARK