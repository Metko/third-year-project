Brixton Energy | Power to, for and by the people! 
===============================

 
 <![endif] 
 Brixton Energy 
 Power to, for and by the people!  
   end row  
 end container  
 Menu 
 Skip to content 
 Home 
 About 
 Directors &#038; Team 
 Partners &#038; Supporters 
 Get in Touch 
 Current Projects 
 Get Involved 
 Brixton Energy Internship 
 Work Experience 
 Open Project Meetings 
 Completed Projects 
 Brixton Energy Solar 1 
 Brixton Energy Solar 2 
 Brixton Energy Solar 3 
 Community Energy Efficiency Fund 
 FAQ 
 Press 
 AGMs 
 Blog 
 Search for: 
    #site-navigation  
 end inner container  
  #navbar  
   end row  
 end container-fluid  
  #masthead  
 Home 
  .entry-header  
 Brixton Energy is a not-for-profit co-operative based in south London. We create cooperatively-owned renewable energy projects whose financial revenues stay within the local community. Check out the rest of the website to learn about our past and current projects, and for ways to get involved! 
  .entry-content  
  .entry-meta  
  #post  
  #comments  
  #content  
   
 Support Brixton Energy 2017   Click the button below to invest in, volunteer, or learn more about our upcoming Brixton Energy 2017 projects. 
 Pledge support Â» 
    .widget-area  
  .sidebar-inner  
  #tertiary  
  .row  
  .container  
  #primary  
  #main  
 Proudly powered by WordPress 
  .site-info  
  #colophon  
  #page  
Brixton Energy | Power to, for and by the people! 
===============================

 
 <![endif] 
 Brixton Energy 
 Power to, for and by the people!  
   end row  
 end container  
 Menu 
 Skip to content 
 Home 
 About 
 Directors &#038; Team 
 Partners &#038; Supporters 
 Get in Touch 
 Current Projects 
 Get Involved 
 Brixton Energy Internship 
 Work Experience 
 Open Project Meetings 
 Completed Projects 
 Brixton Energy Solar 1 
 Brixton Energy Solar 2 
 Brixton Energy Solar 3 
 Community Energy Efficiency Fund 
 FAQ 
 Press 
 AGMs 
 Blog 
 Search for: 
    #site-navigation  
 end inner container  
  #navbar  
   end row  
 end container-fluid  
  #masthead  
 Home 
  .entry-header  
 Brixton Energy is a not-for-profit co-operative based in south London. We create cooperatively-owned renewable energy projects whose financial revenues stay within the local community. Check out the rest of the website to learn about our past and current projects, and for ways to get involved! 
  .entry-content  
  .entry-meta  
  #post  
  #comments  
  #content  
   
 Support Brixton Energy 2017   Click the button below to invest in, volunteer, or learn more about our upcoming Brixton Energy 2017 projects. 
 Pledge support Â» 
    .widget-area  
  .sidebar-inner  
  #tertiary  
  .row  
  .container  
  #primary  
  #main  
 Proudly powered by WordPress 
  .site-info  
  #colophon  
  #page 

 Brixton Energy is a solar energy cooperative in South London. Part of the income generated is reinvested in the Community Energy Efficiency Fund for energy-saving improvements in the local area. 

 Brixton Energy is a solar energy cooperative in South London. Part of the income generated is reinvested in the Community Energy Efficiency Fund for energy-saving improvements in the local area.
 WHOLE PROJECT MARK