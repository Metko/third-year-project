PROJECT NAME: Gitorious
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 Gitorious is a web-based hosting service for collaborative free and open-source software development projects that use the Git revision control system. The name also refers to the free and open-source server software that the Web site is developed and hosted on.

History

Gitorious was started by Johan Sørensen in the end of 2007 as a way to easily host Git repositories for himself. The idea turned out to be so useful that Gitorious began to offer hosting for other projects as well, for free, eventually opening up for anyone to create an account.

In March 2009 Gitorious.org was folded into Shortcut AS, a software company co-founded by Johan. The intention was to improve Gitorious beyond what was feasible for a single developer to manage. Gitorious AS now officially supports and develops Gitorious in cooperation with contributors outside the company, while still being 100% free software. under the GNU Affero General Public License. 

Gitorious is a web-based hosting service for collaborative free and open-source software development projects that use the Git revision control system. The name also refers to the free and open-source server software that the Web site is developed and hosted on.

History

Gitorious was started by Johan Sørensen in the end of 2007 as a way to easily host Git repositories for himself. The idea turned out to be so useful that Gitorious began to offer hosting for other projects as well, for free, eventually opening up for anyone to create an account.

In March 2009 Gitorious.org was folded into Shortcut AS, a software company co-founded by Johan. The intention was to improve Gitorious beyond what was feasible for a single developer to manage. Gitorious AS now officially supports and develops Gitorious in cooperation with contributors outside the company, while still being 100% free software. under the GNU Affero General Public License.
 WHOLE PROJECT MARK