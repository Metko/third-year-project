PROJECT NAME: Fab Lab Barcelona
PROJECT SOURCE: MAKE IT case studies
---------------------------------------
NEW PAGE: 

Fab Lab Barcelona
===============================

Fab Lab Barcelona

 Home 
 About Us 

 What We Do  

 Workshops 
 Projects 
 Machines 
 Fab Academy 
 Master in Design for Emergent Futures 
 HTGAA 
 Master in Advanced Architecture 
 Master in City and Technology 
 Fabricademy 
 Fab Pro 
 Fab Kids 
 Pop Up Fab Lab 
 Visits 

 IAAC Student Booking 

 Fab Labs Around the World 

 Calendar 
 Blog 

 Join Us  

 Visit 

 Visits 

 Learn 

 Fab Academy 
 MDEF 
 Fabricademy 
 Bootcamps 

 Work with us 

 Internship Program 
 Job Opportunities 

 Groups 

 Pop Up Fab Lab 

 Contact 

 One of the leading laboratories of the worldwide network of Fab Labs 

 Latest News 

 Read more here   

 Making Sense presents Citizen Sensing a toolkit 

 The Making Sense team is pride to presents the Citizen Sensing, a toolkit, which describes all methods, lessons and best practices, used and analyzed during the last 2 years of work. 

 read more   

 Fab Academy 2018 

 It's time to make (almost) anything. We welcome you to Fab Academy 2018 that starts today and end in Fab Academy Graduation in July during Toulouse FAB14 

 read more   

 MDEF Director Tom s D ez on UrbanNext Talk 

 Master in Design for Emergent Futures (MDEF) Director,Tom s Diez, is interviewed by urbanNext during the Smart City Expo World Congress 2017 on digital fabrication, circular systems and the future of production in cities. 

 read more   

 10 Years of Fab Lab BCN! 

 10 Years of Fab Lab BCN: we have done a lot, but we will do more in the next 10 years. Learn more about our projects and our programmes. 

 read more   

 Events 

 Read more here   

 Master In Design For Emergent Futures MDEF 

 IAAC and ELISAVA, in collaboration with the Fab Academy and Ideas for Change, are creating the next generation design education program: the Master in Design for Emergent Futures (MDEF). 

 read more   

 Open Source Fashion, Digital pattern making   laser cutting 

 Learn how to use Seamly2D: fashion drawing, measurement file and printing tools, repository of patterns, laser cutting and much more during this intensive course. 

 read more   

 Nous Tallers per a nenes i nens @ Fab Lab BCN 

 Fabkids BCN, en col laboraci  amb minim sica, proposen dos nous Tallers de m sica i tecnologia per a nenes i nens: 'Guitarra i Ampli - Crea la teva guitarra de cartr , experimenta amb sensors i construeix el teu amplificador!' i 'Programaci  creativa amb Sonic PI + Raspberry PI - Programa una can '  

 read more   

 Poblenou Urban District Open Night @ Fab Lab BCN 

 IAAC | Fab Lab Barcelona will participate in the Poblenou Open Night 2017. Friday 24th of November, from 19:00 to 21:00, we will be open for all those who want to know us. 

 read more   

 Organicity 

 Eu Research 

 Making Sense 

 Eu Research 

 GROW Observatory 

 Eu Research 

 iScape 

 Eu Research 

 MAKE-IT 

 Eu Research 

 Made @ EU 

 Eu Culture 

  Workshops 

  Projects 

  Machines 

  Fab Pro 

  Fab Academy  

  Fab Kids  

  Pop-Up Fab Lab  

  Fab Lab Visits 

  IAAC Student Booking 

  Fab Labs Around The World 

  Gallery 

  Fab City 

 Collaborators 

 Subscribe to our newsletter 

 Founded by: 

 In collaboration with: 

 Partners: 

 Visit us 

   Institut d arquitectura avan ada de Catalunya C/Pujades 102 baixos. Poble Nou, Barcelona, 08005, Spain 

 About Fab Lab Barcelona: [email protected] 
 About services and products: [email protected] 

  2015 IAAC | Fab Lab Barcelona 

  Designed   Developed by edgaar 

Fab Lab Barcelona
===============================

Fab Lab Barcelona

 Home 
 About Us 

 What We Do  

 Workshops 
 Projects 
 Machines 
 Fab Academy 
 Master in Design for Emergent Futures 
 HTGAA 
 Master in Advanced Architecture 
 Master in City and Technology 
 Fabricademy 
 Fab Pro 
 Fab Kids 
 Pop Up Fab Lab 
 Visits 

 IAAC Student Booking 

 Fab Labs Around the World 

 Calendar 
 Blog 

 Join Us  

 Visit 

 Visits 

 Learn 

 Fab Academy 
 MDEF 
 Fabricademy 
 Bootcamps 

 Work with us 

 Internship Program 
 Job Opportunities 

 Groups 

 Pop Up Fab Lab 

 Contact 

 One of the leading laboratories of the worldwide network of Fab Labs 

 Latest News 

 Read more here   

 Making Sense presents Citizen Sensing a toolkit 

 The Making Sense team is pride to presents the Citizen Sensing, a toolkit, which describes all methods, lessons and best practices, used and analyzed during the last 2 years of work. 

 read more   

 Fab Academy 2018 

 It's time to make (almost) anything. We welcome you to Fab Academy 2018 that starts today and end in Fab Academy Graduation in July during Toulouse FAB14 

 read more   

 MDEF Director Tom s D ez on UrbanNext Talk 

 Master in Design for Emergent Futures (MDEF) Director,Tom s Diez, is interviewed by urbanNext during the Smart City Expo World Congress 2017 on digital fabrication, circular systems and the future of production in cities. 

 read more   

 10 Years of Fab Lab BCN! 

 10 Years of Fab Lab BCN: we have done a lot, but we will do more in the next 10 years. Learn more about our projects and our programmes. 

 read more   

 Events 

 Read more here   

 BioShades Whorkshop and talks 

 TCBL lasbs are looking forward to the BioShades event: across Europe, they will connect through a video conferencing system to learn how to dye fabrics with bacteria. 

 read more   

 Textile Academy BootCamp 2018 

 The annual Textile-academy Bootcamp will take place in Milan on 7th of May. Professionals, future instructors, students and artists gather together to have a skills exchange and training of 40 intensive hours 

 read more   

 Master In Design For Emergent Futures MDEF 

 IAAC and ELISAVA, in collaboration with the Fab Academy and Ideas for Change, are creating the next generation design education program: the Master in Design for Emergent Futures (MDEF). 

 read more   

 Open Source Fashion, Digital pattern making   laser cutting 

 Learn how to use Seamly2D: fashion drawing, measurement file and printing tools, repository of patterns, laser cutting and much more during this intensive course. 

 read more   

 Nous Tallers per a nenes i nens @ Fab Lab BCN 

 Fabkids BCN, en col laboraci  amb minim sica, proposen dos nous Tallers de m sica i tecnologia per a nenes i nens: 'Guitarra i Ampli - Crea la teva guitarra de cartr , experimenta amb sensors i construeix el teu amplificador!' i 'Programaci  creativa amb Sonic PI + Raspberry PI - Programa una can '  

 read more   

 Poblenou Urban District Open Night @ Fab Lab BCN 

 IAAC | Fab Lab Barcelona will participate in the Poblenou Open Night 2017. Friday 24th of November, from 19:00 to 21:00, we will be open for all those who want to know us. 

 read more   

 Organicity 

 Eu Research 

 Making Sense 

 Eu Research 

 GROW Observatory 

 Eu Research 

 iScape 

 Eu Research 

 MAKE-IT 

 Eu Research 

 Made @ EU 

 Eu Culture 

  Workshops 

  Projects 

  Machines 

  Fab Pro 

  Fab Academy  

  Fab Kids  

  Pop-Up Fab Lab  

  Fab Lab Visits 

  IAAC Student Booking 

  Fab Labs Around The World 

  Gallery 

  Fab City 

 Collaborators 

 Subscribe to our newsletter 

 Founded by: 

 In collaboration with: 

 Partners: 

 Visit us 

   Institut d arquitectura avan ada de Catalunya C/Pujades 102 baixos. Poble Nou, Barcelona, 08005, Spain 

 About Fab Lab Barcelona: [email protected] 
 About services and products: [email protected] 

  2015 IAAC | Fab Lab Barcelona 

  Designed   Developed by edgaar
 WHOLE PROJECT MARK