PROJECT NAME: Peer to peer University (P2PU)
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 


Learning circles are free study groups for people who want to take online classes together and in-person.
group
393 learning circles

Learn together in a small group with your neighbors.
important_devices
269courses

Use free, online courses from around the web.
pin_drop
104 participating cities

Meet weekly in public spaces like libraries and community centers.

Create and facilitate a learning circle

Join a learning circle near you
Kenya National Library Service facilitator training workshop in Nairobi
Learning doesn’t have to end at school

P2PU supports equitable, social learning experiences beyond institutional walls.

Have we met? Read about our history, our values, and our community.

Check out our blog for stories and updates from P2PU friends and affiliates around the world.

Want us to run a training? Can we help kickstart a new program? Hire us to work with you.
Learning circle selfie
Kansas City Public Library “Making the Internet Work For You!” learning circle
Partners
Boston Public Library
Charlotte Mecklenburg Public Library
Cleveland Public Library
Chicago Public Library
Creative Commons
Centre de Recherches Interdisciplinaires
Detroit Public Library
EIFL
Free Library of Philadelphia
Hillsborough County Public Library Cooperative
Kansas City Public Library
Kenya National Library Services
Libraries without Borders
Miami-Dade Public Library System
MIT Media Lab
Mozilla
Multnomah County Library
OERu
Open University
Pierce County Library
Children's Museum of Pittsburgh
Providence Public Library
Richland County Library
San Jose Public Library
TASCHA
Twin Lakes Library System
Wichita Public Library
World Education
Funders
Dollar General Literacy Foundation
Knight Foundation
Institute of Museum and Library Services
Open Society Foundations
Siegel Family Endowment
Stay in touch
@P2PU
P2PUniversity
thepeople@p2pu.org
Get the newsletter


What is P2PU

P2PU is a grassroots network of individuals who seek to create an equitable, empowering, and liberating alternative to mainstream higher education.

We work towards this vision by creating and sustaining learning communities in public spaces around the world. As librarians and community organizers, we bring neighborhoods together to learn with one another. As educators, we train facilitators to organize their own networks and we develop/curate open educational resources. As developers and designers, we build open source software tools that support flourishing learning communities. And as learners, we work together to improve upon and disseminate methods and practices for peer learning to flourish.

P2PU is a distributed organization, incorporated as a 501(c)3 in the United States. Our network stretches the globe, with our heaviest concentration of work currently happening in Canada, Kenya, South Africa, and the United States.

We strive to model our educational values in our organizational structure; as such, we set goals and make decisions collectively as a community. We invite you to join our virtual community and view our governance guidelines, annual goals, and policies (as well as all source code) on Github.
History

P2PU was born out of the 2007 Cape Town Open Education Declaration and has been active in the field of open education ever since. Founded by Philipp Schmidt, Delia Browne, Neeru Paharia, Stian Haklev, and Joel Thierstein, P2PU was originally driven by the belief that with adequate social support, anyone can learn almost anything online for free. This strategy reached tens of thousands of learners in more than 50 countries across a number of projects and collaborations, including The School of Webcraft (with Mozilla), School of Open (with Creative Commons), Mechanical MOOC (with MIT), and Play With Your Music (with NYU and Peter Gabriel). P2PU also pioneered early massive open online courses through it’s courses platform and developed some of the first open badge prototypes in collaboration with Mozilla.

In 2014, P2PU made a strategic decision to stop working exclusively online because we were not reaching people who stand to benefit the most from the promise of free and open online education. In hopes of reaching new audiences, we partnered with Chicago Public Library to run in-person study groups for library patrons who wanted to learn together. This project became known as learning circles, and over the course of 18 months, P2PU and CPL managed to dramatically increase completion rates and reach new audiences who were new to both online learning and postsecondary education. Learning circles also formed strong social bonds for citizens from diverse backgrounds who shared common goals, and helped to highlight the library as a hub for community learning experiences

Given the success in Chicago, P2PU created an open source learning circle toolkit that is now being used by library systems and community centers around the world. Through learning circles, we have maintained our vision of providing viable alternatives to mainstream higher education with a deeper commitment to equity, access, and empowerment. All of our learning circle materials and community tools are openly-licensed and free to use. You can get started with learning circles today.
Values

P2PU is driven by three core values:

Peer learning

Underlying our work is the conviction that learning is a social activity. We believe that every person develops expertise through their own life experiences, that people learn when they share and connect with others, and that feedback is necessary in order to improve.

Community

P2PU is a community-centered project, which is reflected across our organization from learning circles to our governance model. We involve learners and collaborators in all stages of the design and delivery of our work, and believe that sustained learning communities are created through grassroots collaboration, not hierarchical mandates.

Equity

Equity in learning is only possible when we recognize education as a social good rather than a commodity. A prerequisite to this value is a commitment to working openly, leaving the commons better off than we found it. However, creating space for access is not enough. Systemic injustice means that access does not equal equity, and we must actively design for inclusion and accessibility at every step along the way.
People
Our Staff
Grif Peterson Image
Grif Peterson
Chief of Stuff

Grif joined P2PU in early 2015 to start learning circles with Chicago Public Library. Since then, his role has grown with the program, and he does his best to make sure that folks around the world have the opportunity to keep on learning things they care about, for free. Once he presented about learning circles in a bank vault to 2 people. He lives in Cambridge, Massachusetts.
grifpeterson | grifpeterson.com
Nico Koenig Image
Nico Koenig
Town Crier

Nico takes a community-first approach in coordinating and expanding P2PU's Learning Circle program. From education through sport programs in Zambia, to community festivals in the Northwest Territories, to free schools in the storefronts of Toronto, Nico promotes learning just about anywhere. That is, anywhere but the lecture hall. Nico can usually be found biking around Ottawa, Canada.
nicokoenig | nicokoenig.com
Dirk Uys Image
Dirk Uys
Wizard of 0s and 1s

Dirk makes sure that just the right amount of tech is developed at P2PU. After travelling through South and Central America, he is now back home in South Africa and based in Cape Town. When he is not writing code, he is most likely hiking Table mountain, surfing the Atlantic or tinkering with a Raspberry Pi.
riskycud | thebacklog.net
Ryan George Image
Ryan George
Financial Administrator

Ryan oversees Financing and Accounting for P2PU. He is a vibrant and energetic person who has been with P2PU since 2012. He loves culture, innovative ideas, people and numbers. He is based in Cape Town, South Africa.
Sharon Kennedy Image
Sharon Kennedy
Web developer

Sharon is a web developer and designer based in Toronto, Canada. She's working on making the this website a beautiful and useful resource for the P2PU community. When she's not fused to her laptop, she leads wilderness camping and hiking tours.
codeandcanoes | www.followthewhitesquirrel.ca
Our Board
Philipp Schmidt Image
Philipp Schmidt
Co-founder, Director's Fellow MIT Media Lab

Along with a motley crew of edupunks, Philipp co-founded P2PU in 2008 with support from a Shuttleworth Grant. Years later he is still cooking up learning experiences (and mustard) at the MIT Media Lab, and nurturing partnerships for future P2PU projects.
schmidtphi | philippschmidt.org/
June Ahn Image
June Ahn
Associate Professor, New York University

June is a learning sciences and human-computer interaction researcher at NYU. He bravely collaborated with P2PU on past research projects and now serves on the board to help think about strategic new directions for peer learning.
ahnjune | ahnjune.com
Ahrash Bissell Image
Ahrash Bissell
Project Manager Monterey Institute for Technology and Education

As a board member, Ahrash takes care of staying true to our original values, while pushing us to be more experimental. He is a wise man.
Delia Browne Image
Delia Browne
Chair of the Board

As a board member and active community member Delia makes sure things keep running smoothly at P2PU. Next to this she is involved in many of the legal aspects surrounding the organization.
Allen Gunn Image
Allen Gunn
Gunner the Facilitator

It is not without reason that Gunner is our workshop facilitator of choice. He is Executive Director at Aspiration, helping NGOs, activists, foundations and software developers make more effective use of technology for social change.
allengunn
Neeru Paharia Image
Neeru Paharia
Co-founder, Assistant Professor, Georgetown University

As original gang-star par excellence, Neeru didn’t think it was enough to theorize about starting a peer learning platform so she co-founded P2PU and created and organized the‘Behavioral Economics’ pilot course. Neeru has been at the root of P2PU’s success in many ways and now employs her vast experience on the board.
Nadeem Shabir Image
Nadeem Shabir
Technical Lead at Talis

When Nadeem is not eloquently luring people at P2PU workshops into letting their emotions about peer learning run freely, he uses his vast experience in programming adds technical perspective to the board.
kiyanwang | www.virtualchaos.co.uk/blog/
Mark Surman Image
Mark Surman
Executive Director Mozilla Foundation

Mark loves all things open, including wine bars in San Francisco that are still open at 2am. As Mozilla Foundation ED he is leading an army of webmakers to keep the web open. As board member of P2PU he helps Philipp get more done.
msurman | commonspace.wordpress.com
Community Recognition

P2PU is only as successful as people behind the scenes making things happen. A special thanks goes out to some thoughtful and energetic people who helped us get to where we are today: Mark Andersen, Lila Bailey, Adam Bush, John Britton, Pippa Buchanan, Steve Carson, Cathy Casserly, Alison Jean Cole, Larry Cooperman, Maria Droujkova, Chris Ewald, Karen Fasimpaur, Jose Flores, Karen Gabriels, Vanessa Gennarelli, Stian Haklev, Laura Hilliger, Alexander Kehayias, Rebecca Kahn, Molly Kleinman, Pieter Kleymeer, Kate Lapinski, Gary Matkin, Katherine McConachie, João Menezes, Dany Javier Bautista Montaña, Juliana Muchai, Paul Osman, Jane Park, Beck Pitt, Erika Pogorelc, Andrew Rens, Carl Ruppin, Alex Ruthmann, Jessy Kate Schingler, Niels Sprong, Aleksander Stachurka, Joel Thierstein, Chloe Varelidi, Zuzel Vera, Alan Webb.


Learning Circles

Learning circles are study groups for people who want to take online classes together and in-person. Sign up below for a learning circle in your area, or create your own!
Search
search
Filter
Showing 29 results
Machine Learning

schedule
Tuesday from 4:00pm to 5:30pm (EDT)

today
34 weeks starting October 31st, 2017

place
North York, Ontario, Canada

face
Facilitated by David Chasteau

store
Meeting at York University.
High School Equivalency -- Science and Beyond
High School Equivalency -- Science and Beyond

schedule
Saturday from 9:00am to 11:18am (CST)

today
26 weeks starting February 10th, 2018

place
Round Rock, Texas, United States of America

face
Facilitated by Kathryn Davis

store
Meeting at Round Rock Public Library, every other week
Jazz: The Music, The Stories, The Players
Jazz: The Music, The Stories, The Players

schedule
Thursday from 3:30pm to 5:30pm (EDT)

today
6 weeks starting May 3rd, 2018

place
Detroit

face
Facilitated by Virgil Thomas

store
Meeting at Edison Branch Library
Google for Education Level 1 Certification

schedule
Friday from 11:00am to 1:00pm (EDT)

today
12 weeks starting April 6th, 2018

place
Providence

face
Facilitated by Sherry Lehane

store
Meeting at Riverside, Rhode Island
How To Create a Website in a Weekend!

schedule
Thursday from 10:30am to 12:00pm (EDT)

today
6 weeks starting June 7th, 2018

place
Detroit

face
Facilitated by Asia Nelson

store
Meeting at Hubbard Branch Library
Vegetable Gardening 101

schedule
Wednesday from 10:30am to 12:00pm (EDT)

today
6 weeks starting June 6th, 2018

place
Detroit

face
Facilitated by Asia Nelson

store
Meeting at Hubbard Branch Library
Vegetable Gardening 101

schedule
Wednesday from 10:30am to 12:00pm (EDT)

today
6 weeks starting June 6th, 2018

place
Detroit

face
Facilitated by Asia Nelson

store
Meeting at Hubbard Branch Library
Job Skills
Job Skills

schedule
Monday from 5:30pm to 7:00pm (CDT)

today
6 weeks starting May 21st, 2018

place
Kansas City

face
Facilitated by Alexis Burns

store
Meeting at Bluford Branch
How To Create a Website in a Weekend!
How To Create a Website in a Weekend!

schedule
Saturday from 2:30pm to 4:30pm (EAT)

today
7 weeks starting May 5th, 2018

place
Kampala

face
Facilitated by Bonny Olobo

store
Meeting at KCCA Library (Kampala Library & Information Centre)
Art of Storytelling

schedule
Monday from 8:00am to 9:30am (EAT)

today
7 weeks starting April 23rd, 2018

place
Narok

face
Facilitated by Nelly Tanketi

store
Meeting at Kenya National Library Services - Narok Branch
Social Media: What No One Has Told You About Privacy

schedule
Thursday from 2:30pm to 4:00pm (EAT)

today
6 weeks starting May 10th, 2018

place
Nairobi

face
Facilitated by kaltuma sama

store
Meeting at knls buruburu
الصحة النفسية للطفل

schedule
Sunday from 5:00pm to 6:30pm (CET)

today
4 weeks starting May 13th, 2018

place
Sousse

face
Facilitated by Ahmad AlMokny

store
Meeting at aفي
Fundamentals of Public Speaking

schedule
Friday from 11:00am to 12:30pm (EAT)

today
6 weeks starting May 18th, 2018

place
Meru

face
Facilitated by JOAN NJOKI

store
Meeting at meru
Essentials of Entrepreneurship: Thinking & Action

schedule
Monday from 2:00pm to 3:30pm (EAT)

today
6 weeks starting May 21st, 2018

place
Meru

face
Facilitated by JOAN NJOKI

store
Meeting at Meru Library
Fundamentals of Public Speaking

schedule
Thursday from 11:00am to 12:30pm (EAT)

today
6 weeks starting May 10th, 2018

place
Isiolo

face
Facilitated by Rufus Githinji

store
Meeting at isiolo library
Adobe Illustrator

schedule
Sunday from 10:30pm to 12:00am (EST)

today
6 weeks starting May 6th, 2018

place
Panamá

face
Facilitated by Julio David Bermúdez

store
Meeting at usa
US Citizenship Prep Course

schedule
Tuesday from 10:30am to 12:00pm (PDT)

today
6 weeks starting May 22nd, 2018

place
San Jose

face
Facilitated by Javier deLeon

store
Meeting at San Jose Public Library Educational Park Branch
Art of Storytelling
Art of Storytelling

schedule
Thursday from 6:30pm to 8:30pm (EDT)

today
7 weeks starting May 10th, 2018

place
Virginia Beach

face
Facilitated by Susan Hampe

store
Meeting at Meyera E. Oberndorf Central Library
Intro to Statistics

schedule
Thursday from 11:00am to 12:30pm (EAT)

today
8 weeks starting May 17th, 2018

place
Murang'a

face
Facilitated by James Waruinge

store
Meeting at kenya national library service Building
Intro to HTML and CSS

schedule
Monday from 4:30pm to 6:00pm (EAT)

today
8 weeks starting May 21st, 2018

place
Nakuru

face
Facilitated by Joseck Kweya

store
Meeting at Kenya National Library Nakuru
Office 365
Office 365

schedule
Monday from 4:30pm to 6:00pm (CDT)

today
3 weeks starting June 18th, 2018

place
Kansas City

face
Facilitated by Alexis Burns

store
Meeting at Central Library
Antropología de lo digital

schedule
Saturday from 10:28pm to 11:58pm (MSK)

today
18 weeks starting May 19th, 2018

place
Бугульма

face
Facilitated by Александр Смирнов

store
Meeting at ГАПОУ "Бугульминский строительно-технический колледж"
Antropología de lo digital

schedule
Saturday from 10:28pm to 11:58pm (MSK)

today
18 weeks starting May 19th, 2018

place
Бугульма

face
Facilitated by Александр Смирнов

store
Meeting at ГАПОУ "Бугульминский строительно-технический колледж"
Wordpress.com Essential Training

schedule
Tuesday from 9:30am to 11:00am (CAT)

today
16 weeks starting May 22nd, 2018

place
Harare

face
Facilitated by George Taderera

store
Meeting at Estlea
Digital literacies for online learning

schedule
Monday from 8:30am to 11:30am (EAT)

today
3 weeks starting May 28th, 2018

place
Nanyuki

face
Facilitated by Joseph Gitahi

store
Meeting at laikipia library
Understanding User Needs

schedule
Tuesday from 9:30pm to 11:00pm (EAT)

today
6 weeks starting June 12th, 2018

place
nyeri

face
Facilitated by Dorothy Ireri

store
Meeting at nyeri knls library
English Grammar and Style

schedule
Friday from 3:30pm to 5:00pm (CAT)

today
6 weeks starting May 25th, 2018

place
Harare

face
Facilitated by George Taderera

store
Meeting at Harare
Android Basics: User Interface

schedule
Monday from 8:00pm to 9:30pm (-03)

today
6 weeks starting May 28th, 2018

place
Montevideo

face
Facilitated by Matias Villagran

store
Meeting at Home



Peer to Peer University (P2PU) is a nonprofit online open learning community which allows users to organize and participate in courses and study groups to learn about specific topics. Peer 2 Peer University was started in 2009 with funding from the Hewlett Foundation and the Shuttleworth Foundation, with its first of courses in September of that year. An example of the "edupunk" approach to education, P2PU charges no tuition and courses are not accredited. P2PU offers some of the features of massive open online courses, but is focused on people sharing their knowledge on a topic or learning about a topic offered by another user with a DIY wiki-type mentality 


 WHOLE PROJECT MARK
