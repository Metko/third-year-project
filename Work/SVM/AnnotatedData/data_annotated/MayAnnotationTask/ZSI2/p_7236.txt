PROJECT NAME: The Cisco Networking Academy
PROJECT SOURCE: ICT-enabled social innovation: cases
---------------------------------------
NEW PAGE: 

Self Enroll for Course: 516560 - Networking Academy
===============================

Self Enroll for Course: 516560 - Networking Academy

 2.51.180225013517.801 

 Skip to Content 

 Cisco Networking Academy 

 Einf hrung in die Cybersicherheit - Asylplus Cisco Academy 
 AsylPlus e.V. 

 Description 

 Course Details 

 Asylplus-I2CS-DE-Summer17 
 Introduction to Cybersecurity  ( German v2.01 ) 
 22 May 2017 - 31 March 2018 
 Nadia Miloudi, Muafaq Al Mufti 
 online Kurs 

	Enroll Now

 Refresh CAPTCHA 

	Text Verification

			 * 

 Terms and Conditions 
 | 
 Privacy Statement 
 | 
 Cookie Policy 
 | 
 Trademarks 

Self Enroll for Course: 520344 - Networking Academy
===============================

Self Enroll for Course: 520344 - Networking Academy

 2.51.180225013517.801 

 Skip to Content 

 Cisco Networking Academy 

 Einf hrung in das Internet der Dinge - Asylplus Cisco Academy 
 AsylPlus e.V. 

 Description 

 Course Details 

 Asylplus-IoE-DE-Summer17 
 Introduction to the Internet of Everything  ( German v1.21 ) 
 3 June 2017 - 1 April 2018 
 Nadia Miloudi, Muafaq Al Mufti, Hassan Omar 
 Online Kurs 

	Enroll Now

 Refresh CAPTCHA 

	Text Verification

			 * 

 Terms and Conditions 
 | 
 Privacy Statement 
 | 
 Cookie Policy 
 | 
 Trademarks 

Self Enroll for Course: 516577 - Networking Academy
===============================

Self Enroll for Course: 516577 - Networking Academy

 2.51.180225013517.801 

 Skip to Content 

 Cisco Networking Academy 

 CyberSecurity Essentials - Asylplus Cisco Academy 
 AsylPlus e.V. 

 Description 

 Course Details 

 Asylplus-CSESS-EN-Summer17 
 Cybersecurity Essentials  ( English v1.01 ) 
 21 May 2017 - 22 May 2018 
 Nadia Miloudi, Muafaq Al Mufti 
 Online Course 

	Enroll Now

 Refresh CAPTCHA 

	Text Verification

			 * 

 Terms and Conditions 
 | 
 Privacy Statement 
 | 
 Cookie Policy 
 | 
 Trademarks 

Cisco Networking Academy
===============================

Cisco Networking Academy

 Go to Main Content 

 180225013517.1 

 en_US 
 en_US 

 {{$root.loginData.title}} 

 {{loginData.error}} 

                    {{loginData.label_user}}

                    {{loginData.label_password}}

 {{loginData.links.forgot_password.label}} 
 {{loginData.links.resend_activation_email.label}} 
 {{loginData.links.redeem_seat_token.label}} 

 Sign In 

 Caps Lock is on. 

 Forgot Password 

 Forgot Screen Name 

 Redeem Seat Token 

 The Cisco Networking Academy program offers hands-on ICT training to prepare students for in-demand careers and globally recognized certifications. 

The Cisco Networking Academy program offers hands-on ICT training to prepare students for in-demand careers and globally recognized certifications.
 WHOLE PROJECT MARK