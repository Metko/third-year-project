PROJECT NAME: Medic Mobile
PROJECT SOURCE: ICT for social innovations
---------------------------------------
NEW PAGE: 

Medic Mobile
===============================

Medic Mobile

 The Tools 
 Use Cases 
 Deploy Medic Mobile 
 Design 
 Partners 
 Research   Impact Reports 
 Blog 
 Our Team 
 Careers 
 Contact 
 Donate 
 Phone donations 

 Deploy Medic Mobile 
 Our tools are used by large organizations supporting thousands of frontline health workers, as well as local organizations and clinics. Learn more about your options for deploying Medic Mobile. 

 Explore the Tools 
 Want to learn more about our latest software toolkit? On our tools page you can request a walkthrough  our team is happy to give you a tour! 

 Get Involved 

   Start Now 

 THE TOOLS 
 USE CASES 
 PARTNERS 
 DESIGN 
 BLOG 

  Medic Mobile s 2018 Goals 

 We are all health workers 

 The Tools 
 Explore the toolkit 

 People delivering care deserve the best software 

	The Medic Mobile toolkit combines decision support for frontline care, prioritization for home visits and follow-ups, smart messaging, and actionable analytics for managers. The tools we build are free, open-source, and deployed at scale in the last mile of healthcare. Evidence-based workflows come together in the software to support health workers and families   helping to ensure safe deliveries, track outbreaks, treat illnesses door-to-door, keep stock of essential medicines, communicate about emergencies, and more. 

 Use cases 
 Explore the use cases 

 Reducing maternal and neonatal mortality 

 Antenatal care 
 Postnatal care 
 Family planning 

 DETAILS 

 Improving child health 

 Nutrition 
 Immunization 
 Integrated community case management 

 DETAILS 

 Strengthening community health systems 

 Health worker performance management 
 Health system performance management 
 Outbreak surveillance 
 Communication 

 DETAILS 

 Our Global Impact 

 23 
													Countries in Africa, Latin America, and Asia where Medic Mobile is making an impact
																										 16,024 
													Health workers trained and equipped as of Q2 2017

 Our partners 
 Partners 

 We can help you deliver Medic Mobile to health workers 
 Medic Mobile helps partners assess opportunities for their health system, choose and configure the right tools for health workers, design workflows, train local staff on our toolkit, deliver our tools to health workers, monitor programs, and evaluate impact. 

 Design 
 See why we're different 

 Designing for Health Workers 
 At Medic Mobile, health workers are at the center of everything we do. Before building or deploying any technology, we get to know the people who will use and benefit from an improved health system. Our team designs workflows and tools to achieve greater health coverage and improved health equity in the communities they serve.  

 FROM OUR BLOG 
 VIEW OUR BLOG 

 Author, Alix Emden, March 07, 2018 
 Nutrition month at Medic Mobile  

 Author, Alix Emden, February 27, 2018 
 New report reviews the 50+ publications in Medic Mobile s research archive 

 Author, Alix Emden, February 22, 2018 
 What can you do for Medic Mobile? 

 Author, Alix Emden, February 08, 2018 
 Medic Mobile Full Team Meet-up  

 FEATURED IN 

 #}

 Contact 
 3254 19th Street, Floor Two San Francisco, CA 94110 +1(415) 746-9758  
 Send a message 

 Careers 
 Medic Mobile is growing and we re hiring great people. 
 VIEW JOBS 

 News 
 Stay up to date on the latest mHealth news and sign up for our newsletter. 

 Connect 

 Donate 

 We design, deliver, and support world-class software for health workers providing care in the hardest-to-reach communities. 
  2018 Medic Mobile. All Rights Reserved. 
   Privacy Policy 

 Medic Mobile
 WHOLE PROJECT MARK