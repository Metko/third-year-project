PROJECT NAME: DecarboNet
PROJECT SOURCE: Consumer-related projects
---------------------------------------
NEW PAGE: 

Home - DecarboNet
===============================

Home - DecarboNet

 Home 
 The Project 

 Goals and Motivation 
 Project Structure 
 Who We Are 

 Public Results 

 Open Tools 
 Publications 
 Deliverables 

 Earth Hour 
 News 
 Contact 

 Search 

 Menu 

 Home You are here:   Home Media Watch on Climate Change December 10, 2016 The 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2014/10/cc-screenshot.png 
 1288 
 2035 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2016-12-10 15:25:12 2017-01-14 15:14:20 Media Watch on Climate Change EnergyUse December 2, 2016 EnergyUse is 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2016/12/energyuse-screenshot.png 
 1133 
 1396 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2016-12-02 16:37:08 2017-01-23 14:28:48 EnergyUse Analyzing the Digital Talk - TEDx Presentation October 22, 2016 In 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2016/12/tedx-scharl.png 
 720 
 1278 

 Arno Scharl 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Arno Scharl 2016-10-22 13:19:24 2017-01-23 14:25:54 Analyzing the Digital Talk - TEDx Presentation DecarboNet featured in the Drum Beat 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2016/09/cin-screenshot.png 
 1223 
 1867 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2016-09-29 18:30:08 2017-01-23 16:27:07 DecarboNet featured in the Drum Beat Community workshops in Liverpool March 7, 2016 The 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2016/03/DSC_0330.jpg 
 2160 
 3840 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2016-03-07 12:57:37 2016-03-07 12:57:37 Community workshops in Liverpool The Energy Trial has started November 17, 2015 DecarboNet 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2015/11/energy_trial.png 
 1314 
 2222 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2015-11-17 12:17:43 2015-11-17 12:17:43 The Energy Trial has started Collaboration with Earth Hour Global November 5, 2015 In 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2015/11/earth_hour.png 
 526 
 966 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2015-11-05 11:05:25 2015-11-05 11:05:25 Collaboration with Earth Hour Global Strong presence at EnviroInfo 2015 October 3, 2015 The 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2015/11/Converstation_3.jpg 
 336 
 505 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2015-10-03 22:34:18 2015-11-03 22:57:35 Strong presence at EnviroInfo 2015 Interested in learning about your appliances' energy consumption? July 8, 2015 If 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2015/07/Ensemble-mono-support-400-x-400.jpg 
 400 
 400 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2015-07-08 12:04:15 2015-07-08 12:07:54 Interested in learning about your appliances' energy consumption? Previous Next 
 DecarboNet  is a research project funded by the European Commission to investigate the potential of  social platforms  in mitigating climate change. Engaging the public in energy debates and encouraging behaviour change are essential strategies for reducing energy consumption and saving our planet. Studies show that information and technology alone are insufficient for changing behaviour towards more sustainable lifestyle choices, and that what is needed is a combination of socio-technical interventions. How to  raise awareness collectively  by means of social platforms and how to transform it into  behaviour change  are some of the challenges addressed by the project s research agenda. 
 DecarboNet is part of  CAPS (Collective Awareness Platforms for Sustainability  Social Innovation), an FP7 and H2020 research programme of the European Commission. DecarboNet enables new forms of social innovation and leverages emerging network effects by combining social media and distributed knowledge creation to increase awareness and identify potential solutions to problems that require collective efforts. 
 Witness DecarboNet technologies in action by exploring the  Media Watch on Climate Change ,  sharing your energy consumption experiences via the EnergyUse application, or trying out the faceted search of the  Climate Resilience Toolkit based on the webLyzard  Web intelligence  platform. 

 Raising collective awareness and promoting behaviour change towards energy saving To Mitigate Climate Change by means of social media. Previous Next 1 2 3 4 
   Latest Tweets 

 decarbonet @DecarboNet   
			        	RT  @webLyzard : Position Announcement - Assistant Professor in New Media Technology  https://t.co/gOh5Gq9GpR     #BigData   #InfoViz   https://t.co/hutMYtmUAR   

							3 months ago							 

 decarbonet @DecarboNet   
			        	RT  @webLyzard : Measuring Communication Success -  @_FIBEP  World Media Intelligence Congress   https://t.co/aCBm475PLv   #BigData   https://t.co/zrvwocuNL0   

							5 months ago							 

 decarbonet @DecarboNet   
			        	RT  @NOAAClimate : It's that time of year again! The 37th issue of the  #StateoftheClimate  report catalogs the climate in 2016.   https://t.co/4spqdiFCsg   

							6 months ago							 

 decarbonet @DecarboNet   
			        	RT  @webLyzard :  #SmartCity  Media Associations -  @TelAviv  Cities Summit   https://t.co/RaqM05uk95   @DLDTelAviv   @DLDConference   https://t.co/8CXQge5F8m   

							6 months ago							 

 decarbonet @DecarboNet   
			        	RT  @webLyzard : Semantic Systems and Visual Tools to Support Environmental Communication,  #IEEE  Systems Journal   https://t.co/kefSAKvEzL   

							8 months ago							 

  The DecarboNet research project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement  no 610829 . 

 Scroll to top 

Home - DecarboNet
===============================

Home - DecarboNet

 Home 
 The Project 

 Goals and Motivation 
 Project Structure 
 Who We Are 

 Public Results 

 Open Tools 
 Publications 
 Deliverables 

 Earth Hour 
 News 
 Contact 

 Search 

 Menu 

 Home You are here:   Home Media Watch on Climate Change December 10, 2016 The 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2014/10/cc-screenshot.png 
 1288 
 2035 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2016-12-10 15:25:12 2017-01-14 15:14:20 Media Watch on Climate Change EnergyUse December 2, 2016 EnergyUse is 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2016/12/energyuse-screenshot.png 
 1133 
 1396 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2016-12-02 16:37:08 2017-01-23 14:28:48 EnergyUse Analyzing the Digital Talk - TEDx Presentation October 22, 2016 In 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2016/12/tedx-scharl.png 
 720 
 1278 

 Arno Scharl 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Arno Scharl 2016-10-22 13:19:24 2017-01-23 14:25:54 Analyzing the Digital Talk - TEDx Presentation DecarboNet featured in the Drum Beat 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2016/09/cin-screenshot.png 
 1223 
 1867 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2016-09-29 18:30:08 2017-01-23 16:27:07 DecarboNet featured in the Drum Beat Community workshops in Liverpool March 7, 2016 The 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2016/03/DSC_0330.jpg 
 2160 
 3840 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2016-03-07 12:57:37 2016-03-07 12:57:37 Community workshops in Liverpool The Energy Trial has started November 17, 2015 DecarboNet 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2015/11/energy_trial.png 
 1314 
 2222 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2015-11-17 12:17:43 2015-11-17 12:17:43 The Energy Trial has started Collaboration with Earth Hour Global November 5, 2015 In 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2015/11/earth_hour.png 
 526 
 966 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2015-11-05 11:05:25 2015-11-05 11:05:25 Collaboration with Earth Hour Global Strong presence at EnviroInfo 2015 October 3, 2015 The 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2015/11/Converstation_3.jpg 
 336 
 505 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2015-10-03 22:34:18 2015-11-03 22:57:35 Strong presence at EnviroInfo 2015 Interested in learning about your appliances' energy consumption? July 8, 2015 If 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2015/07/Ensemble-mono-support-400-x-400.jpg 
 400 
 400 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2015-07-08 12:04:15 2015-07-08 12:07:54 Interested in learning about your appliances' energy consumption? Previous Next 
 DecarboNet  is a research project funded by the European Commission to investigate the potential of  social platforms  in mitigating climate change. Engaging the public in energy debates and encouraging behaviour change are essential strategies for reducing energy consumption and saving our planet. Studies show that information and technology alone are insufficient for changing behaviour towards more sustainable lifestyle choices, and that what is needed is a combination of socio-technical interventions. How to  raise awareness collectively  by means of social platforms and how to transform it into  behaviour change  are some of the challenges addressed by the project s research agenda. 
 DecarboNet is part of  CAPS (Collective Awareness Platforms for Sustainability  Social Innovation), an FP7 and H2020 research programme of the European Commission. DecarboNet enables new forms of social innovation and leverages emerging network effects by combining social media and distributed knowledge creation to increase awareness and identify potential solutions to problems that require collective efforts. 
 Witness DecarboNet technologies in action by exploring the  Media Watch on Climate Change ,  sharing your energy consumption experiences via the EnergyUse application, or trying out the faceted search of the  Climate Resilience Toolkit based on the webLyzard  Web intelligence  platform. 

 Raising collective awareness and promoting behaviour change towards energy saving To Mitigate Climate Change by means of social media. Previous Next 1 2 3 4 
   Latest Tweets 

 decarbonet @DecarboNet   
			        	RT  @webLyzard : Position Announcement - Assistant Professor in New Media Technology  https://t.co/gOh5Gq9GpR     #BigData   #InfoViz   https://t.co/hutMYtmUAR   

							3 months ago							 

 decarbonet @DecarboNet   
			        	RT  @webLyzard : Measuring Communication Success -  @_FIBEP  World Media Intelligence Congress   https://t.co/aCBm475PLv   #BigData   https://t.co/zrvwocuNL0   

							6 months ago							 

 decarbonet @DecarboNet   
			        	RT  @NOAAClimate : It's that time of year again! The 37th issue of the  #StateoftheClimate  report catalogs the climate in 2016.   https://t.co/4spqdiFCsg   

							6 months ago							 

 decarbonet @DecarboNet   
			        	RT  @webLyzard :  #SmartCity  Media Associations -  @TelAviv  Cities Summit   https://t.co/RaqM05uk95   @DLDTelAviv   @DLDConference   https://t.co/8CXQge5F8m   

							7 months ago							 

 decarbonet @DecarboNet   
			        	RT  @webLyzard : Semantic Systems and Visual Tools to Support Environmental Communication,  #IEEE  Systems Journal   https://t.co/kefSAKvEzL   

							8 months ago							 

  The DecarboNet research project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement  no 610829 . 

 Scroll to top 

Home - DecarboNet
===============================

Home - DecarboNet

 Home 
 The Project 

 Goals and Motivation 
 Project Structure 
 Who We Are 

 Public Results 

 Open Tools 
 Publications 
 Deliverables 

 Earth Hour 
 News 
 Contact 

 Search 

 Menu 

 Home You are here:   Home Media Watch on Climate Change December 10, 2016 The 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2014/10/cc-screenshot.png 
 1288 
 2035 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2016-12-10 15:25:12 2017-01-14 15:14:20 Media Watch on Climate Change EnergyUse December 2, 2016 EnergyUse is 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2016/12/energyuse-screenshot.png 
 1133 
 1396 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2016-12-02 16:37:08 2017-01-23 14:28:48 EnergyUse Analyzing the Digital Talk - TEDx Presentation October 22, 2016 In 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2016/12/tedx-scharl.png 
 720 
 1278 

 Arno Scharl 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Arno Scharl 2016-10-22 13:19:24 2017-01-23 14:25:54 Analyzing the Digital Talk - TEDx Presentation DecarboNet featured in the Drum Beat 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2016/09/cin-screenshot.png 
 1223 
 1867 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2016-09-29 18:30:08 2017-01-23 16:27:07 DecarboNet featured in the Drum Beat Community workshops in Liverpool March 7, 2016 The 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2016/03/DSC_0330.jpg 
 2160 
 3840 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2016-03-07 12:57:37 2016-03-07 12:57:37 Community workshops in Liverpool The Energy Trial has started November 17, 2015 DecarboNet 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2015/11/energy_trial.png 
 1314 
 2222 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2015-11-17 12:17:43 2015-11-17 12:17:43 The Energy Trial has started Collaboration with Earth Hour Global November 5, 2015 In 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2015/11/earth_hour.png 
 526 
 966 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2015-11-05 11:05:25 2015-11-05 11:05:25 Collaboration with Earth Hour Global Strong presence at EnviroInfo 2015 October 3, 2015 The 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2015/11/Converstation_3.jpg 
 336 
 505 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2015-10-03 22:34:18 2015-11-03 22:57:35 Strong presence at EnviroInfo 2015 Interested in learning about your appliances' energy consumption? July 8, 2015 If 

 https://www.decarbonet.eu/wp-content/uploads/sites/23/2015/07/Ensemble-mono-support-400-x-400.jpg 
 400 
 400 

 Lara Schibelsky Godoy Piccolo 

 https://sites.weblyzard.com/playground/wp-content/uploads/sites/23/2014/10/decarbonet-logo2.png 

 Lara Schibelsky Godoy Piccolo 2015-07-08 12:04:15 2015-07-08 12:07:54 Interested in learning about your appliances' energy consumption? Previous Next 
 DecarboNet  is a research project funded by the European Commission to investigate the potential of  social platforms  in mitigating climate change. Engaging the public in energy debates and encouraging behaviour change are essential strategies for reducing energy consumption and saving our planet. Studies show that information and technology alone are insufficient for changing behaviour towards more sustainable lifestyle choices, and that what is needed is a combination of socio-technical interventions. How to  raise awareness collectively  by means of social platforms and how to transform it into  behaviour change  are some of the challenges addressed by the project s research agenda. 
 DecarboNet is part of  CAPS (Collective Awareness Platforms for Sustainability  Social Innovation), an FP7 and H2020 research programme of the European Commission. DecarboNet enables new forms of social innovation and leverages emerging network effects by combining social media and distributed knowledge creation to increase awareness and identify potential solutions to problems that require collective efforts. 
 Witness DecarboNet technologies in action by exploring the  Media Watch on Climate Change ,  sharing your energy consumption experiences via the EnergyUse application, or trying out the faceted search of the  Climate Resilience Toolkit based on the webLyzard  Web intelligence  platform. 

 Raising collective awareness and promoting behaviour change towards energy saving To Mitigate Climate Change by means of social media. Previous Next 1 2 3 4 
   Latest Tweets 

 decarbonet @decarbonet   
			        	RT  @webLyzard : Position Announcement - Assistant Professor in New Media Technology  https://t.co/gOh5Gq9GpR     #BigData   #InfoViz   https://t.co/hutMYtmUAR   

							3 months ago							 

 decarbonet @decarbonet   
			        	RT  @webLyzard : Measuring Communication Success -  @_FIBEP  World Media Intelligence Congress   https://t.co/aCBm475PLv   #BigData   https://t.co/zrvwocuNL0   

							6 months ago							 

 decarbonet @decarbonet   
			        	RT  @NOAAClimate : It's that time of year again! The 37th issue of the  #StateoftheClimate  report catalogs the climate in 2016.   https://t.co/4spqdiFCsg   

							6 months ago							 

 decarbonet @decarbonet   
			        	RT  @webLyzard :  #SmartCity  Media Associations -  @TelAviv  Cities Summit   https://t.co/RaqM05uk95   @DLDTelAviv   @DLDConference   https://t.co/8CXQge5F8m   

							7 months ago							 

 decarbonet @decarbonet   
			        	RT  @webLyzard : Semantic Systems and Visual Tools to Support Environmental Communication,  #IEEE  Systems Journal   https://t.co/kefSAKvEzL   

							8 months ago							 

  The DecarboNet research project has received funding from the European Union's Seventh Framework Programme for research, technological development and demonstration under grant agreement  no 610829 . 

 Scroll to top
 WHOLE PROJECT MARK