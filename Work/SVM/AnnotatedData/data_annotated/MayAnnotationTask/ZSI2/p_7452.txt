PROJECT NAME: CKAN
PROJECT SOURCE: Open Knowledge International
---------------------------------------
NEW PAGE: 

ckan – The open source data portal software
===============================

ckan   The open source data portal software

 Home 
 About 

 CKAN Association 
 Association Members 
 Steering Group 
 Technical Team 
 Community   Comms 
 Govt Working Group 
 CKAN instances 

 Features 
 Case Studies 
 Resources and Support 

 Download and Install 
 Documentation 
 Community Support 
 Commercial Support 
 All Resources 
 Contact 

 Blog 
 FAQs 

 Download CKAN 

 Home 
 About 

 CKAN Association 
 Association Members 
 Steering Group 
 Technical Team 
 Community   Comms 
 Govt Working Group 
 CKAN instances 

 Features 
 Case Studies 
 Resources and Support 

 Download and Install 
 Documentation 
 Community Support 
 Commercial Support 
 All Resources 
 Contact 

 Blog 
 FAQs 

 demo.ckan.org 

 www.europeandataportal.eu 

 data.gov.sg 
 data.gov.au 
 open.canada.ca/data 
 data.environment.nsw.gov.au 
 dataplatform.nl 

 CKAN, the world s leading Open Source data portal platform 
 CKAN is a powerful data management system that makes data accessible   by providing tools to streamline publishing, sharing, finding and using data. 

 What is CKAN? 
 CKAN is aimed at data publishers (national and regional governments, companies and organizations) wanting to make their data open and available.  Learn more 

 Why CKAN? 
 CKAN is open source, free software. This means that you can use it without any license fees, and you retain all rights to the data and metadata you enter. 

 How to contribute? 
 CKAN version releases are coordinated, tested and deployed by the tech team. Being an open source project, CKAN and its extensions are developed by a large community of people. 

 CKAN is used by: 

 Try it for yourself. Register, login and share your data. 
 Demo CKAN  

CKAN Association

The CKAN Association, established in 2014, manages and oversees the CKAN project, supporting the growth of CKAN, its community and stakeholders. Key aspects of the Association are:

    A Steering Group and Advisory Group which oversee the project and represent stakeholders.
    Specific teams to look after particular areas such as a “Technical Team” to oversee technical development and a “Community and Communications team” to oversee materials (including project website) and drive community engagement
    A Membership program for organizations and individuals to participate in CKAN and contribute to its long-term sustainability

The Association has its formal institutional home at the Open Knowledge Foundation but is autonomous and has its own independent governance, in the form the Steering Group which is drawn from major CKAN stakeholders. The Open Knowledge Foundation, who are the original creators of CKAN, will continue to contribute to CKAN at all levels but the Association allows others – from government users to suppliers of CKAN services – to have a formal role in the development of the CKAN project going forward.
Membership

CKAN Association membership is a way for individuals, companies and organisations to support the CKAN Project and be recognised for doing so. By becoming a member you are helping to ensure the long-term sustainability of CKAN.

Members are expected to contribute resources – either through contributing money or providing in-kind resources such as staff time. Members receive recognition for their contribution through display on the website, participation in events etc.

See the Members page for more details including the Membership tiers.
Organization and Structure
Steering Group

This will be made out of the key stakeholders who have committed to oversee and steer the CKAN Association going forward. See the Steering Group page for more information.
Technical Team

The technical team are the “official committers” for the CKAN software and related technologies. See the Technical Team page for more information.
Community and Communications Team

The community and communications team manage engagement, promotion and outreach around CKAN. See the Community and Communication Team page for more details.
Advisory Group

The Advisory Group members are representatives of organizations and companies that support CKAN.

While the Advisory Group does not have decision-making authority for the CKAN Association, its members communicate with the Steering Group and help it guide the overall direction of CKAN and the CKAN Association.

The Group meets regularly on the phone and at least once a year in person, usually at OKFestival. The aim of these meetings is to

    Understand the Advisory Group’s needs, and express them to the community.
    Express the community’s needs to the Advisory Group, especially when it’s something they can help with.
    Get feedback from the Advisory Board on our plans.

Note: usually to be a member of the Advisory Board, an organization must be a Gold member (or if less than 30 employees Silver Member). This requirement may be waived for invited non-profit organizations.
Frequently Asked Questions
Who Will Oversee and Set the CKAN Technical Roadmap

The CKAN Technical Roadmap will be overseen and controlled by the Technical Team. The Steering Group help steer and oversee the overall project but do not have any direct input to the Roadmap as that is owned by the Technical Team. 

CKAN Association Steering Group

The Steering Group is a key part of the CKAN Association. It is made up of key stakeholders who have committed to oversee and steer the CKAN Association going forward.
Current Members

The Steering Group is Chaired by Ashley Casovan and consists of five members:
Ashley Casovan

Ashley Casovan

Canadian Government

Government of Canada Open Government Portal Lead. An engaged, passionate, and innovative public servant. Curious about how new technology has…
View Profile
Steven De Costa

Steven De Costa

Link Digital

Executive Director of Link Digital, member of the Steering Group and Community Team Lead for the CKAN Association and founder of DataShades.com. He is former national organizer of GovHack.org and Board member of Open Knowledge Australia.
View Profile
Paul Walsh

Paul Walsh

Open Knowledge

Paul is Head of Technical Product at Open Knowledge International. We are a non-profit dedicated to openness and transparency, and…
View Profile 
 This site uses cookies.  More info
 WHOLE PROJECT MARK
