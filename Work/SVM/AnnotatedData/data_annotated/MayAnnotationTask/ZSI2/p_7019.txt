PROJECT NAME: Firefox OS
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 Firefox OS[3] (project name: Boot to Gecko, also known as B2G)[4] is a Linux-based open-source operating system for smartphones and tablet computers and is set to be used on smart TVs.[5] It is being developed by Mozilla, the non-profit organization best known for the Firefox web browser.

Firefox OS is designed to provide a "complete"[6] community-based alternative system for mobile devices, using open standards and approaches such as HTML5 applications, JavaScript, a robust privilege model, open web APIs to communicate directly with cellphone hardware,[4] and application marketplace. As such, it competes with proprietary systems such as Apple's iOS, Google's Android and Microsoft's Windows Phone,[6] as well as other open source systems such as Jolla's Sailfish OS and Ubuntu Touch.

Firefox OS was publicly demonstrated in February 2012, on Android-compatible smartphones,[7][8] and again in 2013 running on Raspberry Pi.[9] In January 2013, at CES 2013, ZTE confirmed they would be shipping a smartphone with Firefox OS,[10] and on July 2, 2013, Telefónica launched the first commercial Firefox OS based phone, ZTE Open, in Spain[11][12] which was quickly followed by GeeksPhone's Peak+.[13]

source : wikipedia 

Firefox OS[3] (project name: Boot to Gecko, also known as B2G)[4] is a Linux-based open-source operating system for smartphones and tablet computers and is set to be used on smart TVs.[5] It is being developed by Mozilla, the non-profit organization best known for the Firefox web browser.

Firefox OS is designed to provide a "complete"[6] community-based alternative system for mobile devices, using open standards and approaches such as HTML5 applications, JavaScript, a robust privilege model, open web APIs to communicate directly with cellphone hardware,[4] and application marketplace. As such, it competes with proprietary systems such as Apple's iOS, Google's Android and Microsoft's Windows Phone,[6] as well as other open source systems such as Jolla's Sailfish OS and Ubuntu Touch.

Firefox OS was publicly demonstrated in February 2012, on Android-compatible smartphones,[7][8] and again in 2013 running on Raspberry Pi.[9] In January 2013, at CES 2013, ZTE confirmed they would be shipping a smartphone with Firefox OS,[10] and on July 2, 2013, Telefónica launched the first commercial Firefox OS based phone, ZTE Open, in Spain[11][12] which was quickly followed by GeeksPhone's Peak+.[13]

source : wikipedia
 WHOLE PROJECT MARK