PROJECT NAME: Nova Iskra
PROJECT SOURCE: SI DRIVE  case studies: employment
---------------------------------------
NEW PAGE: 

 NOVA ISKRA is a network of designers and creative consultants, a platform, and a meeting space, who carry out
projects in which young, professionals (unemployed persons) can participate and built up work experience: it offers
a space to meet and it creates opportunities for employment and self-employment, and it is targeting young
(upcoming) social entrepreneurs. NOVA ISKRA, a hybrid, transdisciplinary platform and a fully independent venture
that connects the creative community, links it to the potential businesses and supports the realization of their ideas
on the market; it combines both organizational and service innovations. For many young professionals from the
creative field in Serbia, where the youth unemployment is very high (almost 50%, at the moment), NOVA ISKRA is
the place where they can both improve themselves and find a job (the same applies to SMEs). More importantly,
they have been matched accurately (their particular skills mix and qualifications well-suited to jobs). 

NOVA ISKRA is a network of designers and creative consultants, a platform, and a meeting space, who carry out
projects in which young, professionals (unemployed persons) can participate and built up work experience: it offers
a space to meet and it creates opportunities for employment and self-employment, and it is targeting young
(upcoming) social entrepreneurs. NOVA ISKRA, a hybrid, transdisciplinary platform and a fully independent venture
that connects the creative community, links it to the potential businesses and supports the realization of their ideas
on the market; it combines both organizational and service innovations. For many young professionals from the
creative field in Serbia, where the youth unemployment is very high (almost 50%, at the moment), NOVA ISKRA is
the place where they can both improve themselves and find a job (the same applies to SMEs). More importantly,
they have been matched accurately (their particular skills mix and qualifications well-suited to jobs).
 WHOLE PROJECT MARK