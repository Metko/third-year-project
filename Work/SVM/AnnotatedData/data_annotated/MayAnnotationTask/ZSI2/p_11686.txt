PROJECT NAME: MAMMU
PROJECT SOURCE: SI-drive
---------------------------------------
NEW PAGE: 

MAMMU / about
===============================

MAMMU / about

 home 

 collections 

 shop 

 blog 

 production 

 contact 

 about 

 lookbook 

 MAMMU is a fashion company that closely cooperates with young Latvian mothers in need. Many of them are less mobile in the job market due to their inability to work fixed working hours. Rather often these women cannot enroll their children into a kindergarten, as the number of places available is limited. If they cannot afford to hire a nanny, a full time job is not an option for them. The state social benefit is so small that these women virtually have to survive on an income way below a minimum subsistence level. 
 Involvement of these mothers in the business of MAMMU providing them with flexible working hours is both ends and means of the business activity of this social business company. MAMMU organizes their training, teaches them skills required for production of MAMMU fashion wares and provides them with information needed for setting up their own micro-enterprises for production. After completion of the training stage MAMMU provides these women with materials needed for production. Once the work is done, MAMMU buys these wares from mothers. 
 Being a social business, MAMMU is rather a cause than a profit driven enterprise, and the aim of the MAMMU team is the provision of social benefits for mothers, meanwhile producing creative and  high-quality fashion products. 
 Here at MAMMU we follow the 7 Grameen principles: 

 // Our business objective is to overcome poverty, or problems which threaten people and society, not profit maximization.  // We aim for financial and economic sustainability, not charity. // Investors only get back their investment amount. No dividends are paid beyond the initial investment money. // When the investment amount is paid back, the company's profit stays with the company for expansion and improvement. // Our company is environmentally conscious.The workforce gets the market wage with better working conditions. // ...We do it with joy 

 berta 

MAMMU / about
===============================

MAMMU / about

 home 

 collections 

 shop 

 blog 

 production 

 contact 

 about 

 lookbook 

 MAMMU is a fashion company that closely cooperates with young Latvian mothers in need. Many of them are less mobile in the job market due to their inability to work fixed working hours. Rather often these women cannot enroll their children into a kindergarten, as the number of places available is limited. If they cannot afford to hire a nanny, a full time job is not an option for them. The state social benefit is so small that these women virtually have to survive on an income way below a minimum subsistence level. 
 Involvement of these mothers in the business of MAMMU providing them with flexible working hours is both ends and means of the business activity of this social business company. MAMMU organizes their training, teaches them skills required for production of MAMMU fashion wares and provides them with information needed for setting up their own micro-enterprises for production. After completion of the training stage MAMMU provides these women with materials needed for production. Once the work is done, MAMMU buys these wares from mothers. 
 Being a social business, MAMMU is rather a cause than a profit driven enterprise, and the aim of the MAMMU team is the provision of social benefits for mothers, meanwhile producing creative and  high-quality fashion products. 
 Here at MAMMU we follow the 7 Grameen principles: 

 // Our business objective is to overcome poverty, or problems which threaten people and society, not profit maximization.  // We aim for financial and economic sustainability, not charity. // Investors only get back their investment amount. No dividends are paid beyond the initial investment money. // When the investment amount is paid back, the company's profit stays with the company for expansion and improvement. // Our company is environmentally conscious.The workforce gets the market wage with better working conditions. // ...We do it with joy 

 berta
 WHOLE PROJECT MARK