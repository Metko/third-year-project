PROJECT NAME: Open Data for Tax Justice
PROJECT SOURCE: Open Knowledge International
---------------------------------------
NEW PAGE: 

Open Data for Tax Justice
===============================

Open Data for Tax Justice

                #OD4TJ

 Open Data for Tax Justice 
 We re working to create a global network of people and organisations using open data to improve advocacy, journalism and public policy around tax justice. 
 If you d like to join us, please email the network coordinators using the link below. 
 Paradise lost paper   White Paper     Contact us 

 Member Organisations 

 Open Data Services Co-operative 

 Public Data Lab 

 SOMO 

 Save the Children (UK) 

 Financial Transparency Coalition 

 Web Foundation 

 Christian Aid 

 Global Justice 

 Eurodad 

 African Network of Centers for Investigative Reporting 

 CBGA, India 

 Code for Africa 

 Global Alliance for Tax Justice 

 Global Initiative for Financial Transparency 

 Global Integrity 

 Global Witness 

 International Budget Partnership 

 International Centre for Tax and Development 

 Latindadd 

 LittleSis 

 Natural Resource Governance Institute 

 Open Knowledge International 

 Oxfam 

 Sunlight Foundation 

 Tax Hack 

 Tax Justice Network 

 Tax Justice Network Africa 

 Transparency International 

 War on Want 

 Finance Uncovered 

 Member Individuals 

          Jonathan Gray Prize Fellow, Institute for Policy Research, University of Bath 

          Caelainn Barr Data Journalist, The Guardian 

          Simon Bowers Senior Financial Reporter, The Guardian 

          Mar Cabra Head of Data   Research, The International Consortium of Investigative Journalists (ICIJ) 

          Petr Jansk Assistant Professor Charles University in Prague 

          Charles Kenny Senior Fellow, Center For Global Development 

          Richard Murphy Professor of Practice in International Political Economy, City University London 

          Sol Picciotto Professor Emeritus, Lancaster University Law School and BEPS Monitoring Group 

          Vijaya Ramachandran Senior Fellow, Center For Global Development 

          Attiya Waris Senior Lecturer, Faculty of Law, University of Nairobi 

          Gabriel Zucman Assistant Professor, Department of Economics, University of California, Berkeley 

      The #OD4TJ network is coordinated by Open Knowledge and the Tax Justice Network.

 About 

 Privacy policy 
 IP policy 
 Cookie policy 
 Terms of use
 WHOLE PROJECT MARK