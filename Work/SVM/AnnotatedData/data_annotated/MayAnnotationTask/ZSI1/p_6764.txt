PROJECT NAME: Makerplane
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

MakerPlane.org | Open Source Aviation
===============================


	Plugin URL: Super Simple Google AnalyticsYou've chosen to prevent the tracking code from being inserted on 
	any page. 

	You can enable the insertion of the tracking code by going to 
	Settings > Google Analytics on the Dashboard.Start LogoOpen Source AviationEnd LogoStart SearchboxEnd SearchboxHomeBlogStoreGetting StartedGetting StartedHow Can I Contribute?Aircraft Build StatusToolsThe Hangar WorkshopIdeaForgeReferencesLicensesTask List/BacklogDownloadsForumAbout UsThe TeamSubscribeDonate/SponsorSupported ByMember LoginContact Usend #headerStart FeaturedPreviousNextFeatured ContentFeatured ArticlesNew Stick Shaker Product21 July 2014. MakerPlane and partner Vx Aviation today announced a low-cost, easy-to install haptic stick shaker for Experimental Amateur Built Aircraft. The SWZL-1A is an aircraft “stick shaker” controller system that provides haptic pre-stall warning based on a stall vane switch or Angle of Attack (AoA) information received from an electronic flight information system (EFIS) serial data stream.  The device is compatible with EFIS devices from Dynon, Garmin and Grand Rapids Technology (GRT).  See the full press release here....Read Moreend .descriptionend .slideNew Auto-Trim Product16 July 2014. MakerPlane and its development partner Vx Aviation today announced an enhanced version of the popular M-PWR-2 trim controller.  The M-PWR-2AT adds automatic pitch and roll trim control to aircraft that use Dynon SkyView autopilot systems.  For more information see the full announcement here....Read Moreend .descriptionend .slideOpen Source Dual Channel Trim Motor Controller...We are very happy to announce another open source avionics project released by MakerPlane !  The M-PWR-2 is a two-channel variable speed trim motor control device developed primarily for amateur-built aircraft applications.   The device features two all-electronic motor drive channels in one compact package, capable of controlling Ray Allen (RAC) or Firgelli servo motors directly. Read the full post here....Read Moreend .descriptionend .slideOpen Source Magneto Signal Stabilizer...We are very happy to announce another open source avionics project released by MakerPlane from Vern Little !   This is also available for purchase in the MakerPlane Store. The P-TACH converts the electrically noisy and variable magneto P-Lead signal from aircraft magnetos into a stable digital pulse waveform for processing by modern digital engine monitoring systems. Read more here on the Blog....Read Moreend .descriptionend .slideend #featured_contentEnd Featured ArticlesFeatured MenuEnd Featured Menuend #featuredEnd Featured ContentEnd FeaturedMakerPlane Vision We want more people to safely and quickly build and fly their own airplanes. (Use your mouse to look at the design below more closely.) MakerPlane Project OverviewMakerPlane is an open source aviation organization which will enable people to build and fly their own safe, high quality, reasonable cost plane using advanced personal manufacturing equipment such as CNC mills and 3D printers.  Our  projects  include open source avionics software to enable state-of-the-art digital flight instruments and display capabilities.Basically we are designing an aircraft that can be built on a computer controlled mill at home, or at a makerspace which is easy to assemble and quick to build.  The plans and instructions will be available for free to anyone that wants them!The general specifications for the first MakerPlane aircraft (v1.0) can be found on the MP 1.0 project page located inThe Hangar Workshop.  A direct link to the documents section is here:MP Documents Click here to Learn More About MakerPlane Shareend .entryend #content-areaRecent PostsMP LSA Build Progress for OshkoshNew Stick Shaker ProductNew Product : M-PWR-2AT Auto-Trim for Dynon SkyViewCNC Hotwire AttachmentMakerPlane Parts on CNCDIY CNC Cable Carrierend .widgetNewsletter Signup FormSign up for our newsletter and get the latest in updates, articles and information!Subscribe to our newsletter 

About Us
The Team

The MakerPlane team are passionate about aviation, open source and the maker movement!

Contributors and volunteers to the project come from all over the world proving that this is truly a global initiative.   Stay tuned as we update this page with bios of the leading contributors to the MakerPlane project.
txspacer

 
John Nicol

	
John Nicol, Founder MakerPlane	John is the founder of MakerPlane. A pilot, aircraft homebuilder and maker. A former New Zealand Army Officer, he has also been in the high-tech industry as an executive in the Public Safety and Defence industries in New Zealand and Canada including as CEO of a Canadian Top 40 Defence company. John left Lockheed Martin in 2012 as a Principal Engineer and was involved in the launch to market of the Lockheed Martin Prepar3D flight simulation product. John lives just north of Ottawa, Canada with his wife and son.

 
Jeffrey Meyer

	
	Jeffrey is the lead aeronautical engineer responsible for the MakerPlane v1.0 LSA design and has over 40 years general engineering experience. He has a BSc. Mechanical Engineering and MSc. Aeronautical Engineering from the Technion, Israel Institute of Technology. Jeffrey is currently an adjunct lecturer in the Aerospace Engineering Facility of the Technion. Jeffrey says, "If it flies I like it, but also dabble in electronics, software development, R/C modeling, and teaching kids and university students about aero modeling. Private pilot and glider pilot licenses, long expired – too expensive and troublesome over here."

 
Vern Little

	
	Vern Little comes from the semiconductor and networking equipment industry. As a founder of one of Canada’s largest semiconductor companies, he has expertise in building successful companies including engineering, marketing, business development and venture fundraising in Canada and the US. Vern retired as a director of product research with ten US patents and has built two aircraft—an RV-9A and a Harmon Rocket II. As the principal of Vx-Aviation, Vern pioneered the use of d-sub packages that contain complex avionics functions and generic wiring hubs that reduce aircraft wiring cost and complexity. Vern is a technology business mentor in Victoria, BC.

 
Dave Covert

	
	Dave is a software developer and business owner with a BSc. Computer Science and a BSc. Electrical Engineering from Texas A&M University. In the past twenty years Dave has worked for Microsoft and started two technology companies (sold one). He is a SEL pilot who once owned a Grumman AA-5, and with interests in Augmented Reality, CAD/CAM, 3D printing, and fly-by-wire technologies, now finds himself lusting after a LSA to try some ideas out on. His guiding principle in aviation is ‘Flying should be easy.’ Dave lives in the country about 30 miles south of Houston, Texas.

 
Phil Birkelbach

	
	Phil has a degree in Mechanical Engineering Technology from Texas A&M University and is a Project Engineer for a company that designs and builds control systems for the offshore oil and gas production industry. He lives in Waller, a small town 30 miles NW of Houston Texas, with his lovely wife and way too many animals. He learned to fly when I was 13 years old and soloed a glider at 14. Phil has a PPL with SEL and Glider ratings. He built his RV-7 from 2001 thru 2005.

Supported By

We gratefully receive support from the following organizations:

 
Getting Started
What is MakerPlane?

 

MP1-0 side render white background 2 three prop

MakerPlane is an open source aviation organization which will enable people to build and fly their own safe, high quality, reasonable cost aircraft using advanced personal manufacturing equipment such as CNC mills and 3D printers.  MakerPlane also includes open source avionics and software to enable state-of-the-art digital flight instruments and display capabilities.

Basically we are designing an aircraft that can be built on a computer controlled mill at home, or at a makerspace which is easy to assemble and quick to build.  The plans and instructions will be available for free to anyone that wants them!  Also see our “How Can I Contribute?” page.

 
What does MakerPlane have to offer?

 

We are currently in the process of designing and building our first aircraft (due for flight in late 2014 with plans released 2015). The general specifications for the first MakerPlane aircraft (v1.0) can be found on the MP 1.0 project page located in The Hangar Workshop.  (A direct link to the documents section is here:  MP Documents.)

Avionics and other items are available with a vibrant community contributing to many aspects of open source aviation right now however!    What you can expect from MakerPlane:

    Free open source hardware plans for avionics (see our Downloads page)
    Free open source project Repository (The Hangar Workshop) for you to start your own project
    Online Store to purchase open source hardware kits or assembled products
    IdeaForge to generate and discuss aviation related ideas for new projects
    Book and Publication References of our favourite resources
    Community Forum for discussion and sharing
    Learn how we are designing and building our aircraft!  Tell us what you think!  Check out our Status page for the build.

AXIS-15A-photo

Our future plans include:

    3D model availability for printing 3D objects relevant to aviation (flight grips, knobs, tools etc)
    CNC library for aviation related projects (workbenches, tools, hangars etc)
    Supply of parts and kits to build MakerPlane aircraft
    Potentially providing certified versions of MakerPlane aircraft
    Growing the avionics library including EFIS, Arduino and Android-based projects

Our Strategic Mission and Goals

To provide a more formal overview as an organization, the following mission and goals are stated for the record!
Mission

The mission of MakerPlane is to create innovative and game-changing aircraft, avionics and related systems and the transformational manufacturing processes to build them.  As a result of this aim, aircraft can be built with consistent, repeatable and highly accurate processes which create safer flying at lower cost.
MP 1-0 with wing 2 draft
Goal

MakerPlane strives to become the global hub for open source aviation community activity.   A friendly and open collaborative environment that reflects the amazing attitudes of the pilots and helpers that you will find at any flying club on any given day!
High-Level Objectives/Timeline MakerPlane v1.0 Aircraft Design

MakerPlane aims to achieve the following milestones for our first aircraft design:

    Launch the MakerPlane Open Source Aviation organization officially at AirVenture 2012, 23-29 July – Done! √
    Present a full-sized non-flying prototype in time for AirVenture 2014.  By this time major design elements have been finalized and build/assembly materials and processes are being prepared
    Present a flying MakerPlane version 1.0 at AirVenture 2015.  Test flights are complete and suitable number of hours are flown to confirm all design and performance criteria.
    AirVenture 2015.  All files, plans and instructional material is available as open source from the MakerPlane site.

 How Can I Contribute?
How Can I Contribute?	

Seeking advice from other Open Source Hardware initiatives, we have created this page to highlight areas of contribution that would really help out MakerPlane.   There are so many people that want to be involved and to paraphrase Chris Anderson of 3D Robotics and DIY Drones, “One of the biggest challenges in an open source development project is getting the “Architecture of Participation” right.”  We should be in a position to allow anyone with various levels  of skills and experience the opportunity to contribute in some way to MakerPlane.    People that find our organization on-line and want to help are enthusiastic and get what we are doing, we must be in a position to focus the efforts of the various volunteers and contributors so that we don’t lose that energy!

What people see on our site is honestly the tip of the iceberg in terms of the efforts that are going on constantly behind the scenes to create and maintain the ongoing MakerPlane site and projects.  Lots of emails flying back and forth, Google + hangouts and conferences, private messages on the forums and sometimes packages being sent across the world with electronics and ideas!   We have implemented an Agile project management process for the design and build of the first aircraft with a series of 6 week “Sprints” or work packages.  These work packages are also called the “backlog” of tasks.

pedals3We are a bit behind due to other work commitments and life getting in the way, but still relatively on track in the bigger picture.  A great implementation of the Agile process can be seen within an other open source hardware project called WikiSpeed.   Joe Justice, founder of Wikispeed shares an excellent overview of the process here.   In time, we will provide a MakerPlane specific brief of the process with my particular “Capability Engineering” twist!  The avionics projects in the repository are not as structured and are either under the control of a specific project leader, or are legacy projects looking for some TLC.

To see our current backlog of tasks being undertaken, go to this link:  http://makerplane.org/?page_id=381

If you have come to this page asking yourself, “How Can I Contribute to MakerPlane?”, then here are some specific areas that I have identified.

Updated: 14 Jan 2012
MakerPlane v1.0 LSA Design and Build projects we need volunteers for

    Solidworks CAD experience to assist Jeffrey with the 3D build of sub-assemblies
    Aeronautical engineer to assist in confirmation of some of the aircraft design elements
    Landing Gear detailed design
    Removable wing concept detailed designModelling the brake cylinder sml
    Firewall forward detailed design – assistance with selection of powerplant
    Flight Controls detailed design
    3D model created for 3D printing of a workable stick grip handle – similar to this design potentially:  http://www.aircraftspruce.ca/catalog/graphics/11-02000.jpg
    Layout/specifications of MakerPlane construction and assembly manuals and other multi-media content

Communications and Community Building items that we need volunteers for

    Blog and News articles about open source hardware and software, aviation related topics, how MakerPlane could be used for x, y or z etc.  Post suggestions for articles that you want to contribute and we will get them online.
    Case Studies for implementation of open source hardware and software in aviation would be really interesting.  Maybe you have built an aircraft using the MakerPlane avionics….
    Moderators for topics of interest in the MakerPlane Forums
        Particularly for the CNC topics and Avionics (you should have some experience in these to be a mod)
    Active participation in the forums to ask questions, provide feedback, contribute ideas!
    Evangalists to promote MakerPlane to EAA chapters, schools, academia and so on.  In fact it would be fantastic to see MakerPlane used in schools all over the world to help kids get into aviation!
    A volunteer coordinator to look after these activities would be awesome!

Avionics projects that we need volunteers for

    Phil B. is currently working on an update to the CAN-FIX protocol draft.  We need volunteers interested in reviewing the spec, providing feedback and getting involved with it.  Once the spec is released, we will start building example projects for Arduino and other controllers and then designing avionics based upon it.
    CAN-FIX API.  Need some assistance for Phil to potentially write an API for CAN-FIX.
    There are several projects in the repository that need updating and documenting.   These are the legacy projects kindly donated by Matjaz Vidmar.  Take one on and get-er-done!
    We are getting the code for and Open Source EFIS.  We need a dedicated team of highly skilled and enthusiastic software developers with aviation/avionics experience to take this to the next level!  This is one of those game-changer exciting projects, so we want to here from you NOW!  Just imagine an open source EFIS which you can put together with hardware for $500 or less……

 3D Printing projects that we need volunteers for

    We would like to get a library of 3D models together specific to aviation such as stick grips, throttle knobs, door handles, tools, jigs etc.  Basically non-structural items.  The library could be hosted on MakerPlane, or Thingiverse.
    Ted S. is the new moderator for this forum and ideas, open source designs and models welcome!

CNC projects that we need volunteers for

    We would like to get a library of CNC files together that might be interesting to people interested in aviation, Prototype_Progress_Firewall_Bottom1aircraft homebuilding and so on.  The library of items could include workshop items such as cabinets, work-tables, jigs, tools and so on.   These might be interesting items that EAA chapters could use in their workshops, hangers or classrooms as well like chairs, tables and BBQ smokers!  🙂
    A moderator with CNC experience for this forum is required.

Oshkosh 2013

    Assistance helping out with the MakerPlane booth at Oshkosh would be awesome.  Volunteer and get a free week pass to the show! (six passes available

 Aircraft Build Status

Updated 2 Sep 14

As we progress with the design and build process for the MakerPlane v1.0 LSA aircraft and other major projects, we will update this page with the latest graphics here.

The general specifications for this aircraft can be found within the project page located in The Hangar Workshop.  A direct link to the documents section is here:  MP Documents

txspacer
Prototype Build Progress

The prototype aircraft build status is contained in various blog updates (tag “prototype”).

 24 July
Sprint Plan

Using Agile/Scrum methodologies, we have broken the project down to 6 week work packages called “sprints”.  Each of these sprints will have defined tasks.  If we do not achieve the tasks in the current sprint, they get moved to the next one.  There is a great overview of this process at Wikispeed, which is an open source car project (cool!).   Here is the current high-level overview of our project plan from the Sprint viewpoint.  It will take us to the flying aircraft in 2014.

sprint-overview

 

 
MakerPlane v1.0 LSA

This is initially a snapshot of the detailed design progress.  The physical prototype build log will be added once we begin this phase.  This will include any full-scale mock ups and prototypes of sub-systems.  Let us know if you have any suggestions or thoughts on how we present this data!

 

main-progress-10-oct

 

Each of the boxes above represents a different aircraft system, which is further broken down into sub-systems.  In the images below, each top yellow box represents the overall status of it’s components underneath it.

 

airframe-10-oct

 

landing-gear-10-oct

 

Propulsion

 

Fuel

 

Electrical

 

flight-controls-10-oct

 

Navigation

 

Safety 

 Open source aviation developed by contributors and volunteers 

Open source aviation developed by contributors and volunteers
 WHOLE PROJECT MARK
