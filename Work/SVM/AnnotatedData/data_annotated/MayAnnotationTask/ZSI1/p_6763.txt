PROJECT NAME: Wikispeed
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

WIKISPEED | Changing the world for the better
===============================

WIKISPEED | Changing the world for the better

 No products in the cart. 

 CART  
 Total: $ 0.00 

 WHO WE ARE 

 FAQ 

 Press 
 EVENTS 
 SCRUM 

 The WIKISPEED process 
 Sprints 
 Bring WIKISPEED to Your Business 

 Projects 

 Car 
 Extreme Manufacturing 
 MicroFactory 
 MicroHouse 
 Challenges 

 WIKISPEEDlet Challenge   $1,000 
 3d Printed Differential Challenge   $1,000 
 3d Printed Transmission Challenge   $1,000 

 WHAT S NEW 
 The Team 

 Join the revolution 
 Locations 

 Boulder, CO, USA 
 Burleson, TX, USA 
 Lynnwood, WA, USA 

 Donate 
 Calendar 

 Store 
 My Wikispeed 

 WHO WE ARE 

 FAQ 

 Press 
 EVENTS 
 SCRUM 

 The WIKISPEED process 
 Sprints 
 Bring WIKISPEED to Your Business 

 Projects 

 Car 
 Extreme Manufacturing 
 MicroFactory 
 MicroHouse 
 Challenges 

 WIKISPEEDlet Challenge   $1,000 
 3d Printed Differential Challenge   $1,000 
 3d Printed Transmission Challenge   $1,000 

 WHAT S NEW 
 The Team 

 Join the revolution 
 Locations 

 Boulder, CO, USA 
 Burleson, TX, USA 
 Lynnwood, WA, USA 

 Donate 
 Calendar 

 Store 
 My Wikispeed 

 DISCOVER THE STORY    

 Our car is published under Creative Commons ShareAlike 4.0 International, What will you build? 

   RECENT BLOG POSTS   

 Scrum Hardware is the same as Scrum 
 03/16/2018 

 How to Fire a Scrum Team Member 
 02/21/2018 

 Article Series: Increase Hardware Development Productivity by 400%   Introduction 
 10/10/2017 

 Upcoming Events   

 RECENT COMMENTS Diane Adams  on  Scrum in Hardware Guide Chris Wallace  on  Scrum in Hardware Guide Alex  on  Scrum in Hardware Guide 


WHO WE ARE

We are all volunteers, from all over the world, completing WIKISPEED backlog items on nights and weekends because we think it is worth doing.  The gentleman who founded the materials science lab at MIT is on the team.  A lady who was a tech at Apple
Computer and worked on the first multimedia PC is on the team.  We have electrical and mechanical engineers who have done work for the U.S. Air Force, Lockheed Martin, NASA, and others.  The gentleman who oversaw and managed the largest military
research facility in the world is a member of Team WIKISPEED.  And more than a hundred interested and energetic housewives, househusbands, kids, environmentalists, automotive enthusiasts, artists, musicians, web developers, composite technicians,
accountants, lawyers, project managers, mechanics, fabricators, and more. We want to work with you, no matter where you are in the world and what you background and skills might be.
ETHICS & PRINCIPLES

    If I give money, time, cookies, or supplies to WIKISPEED they are a donation.
    If I give money, time, cookies, or supplies to WIKISPEED, I have no claim on WIKISPEED or its decisions or what it does with my resources after I give them.
    If I share my best thoughts and work with WIKISPEED, WIKISPEED can use them at no cost, and I retain rights to reuse or protect my ideas unless I opt to bequeath them to WIKISPEED or the Public Domain.

    We support businesses that demonstrate the following ethics: Equitable distribution of wealth.
    Avoid profit from waste (e.g., buying a competitor to shut them down).  Grow profit based on visible value.
    If we focus on profit over value to customers, we will obtain neither.  If we focus on visible customer value over profit, we will profit.
    Use less stuff (Lean) wherever responsible.
    Make decisions at the last responsible moment; this is when we know the most.  But be careful not to wait until it is no longer responsible.  We never make a decision before we have to just to “get it out of the way.”
    Start with the minimum useful solution (porters on roller-skates over a mail chute), then iterate aggressively to improve visible value and efficient sustainability of the solution.
    Morale is a multiplier for velocity.
    We trade estimated future states for knowing the most about our current situation and make changes quickly (Agility).
    Trust our team.  This avoids a culture of CYA, which slows down innovation and kills morale.  For example, a company that spends $6 million on intranet security preventing employees from seeing documents above their salary level so as to avoid
    leaking potentially libelous documents, could instead spend that same $6 million having interns and attorneys aggressively helping all levels of the organization identify documents that are potentially libelous, which are usually a sign
    of unethical action, coaching them on how to rectify those situations ASAP as a company with help from all levels of the company.
    Replace a document with a conversation.  Instead of writing a specification document, have a white-boarding conversation in a room where as many other team members as possible can overhear.  Take pictures of the white board when you are done,
    and email the team.  Documents have a maintenance cost.  Produce self-documenting work—a car with a navigation that teaches you how to use it instead of a manual.  Can you imagine if Internet Explorer still shipped with a manual?  We expect
    it to be self-explanatory.  Documents are a hidden expense, requiring editing and rewrite to match the current version.
    Try not to do something alone; pair with another team member whenever responsible.  This aids in knowledge transfer and avoids requirements for documentation.

A LITTLE ABOUT OUR FOUNDER

Joe JusticeJoe Justice is a consultant at Scrum, Inc., a TEDx speaker, and coach for agile hardware and manufacturing teams around the world. Scrum, Inc. is the principle thought leadership
and training body on Scrum and agile development. He is the founder of Team WIKISPEED, an all Scrum volunteer-based, “green” automotive prototyping company, with a goal to change the world for the better. Joe consults and coaches teams and
companies on implementing Scrum at all levels of their organization, both in software and physical manufacturing. Joe has been featured in Forbes 5 times, CNN Money once to date, interviewed by Fast Company, featured on the Discovery Channel,
and other media outlets globally for his work reducing time to value in companies globally and within the non-profit social good do-tank, Team WIKISPEED. Joe founded Team WIKISPEED in 2006, and with the distributed, collaborative, volunteer
team tied for 10th place in the Progressive Insurance Automotive X Prize and in the process formalized eXtreme Manufacturing, a process adapting the fastest moving methods of fast moving software startups to non-software development,
testing, and manufacture. As a result, he is lucky enough to serve on the board of advisors for groups from aerospace engineering to manufacturing to education. Joe has spoken and/or launched teams at UC Berkley, Cambridge, Google, Microsoft,
Boeing, Lockheed Martin, Raytheon, John Deere, and others; in Vietnam, India, China, Switzerland, Germany, France, Romania, the UK, Brazil, Canada, the United States, and others. Lucky for Joe, this is tremendously enjoyable and rewarding
work, resulting in faster time to value in industries from medical devices to construction. His story was featured in Forbes and CNN Money/Fortune. If you are interested in having Joe present, consult, coach, or train with your team, please visit
http://scruminc.com/services/, send an email to Justice@ScrumInc.com, or call (617) 225-4326. Let’s do AWESOME together!
ACKNOWLEDGEMENTS

WIKISPEED is redeveloping automotive control using the open-development Arduino and Netduino platforms.

Our team of talented volunteer hardware and software engineers is finding new and interesting ways to modularize the functions of controlling the WIKISPEED car. These will allow drop-in replacements for sub systems without having expensive
reengineering when the customer or market dictate change.

Our tablet-based dashboard will allow the customization of instrumentation to suite clients’ preferences without any actual changes to the hard structure of the car. The possibility of changing the car’s security systems from RFID to biometric
by simply changing the System Access module is an example of how this abstraction of systems allows for an almost infinite set of permutations of configuration.

As well as using open-sourced hardware platforms, the team is also using the open-development project of Fritzing labs for the design and fabrication of its PCB and the distribution of design ideas between the team members.

This ideal of collaborative working is part of the DNA of WIKISPEED, and all contributors are able to share code through our Sourceforge repositories where you will find all the Fritzing files and source for the developments we are undertaking.

If you feel you have a talent that you can offer or help in the development in some way, please contact the team.

—Peter Baines, Head of Hardware R&D

WIKISPEED extends great thanks for great products and work to Alibre Design CAD software, Ampstech FEA software, Skunk2 Racing suspension
products,Hondata automotive electronics, Carhartt fire-retardent work clothing, Rubbermaid Commercial
Products, Mohrcomposites composite prototyping, Advanced Autometric mechanics and fabrication (Hillsdale, MI, USA), South Seattle Community College,
Frizting circuit design, EmbeddedAT electronics rapid prototyping, and many, many more organizations and individuals.

WIKISPEED thanks Photography by Cecilio for beautiful photo shoots.


Press

Press Packet

All members of the press, blogoshpere, and everyone else, have permission to publish and distribute the assets below, worldwide in print and via electronic media.

You can even put these assets on a T-shirt and sell it, and keep the profit, if you really want to. We’d request that you send a portion of the profit to us via Paypal, but that is just a request.

Print and Electronic – to include sublicensing, without charge, of publication or transcription in Braille, large-type editions, or recordings for the blind and other special editions for use by the physically handicapped by approved non-profit organisations.

This includes release and permission to use the material for the life of the work (i.e. there should be no time restrictions on the reuse of material, e.g. one-year license granted).

The following photos are posted under the Creative Commons license for public, press, and commercial use.


Car

In 2010 we tied for 10th place in the Mainstream Class of the Progessive Insurance Automotive X Prize, achieving 109 mpg combined EPA city/highway in simulation of that test. We received $10,000 in road legalization testing and consulting as our consolation prize. With that money, we have developed the Roadster, which is avialable for sale in our online store as 8 separate modules.

Now we are working on:

A next generation mail delivery vehicle for postal services globally. We intend to update this design for use as a compact, ultra-efficient RV camping vehicle and a light commercial delivery vehicle.

A next generation taxi cab vehicle for global use.

An ultra-light racing vehicle to campaign at the world’s most famous race track, the Nuremburgring. We have a prototype there currently under development by LG WikiSpeed.De.

An ultra-lite 5 seat sedan as part of ARPA-E’s LiteCar Challenge.

Our current open source roadster is shown below:

Lockheed_Build_Party

3-14-14-2

IMG_6241

You may prototype your own WIKISPEED car with us from our Open Source plans for free, or purchase the modules you’d like from our Online Store.

Developed as a Minimum Viable Product and descended from our eco-challenge race car, the modules available for sale skip every amenity except a smartphone dock in the dash panel.  They do not have a lockable trunk, roof, or cup holders – yet.  Step over the side crush structures and fixed door panels then sit down inside like a pure race car for the ultimate in light-weight and simplicity.

    We have tested the car internally, while campaigning in the Progressive Insurance Automotive X Prize, and with Roush Industries where it scored 69.01 highway on it’s best run before we found the wheels were dragging on the dynamometer and the fan was not blowing on our cooling system, meaning we are confident we can score much higher.  We excitedly await our next test at an independent institution and official EPA rankings.  Thanks for considering supporting us by building one of our prototypes!

WikispeedThree2106eWe have built to NHTSA and IIHS specifications and prepare for official test and rankings.  The car has been impact tested at CAPE in Indiana, where the driver’s seat broke loose, allowing us to discover faulty MIG weld seams.  The seat mounts have been replaced by bolt-in plates, and we are iterating to replace all welded joins with bolt-in gusset plates.  The car has a driver’s air bag option but does not yet have an airbag sensor strategy and airbag computer.  We excitedly look forward to partnering with an airbag and related safety systems group or engineer to provide a complete and testable airbag system, and at the same time are prototyping our own Open Source airbag computer and firing system.

    0 to 60 MPH comes in 5 seconds with a Honda R18 engine from a 2006-2011 Honda Civic 1.8l.
    Current top speed of 149 MPH with a Honda R18 engine from a 2006-2011 Honda Civic 1.8l.
    The ultra-efficient gasoline engine is mid-mounted for optimum efficiency of power delivery and minimal energy loss when turning. There is room for 4 adults, just barely, or 2 adults with childseats or parcels in the back. And it’s super fun.wikispeed-car
    Prototypes are available as 8 types of modules, in our Online Store. The body is crafted in Maryland, the chassis built in Washington, the engine and transmission produced in Ohio, and the electronics systems built in Japan and France and the United Kingdom.
    Mechanicals serviceable by any shop equipped to service current generation Honda engines. Modules slide out, providing access to all sides of most components, for ease of self service if desired.

wikispeed-sgt01-exploded

    Snow and foul weather stable handling with the engine directly over the driven wheels. It’s similar in foul weather to a Honda Accord.

Financing Options We do not currently offer financing, but purchasing your vehicle one module at a time allows splitting the cost up as your cars pieces are assembled. For the most opportunity to iterate your vehicle, we highly recommended building one or more of the modules yourself from our open source plans, which are posted for free in the online Store.

Current bug report (Updated March 7th 2015):
Unstable at highway speed turning. FIXED in Suspension Module version 29b and above!
Wide turn radius at low speed. FIXED in Suspension Module version 29b and above!
No fuel gauge with current fuel system. FIXED In engine module 5 and above!
Overheats intermittently. FIX In testing! Prototype this with us!
No wipers in current iteration.
No parking brake in current iteration. FIX In testing! Prototype this with us!
Airbags not functional. IN DEVELOPMENT.
No rear seats and belts currently.
Pulse and Glide not automated.
No windshield defrost.
Last EPA test 69 mpg hwy, need to run new test.
Roadster aero package needs a aerodynamic roof panel and a boat-tail.

Roadster_World_Debut_1000px
WHAT IF WE ALL SPENT TWO HOURS A WEEK DESIGNING THE VEHICLES OF THE FUTURE? At WIKISPEED we are driven to do awesome things to change the world for the better.  Our cars are super-efficient which is exactly how we run our business, and we genuinely care about making things better every iteration.  We are developing convertible bodies as well as a pickup truck, which can be swapped out in about the time it takes to change a tire.  Amazing things like this are possible because:

    Our car is modular—meaning we can swap out parts easily.
    Improvements to any portion of the car are incorporated the next one week sprint by people just like you pairing with experts globally, unlike multi-year design up-front cycles we see in most manufacturing companies.
    Our Agile, distributed international team works on simply creating the most awesome car possible. Awesome in terms of environmental impact, fun to drive, outright speed, ergonomics, capacity, utility, price, safety, maintainability, durability, and reliability.


Extreme Manufacturing

Extreme Manufacturing is a set of technical practices and management principles to go from an idea to a product or service in the customers hands in less than 7 days. This involves Agile design, Agile manufacturing, Scrum teams, Kanban flow optimizations, Radical Management, Lean and TPS, rapid prototyping tools and practices, Scrum vendor practices, and Extreme Programing technical practices.

To learn more, check out our Extreme Manufacturing webinar on ScrumInc.com, located here:
http://scrumlab.scruminc.com/articles.html/_/open/extreme-manufacturing-r93

Host an eXtreme Manufacturing build party presented by Scrum Inc.


MicroFactory

WIKISPEED Shop in a Box molding shop 

Basic Structure

    40’ x 9’ x 8’ single use steel shipping container
    Hardwood deck
    Full opening center catch doors

Interior Additions

    Full width full height storage unit fixed to the rear wall of unit for the containment of the fiber cloth rolls and the bonding materials
    Beginning 6’ inside the structure 1 foam board affixed to the sides and ceiling for 20’
    Roll-up curtains at each end of this insulated section / or pole stand curtains for each end of the insulated section
    Ceiling mounted RV type heating unit that uses either propane or diesel fuel
    Ceiling mounted electric generator, where lack of infrastructure dictates, for lighting
    LED light bars with angle reflectors on both top sides of the interior with extensions for the exterior work space
    Floor and wall tie downs for securing items as needed
    Electrical outlets as required for power tools and/or chargers
    Brackets on inside surface of the doors to mount 4’ x 8’ extension panels and structural support bars
    2” x 2” x28’ aluminum angle x 2

Additional Material Items

    Truck tarp to enclose exterior work space of 12’ x 20’
    10 – 4’ x 8’ OSB ¾ sheets with hinge attachments and eyelets for scaffold braces to construct
    side wall extensions
    4- X type scaffold braces to support exterior side walls
    10 – 12’ x 1” diameter conduit sections to support the tarp and define the width of the container extension
    Mandrel cart with 6 dia. full rotation wheels to carry the body mold
    Body mold
    Vacuum table fixed in the rear portion of the container for the molding of light enclosures, mirrors, windshield and roof panels
    Cable pull block and tackle for moving the cart and lay-up mold in and out of the container
    “T” handle extension to control the cart when moving it in or out of the container
    Portable lights and stand for the extension room area
    Work bench for hand work

Delivery System

    Secure the tools and layup materials in the rear storage cabinet
    Place the 10 sheets of OSB banded centered and tied down to the floor in front of the cabinet
    Place the 10 sheets of foam board directly in front of the OSB, banded and anchored to the floor
    Place the mold on top of the sheet material and secure
    Anchor the mandrel cart to the side wall
    Lay the bundled conduit and X braces next to the sheet material on the floor and block in place
    Secure the work bench and the vacuum tables to the floor and side wall of the container
    Pre-mount generator, furnace, electrical outlets, track guides to floor, and LED lighting system
    Upon delivery the materials will be checked for condition
    Remove the external items and set up the extension room area and brace in place
    Cover with truck tarp
    Extend the LED lighting into the work space
    Affix the blue foam to the interior side walls of the container with construction glue and self tapping stainless fasteners, top, center and bottom of each panel. Three fasteners 1” in from each edge and in the center divide of each panel. A total of 9 fasteners per panel will require 90 fasteners minimum for the area to be covered @20’ per side wall
    Joints between panels should be taped with high tack thin packing tape

Manufacturing Process

    Mount the mold to the mandrel cart
    Affix the cable pull system to the cart and the “T” handle
    Check the motion of the cart into the container (should move freely and miss sides of container)
    Check heating, electric and lighting
    Place cart in the work space and apply the release agent to the mold surface
    Begin cutting and laying up the layers of fiber material
    Actual step by step process will be documented by Joe Justice and Rob Mohrbacher
    Apply the rosin catalyst
    Smooth as needed
    Begin heating the cure area with the curtains in place to 70 degrees F
    Close the mold
    Open the door end curtains of the cure area
    Move the cart and body into the cure area
    Secure the curtain and cure for ??? hours
    After cure cycle move cart to work area
    Remove the external mold features
    Begin the hand finishing of the body (Joe and Rob to provide details)
    Set up the vacuum table and form the light pockets and lenses plus the mirror housings
    Set up table to form the wind screen
    Prior to removing the body from the mold mandrel clean the entire work area
    Inventory material use to determine what is needed for the next body
    Place order for additional materials
    Remove body from mold and secure on chassis

Rinse and repeat

The sketch detail will be sent in a photo format showing the placement of angle, room extension construction and cover, the placement of the insulation panels

This is a proposed concept not to be considered a volume production facility.

We are making a bit of a difference on strategic social good initiatives every week.  And now, with a goal towards helping to end involuntary homelessness, we have a prototype micro-house to provide a clean bedroom, clean bathroom, and lockable front door for under $100 USD.

Currently it functions as a hyper-insulated tent, allowing one adult human’s body heat to warm the interior 20 degrees F over the ambient outdoor temperature. The goal is to add open source, ultra-low cost, mass-production possible amenities until it is possible for a person to spend a night or a lifetime in the MicroHouse, and emerge groomed and fresh as if they had just exited a boutique hotel, with a charged cell phone and ready to meet their world.

This isn’t a rammed earth package, as we’re loosely coupling them to the earth in order to move and swap them around. It’s also not made of natural materials that may or may not be available in mass quantity without damaging the landscape. It’s instead made out of waste material from our automotive manufacturing process, which means insulation foam that we use to make our molds, epoxy that we use to make our carbon fiber parts, strips of leftover carbon fiber, aluminum, and polycarbonate. It currently fits in one parking space, in the bed of a truck, or on a 4’x8′ trailer which any compact car is able to move around. If you would like to sleep in a prototype or 

 Wikispeed is an automotive manufacturer that produces modular cars. Wikispeed innovates by applying scrum development techniques borrowed from the software world. They use open source tools and lean management methods to improve their productivity 

Wikispeed is an automotive manufacturer that produces modular cars. Wikispeed innovates by applying scrum development techniques borrowed from the software world. They use open source tools and lean management methods to improve their productivity
 WHOLE PROJECT MARK
