PROJECT NAME: Elgg
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

Elgg.org
===============================

Elgg.org

 Log in Username or email ast Password ast 

		Remember me	 
 Register Lost password 

 About Core Team Documentation Domain Policy Elgg Foundation Features Hosting License Services Supporters Blog Community Activity Discussions Register Download Groups Beginning Developers Feedback   Planning General Discussions Performance   Scalability Plugin Development Professional Services Technical Support Theme Development Plugins Showcase 

 Introducing a powerful open source social networking engine 
 Providing you with the core components needed to build a socially aware web application 

 Get Elgg 2.3.5 Learn More Open Source   

				Elgg is an award-winning open source social networking engine that provides a robust framework on which to build all kinds of social environments, from a campus wide social network for your university, school or college or an internal collaborative platform for your organization through to a brand-building communications tool for your company and its clients.

 alphabetboxbusiness   coindesert        file       github-logographic-design                   keyboard                     ladybug                       light-bulb             network   newborn         puzzleseo                  technology      workspace                                

 Getting Started 

 Install   

							Learn about requirements and steps needed to install Elgg on your server.

 Showcase   

							Get inspiration from other projects powered by Elgg.

 Plugins   Themes   

							Find the ideal plugins and themes in our repository of community sourced projects.

 Hello, World!   

							Our tutorials will guide you through the first steps of building an Elgg plugin.

 Beginning Developers Group   

							Join our community group dedicated to helping get started with Elgg customizations and plugin development.

 Developer Center 

 Documentation   

							Find detailed information about Elgg's architecture, approach and features.

 API Reference   

							This is a handy resource to search and find out what functions exist within Elgg.

 Source Code   

							Elgg is an open source framework hosted and developed on Github.

 Bug Tracker   

							This is Elgg's bug tracker, if you find bugs or have patches for existing bugs then head on over and get involved.

 Feedback   Planning   

							Discussions about the past, present, and future of Elgg.

 Professional Services 

 Featured Providers   

							Hire trusted Elgg developers who contribute to core development and Elgg's ecosystem by participating in our supporters scheme.

 Hosting   

							Find providers who tailor their services to host Elgg projects.

 Professional Services Group   

							Find community users who provide professional Elgg services.

 Giving Back 

 Supporters Scheme   

							Are you an individual or organization who uses and likes Elgg? Consider becoming an official supporter of the Elgg project.

 Contribute   

							Want to improve Elgg? Check out our contributor's guide.

 Join Our Community   

							Join our community and gain first-hand experience using Elgg.

 Navigation Home Community Blog Hosting Services Learn Download Elgg   

 Contact 
 info@elgg.org 
 Security issues should be reported to security@elgg.org! 

 Legal 
 2014 the Elgg Foundation 
 Elgg is a registered trademark of Thematic Networks. 
 Cover image by  Ra l Utrera  is used under Creative Commons license. 
 Icons by  Flaticon  and  FontAwesome . 

 About Terms Privacy Domain policy 

Domain Policy : Elgg.org
===============================

Domain Policy : Elgg.org

 Log in Username or email ast Password ast 

		Remember me	 
 Register Lost password 

 About Core Team Documentation Domain Policy Elgg Foundation Features Hosting License Services Supporters Blog Community Activity Discussions Register Download Groups Beginning Developers Feedback   Planning General Discussions Performance   Scalability Plugin Development Professional Services Technical Support Theme Development Plugins Showcase 

 Domain Policy The domain policy establishes who can use Elgg in top level domain names and
	exists to identify the official Elgg sites, to protect Elgg users, and to protect Elgg
	as a project. 
 The only domains permitted to use "elgg" in the name are elgg.org, theelggfoundation.org,
	and elgg.com. These are the official sites and are operated by the Elgg Foundation or
	Thematic Networks. Any other sites using  elgg  in a top level domain is in violation
	of this policy and the Elgg trademark. 
 Special exceptions can be requested for sites dedicated to Elgg conferences. Elgg
	conferences, usually called  Elgg Camps,  serve to further Elgg as a project, are
	operated without generating revenue, and do not use Elgg in a commercial capacity. If
	you plan to organize an Elgg Camp and want to create a domain for it, please contact the
	Elgg Foundation at info@elgg.org to request a domain exception. You should not
	purchase a domain until you receive notification that your exception request has been
	approved. 
 The domain policy does not apply to subdomains. For example, you may create the
	subdomain  elgg.mysite.org  and still satisfy the requirements of this policy. 
 If your site is in violation of this policy, you should redirect to a subdomain or a
	new domain. We are happy to assist anyone who is planning to migrate their site from a
	violating domain, so please contact us if you need help. 
 Violators of this policy will not be permitted to have an account on
	elgg.org or to advertise services on elgg.org.   

 Documentation Domain Policy Features Elgg Foundation Hosting License Services Supporters Core Team 

 Elgg on Github 

 You can follow the latest work in progress or contribute via GitHub. 

 Supporter Scheme 

	Are you an individual or organization who uses and likes Elgg? Consider becoming an official supporter of the Elgg project.
	With only $50 per year for individuals or $150 per year for organizations, you can help the Elgg project. 
	 Learn more  

 Hosting 

 If you are looking for somewhere to host your Elgg powered network, we are putting together
			a  list of providers  who have added Elgg hosting to their services. 

 Navigation Home Community Blog Hosting Services Learn Download Elgg   

 Contact 
 info@elgg.org 
 Security issues should be reported to security@elgg.org! 

 Legal 
 2014 the Elgg Foundation 
 Elgg is a registered trademark of Thematic Networks. 
 Cover image by  Ra l Utrera  is used under Creative Commons license. 
 Icons by  Flaticon  and  FontAwesome . 

 About Terms Privacy Domain policy 

Services : Elgg.org
===============================

Services : Elgg.org

 Log in Username or email ast Password ast 

		Remember me	 
 Register Lost password 

 About Core Team Documentation Domain Policy Elgg Foundation Features Hosting License Services Supporters Blog Community Activity Discussions Register Download Groups Beginning Developers Feedback   Planning General Discussions Performance   Scalability Plugin Development Professional Services Technical Support Theme Development Plugins Showcase 

 Services 

 Elgg Services 

   Thematic Networks  creates socially-enabled web platforms with enterprise tools that help businesses to engage with their communities and generate revenue from published digital content. Our solutions power all sorts of applications, ranging from Software-as-a-Service networks to customized enterprise social networks, corporate e-learning platforms and social marketplaces with App Stores.

						Thematic Networks is a founding member and proud supporter of the Elgg Foundation.

						Contact:  services@thematic.net 

 Listings 

 Keetup Development 
 We are professional Elgg developers specializing in plugins, themes and social networking advice. We also advise companies on Elgg implementations. 

 Keetup Development website  |
								 Keetup Development Elgg community profile 

 Arck Interactive 
 We are a Portland-based web development agency focused on developing custom social networks, intranets and other applications using Elgg. 

 Arck Interactive website  |
								 Arck Interactive Elgg community profile 

				The service listing feature is currently on hold while we establish the Elgg Foundation. Watch this space and the  Elgg Blog  for more information!

 Documentation Domain Policy Features Elgg Foundation Hosting License Services Supporters Core Team 

 Elgg on Github 

 You can follow the latest work in progress or contribute via GitHub. 

 Supporter Scheme 

	Are you an individual or organization who uses and likes Elgg? Consider becoming an official supporter of the Elgg project.
	With only $50 per year for individuals or $150 per year for organizations, you can help the Elgg project. 
	 Learn more  

 Hosting 

 If you are looking for somewhere to host your Elgg powered network, we are putting together
			a  list of providers  who have added Elgg hosting to their services. 

 Navigation Home Community Blog Hosting Services Learn Download Elgg   

 Contact 
 info@elgg.org 
 Security issues should be reported to security@elgg.org! 

 Legal 
 2014 the Elgg Foundation 
 Elgg is a registered trademark of Thematic Networks. 
 Cover image by  Ra l Utrera  is used under Creative Commons license. 
 Icons by  Flaticon  and  FontAwesome . 

 About Terms Privacy Domain policy 

Download : Elgg.org
===============================

Download : Elgg.org

 Log in Username or email ast Password ast 

		Remember me	 
 Register Lost password 

 About Core Team Documentation Domain Policy Elgg Foundation Features Hosting License Services Supporters Blog Community Activity Discussions Register Download Groups Beginning Developers Feedback   Planning General Discussions Performance   Scalability Plugin Development Professional Services Technical Support Theme Development Plugins Showcase 

 Download 
	The simplest way to install and maintain your Elgg project is via Composer.
	Please check our  Installation  docs for instructions.

 License 

 Elgg is available under GPL Version 2, with a portion of the code alternately available under MIT.
			 More info . 
 You may
			 request an MIT release  (please specify version) or create one easily: After executing
			 composer install , delete the contents of the  /mod  directory. 

 Development Release - March 8, 2018 Elgg 3.0.0-beta.3 is available for early testers.  Do not use this version in production. 
		Please report all bugs to  GitHub Download 3.0.0-beta.3 Stable Release - December 6, 2017 Elgg 2.3.5 ( changelog )
	is the latest and recommended version of Elgg. Download 2.3.5 LTS Release - September 21, 2017 Elgg 1.12.17 ( changelog ) is the recommended release if using Elgg 1.12 Download 1.12.17 

 Previous releases 

 2.3.4  ( zip ,  source ) - released September 21, 2017 2.3.3  ( zip ,  source ) - released May 16, 2017 2.3.2  ( zip ,  source ) - released March 16, 2017 2.3.1  ( zip ,  source ) - released February 14, 2017 2.3.0  ( zip ,  source ) - released December 27, 2016 1.12.16  ( zip ,  source ) - released May 10, 2017 1.12.15  ( zip ,  source ) - released January 26, 2017 1.12.14  ( zip ,  source ) - released November 8, 2016 1.12.13  ( zip ,  source ) - released October 2, 2016 1.12.12  ( zip ,  source ) - released August 5, 2016 1.12.11  ( zip ,  source ) - released June 13, 2016 1.12.10  ( zip ,  source ) - released May 29, 2016 1.12.9  ( zip ,  source ) - released March 6, 2016 1.12.8  ( zip ,  source ) - released January 31, 2016 1.12.7  ( zip ,  source ) - released January 3, 2016 1.12.6  ( zip ,  source ) - released December 14, 2015 1.12.5  ( zip ,  source ) - released November 29, 2015 1.12.4  ( zip ,  source ) - released September 20, 2015 1.12.3  ( zip ,  source ) - released September 6, 2015 1.12.2  ( zip ,  source ) - released August 23, 2015 1.12.1  ( zip ,  source ) - released August 5, 2015 1.12.0  ( zip ,  source ) - released July 7, 2015   

 Security releases 

 Elgg 2.2, 2.1, 2.0, 1.11, and 1.10 are receiving only security updates. 

 2.2.4  ( zip ,  source ) - released January 27, 2017 2.2.3  ( zip ,  source ) - released November 8, 2016 2.2.2  ( zip ,  source ) - released October 2, 2016 2.2.1  ( zip ,  source ) - released September 21, 2016 2.2.0  ( zip ,  source ) - released August 5, 2016 2.2.0-rc.1  ( zip ,  source ) - released June 16, 2016 2.1.3  ( zip ,  source ) - released August 5, 2016 2.1.2  ( zip ,  source ) - released June 13, 2016 2.1.1  ( zip ,  source ) - released March 20, 2016 2.1.0  ( zip ,  source ) - released March 13, 2016 2.0.4  ( zip ,  source ) - released June 13, 2016 2.0.3  ( zip ,  source ) - released March 6, 2016 2.0.2  ( zip ,  source ) - released February 3, 2016 2.0.1  ( zip ,  source ) - released January 3, 2016 2.0.0  ( zip ,  source ) - released December 14, 2015 2.0.0-rc.2  ( zip ,  source ) - released November 29, 2015 2.0.0-rc.1  ( zip ,  source ) - released November 8, 2015 2.0.0-beta.3  ( zip ,  source ) - released October 5, 2015 2.0.0-beta.2  ( zip ,  source ) - released September 24, 2015 2.0.0-beta.1  ( zip ,  source ) - released September 6, 2015 2.0.0-alpha.3  ( zip ,  source ) - released August 24, 2015 2.0.0-alpha.2  ( zip ,  source ) - released August 6, 2015 2.0.0-alpha.1  ( zip ,  source ) - released July 10, 2015 1.11.6  ( zip ,  source ) - released June 13, 2016 1.11.5  ( zip ,  source ) - released December 13, 2015 1.11.4  ( zip ,  source ) - released July 7, 2015 1.11.3  ( zip ,  source ) - released June 14, 2015 1.11.2  ( zip ,  source ) - released May 25, 2015 1.11.1  ( zip ,  source ) - released April 26, 2015 1.11.0  ( zip ,  source ) - released April 13, 2015 1.10.6  ( zip ,  source ) - released December 13, 2015 1.10.5  ( zip ,  source ) - released April 5, 2015 1.10.4  ( zip ,  source ) - released March 22, 2015 1.10.3  ( zip ,  source ) - released March 8, 2015 1.10.2  ( zip ,  source ) - released February 21, 2015 1.10.1  ( zip ,  source ) - released January 25, 2015 1.10.0  ( zip ,  source ) - released January 12, 2015   

 Unsupported releases 

 These versions of Elgg are no longer supported: 

 3.0.0-beta.3  ( zip ,  source ) - released March 8, 2018 3.0.0-beta.2  ( zip ,  source ) - released January 31, 2018 3.0.0-beta.1  ( zip ,  source ) - released January 29, 2018 2.3.0-rc.1  ( zip ,  source ) - released November 9, 2016 2.2.0-rc.1  ( zip ,  source ) - released June 16, 2016 2.0.0-rc.2  ( zip ,  source ) - released November 29, 2015 2.0.0-rc.1  ( zip ,  source ) - released November 8, 2015 2.0.0-beta.3  ( zip ,  source ) - released October 5, 2015 2.0.0-beta.2  ( zip ,  source ) - released September 24, 2015 2.0.0-beta.1  ( zip ,  source ) - released September 6, 2015 2.0.0-alpha.3  ( zip ,  source ) - released August 24, 2015 2.0.0-alpha.2  ( zip ,  source ) - released August 6, 2015 2.0.0-alpha.1  ( zip ,  source ) - released July 10, 2015 1.9.7  ( zip ,  source ) - released December 14, 2014 1.9.6  ( zip ,  source ) - released November 30, 2014 1.9.5  ( zip ,  source ) - released November 17, 2014 1.9.4  ( zip ,  source ) - released October 19, 2014 1.9.3  ( zip ,  source ) - released October 6, 2014 1.9.2  ( zip ,  source ) - released September 21, 2014 1.9.1  ( zip ,  source ) - released September 12, 2014 1.9.0  ( zip ,  source ) - released September 7, 2014 1.8.20  ( zip ,  source ) - released September 4, 2014 1.8.19  ( zip ,  source ) - released March 12, 2014 1.8.18  ( zip ,  source ) - released January 11, 2014 1.8.17  ( zip ,  source ) - released January 1, 2014 1.8.16  ( zip ,  source ) - released June 25, 2013 1.8.15  ( zip ,  source ) - released April 23, 2013 1.8.14  ( zip ,  source ) - released March 12, 2013 1.8.13  ( zip ,  source ) - released January 29, 2013 1.8.12  ( zip ,  source ) - released January 4, 2013 1.8.11  ( zip ,  source ) - released December 5, 2012 1.8.10  ( zip ,  source ) - released December 4, 2012 1.8.9  ( zip ,  source ) - released November 11, 2012 1.8.8  ( zip ,  source ) - released July 11, 2012 1.8.7  ( zip ,  source ) - released July 10, 2012 1.8.6  ( zip ,  source ) - released June 18, 2012 1.8.5  ( zip ,  source ) - released May 17, 2012 1.8.4  ( zip ,  source ) - released April 24, 2012 1.8.3  ( zip ,  source ) - released January 12, 2012 1.8.1  ( zip ,  source ) - released November 16, 2011 1.8.0.1  ( zip ,  source ) - released September 5, 2011 1.7.23  ( zip ,  source ) - released March 18, 2015 1.7.22  ( zip ,  source ) - released March 12, 2014 1.7.21  ( zip ,  source ) - released January 3, 2014 1.7.20  ( zip ,  source ) - released January 1, 2014 1.7.19  ( zip ,  source ) - released March 14, 2013 1.7.18  ( zip ,  source ) - released March 12, 2013 1.7.17  ( zip ,  source ) - released January 29, 2013 1.7.16  ( zip ,  source ) - released November 11, 2012 1.7.15  ( zip ,  source ) - released January 20, 2012 1.7.14  ( zip ,  source ) - released October 20, 2011 1.7.13  ( zip ,  source ) - released October 8, 2011 1.7.12  ( zip ,  source ) - released September 29, 2011 1.7.11  ( zip ,  source ) - released August 15, 2011 1.7.10  ( zip ,  source ) - released June 15, 2011 1.7.9  ( zip ,  source ) - released June 1, 2011 1.7.8  ( zip ,  source ) - released April 4, 2011 1.7.7  ( zip ,  source ) - released February 1, 2011 1.7.6  ( zip ,  source ) - released December 23, 2010 1.7.5  ( zip ,  source ) - released November 26, 2010 1.7.4  ( zip ,  source ) - released October 14, 2010 1.7.3  ( zip ,  source ) - released September 2, 2010 1.7.2  ( zip ,  source ) - released August 25, 2010 1.7.1  ( zip ,  source ) - released April 21, 2010 1.7.0  ( zip ,  source ) - released March 2010 1.6.4  ( zip ) - released November 26, 2010 1.6.3  ( zip ) - released September 2, 2010 1.6.2  ( zip ) - released April 2010 1.6.1  ( zip ) - released September 2009 1.6.0  ( zip ,  source ) - released August 2009 1.5.1  ( zip ) - released April 2010 1.5.0  ( zip ,  source ) - released March 2009 1.2  ( zip ) - released December 2008 1.0  ( zip ) - released August 2008   

 Documentation Domain Policy Features Elgg Foundation Hosting License Services Supporters Core Team 

 Elgg on Github 

 You can follow the latest work in progress or contribute via GitHub. 

 Supporter Scheme 

	Are you an individual or organization who uses and likes Elgg? Consider becoming an official supporter of the Elgg project.
	With only $50 per year for individuals or $150 per year for organizations, you can help the Elgg project. 
	 Learn more  

 Hosting 

 If you are looking for somewhere to host your Elgg powered network, we are putting together
			a  list of providers  who have added Elgg hosting to their services. 

 Navigation Home Community Blog Hosting Services Learn Download Elgg   

 Contact 
 info@elgg.org 
 Security issues should be reported to security@elgg.org! 

 Legal 
 2014 the Elgg Foundation 
 Elgg is a registered trademark of Thematic Networks. 
 Cover image by  Ra l Utrera  is used under Creative Commons license. 
 Icons by  Flaticon  and  FontAwesome . 

 About Terms Privacy Domain policy 

Hosting : Elgg.org
===============================

Hosting : Elgg.org

 Log in Username or email ast Password ast 

		Remember me	 
 Register Lost password 

 About Core Team Documentation Domain Policy Elgg Foundation Features Hosting License Services Supporters Blog Community Activity Discussions Register Download Groups Beginning Developers Feedback   Planning General Discussions Performance   Scalability Plugin Development Professional Services Technical Support Theme Development Plugins Showcase 

 Hosting 
	This page is a list of companies who specialise in Elgg hosting and is provided to help Elgg users looking for an appropriate hosting company. Neither Thematic Networks nor the Elgg project officially endorse the companies listed*, although they do help support Elgg.org through their related affiliate schemes.

 A2 Hoting 

 300% faster page loads than the competition on SwiftServer SSDs. Features a no-click Elgg install! 

 ArckCloud 

 High performance, single-click deploy, Elgg hosting from the expert Elgg development team at Arck Interactive. Individual and enterprise plans, CDN, support, development and code optimizations, All data storage is on our blazing fast CEPH SSD cluster and 10x. 

 Arvixe 

 For fast, secure and reliable Elgg Hosting, look no further than Arvixe for all your web hosting related needs. 

 Feedback / Info 
 We constantly monitor companies who host Elgg to help create and foster good hosting options for those wishing to run an Elgg powered site. If you encounter issues with one of the hosts listed here, please get in touch with us. If you are going to get in touch, please make sure you provide adequate detail regarding the issues you have had. 
 If you have a favorite host you think should be listed here, please send them a link to this page. 
 If you represent a hosting company which provides Elgg hosting and would like to be listed, please get in touch. 

 Documentation Domain Policy Features Elgg Foundation Hosting License Services Supporters Core Team 

 Elgg on Github 

 You can follow the latest work in progress or contribute via GitHub. 

 Supporter Scheme 

	Are you an individual or organization who uses and likes Elgg? Consider becoming an official supporter of the Elgg project.
	With only $50 per year for individuals or $150 per year for organizations, you can help the Elgg project. 
	 Learn more  

 Hosting 

 If you are looking for somewhere to host your Elgg powered network, we are putting together
			a  list of providers  who have added Elgg hosting to their services. 

 Navigation Home Community Blog Hosting Services Learn Download Elgg   

 Contact 
 info@elgg.org 
 Security issues should be reported to security@elgg.org! 

 Legal 
 2014 the Elgg Foundation 
 Elgg is a registered trademark of Thematic Networks. 
 Cover image by  Ra l Utrera  is used under Creative Commons license. 
 Icons by  Flaticon  and  FontAwesome . 

 About Terms Privacy Domain policy 

Elgg.org
===============================

Elgg.org

 Log in Username or email ast Password ast 

		Remember me	 
 Register Lost password 

 About Core Team Documentation Domain Policy Elgg Foundation Features Hosting License Services Supporters Blog Community Activity Discussions Register Download Groups Beginning Developers Feedback   Planning General Discussions Performance   Scalability Plugin Development Professional Services Technical Support Theme Development Plugins Showcase 

 Introducing a powerful open source social networking engine 
 Providing you with the core components needed to build a socially aware web application 

 Get Elgg 2.3.5 Learn More Open Source   

				Elgg is an award-winning open source social networking engine that provides a robust framework on which to build all kinds of social environments, from a campus wide social network for your university, school or college or an internal collaborative platform for your organization through to a brand-building communications tool for your company and its clients.

 alphabetboxbusiness   coindesert        file       github-logographic-design                   keyboard                     ladybug                       light-bulb             network   newborn         puzzleseo                  technology      workspace                                

 Getting Started 

 Install   

							Learn about requirements and steps needed to install Elgg on your server.

 Showcase   

							Get inspiration from other projects powered by Elgg.

 Plugins   Themes   

							Find the ideal plugins and themes in our repository of community sourced projects.

 Hello, World!   

							Our tutorials will guide you through the first steps of building an Elgg plugin.

 Beginning Developers Group   

							Join our community group dedicated to helping get started with Elgg customizations and plugin development.

 Developer Center 

 Documentation   

							Find detailed information about Elgg's architecture, approach and features.

 API Reference   

							This is a handy resource to search and find out what functions exist within Elgg.

 Source Code   

							Elgg is an open source framework hosted and developed on Github.

 Bug Tracker   

							This is Elgg's bug tracker, if you find bugs or have patches for existing bugs then head on over and get involved.

 Feedback   Planning   

							Discussions about the past, present, and future of Elgg.

 Professional Services 

 Featured Providers   

							Hire trusted Elgg developers who contribute to core development and Elgg's ecosystem by participating in our supporters scheme.

 Hosting   

							Find providers who tailor their services to host Elgg projects.

 Professional Services Group   

							Find community users who provide professional Elgg services.

 Giving Back 

 Supporters Scheme   

							Are you an individual or organization who uses and likes Elgg? Consider becoming an official supporter of the Elgg project.

 Contribute   

							Want to improve Elgg? Check out our contributor's guide.

 Join Our Community   

							Join our community and gain first-hand experience using Elgg.

 Navigation Home Community Blog Hosting Services Learn Download Elgg   

 Contact 
 info@elgg.org 
 Security issues should be reported to security@elgg.org! 

 Legal 
 2014 the Elgg Foundation 
 Elgg is a registered trademark of Thematic Networks. 
 Cover image by  Ra l Utrera  is used under Creative Commons license. 
 Icons by  Flaticon  and  FontAwesome . 

 About Terms Privacy Domain policy 

All Site Activity : Elgg.org
===============================

All Site Activity : Elgg.org

 Log in Username or email ast Password ast 

		Remember me	 
 Register Lost password 

 About Core Team Documentation Domain Policy Elgg Foundation Features Hosting License Services Supporters Blog Community Activity Discussions Register Download Groups Beginning Developers Feedback   Planning General Discussions Performance   Scalability Plugin Development Professional Services Technical Support Theme Development Plugins Showcase 

 All Site Activity Filter   
 Paulo Santos @paulohsantos 

 Paulo Santos  added a new discussion topic  Erro 404  in the group  General Discussion   35 minutes ago 
 My site is giving 404 error after I enter the username and password 

 personaldistaste @personaldistaste 

 personaldistaste  joined the group  Beginning Developers   an hour ago 

 RvR @rivervanrain 

 RvR  replied on the discussion topic  adding country field   an hour ago 
 https://github.com/hypeJunction/Elgg-countries 

Updated fork is https://github.com/nlybe/Elgg-countries that compatibility with Profile manager  view reply 

 iionly @iionly 

 iionly  replied on the discussion topic  adding country field   2 hours ago 
 There's an old plugin ("Country Selector" - https://elgg.org/plugins/461662) you could take as a starting point. It has been written for Elgg 1.8 so it's likely not fully working anymore on more recent versions of Elgg. But if...  view reply 

 iionly @iionly 

 iionly  replied on the discussion topic  Cannot Log in   2 hours ago 
 http://learn.elgg.org/en/stable/appendix/faqs.html#form-is-missing-token-or-ts-fields  view reply 

 jmperu @jmperu 

 jmperu  added a new discussion topic  adding country field  in the group  General Discussion   4 hours ago 

Hello, 

I would like to add a country field in the profile 

with when entering the list of countries for the selection 

cordially 

 iionly @iionly 
 iionly   2 hours ago There's an old plugin ("Country Selector" -  https://elgg.org/plugins/461662 ) you could take as a starting point. It has been written for Elgg 1.8 so it's likely not fully working anymore on more recent versions of Elgg. But if properly updated this plugin should be capable of providing a country profile field when used together with the Profile Manager plugin. 
 RvR @rivervanrain 
 RvR   an hour ago https://github.com/hypeJunction/Elgg-countries Updated fork is  https://github.com/nlybe/Elgg-countries  that compatibility with Profile manager 
 Muttsams @AMSSupportScott 

 Muttsams  added a new discussion topic  Cannot Log in  in the group  General Discussion   7 hours ago 
 When trying to log into a fresth Elgg 2.3.5 install, I get the error: 

Form is missing __token or __ts fields 

 iionly @iionly 
 iionly   2 hours ago http://learn.elgg.org/en/stable/appendix/faqs.html#form-is-missing-token-or-ts-fields 
 Wagner @Wagner 

 Wagner  commented on the plugin  Video YouTube style Script   8 hours ago 
 this is the way the settings. 

http://tinypic.com/view.php?pic=x2s3ld s=9 

http://en.tinypic.com/view.php?pic=28rfrmf s=9 

lookat the screenshots please. 

 Wagner @Wagner 

 Wagner  commented on the plugin  Video YouTube style Script   12 hours ago 
 I've made all the necessary settings,   
I also configured cron to work every minute 
but a sinal "?" Appears. in place of the video icon, and clicking to view the video returns the message 

 "Fatal error: Call to a member function getIconURL ()... 

 bmt Promotions @bmtpromotions 

 bmt Promotions  has a new avatar   13 hours ago 

 Loa  p @loadep 

 Loa  p  has a new avatar   14 hours ago 

 H t b  ph t Vi t T n @hutbephotviet 

 H t b  ph t Vi t T n  has a new avatar   15 hours ago 

 Chip @Chipf 

 Chip  commented on the plugin  Video YouTube style Script   16 hours ago 
 yes and filled my email 

 hutbephothanoi.com.vn @hutbephothanoi.com.vn 

 hutbephothanoi.com.vn  has a new avatar   19 hours ago 

 Tom @Thomasondiba 

 Tom  commented on the plugin  Video YouTube style Script   21 hours ago 
 @ Chip , If you are seeing an Icon on Video page then it means that the Video has been uploaded to your server. Next, your Cron Jobs have to run in order to trigger Video conversion that has been uploaded. Did you set your Cron Jobs to run... 

 Tom @Thomasondiba 

 Tom  commented on the plugin  Video YouTube style Script   21 hours ago 
 @Jaqueline , You are right. The next update will include  your request so that once a user selects or clicks Videos menu links, they should be taken to a theme that is similar to YouTube. 

 Chip @Chipf 

 Chip  commented on the plugin  Video YouTube style Script   22 hours ago 
 yes 

 Jaqueline @Jaqueline 

 Jaqueline  commented on the plugin  Video YouTube style Script   22 hours ago 
 Hi Tom, thanks for all the answers, were very informative, just to finalize, and I no longer take your time, you have forecast to leave update and stay like you said it would look like the theme of YouTube when you click to watch the videos ? 

 Tom @Thomasondiba 

 Tom  commented on the plugin  Video YouTube style Script   22 hours ago 
 @Chip, Did yo set up the Cpanel Cron jobs as described in the .pdf file that comes with the plugin? 

 Chip @Chipf 

 Chip  commented on the plugin  Video YouTube style Script   23 hours ago 
 I can't get the videos to play just an icon. 
   Previous 1 2 3 4 5 ... 11262 Next   

 Navigation Home Community Blog Hosting Services Learn Download Elgg   

 Contact 
 info@elgg.org 
 Security issues should be reported to security@elgg.org! 

 Legal 
 2014 the Elgg Foundation 
 Elgg is a registered trademark of Thematic Networks. 
 Cover image by  Ra l Utrera  is used under Creative Commons license. 
 Icons by  Flaticon  and  FontAwesome . 

 About Terms Privacy Domain policy 

 Elgg is open source social networking software that provides individuals and organizations with the components needed to create an online social environment. It offers blogging, microblogging, file sharing, networking, groups and a number of other features. 

Elgg is open source social networking software that provides individuals and organizations with the components needed to create an online social environment. It offers blogging, microblogging, file sharing, networking, groups and a number of other features.
 WHOLE PROJECT MARK