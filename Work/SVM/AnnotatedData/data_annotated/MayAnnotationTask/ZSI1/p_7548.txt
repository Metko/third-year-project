PROJECT NAME: Neighborhood Academy
PROJECT SOURCE: MAZI Pilot Studies
---------------------------------------
NEW PAGE: 

Nachbar­schafts­akademie  – About us
===============================

Nachbar schafts akademie    About us

 German 
 English (United States) 

 Prinzessinnengarten 

 Nachbar schafts akademie 
 Offene Plattform f r Wissensaustausch, kulturelle Praxis und Aktivismus zwischen Stadt und Land 

 About us 
 Projekte 
 Blog 
 Manuals 
 Contact 

 The Neighborhood Academy in Prinzessinnengarten is a self-organized open platform for urban and rural knowledge sharing, cultural practice and activism. 

 The Neighborhood Academy started in the summer of 2015 with the program  City Country Land , and invited activists, artists, architects, researchers and representatives of initiatives from New York, Chicago, Madrid, Paris, Hamburg, Berlin and Brandenburg  to work on questions of urban and rural resilience, commons, land-politics and social housing. The invited guests, together with partners from Moritzplatz, Kreuzberg and Brandenburg, worked on local issues and exchange experiences and working tools through workshops, walks, interventions and with the help of manuals. Based on the first program we started to create an open and jointly build knowledge and experience platform. Right now we are complimenting the learning platform with a physical space through the construction of an experimental DIY-architecture:  Die Laube  (The Arbor). Within the European  MAZI-project  we develope a local DIY-network that will serve as a living archive for the academy. 
 In 2016 the programm of the Neighborhood Academy will focus on the topic 
 Collective Learning   Creating local and global neighbourhoods 
 We are engaged in 
 Local and global exchange between urban and rural communities 
Commons 
Right to the city 
Socio-ecological transformation from the bottom-up 
Self-organized learning 
 In cooperation with practitioners, artists, scholars, and different initiatives we are currently working on: 
 The Arbor    A self-build experimental architecture in Prinzessinneng rten, which will serve as an open space for learning and exchange 
 MAZI    building local, community wireless networks 
 ZUsammenKUNFT    a pilot project on living and working with refugees 
 Moritzplatz   Futures    a long-term neighbourhood oriented, social and ecological development of the site at Moritzplatz in Berlin-Kreuzberg 
 In 2016 we collaborate with 
 R-Urban (Paris, Colombes), Michelle Teran (Berlin, Madrid), Design for the Living World / Marjetica Potrc (HFBK Hamburg), Brett Bloom (Chicago), Paula Z. Segal (596 Acres, New York), hiddenecomonies (Aarhus), Design Research Lab (UdK, Berlin), Z/KU, campus cosmopolis, THF 100, Kotti   Co, Stadt von Unten, and Berliner Hefte. 
 The program of the Neighbourhood Academy is curated by 
 Robert Burghardt, Elizabeth Calderon-L ning, and Anna Heilgemeir (common grounds), Marco Clausen (Prinzessinneng rten), and Asa Sonjasdotter. 

 The Idea of the Neighborhood Academy 
 The Academy draws on processes that determine our every day life as well as the coexistence of plants, people and animals. It opens a space for the questions: How can we learn from each other in ways that relate to the world and engage in and with it? Can this be done through methods that are similar to the approach of a gardener   that are caring for and nurturing life processes? How can the work we do in our neighbourhoods also help us understand relations in larger and more complex contexts? How can we cooperate with initiatives in communities, other cities and rural areas in ways that influence these contexts? 
 This bottom-up academy combines different knowledge- and experienced-based formats: non-standardized knowledge, hands-on know-how, sensuous narratives and research methods. People, organizations and projects from different neighborhoods come together. Participants can come from Kreuzberg or Oderbruch just as likely as from Detroit or the rural regions of Greece to find common ground for learning and teaching. The Academy in Prinzessinnengarten supports and builds communities. Invited initiatives work cooperatively on shared issues and develop so-called Manuals   simple action guidelines   accessible and usable for the public and stored in the Archive of Commitment. 
 The Neighborhood Academy is a place free from admission. It does not give out certificates. It cares for experiences and knowledges made in self-organized urban and rural spaces, and it supports the growth of new perspectives and narratives. Its methods are as diverse as the people we wish to connect to: They range from community cooking, public talks and film screenings to workshops, artistic interventions and expert contributions. The Neighborhood Academy has not a set format. It is made through shared engagement. 
 The Neighborhood Academy is an open and continously growing co-operation. It is initiated by Marco Clausen ( Prinzessinnengarten) , Elizabeth Calder n L ning ( common grounds ) ,  sa Sonjasdotter  and  anstiftung . The implementation of the 2015 program was made possible through the commitment of all people involved and with the financial support of anstiftung.
 WHOLE PROJECT MARK