PROJECT NAME: European Network of Living Labs (ENoLL)
PROJECT SOURCE: TRANSIT social innovation networks
---------------------------------------
NEW PAGE: 

Open Living Labs | The First step towards a new Innovation System
===============================

Open Living Labs | The First step towards a new Innovation System

            	OpenLivingLabs Home

 news ENoLL Newsletters 
 In the news 
 Publications 
 Connect with ENoLL 

 about us What are Living Labs 
 ENoLL History 
 Services 
 How to join us 
 Vacancies 

 ENoLL expert groups 
 Strategic Partners 
 Projects 
 ENoLL People Council 
 Office 

 FAQ 

 events 
 network Effective Members 
 Innovation Partners 
 All Living Labs living labs map 
 search living labs 

   members 

 This section provides access to the ENoLL Intranet. If you are an ENoLL Member, please  login  or  sign up to gain access. 

 The European Network Of Living Labs
             - the first step towards a new Innovation System! 

 12th Wave Opening 

 ENoLL's contribution to advancing co-creative, human-centric, and user-driven research over the years has grown tremendously, and we are looking forward to expanding our member base in order to bring higher levels of co-operation and sharing of knowledge... 

 Read more 

 Call for external expertise 

 Cities around the world are undergoing significant transformations and are facing substantial challenges in the form of urban densification and extreme weather conditions, due to climate change and the ongoing urbanization. Nature-based... 

 Read more 

 Graphic Design Internship 

 We are looking for a Graphic Design Intern who would contribute to and develop visual identity of the association. ENoLL is a non-profit international association headquartered in Brussels, Belgium and representing 150+ Living Labs in Europe and... 

 Read more 

 Living Lab methodology handbook published 

 With the help of collaborative efforts of several individuals in ENoLL network, a new Living Lab Methodology Handbook has been published. This handbook, available on Scribd, was initiated under the coordination and support action  User Engagement for... 

 Read more 

 New release of end-user engagement toolkit 

 A toolkit has been created by ENoLL in the context of U4IoT project that supports the European IoT Large-Scale Pilots programme specifically in end-user engagement. It is part of recently updated U4IoT website with fresh design and better usability -... 

 Read more 

 OpenLivingLab Days: Call for Papers 

 The call for submissions to the 9th Open Living Lab Days in Geneva is now openThe deadline for submissions is May 15th, 2018 Theme of the Research Day: Living Labs and Transformative Research Contributing to Sustainable Development through... 

 Read more 

 Login  or  Sign up! 

 NEWS 

 OpenLivingLab Days: Call for Papers 

        Post date:

 SUBMITED BY enoll ON Mon, 2018-03-05 15:01 

 12th Wave Opening 

        Post date:

 SUBMITED BY enoll ON Mon, 2018-01-29 10:57 

 Graphic Design Internship 

        Post date:

 SUBMITED BY enoll ON Thu, 2018-01-04 11:44 

 Health Living Labs: HELIUM Staff Exchange programme 

        Post date:

 SUBMITED BY enoll ON Mon, 2018-03-05 15:25 

 Call for external expertise 

        Post date:

 SUBMITED BY enoll ON Fri, 2018-02-23 18:45 

 Read more news 

 JOIN THE ENOLL COMMUNITY THROUGH THE 12TH WAVE 

 SPOTLIGHT SLIDES 

        City Drivers     from  European Network of Living Labs (ENoLL)     

 SPOTLIGHT VIDEO 

 OpenLivingLab Days 2017  from  European Network of Living Labs  on  Vimeo . 

 TWITTER 

   Tweets by @openlivinglabs 

 LATEST PHOTOS 

 More photos from ENoLL   available here on Flickr 

 events 

 EVENTS 

 Open Science Conference 2018 

        Date:

 13/3/2018  -  14/3/2018 

        Location:

 Berlin 

 Golden Opportunities for the Silver Economy 

        Date:

 28/3/2018 

        Location:

 International Conference on Urban Planning and Regional Development in the Information Society 

        Date:

 4/4/2018  -  6/4/2018 

        Location:

 Vienna 

 Future oriented- technology analysis conference 

        Date:

 4/6/2018  -  5/6/2018 

        Location:

 Brussels 

 International Business Festival 

        Date:

 12/6/2018  -  28/6/2018 

        Location:

 Liverpool 

 SMART CITY EVENT 2018 

        Date:

 27/6/2018  -  28/6/2018 

        Location:

 The Hague 

 join our activities 

 If you want to follow our activities, and receive our newsletter, please  sign up here . 
 The European Network of Living Labs is strong, connected and growing community, with almost 400 historically recognised Living Labs and more than 170 active member today, 20% of which come from outside of Europe. 
 ENoLL regular hosts events and events of our members can also be found in our extensive  event callender  sharing upcoming conferences, festivals and workshops on topics rangind from smart cities to cultural herritage and education.  OpenLivingLab Days  is the annual summit of the worldwide Living Lab community hosted annually in August. The next edition happens between 29 August and 1 September 2017 and will take place in Krakow, Poland. 

 all our labs 

 Australia 
 Austria 
 Belgium 
 Brazil 
 Bulgaria 
 Canada 
 China 
 Colombia 
 Croatia 
 Cyprus 
 Czech Republic 
 Denmark 
 Egypt 
 Estonia 
 Finland 
 France 
 Germany 
 Greece 
 Hungary 
 Iceland 
 India 
 Ireland 
 Italy 
 Japan 
 Latvia 
 Lebanon 
 Luxembourg 
 Malta 
 Mauritius 
 Mexico 
 Mozambique 
 Netherlands 
 Nigeria 
 Norway 
 Paraguay 
 Peru 
 Poland 
 Portugal 
 Republic of Cameroon 
 Romania 
 Saudi Arabia 
 Senegal 
 Serbia 
 Slovenia 
 South Africa 
 Spain 
 Sweden 
 Switzerland 
 Taiwan 
 Trinidad and Tobago 
 Tunisia 
 Turkey 
 UK 
 USA 

 elsewhere 

 OpenLivingLab Days 
 Living Lab Knowledge Centre 
 Living Lab LinkedIn Group 
 Connected Smart Cities Network 
 Digital Agenda for Europe 
 FIWARE 
 Design for Europe
 WHOLE PROJECT MARK