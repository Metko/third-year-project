PROJECT NAME: PLUG (PLay Ubiquitous Games)
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

Home PLUG (PLUG.WebHome) - XWiki =============================== xml version = '1.0' encoding = '% SOUP-ENCODING% 'DOCTYPE html PUBLIC `` // W3C // DTD XHTML 1.0 Strict // EN' '`` http: //www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd' ' Home PLUG (PLUG.WebHome) - XWiki [if IE]> General Actions: IdentificationWiki: Plug: Index of documents Section: PLUG: Index of documents Page: WebHome Page Actions: Export: Export as PDFExport as RTFExport as HTMLPlus actions: PreviewView source codeHome PLUGComments (0) Annotations (0) Attachments (0) History InformationsPLUGPLay Ubiquitous Games and play morePartners: Academic: CNAM-CEDRIC, CNAM-Museum of Arts and Crafts, * Telecom-Management Institute Sud-Paris, Institut Telecom-Paris Tech, * L3i La Rochelle University, Industrial: Orange-France TelecomNet InnovationsTetraEdgeKeywords in relation to the ANR / CNC call: mobile video games mixtemiddleware for games videogameplayinteractivity Short description: PLUG is a project of industrial researchur24 months. He studies embedded and mobile technologies for the implementation of ambient / pervasive / ubiquitous games and their acceptability from a socio-cultural, economic and industrial point of view. The design and legame-designde pervasive games are prominent aspects of PLUG. Two types of games will be explored: one game per session in 2008, and a game in continuous mode, ie where players enter and leave during the game without the game stops. The CNAM museum sets a cultural and educational framework for the game. As such, the pervasive approach will make it possible to evaluate new playful / pedagogical / cultural interactions with the museum, its scientific and industrial content, and its objects. The development of wireless and embedded terminals offers radically new perspectives of reality increased for multiplayer games that build on the environment in which players evolve. For example: IPerG a European project, Human Pacman (NTU Mixed Reality Laboratory). We can also refer to Mogi and Botfighters: if their appearance is limited to geo-location on mobile phone, these games are against commercial projects in Japan and Sweden. PLUG plans to use all technologies that allow the construction of ubiquitous applications such that the player can be at the center of a technological device that immerses him in the game and its atmosphere, so integrated by legame-design, and without his knowledge. PLUG is based on the fact that the objects that surround a player are communicating with each other and with the player (Human to Human is implicit). The terminal available to the player to play a ubiquitous game today does not exist on the shelf, it would be similar to the `` satisfactor '' (The Age of Satisfactor, The Age of Pussyfoot (1969), The Science Mask Fiction). Two characteristics are essential to this equipment: the terminal is a personal object like a mobile phone, it accompanies the player in all places and at all times. In addition, he is in constant communication with the surrounding environment (building, street furniture, consumer goods, works of art or exhibits, services, other players). The environment acts on the player as the player acts on his environment, the interaction is complete and permanent. Today, a Smartphone or PDA terminal with a camera would be the minimal equipment that comes closest to our needs. However, it lacks functions especially for the player's communication and instrumentation parts. . The instrumentation will be implemented through bio-medical sensors (sweating, heartbeat) and a bio-feedback technique through an "affective computing" approach. These measures may be partially provided to the player, they will also be used to monitor the player and the evolution of his emotions. To our knowledge, this Human Machine Interface aspect offers a radically innovative dimension to the study of pervasive games. The use of biomedical sensors will also be integrated into the game-design. Wireless networks are critical to the success of the project. We will use all the technologies available to date: mobile telephony (GPRS / EDGE / UMTS / HSDPA), WIFI, BlueTooth, RFID. However, we will ensure that the game-design, immersion, the pleasure of playing are not erased by this combination of technologies, it is a guarantee of success. The project taking place over 2 years, a real experience under the form of a public event is produced at each occurrence of the Science Fair (intermediate experiences will be realized but not official) in 2008 and 2009. At each of these occurrences, a complete assessment of the project is made. The life cycle of PLUG corresponds to the spiral: the results obtained the previous year and demonstrated during the Science Fair serve to evolve a new cycle over the next year. Ambient / pervasive / ubiquitous games offer new perspectives. The acceptability study is a critical part of PLUG. The acceptability relates to socio-cultural and industrial plans.pl ug contact @ picoforge.int evry.fr (anti-spam: remove the first `` - '') Tags: Created byFrédéric Lemoinele 2008/11/19 14:27 Comments (0) Annotations (0 ) Pièces Jointes ( 0 ) HistoriqueInformationsvar hashviewer = self.document.location.hash.substring ( 1 ) ; var extraInit = function ( ) { XWiki.displayDocExtra ( `` Comments '' , `` commentsinline.vm '' , false ) } ; if ( hashviewer == `` Comments '' ) { var extraInit = function ( ) { XWiki.displayDocExtra ( `` Comments '' , `` commentsinline.vm '' , true ) } ; } if ( $ ( `` Commentslink '' ) ! = null ) { $ ( `` Commentslink '' ) .href= '' # Comments '' ; Event.observe ( $ ( `` Commentslink '' ) , `` click '' , function ( ) { XWiki.displayDocExtra ( `` Comments '' , `` commentsinline.vm '' , false ) ; } , false ) ; } if ( $ ( `` tmShowComments '' ) ! = null ) { $ ( `` tmShowComments '' ) .href= '' # Comments '' ; Event.observe ( $ ( `` tmShowComments '' ) , `` click '' , function ( ) { XWiki.displayDocExtra ( `` Comments '' , `` commentsinline.vm '' , true ) ; } , false ) ; } if ( $ ( `` commentsshortcut '' ) ! = null ) { $ ( `` commentsshortcut '' ) .down ( 'a ' ) .href= '' # comments '' ; Event.observe ( $ ( `` commentsshortcut '' ) , `` click '' , function ( ) { XWiki.displayDocExtra ( `` Comments '' , `` commentsinline.vm '' , true ) ; } , false ) ; } if ( hashviewer == `` Annotations '' ) { var extraInit = function ( ) { XWiki.displayDocExtra ( `` Annotations '' , `` annotationsinline.vm '' , true ) } ; } if ( $ ( `` Annotationslink '' ) ! = null ) { $ ( `` Annotationslink '' ) .href= '' # Annotations '' ; Event.observe ( $ ( `` Annotationslink '' ) , `` click '' , function ( ) { XWiki.displayDocExtra ( `` Annotations '' , `` annotationsinline.vm '' , false ) ; } , false ) ; } if ( $ ( `` tmShowAnnotations '' ) ! = null ) { $ ( `` tmShowAnnotations '' ) .href= '' # Annotations '' ; Event.observe ( $ ( `` tmShowAnnotations '' ) , `` click '' , function ( ) { XWiki.displayDocExtra ( `` Annotations '' , `` annotationsinline.vm '' , true ) ; } , false ) ; } if ( $ ( `` annotationsshortcut '' ) ! = null ) { $ ( `` annotationsshortcut '' ) .down ( 'a ' ) .href= '' # annotations '' ; Event.observe ( $ ( `` annotationsshortcut '' ) , `` click '' , function ( ) { XWiki.displayDocExtra ( `` Annotations '' , `` annotationsinline.vm '' , true ) ; } , false ) ; } if ( hashviewer == `` Attachments '' ) { var extraInit = function ( ) { XWiki.displayDocExtra ( `` Attachments '' , `` attachmentsinline.vm '' , true ) } ; } if ( $ ( `` Attachmentslink '' ) ! = null ) { $ ( `` Attachmentslink '' ) .href= '' # Attachments '' ; Event.observe ( $ ( `` Attachmentslink '' ) , `` click '' , function ( ) { XWiki.displayDocExtra ( `` Attachments '' , `` attachmentsinline.vm '' , false ) ; } , false ) ; } if ( $ ( `` tmShowAttachments '' ) ! = null ) { $ ( `` tmShowAttachments '' ) .href= '' # Attachments '' ; Event.observe ( $ ( `` tmShowAttachments '' ) , `` click '' , function ( ) { XWiki.displayDocExtra ( `` Attachments '' , `` attachmentsinline.vm '' , true ) ; } , false ) ; } if ( $ ( `` attachmentsshortcut '' ) ! = null ) { $ ( `` attachmentsshortcut '' ) .down ( 'a ' ) .href= '' # attachments '' ; Event.observe ( $ ( `` attachmentsshortcut '' ) , `` click '' , function ( ) { XWiki.displayDocExtra ( `` Attachments '' , `` attachmentsinline.vm '' , true ) ; } , false ) ; } if ( hashviewer == `` History '' ) { var extraInit = function ( ) { XWiki.displayDocExtra ( `` History '' , `` historyinline.vm '' , true ) } ; } if ( $ ( `` Historylink '' ) ! = null ) { $ ( `` Historylink '' ) .href= '' # History '' ; Event.observe ( $ ( `` Historylink '' ) , `` click '' , function ( ) { XWiki.displayDocExtra ( `` History '' , `` historyinline.vm '' , false ) ; } , false ) ; } if ( $ ( `` tmShowHistory '' ) ! = null ) { $ ( `` tmShowHistory '' ) .href= '' # History '' ; Event.observe ( $ ( `` tmShowHistory '' ) , `` click '' , function ( ) { XWiki.displayDocExtra ( `` History '' , `` historyinline.vm '' , true ) ; } , false ) ; } if ( $ ( `` historyshortcut '' ) ! = null ) { $ ( `` historyshortcut '' ) .down ( 'a ' ) .href= '' # history '' ; Event.observe ( $ ( `` historyshortcut '' ) , `` click '' , function ( ) { XWiki.displayDocExtra ( `` History '' , `` historyinline.vm '' , true ) ; } , false ) ; } if ( hashviewer == `` Information '' ) { var extraInit = function ( ) { XWiki.displayDocExtra ( `` Information '' , `` informationinline.vm '' , true ) } ; } if ( $ ( `` Informationlink '' ) ! = null ) { $ ( `` Informationlink '' ) .href= '' # Information '' ; Event.observe ( $ ( `` Informationlink '' ) , `` click '' , function ( ) { XWiki.displayDocExtra ( `` Information '' , `` informationinline.vm '' , false ) ; } , false ) ; } if ( $ ( `` tmShowInformation '' ) ! = null ) { $ ( `` tmShowInformation '' ) .href= '' # Information '' ; Event.observe ( $ ( `` tmShowInformation '' ) , `` click '' , function ( ) { XWiki.displayDocExtra ( `` Information '' , `` informationinline.vm '' , true ) ; } , false ) ; } if ( $ ( `` informationshortcut '' ) ! = null ) { $ ( `` informationshortcut '' ) .down ( 'a ' ) .href= '' # information '' ; Event.observe ( $ ( `` informationshortcut ''), `` click '', function () {XWiki.displayDocExtra (`` Information '', `` informationinline.vm '', true); }, false); } document.observe (`` dom: loaded '', extraInit, false); PLUGPLUG Appendix TechPLUG Contacts PartenairesPLUG Itération2008PLUG Itération2009T1 0Gestion ProjetT1 1CoordinationT1 2BilansT2 0DisséminationT2 1site WEBT2 2Scientifique T2 T3 3IndustrielleT3 0AcceptabilitéT3 1Economique 2Design MuséalT3 3Socio culturelT4 0Jeu PLUGT4 1Etat of artT4 2ConceptionT4 3DéploiementT5 0TechnologieT5 1RéseauxT5 2SupervisionT5 3MiddlewareT5 4Bio FeedbackT5 5Tracer JoueursT6 DémonstrationsT7 RessourcesU1 0Gestion ProjetU1 1CoordinationU1 2BilansU2 0DisséminationU2 1site WebU2 2Scientific U2 3IndustrialU3 0AcceptabilityU3 1Economic U3 2 Musical DesignU3 3Socio culturelU4 0Jeu PLUGU4 1Etat de l'ArtU4 2ConceptionU4 3DéploiementU5 0TechnologieU5 1RéseauxU5 2SupervisionU5 3MiddlewareU5 4Bio FeedbackU5 5Tracer JoueurWeb HomeWeb PreferencesThis wiki is licensed under aCreative Commons 2.0licenseXWiki Enterprise 3.0. $ { buildNumber } -Documentation



PLUG (PLay Ubiquitous Games)
In short

The PLUG project dealt with the way pervasive computing could change our experience of the museum. We designed for this project a pervasive game for the Musée des Arts et Métiers-Paris, « Plug- the secrets of the museum ».
Context

ANR PROJECT // 2007-2009 / Musée des Arts et Métiers / Cédric CNAM / Orange / Tetraedge / TELECOM Management SudParis / TELECOM ParisTech
By Annie Gentes, Camille Jutant, Aude Guyot, Michel Simatic.
Related papers

Gentes, A., Jutant, C., Guyot, A., Simatic, M. (2009). Designing mobility: pervasiveness as the enchanting tool of mobility. Workshop on Innovative Mobile User Interactivity (Mobicase ’09), San Diego, USA, October 26-29,

Plug trailer from Codesign Lab on Vimeo.
Pervasive computing in museum

The PLUG project dealt with the way pervasive computing could change our experience of the museum.
PLUG  the secrets of the museum, was a game which applied the RFID technology on the environment of a specific museum. Indeed, this game took place in Paris in the Musée des Arts et Métiers dedicated to the history of sciences. It aimed at delivering information about exhibited objects by playing.
Design of PLUG – The secrets of the Museum

We designed the different game interfaces: screens of the phones and stations with RFID tags.Each phone contained four cards representing items of the museum and was used to collect, exchange and also explore RFID tags dispersed throughout the museum. Each player could move the cards between a phone and a station or between two phones. Thus the gameplay proposed an original application of the RFID. The players had to develop different qualities and strategies to win. They had to be curious, to answer quiz, to collaborate with each other, to sort out and collect cards of the same family.
We used a narrative approach, personifying chosen items and reinforcing links between the place and the game and between the visitors-players and the objects.
We issued a manual explaining and illustrating the goal of the game and its rules.
more on: http://deptinfo.cnam.fr/xwiki/bin/view/PLUG/

    « Plug: Secrets of the Museum »: A Pervasive Game Taking Place in a Museum
    RFID Technology: Fostering Human Interactions
    Livrable ANR : Rapport d’expérimentation 2008


WELCOME TO CODESIGN LAB

Our team focuses on « extreme design », that is design that redefines its traditional limits and explores new territories and unconventional practices and collaborations in: science, society, politics, and art.
Find more on our team, or more on our projects.

Working session

Fundamental design
We build a semiotic model of design as a “meaning making” process that studies the rationale of design through a diversity of situations of conception to elucidate methods and concepts.

Applied design
We elaborate methods to build artefacts and services that test our hypotheses on design as a meaning making process articulating engineering, social sciences, and humanities. We therefore work closely with engineering scientists to develop new media: distributed mobile architectures, pervasive computing, connected objects, virtual intelligent agents,…

Social design
We study new design practices such as critical design, ludic design, speculative design and more generally all forms of exploratory design and generative practices in society.
Tricky questions

• Why is the design of infrastructures political?
• What are the poetical practices supporting design (naming, narrating, performing?)
• How does design produce the future?
• How could design artefacts articulate a debate?
• What is the relation between design and playfulness?
• Can design support specific emotions?
• How unconventional practices such as hacking, or trolls, redefine political participation?

Facts

The Codesignlab was founded in 2003 by Annie Gentès. It is part of the department of social and economic sciences of Telecom ParisTech and belongs to the Interdisciplinary Institute of Innovation (i3), http://www.i-3.fr/.

The Codesign Lab has been involved in 16 research projects spanning over distributed Networks, virtual intelligent agents, cultural heritage and crowdsourcing, pervasive computing and game design.
It works within Industrial contracts, public research contracts, European programs.
With a diversity of partners: museums and cultural institutions( Paris, Museum of Arts and Crafts, Museum of Decorative Arts, Réunion des Musées nationaux, Marseilles: Friche Belle de Mai), industrial partners (Alcatel Lucent, Thales, Orange, 3S Informatique, Renault, …), academic partners (CNAM, ENSCI, Mines ParisTech…)

About the word co-design
Electronic and computer scientists speak of co-design when they want to describe the way hardware anticipates software and software adapts to hardware, both evolving towards a better integration. Introduced by Isabelle Demeure and Annie Gentes, this concept of CoDesign research spans over the different layers of integrations including social and cultural facets of these new media.

CodesignLab-Women at the frontiere
Disciplines?

Our research is based on several disciplines: communication & information sciences, semiotics & design research, philosophy, sociology of science & technology and human computer interface. We want to analyze design practices and productions. In particular, we develop and test new man-machine interactive systems to better understand meaning making in design and engineering.
WHERE TO FIND US?
CONTACT

annie.gentes[at]telecom-paristech[dot]fr

 

 PLUG is a research project that studies embedded mobile technologies for the implementation of ubiquitous ambient games from a socio-cultural, economic and industrial point of view. 

PLUG is a research project that studies embedded mobile technologies for the implementation of ubiquitous ambient games from a socio-cultural, economic and industrial point of view.
 WHOLE PROJECT MARK
