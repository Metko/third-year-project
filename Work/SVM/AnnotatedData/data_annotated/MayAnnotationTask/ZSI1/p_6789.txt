PROJECT NAME: Mozilla Foundation
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 The Mozilla Foundation is a non-profit organization that exists to support and lead the open source Mozilla project. Founded in July 2003, the organization sets the policies that govern development, operates key infrastructure and controls Mozilla trademarks and copyrights. It owns a taxable subsidiary: the Mozilla Corporation, which employs many Mozilla developers and coordinates releases of the Mozilla Firefox web browser and Mozilla Thunderbird email client. The subsidiary is 100% owned by the parent, and therefore follows the same non-profit principles. The Mozilla Foundation was founded by the Netscape-affiliated Mozilla Organization, and is funded almost exclusively by Google Inc. The organization is currently based in the Silicon Valley city of Mountain View, California, USA.---------------------------------------
NEW PAGE: 


The Mozilla Foundation

The nonprofit Mozilla Foundation believes the Internet must always remain a global public resource that is open and accessible to all. Our work is guided by the Mozilla Manifesto.

The direct work of the Mozilla Foundation focuses on fueling the movement for an open Internet. We do this by connecting open Internet leaders with each other and by mobilizing grassroots activists around the world.

The Foundation is also the sole shareholder in the Mozilla Corporation, the maker of Firefox and other open source tools. Mozilla Corporation functions as a self-sustaining social enterprise – money earned through its products is reinvested into the organization.

Read the Mozilla Foundation’s 2016-2018 Strategy
Foundation Programs

Working with people and communities all across Mozilla, the Foundation team is focused on fueling the open Internet movement. Our programs are focused in three areas:
Shape the agenda

Mozilla has identified five key issues that are critical to build the open Internet we want:

    Privacy and Security
    Open Innovation
    Decentralization
    Web Literacy
    Digital Inclusion

Connect leaders

Our nascent Mozilla Leadership Network finds, connects and provides learning opportunities for a new generation of leaders who will ensure the next wave of access, inclusion and opportunity online.
Rally citizens

Mozilla works with allies to cultivate a global force of tens of millions of people prepared to support the rights of citizens of the web.
Products and Technology

Mozilla Corporation, a wholly owned subsidiary of the Foundation, makes consumer Internet products that advance the same values outlined in the Mozilla Manifesto.

Firefox, our flagship product, gives increased choice, privacy and security to 100s of millions of people around the world. Product exploration initiatives like our ‘connected devices’ program aim to bring more mainstream products with Mozilla values into the world. Technology initiatives like Rust and Mozilla’s online game centre aim to spread new thinking about open technology across the whole of the Internet industry.
Mozilla Community

The real power behind Mozilla is a global community of 10,000s of volunteers, allies and partners. Members of the Mozilla community participate by doing everything from contributing code to Firefox and teaching digital literacy to translating our software, organizing advocacy campaigns and writing the world's most referenced web developer site.
Get Involved

Join us and help protect the internet as a global public resource. To get started, join our community or make a donation.


The Mozilla Foundation is a non-profit organization that exists to support and lead the open source Mozilla project. Founded in July 2003, the organization sets the policies that govern development, operates key infrastructure and controls Mozilla trademarks and copyrights. It owns a taxable subsidiary: the Mozilla Corporation, which employs many Mozilla developers and coordinates releases of the Mozilla Firefox web browser and Mozilla Thunderbird email client. The subsidiary is 100% owned by the parent, and therefore follows the same non-profit principles. The Mozilla Foundation was founded by the Netscape-affiliated Mozilla Organization, and is funded almost exclusively by Google Inc. The organization is currently based in the Silicon Valley city of Mountain View, California, USA.
 WHOLE PROJECT MARK
