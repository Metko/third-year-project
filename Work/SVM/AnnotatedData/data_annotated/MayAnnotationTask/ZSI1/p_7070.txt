PROJECT NAME: NordicDEi
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 NordicDEi democratises data with a crowd powered collaborative-data engine where you don’t have to find a database, or worry about security, authentication, or privacy. 

We're building a launch-pad for 1,000’s of startups and new ventures, enabling empowered people and enlightened organisations to deliver measurable social impact and sustainable citizen prosperity.

To do this during times of unprecedented social, economic, environmental, demographic, and political uncertainty will require us to take a bold approach and step outside of the way we normally do things. 

What we need is a radical change in attitudes in the society of which we are a part. What we really need is a cultural revolution. 

We're suffering from a compassion and integrity deficit. And this matters a lot more to most of us than we dare to admit. 

NordicDEi democratises data with a crowd powered collaborative-data engine where you don’t have to find a database, or worry about security, authentication, or privacy. 

We're building a launch-pad for 1,000’s of startups and new ventures, enabling empowered people and enlightened organisations to deliver measurable social impact and sustainable citizen prosperity.

To do this during times of unprecedented social, economic, environmental, demographic, and political uncertainty will require us to take a bold approach and step outside of the way we normally do things. 

What we need is a radical change in attitudes in the society of which we are a part. What we really need is a cultural revolution. 

We're suffering from a compassion and integrity deficit. And this matters a lot more to most of us than we dare to admit.
 WHOLE PROJECT MARK