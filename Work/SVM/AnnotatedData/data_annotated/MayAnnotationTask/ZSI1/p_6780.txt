PROJECT NAME: OSP
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

Open Science Federation | to open science
===============================

Open Science Federation | to open science

 Make Contact  

 Navigation Menu 

 recent projects include: 
 About the Federation  
 All blogs citizen science education open access open data open source scicomm social media web hosting 

 Contact us . About us . Built and hosted by  the Open Science Federation  in the  Public Domain , thanks to the  WordPress  and open source communities. 
 We manage the @openscience community account on Twitter , the  Open Science Community on Google+  and our  Google+ page . 

 Skip to toolbar 

 Open Science Federation 
 Recent Projects   
 Make Contact   
 About the Federation     

 Log In 

 The OpenScience project is dedicated to writing and releasing free and Open Source scientific software. We are a group of scientists, mathematicians and engineers who want to encourage a collaborative environment in which science can be pursued by anyone who is inspired to discover something new about the natural world.

Much of the work of science depends on having appropriate tools available to analyze experimental data and to interract with theoretical models. Powerful computers are now cheap enough so that significant processing power is within reach of many people. The missing piece of the puzzle is software that lets the scientist choose between models and make sense of his or her observations. That is where the OpenScience project can help.---------------------------------------
NEW PAGE: 

Open Science Federation | to open science
===============================

Open Science Federation | to open science

 Make Contact  

 Navigation Menu 

 recent projects include: 
 About the Federation  
 All blogs citizen science education open access open data open source scicomm social media web hosting 

 Contact us . About us . Built and hosted by  the Open Science Federation  in the  Public Domain , thanks to the  WordPress  and open source communities. 
 We manage the @openscience community account on Twitter , the  Open Science Community on Google+  and our  Google+ page . 

 Skip to toolbar 

 Open Science Federation 
 Recent Projects   
 Make Contact   
 About the Federation     

 Log In 


About the Federation

The Open Science Federation is a nonprofit alliance working to improve the conduct and communication of science. We are scientists and citizen scientists, writers, journalists, and educators, and makers of and advocates for Open Data, Open Access, and Open Source and Standards.

Get to know us at @openscience on Twitter, or in Google+ and beyond with which we have connected the largest Open Science network in the world. We recently took up a count, deduplicated, and identified over 80,000 people and groups across our social network.

We do not intend to be at the centre of the Open Science community per se, though analyses often place us there, such as in NodeXL SNA Maps via @marc_smith, here and here. See also this analysis and some open data from the #openscience hashtag on Twitter, now a few years ago.

A network can be stronger than any one organization, and a federation of networks, stronger still. Thus we share access to our social media accounts with many individuals and organisations. Some of our account managers are listed on this page in Google+ and some of our colleagues at @openscience on Twitter and elsewhere are anonymous.

Several companies in publishing and one in healthcare have attempted to purchase our social media presence and our contacts’ data; we have declined and always will. Our efforts are by, for, and belong to the Open Science community.

The Open Science community includes many vital corporate members and our Federation has itself benefited from their generosity. We have for example accepted donations of hosting resources from Amazon, event venues from Mozilla, Microsoft Research, et al. Many have given their ingenuity to push Open Science forward such as Experiment.com, Academia.edu, and Science Exchange to name only a few.

As technically skilled volunteers and sometimes contractors, we take up technical projects under the Open Science Federation banner from time to time, especially in open source software for science, for science publishing, and for science communications.

Some of our favourite work has been to build a federation of publishing and social networks, initially for the SciFund Challenge and ScienceOnline in Seattle, the Bay Area, and in Vancouver, which has since grown. SciFund’s network of researcher blogs and sites, for example spawned another, the Open Notebook Science Network. Altogether, our federation is comprised of now more than one thousand sites, blogs, and networks serving researchers and labs, event series, workshops and working groups and so on.

These networks are federated in several regards, for example one’s username and password can be used in federated sites, and all sites share one copy of the open source software and services which power them. Some parts of our system are like a WordPress.com for science, only rather than being one network it’s a network of networks, and we do not only run WordPress. Our code is open source and available in the public repositories for WordPress, Drupal, diaspora*, et al.

We also run a separate federation for young adult researchers and bloggers, teachers, and mentors, called the Budding Scientists Federation which has included  Future Science Leaders in British Columbia, the Student Bio Expo in the American Pacific Northwest, and U20 Science online. For this junior federation alone, our volunteers have given in the low thousands of hours of pro bono software development and technical support, while also donating the cost of services such as web and email hosting.

Separately from all of the above, we maintain more highly secured, semi-private networks, used for example by biomedical researchers for online lab notebooks, intranets, event management, and so on. Contact us if you are interested in our services. If your work contributes to opening science, we want to help.

OpenScienceFederation.com is now a yearbook with a couple dozen projects. Some of us are considering ways to make our site more interesting, possibly as a global calendar of Open Science events, a directory of people and projects in Open Science, or a distributed, open source social layer for science.



 The OpenScience project is dedicated to writing and releasing free and Open Source scientific software. We are a group of scientists, mathematicians and engineers who want to encourage a collaborative environment in which science can be pursued by anyone who is inspired to discover something new about the natural world.

Much of the work of science depends on having appropriate tools available to analyze experimental data and to interract with theoretical models. Powerful computers are now cheap enough so that significant processing power is within reach of many people. The missing piece of the puzzle is software that lets the scientist choose between models and make sense of his or her observations. That is where the OpenScience project can help. 

The OpenScience project is dedicated to writing and releasing free and Open Source scientific software. We are a group of scientists, mathematicians and engineers who want to encourage a collaborative environment in which science can be pursued by anyone who is inspired to discover something new about the natural world.

Much of the work of science depends on having appropriate tools available to analyze experimental data and to interract with theoretical models. Powerful computers are now cheap enough so that significant processing power is within reach of many people. The missing piece of the puzzle is software that lets the scientist choose between models and make sense of his or her observations. That is where the OpenScience project can help.
 WHOLE PROJECT MARK
