PROJECT NAME: BIEN
PROJECT SOURCE: TRANSIT social innovation networks
---------------------------------------
NEW PAGE: 

BIEN | Basic Income Earth Network
===============================

BIEN | Basic Income Earth Network

 Menu 
 About BIEN 
 National   Regional Affiliates 
 Contact 
 Membership 
 NewsFlash 
 Press Room 
 Submit News 
 Add an Event 

 Facebook 
 Twitter 
 Youtube 
 Google Plus 
 RSS 

 Search for: 

 Menu 
 Home 
 About BIEN 

 Overview 
 Executive Committee 
 Annual Reports   Treasury Reports 
 BIEN Charter 
 General Assembly Meeting 
 A Short History of BIEN 
 Press Contacts 
 About Basic Income News 
 National   Regional Affiliates 
 Member Biographies 
 Contact 

 About Basic Income 

 What is Basic Income? 
 History of Basic Income 
 Frequently Asked Questions 

 Research 

 Congress Papers 
 Research depository 
 Basic Income Studies 
 The Navigator  Academic Blog 

 Basic Income News 

 Homepage 
 News   Events 

 News 
 Events 
 Obituaries 
 From the Web 

 Academic Articles 
 Popular Media Articles 
 Book Announcements 
 Audio, Podcasts   Radio 
 Video   TV 

 Opinion 

 Op-Ed 
 Interviews 
 Reviews 
 Blogs 

 The Independentarian 
 The USBIG Blog, edited by Michael A. Lewis 

 Original Podcasts 

 The UBI Podcast 

 Press Room 
 Suggest a News Item 
 Subscribe to NewsFlash Emails 
 Pitch an Opinion Article 
 Join our Volunteer Team 
 About Basic Income News 

 BIEN Congresses 

 Congresses Overview 
 Host a Congress 

 Get Involved 

 Donate 
 Become a Member 
 Volunteer for BIEN 
 Volunteer for Basic Income News 
 Join Our Affiliates 

 Donate 

 Alaska s Permanent Fund Dividend has no overall effect on employment 
 Alaska s provision of regular, unconditional income to its inhabitants has had no overall effect on 

 Read more  

 EUROPE: Council of Europe adopts resolution on basic income 
 The Council of Europe Assembly voted on 23 January 2018 in favour of  a Resolution 

 Read more  

 CANADA: Quebec is implementing a means-tested benefit, not a basic income 
 The province of Quebec, in Canada, has been fostering conversations around basic income and even, 

 Read more  

 EUROPE: European Social Survey (ESS) reveal findings about attitudes toward Universal Basic Income across Europe 
 Map of Europe. Credit to: Flickr   The European Social Survey (ESS) has published its 

 Read more  

 European survey reveals significant public support for Basic Income 
 50.8% in UK support implementation of BI, 49.2% oppose 

 Read more  

 SCOTLAND: Scottish Government provides  250k to support feasibility work on BI pilots 
 Angela Constance, Cabinet Secretary for Communities, Social Security and Equalities of the Scottish government announced 

 Read more  

 US/KENYA: GiveDirectly Officially Launches UBI experiment 
 The US charity GiveDirectly has officially launched its trial of basic income in rural Kenya, and is now enrolling experimental participants. 

 Read more  

 UNITED STATES: Y Combinator releases proposal for expanded study of basic income 
 Silicon Valley s Y Combinator has concluded its feasibility study in Oakland and released a draft proposal for a large-scale randomized control trial of basic income in the United States. 

 Read more  

 Recent News about Basic Income 

 International: Being paid for data 

 March 16, 2018   

 Leave a comment   

 Credit to: The Blue Diamond Gallery. Who does data belong to? Would it be possible for all of us to receive monetary compensation for what 

 RSA suggests stepping stone to UBI 

 March 15, 2018   

 Leave a comment   

 Europe: New paper by Institute of Labour Economics contributes to literature on the effects of introducing a UBI into current social security systems 

 March 5, 2018   

 Leave a comment   

 Indian government advisor suggests some areas of the country may soon introduce UBI 

 March 4, 2018   

 Leave a comment   

 More News and Opinions 

 KEEPING THE GLOBAL RECESSION IN PERSPECTIVE (from 2009) 

 March 14, 2018   

 Leave a comment   

 HEALING BUT NOT A CURE (from 2009) 

 March 7, 2018   

 Leave a comment   

 OPINION: Isn t true love unconditional? : The International Women s Day, the UK government s Valentine message, and the 50th anniversary of working class women demanding UBI. 

 March 7, 2018   

 Leave a comment   

 Rural basic income  maximizes impact  for society 

 March 1, 2018   

 One comment   

 New book: Beyond the Income for Inclusion, for a guaranteed Income as a Basic Right. 

 March 18, 2018   

 Leave a comment   

 New Book: Basic income, the whole world talks about it. Experiences, proposals and experiments. 

 March 17, 2018   

 2 comments   

 RSA suggests stepping stone to UBI 

 March 15, 2018   

 Leave a comment   

 New Book: Daniel Ravent s  and Julie Wark s  Against Charity 

 March 10, 2018   

 Leave a comment   

 BIEN's purpose 

 The mission of the Basic Income Earth Network (BIEN) is to offer education to the wider public about alternative arguments about, proposals for, and problems concerning, basic income as idea, institution, and public policy practice. To this end, BIEN organises public conferences around the world on an annual basis in which empirical research and new ideas are disseminated and discussed. BIEN promotes and serves as a repository of published research, including congress papers, an academic blog featuring balanced debate for and against the basic income proposal in different contexts and forms, and by means of an independent academic journal linked with BIEN   Basic Income Studies. BIEN does not subscribe to any particular version of basic income, and fosters evidenced-based research, plural debate, and critical engagement about basic income and related ideas and public policy developments. Individuals connected with BIEN   including affiliated organisations - may express particular opinions about basic income, but they are not opinions of BIEN. BIEN s explicit mission is to remain neutral among competing arguments for and against basic income and the relation of basic income with other ideas and policies. 

 About BIEN   The Basic Income European Network (BIEN) was founded in 1986 to serve as a link between individuals and groups committed to or interested in basic income, and to foster informed discussion on the topic throughout Europe. It extended its scope from Europe to the "Earth" in 2004. 

 Read More  

 What is Basic Income?   A basic income is a periodic cash payment unconditionally delivered to all on an individual basis, without means test or work requirement.  Read More  

 Our National   Regional Affiliates   See all affiliate organisations 

 Basic Income Videos   
 Sign up for the NewsFlash 
 Basic Income News' digest right to your mailbox 

	Sign up now   

 About BIEN   Founded in 1986, BIEN serves as a link between individuals and groups interested in basic income and fosters informed discussion on it. 

 More about us 
 About BIEN 
 National   Regional Affiliates 
 Contact 
 Membership 
 NewsFlash 
 Press Room 
 Submit News 
 Add an Event 

   Recent news 

 International: Being paid for data 

 RSA suggests stepping stone to UBI 

 Europe: New paper by Institute of Labour Economics contributes to literature on the effects of introducing a UBI into current social security systems 

 Indian government advisor suggests some areas of the country may soon introduce UBI 

 USA: Forbes 30 Under 30 Names Stockton Mayor Pioneering UBI in California 

 News by Topic Alaska 
 Australia 
 Austria 
 automation 
 Basic income 
 Belgium 
 BIEN 
 Brazil 
 Canada 
 China 
 Denmark 
 Europe 
 European Union 
 Feminism 
 Finland 
 France 
 Germany 
 Green Party 
 Guy Standing 
 India 
 International 
 Ireland 
 Italy 
 Japan 
 Kenya 
 Korea 
 libertarian 
 Namibia 
 New Zealand 
 Norway 
 ontario 
 Pilot projects 
 Portugal 
 Scotland 
 South Africa 
 South Korea 
 spain 
 Switzerland 
 The Netherlands 
 Uganda 
 United Kingdom 
 United States 
 Van Parijs 
 Video 
 Welfare 
 Archives   Archives 

 Stay in touch with us   
 Facebook 
 Twitter 
 Youtube 
 Google Plus 
 RSS 

 Follow us on facebook   

 Menu 

Except where otherwise noted, content on this site is licensed under the  Creative Commons license CC BY NC SA .
 WHOLE PROJECT MARK