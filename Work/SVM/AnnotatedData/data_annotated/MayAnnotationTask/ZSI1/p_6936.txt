PROJECT NAME: DiyDrones
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 Community for amateur Unmanned Aerial Vehicles. This community created the Arduino-based ArduPilot, the world's first universal autopilot (planes, multicopters of all sorts and ground rovers). The Pixhawk and APM 2.6 autopilots run a variety of powerful free and open UAV software systems, including: ArduCopter, a fully-autonomous multicopter and heli UAV system; ArduPlane, a pro-level UAV system for planes of all types; ArduRover, a fully-autonomous ground-based vehicle system.
ArduCopter is an open source multicopter aerial vehicle platform created by the DIY Drones community based on the Arduino platform and sold by 3D Robotics.
The free software approach from ArduCopter is similar to that of the Paparazzi Project and PX4 autopilot where low cost and availability enables its hobbyist use in small remotely piloted aircraft such as micro air vehicles and miniature UAVs.
The full-featured multicopter UAV controller won the Sparkfun 2013 Autonomous Vehicle Competition. A team of developers from around the globe are constantly improving and refining the performance and capabilities.
Programming and mission operations are done through an elegant and well-developed software interface. The entire package is designed to be easily approachable for the novice, while remaining open-ended for custom applications, education, and research use. 

Community for amateur Unmanned Aerial Vehicles. This community created the Arduino-based ArduPilot, the world's first universal autopilot (planes, multicopters of all sorts and ground rovers). The Pixhawk and APM 2.6 autopilots run a variety of powerful free and open UAV software systems, including: ArduCopter, a fully-autonomous multicopter and heli UAV system; ArduPlane, a pro-level UAV system for planes of all types; ArduRover, a fully-autonomous ground-based vehicle system.
ArduCopter is an open source multicopter aerial vehicle platform created by the DIY Drones community based on the Arduino platform and sold by 3D Robotics.
The free software approach from ArduCopter is similar to that of the Paparazzi Project and PX4 autopilot where low cost and availability enables its hobbyist use in small remotely piloted aircraft such as micro air vehicles and miniature UAVs.
The full-featured multicopter UAV controller won the Sparkfun 2013 Autonomous Vehicle Competition. A team of developers from around the globe are constantly improving and refining the performance and capabilities.
Programming and mission operations are done through an elegant and well-developed software interface. The entire package is designed to be easily approachable for the novice, while remaining open-ended for custom applications, education, and research use.
 WHOLE PROJECT MARK