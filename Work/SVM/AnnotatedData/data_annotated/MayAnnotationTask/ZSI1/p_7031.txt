PROJECT NAME: Fairphone
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 Fairphone is a social enterprise that started with the goal of opening up the supply chain, understanding how products are made and creating a better connection between people and the things they own. Our hope was to create greater transparency and start serious discussions – we didn’t actually envision ourselves as consumer electronics producers when we began! 

It started in 2010 as a project of Waag Society, Action Aid and Schrijf-Schrijf aimed at raising awareness about conflict minerals in electronics and the wars that the sourcing of these minerals is fueling in the DR Congo. The campaign and research into the complex supply chain ran for 3 years. In 2013, we established our social enterprise with the aim of designing, creating and producing our first smartphone and taking the next crucial steps in uncovering the story behind the sourcing, production, distribution and recycling of electronics. 

Fairphone is a social enterprise that started with the goal of opening up the supply chain, understanding how products are made and creating a better connection between people and the things they own. Our hope was to create greater transparency and start serious discussions – we didn’t actually envision ourselves as consumer electronics producers when we began! 

It started in 2010 as a project of Waag Society, Action Aid and Schrijf-Schrijf aimed at raising awareness about conflict minerals in electronics and the wars that the sourcing of these minerals is fueling in the DR Congo. The campaign and research into the complex supply chain ran for 3 years. In 2013, we established our social enterprise with the aim of designing, creating and producing our first smartphone and taking the next crucial steps in uncovering the story behind the sourcing, production, distribution and recycling of electronics.
 WHOLE PROJECT MARK