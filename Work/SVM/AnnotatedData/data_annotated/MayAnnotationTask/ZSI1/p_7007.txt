PROJECT NAME: Sensorica
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 SENSORICA is an open, decentralized, and self-organizing value network. It is a commons-based peer-production network, focused on designing sensing and sensemaking technology. 

In their own words:
Our mission: SENSORICA is committed to the design and deployment of intelligent, open sensors, and sensemaking systems, which allow our communities to optimize interactions with our physical environment and realize our full human potential

As an organization, SENSORICA is designed to facilitate large-scale co-creation and exchange of value:
facilitates large-scale coordination and collaboration secures transactions among affiliates and reduces transaction costs facilitates resource management allows multi-dimensional value accounting and fair redistribution of revenue open: easy access to participation horizontal: non-hierarchical governance, autonomy of affiliates, structure shaped by value-based relations rather than power-based relations
decentralized: bottom up allocation of resources, no central planning, no collective budget emergent and adaptive structure, constantly changing with respect to internal and environmental conditions SENSORICA is an ecosystem built on a value network. 

SENSORICA is an open, decentralized, and self-organizing value network. It is a commons-based peer-production network, focused on designing sensing and sensemaking technology. 

In their own words:
Our mission: SENSORICA is committed to the design and deployment of intelligent, open sensors, and sensemaking systems, which allow our communities to optimize interactions with our physical environment and realize our full human potential

As an organization, SENSORICA is designed to facilitate large-scale co-creation and exchange of value:
facilitates large-scale coordination and collaboration secures transactions among affiliates and reduces transaction costs facilitates resource management allows multi-dimensional value accounting and fair redistribution of revenue open: easy access to participation horizontal: non-hierarchical governance, autonomy of affiliates, structure shaped by value-based relations rather than power-based relations
decentralized: bottom up allocation of resources, no central planning, no collective budget emergent and adaptive structure, constantly changing with respect to internal and environmental conditions SENSORICA is an ecosystem built on a value network.
 WHOLE PROJECT MARK