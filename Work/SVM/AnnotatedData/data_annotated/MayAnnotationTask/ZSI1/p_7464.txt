PROJECT NAME: School of Data
PROJECT SOURCE: Open Knowledge International
---------------------------------------
NEW PAGE: 

School of Data              - Evidence is Power          
===============================

      School of Data              - Evidence is Power          

 Menu 

 Learn     Online Courses   Offline Trainings       Our Work     Programmes   Methodology       Fellows   Experts     The Fellowship Programme   The Data Expert Programme   Our Fellows   Experts       About     Who we are   Our History   Contact       Join     Contribute   Become a Member       Blog     

 Search for: 

 Search these: 

 Learn how to work with  extractive  gender  environment  budget  aid  corruption  sanitation  education  health  geospatial  human rights  crowdsourced  dirty  unstructured  migrant  elections  transparency  climate change  energy  humanitarian  data 
 We are a network of  individuals  and  organizations  working on empowering civil society organizations, journalists and citizens with  skills  they need to use data effectively. 
 We are School of Data and we believe that  evidence is power . 

 Learn  with us 
 Contribute  with us 

 Latest  blog post 
 Data is a Team Sport Published on December 20, 2017 by  Dirk Slater Data is a Team Sport is a series of online conversations held with data literacy practitioners in mid-2017 that explores the ever evolving data literacy eco-system. Our aim in producing  Data is a Team Sport  was to surface learnings and present them in formats that would be accessible to data literacy practitioners. Thanks to the efforts [ ] Full Post 

 Latest  course 
 Course outline: mobile data collection with ODK Published on November 13, 2015 by  Sheena Carmel Opulencia-Calub ##Introduction Decades ago, the use of papers and pen was a painstaking and very expensive effort to collect data. Most of us have experienced paper forms getting wet or damaged, or receiving paper forms that were barely answered. But as the age of smartphones and tablets arrived, mobile-based data collection technologies have also gained a [ ] Full Post 

 Latest  publication 
 The Easy Guide to Mobile Data Collection Published on February 15, 2016 by  Cedric Lombion Since the advent of the cheap smartphone, these devices have been used for an impressive range of purposes, from detecting illegal logging in rainforests to the creation of detailed maps of cities around the world. Everywhere, civil society organisations and other movements are mapping their local areas, whether that be recording the locations of homeless [ ] Full Post 

 Follow  us on 

 School of Data   SchoolOfData   4/02/18 - 11:59 am 
 RT  @witnessorg : . @Advocassembly  is a great resource and provides free online courses for activists from  @_SecurityFirst ,  @SchoolOfData , WIT 
 Reply   Retweet     Favorite   School of Data   SchoolOfData   17/01/18 - 5:22 pm 
 RT  @Morchickit : Getting ready for the  @360Giving   #data  expedition tomorrow in Brighton with  @ocsi_uk  and East Sussex funders! We got our  
 Reply   Retweet     Favorite   School of Data   SchoolOfData   12/01/18 - 5:35 pm 
 RT  @gijn : Top 10  #ddj : Check out  @SchoolOfData 's "Data Is A Team Sport" podcasts, which features conversations on data literacy with @Dqlep 
 Reply   Retweet     Favorite   School of Data   SchoolOfData   10/01/18 - 10:24 pm 
 @thomasgpadilla   @brandontlocke   @yasmeen_azadi   @BergisJules   @IandPangurBan   @AWhitTwit   @OKFN  Thanks for the recommend   twitter.com/i/web/status/9 
 Reply   Retweet     Favorite   School of Data   SchoolOfData   20/12/17 - 10:06 pm 
 RT  @NataliaMazotte : In 2018 we're gonna have a brand new Data Journalism Handbook! And I'm super honored and excited to contribute to the n 
 Reply   Retweet     Favorite   

 6000 +   people  trained 

44  learning  modules 

13  member  organisations 

100 individual  members 

34  countries  represented 

 Built with the support of 

 Terms of Use   Privacy Policy     

 This site uses cookies  More info 

      School of Data              - Evidence is Power          
===============================

      School of Data              - Evidence is Power          

 Menu 

 Learn     Online Courses   Offline Trainings       Our Work     Programmes   Methodology       Fellows   Experts     The Fellowship Programme   The Data Expert Programme   Our Fellows   Experts       About     Who we are   Our History   Contact       Join     Contribute   Become a Member       Blog     

 Search for: 

 Search these: 

 Learn how to work with  extractive  gender  environment  budget  aid  corruption  sanitation  education  health  geospatial  human rights  crowdsourced  dirty  unstructured  migrant  elections  transparency  climate change  energy  humanitarian  data 
 We are a network of  individuals  and  organizations  working on empowering civil society organizations, journalists and citizens with  skills  they need to use data effectively. 
 We are School of Data and we believe that  evidence is power . 

 Learn  with us 
 Contribute  with us 

 Latest  blog post 
 Data is a Team Sport Published on December 20, 2017 by  Dirk Slater Data is a Team Sport is a series of online conversations held with data literacy practitioners in mid-2017 that explores the ever evolving data literacy eco-system. Our aim in producing  Data is a Team Sport  was to surface learnings and present them in formats that would be accessible to data literacy practitioners. Thanks to the efforts [ ] Full Post 

 Latest  course 
 Course outline: mobile data collection with ODK Published on November 13, 2015 by  Sheena Carmel Opulencia-Calub ##Introduction Decades ago, the use of papers and pen was a painstaking and very expensive effort to collect data. Most of us have experienced paper forms getting wet or damaged, or receiving paper forms that were barely answered. But as the age of smartphones and tablets arrived, mobile-based data collection technologies have also gained a [ ] Full Post 

 Latest  publication 
 The Easy Guide to Mobile Data Collection Published on February 15, 2016 by  Cedric Lombion Since the advent of the cheap smartphone, these devices have been used for an impressive range of purposes, from detecting illegal logging in rainforests to the creation of detailed maps of cities around the world. Everywhere, civil society organisations and other movements are mapping their local areas, whether that be recording the locations of homeless [ ] Full Post 

 Follow  us on 

 School of Data   SchoolOfData   13/03/18 - 5:06 pm 
 RT  @Morchickit : Check out the  @360Giving  data expedition video inspired by the methodology of  @SchoolOfData  !  twitter.com/360Giving/stat 
 Reply   Retweet     Favorite   School of Data   SchoolOfData   4/02/18 - 11:59 am 
 RT  @witnessorg : . @Advocassembly  is a great resource and provides free online courses for activists from  @_SecurityFirst ,  @SchoolOfData , WIT 
 Reply   Retweet     Favorite   School of Data   SchoolOfData   17/01/18 - 5:22 pm 
 RT  @Morchickit : Getting ready for the  @360Giving   #data  expedition tomorrow in Brighton with  @ocsi_uk  and East Sussex funders! We got our  
 Reply   Retweet     Favorite   School of Data   SchoolOfData   12/01/18 - 5:35 pm 
 RT  @gijn : Top 10  #ddj : Check out  @SchoolOfData 's "Data Is A Team Sport" podcasts, which features conversations on data literacy with @Dqlep 
 Reply   Retweet     Favorite   School of Data   SchoolOfData   10/01/18 - 10:24 pm 
 @thomasgpadilla   @brandontlocke   @yasmeen_azadi   @BergisJules   @IandPangurBan   @AWhitTwit   @OKFN  Thanks for the recommend   twitter.com/i/web/status/9 
 Reply   Retweet     Favorite   

 6000 +   people  trained 

44  learning  modules 

13  member  organisations 

100 individual  members 

34  countries  represented 

 Built with the support of 

 Terms of Use   Privacy Policy     

 This site uses cookies  More info 

      School of Data              - Evidence is Power          
===============================

      School of Data              - Evidence is Power          

 Menu 

 Learn     Online Courses   Offline Trainings       Our Work     Programmes   Methodology       Fellows   Experts     The Fellowship Programme   The Data Expert Programme   Our Fellows   Experts       About     Who we are   Our History   Contact       Join     Contribute   Become a Member       Blog     

 Search for: 

 Search these: 

 Learn how to work with  extractive  gender  environment  budget  aid  corruption  sanitation  education  health  geospatial  human rights  crowdsourced  dirty  unstructured  migrant  elections  transparency  climate change  energy  humanitarian  data 
 We are a network of  individuals  and  organizations  working on empowering civil society organizations, journalists and citizens with  skills  they need to use data effectively. 
 We are School of Data and we believe that  evidence is power . 

 Learn  with us 
 Contribute  with us 

 Latest  blog post 
 Data is a Team Sport Published on December 20, 2017 by  Dirk Slater Data is a Team Sport is a series of online conversations held with data literacy practitioners in mid-2017 that explores the ever evolving data literacy eco-system. Our aim in producing  Data is a Team Sport  was to surface learnings and present them in formats that would be accessible to data literacy practitioners. Thanks to the efforts [ ] Full Post 

 Latest  course 
 Course outline: mobile data collection with ODK Published on November 13, 2015 by  Sheena Carmel Opulencia-Calub ##Introduction Decades ago, the use of papers and pen was a painstaking and very expensive effort to collect data. Most of us have experienced paper forms getting wet or damaged, or receiving paper forms that were barely answered. But as the age of smartphones and tablets arrived, mobile-based data collection technologies have also gained a [ ] Full Post 

 Latest  publication 
 The Easy Guide to Mobile Data Collection Published on February 15, 2016 by  Cedric Lombion Since the advent of the cheap smartphone, these devices have been used for an impressive range of purposes, from detecting illegal logging in rainforests to the creation of detailed maps of cities around the world. Everywhere, civil society organisations and other movements are mapping their local areas, whether that be recording the locations of homeless [ ] Full Post 

 Follow  us on 

 School of Data   SchoolOfData   13/03/18 - 5:06 pm 
 RT  @Morchickit : Check out the  @360Giving  data expedition video inspired by the methodology of  @SchoolOfData  !  twitter.com/360Giving/stat 
 Reply   Retweet     Favorite   School of Data   SchoolOfData   4/02/18 - 11:59 am 
 RT  @witnessorg : . @Advocassembly  is a great resource and provides free online courses for activists from  @_SecurityFirst ,  @SchoolOfData , WIT 
 Reply   Retweet     Favorite   School of Data   SchoolOfData   17/01/18 - 5:22 pm 
 RT  @Morchickit : Getting ready for the  @360Giving   #data  expedition tomorrow in Brighton with  @ocsi_uk  and East Sussex funders! We got our  
 Reply   Retweet     Favorite   School of Data   SchoolOfData   12/01/18 - 5:35 pm 
 RT  @gijn : Top 10  #ddj : Check out  @SchoolOfData 's "Data Is A Team Sport" podcasts, which features conversations on data literacy with @Dqlep 
 Reply   Retweet     Favorite   School of Data   SchoolOfData   10/01/18 - 10:24 pm 
 @thomasgpadilla   @brandontlocke   @yasmeen_azadi   @BergisJules   @IandPangurBan   @AWhitTwit   @OKFN  Thanks for the recommend   twitter.com/i/web/status/9 
 Reply   Retweet     Favorite   

 6000 +   people  trained 

44  learning  modules 

13  member  organisations 

100 individual  members 

34  countries  represented 

 Built with the support of 

 Terms of Use   Privacy Policy     

 This site uses cookies  More info
 WHOLE PROJECT MARK