PROJECT NAME: RepRap Project
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

RepRap - RepRapWiki
===============================

RepRap - RepRapWiki



 About  |  Development  |   Community  |  RepRap Machines  |  Resources  |  Policy 

   Welcome to  RepRap.org 

 RepRap  is humanity's first general-purpose  self-replicating manufacturing machine .
 RepRap  takes the form of a  free   desktop 3D printer  capable of printing plastic objects. Since many parts of RepRap are made from plastic and RepRap prints those parts, RepRap self-replicates by making a kit of itself  - a kit that anyone can assemble given time and materials.  It also means that - if you've got a RepRap - you can print  lots of useful stuff , and you can  print another RepRap  for a  friend ...
 RepRap is about making self-replicating machines , and making them freely available for the benefit of everyone.  We are using 3D printing to do this, but if you have other technologies that can copy themselves and that can be made freely available to all, then this is the place for you too.
 Reprap.org is a community project , which means you are welcome to edit most pages on this site, or better yet, create new pages of your own.  Our  community portal  and  New Development  pages have more information on how to get involved.  Use the links below and on the left to explore the site contents. You'll find some content  translated into other languages .
 RepRap  was the first of the low-cost 3D printers, and the RepRap Project started the open-source 3D printer revolution.  It has become the most widely-used 3D printer among the global members of the  Maker Community .
 RepRap  was voted  the most significant 3D-printed object  in 2017.  That is to say people think that the most important thing you can print in a 3D printer is another 3D printer - the whole reason for the  RepRap  project.  Also in 2017  RepRap' s creator  Adrian Bowyer  was awarded the 3D Printing Industry's  Outstanding Contribution to 3D Printing Award  and was inducted into the  3D Printing Hall of Fame .
 RepRap  state-of-the-art when this page was last updated (June 2017) is well represented by the   RepRap Snappy ,   RepRap Dollo  and the  RepRap Generation 7 Electronics .  By state-of-the-art we mean the best self-replicators.  These may not be the most technically advanced 3D printers, but - arguably - any self-replicator is, by that very fact, more advanced than anything that is not.

   Source: Moilanen, J.   Vad n, T.: 3D printing community and emerging practices of peer production,  First Monday, Volume 18, Number 8 - 5 August 2013 . 

  A family using one RepRap to print only 20 domestic products per year (about 0.02% of the products available) can expect to save between  $300 and $2000: 
 "...the unavoidable conclusion from this study is that the RepRap is an economically attractive investment for the average US household already." 

   Source: B.T. Wittbrodt  et al ., Life-cycle economic analysis of distributed manufacturing with open-source 3-D printers,  Mechatronics . 

 About    |    Development    |    Community    |    RepRap Machines    |    Resources    |    Policy 

 If you want to   build a RepRap, click here  for details of some of the more popular designs.  There is also a legacy section at the end for our older machines so you can see how the designs have evolved. 

   Recent Wiki Changes 

				Retrieved from " http://reprap.org/mediawiki/index.php?title=RepRap oldid=181805 "				 

 Categories :  Community Categories  

 Navigation menu 

 Personal tools 

 Log in   

 Namespaces 

 Page 
 Discussion 

 Variants 

 Views 

 Read 
 View source 
 View history 

 Actions 

 Search 

 Navigation 

 Main Page 
 Build a RepRap 
 Glossary 
 Reference 

 Participation 

 Recent Changes 
 Get a Wiki account 
 Create a new page 
 Donation Page 
 Policy 

 Community 

 RepRap Forum 
 RepRap IRC 
 Development Index 
 Planet RepRap 
 RepRap User Groups 
 Community Portal 
 Licence 

 Toolbox 

 What links here 
 Related changes 
 Special pages 
 Printable version 
 Permanent link 
 Page information 

  This page was last modified on 6 March 2018, at 08:01. 
 Content is available under  GNU Free Documentation License 1.2 . 

 Privacy policy 
 About RepRapWiki 
 Disclaimers 

RepRap - RepRapWiki
===============================

RepRap - RepRapWiki

 RepRap 

 From RepRapWiki 

					Jump to:					 navigation , 					 search 

 There are  security restrictions  on this page 


 English 

 catal 
 esky 
 Deutsch 

 espa ol 

 fran ais 
 hrvatski 
 magyar 
 italiano 
 rom n 

 lietuvi 
 Nederlands 
 norsk bokm l 
 polski 
 portugu s 

 T rk e 

 az rbaycanca 

 About  |  Development  |   Community  |  RepRap Machines  |  Resources  |  Policy 

   Welcome to  RepRap.org 

 RepRap  is humanity's first general-purpose  self-replicating manufacturing machine .
 RepRap  takes the form of a  free   desktop 3D printer  capable of printing plastic objects. Since many parts of RepRap are made from plastic and RepRap prints those parts, RepRap self-replicates by making a kit of itself  - a kit that anyone can assemble given time and materials.  It also means that - if you've got a RepRap - you can print  lots of useful stuff , and you can  print another RepRap  for a  friend ...
 RepRap is about making self-replicating machines , and making them freely available for the benefit of everyone.  We are using 3D printing to do this, but if you have other technologies that can copy themselves and that can be made freely available to all, then this is the place for you too.
 Reprap.org is a community project , which means you are welcome to edit most pages on this site, or better yet, create new pages of your own.  Our  community portal  and  New Development  pages have more information on how to get involved.  Use the links below and on the left to explore the site contents. You'll find some content  translated into other languages .
 RepRap  was the first of the low-cost 3D printers, and the RepRap Project started the open-source 3D printer revolution.  It has become the most widely-used 3D printer among the global members of the  Maker Community .
 RepRap  was voted  the most significant 3D-printed object  in 2017.  That is to say people think that the most important thing you can print in a 3D printer is another 3D printer - the whole reason for the  RepRap  project.  Also in 2017  RepRap' s creator  Adrian Bowyer  was awarded the 3D Printing Industry's  Outstanding Contribution to 3D Printing Award  and was inducted into the  3D Printing Hall of Fame .
 RepRap  state-of-the-art when this page was last updated (June 2017) is well represented by the   RepRap Snappy ,   RepRap Dollo  and the  RepRap Generation 7 Electronics .  By state-of-the-art we mean the best self-replicators.  These may not be the most technically advanced 3D printers, but - arguably - any self-replicator is, by that very fact, more advanced than anything that is not.

   Source: Moilanen, J.   Vad n, T.: 3D printing community and emerging practices of peer production,  First Monday, Volume 18, Number 8 - 5 August 2013 . 

  A family using one RepRap to print only 20 domestic products per year (about 0.02% of the products available) can expect to save between  $300 and $2000: 
 "...the unavoidable conclusion from this study is that the RepRap is an economically attractive investment for the average US household already." 

   Source: B.T. Wittbrodt  et al ., Life-cycle economic analysis of distributed manufacturing with open-source 3-D printers,  Mechatronics . 

 About    |    Development    |    Community    |    RepRap Machines    |    Resources    |    Policy 

 If you want to   build a RepRap, click here  for details of some of the more popular designs.  There is also a legacy section at the end for our older machines so you can see how the designs have evolved. 

   Recent Wiki Changes 

				Retrieved from " http://reprap.org/mediawiki/index.php?title=RepRap oldid=181805 "				 

 Categories :  Community Categories  

 Navigation menu 

 Personal tools 

 Log in   

 Namespaces 

 Page 
 Discussion 

 Variants 

 Views 

 Read 
 View source 
 View history 

 Actions 

 Search 

 Navigation 

 Main Page 
 Build a RepRap 
 Glossary 
 Reference 

 Participation 

 Recent Changes 
 Get a Wiki account 
 Create a new page 
 Donation Page 
 Policy 

 Community 

 RepRap Forum 
 RepRap IRC 
 Development Index 
 Planet RepRap 
 RepRap User Groups 
 Community Portal 
 Licence 

 Toolbox 

 What links here 
 Related changes 
 Special pages 
 Printable version 
 Permanent link 
 Page information 

  This page was last modified on 6 March 2018, at 09:01. 
 Content is available under  GNU Free Documentation License 1.2 . 

 Privacy policy 
 About RepRapWiki 
 Disclaimers 

 The RepRap project is an initiative to develop a 3D printer that can print most of its own components. RepRap (short for replicating rapid prototyper) uses a variant of fused deposition modeling, an additive manufacturing technique. The project calls it Fused Filament Fabrication (FFF) to avoid trademark issues around the "fused deposition modeling" term.---------------------------------------
NEW PAGE: 

RepRap - RepRapWiki
===============================

RepRap - RepRapWiki

 RepRap 

 From RepRapWiki 

					Jump to:					 navigation , 					 search 

 There are  security restrictions  on this page 

 edit  is restricted to the  sysop  group (set from the "protect" tab) 
 move  is restricted to the  sysop  group (set from the "protect" tab) 
 read  is restricted to the  sysop  group (set from the "protect" tab) 

 English 

 catal 
 esky 
 Deutsch 

 espa ol 

 fran ais 
 hrvatski 
 magyar 
 italiano 
 rom n 

 lietuvi 
 Nederlands 
 norsk bokm l 
 polski 
 portugu s 

 T rk e 

 az rbaycanca 

 About  |  Development  |   Community  |  RepRap Machines  |  Resources  |  Policy 

   Welcome to  RepRap.org 

 RepRap  is humanity's first general-purpose  self-replicating manufacturing machine .
 RepRap  takes the form of a  free   desktop 3D printer  capable of printing plastic objects. Since many parts of RepRap are made from plastic and RepRap prints those parts, RepRap self-replicates by making a kit of itself  - a kit that anyone can assemble given time and materials.  It also means that - if you've got a RepRap - you can print  lots of useful stuff , and you can  print another RepRap  for a  friend ...
 RepRap is about making self-replicating machines , and making them freely available for the benefit of everyone.  We are using 3D printing to do this, but if you have other technologies that can copy themselves and that can be made freely available to all, then this is the place for you too.
 Reprap.org is a community project , which means you are welcome to edit most pages on this site, or better yet, create new pages of your own.  Our  community portal  and  New Development  pages have more information on how to get involved.  Use the links below and on the left to explore the site contents. You'll find some content  translated into other languages .
 RepRap  was the first of the low-cost 3D printers, and the RepRap Project started the open-source 3D printer revolution.  It has become the most widely-used 3D printer among the global members of the  Maker Community .
 RepRap  was voted  the most significant 3D-printed object  in 2017.  That is to say people think that the most important thing you can print in a 3D printer is another 3D printer - the whole reason for the  RepRap  project.  Also in 2017  RepRap' s creator  Adrian Bowyer  was awarded the 3D Printing Industry's  Outstanding Contribution to 3D Printing Award  and was inducted into the  3D Printing Hall of Fame .
 RepRap  state-of-the-art when this page was last updated (June 2017) is well represented by the   RepRap Snappy ,   RepRap Dollo  and the  RepRap Generation 7 Electronics .  By state-of-the-art we mean the best self-replicators.  These may not be the most technically advanced 3D printers, but - arguably - any self-replicator is, by that very fact, more advanced than anything that is not.

   Source: Moilanen, J.   Vad n, T.: 3D printing community and emerging practices of peer production,  First Monday, Volume 18, Number 8 - 5 August 2013 . 

  A family using one RepRap to print only 20 domestic products per year (about 0.02% of the products available) can expect to save between  $300 and $2000: 
 "...the unavoidable conclusion from this study is that the RepRap is an economically attractive investment for the average US household already." 

   Source: B.T. Wittbrodt  et al ., Life-cycle economic analysis of distributed manufacturing with open-source 3-D printers,  Mechatronics . 

 About    |    Development    |    Community    |    RepRap Machines    |    Resources    |    Policy 

 If you want to   build a RepRap, click here  for details of some of the more popular designs.  There is also a legacy section at the end for our older machines so you can see how the designs have evolved. 

   Recent Wiki Changes 

				Retrieved from " http://reprap.org/mediawiki/index.php?title=RepRap oldid=181805 "				 

 Categories :  Community Categories  

 Navigation menu 

 Personal tools 

 Log in   

 Namespaces 

 Page 
 Discussion 

 Variants 

 Views 

 Read 
 View source 
 View history 

 Actions 

 Search 

 Navigation 

 Main Page 
 Build a RepRap 
 Glossary 
 Reference 

 Participation 

 Recent Changes 
 Get a Wiki account 
 Create a new page 
 Donation Page 
 Policy 

 Community 

 RepRap Forum 
 RepRap IRC 
 Development Index 
 Planet RepRap 
 RepRap User Groups 
 Community Portal 
 Licence 

 Toolbox 

 What links here 
 Related changes 
 Special pages 
 Printable version 
 Permanent link 
 Page information 

  This page was last modified on 6 March 2018, at 08:01. 
 Content is available under  GNU Free Documentation License 1.2 . 

 Privacy policy 
 About RepRapWiki 
 Disclaimers 

RepRap - RepRapWiki
===============================

RepRap - RepRapWiki

 RepRap 

 From RepRapWiki 

					Jump to:					 navigation , 					 search 

 There are  security restrictions  on this page 

 edit  is restricted to the  sysop  group (set from the "protect" tab) 
 move  is restricted to the  sysop  group (set from the "protect" tab) 
 read  is restricted to the  sysop  group (set from the "protect" tab) 

 English 

 catal 
 esky 
 Deutsch 

 espa ol 

 fran ais 
 hrvatski 
 magyar 
 italiano 
 rom n 

 lietuvi 
 Nederlands 
 norsk bokm l 
 polski 
 portugu s 

 T rk e 

 az rbaycanca 

 About  |  Development  |   Community  |  RepRap Machines  |  Resources  |  Policy 

   Welcome to  RepRap.org 

 RepRap  is humanity's first general-purpose  self-replicating manufacturing machine .
 RepRap  takes the form of a  free   desktop 3D printer  capable of printing plastic objects. Since many parts of RepRap are made from plastic and RepRap prints those parts, RepRap self-replicates by making a kit of itself  - a kit that anyone can assemble given time and materials.  It also means that - if you've got a RepRap - you can print  lots of useful stuff , and you can  print another RepRap  for a  friend ...
 RepRap is about making self-replicating machines , and making them freely available for the benefit of everyone.  We are using 3D printing to do this, but if you have other technologies that can copy themselves and that can be made freely available to all, then this is the place for you too.
 Reprap.org is a community project , which means you are welcome to edit most pages on this site, or better yet, create new pages of your own.  Our  community portal  and  New Development  pages have more information on how to get involved.  Use the links below and on the left to explore the site contents. You'll find some content  translated into other languages .
 RepRap  was the first of the low-cost 3D printers, and the RepRap Project started the open-source 3D printer revolution.  It has become the most widely-used 3D printer among the global members of the  Maker Community .
 RepRap  was voted  the most significant 3D-printed object  in 2017.  That is to say people think that the most important thing you can print in a 3D printer is another 3D printer - the whole reason for the  RepRap  project.  Also in 2017  RepRap' s creator  Adrian Bowyer  was awarded the 3D Printing Industry's  Outstanding Contribution to 3D Printing Award  and was inducted into the  3D Printing Hall of Fame .
 RepRap  state-of-the-art when this page was last updated (June 2017) is well represented by the   RepRap Snappy ,   RepRap Dollo  and the  RepRap Generation 7 Electronics .  By state-of-the-art we mean the best self-replicators.  These may not be the most technically advanced 3D printers, but - arguably - any self-replicator is, by that very fact, more advanced than anything that is not.

   Source: Moilanen, J.   Vad n, T.: 3D printing community and emerging practices of peer production,  First Monday, Volume 18, Number 8 - 5 August 2013 . 

  A family using one RepRap to print only 20 domestic products per year (about 0.02% of the products available) can expect to save between  $300 and $2000: 
 "...the unavoidable conclusion from this study is that the RepRap is an economically attractive investment for the average US household already." 

   Source: B.T. Wittbrodt  et al ., Life-cycle economic analysis of distributed manufacturing with open-source 3-D printers,  Mechatronics . 

 About    |    Development    |    Community    |    RepRap Machines    |    Resources    |    Policy 

 If you want to   build a RepRap, click here  for details of some of the more popular designs.  There is also a legacy section at the end for our older machines so you can see how the designs have evolved. 

   Recent Wiki Changes 

				Retrieved from " http://reprap.org/mediawiki/index.php?title=RepRap oldid=181805 "				 

 Categories :  Community Categories  

 Navigation menu 

 Personal tools 

 Log in   

 Namespaces 

 Page 
 Discussion 

 Variants 

 Views 

 Read 
 View source 
 View history 

 Actions 

 Search 

 Navigation 

 Main Page 
 Build a RepRap 
 Glossary 
 Reference 

 Participation 

 Recent Changes 
 Get a Wiki account 
 Create a new page 
 Donation Page 
 Policy 

 Community 

 RepRap Forum 
 RepRap IRC 
 Development Index 
 Planet RepRap 
 RepRap User Groups 
 Community Portal 
 Licence 

 Toolbox 

 What links here 
 Related changes 
 Special pages 
 Printable version 
 Permanent link 
 Page information 

  This page was last modified on 6 March 2018, at 09:01. 
 Content is available under  GNU Free Documentation License 1.2 . 

 Privacy policy 
 About RepRapWiki 
 Disclaimers 

 The RepRap project is an initiative to develop a 3D printer that can print most of its own components. RepRap (short for replicating rapid prototyper) uses a variant of fused deposition modeling, an additive manufacturing technique. The project calls it Fused Filament Fabrication (FFF) to avoid trademark issues around the "fused deposition modeling" term. 

The RepRap project is an initiative to develop a 3D printer that can print most of its own components. RepRap (short for replicating rapid prototyper) uses a variant of fused deposition modeling, an additive manufacturing technique. The project calls it Fused Filament Fabrication (FFF) to avoid trademark issues around the "fused deposition modeling" term.
 WHOLE PROJECT MARK
