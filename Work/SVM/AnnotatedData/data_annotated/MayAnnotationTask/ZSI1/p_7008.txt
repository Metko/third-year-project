PROJECT NAME: Unhosted
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

unhosted web apps
===============================

unhosted web apps

 unhosted web apps 
 freedom from web 2.0's monopoly platforms 

 definition 
 Also known as "serverless", "client-side", 
or "static" web apps, unhosted web apps do not send 
your user data to their server.
Either you connect your own server at runtime, 
or your data stays within the browser. 
 advantages compared to native (iOS, Android) apps 

 web apps work on any device, no matter which platform or provider 
 you can get apps from any website, not just through a censored app store

 advantages compared to hosted web apps 

 choose your server provider independently of which apps you use 
 likewise, your choice of apps is not narrowed down by your storage provider 
 the app provider no longer gets to see your data by default 
 unhosted web apps are cheaper to publish than hosted ones 
 unhosted web apps can easily be mirrored for kill-switch resilience 
 infinite per-app backend scalability (there simply  is  no per-app backend) 

 what? no backend? so the whole app is actually client-side? 

 your everyday LAMP-stack, J2SE, .net, or Ruby-on-Rails hosted web app 
 yep. over http, you receive the app's  source code , rather than an opaque user interface. take a minute to wrap your head around this 

 the new  unhosted web app  architecture 
  and then
 read on ,
 learn about tools ,
or  build your first app ! 

 Overview: 
 i. definition 
  ii.  getting started 
  iii.  example apps 
  iv.  people 
  v.  events 
  vi.  dev tools 
  vii.  forum 
 (all episodes as a book) 
 ( .pdf )( .epub )  ( .mobi )  
 Adventures: 
  1.  intro 
  2.  editor 
  3.  server 
  4.  WebSockets 
  5.  social 
  6.  webshell 
  7.  remoteStorage 
  8.  your data 
  9.  email 
  10.  web linking 
  11.  app hosting 
  12.  app discovery 
  13.  users 
  14.  p2p 
  15.  unhosted oauth 
  16.  save the web 
 Decentralize: 
  17.  cryptography 
  18.  dht 
  19.  internet 
  20.  identity 
  21.  browser sessions 
  22.  search 
  23.  neutrality 
  24.  federation 
  25.  anonymity 
  26.  reputation 
 Practice: 
  27.  browser storage 
  28.  sync 
  29.  offline first 
  30.  baas 
  31.  per-user backend 
  32.  per-user clients 
  33.  client-side frontend 
  34.  conclusions 

 Supporters: 

          and  many more 

 You can follow 
 @unhosted 
 on twitter and in 
 many other ways . So stay tuned! :) 

 A software development project that aims to provide freedom from web 2.0 monopoly platforms.

Also known as "serverless", "client-side", or "static" web apps, unhosted web apps do not send your user data to their server. Either you connect your own server at runtime, or your data stays within the browser. 

A software development project that aims to provide freedom from web 2.0 monopoly platforms.

Also known as "serverless", "client-side", or "static" web apps, unhosted web apps do not send your user data to their server. Either you connect your own server at runtime, or your data stays within the browser.
 WHOLE PROJECT MARK