PROJECT NAME: National Rural Livelihood Mission
PROJECT SOURCE: SI-drive
---------------------------------------
NEW PAGE: 

Welcome to National Rural Livelihoods Mission | National Rural Livelihoods Mission
===============================

Welcome to National Rural Livelihoods Mission | National Rural Livelihoods Mission

 Home 
 Feedback 
 Sitemap 

 Login 

 Skip to main content 

 Screen Reader Access 

 lang_dropdown_select 

 About DAY-NRLM Genesis 
 Mission 
 Features 
 Implementation 
 Support Structure 
 NRLP 

 Components Institutional Building   Capacity Building 
 Financial Inclusion 
 Livelihoods Promotion 
 Social Inclusion   Development 
 Systems 
 Convergence 

 NRLPS Overview 
 Objectives 
 General Body 
 Executive Committee 
 NRLPS Documents 
 Audit Reports 2014-2015 

 National Team MoRD 
 NRLPS 

 State Missions 
 Careers 

 Search form 

 Search  

 Highlights 
 Pause 

 Presentations made during NRLM National Best Practices Workshop in Delhi (22-23 AUG 2016) Day 1 
 Presentations made during NRLM National Best Practices Workshop in Delhi (22-23 AUG 2016) Day 2 
 Presentation given by Shri Atal Dulloo (JS-RL) on DAY-NRLM at PRC meeting of MoRD on July 14-15, 2016 
 All donations towards the Prime Minister s National Relief Fund (PMNRF) are notified for 100% deduction from taxable income under Section 80G of the Income Tax Act, 1961 
 NRLP Mid Term Assessment Report March 2015 
 Report of the Expert group on Setting up a Developmental Financial Institution for Women SHGs 

 National Repository of Knowledge Products 

 Search 

 NRLM Documents 
 Newsletters 
 Success Stories 

 Welcome to DEENDAYAL ANTYODAYA YOJANA - NRLM 

 Aajeevika - National Rural Livelihoods Mission (NRLM) was launched by the Ministry of Rural Development (MoRD), Government of India in June 2011. 
 Aided in part through investment support by the World Bank, the Mission aims at creating efficient and effective institutional platforms of the rural poor, enabling them to increase household income through sustainable livelihood enhancements and improved access to financial services.   

 Read more  about Welcome to DEENDAYAL ANTYODAYA YOJANA - NRLM 

 Updates 
 Letters/Circulars 
 Tenders 
 Events 
 Media/News 

   Pause 

 NRLPS EOI for Empanelment of TSA  for Value Chain Developement Final Draft 16 Jan 18 

 TOR Value Chain Development for empanelment for SRLM final draft 19 Feb 2018                                                                                                                                                                                    

 Advertisement for different posts at state and districts level under Sanjeevini, Karnataka SRLM 

 Engagement of State Project Manager (Social Development) under WBSRLM 

 Terms of reference for Hiring an agency for Conducting a Mid-Term Evaluation of Start-up Village Entrepreneurship Programme (SVEP).  

  Expression of Interest for Hiring an agency for Conducting a Mid-Term Evaluation of Start-up Village Entrepreneurship Programme (SVEP). Last date for submission 22nd January 2018.   

  Filling of the post of Director (Administration and Finance), Deputy Director (Administration) and Accounts Officer on deputation basis in the NRLPS, Ministry of Rural Development. 

 Corrigendum No. I to Request for Expression of Interest (REOl)  advertised on CPP Portal dated 8th December 2017 for hiring an agency for Evaluation of MKSP 

 Draft Terms of Reference for Conducting an Evaluation of Mahila Kishan Sashatikaran Pariyojna (MKSP) 

 Expression of Interest for Hiring an agency for Conducting an Evaluation of Mahila Kishan Sashatikaran Pariyojna (MKSP) 

 View All  

   Pause 

 Tasar Foundation recognized as DAY-NRLM NSO to provide support Tasar based livelihoods to State Missions 
 06 March,2018   

 CPC Facilitator's Manual 
 01 March,2018   

 PRI-CBO Convergence Toolkit 
 01 March,2018   

 Workshop Minutes to develop the implementation strategy and roll-out for Organic village clusters", held from 21 to 23 Feb 2018 at NIRD PR, Hyderabad 
 28 February,2018   

 Environmental Management Framework for NRETP 
 27 February,2018   

 Social Management Framework for NRETP 
 27 February,2018   

 Proceedings of DAY-NRLM Farm Livelihoods Half- Yearly Review (5th to 7th December, 2017), at NIRD-PR, Hyderabad.  
 26 February,2018   

 Joint Advisory on Nutrition 
 13 February,2018   

 Work distribution in RL Division 
 07 February,2018   

 Joint Advisory on Nutrition 
 06 February,2018   

 View All  

 Tenders and Contracts 

   Pause 

 TOR Value Chain Development for empanelment for SRLM final draft 19 Feb 2018                                                                                                                                                                                    
 21 February,2018   

 NRLPS EOI for Empanelment of TSA  for Value Chain Developement Final Draft 16 Jan 18 
 21 February,2018   

  Expression of Interest for Hiring an agency for Conducting a Mid-Term Evaluation of Start-up Village Entrepreneurship Programme (SVEP). Last date for submission 22nd January 2018.   
 08 January,2018   

 Terms of reference for Hiring an agency for Conducting a Mid-Term Evaluation of Start-up Village Entrepreneurship Programme (SVEP).  
 08 January,2018   

 Corrigendum No. I to Request for Expression of Interest (REOl)  advertised on CPP Portal dated 8th December 2017 for hiring an agency for Evaluation of MKSP 
 22 December,2017   

 Expression of Interest for Hiring an agency for Conducting an Evaluation of Mahila Kishan Sashatikaran Pariyojna (MKSP) 
 08 December,2017   

 Draft Terms of Reference for Conducting an Evaluation of Mahila Kishan Sashatikaran Pariyojna (MKSP) 
 08 December,2017   

 Request for EOI (expression of interest) from NGOs for Empanelment of Agencies for Implementation of SVEP 
 11 July,2017   

 NRLP EOI for Conducting Multi-State Impact Assessment of DAY NRLM 
 25 June,2017   

 Hiring of HR Recruitment Agency for Maharashtra SRLM 
 24 May,2017   

 View All  

   Pause 

 Regional SARAS Fair-2017 at Raipur and Durg-Bhilai 
 28 November,2017   

 Update from CGSRLM 
 03 March,2017   

 SARAS Calendar  FAIRS 2016-2017 updated on 26.12.2016 
 27 December,2016   

 Farm Livelihoods Workshop 
 04 November,2016   

  SARAS Calendar 2015-16 as on 14-09-2015 
 18 September,2015   

 SARAS Calander2015-16 
 16 June,2015   

 Minutes of consultation meeting 21st May SVEP 
 09 June,2015   

 Write shop in Trivandrum on NRLM -NREGS convergence 
 08 December,2014   

 HR-IB-CB- FI Meeting in Hyderabad 
 08 December,2014   

 National Consultation on Bonded labour in Bubabneswar 
 08 December,2014   

 View All  

   Pause 

 DAY-NRLM Presentation for PRC Meeting on July 14-15, 2016 
 15 July,2016   

 Mizoram Vacancies - Apply by 20 November 2015 
 10 November,2015   

 Rs 500 crore saving from NRLM to be allocated for Naxal-hit areas 
 11 November,2014   

  J K Bank distributes Rs. 30 lakhs to 60 SHGs under UMEED programme  
 25 June,2014   

 CRP rounds beings in Nagaland  
 25 June,2014   

 View All    

 Quick Links 

 DAY-NRLM-MIS 

 SHG Bank Linkage Portal 

 Other Useful Links 

 RTI Act 
 Parliament Q   A 
 Forums 
 FAQ 
 Multimedia 
 Resources 

 Page last updated on: 20/03/18   

 Visitors Counter:  , Since: March, 2015 

 Terms   Conditions 
 Privacy Policy 
 Copyright Policy 
 Hyperlink Policy 
 Disclaimer 
 Accessibility Statement 
 Contact CEO 

 Contact Us 
 National Rural Livelihoods Mission (NRLM) 
 6th Floor, Hotel Samrat Kautilya Marg, Chanakyapuri 
 New Delhi - 110021 
 Phone: 011 24122947 

 Contents provided by NRLM - MORD, Government of India. 

 Back to Top 

 Alternate text for js
 WHOLE PROJECT MARK