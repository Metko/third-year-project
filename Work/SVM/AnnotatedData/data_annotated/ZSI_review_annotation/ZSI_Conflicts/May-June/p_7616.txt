PROJECT NAME: Web-COSI
PROJECT SOURCE: Consumer-related projects
---------------------------------------
NEW PAGE: 

Web-COSI | Web-COmmunities for Statistics for Social Innovation
===============================

Web-COSI | Web-COmmunities for Statistics for Social Innovation

 Home 
 About 

 About Web-COSI 
 Project Consortium 

 Istat 
 OECD 
 Lunaria 
 i-genius 

 Advisory Board 
 Work Plan 
 Outputs and Documentation 

 2014 
 2015 
 2016 

 News 
 Events 
 Digital Initiatives 
 Wiki of Progress Stat 
 Reports 

 Reports 
 Policy Briefs 
 Deliverable Reports 

 Media 

 Presentations 
 Videos 
 Posters and Flyers 
 Brochure 
 Photo Gallery 
 Short Film 
 Useful Links 

 Contact Us 
 Navigation 

 Statistics for Everyone 

 Latest News 

 Web-COSI Presents at the Q2016 Conference in Madrid 

								by Web-COSI on July 19, 2016									 

										Web-COSI recently presented at the Q2016 Conference, Madrid 1-3 June - an important biannual event supported by Eurostat to present and discuss the progress and development of quality in official statistics. During the panel session  Big Data Oriented Systems , Donatella Fazio, the Web-COSI scientific coordinator, illustrated the paper  Data ecosystem:   

 Last Event 
Final Conference 
OECD, Paris, 3/12/2015 

 Beyond GDP   Statistics for Everyone  represents the Final Conference of the European Union Web-COSI project. It highlights the ground breaking findings of the study. To register and find out more,  click here  

 Wiki of Progress Stat 

 The Web-COSI data portal (hosted on  wikiprogress platform ) is an open knowledge base of well-being and sustainability projects from around the world. We call on everyone to register their organisation and add any initiatives, data and reports that you would like to share. To do so, visit:  http://wikiprogress.org 

 Wikiprogress Platform 

 Project Partners 

 Follow Us   

 Search 

 Web-COSI Newsletter   Register to receive updates and invites to Web-COSI activities 
 Click here to register  
 Web-COSI   Web-COSI is a grassroots co-ordination action project with the general objective to improve the engagement of citizens and society at large with statistics in the area of new measures of societal progress and well-being. Under the mantra  Statistics for Everyone , Web-COSI explores innovative ways to bring the production, promotion, access and engagement with statistics to life.  
 Previous Events Beyond GDP - Statistics for Everyone | Web-COSI Final Conference 03/12/2015 EU Policy Seminar on the Usage of Data for Driving Social Entrepreneurship 07/07/2015 EU Social Entrepreneur Focus Groups 18/06/2015 - 28/08/2015 Workshop: Well-being, plausibly 21/04/2015 Workshop on the Usage of Data for Driving Social Entrepreneurship 19/02/2015 Using Technology to Engage Citizens with Well-being Statistics - Perspectives from Official Statistics and Government - Workshop 27/10/2014 Using Technology to Engage Citizens with Well-being Statistics - Workshop  18/09/2014 Paris Webinar: Civil society engagement in well-being statistics: good practices from Italy 20/06/2014 Making data more accessible for society at large: the role of open data, communication and technology 11/06/2014 - 24/06/2014 Online Discussion: Engaging citizens in well-being and progress statistics 22/04/2014 - 30/04/2014 CAPS | Impact4You IA4SI   

 Back to Top Privacy Policy | Terms   Conditions | Cookies Policy | Sitemap 
LEGAL NOTICE: The views expressed on this website are the sole responsibility of the author and do not necessarily reflect the views of the European Commission. Web-COSI website uses cookies to distinguish you from other users of our website. This helps Web-COSI to provide you with a good experience when you browse the Web-COSI website and also allows Web-COSI to improve the website. To read our Cookies Policy, click here:  http://www.webcosi.eu/about/cookies-policy/ Copyright   2016  Web-COSI All Rights Reserved 

Web-COSI | Web-COmmunities for Statistics for Social Innovation
===============================

Web-COSI | Web-COmmunities for Statistics for Social Innovation

 Home 
 About 

 About Web-COSI 
 Project Consortium 

 Istat 
 OECD 
 Lunaria 
 i-genius 

 Advisory Board 
 Work Plan 
 Outputs and Documentation 

 2014 
 2015 
 2016 

 News 
 Events 
 Digital Initiatives 
 Wiki of Progress Stat 
 Reports 

 Reports 
 Policy Briefs 
 Deliverable Reports 

 Media 

 Presentations 
 Videos 
 Posters and Flyers 
 Brochure 
 Photo Gallery 
 Short Film 
 Useful Links 

 Contact Us 
 Navigation 

 Statistics for Everyone 

 Latest News 

 Web-COSI Presents at the Q2016 Conference in Madrid 

								by Web-COSI on July 19, 2016									 

										Web-COSI recently presented at the Q2016 Conference, Madrid 1-3 June - an important biannual event supported by Eurostat to present and discuss the progress and development of quality in official statistics. During the panel session  Big Data Oriented Systems , Donatella Fazio, the Web-COSI scientific coordinator, illustrated the paper  Data ecosystem:   

 Last Event 
Final Conference 
OECD, Paris, 3/12/2015 

 Beyond GDP   Statistics for Everyone  represents the Final Conference of the European Union Web-COSI project. It highlights the ground breaking findings of the study. To register and find out more,  click here  

 Wiki of Progress Stat 

 The Web-COSI data portal (hosted on  wikiprogress platform ) is an open knowledge base of well-being and sustainability projects from around the world. We call on everyone to register their organisation and add any initiatives, data and reports that you would like to share. To do so, visit:  http://wikiprogress.org 

 Wikiprogress Platform 

 Project Partners 

 Follow Us   

 Search 

 Web-COSI Newsletter   Register to receive updates and invites to Web-COSI activities 
 Click here to register  
 Web-COSI   Web-COSI is a grassroots co-ordination action project with the general objective to improve the engagement of citizens and society at large with statistics in the area of new measures of societal progress and well-being. Under the mantra  Statistics for Everyone , Web-COSI explores innovative ways to bring the production, promotion, access and engagement with statistics to life.  
 Previous Events Beyond GDP - Statistics for Everyone | Web-COSI Final Conference 03/12/2015 EU Policy Seminar on the Usage of Data for Driving Social Entrepreneurship 07/07/2015 EU Social Entrepreneur Focus Groups 18/06/2015 - 28/08/2015 Workshop: Well-being, plausibly 21/04/2015 Workshop on the Usage of Data for Driving Social Entrepreneurship 19/02/2015 Using Technology to Engage Citizens with Well-being Statistics - Perspectives from Official Statistics and Government - Workshop 27/10/2014 Using Technology to Engage Citizens with Well-being Statistics - Workshop  18/09/2014 Paris Webinar: Civil society engagement in well-being statistics: good practices from Italy 20/06/2014 Making data more accessible for society at large: the role of open data, communication and technology 11/06/2014 - 24/06/2014 Online Discussion: Engaging citizens in well-being and progress statistics 22/04/2014 - 30/04/2014 CAPS | Impact4You IA4SI   

 Back to Top Privacy Policy | Terms   Conditions | Cookies Policy | Sitemap 
LEGAL NOTICE: The views expressed on this website are the sole responsibility of the author and do not necessarily reflect the views of the European Commission. Web-COSI website uses cookies to distinguish you from other users of our website. This helps Web-COSI to provide you with a good experience when you browse the Web-COSI website and also allows Web-COSI to improve the website. To read our Cookies Policy, click here:  http://www.webcosi.eu/about/cookies-policy/ Copyright   2016  Web-COSI All Rights Reserved 

Web-COSI | Web-COmmunities for Statistics for Social Innovation
===============================

Web-COSI | Web-COmmunities for Statistics for Social Innovation

 Home 
 About 

 About Web-COSI 
 Project Consortium 

 Istat 
 OECD 
 Lunaria 
 i-genius 

 Advisory Board 
 Work Plan 
 Outputs and Documentation 

 2014 
 2015 
 2016 

 News 
 Events 
 Digital Initiatives 
 Wiki of Progress Stat 
 Reports 

 Reports 
 Policy Briefs 
 Deliverable Reports 

 Media 

 Presentations 
 Videos 
 Posters and Flyers 
 Brochure 
 Photo Gallery 
 Short Film 
 Useful Links 

 Contact Us 
 Navigation 

 Statistics for Everyone 

 Latest News 

 Web-COSI Presents at the Q2016 Conference in Madrid 

								by Web-COSI on July 19, 2016									 

										Web-COSI recently presented at the Q2016 Conference, Madrid 1-3 June - an important biannual event supported by Eurostat to present and discuss the progress and development of quality in official statistics. During the panel session  Big Data Oriented Systems , Donatella Fazio, the Web-COSI scientific coordinator, illustrated the paper  Data ecosystem:   

 Last Event 
Final Conference 
OECD, Paris, 3/12/2015 

 Beyond GDP   Statistics for Everyone  represents the Final Conference of the European Union Web-COSI project. It highlights the ground breaking findings of the study. To register and find out more,  click here  

 Wiki of Progress Stat 

 The Web-COSI data portal (hosted on  wikiprogress platform ) is an open knowledge base of well-being and sustainability projects from around the world. We call on everyone to register their organisation and add any initiatives, data and reports that you would like to share. To do so, visit:  http://wikiprogress.org 

 Wikiprogress Platform 

 Project Partners 

 Follow Us   

 Search 

 Web-COSI Newsletter   Register to receive updates and invites to Web-COSI activities 
 Click here to register  
 Web-COSI   Web-COSI is a grassroots co-ordination action project with the general objective to improve the engagement of citizens and society at large with statistics in the area of new measures of societal progress and well-being. Under the mantra  Statistics for Everyone , Web-COSI explores innovative ways to bring the production, promotion, access and engagement with statistics to life.  
 Previous Events Beyond GDP - Statistics for Everyone | Web-COSI Final Conference 03/12/2015 EU Policy Seminar on the Usage of Data for Driving Social Entrepreneurship 07/07/2015 EU Social Entrepreneur Focus Groups 18/06/2015 - 28/08/2015 Workshop: Well-being, plausibly 21/04/2015 Workshop on the Usage of Data for Driving Social Entrepreneurship 19/02/2015 Using Technology to Engage Citizens with Well-being Statistics - Perspectives from Official Statistics and Government - Workshop 27/10/2014 Using Technology to Engage Citizens with Well-being Statistics - Workshop  18/09/2014 Paris Webinar: Civil society engagement in well-being statistics: good practices from Italy 20/06/2014 Making data more accessible for society at large: the role of open data, communication and technology 11/06/2014 - 24/06/2014 Online Discussion: Engaging citizens in well-being and progress statistics 22/04/2014 - 30/04/2014 CAPS | Impact4You IA4SI   

 Back to Top Privacy Policy | Terms   Conditions | Cookies Policy | Sitemap 
LEGAL NOTICE: The views expressed on this website are the sole responsibility of the author and do not necessarily reflect the views of the European Commission. Web-COSI website uses cookies to distinguish you from other users of our website. This helps Web-COSI to provide you with a good experience when you browse the Web-COSI website and also allows Web-COSI to improve the website. To read our Cookies Policy, click here:  http://www.webcosi.eu/about/cookies-policy/ Copyright   2016  Web-COSI All Rights Reserved 

Web-COSI | Web-COmmunities for Statistics for Social Innovation
===============================

Web-COSI | Web-COmmunities for Statistics for Social Innovation

 Home 
 About 

 About Web-COSI 
 Project Consortium 

 Istat 
 OECD 
 Lunaria 
 i-genius 

 Advisory Board 
 Work Plan 
 Outputs and Documentation 

 2014 
 2015 
 2016 

 News 
 Events 
 Digital Initiatives 
 Wiki of Progress Stat 
 Reports 

 Reports 
 Policy Briefs 
 Deliverable Reports 

 Media 

 Presentations 
 Videos 
 Posters and Flyers 
 Brochure 
 Photo Gallery 
 Short Film 
 Useful Links 

 Contact Us 
 Navigation 

 Statistics for Everyone 

 Latest News 

 Web-COSI Presents at the Q2016 Conference in Madrid 

								by Web-COSI on July 19, 2016									 

										Web-COSI recently presented at the Q2016 Conference, Madrid 1-3 June - an important biannual event supported by Eurostat to present and discuss the progress and development of quality in official statistics. During the panel session  Big Data Oriented Systems , Donatella Fazio, the Web-COSI scientific coordinator, illustrated the paper  Data ecosystem:   

 Last Event 
Final Conference 
OECD, Paris, 3/12/2015 

 Beyond GDP   Statistics for Everyone  represents the Final Conference of the European Union Web-COSI project. It highlights the ground breaking findings of the study. To register and find out more,  click here  

 Wiki of Progress Stat 

 The Web-COSI data portal (hosted on  wikiprogress platform ) is an open knowledge base of well-being and sustainability projects from around the world. We call on everyone to register their organisation and add any initiatives, data and reports that you would like to share. To do so, visit:  http://wikiprogress.org 

 Wikiprogress Platform 

 Project Partners 

 Follow Us   

 Search 

 Web-COSI Newsletter   Register to receive updates and invites to Web-COSI activities 
 Click here to register  
 Web-COSI   Web-COSI is a grassroots co-ordination action project with the general objective to improve the engagement of citizens and society at large with statistics in the area of new measures of societal progress and well-being. Under the mantra  Statistics for Everyone , Web-COSI explores innovative ways to bring the production, promotion, access and engagement with statistics to life.  
 Previous Events Beyond GDP - Statistics for Everyone | Web-COSI Final Conference 03/12/2015 EU Policy Seminar on the Usage of Data for Driving Social Entrepreneurship 07/07/2015 EU Social Entrepreneur Focus Groups 18/06/2015 - 28/08/2015 Workshop: Well-being, plausibly 21/04/2015 Workshop on the Usage of Data for Driving Social Entrepreneurship 19/02/2015 Using Technology to Engage Citizens with Well-being Statistics - Perspectives from Official Statistics and Government - Workshop 27/10/2014 Using Technology to Engage Citizens with Well-being Statistics - Workshop  18/09/2014 Paris Webinar: Civil society engagement in well-being statistics: good practices from Italy 20/06/2014 Making data more accessible for society at large: the role of open data, communication and technology 11/06/2014 - 24/06/2014 Online Discussion: Engaging citizens in well-being and progress statistics 22/04/2014 - 30/04/2014 CAPS | Impact4You IA4SI   

 Back to Top Privacy Policy | Terms   Conditions | Cookies Policy | Sitemap 
LEGAL NOTICE: The views expressed on this website are the sole responsibility of the author and do not necessarily reflect the views of the European Commission. Web-COSI website uses cookies to distinguish you from other users of our website. This helps Web-COSI to provide you with a good experience when you browse the Web-COSI website and also allows Web-COSI to improve the website. To read our Cookies Policy, click here:  http://www.webcosi.eu/about/cookies-policy/ Copyright   2016  Web-COSI All Rights Reserved
 WHOLE PROJECT MARK