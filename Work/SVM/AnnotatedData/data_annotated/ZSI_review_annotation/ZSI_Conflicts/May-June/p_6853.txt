PROJECT NAME: Zotero
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

Zotero | Your personal research assistant
===============================

Zotero | Your personal research assistant

 Zotero 
 Groups 
 Documentation 
 Forums 
 Get Involved 
 Log In 
 Upgrade Storage 

 Groups 
 Documentation 
 Forums 
 Get Involved 
 Log In 
 Upgrade Storage 

 Your personal research assistant 
 Zotero is a free, easy-to-use tool to help you   collect, organize, cite, and share research. 
 Download 
 Available for Mac, Windows, and Linux 

 Meet Zotero. 

 Collect with a click. 
 Zotero is the only software that automatically senses research on the web. Need an article from JSTOR or a preprint from arXiv.org? A news story from the New York Times or a book from a library? Zotero has you covered, everywhere. 

 Organize your way. 
 Zotero helps you organize your research any way you want. You can sort items into collections and tag them with keywords. Or create saved searches that automatically fill with relevant materials as you work. 

 Cite in style. 
 Zotero instantly creates references and bibliographies for any text editor, and directly inside Word and LibreOffice. With support for over 8,000 citation styles, you can format your work to match any style guide or publication. 

 Stay in sync. 
 Zotero automatically synchronizes your data across your devices, keeping your notes, files, and bibliographic data seamlessly up to date. Even without Zotero installed, you can always access your research from any web browser. 

 Collaborate freely. 
 Zotero lets you freely collaborate with fellow researchers and distribute class materials to your students. With no restrictions on membership, you can share your Zotero library in public or in private. 

 Ready to try Zotero? 
 Download 

 Documentation 
 Forums 
 Blog 
 Privacy 
 Get Involved 
 Developers 
 Jobs 

 Follow us 

 Zotero is a project of the  Corporation for Digital Scholarship  and the  Roy Rosenzweig Center for History and New Media . It was initially funded by the   Andrew W. Mellon Foundation , the   Institute of Museum and Library Services , and the  Alfred P. Sloan Foundation. 

 Zotero is a free and open-source reference management software to manage bibliographic data and related research materials. 

Zotero is a free and open-source reference management software to manage bibliographic data and related research materials.
 WHOLE PROJECT MARK