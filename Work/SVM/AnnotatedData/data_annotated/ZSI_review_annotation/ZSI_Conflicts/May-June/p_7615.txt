PROJECT NAME: CAPSSI
PROJECT SOURCE: Consumer-related projects
---------------------------------------
NEW PAGE: 

CAPSSI | Sustainability   Digital Social Innovation
===============================

CAPSSI | Sustainability   Digital Social Innovation

 Twitter Facebook Linkedin Youtube   

 About  

 About CAPSSI 
 CAPS Bytes (Newsletter) 
 CAPSSI Videos 

 CAPS Projects 
 News 
 Events  

 Upcoming events 
 ChiC events 
 Events attended by ChiC 

 CAPS Outcomes 
 Contact 
  CAPS Community 

 SAVE  

 THE  

 DATE  

 6-7 June 2018 | Rome, Italy  

  Digital transformation for a better society  

 ENDORSE  

 IT  

 HERE  

 I endorse it !

 DSI 
Manifesto 

 Collective 
awareness  

 Sustainability  

 Social innovation  

 Enabling new forms of social innovation  

 Latest News 

 + 

 airACT: an app to check the air we breathe 

 March 13, 2018 March 13, 2018 ,  CAPS ,  CAPSSI Projects ,  Captor   

 CAPTOR project launches airACT, a new application that informs about air pollutants in real time and helps to act...   

						Read more 

 + 

 Report: Workshop Connected Technologies for Social Good 

 March 5, 2018 March 5, 2018 ,  CAPS ,  CAPSSI ,  ChiC ,  CHIC attended events ,  Event ,  workshop   

 On  February 14-15 2018, the ChiC consortium organised a two day workshop in Brussels with the COST community in...   

						Read more 

 + 

 Ushahidi COMRADES platform monitors the Kenyan Elections 

 February 27, 2018 February 27, 2018 ,  CAPS ,  CAPSSI Projects ,  comrades   

 The crowdsourcing technology company Ushahidi used their platform to monitor Kenya s General Election held throughout the period ranging from...   

						Read more 

 + 

 Transport policies to face Air Pollution and Climate Change   CAPTOR Workshop report 

 February 23, 2018 February 23, 2018 ,  CAPS ,  CAPSSI Projects ,  Captor ,  workshop   

 Transport policies to face Air Pollution and Climate Change   Workshop conclusions in the III Catalan Environmental Congress. More...   

						Read more 

 + 

 2nd CAPTOR Newsletter now available 

 February 23, 2018 February 23, 2018 ,  CAPS ,  CAPSSI ,  CAPSSI Projects ,  Captor ,  Newsletter   

 The 2nd CAPTOR Newsletter highlights their work with citizens to fight Ozone Pollution. It provides insights into the highlights...   

						Read more 

 + 

 HackAIR newsletter now available 

 February 13, 2018 February 13, 2018 ,  CAPS ,  CAPSSI ,  hackAIR ,  Newsletter   

 How clean is the air you breathe? The hackAIR app shows you local air quality and lets you reduce...   

						Read more 

 + 

 ChiC presence at EmpoderaLIVE 2017 

 January 8, 2018 January 8, 2018 ,  CAPS ,  CAPSSI ,  ChiC ,  CHIC attended events ,  Event   

 EmpoderaLive in Malaga on September 11th and 12th, 2017 EmpoderaLIVE is an international event which year after year has...   

						Read more 

 + 

 ChiC in the Blockchains for Social Good conference 

 December 19, 2017 December 19, 2017 ,  CAPS ,  ChiC ,  CHIC attended events ,  Event   

 Digital Social-good innovators bet on blockchains to solve big problems On the 15th December, ChiC attended the  Blockchains for...   

						Read more 

 + 

 New release of the MAZI Toolkit 

 December 18, 2017 December 18, 2017 ,  CAPS ,  CAPSSI ,  CAPSSI Projects ,  mazi   

 In November 2017, version 2 of the MAZI Do-It-Yourself wireless networking toolkit was updated on the MAZI project website....   

						Read more 

 + 

 DSISCALE is kicking off in January! 

 December 14, 2017 December 14, 2017 ,  CAPS ,  dsi4eu   

 Following on from the successful DSI4EU project, Nesta and the Waag Society are excited to be working with five...   

						Read more 

 Let s get in contact ! 

  Subscribe to the CAPSSI newsletter to be connected to digital social innovation in Europe and beyond: 

 Leave this field empty if you're human:  

 Upcoming Events 

                                   Posterboard                 Agenda               Day               Month               Week               Posterboard               Stream                          April   September 2018   Apr   Sep 2018                             Apr   11   Wed   2018                 Apr   12   Thu   2018                Co-creation, big data, and the future of digitally enhanced public spaces       Apr 11   Apr 12  all-day                  00:00          Tickets        The final event of the COST Action TU1306 CyberParks is an international and interdisciplinary scientific conference that invites both scholars and practitioners to reflect on how information and communication technologies (ICT) re-configurate the production and use of public spaces. Thinking within the conceptual nexus between the notions of place, technology, and society, the CyberParks network reflects on the innovations, potentials,[...]         Berlin, Germany                          Apr   20   Fri   2018              CAPTOR building workshop       Apr 20 @ 10:00   20:00                 10:00          Tickets        Register now for CAPTOR building workshop! Citizen Science meets do it yourself. Now it is possible to build an ozone measuring instrument yourself. The finished measuring devices can be hung by some hobbyists themselves, provided that the necessary conditions are met. The number of measuring devices to be built is limited. The combination of DIY and scientific curiosity is perfect[...]         Vienna, Autria                          May   17   Thu   2018                 May   18   Fri   2018                Information Ethics Roundtable       May 17   May 18  all-day                  00:00          Tickets        The Information Ethics Roundtable (held annually since 2003) is a yearly conference that brings together researchers from disciplines such as philosophy, information science, communications, public administration, anthropology, and law to discuss ethical issues such as information privacy, intellectual property, intellectual freedom, and censorship. The theme for the Information Ethics Roundtable 2018 is Surveillance, Algorithms, and Digital Culture and the roundtable[...]         Copenhagen, Denmark                          Jun   6   Wed   2018                 Jun   7   Thu   2018                Digital Social Innovation fair 2018       Jun 6   Jun 7  all-day                  00:00          Tickets        The DSI Fair will come back to Rome, 6-7 June 2018, under the auspices of the Municipality of Rome. There is a dedicated website for the event where you can find all the necessary information about the event: dsifair.eu           Rome, Italy                          Jun   11   Mon   2018                 Jun   14   Thu   2018                9th International Sustainability Transitions Conference       Jun 11   Jun 14  all-day                  00:00          Tickets        The Sustainable Consumption Institute (SCI) and Manchester Institute of Innovation Research (MIOIR) will host the 9th International Sustainability Transitions Conference at the University of Manchester. Building on past events organised under the auspices of the Sustainability Transitions Research Network (STRN), the conference will provide an opportunity for scholars and stakeholders to exchange ideas coming from the latest research in the[...]         Manchester, UK                          Sep   3   Mon   2018                 Sep   5   Wed   2018                International Social Innovation Research Conference (ISIRC 2108)       Sep 3   Sep 5  all-day                  00:00          Tickets        ISIRC is the world s leading  interdisciplinary social innovation research conference. The conference brings together scholars from across the globe to discuss social innovation in a variety of perspectives. ISIRC 2017 is hosted by the Ruprecht-Karls-University Heidelberg, Center for Social Investment and Innovation in cooperation with the Westphalian University Gelsenkirchen, Institute for Work and Technology. The conference will take place at[...]         Heidelberg, Germany                          Sep   13   Thu   2018              Next Generation Internet Forum 2018       Sep 13  all-day                  00:00          Tickets        The next edition of the NGI Forum will be held in 2018 on 13th September 2018 in Porto, Portugal. In line with the first edition, which was held in September 2017, the main goal for this event is to ensure the involvement of the best Internet researchers and innovators to address technological opportunities arising from cross-links and advances in various[...]         Porto, Portugal                              April   September 2018   Apr   Sep 2018                            Subscribe               Add to Timely Calendar           Add to Google           Add to Outlook           Add to Apple Calendar           Add to other calendar           Export to XML                  

 CAPSSI Twitter Channel 

 Tweets by CAPSSIEU   

 CAPS Projects 

 + 

 Grassroots Wavelengths 

 Grassroots Wavelengths Grassroots Wavelengths is a CAPS init...   

 + 

 Share4Rare 

 Share4Rare: Social media platform dedicated to rare diseases...   

 + 

 PTwist 

 PTwist: An open platform for plastics lifecycle awareness, m...   

 + 

 Families_Share 

 Families_Share   Socialising and sharing time for work/ li...   

 1 2 3 11 

 Collective Awareness Platforms for Sustainability   Social Innovation 

 Collective Awareness Platforms for Sustainability and Social Innovation (CAPSSI) are ICT systems leveraging the emerging  network effect  by combining open online social media, distributed knowledge creation and data from real environments ( Internet of Things ) in order to create awareness of problems and possible solutions requesting collective efforts, enabling new forms of social innovation. 

 About   Collective Awareness Platforms for Sustainability and Social Innovation (CAPSSI) is manage by the Coordination and Support Action CHiC project which is funded under Grant Agreement No. 687686 by the European Commission. 

 Main menu About 

 About CAPSSI 
 CAPS Bytes (Newsletter) 
 CAPSSI Videos 

 CAPS Projects 
 News 
 Events 

 Upcoming events 
 ChiC events 
 Events attended by ChiC 

 CAPS Outcomes 
 Contact 
 CAPS Community 

 Let s get in contact ! 
  Subscribe to the CAPSSI newsletter to be connected to digital social innovation in Europe and beyond: 

 Leave this field empty if you're human:  

 Follow us   

 2018 - ChiC Project -  Privacy Policy  -  Terms   Conditions   

 Log in with your credentials 

 or 

                        Create an account                     

 Remember me             
 Lost your password? 

 Forgot your details? 

 I remember my details 

 Create Account 

 This site is free and open to everyone, but our registered users get extra privileges like commenting, and voting.  

 Username 

 Password 

  Remember Me 

 Register 

CAPSSI | Sustainability   Digital Social Innovation
===============================

CAPSSI | Sustainability   Digital Social Innovation

 Twitter Facebook Linkedin Youtube   

 About  

 About CAPSSI 
 CAPS Bytes (Newsletter) 
 CAPSSI Videos 

 CAPS Projects 
 News 
 Events  

 Upcoming events 
 ChiC events 
 Events attended by ChiC 

 CAPS Outcomes 
 Contact 
  CAPS Community 

 SAVE  

 THE  

 DATE  

 6-7 June 2018 | Rome, Italy  

  Digital transformation for a better society  

 ENDORSE  

 IT  

 HERE  

 I endorse it !

 DSI 
Manifesto 

 Collective 
awareness  

 Sustainability  

 Social innovation  

 Enabling new forms of social innovation  

 Latest News 

 + 

 airACT: an app to check the air we breathe 

 March 13, 2018 March 13, 2018 ,  CAPS ,  CAPSSI Projects ,  Captor   

 CAPTOR project launches airACT, a new application that informs about air pollutants in real time and helps to act...   

						Read more 

 + 

 Report: Workshop Connected Technologies for Social Good 

 March 5, 2018 March 5, 2018 ,  CAPS ,  CAPSSI ,  ChiC ,  CHIC attended events ,  Event ,  workshop   

 On  February 14-15 2018, the ChiC consortium organised a two day workshop in Brussels with the COST community in...   

						Read more 

 + 

 Ushahidi COMRADES platform monitors the Kenyan Elections 

 February 27, 2018 February 27, 2018 ,  CAPS ,  CAPSSI Projects ,  comrades   

 The crowdsourcing technology company Ushahidi used their platform to monitor Kenya s General Election held throughout the period ranging from...   

						Read more 

 + 

 Transport policies to face Air Pollution and Climate Change   CAPTOR Workshop report 

 February 23, 2018 February 23, 2018 ,  CAPS ,  CAPSSI Projects ,  Captor ,  workshop   

 Transport policies to face Air Pollution and Climate Change   Workshop conclusions in the III Catalan Environmental Congress. More...   

						Read more 

 + 

 2nd CAPTOR Newsletter now available 

 February 23, 2018 February 23, 2018 ,  CAPS ,  CAPSSI ,  CAPSSI Projects ,  Captor ,  Newsletter   

 The 2nd CAPTOR Newsletter highlights their work with citizens to fight Ozone Pollution. It provides insights into the highlights...   

						Read more 

 + 

 HackAIR newsletter now available 

 February 13, 2018 February 13, 2018 ,  CAPS ,  CAPSSI ,  hackAIR ,  Newsletter   

 How clean is the air you breathe? The hackAIR app shows you local air quality and lets you reduce...   

						Read more 

 + 

 ChiC presence at EmpoderaLIVE 2017 

 January 8, 2018 January 8, 2018 ,  CAPS ,  CAPSSI ,  ChiC ,  CHIC attended events ,  Event   

 EmpoderaLive in Malaga on September 11th and 12th, 2017 EmpoderaLIVE is an international event which year after year has...   

						Read more 

 + 

 ChiC in the Blockchains for Social Good conference 

 December 19, 2017 December 19, 2017 ,  CAPS ,  ChiC ,  CHIC attended events ,  Event   

 Digital Social-good innovators bet on blockchains to solve big problems On the 15th December, ChiC attended the  Blockchains for...   

						Read more 

 + 

 New release of the MAZI Toolkit 

 December 18, 2017 December 18, 2017 ,  CAPS ,  CAPSSI ,  CAPSSI Projects ,  mazi   

 In November 2017, version 2 of the MAZI Do-It-Yourself wireless networking toolkit was updated on the MAZI project website....   

						Read more 

 + 

 DSISCALE is kicking off in January! 

 December 14, 2017 December 14, 2017 ,  CAPS ,  dsi4eu   

 Following on from the successful DSI4EU project, Nesta and the Waag Society are excited to be working with five...   

						Read more 

 Let s get in contact ! 

  Subscribe to the CAPSSI newsletter to be connected to digital social innovation in Europe and beyond: 

 Leave this field empty if you're human:  

 Upcoming Events 

                                   Posterboard                 Agenda               Day               Month               Week               Posterboard               Stream                          April   September 2018   Apr   Sep 2018                             Apr   11   Wed   2018                 Apr   12   Thu   2018                Co-creation, big data, and the future of digitally enhanced public spaces       Apr 11   Apr 12  all-day                  00:00          Tickets        The final event of the COST Action TU1306 CyberParks is an international and interdisciplinary scientific conference that invites both scholars and practitioners to reflect on how information and communication technologies (ICT) re-configurate the production and use of public spaces. Thinking within the conceptual nexus between the notions of place, technology, and society, the CyberParks network reflects on the innovations, potentials,[...]         Berlin, Germany                          Apr   20   Fri   2018              CAPTOR building workshop       Apr 20 @ 10:00   20:00                 10:00          Tickets        Register now for CAPTOR building workshop! Citizen Science meets do it yourself. Now it is possible to build an ozone measuring instrument yourself. The finished measuring devices can be hung by some hobbyists themselves, provided that the necessary conditions are met. The number of measuring devices to be built is limited. The combination of DIY and scientific curiosity is perfect[...]         Vienna, Autria                          Apr   21   Sat   2018                 Apr   26   Thu   2018                MAZI and MakingSense workshop at ACM CHI 2018       Apr 21   Apr 26  all-day                  00:00          Tickets        This workshop will encourage cross-disciplinary discussions through presentation of case studies, experiences and perspectives. Themes include participatory design, maker culture, Do-It-Yourself approaches, social innovation, democratic design, research methods and frameworks, user and participant perspectives, creativity and materiality, grassroots activities and activism, and research  in the wild . The organisers are the CAPS projects Making Sense and MAZI which are developing approaches[...]         Montr al, Canada                          May   8   Tue   2018              CAPSELLA Workshop: Harvesting Open and Digital Innovation for Sustainable Agriculture and Food Systems       May 8  all-day                  00:00          Tickets        The CAPSELLA Consortium is pleased to invite you to its digital innovation event. Organised in Milan on May the 8th 2018, during the Food Week, with the leadership of Municipality of Milan, OPERA Italian Observatory for Agrobiodiversity, and leading minds from the EU CAPSELLA project, this workshop gives you a chance to hear from Europe s most creative scientists in open[...]         Milan, Italy                          May   17   Thu   2018                 May   18   Fri   2018                Information Ethics Roundtable       May 17   May 18  all-day                  00:00          Tickets        The Information Ethics Roundtable (held annually since 2003) is a yearly conference that brings together researchers from disciplines such as philosophy, information science, communications, public administration, anthropology, and law to discuss ethical issues such as information privacy, intellectual property, intellectual freedom, and censorship. The theme for the Information Ethics Roundtable 2018 is Surveillance, Algorithms, and Digital Culture and the roundtable[...]         Copenhagen, Denmark                          Jun   6   Wed   2018                 Jun   7   Thu   2018                Digital Social Innovation fair 2018       Jun 6   Jun 7  all-day                  00:00          Tickets        The DSI Fair will come back to Rome, 6-7 June 2018, under the auspices of the Municipality of Rome. There is a dedicated website for the event where you can find all the necessary information about the event: dsifair.eu           Rome, Italy                          Jun   11   Mon   2018                 Jun   14   Thu   2018                9th International Sustainability Transitions Conference       Jun 11   Jun 14  all-day                  00:00          Tickets        The Sustainable Consumption Institute (SCI) and Manchester Institute of Innovation Research (MIOIR) will host the 9th International Sustainability Transitions Conference at the University of Manchester. Building on past events organised under the auspices of the Sustainability Transitions Research Network (STRN), the conference will provide an opportunity for scholars and stakeholders to exchange ideas coming from the latest research in the[...]         Manchester, UK                          Sep   3   Mon   2018                 Sep   5   Wed   2018                International Social Innovation Research Conference (ISIRC 2108)       Sep 3   Sep 5  all-day                  00:00          Tickets        ISIRC is the world s leading  interdisciplinary social innovation research conference. The conference brings together scholars from across the globe to discuss social innovation in a variety of perspectives. ISIRC 2017 is hosted by the Ruprecht-Karls-University Heidelberg, Center for Social Investment and Innovation in cooperation with the Westphalian University Gelsenkirchen, Institute for Work and Technology. The conference will take place at[...]         Heidelberg, Germany                          Sep   13   Thu   2018              Next Generation Internet Forum 2018       Sep 13  all-day                  00:00          Tickets        The next edition of the NGI Forum will be held in 2018 on 13th September 2018 in Porto, Portugal. In line with the first edition, which was held in September 2017, the main goal for this event is to ensure the involvement of the best Internet researchers and innovators to address technological opportunities arising from cross-links and advances in various[...]         Porto, Portugal                              April   September 2018   Apr   Sep 2018                            Subscribe               Add to Timely Calendar           Add to Google           Add to Outlook           Add to Apple Calendar           Add to other calendar           Export to XML                  

 CAPSSI Twitter Channel 

 Tweets by CAPSSIEU   

 CAPS Projects 

 + 

 Grassroots Wavelengths 

 Grassroots Wavelengths Grassroots Wavelengths is a CAPS init...   

 + 

 Share4Rare 

 Share4Rare: Social media platform dedicated to rare diseases...   

 + 

 PTwist 

 PTwist: An open platform for plastics lifecycle awareness, m...   

 + 

 Families_Share 

 Families_Share   Socialising and sharing time for work/ li...   

 1 2 3 11 

 Collective Awareness Platforms for Sustainability   Social Innovation 

 Collective Awareness Platforms for Sustainability and Social Innovation (CAPSSI) are ICT systems leveraging the emerging  network effect  by combining open online social media, distributed knowledge creation and data from real environments ( Internet of Things ) in order to create awareness of problems and possible solutions requesting collective efforts, enabling new forms of social innovation. 

 About   Collective Awareness Platforms for Sustainability and Social Innovation (CAPSSI) is manage by the Coordination and Support Action CHiC project which is funded under Grant Agreement No. 687686 by the European Commission. 

 Main menu About 

 About CAPSSI 
 CAPS Bytes (Newsletter) 
 CAPSSI Videos 

 CAPS Projects 
 News 
 Events 

 Upcoming events 
 ChiC events 
 Events attended by ChiC 

 CAPS Outcomes 
 Contact 
 CAPS Community 

 Let s get in contact ! 
  Subscribe to the CAPSSI newsletter to be connected to digital social innovation in Europe and beyond: 

 Leave this field empty if you're human:  

 Follow us   

 2018 - ChiC Project -  Privacy Policy  -  Terms   Conditions   

 Log in with your credentials 

 or 

                        Create an account                     

 Remember me             
 Lost your password? 

 Forgot your details? 

 I remember my details 

 Create Account 

 This site is free and open to everyone, but our registered users get extra privileges like commenting, and voting.  

 Username 

 Password 

  Remember Me 

 Register 

CAPSSI | Sustainability   Digital Social Innovation
===============================

CAPSSI | Sustainability   Digital Social Innovation

 Twitter Facebook Linkedin Youtube   

 About  

 About CAPSSI 
 CAPS Bytes (Newsletter) 
 CAPSSI Videos 

 CAPS Projects 
 News 
 Events  

 Upcoming events 
 ChiC events 
 Events attended by ChiC 

 CAPS Outcomes 
 Contact 
  CAPS Community 

 SAVE  

 THE  

 DATE  

 6-7 June 2018 | Rome, Italy  

  Digital transformation for a better society  

 ENDORSE  

 IT  

 HERE  

 I endorse it !

 DSI 
Manifesto 

 Collective 
awareness  

 Sustainability  

 Social innovation  

 Enabling new forms of social innovation  

 Latest News 

 + 

 airACT: an app to check the air we breathe 

 March 13, 2018 March 13, 2018 ,  CAPS ,  CAPSSI Projects ,  Captor   

 CAPTOR project launches airACT, a new application that informs about air pollutants in real time and helps to act...   

						Read more 

 + 

 Report: Workshop Connected Technologies for Social Good 

 March 5, 2018 March 5, 2018 ,  CAPS ,  CAPSSI ,  ChiC ,  CHIC attended events ,  Event ,  workshop   

 On  February 14-15 2018, the ChiC consortium organised a two day workshop in Brussels with the COST community in...   

						Read more 

 + 

 Ushahidi COMRADES platform monitors the Kenyan Elections 

 February 27, 2018 February 27, 2018 ,  CAPS ,  CAPSSI Projects ,  comrades   

 The crowdsourcing technology company Ushahidi used their platform to monitor Kenya s General Election held throughout the period ranging from...   

						Read more 

 + 

 Transport policies to face Air Pollution and Climate Change   CAPTOR Workshop report 

 February 23, 2018 February 23, 2018 ,  CAPS ,  CAPSSI Projects ,  Captor ,  workshop   

 Transport policies to face Air Pollution and Climate Change   Workshop conclusions in the III Catalan Environmental Congress. More...   

						Read more 

 + 

 2nd CAPTOR Newsletter now available 

 February 23, 2018 February 23, 2018 ,  CAPS ,  CAPSSI ,  CAPSSI Projects ,  Captor ,  Newsletter   

 The 2nd CAPTOR Newsletter highlights their work with citizens to fight Ozone Pollution. It provides insights into the highlights...   

						Read more 

 + 

 HackAIR newsletter now available 

 February 13, 2018 February 13, 2018 ,  CAPS ,  CAPSSI ,  hackAIR ,  Newsletter   

 How clean is the air you breathe? The hackAIR app shows you local air quality and lets you reduce...   

						Read more 

 + 

 ChiC presence at EmpoderaLIVE 2017 

 January 8, 2018 January 8, 2018 ,  CAPS ,  CAPSSI ,  ChiC ,  CHIC attended events ,  Event   

 EmpoderaLive in Malaga on September 11th and 12th, 2017 EmpoderaLIVE is an international event which year after year has...   

						Read more 

 + 

 ChiC in the Blockchains for Social Good conference 

 December 19, 2017 December 19, 2017 ,  CAPS ,  ChiC ,  CHIC attended events ,  Event   

 Digital Social-good innovators bet on blockchains to solve big problems On the 15th December, ChiC attended the  Blockchains for...   

						Read more 

 + 

 New release of the MAZI Toolkit 

 December 18, 2017 December 18, 2017 ,  CAPS ,  CAPSSI ,  CAPSSI Projects ,  mazi   

 In November 2017, version 2 of the MAZI Do-It-Yourself wireless networking toolkit was updated on the MAZI project website....   

						Read more 

 + 

 DSISCALE is kicking off in January! 

 December 14, 2017 December 14, 2017 ,  CAPS ,  dsi4eu   

 Following on from the successful DSI4EU project, Nesta and the Waag Society are excited to be working with five...   

						Read more 

 Let s get in contact ! 

  Subscribe to the CAPSSI newsletter to be connected to digital social innovation in Europe and beyond: 

 Leave this field empty if you're human:  

 Upcoming Events 

                                   Posterboard                 Agenda               Day               Month               Week               Posterboard               Stream                          April   September 2018   Apr   Sep 2018                             Apr   11   Wed   2018                 Apr   12   Thu   2018                Co-creation, big data, and the future of digitally enhanced public spaces       Apr 11   Apr 12  all-day                  00:00          Tickets        The final event of the COST Action TU1306 CyberParks is an international and interdisciplinary scientific conference that invites both scholars and practitioners to reflect on how information and communication technologies (ICT) re-configurate the production and use of public spaces. Thinking within the conceptual nexus between the notions of place, technology, and society, the CyberParks network reflects on the innovations, potentials,[...]         Berlin, Germany                          Apr   20   Fri   2018              CAPTOR building workshop       Apr 20 @ 10:00   20:00                 10:00          Tickets        Register now for CAPTOR building workshop! Citizen Science meets do it yourself. Now it is possible to build an ozone measuring instrument yourself. The finished measuring devices can be hung by some hobbyists themselves, provided that the necessary conditions are met. The number of measuring devices to be built is limited. The combination of DIY and scientific curiosity is perfect[...]         Vienna, Autria                          Apr   21   Sat   2018                 Apr   26   Thu   2018                MAZI and MakingSense workshop at ACM CHI 2018       Apr 21   Apr 26  all-day                  00:00          Tickets        This workshop will encourage cross-disciplinary discussions through presentation of case studies, experiences and perspectives. Themes include participatory design, maker culture, Do-It-Yourself approaches, social innovation, democratic design, research methods and frameworks, user and participant perspectives, creativity and materiality, grassroots activities and activism, and research  in the wild . The organisers are the CAPS projects Making Sense and MAZI which are developing approaches[...]         Montr al, Canada                          May   8   Tue   2018              CAPSELLA Workshop: Harvesting Open and Digital Innovation for Sustainable Agriculture and Food Systems       May 8  all-day                  00:00          Tickets        The CAPSELLA Consortium is pleased to invite you to its digital innovation event. Organised in Milan on May the 8th 2018, during the Food Week, with the leadership of Municipality of Milan, OPERA Italian Observatory for Agrobiodiversity, and leading minds from the EU CAPSELLA project, this workshop gives you a chance to hear from Europe s most creative scientists in open[...]         Milan, Italy                          May   17   Thu   2018                 May   18   Fri   2018                Information Ethics Roundtable       May 17   May 18  all-day                  00:00          Tickets        The Information Ethics Roundtable (held annually since 2003) is a yearly conference that brings together researchers from disciplines such as philosophy, information science, communications, public administration, anthropology, and law to discuss ethical issues such as information privacy, intellectual property, intellectual freedom, and censorship. The theme for the Information Ethics Roundtable 2018 is Surveillance, Algorithms, and Digital Culture and the roundtable[...]         Copenhagen, Denmark                          Jun   6   Wed   2018                 Jun   7   Thu   2018                Digital Social Innovation fair 2018       Jun 6   Jun 7  all-day                  00:00          Tickets        The DSI Fair will come back to Rome, 6-7 June 2018, under the auspices of the Municipality of Rome. There is a dedicated website for the event where you can find all the necessary information about the event: dsifair.eu           Rome, Italy                          Jun   11   Mon   2018                 Jun   14   Thu   2018                9th International Sustainability Transitions Conference       Jun 11   Jun 14  all-day                  00:00          Tickets        The Sustainable Consumption Institute (SCI) and Manchester Institute of Innovation Research (MIOIR) will host the 9th International Sustainability Transitions Conference at the University of Manchester. Building on past events organised under the auspices of the Sustainability Transitions Research Network (STRN), the conference will provide an opportunity for scholars and stakeholders to exchange ideas coming from the latest research in the[...]         Manchester, UK                          Sep   3   Mon   2018                 Sep   5   Wed   2018                International Social Innovation Research Conference (ISIRC 2108)       Sep 3   Sep 5  all-day                  00:00          Tickets        ISIRC is the world s leading  interdisciplinary social innovation research conference. The conference brings together scholars from across the globe to discuss social innovation in a variety of perspectives. ISIRC 2017 is hosted by the Ruprecht-Karls-University Heidelberg, Center for Social Investment and Innovation in cooperation with the Westphalian University Gelsenkirchen, Institute for Work and Technology. The conference will take place at[...]         Heidelberg, Germany                          Sep   13   Thu   2018              Next Generation Internet Forum 2018       Sep 13  all-day                  00:00          Tickets        The next edition of the NGI Forum will be held in 2018 on 13th September 2018 in Porto, Portugal. In line with the first edition, which was held in September 2017, the main goal for this event is to ensure the involvement of the best Internet researchers and innovators to address technological opportunities arising from cross-links and advances in various[...]         Porto, Portugal                              April   September 2018   Apr   Sep 2018                            Subscribe               Add to Timely Calendar           Add to Google           Add to Outlook           Add to Apple Calendar           Add to other calendar           Export to XML                  

 CAPSSI Twitter Channel 

 Tweets by CAPSSIEU   

 CAPS Projects 

 + 

 Grassroots Wavelengths 

 Grassroots Wavelengths   Highly Networked Grassroots C...   

 + 

 Share4Rare 

 Share4Rare: Social media platform dedicated to rare diseases...   

 + 

 PTwist 

 PTwist: An open platform for plastics lifecycle awareness, m...   

 + 

 Families_Share 

 Families_Share   Socialising and sharing time for work/ li...   

 1 2 3 11 

 Collective Awareness Platforms for Sustainability   Social Innovation 

 Collective Awareness Platforms for Sustainability and Social Innovation (CAPSSI) are ICT systems leveraging the emerging  network effect  by combining open online social media, distributed knowledge creation and data from real environments ( Internet of Things ) in order to create awareness of problems and possible solutions requesting collective efforts, enabling new forms of social innovation. 

 About   Collective Awareness Platforms for Sustainability and Social Innovation (CAPSSI) is manage by the Coordination and Support Action CHiC project which is funded under Grant Agreement No. 687686 by the European Commission. 

 Main menu About 

 About CAPSSI 
 CAPS Bytes (Newsletter) 
 CAPSSI Videos 

 CAPS Projects 
 News 
 Events 

 Upcoming events 
 ChiC events 
 Events attended by ChiC 

 CAPS Outcomes 
 Contact 
 CAPS Community 

 Let s get in contact ! 
  Subscribe to the CAPSSI newsletter to be connected to digital social innovation in Europe and beyond: 

 Leave this field empty if you're human:  

 Follow us   

 2018 - ChiC Project -  Privacy Policy  -  Terms   Conditions   

 Log in with your credentials 

 or 

                        Create an account                     

 Remember me             
 Lost your password? 

 Forgot your details? 

 I remember my details 

 Create Account 

 This site is free and open to everyone, but our registered users get extra privileges like commenting, and voting.  

 Username 

 Password 

  Remember Me 

 Register
 WHOLE PROJECT MARK