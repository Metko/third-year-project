PROJECT NAME: QScience
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 


WELCOME TO QSCIENCE.COM

QScience.com is the innovative and collaborative, peer-reviewed, online publishing platform from Hamad bin Khalifa University Press (HBKU Press). It offers a fast and transparent Open Access scholarly publishing process, which is centered on the author, bringing their research to a global audience.

QScience.com: Innovative, fast global publishing  
QScience.com is one of the best collaborative, peer-reviewed online publishing platforms in the industry today. As part of Hamad bin Khalifa University Press (HBKU Press), a member of Qatar Foundation for Education, Science and Community Development, our goal is to create a unique and collaborative research environment for Qatar and the rest of the world.

We strive to create a fresh, digital approach to research publishing. Our journals reach a global readership that awaits each author's work. And we make it all happen with a simple, fast and transparent publication process.

Take a good look
We're all you expect and more from a leading online publisher:

    Highly sophisticated search tools
    Flexible citation alert functions
    User-friendly library tools:
        all standard authentication models
        custom branding
        COUNTER-compliant usage statistics
        forward linking and link resolving support
    Interactive polls and discussion forums
    Multilingual interface in both English and Arabic

Advancing international research
QScience.com is the online home for a growing range of peer-reviewed open access journals. Providing centralized databases and repositories, we are bringing the burgeoning research environment of Qatar to the wider world - now, and in the future.

We stick to the highest scholarly publishing standards as a publishing house with a truly international outlook. Coordinating closely with scientists and researchers in the region, our intention is to contribute to the renaissance of Arab scientific research and thought. We also actively participate in shaping the international scholarly communication landscape with active membership of international industry associations and organizations.

Our best meets your best
Have a look around QScience.com and you'll find a growing collection of the latest research and reviews. Our authors bring you the best in:

    Medicine and Bioscience
    Healthcare
    Social Science
    Islamic Studies
    Engineering

Our publishing platform has been built to the latest technological standards.  Designed with both readers’ and authors’ needs in mind, our platform  displays full text content in PDF and HTML formats.  Additionally, all abstracts are available in both English and Arabic. And all content is accessible and fully searchable, 24/7, on the user's desktop or mobile device.

Advanced search
Just what you would expect from our innovative approach: a platform with sophisticated search functionality, allowing fast and efficient queries of articles and supporting materials in text, data, audio and video format.

Research gone wide
We're also planning to further expand our range - new journals and subjects are just around the corner. So visit QScience.com, and keep up with the changes.

About Qatar Foundation
Qatar Foundation, located in Doha, Qatar, is an independent, private, non-profit, chartered organization founded in 1995 by decree of His Highness Sheikh Hamad Bin Khalifa Al-Thani, Amir of the State of Qatar, to support centers of excellence which develop people's abilities through investments in human capital, innovative technology, state of the art facilities and partnerships with elite organizations, thus raising the competency of people and the quality of life.

Development partners
QScience.com has been developed with the support of Atypon™ and Aries Systems to ensure an efficient and high-standard peer review process and content delivery.

About the QScience.com team
QScience.com's leadership team and Advisory Board brings broad international experience and a fresh, dynamic and non-traditional approach to scientific publishing. 

 QScience is a free, open source, distributed platform tailored to support the needs of modern scholarly communities. QScience offers a free, open source, web 2.0 venue for scientists to meet and discuss about science. Display of ratings of articles, users reputation and indexes of scholarly productivity are among the supported features, however what really distinguish QScience from other analogous software and web site are the following principles: 
Reclaim your data.
No sign-up, no central authority. Get QScience, install it, be online. Share what you want with whom you want.
Networked Mind. 
A QScience instance is never isolated, but rather belongs to a network of relevant communities, among which data can be searched for, shared, and exchanged in a totally secure way with strong cryptography techniques.
Customization in Evolution. 
QScience is fully customizable to fit different purposes, e.g. a scholarly discussion forum, a research group home page or a research project web site. When you personalize your own QScience instance, you generate a modified version of QScience, that automatically becomes available to the world for download. In this way QScience keeps updated to the needs of the users.
QScience is declared to be about to be released to the public. 

QScience is a free, open source, distributed platform tailored to support the needs of modern scholarly communities. QScience offers a free, open source, web 2.0 venue for scientists to meet and discuss about science. Display of ratings of articles, users reputation and indexes of scholarly productivity are among the supported features, however what really distinguish QScience from other analogous software and web site are the following principles: 
Reclaim your data.
No sign-up, no central authority. Get QScience, install it, be online. Share what you want with whom you want.
Networked Mind. 
A QScience instance is never isolated, but rather belongs to a network of relevant communities, among which data can be searched for, shared, and exchanged in a totally secure way with strong cryptography techniques.
Customization in Evolution. 
QScience is fully customizable to fit different purposes, e.g. a scholarly discussion forum, a research group home page or a research project web site. When you personalize your own QScience instance, you generate a modified version of QScience, that automatically becomes available to the world for download. In this way QScience keeps updated to the needs of the users.
QScience is declared to be about to be released to the public.
 WHOLE PROJECT MARK
