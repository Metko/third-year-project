PROJECT NAME: Alaska Permanent Fund
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

    What We Do
    Board of Trustees
    Performance
    Fund News
    Resources

INVESTING FOR ALASKA
INVESTING FOR THE LONG RUN
Our Mandate
WHO WE ARE
APFC is a state-owned corporation, based in Juneau, that manages the assets of the Alaska Permanent Fund and other funds designated by law, such as the Alaska Mental Health Trust Fund.
Our vision is to deliver sustained
and compelling investment returns
A Pioneering Investment Model
Assets Under Management
UNAUDITED AS OF April 30, 2018
$64.9B
Principal $47.0 BILLION | ERA $17.9 BILLION
Financial Statements
Value
Added
AS OF June 30, 2017
$4.4 B
VS PASSIVE BENCHMARK OVER 3 Years
Daily Fund Market Value
RATE OF
RETURN
FYTD AS OF June 30, 2017
12.89%
8.85% OVER 5 YEARS (with lag)
Performance Reports
Fund News
Quarterly Meeting of the Board of Trustees in Anchorage on May 23-24, 2018
May 16, 2018
APFC Recognizes the Legislature’s Commitment to Structured Draws from the Fund
May 8, 2018
Alaska Permanent Fund Returns 8.86% thru the third quarter of FY18
May 2, 2018
Privately Speaking – Alaska rocks, Private Equity International Issue 164, April 2018
April 13, 2018
>>News archive
APFC’s Board of Trustees Affirm the Fund’s Strategic Direction
May 25, 2018

The Board of Trustees held their quarterly meeting in Anchorage, Alaska on May 23-24, 2018 to review the Alaska Permanent Fund’s performance as of March 31, 2018 and consider APFC’s comprehensive investment policy and asset allocation.

The Alaska Permanent Fund’s (Fund) investments gained 8.86% thru the third quarter of fiscal year 2018 (FY18) and the Fund ended March 31, 2018 with assets under management totaling $64.6 billion. Overall the Fund has performed favorably over the long term, returning 8.35% over the last 5 years and 6.52% over the last 20 years.

Chair Moran noted that “the Fund has benefited from having strong governance practices in place that provide long term discipline and a structure that allows for APFC to identify and respond to targeted opportunities and risks within a shifting global financial landscape.”

Download PDF

    BONDS
    STOCKS
    REAL ESTATE
    ALTERNATIVE INVESTMENTS

Image
BONDS
The Fixed Income Plus portfolio acts as an anchor to the Fund and is comprised of Bonds, Emerging Market Debt, TIPS, REITs, and Listed Infrastructure. During volatile market cycles, these assets provide stability and a source of liquidity to take advantage of market dislocations. Trading is…read more


 The Alaska Permanent Fund is a constitutionally established permanent fund managed by a state-owned corporation, the Alaska Permanent Fund Corporation (APFC). It is a stakeholder trust model of management for large-scale commons that distributes revenues from a shared asset (such as oil on Alaskan land) to every household in the state (roughly $1,000 to $2,000 each year). 
The fund was established in Alaska in 1976, after oil from Alaska began flowing to market through the Trans-Alaska Pipeline System, to invest at least 25% of the oil money into a dedicated fund for current and future generations of Alaskans. The Fund grew from an initial investment of $734,000 in 1977 to approximately $42.1 billion as of August 31, 2012. 

The Alaska Permanent Fund is a constitutionally established permanent fund managed by a state-owned corporation, the Alaska Permanent Fund Corporation (APFC). It is a stakeholder trust model of management for large-scale commons that distributes revenues from a shared asset (such as oil on Alaskan land) to every household in the state (roughly $1,000 to $2,000 each year). 
The fund was established in Alaska in 1976, after oil from Alaska began flowing to market through the Trans-Alaska Pipeline System, to invest at least 25% of the oil money into a dedicated fund for current and future generations of Alaskans. The Fund grew from an initial investment of $734,000 in 1977 to approximately $42.1 billion as of August 31, 2012.
 WHOLE PROJECT MARK
