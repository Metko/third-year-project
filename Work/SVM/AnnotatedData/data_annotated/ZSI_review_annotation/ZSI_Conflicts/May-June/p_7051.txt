PROJECT NAME: BoardGameGeek
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 BoardGameGeek is an online board gaming resource and community. There are reviews, ratings, images, play-aids, translations, and session reports from board game geeks across the world, as well as live discussion forums. In addition, every day nearly a hundred game trades pass through the hands of registered members, as well as transactions in the marketplace. 
The site is updated on a real-time basis by its large user base. 
BoardGameGeek was founded in January 2000 by Scott Alden and Derk Solko as a resource for the board gaming hobby. The database holds reviews, articles, and session reports for over 66,000 different games, expansions, and designers. BoardGameGeek is used by a large, international community with over 400,000 users, as of August 3, 2011, of which about 100,000 are active users (source, Wikipedia). One major activity is the creation of GeekLists, which are lists of games based on a particular theme, or games that people want to trade. There are forums in which members discuss
The site also features bulletin boards, a marketplace, several online boardgames, and a gamer database to help gamers find each other in the same location. 
There is no charge to become a registered member of BGG, although users are encouraged to help improve the site by adding their own reviews and thoughts on games to the existing database. 

BoardGameGeek is an online board gaming resource and community. There are reviews, ratings, images, play-aids, translations, and session reports from board game geeks across the world, as well as live discussion forums. In addition, every day nearly a hundred game trades pass through the hands of registered members, as well as transactions in the marketplace. 
The site is updated on a real-time basis by its large user base. 
BoardGameGeek was founded in January 2000 by Scott Alden and Derk Solko as a resource for the board gaming hobby. The database holds reviews, articles, and session reports for over 66,000 different games, expansions, and designers. BoardGameGeek is used by a large, international community with over 400,000 users, as of August 3, 2011, of which about 100,000 are active users (source, Wikipedia). One major activity is the creation of GeekLists, which are lists of games based on a particular theme, or games that people want to trade. There are forums in which members discuss
The site also features bulletin boards, a marketplace, several online boardgames, and a gamer database to help gamers find each other in the same location. 
There is no charge to become a registered member of BGG, although users are encouraged to help improve the site by adding their own reviews and thoughts on games to the existing database.
 WHOLE PROJECT MARK