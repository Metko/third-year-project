PROJECT NAME: Taste of Home
PROJECT SOURCE: SI DRIVE  case studies: poverty reduction and sustainable development
---------------------------------------
NEW PAGE: 
 he home page is home to the Taste Home team and the Center for Peace Studies.

The site was created through the project "Quality solutions for the integration of refugees" financed by the European Commission.

The site has been updated through the ARSICRO 2015 project financed by the CONCORD network under a contract with the European Commission. The partner organizations of this project are: Slovenian Platform for Development Cooperation and Humanitarian Aid - SLOGA, Crosol and Center for Peace Studies.

The site was updated in 2017 through the support of the US Embassy in Croatia for the implementation of the project "From Migrant to Entrepreneur". Within the project, an international conference on migrant (social) entrepreneurship was held and a publication and video [MIGRENT] was published. The project was jointly conducted by the Center for Peace Studies and Taste Home.

Information on the project 'Quality solutions for the integration of refugees'

Project leader: Fantastic good institution - FADE IN

Partner organizations:

    Center for Peace Studies (CMS)
    Cluster for eco-social innovation and development (CEDRA HR)
    Association of Challenges
    Iskra association
    NESsT
    NK Zagreb 041

Objective and brief description of the activity (project info):
During the duration of the project Quality Integration Solutions for refugees, it is planned to empower and empower a socially vulnerable group of refugees in the Republic of Croatia in order to independently start the process of employment and / or develop the idea of ​​social entrepreneurship in order to better integrate into society.

The objectives of this project are:

    empowering refugee employability skills through training and support for innovative ones
    initiatives of social entrepreneurship,
    advocacy of social inclusion and employment policies for refugees,
    networking of civil initiatives for the provision of social services and inclusion of refugees in society.

The project consists of:

    activities of informing and empowering persons who received refugee status (asylum and subsidiary protection) or live as foreigners in Croatia,
    media campaigns and documentary film on the integration of refugees in Croatia,
    cooperation with civil society organizations in supporting refugee integration.

Total project value and EU share in project financing
The total value of the project The quality steps towards the integration of refugees is EUR 118,581.35. The total share of EU funding in this project is EUR 99,999.65 (84.33%).
The Office for NGOs of the Government of the Republic of Croatia co-financed this project with EUR 13,007.19 (10.96%).

Project implementation period: 01.03.2014 - 28.02.2015.

The core idea of the project is to support and improve integration of immigrants and refugees from war torn and otherwise economically oppressed nations of Africa and the Middle east into Croatian society. It is an effort that seeks to provide a pathway both for the arriving and domestic population to interact in a positive shared atmosphere, as well as to enable the immigrants to develop marketable skills they can use to become full economic contributors and beneficiaries within Croatia.
The goal is to push economic emancipation of the refugees and other migrants by using their knowledge, skills and earlier experience while sensitizing environment/society on potentials of their integration. The whole action relies on multi-cultural and intercultural theoretical model and practice.
 WHOLE PROJECT MARK
