PROJECT NAME: FreedomBox
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 Freedom Box Project was inspired by Eben Moglen's vision of a small, cheap and simple computer that serves freedom in the home. It is building a Debian based platform for distributed applications. 
It aims to provide people privacy protection on a cheap plug server so everybody can have privacy and data can't be mined by governments, billionaires, thugs or even gossipy neighbors. 
It aims to provide Email and telecommunications that protects privacy and resists eavesdropping; a publishing platform that resists oppression and censorship; an organizing tool for democratic activists in hostile regimes; an emergency communication network in times of crisis.

FreedomBox aims tol put in people's own hands and under their own control encrypted voice and text communication, anonymous publishing, social networking, media sharing, and (micro)blogging.

Much of the software already exists: onion routing, encryption, virtual private networks, etc. There are tiny, low-watt computers known as "plug servers" to run this software. The hard parts is integrating that technology, distributing it, and making it easy to use without expertise. The harder part is to decentralize it so users have no need to rely on and trust centralized infrastructure.

That's what FreedomBox is: it integrates privacy protection on a cheap plug server so everybody can have privacy. 

Freedom Box Project was inspired by Eben Moglen's vision of a small, cheap and simple computer that serves freedom in the home. It is building a Debian based platform for distributed applications. 
It aims to provide people privacy protection on a cheap plug server so everybody can have privacy and data can't be mined by governments, billionaires, thugs or even gossipy neighbors. 
It aims to provide Email and telecommunications that protects privacy and resists eavesdropping; a publishing platform that resists oppression and censorship; an organizing tool for democratic activists in hostile regimes; an emergency communication network in times of crisis.

FreedomBox aims tol put in people's own hands and under their own control encrypted voice and text communication, anonymous publishing, social networking, media sharing, and (micro)blogging.

Much of the software already exists: onion routing, encryption, virtual private networks, etc. There are tiny, low-watt computers known as "plug servers" to run this software. The hard parts is integrating that technology, distributing it, and making it easy to use without expertise. The harder part is to decentralize it so users have no need to rely on and trust centralized infrastructure.

That's what FreedomBox is: it integrates privacy protection on a cheap plug server so everybody can have privacy.
 WHOLE PROJECT MARK