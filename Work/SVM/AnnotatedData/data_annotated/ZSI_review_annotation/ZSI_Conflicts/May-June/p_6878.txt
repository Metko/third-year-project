PROJECT NAME: CiviCRM
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

CiviCRM | Open source constituent relationship management for non-profits, NGOs and advocacy organizations.
===============================

CiviCRM | Open source constituent relationship management for non-profits, NGOs and advocacy organizations.

 Skip to main content 

 About  About 
 Vision   Values 
 Roadmap 
 Project Teams 
 Working Groups 
 License 
 Contact 

 Events 
 News 
 Support  Find Support 
 Issue Queue 
 Extensions 
 Trainings 
 Find an Expert 
 Get Involved 

 Get Involved  Get Involved 
 Community guidelines 
 Develop   Extend 
 Support   Educate 
 Market   Promote 
 Make it happen 

 Resources  Documentation 
 CiviCRM Jobs 
 Support CiviCRM 
 Subscribe to newsletters 
 Security announcements 
 Become a member 
 Become a partner 

 Login 

 Explore CiviCRM 
 Find an Expert 
 Demo 
 Download 

      Centralize, build, and engage your constituents     

 Build, engage, and   organize your constituents 
 Get the powerful  open source CRM used by more than  10,000  non-profits. 
 Explore CiviCRM 

 Centralize constituent communications 
 Build and manage your donor base   membership 
 Full integration with  Wordpress ,  Drupal ,  Joomla     Backdrop . 

 Organizations use CiviCRM  A lot 

				24 Million Participants 

				Our users have registered more than 24,441,697 event participants 

				116 Million Donations 

				Our users have processed more than 116,306,758 donations 

				189 Million Contacts 

				CiviCRM sites manage over 189,030,793 contacts around the world 

 Managing a Multifaceted Membership Structure with CiviCRM 
 The Women's Fund of Santa Barbara (WFSB) is a collective donor group that enables women to combine their charitable donations into significant grants focused on the critical needs of women, children and families in Santa Barbara, California. 
 Read more 

 Increasing engagement with members in outback Australia 
 Training and support ensure their CiviCRM investment is successful 
 Read more 

 Improving the Membership Process and Increasing Participation 
 Learn how the Denver Ballet Guild has strengthened their membership with CiviCRM 
 Read more 

 Empowering Girls and Increasing Revenue 
 Girls on the Run St. Louis grew their organization and increased donations by 70% 
 Learn more 

 Access to real-time data improves early childhood education for at-risk children 
 Educational First Steps reduces time to reach educational excellence by 20 months with CiviCRM 
 Find out how 

 A global project. World Family of Radio Maria and SaaS CiviCRM 
 See how World Family of Radio Maria choose a SaaS CiviCRM solution for its Global Project 
 Read More. 

 Stunning websites that promote your members 
 CiviCRM's integration with Drupal Joomla and Wordpress make it easy to create stunning websites that promote your members 
 Read more 

 Focus on your mission - automate your admin 
 MAF Norway use CiviContribute with CiviRules to automate their Thank You processing 
 Learn more 

 Bring fundraising and grantmaking together 
 How MRG Foundation set up CiviCRM with donation, event and grant data living happily under one roof. 
 See how 

 Learn more   

 We've made it easy to learn about CiviCRM.  Try a  demo  to see how easy it is to use. Talk to our  ambassadors  - real users that are happy to share their experiences with you. Attend an  introductory webinar  run by our partners. Or read our complete  documentation  to learn how to do anything with CiviCRM. 
 Try a demo   

	Get hosting 

 CiviCRM gives you total freedom in your choice of host. You're not tied to a particular data centre or country.  Take the hassle out of hosting by choosing one of our  expert partners  for your CiviCRM installation - you'll be up and running in no time. Or  download , install, and host CiviCRM on your own hosting infrastructure (experts only!). 
 Hosted CiviCRM   

	Work with pros 

 We're proud of our large and diverse partner ecosystem - companies of all shapes and sizes that help an amazing range of organisations make the most out of CiviCRM.  Find the right provider for you in our  experts directory . And check out our  technology s ponsors  that provide complimentary services that integrate with CiviCRM. 
 Find a Partner   

 Partners and Contributors 
 CiviCRM is made possible through the contributions of  thousands of organizations and individuals , including... 

 View all partners and contributors 

      Join the community     

 CiviCRM is created and supported by a passionate community of users, implementers, and developers from around the world. Join them and help ensure that everyone has access to an amazing CRM that engages their contacts and furthers their goals. 
 Get involved   

      Attend a meet up     

 Meet ups, conferences, and other real world events are a great way to find out who is using CiviCRM near you. Check out this list of upcoming events for something near you. 
 Meet up   

      Sign up to our newsletter     

 Subscribe to our newsletters to keep up to date with the latest news and developments in the world of CiviCRM. 
 subscribe now   

      Latest news     

   Fosdem, CiviCamp Brussels and the sprint make a great mix!   
   Just arrived back home after spending a few days with the CiviCRM community in Brussels. And as always I really enjoyed myself and returned inspired and full of inspiration.
	I started last Sunday, 4 feb 2018 at FOSDEM (https://fosdem.org/2018/). What an experience, I have never seen so many geeks...   
   by  ErikHommel  | Feb 07, 2018     

   CiviCamp Calgary 2018 - Registration now Open!   
   Tickets as low as CAD $49 (includes lunch) for CiviCamp and as low as CAD $15 for Workshops; Sprint is Free (if you're an official CiviCRM Partner contact me for a discount code) Join us on Tuesday May 22 - 2018 for CiviCamp Calgary - a one day event covering everything related to CiviCRM. Attend...   
   KarinG | Jan 30, 2018     

   Someone Else Pays   
   We are thinking about creating a native CiviCRM extension Someone Else Pays.
	In CiviCRM you could use the soft credits (with a new soft credit type) to register another contact pay for an event registration, a membership and in theory any contribution. Our customer Domus Medica is using that at...   
   ErikHommel | Jan 29, 2018     

 Read more blog posts 

      Upcoming events     

   CiviCRM Edinburgh, UK meetup   
   Monday 12th March at 3:00pm   
   Online Training |  Register Now   

   Organizing a Fundraising Campaign   
   Tuesday 13th March at 12:00pm   
   Online Training |  Register Now   

   CiviTip Webinar: CiviCampaign   
   Wednesday 14th March at 11:30am   
   Online Training |  Register Now   

 Browse all events 

      Give Back     

 Whether downloading and donating, or supporting a specific Make It Happen campaign, one-time gifts help sustain CiviCRM for the thousands of organizations worldwide that rely on it. 
 Make a gift   

      become a member     

 End users can join CiviCRM as members and enjoy unique benefits including event discounts, promotional perks, access to key stakeholders, and the ability to directly shape the project. 
 become a member   

      Become a partner     

 Give back to the project that is benefiting your company through partnership and receive partner-only benefits including promotion to CiviCRM's network of users. 
 become a partner   

 Header  photograph  by  Darwin Yamamoto  under  CC BY-NC-ND 

      HIRE A TRUSTED PRO     

 WORK WITH A TRUSTED EXPERT  TO SETUP YOUR CIVICRM 
 Make your next CiviCRM implementation a Success. 
 Find an Expert 

 Features Contact management 
 Contributions 
 Events 
 Memberships 
 Email marketing 
 Reports 
 Configurable   Customizable 
 Accounting integration 
 Case management 
 Advocacy campaigns 
 Peer-to-peer fundraisers 
 Make it happen 

 Get started Case studies 
 Introductory webinars 
 Find an expert 
 Watch videos 
 Try a demo 
 Read documentation 
 Ask a question 
 Download 
 Extensions 
 Training 
 Support CiviCRM 

 Get involved Register your site 
 Events 
 CiviCamps 
 CiviCons 
 Local events 
 Spread the word 
 Community guidelines 
 Developer resources 
 The issue queue 
 Bug reporting 
 Security announcements 
 Subscribe to newsletters 

 About Vision 
 Roadmap 
 License 
 Contact 
 All case studies 

 Paid issue queue 
 Work with the core team 
 Make it happen 
 Support CiviCRM Become a member 
 Become a partner 
 Support CiviCRM 
 Make a one time gift 

  2005 - 2018, CIVICRM LLC. All rights reserved. 
	CiviCRM and the CiviCRM logo are trademarks of CIVICRM LLC. Except where otherwise noted, content on this site is licensed under a Creative Commons Attribution-Share Alike 3.0 United States Licence. 

CiviCRM | Open source constituent relationship management for non-profits, NGOs and advocacy organizations.
===============================

CiviCRM | Open source constituent relationship management for non-profits, NGOs and advocacy organizations.

 Skip to main content 

 About  About 
 Vision   Values 
 Roadmap 
 Project Teams 
 Working Groups 
 License 
 Contact 

 Events 
 News 
 Support  Find Support 
 Issue Queue 
 Extensions 
 Trainings 
 Find an Expert 
 Get Involved 

 Get Involved  Get Involved 
 Community guidelines 
 Develop   Extend 
 Support   Educate 
 Market   Promote 
 Make it happen 

 Resources  Documentation 
 CiviCRM Jobs 
 Support CiviCRM 
 Subscribe to newsletters 
 Security announcements 
 Become a member 
 Become a partner 

 Login 

 Explore CiviCRM 
 Find an Expert 
 Demo 
 Download 

      Centralize, build, and engage your constituents     

 Build, engage, and   organize your constituents 
 Get the powerful  open source CRM used by more than  10,000  non-profits. 
 Explore CiviCRM 

 Centralize constituent communications 
 Build and manage your donor base   membership 
 Full integration with  Wordpress ,  Drupal ,  Joomla     Backdrop . 

 Organizations use CiviCRM  A lot 

				24 Million Participants 

				Our users have registered more than 24,441,697 event participants 

				116 Million Donations 

				Our users have processed more than 116,306,758 donations 

				189 Million Contacts 

				CiviCRM sites manage over 189,030,793 contacts around the world 

 Managing a Multifaceted Membership Structure with CiviCRM 
 The Women's Fund of Santa Barbara (WFSB) is a collective donor group that enables women to combine their charitable donations into significant grants focused on the critical needs of women, children and families in Santa Barbara, California. 
 Read more 

 Increasing engagement with members in outback Australia 
 Training and support ensure their CiviCRM investment is successful 
 Read more 

 Improving the Membership Process and Increasing Participation 
 Learn how the Denver Ballet Guild has strengthened their membership with CiviCRM 
 Read more 

 Empowering Girls and Increasing Revenue 
 Girls on the Run St. Louis grew their organization and increased donations by 70% 
 Learn more 

 Access to real-time data improves early childhood education for at-risk children 
 Educational First Steps reduces time to reach educational excellence by 20 months with CiviCRM 
 Find out how 

 A global project. World Family of Radio Maria and SaaS CiviCRM 
 See how World Family of Radio Maria choose a SaaS CiviCRM solution for its Global Project 
 Read More. 

 Stunning websites that promote your members 
 CiviCRM's integration with Drupal Joomla and Wordpress make it easy to create stunning websites that promote your members 
 Read more 

 Focus on your mission - automate your admin 
 MAF Norway use CiviContribute with CiviRules to automate their Thank You processing 
 Learn more 

 Bring fundraising and grantmaking together 
 How MRG Foundation set up CiviCRM with donation, event and grant data living happily under one roof. 
 See how 

 Learn more   

 We've made it easy to learn about CiviCRM.  Try a  demo  to see how easy it is to use. Talk to our  ambassadors  - real users that are happy to share their experiences with you. Attend an  introductory webinar  run by our partners. Or read our complete  documentation  to learn how to do anything with CiviCRM. 
 Try a demo   

	Get hosting 

 CiviCRM gives you total freedom in your choice of host. You're not tied to a particular data centre or country.  Take the hassle out of hosting by choosing one of our  expert partners  for your CiviCRM installation - you'll be up and running in no time. Or  download , install, and host CiviCRM on your own hosting infrastructure (experts only!). 
 Hosted CiviCRM   

	Work with pros 

 We're proud of our large and diverse partner ecosystem - companies of all shapes and sizes that help an amazing range of organisations make the most out of CiviCRM.  Find the right provider for you in our  experts directory . And check out our  technology s ponsors  that provide complimentary services that integrate with CiviCRM. 
 Find a Partner   

 Partners and Contributors 
 CiviCRM is made possible through the contributions of  thousands of organizations and individuals , including... 

 View all partners and contributors 

				Introducing CiviCRM Spark 

				A new, quick and secure way to get started with the leading open source CRM 
 Sign up and get started today Learn more 

      Join the community     

 CiviCRM is created and supported by a passionate community of users, implementers, and developers from around the world. Join them and help ensure that everyone has access to an amazing CRM that engages their contacts and furthers their goals. 
 Get involved   

      Attend a meet up     

 Meet ups, conferences, and other real world events are a great way to find out who is using CiviCRM near you. Check out this list of upcoming events for something near you. 
 Meet up   

      Sign up to our newsletter     

 Subscribe to our newsletters to keep up to date with the latest news and developments in the world of CiviCRM. 
 subscribe now   

      Latest news     

   v5.0: The littlest biggest increment   
   During this coming April, you may notice something peculiar on the civicrm.org download page -- instead of 4.7.32, you'll see a jump up to 5.0.0. Does this mean that CiviCRM is finally implementing a personal voice-assistant to take-down Amazon Echo? Nope. Maybe it means open-season on changes,...   
   by  dev-team  | Mar 13, 2018     

   Fosdem, CiviCamp Brussels and the sprint make a great mix!   
   Just arrived back home after spending a few days with the CiviCRM community in Brussels. And as always I really enjoyed myself and returned inspired and full of inspiration.
	I started last Sunday, 4 feb 2018 at FOSDEM (https://fosdem.org/2018/). What an experience, I have never seen so many geeks...   
   ErikHommel | Feb 07, 2018     

   CiviCamp Calgary 2018 - Registration now Open!   
   Tickets as low as CAD $49 (includes lunch) for CiviCamp and as low as CAD $15 for Workshops; Sprint is Free (if you're an official CiviCRM Partner contact me for a discount code) Join us on Tuesday May 22 - 2018 for CiviCamp Calgary - a one day event covering everything related to CiviCRM. Attend...   
   KarinG | Jan 30, 2018     

 Read more blog posts 

      Upcoming events     

   Free CiviCRM Seminar, London   
   Wednesday 21st March at 10:00am   
   Online Training |  Register Now   

   CiviLunch Webinar - Searching Effectively, Part 1   
   Wednesday 21st March at 12:00pm   
   Online Training |  Register Now   

   User Guide remote documentation sprint   
   Thursday 22nd March at 1:00am   
   Online Training |  Register Now   

 Browse all events 

      Give Back     

 Whether downloading and donating, or supporting a specific Make It Happen campaign, one-time gifts help sustain CiviCRM for the thousands of organizations worldwide that rely on it. 
 Make a gift   

      become a member     

 End users can join CiviCRM as members and enjoy unique benefits including event discounts, promotional perks, access to key stakeholders, and the ability to directly shape the project. 
 become a member   

      Become a partner     

 Give back to the project that is benefiting your company through partnership and receive partner-only benefits including promotion to CiviCRM's network of users. 
 become a partner   

 Header  photograph  by  Darwin Yamamoto  under  CC BY-NC-ND 

      HIRE A TRUSTED PRO     

 WORK WITH A TRUSTED EXPERT  TO SETUP YOUR CIVICRM 
 Make your next CiviCRM implementation a Success. 
 Find an Expert 

 Features Contact management 
 Contributions 
 Events 
 Memberships 
 Email marketing 
 Reports 
 Configurable   Customizable 
 Accounting integration 
 Case management 
 Advocacy campaigns 
 Peer-to-peer fundraisers 
 Make it happen 

 Get started Case studies 
 Introductory webinars 
 Find an expert 
 Watch videos 
 Try a demo 
 Read documentation 
 Ask a question 
 Download 
 Extensions 
 Training 
 Support CiviCRM 

 Get involved Register your site 
 Events 
 CiviCamps 
 CiviCons 
 Local events 
 Spread the word 
 Community guidelines 
 Developer resources 
 The issue queue 
 Bug reporting 
 Security announcements 
 Subscribe to newsletters 

 About Vision 
 Roadmap 
 License 
 Contact 
 All case studies 

 Paid issue queue 
 Work with the core team 
 Make it happen 
 Support CiviCRM Become a member 
 Become a partner 
 Support CiviCRM 
 Make a one time gift 

  2005 - 2018, CIVICRM LLC. All rights reserved. 
	CiviCRM and the CiviCRM logo are trademarks of CIVICRM LLC. Except where otherwise noted, content on this site is licensed under a Creative Commons Attribution-Share Alike 3.0 United States Licence. 

 CiviCRM is a web-based, internationalized suite of computer software for constituency relationship management, that falls under the broad rubric of customer relationship management. It is specifically designed for the needs of non-profit, non-governmental, and advocacy groups, and serves as an association management system. CiviCRM is designed to manage information about an organization's donors, members, event registrants, subscribers, grant application seekers and funders, and case contacts. Volunteers, activists, voters as well as more general sorts of business contacts such as employees, clients, or vendors can be managed using CiviCRM.
CiviCRM is deployed in conjunction with either the Drupal, Joomla! or Wordpress content management systems (CMS). 
CiviCRM is used by many large NGOs including Amnesty International, Creative Commons, the Free Software Foundation, and the Wikimedia Foundation for their fundraising. There are also cases of very large record sets being used with one company claiming to have set up CiviCRM with a set of over 3 million constituents. CiviCRM is also used by Kabissa to provide CRM capabilities to over 1,500 organizations, mostly in Africa.
CiviCRM downloads are available from SourceForge, where it was 'project of the month' for January 2011.
CiviCRM is created by an open source community coordinated by CiviCRM LLC, and the 501c3 non-profit Social Source Foundation. The project also receives ongoing input and guidance from our Community Advisory Board. The Social Source Foundation is a nonproft organization (501c3) that exists to create open source, mission-focused technology for the nonprofit and NGO sector. 
CiviCRM LLC holds the copyright to much of the codebase of CiviCRM, which it has contributed back to the community under the GNU Affero General Public License 3 (GNU AGPL 3). 

CiviCRM is a web-based, internationalized suite of computer software for constituency relationship management, that falls under the broad rubric of customer relationship management. It is specifically designed for the needs of non-profit, non-governmental, and advocacy groups, and serves as an association management system. CiviCRM is designed to manage information about an organization's donors, members, event registrants, subscribers, grant application seekers and funders, and case contacts. Volunteers, activists, voters as well as more general sorts of business contacts such as employees, clients, or vendors can be managed using CiviCRM.
CiviCRM is deployed in conjunction with either the Drupal, Joomla! or Wordpress content management systems (CMS). 
CiviCRM is used by many large NGOs including Amnesty International, Creative Commons, the Free Software Foundation, and the Wikimedia Foundation for their fundraising. There are also cases of very large record sets being used with one company claiming to have set up CiviCRM with a set of over 3 million constituents. CiviCRM is also used by Kabissa to provide CRM capabilities to over 1,500 organizations, mostly in Africa.
CiviCRM downloads are available from SourceForge, where it was 'project of the month' for January 2011.
CiviCRM is created by an open source community coordinated by CiviCRM LLC, and the 501c3 non-profit Social Source Foundation. The project also receives ongoing input and guidance from our Community Advisory Board. The Social Source Foundation is a nonproft organization (501c3) that exists to create open source, mission-focused technology for the nonprofit and NGO sector. 
CiviCRM LLC holds the copyright to much of the codebase of CiviCRM, which it has contributed back to the community under the GNU Affero General Public License 3 (GNU AGPL 3).
 WHOLE PROJECT MARK