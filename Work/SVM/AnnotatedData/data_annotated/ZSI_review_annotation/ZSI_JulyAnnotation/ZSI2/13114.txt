Intolerant? Me? | Changemakers
===============================

 
  Preloader  
  <div id="preloader">
	<div id="status"> </div>
</div>  
 Skip to main content 
 About 
 Challenges 
 Projects 
 Learning 
 Blog 
   
   
 Changemakers 
 English Español Português Français 
   
 Search form 
 Search  
   
 Log in   
 FAQs 
   
 You are here Home 
 Intolerant? Me? 
   Activating Empathy: Transforming Schools To Teach What Matters     
   
   Congratulations! This Entry has been selected as a semifinalist.     
   
   
   
 Intolerant? Me? 
   Lisboa,Porto, Coimbra e Algarve, Portugal Lisboa, Portugal     
   
 Associacao PAR ... <div class="founder">Founder</div> 
 Organization type:  nonprofit/ngo/citizen sector   
    Project Stage:
   
 
    Scaling   
 Budget:  $100,000 - $250,000   
    Website:
   
 http://www.par.org.pt/   
 Arts &amp; culture   
 Citizen participation   
 Intercultural relations   
 Education   
 Tolerance   
 Migration   
 Mentorship   
 Project Summary Elevator Pitch   
    Concise Summary: Help us pitch this solution! Provide an explanation within 3-4 short sentences.   
 An initiative that excels in its simplicity and strong emphasis on responsibility, autonomy and the human potential of students involved. 
 About Project   
    Problem: What problem is this project trying to address?   
 
    The school, a place of coexistence of different cultures &amp; potential conflicts has the task &amp; challenge of integrally educating children &amp; youth as people, making the cultural diversity of students be considered a factor of cohesion and personal/social enrichment. The Intolerant? Me? Program has been working since 12/07 for the promotion of intercultural dialogue &amp; tolerance through music with young secondary &amp; higher edu. students. At this stage, we intend to expand the project &amp; to address a very real concern that has been presented by various teachers &amp; members of the Executive Board of the School of Albufeira in Algarve, which has students from 24 nationalities: combating the difficult integration of young immigrants &amp; other minority students and their progressive segregation by peers    
 
    Solution: What is the proposed solution? Please be specific!   
 
    Building on the results and investment in Intolerant? Me?, our goal now is to expand the initiative to over 10 schools and create, in each school, a core group of students responsible for welcoming and integrating new students, particularly minorities, including immigrant students.
Based on the methods already used in formal education and peer education, the Association will have full responsibility for training and mentoring of these groups in schools to ensure their success.
The tools used by each group for the reception of new students will be defined together with the students who comprise them, because it is intended for the integration strategies to be defined by young people themselves. Based on previous experience, we can give the example of an activity created by the group in Albufeira: a peddy paper at school with the goal of presenting the school and integrating the new students for whom the starting line is written in many different languages.
   
 Impact: How does it Work   
    Example: Walk us through a specific example(s) of how this solution makes a difference; include its primary activities.   
 
    Key: Involve students in the process of integration of their peers.
1-Mobilization and Training:
1.1.Teaser: travel the world in seven minutes using the senses as means of transport. It has been an extremely effective way to engage young people&#039;s participation.
1.2. Intercultural Dialogue Workshop: To create a constant reflection and action in the context of intercultural  respect and diversity - both cultural and individual.

2-Mentoring Centers: After the workshop, young people are invited to form a group for welcoming new students. With the mentor, the group must develop strategies to involve fellow immigrants in a climate friendly and conducive to the establishment and strengthening of good interpersonal relationships among peers, teachers and a quality learning environment.

3-Groups in practice: An example
&quot;John is 16 years old and is in 10th grade at School Y. One day he noticed an event that allowed the school to travel around the world in 7 min. He decided to try it and loved it! The group then asked him if he would like to participate in a workshop on intercultural dialogue. As it was free, he decided to try it too. He left the workshop with an eagerness to accept the proposal which had been developed: to create activities to help welcome immigrant students to the school. Full of ideas, John became a leader of the group and, together with his colleagues, prepared a game to introduce new students to school, from the courtyard to the cafeteria. New students were thus able to interact with other students and experience a more seamless integration.” 
   
 Sustainability   
    Marketplace: Who else is addressing the problem outlined here? How does the proposed project differ from these approaches?   
 
    In Portugal there are projects working on intercultural dialogue in schools: the project M=igual works in the area of Education for Development and ODM and the UNESCO Clubs have already existed in some schools in the country. However, none of the projects specifically focus on the issues related to intercultural dialogue, tolerance and cultural diversity, both end up working with issues related to schools. Other organizations, teachers and people are working on diversity as an asset to the educational system, however, in our view it is necessary to improve and do more. More than teaching to maintain and respect different cultural identities, we intend to implement a shared mode of resolving conflicts and problems common to the need to develop a real sense of intercultural coexistence.   
 About You Organization:   Associação PAR - Respostas Sociais About You First Name Associacao PAR 
 Last Name Respostas Sociais 
 Twitter URL Facebook URL About Your Organization Organization Name Associação PAR - Respostas Sociais 
 Organization Country , LI, Lisboa 
 Country where this project is creating social impact , Lisboa,Porto, Coimbra e Algarve 
 Your role in Education Coach. The type of school(s) your solution is affiliated with Public (tuition-free) How long has your organization been operating? 1‐5 years The information you provide here will be used to fill in any parts of your profile that have been left blank, such as interests, organization information, and website. No contact information will be made public. Please uncheck here if you do not want this to happen.. Innovation How long has your solution been in operation? Operating for 1‐5 years Now that you have thought out your entry, help us pitch it. Define your company, program, service, or product in 1-2 short sentences [136 characters] An initiative that excels in its simplicity &amp; strong emphasis on responsibility, autonomy &amp; the human potential of students involved 
 Identify what is innovative about your solution in 1-2 short sentences [136 characters] characters] 
Student Autonomy 
Information from students to teachers 
Synergies in Community Learning 
Simple, Effective and Fun 
 Social Impact What has been the impact of your solution to date? Awareness Campaign 
-49 Musicians involved out of respect for diversity. 
-11,000  Intolerant? Me? CDs produced and edited 
Youth Mobilization Tool - Travel the World 
-Awareness Campaign in 7 Summer Festivals 
Festival-with all the musicians on the CD 
-1200 People directly and 100,000 indirectly trained about the importance of tolerance and intercultural dialogue 
Intercultural Education Program 
-Intercultural exchange 
-15 Workshops for Members of Tolerance and Intercultural Dialogue 
-1 Training of Facilitators 
-Creation of Member Manual for Tolerance and Intercultural Dialogue 
-Creation of Facilitator&#039;s Manual for Intercultural Education 
-230 Members trained for Intercultural Dialogue (67 motivated to continue the work). 
-10 Young people trained to be facilitators of the workshop independently 
Since 2009, Members and Facilitators have streamlined operations (awareness and training) in secondary schools reaching 906 young people. 
 What is your projected impact over the next 1-3 years? We anticipate that within three years the project will have grown to influence at national level, &amp; if possible internationally. Specifically, we anticipate the creation of Centers of integration of minority students in 10 schools per year. This means that in 3 years, we will have implemented the project in 30 schools in Portugal. These schools &amp; centers will operate independently &amp; remain accountable &amp; after a year of mentoring on the part of coaches, members &amp; facilitators from the Par Association. Thus, at the end of 3 years we will have 20 schools with chapters functioning independently &amp; 10 schools still in the process of mentoring by the Par Association. In parallel, we intend to evaluate the intervention and demonstrate its effectiveness in order to justify the growth of the model 
 What barriers might hinder the success of your project?  How do you plan to overcome them? Our success factors include: A) Integrating the project in schools; B) availability of at least one teacher per school; C) motivating young people to maintain the groups; D)effectiveness of the actions of the groups and E) turnover students in the groups. Any gap in these areas will be a barrier to project success. In order to overcome these challenges, the Association proposes, respectively: A) present data on the previous success of the project to the School Board (video with teachers explaining the need &amp; results in their schools, B) partnership with the school leadership in order to integrate the activities of the project with teachers’ schedules, C) and D) invest in methodologies with proven success and E) mentor young people in the groups to empower other students to join the process 
 Winning entries present a strong plan for how they will achieve and track growth.  Identify your six-month milestone for growing your impact Guarantee the participation of 10 schools (5 primary, 5 secondary) &amp; initiate the mobilization &amp; training phases for the student 
 Identify three major tasks you will have to complete to reach your six-month milestone Task 1 Project communication plan created and disseminated for the leadership at 30 schools. ( 15 primary/15 Secondary) 
 Task 2 Teaser Activities in 10 schools for at least 100 students per school. 
 Task 3 Intercultural Dialogue Workshop in  10 schools for 20 students per school. 200 students total 
 Now think bigger!  Identify your 12-month impact milestone 10 schools with active groups working to integrate immigrant students or those belonging to other minorities. 
 Identify three major tasks you will have to complete to reach your 12-month milestone Task 1 After the Workshop, mobilize the youth to form a group for the integration of new students in each one of the schools. 
 Task 2 Mentoring of the student groups by trainers, staff and facilitators of the PAR Association. 
 Task 3 Evaluation of the impact and efficiency of each group in each school together with students, teachers and supporting stakeholder 
 Founding Story: We want to hear about your "Aha!" moment.  Share the story of where and when the founder(s) saw this solution's potential to change the world [125 words] The story is curious. The idea came from a youth, 24 years old at the time, and with a great passion for music. Aside from writing songs, she believed in the potential of art to bring people together and build relationships. At the same time, she strongly believed that music could change perceptions, or at least start the discussion. Another passion was always  people and interpersonal relationships, including their relationship with the differences. After finishing a degree in psychology, she co-founded the PAR Association. After founding PAR he remained hyper motivated to change the world, and the ideas just kept coming. One day, she woke up in the middle of the night and said in loud voice: &quot;I know! I&#039;ll work on diversity through music, edit a CD and generate funds to work with young people through informal education.&quot; The next day she wrote a song &quot;hey you&quot;, the first step for the CD &quot;Perspectives&quot; that, while showcasing jazz to heavy metal, would have a common theme: respect for 
 Sustainability Tell us about your partnerships The project&#039;s success depends heavily on partnerships with schools in which we implement the project.  They ensure minimum material for conducting the activities of the groups (school supplies, photocopies, etc.) and ensure the commitment of a teacher to monitor the group, which are key elements in this model as the quality of this partnership is the basis of the success of the intervention. Simultaneously, we intend to establish partnerships with Parish Councils and Municipal Councils of the school regions to ensure additional physical and material support. 
 What type of team (staff, volunteers, etc.) will ensure that you achieve the growth milestones identified in the  Social Impact  section? [75 words] At this stage the vast majority of the implementation team consists of young volunteers who have participated in the Intolerant? Me? Project in the past and now wish to contribute to its continuation. Thus, for each year of intervention, the project team will be formed by the mentor of the project - responsible for coordination, for 10 teachers (one per school), and about 20 staff (responsible for the teaser phase) and 10 facilitators (responsible for workshops) who work on a voluntary basis, as they have done so far with great skill and energy. 
 Please elaborate on any needs or offers you have mentioned above and/or suggest categories of support that aren't specified within the list   
 Printer-friendly version Download PDF 
 Give Feedback 
 Your insights will be sent privately to this project to help them improve. Rate This Project 
 Overall  * 
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Innovation  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Impact  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Sustainability  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Write a Private Feedback  * 
 Strength Need Improvement   
 Clarity of model 
   
 Financial Sustainability 
   
 Idea Originality 
   
 Impact Measurement 
   
 Impact Potential 
   
 Partnerships 
   
 Scalability 
   
 Team 
   
 Understanding of Marketplace 
   
   
   
   
   
   
 © 2016-2017 Ashoka Changemakers. 
 Terms  &amp;  Privacy Policy  |  Feedback 
   
 What We Do 
 What You Can Do Here 
 Our Team 
 Careers 
 FAQs 
 Partner With Us 
 Contact Us 
   
   
   
   
Intolerant? Me? | Changemakers
===============================

 
  Preloader  
  <div id="preloader">
	<div id="status"> </div>
</div>  
 Skip to main content 
 About 
 Challenges 
 Projects 
 Learning 
 Blog 
   
   
 Changemakers 
 English Español Português Français 
   
 Search form 
 Search  
   
 Log in   
 FAQs 
   
 You are here Home 
 Intolerant? Me? 
   Activating Empathy: Transforming Schools To Teach What Matters     
   
   Congratulations! This Entry has been selected as a semifinalist.     
   
   
   
 Intolerant? Me? 
   Lisboa,Porto, Coimbra e Algarve, Portugal Lisboa, Portugal     
   
 Associacao PAR ... <div class="founder">Founder</div> 
 Organization type:  nonprofit/ngo/citizen sector   
    Project Stage:
   
 
    Scaling   
 Budget:  $100,000 - $250,000   
    Website:
   
 http://www.par.org.pt/   
 Arts &amp; culture   
 Citizen participation   
 Intercultural relations   
 Education   
 Tolerance   
 Migration   
 Mentorship   
 Project Summary Elevator Pitch   
    Concise Summary: Help us pitch this solution! Provide an explanation within 3-4 short sentences.   
 An initiative that excels in its simplicity and strong emphasis on responsibility, autonomy and the human potential of students involved. 
 About Project   
    Problem: What problem is this project trying to address?   
 
    The school, a place of coexistence of different cultures &amp; potential conflicts has the task &amp; challenge of integrally educating children &amp; youth as people, making the cultural diversity of students be considered a factor of cohesion and personal/social enrichment. The Intolerant? Me? Program has been working since 12/07 for the promotion of intercultural dialogue &amp; tolerance through music with young secondary &amp; higher edu. students. At this stage, we intend to expand the project &amp; to address a very real concern that has been presented by various teachers &amp; members of the Executive Board of the School of Albufeira in Algarve, which has students from 24 nationalities: combating the difficult integration of young immigrants &amp; other minority students and their progressive segregation by peers    
 
    Solution: What is the proposed solution? Please be specific!   
 
    Building on the results and investment in Intolerant? Me?, our goal now is to expand the initiative to over 10 schools and create, in each school, a core group of students responsible for welcoming and integrating new students, particularly minorities, including immigrant students.
Based on the methods already used in formal education and peer education, the Association will have full responsibility for training and mentoring of these groups in schools to ensure their success.
The tools used by each group for the reception of new students will be defined together with the students who comprise them, because it is intended for the integration strategies to be defined by young people themselves. Based on previous experience, we can give the example of an activity created by the group in Albufeira: a peddy paper at school with the goal of presenting the school and integrating the new students for whom the starting line is written in many different languages.
   
 Impact: How does it Work   
    Example: Walk us through a specific example(s) of how this solution makes a difference; include its primary activities.   
 
    Key: Involve students in the process of integration of their peers.
1-Mobilization and Training:
1.1.Teaser: travel the world in seven minutes using the senses as means of transport. It has been an extremely effective way to engage young people&#039;s participation.
1.2. Intercultural Dialogue Workshop: To create a constant reflection and action in the context of intercultural  respect and diversity - both cultural and individual.

2-Mentoring Centers: After the workshop, young people are invited to form a group for welcoming new students. With the mentor, the group must develop strategies to involve fellow immigrants in a climate friendly and conducive to the establishment and strengthening of good interpersonal relationships among peers, teachers and a quality learning environment.

3-Groups in practice: An example
&quot;John is 16 years old and is in 10th grade at School Y. One day he noticed an event that allowed the school to travel around the world in 7 min. He decided to try it and loved it! The group then asked him if he would like to participate in a workshop on intercultural dialogue. As it was free, he decided to try it too. He left the workshop with an eagerness to accept the proposal which had been developed: to create activities to help welcome immigrant students to the school. Full of ideas, John became a leader of the group and, together with his colleagues, prepared a game to introduce new students to school, from the courtyard to the cafeteria. New students were thus able to interact with other students and experience a more seamless integration.” 
   
 Sustainability   
    Marketplace: Who else is addressing the problem outlined here? How does the proposed project differ from these approaches?   
 
    In Portugal there are projects working on intercultural dialogue in schools: the project M=igual works in the area of Education for Development and ODM and the UNESCO Clubs have already existed in some schools in the country. However, none of the projects specifically focus on the issues related to intercultural dialogue, tolerance and cultural diversity, both end up working with issues related to schools. Other organizations, teachers and people are working on diversity as an asset to the educational system, however, in our view it is necessary to improve and do more. More than teaching to maintain and respect different cultural identities, we intend to implement a shared mode of resolving conflicts and problems common to the need to develop a real sense of intercultural coexistence.   
 About You Organization:   Associação PAR - Respostas Sociais About You First Name Associacao PAR 
 Last Name Respostas Sociais 
 Twitter URL Facebook URL About Your Organization Organization Name Associação PAR - Respostas Sociais 
 Organization Country , LI, Lisboa 
 Country where this project is creating social impact , Lisboa,Porto, Coimbra e Algarve 
 Your role in Education Coach. The type of school(s) your solution is affiliated with Public (tuition-free) How long has your organization been operating? 1‐5 years The information you provide here will be used to fill in any parts of your profile that have been left blank, such as interests, organization information, and website. No contact information will be made public. Please uncheck here if you do not want this to happen.. Innovation How long has your solution been in operation? Operating for 1‐5 years Now that you have thought out your entry, help us pitch it. Define your company, program, service, or product in 1-2 short sentences [136 characters] An initiative that excels in its simplicity &amp; strong emphasis on responsibility, autonomy &amp; the human potential of students involved 
 Identify what is innovative about your solution in 1-2 short sentences [136 characters] characters] 
Student Autonomy 
Information from students to teachers 
Synergies in Community Learning 
Simple, Effective and Fun 
 Social Impact What has been the impact of your solution to date? Awareness Campaign 
-49 Musicians involved out of respect for diversity. 
-11,000  Intolerant? Me? CDs produced and edited 
Youth Mobilization Tool - Travel the World 
-Awareness Campaign in 7 Summer Festivals 
Festival-with all the musicians on the CD 
-1200 People directly and 100,000 indirectly trained about the importance of tolerance and intercultural dialogue 
Intercultural Education Program 
-Intercultural exchange 
-15 Workshops for Members of Tolerance and Intercultural Dialogue 
-1 Training of Facilitators 
-Creation of Member Manual for Tolerance and Intercultural Dialogue 
-Creation of Facilitator&#039;s Manual for Intercultural Education 
-230 Members trained for Intercultural Dialogue (67 motivated to continue the work). 
-10 Young people trained to be facilitators of the workshop independently 
Since 2009, Members and Facilitators have streamlined operations (awareness and training) in secondary schools reaching 906 young people. 
 What is your projected impact over the next 1-3 years? We anticipate that within three years the project will have grown to influence at national level, &amp; if possible internationally. Specifically, we anticipate the creation of Centers of integration of minority students in 10 schools per year. This means that in 3 years, we will have implemented the project in 30 schools in Portugal. These schools &amp; centers will operate independently &amp; remain accountable &amp; after a year of mentoring on the part of coaches, members &amp; facilitators from the Par Association. Thus, at the end of 3 years we will have 20 schools with chapters functioning independently &amp; 10 schools still in the process of mentoring by the Par Association. In parallel, we intend to evaluate the intervention and demonstrate its effectiveness in order to justify the growth of the model 
 What barriers might hinder the success of your project?  How do you plan to overcome them? Our success factors include: A) Integrating the project in schools; B) availability of at least one teacher per school; C) motivating young people to maintain the groups; D)effectiveness of the actions of the groups and E) turnover students in the groups. Any gap in these areas will be a barrier to project success. In order to overcome these challenges, the Association proposes, respectively: A) present data on the previous success of the project to the School Board (video with teachers explaining the need &amp; results in their schools, B) partnership with the school leadership in order to integrate the activities of the project with teachers’ schedules, C) and D) invest in methodologies with proven success and E) mentor young people in the groups to empower other students to join the process 
 Winning entries present a strong plan for how they will achieve and track growth.  Identify your six-month milestone for growing your impact Guarantee the participation of 10 schools (5 primary, 5 secondary) &amp; initiate the mobilization &amp; training phases for the student 
 Identify three major tasks you will have to complete to reach your six-month milestone Task 1 Project communication plan created and disseminated for the leadership at 30 schools. ( 15 primary/15 Secondary) 
 Task 2 Teaser Activities in 10 schools for at least 100 students per school. 
 Task 3 Intercultural Dialogue Workshop in  10 schools for 20 students per school. 200 students total 
 Now think bigger!  Identify your 12-month impact milestone 10 schools with active groups working to integrate immigrant students or those belonging to other minorities. 
 Identify three major tasks you will have to complete to reach your 12-month milestone Task 1 After the Workshop, mobilize the youth to form a group for the integration of new students in each one of the schools. 
 Task 2 Mentoring of the student groups by trainers, staff and facilitators of the PAR Association. 
 Task 3 Evaluation of the impact and efficiency of each group in each school together with students, teachers and supporting stakeholder 
 Founding Story: We want to hear about your "Aha!" moment.  Share the story of where and when the founder(s) saw this solution's potential to change the world [125 words] The story is curious. The idea came from a youth, 24 years old at the time, and with a great passion for music. Aside from writing songs, she believed in the potential of art to bring people together and build relationships. At the same time, she strongly believed that music could change perceptions, or at least start the discussion. Another passion was always  people and interpersonal relationships, including their relationship with the differences. After finishing a degree in psychology, she co-founded the PAR Association. After founding PAR he remained hyper motivated to change the world, and the ideas just kept coming. One day, she woke up in the middle of the night and said in loud voice: &quot;I know! I&#039;ll work on diversity through music, edit a CD and generate funds to work with young people through informal education.&quot; The next day she wrote a song &quot;hey you&quot;, the first step for the CD &quot;Perspectives&quot; that, while showcasing jazz to heavy metal, would have a common theme: respect for 
 Sustainability Tell us about your partnerships The project&#039;s success depends heavily on partnerships with schools in which we implement the project.  They ensure minimum material for conducting the activities of the groups (school supplies, photocopies, etc.) and ensure the commitment of a teacher to monitor the group, which are key elements in this model as the quality of this partnership is the basis of the success of the intervention. Simultaneously, we intend to establish partnerships with Parish Councils and Municipal Councils of the school regions to ensure additional physical and material support. 
 What type of team (staff, volunteers, etc.) will ensure that you achieve the growth milestones identified in the  Social Impact  section? [75 words] At this stage the vast majority of the implementation team consists of young volunteers who have participated in the Intolerant? Me? Project in the past and now wish to contribute to its continuation. Thus, for each year of intervention, the project team will be formed by the mentor of the project - responsible for coordination, for 10 teachers (one per school), and about 20 staff (responsible for the teaser phase) and 10 facilitators (responsible for workshops) who work on a voluntary basis, as they have done so far with great skill and energy. 
 Please elaborate on any needs or offers you have mentioned above and/or suggest categories of support that aren't specified within the list   
 Printer-friendly version Download PDF 
 Give Feedback 
 Your insights will be sent privately to this project to help them improve. Rate This Project 
 Overall  * 
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Innovation  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Impact  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Sustainability  
 Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5 
 Write a Private Feedback  * 
 Strength Need Improvement   
 Clarity of model 
   
 Financial Sustainability 
   
 Idea Originality 
   
 Impact Measurement 
   
 Impact Potential 
   
 Partnerships 
   
 Scalability 
   
 Team 
   
 Understanding of Marketplace 
   
   
   
   
   
   
 © 2016-2017 Ashoka Changemakers. 
 Terms  &amp;  Privacy Policy  |  Feedback 
   
 What We Do 
 What You Can Do Here 
 Our Team 
 Careers 
 FAQs 
 Partner With Us 
 Contact Us
 WHOLE PROJECT MARK