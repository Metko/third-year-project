iSave the World Online Game
===============================

   <![endif] 
  Head  
  end #head_logo  
 People. Planet. Profit. Play.
 
  end #head_contact  
  end #head  
  <li><a href="index.html" title="Demo Intro">Demo Intro</a></li>  
 Game Overview 
 Prototype 
  end #menu_container  
  end #head_container  
  End Head  
  Content  
 iSave the World  is an innovative business simulation game where kids ages 8 to 12 become digital social entrepreneurs. Operating their own virtual impact business, players discover the powerful effect they can have on our planet and its people through the choices they make. Most importantly, they'll be having a fun time doing it.  
   
 The game is currently in development by  content4good , a  socially-conscious gaming company for kids. The tech team has over a decade of experience creating games for brands that include Disney, Scholastic and Marvel. 
 Entrepreneur Coach Cameron Herold said, &ldquo;if we could get kids to embrace the idea at a young  age of being entrepreneurial, we could change everything in the world that is a problem  today.&rdquo;  iSave the World  is a game that will empower kids to do exactly that.
 
  end #content_container  
  End Content  
  Footer  
 For more info please contact Producer Erin Skillen via  email  or at  250-514-3506 . 
 
Copyright Â© 2015  content4good .  All rights reserved.
  end #footer_copyright  
 
Website by  JetDog 
  end #footer_jetdog  
  end #footer_container  
  end #footer  
  End Footer  
  end #container .container  
iSave the World Online Game Preview
===============================

   <![endif] 
  Head  
  end #head_logo  
 People. Planet. Profit. Play.
 
  end #head_contact  
  end #head  
  <li><a href="index.html" title="Demo Intro">Demo Intro</a></li>  
 Game Overview 
 Prototype 
  end #menu_container  
  end #head_container  
  End Head  
  Content  
 Prototype 
 Here's a rough mockup of the game. 
 Want to go straight to the skateboard builder?  Here you go ! 
  end #content_container  
  End Content  
  Footer  
 For more info please contact Producer Erin Skillen via  email  or at  250-514-3506 . 
 
Copyright Â© 2015  content4good .  All rights reserved.
  end #footer_copyright  
 
Website by  JetDog 
  end #footer_jetdog  
  end #footer_container  
  end #footer  
  End Footer  
  end #container .container  
iSave the World Online Game
===============================

   <![endif] 
  Head  
  end #head_logo  
 People. Planet. Profit. Play.
 
  end #head_contact  
  end #head  
  <li><a href="index.html" title="Demo Intro">Demo Intro</a></li>  
 Game Overview 
 Prototype 
  end #menu_container  
  end #head_container  
  End Head  
  Content  
 iSave the World  is an innovative business simulation game where kids ages 8 to 12 become digital social entrepreneurs. Operating their own virtual impact business, players discover the powerful effect they can have on our planet and its people through the choices they make. Most importantly, they'll be having a fun time doing it.  
   
 The game is currently in development by  content4good , a  socially-conscious gaming company for kids. The tech team has over a decade of experience creating games for brands that include Disney, Scholastic and Marvel. 
 Entrepreneur Coach Cameron Herold said, &ldquo;if we could get kids to embrace the idea at a young  age of being entrepreneurial, we could change everything in the world that is a problem  today.&rdquo;  iSave the World  is a game that will empower kids to do exactly that.
 
  end #content_container  
  End Content  
  Footer  
 For more info please contact Producer Erin Skillen via  email  or at  250-514-3506 . 
 
Copyright Â© 2015  content4good .  All rights reserved.
  end #footer_copyright  
 
Website by  JetDog 
  end #footer_jetdog  
  end #footer_container  
  end #footer  
  End Footer  
  end #container .container  
iSave the World Online Game
===============================

   <![endif] 
  Head  
  end #head_logo  
 People. Planet. Profit. Play.
 
  end #head_contact  
  end #head  
  <li><a href="index.html" title="Demo Intro">Demo Intro</a></li>  
 Game Overview 
 Prototype 
  end #menu_container  
  end #head_container  
  End Head  
  Content  
 iSave the World  is an innovative business simulation game where kids ages 8 to 12 become digital social entrepreneurs. Operating their own virtual impact business, players discover the powerful effect they can have on our planet and its people through the choices they make. Most importantly, they'll be having a fun time doing it.  
   
 The game is currently in development by  content4good , a  socially-conscious gaming company for kids. The tech team has over a decade of experience creating games for brands that include Disney, Scholastic and Marvel. 
 Entrepreneur Coach Cameron Herold said, &ldquo;if we could get kids to embrace the idea at a young  age of being entrepreneurial, we could change everything in the world that is a problem  today.&rdquo;  iSave the World  is a game that will empower kids to do exactly that.
 
  end #content_container  
  End Content  
  Footer  
 For more info please contact Producer Erin Skillen via  email  or at  250-514-3506 . 
 
Copyright Â© 2015  content4good .  All rights reserved.
  end #footer_copyright  
 
Website by  JetDog 
  end #footer_jetdog  
  end #footer_container  
  end #footer  
  End Footer  
  end #container .container  
iSaveTheWorld Demo#2
===============================

 
 
      Preloading Graphics and Sounds... 
 Awesome! 
 You built your company's first board. 
   
 0 
 Total Cost 
 $0.00 
 <div class="social-points-box">
            <div class="label">Social Points</div>
            <div class="value">0</div>
          </div> 
 Ready to test it out?  
 Go Back 
 Return to Prototype 
 &times; 
 Customize Your 
 Deck 
 Graphics 
 Trucks 
 Wheels 
 Griptape 
   
 0 
 Total Cost 
 $0.00 
 
      <div class="social">
        <div class="label">Social</div>
        <div class="value value-5">5</div>
      </div>
      <div class="eco">
        <div class="label">Eco</div>
        <div class="value value-5">5</div>
      </div>
	   
 5 
 <div class="label">Cost</div> 
 $20.00 
  .item    
 
      <div class="social">
        <div class="label">Social</div>
        <div class="value value-2">2</div>
      </div>
      <div class="eco">
        <div class="label">Eco</div>
        <div class="value value-2">2</div>
      </div>
	   
 2 
 <div class="label">Cost</div> 
 $12.00 
  .item    
 
      <div class="social">
        <div class="label">Social</div>
        <div class="value value-4">4</div>
      </div>
      <div class="eco">
        <div class="label">Eco</div>
        <div class="value value-3">3</div>
      </div>
	   
 3 
 <div class="label">Cost</div> 
 $15.00 
  .item    
 Choose My Graphics &rarr; 
 Choose the wood for your skateboard's deck. Click on the ! to find out more about each supplier and the materials they use. 
 Cost 
 $6.50 
  .item  
 Cost 
 $5.50 
  .item  
 Cost 
 $7.50 
  .item  
 Cost 
 $6.50 
  .item  
 Cost 
 $5.50 
  .item  
 Cost 
 $7.50 
  .item  
 Cost 
 $6.50 
  .item  
 Cost 
 $5.50 
  .item  
 Cost 
 $7.50 
  .item  
 Cost 
 $6.50 
  .item  
 Cost 
 $5.50 
  .item  
 Cost 
 $7.50 
  .item  
 Choose My Trucks &rarr; 
 Then you get to decorate your board.  You have limited options now, but will have more to choose from as you play the game. 
 <div class="social">
        <div class="label">Social</div>
        <div class="value value-3">3</div>
      </div> 
 3 
 $10.00 
  .item    
 <div class="social">
        <div class="label">Social</div>
        <div class="value value-1">1</div>
      </div> 
 1 
 $7.00 
  .item    
 <div class="social">
        <div class="label">Social</div>
        <div class="value value-5">5</div>
      </div> 
 5 
 $12.00 
  .item    
 Choose My Wheels &rarr; 
 The trucks look mostly the same, but click ! to learn more about each one. 
   
 1 
 $3.00 
  .item    
   
 4 
 $8.00 
  .item    
   
 3 
 $5.00 
  .item    
 Choose My Griptape &rarr; 
 Choose the kind of wheel you want and the colour. 
   
 5 
 $4.00 
  .item    
   
 2 
 $3.00 
  .item    
   
 3 
 $2.00 
  .item    
 Show me my board! 
 Choose the kind of griptape you want and the colour.  
 alternative content 
 alternative content 
 alternative content 
 alternative content 
 alternative content
 WHOLE PROJECT MARK