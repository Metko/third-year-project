Less School Cafeteria Food in the Bins ( Sweden Retail) 
===============================

 
 <body class="tm-sidebar-a-right tm-sidebar-b-left tm-sidebars-2 tm-isblog"> 
 Home About FUSIONS Work Structure Project Partners Newsletter Archives News Archives Contact us Food Waste Wiki Members Events Events calendar FUSIONS Platform Meetings Awareness Raising Events Publications FAQ Social Innovation Projects FUSIONS feasibility studies Social innovation inventory Submit details of your project Blog 
 Environment Authority of Gothenburg Municipality (   Sweden) 
 The Environment Authority of Gothenburg Municipality developed an information campaign about environmental and financial consequences of food waste for the schools in the municipality. Teachers and students were encouraged to not take more food than they thought they could manage to eat and school kitchens were instructed on how to better plan the cooking. The schools participating reduced their food waste by just over 10 percent. Other schools competed in who could reduce food waste the most. Both school kitchen and school cafeteria waste was included. The result was 11 percent less food thrown into the bins. Others aimed to reduce salad waste, by only bringing out a little at a time to the buffet and refilling it often. 
   
 Follow us on Twitter 
 Tweets by @EU_FUSIONS 
 FUSIONS Members 
 Login 
 Get involved with FUSIONS! 
 Become a FUSIONS Member : by doing so, you will be  able to use our forum and contribute more actively to the FUSIONS community! 
 Contribute to the FUSIONS forum : tell us more about your socially innovative project, debate about policy drivers and barriers to reducing food waste, learn more about food waste quantification… Your contribution may be published in our next  Newsletter ! 
 Submit your own socially innovative projects to the FUSIONS team : help us promote best practices and share good ideas all across the EU! 
 Submit your event to FUSIONS Events page : give your event more visibility!      
 For any other queries, please contact us via email at  This email address is being protected from spambots. You need JavaScript enabled to view it.   
  Copyright © 2016 | EU FUSIONS |  Contact us 
This project has received funding from the European Union’s Seventh Framework Programme for research, technological development and demonstration under grant agreement no 311972. 
“The views and opinions expressed in this website are purely those of the writers and may not in any circumstances be regarded as stating an official position of the European Commission.” 

 A campaign for the collection and organisation of waste was arranged through a combination of public service and engagement by the population. All key actors have been activated facilitate minimisation of wasteful food usage. 

 A campaign for the collection and organisation of waste was arranged through a combination of public service and engagement by the population. All key actors have been activated facilitate minimisation of wasteful food usage.
 WHOLE PROJECT MARK