Myanmar - FXB English
===============================

   <![endif] 
 Skip to content &raquo; 
  #accessibility  
  nav icons  
 Facebook 
 Twitter 
 Instagram 
 Youtube 
 
				<div class="col-sm-6 col-sm-7 | icons">
					<ul>
						<li><a class="facebook" href="https://www.facebook.com/FXBinternational" title="Facebook"><span class="sr-only">Facebook</span><i class="fa fa-facebook"></i></a></li>
						<li><a class="twitter" href="https://twitter.com/FXBIntl" title="Twitter"><span class="sr-only">Twitter</span><i class="fa fa-twitter"></i></a></li>
						<li><a class="search toggle-search" href="#" title="Search">Search<i class="fa fa-search"></i></a></li>
			            			              <li><a class="language toggle-language" href="#" title="Select Language">Select Language<i class="fa fa-language"></i></a></li>
			                      			</ul>         			
				</div>
				 
  /nav icons  
  actions  
 Español 
  |  
 Français 
 Search  
 Sign up 
 Donate  
  /actions  
  branding and mobile menu  
 FXB English 
 MENU 
  /branding and mobile menu  
  .global-header 
  nav for large screens  
 Who we are 
 FXB  International  in action 
 Our Vision and Mission 
 Our Founder and History 
 Our Board 
 Our Team 
 Ethics Charter 
 Donors &#038; Partners 
 Transparency 
 FXB  Foundation 
 FXB USA 
 FXB Center for Health &#038; Human Rights 
 Frequently Asked Questions 
 What we do 
 Domains of Intervention 
 FXBVillage Methodology 
 FXBVillage Toolkit 
 Measuring Impacts 
 Where we work 
 Current &#038; Past Work 
 Newsroom 
 FXB News 
 Real Life Stories 
 Events 
 In the Media 
 Videos and Photos 
 Newsletters 
 Publications 
 Testimonials from Experts 
 Media Contacts 
 Media Kit 
 Get involved 
 Donate 
 Take Action 
 Contact Us 
  #global-navigation for large screens  
                 menu de merde foundation 
 The FXB Association 
  #global-navigation for large screens  
  .global-header-wrapper  
 FXB in action 
 FXB has provided essential assistance in Myanmar since 1992. The organization developed programs related to health, education, vocational training and income generating activities, organized HIV/AIDS and human-trafficking campaigns, and has undertaken emergency and reconstruction operations following floods and cyclone Nargis. 
 In 2016, FXB assisted over 17,000 vulnerable people. Currently, FXB leads several main projects in Myanmar: 
 Vocational Training and Life Skills aimed at Young People at Risk 
 The objective of this project is to improve access to education and promote self-sufficiency for  vulnerable young people disadvantaged in regards with poverty, health, education, gender, and displacement. FXB offers three different kinds of professional training: 
 In Shwe Pyi Thar (township of Yangon), between 80 and 150 students are annually trained in  weaving, tailoring, interior decoration, furniture making, as well as metal and wood work . The training allows them to acquire the needed skills to find a job in a factory or to open their own workshop. 
 In Ngapali, the  FXB Hospitality Vocational Training Program  is set to provide training opportunities for 50 disadvantaged youth per year to prepare them to work in the hospitality industry in particular, and tourism in general. At the end of their training, the students have the possibility to be assessed by the National Skills Standards Authority (NSSA) for their competencies to be certified. 
 Mobile Training Units  in remote areas of Myanmar allow vulnerable young women to receive professional training in garment making that will improve their competencies and their attractiveness in the labor market. 
 Informal Education for Children 
 The FXB Training Center for children, located in Shwe Pyi Thar, one hour and a half away from Yangon, also daily welcomes 30 out of school and street children and provides them with informal education. 
 Community Development Program 
 A  FXBVillage  Program is ongoing in Môn State, an area benefiting from a ceasefire agreement with the government. The latter will lead 489 people to economic and social autonomy within three years and will also promote peacebuilding activities in the region. 
 Community-Led Development through Theater Capacity Training 
 The project is in its fourth year and so far the troupe, made up of twelve young performers and two theater Managers has conducted over 300 performances, reaching more than 37,000 community members from Yangon, Tanintharyi, Kachin, Mon and Kayin States. The theatre performances target issues focusing on both women and children&#8217;s rights. 
 Reproductive Health and HIV/AIDS Information for Young People 
 FXB implements a large project on “Promoting Access to Adolescent Reproductive Health and HIV/AIDS Information” which informs adolescents and youth about reproductive health and HIV/AIDS. 
 Capacity building training on Reproductive Health, Gender and HIV/AIDS are also provided to hundreds of young people every year. 
 Sunday Empowerment Groups 
 Through self-help and Sunday empowerment groups, FXB offers psychological support to people living with HIV/AIDS and their impacted families. 
 FXB Showroom (Yangon) 
 A showroom sells tapestries, curtain s, furniture, clothes and toys, made by 40 people holding a FXB training diploma. The profits are used for their salaries, to buy raw materials, and to provide a small allowance to FXB&#8217;s apprentices. 
 Address of the showroom:  No.294/3, Shwe Gon Daing Road. 
Middle Shwe Gon Daing Ward. In Front of Nga Htat Gyi Pagoda 
Bahan Township. Yangon. Myanmar &#8211; Tel: +95 9 73016552 
   
 Response to Gender-Based Violence 
 A new project, &#8220;Immediate and Rapid Response to Gender-Based Violence&#8221; reached the most affected areas in the Ayeyarwady Region through the provision of mobile case management and psychosocial support. 
 Policy dialogue related to Gender-Based Violence is also facilitated at National, State and Regional level. 
 Background 
 With a population of over 54 million people, Myanmar is among the world’s poorest countries, ranking 148th out of 188 countries in the Human Development Index (2015). The 2015 elections marked a historic milestone in the country’s political and economic transition that began in 2011. The opposition National League for Democracy (NLD) swept into power, leading to the parliament’s election of Myanmar’s first civilian state leader in decades. 
 Although some areas in Myanmar are reforming by all means, poverty levels are at an estimated one fourth of the population. Poverty is twice as high in rural areas where 70% of the population lives. The remote border areas, mainly populated by Myanmar’s minority ethnic groups, as well as areas emerging from conflict are particularly poor (Human Poverty Report 2014). 
 The tourism industry is overstrained. Lack of qualified staff is the most obvious challenge, as tourism is expected to boom in the coming years. Institutional research predicted that the tourism sector will need an additional 2 million employees by 2030 to provide services in line with demands. Without  support and training, vulnerable populations will not be able to seize those opportunities. 
 Download our 2017 Factsheet:   FXB Myanmar 
   
 Burundi  &#8211;  Burkina Faso  &#8211;  China &#8211;   Colombia &#8211;   India &#8211;   Mongolia &#8211;   Myanmar &#8211;   Niger &#8211;   Namibia  &#8211;  Rwanda &#8211;   South Africa  &#8211;  Uganda 
  #content  
  #content-wrapper  
 For more information FXB  International 
 FXB USA 
 FXB  Foundation 
 FXB Center for Health &#038; Human Rights – Harvard University 
 FXB India Suraksha 
 Contact Us 
  .widget  
  #footer  
  #footer-wrapper  
  #wrapper .hfeed  
  #wp-footer  
  modal menus  
 Who we are 
 FXB  International  in action 
 FXB  Foundation 
 FXB USA 
 FXB Center for Health &#038; Human Rights 
 Frequently Asked Questions 
 What we do 
 Domains of Intervention 
 FXBVillage Methodology 
 FXBVillage Toolkit 
 Measuring Impacts 
 Where we work 
 Current &#038; Past Work 
 Newsroom 
 FXB News 
 Real Life Stories 
 Events 
 In the Media 
 Videos and Photos 
 Newsletters 
 Publications 
 Testimonials from Experts 
 Media Contacts 
 Media Kit 
 Get involved 
 Donate 
 Take Action 
 Contact Us 
 Español 
 Français 
 English 
 Search for: 
 Search 
   
  /modal menus  
 Top 
Myanmar - FXB English
===============================

   <![endif] 
 Skip to content &raquo; 
  #accessibility  
  nav icons  
 Facebook 
 Twitter 
 Instagram 
 Youtube 
 
				<div class="col-sm-6 col-sm-7 | icons">
					<ul>
						<li><a class="facebook" href="https://www.facebook.com/FXBinternational" title="Facebook"><span class="sr-only">Facebook</span><i class="fa fa-facebook"></i></a></li>
						<li><a class="twitter" href="https://twitter.com/FXBIntl" title="Twitter"><span class="sr-only">Twitter</span><i class="fa fa-twitter"></i></a></li>
						<li><a class="search toggle-search" href="#" title="Search">Search<i class="fa fa-search"></i></a></li>
			            			              <li><a class="language toggle-language" href="#" title="Select Language">Select Language<i class="fa fa-language"></i></a></li>
			                      			</ul>         			
				</div>
				 
  /nav icons  
  actions  
 Español 
  |  
 Français 
 Search  
 Sign up 
 Donate  
  /actions  
  branding and mobile menu  
 FXB English 
 MENU 
  /branding and mobile menu  
  .global-header 
  nav for large screens  
 Who we are 
 FXB  International  in action 
 Our Vision and Mission 
 Our Founder and History 
 Our Board 
 Our Team 
 Ethics Charter 
 Donors &#038; Partners 
 Transparency 
 FXB  Foundation 
 FXB USA 
 FXB Center for Health &#038; Human Rights 
 Frequently Asked Questions 
 What we do 
 Domains of Intervention 
 FXBVillage Methodology 
 FXBVillage Toolkit 
 Measuring Impacts 
 Where we work 
 Current &#038; Past Work 
 Newsroom 
 FXB News 
 Real Life Stories 
 Events 
 In the Media 
 Videos and Photos 
 Newsletters 
 Publications 
 Testimonials from Experts 
 Media Contacts 
 Media Kit 
 Get involved 
 Donate 
 Take Action 
 Contact Us 
  #global-navigation for large screens  
                 menu de merde foundation 
 The FXB Association 
  #global-navigation for large screens  
  .global-header-wrapper  
 FXB in action 
 FXB has provided essential assistance in Myanmar since 1992. The organization developed programs related to health, education, vocational training and income generating activities, organized HIV/AIDS and human-trafficking campaigns, and has undertaken emergency and reconstruction operations following floods and cyclone Nargis. 
 In 2016, FXB assisted over 17,000 vulnerable people. Currently, FXB leads several main projects in Myanmar: 
 Vocational Training and Life Skills aimed at Young People at Risk 
 The objective of this project is to improve access to education and promote self-sufficiency for  vulnerable young people disadvantaged in regards with poverty, health, education, gender, and displacement. FXB offers three different kinds of professional training: 
 In Shwe Pyi Thar (township of Yangon), between 80 and 150 students are annually trained in  weaving, tailoring, interior decoration, furniture making, as well as metal and wood work . The training allows them to acquire the needed skills to find a job in a factory or to open their own workshop. 
 In Ngapali, the  FXB Hospitality Vocational Training Program  is set to provide training opportunities for 50 disadvantaged youth per year to prepare them to work in the hospitality industry in particular, and tourism in general. At the end of their training, the students have the possibility to be assessed by the National Skills Standards Authority (NSSA) for their competencies to be certified. 
 Mobile Training Units  in remote areas of Myanmar allow vulnerable young women to receive professional training in garment making that will improve their competencies and their attractiveness in the labor market. 
 Informal Education for Children 
 The FXB Training Center for children, located in Shwe Pyi Thar, one hour and a half away from Yangon, also daily welcomes 30 out of school and street children and provides them with informal education. 
 Community Development Program 
 A  FXBVillage  Program is ongoing in Môn State, an area benefiting from a ceasefire agreement with the government. The latter will lead 489 people to economic and social autonomy within three years and will also promote peacebuilding activities in the region. 
 Community-Led Development through Theater Capacity Training 
 The project is in its fourth year and so far the troupe, made up of twelve young performers and two theater Managers has conducted over 300 performances, reaching more than 37,000 community members from Yangon, Tanintharyi, Kachin, Mon and Kayin States. The theatre performances target issues focusing on both women and children&#8217;s rights. 
 Reproductive Health and HIV/AIDS Information for Young People 
 FXB implements a large project on “Promoting Access to Adolescent Reproductive Health and HIV/AIDS Information” which informs adolescents and youth about reproductive health and HIV/AIDS. 
 Capacity building training on Reproductive Health, Gender and HIV/AIDS are also provided to hundreds of young people every year. 
 Sunday Empowerment Groups 
 Through self-help and Sunday empowerment groups, FXB offers psychological support to people living with HIV/AIDS and their impacted families. 
 FXB Showroom (Yangon) 
 A showroom sells tapestries, curtain s, furniture, clothes and toys, made by 40 people holding a FXB training diploma. The profits are used for their salaries, to buy raw materials, and to provide a small allowance to FXB&#8217;s apprentices. 
 Address of the showroom:  No.294/3, Shwe Gon Daing Road. 
Middle Shwe Gon Daing Ward. In Front of Nga Htat Gyi Pagoda 
Bahan Township. Yangon. Myanmar &#8211; Tel: +95 9 73016552 
   
 Response to Gender-Based Violence 
 A new project, &#8220;Immediate and Rapid Response to Gender-Based Violence&#8221; reached the most affected areas in the Ayeyarwady Region through the provision of mobile case management and psychosocial support. 
 Policy dialogue related to Gender-Based Violence is also facilitated at National, State and Regional level. 
 Background 
 With a population of over 54 million people, Myanmar is among the world’s poorest countries, ranking 148th out of 188 countries in the Human Development Index (2015). The 2015 elections marked a historic milestone in the country’s political and economic transition that began in 2011. The opposition National League for Democracy (NLD) swept into power, leading to the parliament’s election of Myanmar’s first civilian state leader in decades. 
 Although some areas in Myanmar are reforming by all means, poverty levels are at an estimated one fourth of the population. Poverty is twice as high in rural areas where 70% of the population lives. The remote border areas, mainly populated by Myanmar’s minority ethnic groups, as well as areas emerging from conflict are particularly poor (Human Poverty Report 2014). 
 The tourism industry is overstrained. Lack of qualified staff is the most obvious challenge, as tourism is expected to boom in the coming years. Institutional research predicted that the tourism sector will need an additional 2 million employees by 2030 to provide services in line with demands. Without  support and training, vulnerable populations will not be able to seize those opportunities. 
 Download our 2017 Factsheet:   FXB Myanmar 
   
 Burundi  &#8211;  Burkina Faso  &#8211;  China &#8211;   Colombia &#8211;   India &#8211;   Mongolia &#8211;   Myanmar &#8211;   Niger &#8211;   Namibia  &#8211;  Rwanda &#8211;   South Africa  &#8211;  Uganda 
  #content  
  #content-wrapper  
 For more information FXB  International 
 FXB USA 
 FXB  Foundation 
 FXB Center for Health &#038; Human Rights – Harvard University 
 FXB India Suraksha 
 Contact Us 
  .widget  
  #footer  
  #footer-wrapper  
  #wrapper .hfeed  
  #wp-footer  
  modal menus  
 Who we are 
 FXB  International  in action 
 FXB  Foundation 
 FXB USA 
 FXB Center for Health &#038; Human Rights 
 Frequently Asked Questions 
 What we do 
 Domains of Intervention 
 FXBVillage Methodology 
 FXBVillage Toolkit 
 Measuring Impacts 
 Where we work 
 Current &#038; Past Work 
 Newsroom 
 FXB News 
 Real Life Stories 
 Events 
 In the Media 
 Videos and Photos 
 Newsletters 
 Publications 
 Testimonials from Experts 
 Media Contacts 
 Media Kit 
 Get involved 
 Donate 
 Take Action 
 Contact Us 
 Español 
 Français 
 English 
 Search for: 
 Search 
   
  /modal menus  
 Top 

 Addresses the growing issue of human trafficking for the Thai sex industry. Offers individualised reintegration depending on each individuals aspirations and skills. Helps the girls find apprenticeships or start a small-scale business. 

 Addresses the growing issue of human trafficking for the Thai sex industry. Offers individualised reintegration depending on each individuals aspirations and skills. Helps the girls find apprenticeships or start a small-scale business.
 WHOLE PROJECT MARK