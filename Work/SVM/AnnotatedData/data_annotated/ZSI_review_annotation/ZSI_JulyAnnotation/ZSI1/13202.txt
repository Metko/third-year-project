GoodGym
===============================

 
  none  
 Sign in 
 Do good, get fit 
 
We're a community of runners that combines getting fit with doing good.
 
Join us and find your reason to run.
 
 Get Started 
 Mission Runs 
 Run to help older people with one-off tasks 
 Find out more 
 Coach Runs 
 Run regularly to see an isolated older person 
 Find out more 
 Group Runs 
 Run with a group to help community projects 
 Find out more 
 Find your reason to run 
 Search 
 
Before GoodGym I always lost motivation to stick at running or the gym
but now I run two or three times a week.
 
 
Hazel, GoodGym Barnet Runner
 
 
Running with Goodgym is great because I get fit, do good and get to
hang with a load of awesome people.
 
 
Mark, GoodGym Islington runner
 
 
GoodGym combines doing good with running, which helps me forget I'm
even exercising. It's great!
 
 
Judy, GoodGym Lambeth runner
 
 92,083 
Good deeds done
 
 Areas 
 44 
Areas running
 
 4 
Starting soon
 
 73 
In proposal
 
 
We are a community of runners that combine getting fit with doing good.
We stop off on our runs to do physical tasks for community organisations
and to support isolated older people with social visits and one-off
tasks they can't do on their own.
It's a great way to get fit, meet new people and do some good.
As long as you're up for getting sweaty, everyone's welcome.
 
 About GoodGym 
 
If you're lacking motivation when it comes to fitness,
why not get fit by accident?
 
 
GoodGym offers an imaginative alternative to conventional gyms,
tapping into the human potential of local communities.
 
 
Why burn energy in the gym when you could harness that effort to
spruce up public spaces, or visit lonely pensioners?
 
 
The World's most innovative Aging projects.
 
 
An inspired way to experience the feelgood factor.
 
 Find your reason to run 
 Search 
 Supported by 
 About 
 FAQs 
 Jobs 
 Contact us 
 Somerset House, Strand, London, WC2R 1LA 
 Reg. Charity 1160988 
 Terms of Service 
 Privacy Policy
 Updated 
 Cookies help us deliver our services. By using our services, you agree to our use of cookies. 
  I AGREE  
  Learn more  
GoodGym
===============================

 
  none  
 Sign in 
 Do good, get fit 
 
We're a community of runners that combines getting fit with doing good.
 
Join us and find your reason to run.
 
 Get Started 
 Mission Runs 
 Run to help older people with one-off tasks 
 Find out more 
 Coach Runs 
 Run regularly to see an isolated older person 
 Find out more 
 Group Runs 
 Run with a group to help community projects 
 Find out more 
 Find your reason to run 
 Search 
 
Before GoodGym I always lost motivation to stick at running or the gym
but now I run two or three times a week.
 
 
Hazel, GoodGym Barnet Runner
 
 
Running with Goodgym is great because I get fit, do good and get to
hang with a load of awesome people.
 
 
Mark, GoodGym Islington runner
 
 
GoodGym combines doing good with running, which helps me forget I'm
even exercising. It's great!
 
 
Judy, GoodGym Lambeth runner
 
 92,083 
Good deeds done
 
 Areas 
 44 
Areas running
 
 4 
Starting soon
 
 73 
In proposal
 
 
We are a community of runners that combine getting fit with doing good.
We stop off on our runs to do physical tasks for community organisations
and to support isolated older people with social visits and one-off
tasks they can't do on their own.
It's a great way to get fit, meet new people and do some good.
As long as you're up for getting sweaty, everyone's welcome.
 
 About GoodGym 
 
If you're lacking motivation when it comes to fitness,
why not get fit by accident?
 
 
GoodGym offers an imaginative alternative to conventional gyms,
tapping into the human potential of local communities.
 
 
Why burn energy in the gym when you could harness that effort to
spruce up public spaces, or visit lonely pensioners?
 
 
The World's most innovative Aging projects.
 
 
An inspired way to experience the feelgood factor.
 
 Find your reason to run 
 Search 
 Supported by 
 About 
 FAQs 
 Jobs 
 Contact us 
 Somerset House, Strand, London, WC2R 1LA 
 Reg. Charity 1160988 
 Terms of Service 
 Privacy Policy
 Updated 
 Cookies help us deliver our services. By using our services, you agree to our use of cookies. 
  I AGREE  
  Learn more
 WHOLE PROJECT MARK