EAT Initiative | EAT
===============================

 
 [if lt IE 8]>
    <div class="alert alert-warning">
      You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.    </div>
  <![endif] 
 * :  * 
 Toggle navigation 
 MENU 
  mobile search  
 Foundation 
 Events 
 Archive 
 Programmes 
 Press 
  Mobile menu  
 Foundation 
 What is EAT? 
 Who is EAT? 
 Board of Trustees 
 Advisory Board 
 Strategic Advisors 
 Partners 
 Team 
 Contact 
 Events 
 EAT Stockholm Food Forum 2018 
 EAT Asia-Pacific Food Forum 2017 
 EAT Stockholm Food Forum 2017 
 EATx UNGA 2016 
 EAT Stockholm Food Forum 2016 
 Archive 
 Videos 
 Articles 
 Programmes 
 C40 Food Systems Network 
 EAT-Lancet Commission 
 Nordic Cities EAT Initiative 
 FReSH 
 Press 
 Media Center 
 Press contact 
  tablet/desktop search  
 Search 
  Desktop menu  
 Foundation  ● Overview  
 What is EAT? 
 Who is EAT? 
 Board of Trustees 
 Advisory Board 
 Strategic Advisors 
 Partners 
 Team 
 Contact 
 Events  ● Overview  
 EAT Stockholm Food Forum 2018 
 EAT Asia-Pacific Food Forum 2017 
 EAT Stockholm Food Forum 2017 
 EATx UNGA 2016 
 EAT Stockholm Food Forum 2016 
 Archive  ● Overview  
 Videos 
 Articles 
 Programmes  ● Overview  
 C40 Food Systems Network 
 EAT-Lancet Commission 
 Nordic Cities EAT Initiative 
 FReSH 
 Press  ● Overview  
 Media Center 
 Press contact 
 
									The EAT Initiative							 
 
									Science, politics and business sharing food for thought							 
 
															Professor Johan Rockström													 
 EAT 2015 Opening talk by Prof. Johan Rockström 
  start (first) row, element 1   
 
																	Jonas Gahr Støre															 
 Why EAT is important 
 Notice :  Undefined offset: 0 in  /var/www/1/93561/www/wp-content/themes/eat3/lib/extras.php  on line  58 
 EATx event coincided with UN General Assembly 
 Foundation 
 What is EAT? 
 
							Read in-depth information about what EAT is.						 
 Read more 
  end row, element 3   
 
				What is EAT?			 
 Providing the growing global population a healthy and nutritious diet within safe environmental limits is one of the greatest challenges facing humanity today. It can only be addressed through an integration of knowledge and action in the interwoven areas of food, health and sustainability.  
 Read more 
 
				Board of Trustees			 
 EAT is governed and managed by a Board of Trustees. Each Core Partner is entitled to appoint two members of the Board. Additionally, there are two independent member of the Board, selected by the Core Partners. 
 Read more 
 
				Strategic Advisors			 
 EAT Strategic Advisors represent global experts - in fields including research, policy, economics and business. Attending EAT Advisory Board meetings and activities when relevant and available, or being consulted on an individual basis, EAT Strategic Advisors provide technical and operational advice on specific issues relating to core EAT activities. 
 Read more 
 
				Team			 
 The EAT Team is a small, hard working group of dedicated people with very different backgrounds. This is "The Dream Team" as Gunhild calls them. 
 Read more 
 
				Awards (new)			 
 Read more 
 
				Who is EAT?			 
 EAT Foundation is founded by the Stordalen Foundation, Wellcome Trust and Stockholm Resilience Centre. 
 Read more 
 
				Advisory Board			 
 The advisory board reflects the cross disciplinary, multinational approach of EAT through a carefully selected group of some of the world’s most renowned experts in their respective fields, from science to business and politics. 
 Read more 
 
				Awards			 
 EAT strongly believes that rewarding innovation is an important way to find future solutions for healthy and sustainable food. The EAT Awards are instituted to promote and reward ideas and solutions from all around the world. 
 Read more 
 
				Partners			 
 EAT is partnering with a range of foundations, organisations and companies from the academic, public, business and civil society sectors, who serve to provide strategic advice and financial support to EAT. Corporate partners are carefully selected companies that have a clear sustainability profile and a strong focus on healthy and sustainable food products or production practices. EAT delivers unique value to its partners through its collaborative network, annual and regional meetings and other activities. 
 Read more 
 
				Contact			 
 The EAT office is located at Kongens gate 11, 0152 Oslo. 
Send us an email and let us know what you have on your mind, and we'll get back to you as soon as we can. 
 Read more 
 Learn more 
 Read more 
 <footer class="content-info" role="contentinfo">
  <div class="container">
      </div>
</footer> 
 EAT 
 Events 
 Previous events 
 Initiative 
 What is EAT? 
 Who is EAT? 
 Board of Trustees 
 Advisory Board 
 Strategic Advisors 
 Team 
 Partners 
 Awards 
 Contact 
 Awards (new) 
 Events 
 EAT Stockholm Food Forum 2018 
 EAT Asia-Pacific Food Forum 2017 
 EAT Stockholm Food Forum 2017 
 EATx UNGA 2016 
 EAT Stockholm Food Forum 2016 
 EATx Cali 
 EATx post 2015 
 EAT MOVE SLEEP Arendalsuka 2015 
 Stockholm Food Forum 2015 
 EATx Middle East and North Africa 
 EATx UNGA 2014 
 Stockholm Food Forum 2014 
 Archive 
 Videos 
 Articles 
 Press 
 Press releases 
 Event accreditation 
 Press contact 
 Contact 
 
				Questions? Feel free to contact us.
			 
 info@eatforum.org 
    /.main  
  /.wrap  
EAT Initiative | EAT
===============================

 
 [if lt IE 8]>
    <div class="alert alert-warning">
      You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.    </div>
  <![endif] 
 * :  * 
 Toggle navigation 
 MENU 
  mobile search  
 Foundation 
 Events 
 Archive 
 Programmes 
 Press 
  Mobile menu  
 Foundation 
 What is EAT? 
 Who is EAT? 
 Board of Trustees 
 Advisory Board 
 Strategic Advisors 
 Partners 
 Team 
 Contact 
 Events 
 EAT Stockholm Food Forum 2018 
 EAT Asia-Pacific Food Forum 2017 
 EAT Stockholm Food Forum 2017 
 EATx UNGA 2016 
 EAT Stockholm Food Forum 2016 
 Archive 
 Videos 
 Articles 
 Programmes 
 C40 Food Systems Network 
 EAT-Lancet Commission 
 Nordic Cities EAT Initiative 
 FReSH 
 Press 
 Media Center 
 Press contact 
  tablet/desktop search  
 Search 
  Desktop menu  
 Foundation  ● Overview  
 What is EAT? 
 Who is EAT? 
 Board of Trustees 
 Advisory Board 
 Strategic Advisors 
 Partners 
 Team 
 Contact 
 Events  ● Overview  
 EAT Stockholm Food Forum 2018 
 EAT Asia-Pacific Food Forum 2017 
 EAT Stockholm Food Forum 2017 
 EATx UNGA 2016 
 EAT Stockholm Food Forum 2016 
 Archive  ● Overview  
 Videos 
 Articles 
 Programmes  ● Overview  
 C40 Food Systems Network 
 EAT-Lancet Commission 
 Nordic Cities EAT Initiative 
 FReSH 
 Press  ● Overview  
 Media Center 
 Press contact 
 
									The EAT Initiative							 
 
									Science, politics and business sharing food for thought							 
 
															Professor Johan Rockström													 
 EAT 2015 Opening talk by Prof. Johan Rockström 
  start (first) row, element 1   
 
																	Jonas Gahr Støre															 
 Why EAT is important 
 Notice :  Undefined offset: 0 in  /var/www/1/93561/www/wp-content/themes/eat3/lib/extras.php  on line  58 
 EATx event coincided with UN General Assembly 
 Foundation 
 What is EAT? 
 
							Read in-depth information about what EAT is.						 
 Read more 
  end row, element 3   
 
				What is EAT?			 
 Providing the growing global population a healthy and nutritious diet within safe environmental limits is one of the greatest challenges facing humanity today. It can only be addressed through an integration of knowledge and action in the interwoven areas of food, health and sustainability.  
 Read more 
 
				Board of Trustees			 
 EAT is governed and managed by a Board of Trustees. Each Core Partner is entitled to appoint two members of the Board. Additionally, there are two independent member of the Board, selected by the Core Partners. 
 Read more 
 
				Strategic Advisors			 
 EAT Strategic Advisors represent global experts - in fields including research, policy, economics and business. Attending EAT Advisory Board meetings and activities when relevant and available, or being consulted on an individual basis, EAT Strategic Advisors provide technical and operational advice on specific issues relating to core EAT activities. 
 Read more 
 
				Team			 
 The EAT Team is a small, hard working group of dedicated people with very different backgrounds. This is "The Dream Team" as Gunhild calls them. 
 Read more 
 
				Awards (new)			 
 Read more 
 
				Who is EAT?			 
 EAT Foundation is founded by the Stordalen Foundation, Wellcome Trust and Stockholm Resilience Centre. 
 Read more 
 
				Advisory Board			 
 The advisory board reflects the cross disciplinary, multinational approach of EAT through a carefully selected group of some of the world’s most renowned experts in their respective fields, from science to business and politics. 
 Read more 
 
				Awards			 
 EAT strongly believes that rewarding innovation is an important way to find future solutions for healthy and sustainable food. The EAT Awards are instituted to promote and reward ideas and solutions from all around the world. 
 Read more 
 
				Partners			 
 EAT is partnering with a range of foundations, organisations and companies from the academic, public, business and civil society sectors, who serve to provide strategic advice and financial support to EAT. Corporate partners are carefully selected companies that have a clear sustainability profile and a strong focus on healthy and sustainable food products or production practices. EAT delivers unique value to its partners through its collaborative network, annual and regional meetings and other activities. 
 Read more 
 
				Contact			 
 The EAT office is located at Kongens gate 11, 0152 Oslo. 
Send us an email and let us know what you have on your mind, and we'll get back to you as soon as we can. 
 Read more 
 Learn more 
 Read more 
 <footer class="content-info" role="contentinfo">
  <div class="container">
      </div>
</footer> 
 EAT 
 Events 
 Previous events 
 Initiative 
 What is EAT? 
 Who is EAT? 
 Board of Trustees 
 Advisory Board 
 Strategic Advisors 
 Team 
 Partners 
 Awards 
 Contact 
 Awards (new) 
 Events 
 EAT Stockholm Food Forum 2018 
 EAT Asia-Pacific Food Forum 2017 
 EAT Stockholm Food Forum 2017 
 EATx UNGA 2016 
 EAT Stockholm Food Forum 2016 
 EATx Cali 
 EATx post 2015 
 EAT MOVE SLEEP Arendalsuka 2015 
 Stockholm Food Forum 2015 
 EATx Middle East and North Africa 
 EATx UNGA 2014 
 Stockholm Food Forum 2014 
 Archive 
 Videos 
 Articles 
 Press 
 Press releases 
 Event accreditation 
 Press contact 
 Contact 
 
				Questions? Feel free to contact us.
			 
 info@eatforum.org 
    /.main  
  /.wrap 

 EAT engages stakeholders to achieve transformative change of the global food system to sustainably feed a healthy population of nine billion people by mid-Century. 

 EAT engages stakeholders to achieve transformative change of the global food system to sustainably feed a healthy population of nine billion people by mid-Century.
 WHOLE PROJECT MARK