SHE Empowerment | B92 Fund
===============================

 
 
                    English |                                  Srpski 
  header social menu start  
 Twitter Facebook Youtube   
  header social menu end  
 B92 Fund 
 Menu 
 Home Floods! Projects Projects Battle for the Babies 65 plus Movement Re:Genaration Free zone festival Free zone junior Safer Internet Center SHE Empowerment Food for all Rex Safe house The campaign against breast cancer Corporate social responsibility activities Back to the Past Battle for knowledge News About us Contact 
   
 
        SHE Empowerment – Safe House Economic Empowerment     
   
 Empowered women, violence survivors from West Bačka District, are working to become independent providers. Safe House Economic Empowerment project (SHE-Empowerment), financed by UN Trust Fund to End Violence against Women, is focused on empowerment of women survivors of violence toward employment and self-employment through the provision of new service of educational and mentoring program in Safe Houses. This is the final missing link in current institutional response to female survivors in Serbia &mdash; providing women with economic empowerment along with currently provided physical and psychological help, thus making that help more effective. 
 Instead of hiding from their violent partners, accepting any given job and struggling through life with little to count on, women will be provided with support that will help them achieve economic independence (from their violent partners) and live their lives independently. Women survivors will increase their knowledge/skills and capacities to engage in (self)employment of their own choice and will at the same time institutionally provide them with a slight initial lead in the labour market. 
 SHE-Empowerment will pilot a new service for economic empowerment in one Safe House in Serbia (City of Sombor).New service will include provision of tailor-made trainings/courses for current/former prot&eacute;g&eacute;s of Sombor Safe House (SOSH) and networking with private sector actors that are in position and willing to invest resources in women&rsquo;s economic empowerment. Through trainings and courses women will gain knowledge, skills, and self-confidence to tackle the problems of economic existence. The connections with private sector will be accomplished through establishment of SOSH Network of socially responsible businessmen, employers, banks and other private sector representatives. To make this new educational service self-sustainable after SHE-Empowerment project ends, a Social Purpose Organization (SPO) will be established and managed by women within SOSH, with the purpose of investing its profits in education for economic empowerment of new SOSH residents. Out of many revised options, organic vegetable production in greenhouses was chosen for SPO as the most appropriate for women, since it is not hard work farming and since working with land and growing plants has therapeutic effect. At the end of the project, the practice will be presented to decision makers, for transferring to other Safe Houses in Serbia. 
 Fund B92 is the Project lead and there are two partner organizations on this Project: Centre for Social Welfare Sombor and SMART Kolektiv. Centre for Social Welfare Sombor manages the Sombor Safe House and is a pioneer in introducing integrated institutional response to violence against women. SMART Kolektiv CSO is experienced in development of SPOs and with developed wide network of private sector actors. A number of CSOs also expressed their interest in the project and will be a valuable help during its implementation. 
 Provincial Secretariat for Economy, Employment and Gender Equality and Provincial Secretariat for Agriculture, Forestry and Water Management will provide institutional support to the new service in the Province of Vojvodina that is innovative for Serbia and that can be transferred to other Safe Houses in the country.   
     
 Office: 
 7 Hilandarska Street, 11000 Belgrade Serbia Tel/fax:   +381 (0)11 3284 534 
   
 Accounts: 
 265-1100310003520-62, Raiffeisen bank a.d.Belgrade 
 340-36233-65, Erste bank a.d. Novi Sad 
   
 Company Registration Number: 17539744 
 Tax identification Number: 103281849 
   
 www.fondb92.org 
 www.donacije.rs 
   
 &copy; 2016 Fond B92. All rights reserved.   
 Web development jazzieweb.com 
  /div#page  
  /div.canvas  
  Share this begin  
  Share this end  
SHE Empowerment | B92 Fund
===============================

 
 
                    English |                                  Srpski 
  header social menu start  
 Twitter Facebook Youtube   
  header social menu end  
 B92 Fund 
 Menu 
 Home Floods! Projects Projects Battle for the Babies 65 plus Movement Re:Genaration Free zone festival Free zone junior Safer Internet Center SHE Empowerment Food for all Rex Safe house The campaign against breast cancer Corporate social responsibility activities Back to the Past Battle for knowledge News About us Contact 
   
 
        SHE Empowerment – Safe House Economic Empowerment     
   
 Empowered women, violence survivors from West Bačka District, are working to become independent providers. Safe House Economic Empowerment project (SHE-Empowerment), financed by UN Trust Fund to End Violence against Women, is focused on empowerment of women survivors of violence toward employment and self-employment through the provision of new service of educational and mentoring program in Safe Houses. This is the final missing link in current institutional response to female survivors in Serbia &mdash; providing women with economic empowerment along with currently provided physical and psychological help, thus making that help more effective. 
 Instead of hiding from their violent partners, accepting any given job and struggling through life with little to count on, women will be provided with support that will help them achieve economic independence (from their violent partners) and live their lives independently. Women survivors will increase their knowledge/skills and capacities to engage in (self)employment of their own choice and will at the same time institutionally provide them with a slight initial lead in the labour market. 
 SHE-Empowerment will pilot a new service for economic empowerment in one Safe House in Serbia (City of Sombor).New service will include provision of tailor-made trainings/courses for current/former prot&eacute;g&eacute;s of Sombor Safe House (SOSH) and networking with private sector actors that are in position and willing to invest resources in women&rsquo;s economic empowerment. Through trainings and courses women will gain knowledge, skills, and self-confidence to tackle the problems of economic existence. The connections with private sector will be accomplished through establishment of SOSH Network of socially responsible businessmen, employers, banks and other private sector representatives. To make this new educational service self-sustainable after SHE-Empowerment project ends, a Social Purpose Organization (SPO) will be established and managed by women within SOSH, with the purpose of investing its profits in education for economic empowerment of new SOSH residents. Out of many revised options, organic vegetable production in greenhouses was chosen for SPO as the most appropriate for women, since it is not hard work farming and since working with land and growing plants has therapeutic effect. At the end of the project, the practice will be presented to decision makers, for transferring to other Safe Houses in Serbia. 
 Fund B92 is the Project lead and there are two partner organizations on this Project: Centre for Social Welfare Sombor and SMART Kolektiv. Centre for Social Welfare Sombor manages the Sombor Safe House and is a pioneer in introducing integrated institutional response to violence against women. SMART Kolektiv CSO is experienced in development of SPOs and with developed wide network of private sector actors. A number of CSOs also expressed their interest in the project and will be a valuable help during its implementation. 
 Provincial Secretariat for Economy, Employment and Gender Equality and Provincial Secretariat for Agriculture, Forestry and Water Management will provide institutional support to the new service in the Province of Vojvodina that is innovative for Serbia and that can be transferred to other Safe Houses in the country.   
     
 Office: 
 7 Hilandarska Street, 11000 Belgrade Serbia Tel/fax:   +381 (0)11 3284 534 
   
 Accounts: 
 265-1100310003520-62, Raiffeisen bank a.d.Belgrade 
 340-36233-65, Erste bank a.d. Novi Sad 
   
 Company Registration Number: 17539744 
 Tax identification Number: 103281849 
   
 www.fondb92.org 
 www.donacije.rs 
   
 &copy; 2016 Fond B92. All rights reserved.   
 Web development jazzieweb.com 
  /div#page  
  /div.canvas  
  Share this begin  
  Share this end 

 The main objective of the project was to develop a self-sustainable economic empowerment program for women survivors of domestic violence as a new service offered by the shelter in Western Backa county in the Republic of Serbia. 

 The main objective of the project was to develop a self-sustainable economic empowerment program for women survivors of domestic violence as a new service offered by the shelter in Western Backa county in the Republic of Serbia.
 WHOLE PROJECT MARK