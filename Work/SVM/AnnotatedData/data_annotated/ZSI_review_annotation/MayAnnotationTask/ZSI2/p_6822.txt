PROJECT NAME: Aka-Aki
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

MNT – It's More Fun To Compute
===============================

MNT   It's More Fun To Compute

 News 
 Clients 
 About 
 Contact 
 Shop 

 Welcome to MNT. 
  Boutique Computing Since 1984 
 Latest News 

 2018 
 Reform: Open Source DIY ARM Laptop. 
 MNT introduces a new, open computer system. 

 Article about the first prototype 
 Newsletter 
 GitHub 

 2018 
 MNT acquires aka-aki. 

              2018-03-14: MNT acquires the brand assets of 2000s era mobile social network  aka-aki . An internet "mooseum" is planned.  Get a moose T-Shirt 

 2016, 2017 
 VA2000: Open Source FPGA Graphics Card for Amiga Computers. 

 GitHub 
 YouTube 
 Hackaday 
 Kotaku 
 Xilinx 
 Firmware   Drivers 

 2016, 2017 
 MusicSuit. 
 A MIDI suit with wireless inertial measurement units (IMUs). Collaboration with fashion designer  Greta Melnik  Finalist of Telekom Fashion Fusion competition. 

 Interview Magazin   Electric Runway 

 2016, 2014 
 Interface Critique: Minimalismus als Strategie zu einem aufgekl rten Computerdesign 

 Book: Interface Critique 
 Amazon 
 Talk at UdK Berlin 

 2015 
 Interim OS  The Interim Lisp Operating System

 interim-os.com 
 GitHub 

 2015, May 6 
 Source Code Reading (Quelltextlesung) live at Re:publica 

 FAZ 
 S ddeutsche 
 Videos "Source Code Reading" 
 Recording of re:publica session 

 2014, Oct 2 
 Wikimedia-Salon "DATENBERG" 
 at Wikimedia Deutschland e.V.

 Info: Wikimedia 
 Recording on YouTube 

 2014, Oct 3-5   Commodore Internet Workshop  at Vintage Computing Festival Berlin

 Details: VCFB 

 2014, June 29   Digital Bauhaus Summit 

  Talk "Tools for Online Collaboration from the Developer s Perspective" 
 Stylepark 

 2013   Debugging my Genome (23andme) 

 german original 
 english translation 
 io9 
 quartz 
 S ddeutsche 
 CNN 
 Wired 
 Forbes 
 Focus 

 2010-2016   Fleischdolls  A German Cyber Punk Band 
 Albums at Discogs 

18.12.2010 Berlin, Trickster (With Grizou) 
11.03.2011 Berlin, Kaffee Burger 
05.05.2011 Berlin, Kingkongklub (With Brigade Rosse) 
17.06.2011 Berlin, White Trash Fast Food (With Dadajugend Polyform) 
11.11.2011 Berlin, 75 Jahre Kaffee burger 
06.03.2012 Berlin, Subversiv (Zur ck zum Beton) 
06.10.2012 Berlin, Haus Ungarn (Battle of the Startup Bands) 
15.11.2012 Berlin, Trickster 
25.05.2013 Berlin, White Trash Fast Food (With Unkindness of Ravens, Nikaya) 
19.07.2013 Hamburg, Gr nspan 
28.09.2013 Berlin, Betahaus (Battle of the Startup Bands 2) 

 Client Projects. 
  Back to top 
 Unendlicher Spa  / Unendliches Spiel. 

 Crowd-Recording Platform   Video Streaming.  For Andreas Ammer, Andreas Gerth and Acid Pauli, MNT developed the technology platform for crowd-recording and publishing the longest audiobook ever made: David Foster Wallace's Infinite Jest. The web-based platform allowed the authors to manage all 1500 pages of the book and let anyone record their voices to be part of the final work. The spectacular project is completed in February 2017.
           Website 

 Teledisko. 

 2015   Hardware/Software Design.   Teledisko  is the world's smallest discotheque. Integrated in refurbished telephone booths, Telediskos are coin-operated machines that supply you with a full-blown, short-time partying experience, complete with fog, lighting effects, video sharing and photo printing. MNT developed Teledisko's hardware/software operating system and graphical user interface (GUI) on GNU/Linux, SDL and Arduino platforms. Today, Teledisko is a successful, self-sustaining business.

 About. 
  Back to top 

          MNT Media and Technology UG (haftungsbeschr nkt) is a computer and media production company located in Berlin, Germany.

          It is the creative and business outlet of Lukas F. Hartmann.
           GitHub 
 Twitter 

 Reach Out. 
  Back to top 
 lukas@mntmn.com 

          MNT Media and Technology UG (haftungsbeschr nkt) 
          Anschrift: Bluecherstr. 32C, 10961 Berlin 
          Gesch ftsf hrer: Lukas F. Hartmann 
          Registergericht: Amtsgericht Charlottenburg 
          Aktenzeichen: HRB 136605 B 
          VAT ID: DE278908891 
          WEEE: DE 33315564 

 Aka Aki is a Germany-based mobile social networking site that wants to let users discover and connect with members as they go about their days. They have two different versions of the site, a mobile version and a web version. The web version has all the standard features like profiles, friends and messaging, but, the mobile version is much different with its Bluetooth-sensing capabilities 

Aka Aki is a Germany-based mobile social networking site that wants to let users discover and connect with members as they go about their days. They have two different versions of the site, a mobile version and a web version. The web version has all the standard features like profiles, friends and messaging, but, the mobile version is much different with its Bluetooth-sensing capabilities
 WHOLE PROJECT MARK