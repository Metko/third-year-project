PROJECT NAME: The FairCoin
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 
THE ECO-FRIENDLY COIN

FOR A FAIR ECONOMY

Our mission

To build a currency that is stable, global and incorruptible. Our aim is to create an innovative glocal economic system from the bottom up in favor of an alternative and post-capitalist model, paving the way for a collective change towards a life based on values in common. FairCoin is our means of exchange. Its value is determined by the community and it never devalues. The ongoing revaluation process of FairCoin contributes to distributing the wealth inside the community using the coin. Cooperation, ethics, solidarity and transparency are key factors for us to create a truly just system for everyone. The development and use of powerful interconnected digital tools (global) and regional networks (local) are crucial for our success.

THE CURRENCY FOR THE 99%

FairCoin Features

With the support of FairCoop, FairCoin implements fair value exchange on a global level. Our innovative Proof-of-Cooperation (PoC) blockchain mechanism is the unique consensus algorithm developed for FairCoin. It requires much less energy than other blockchains but also enables faster transactions. We are proud that FairCoin now is the the most ecological and resilient cryptocurrency.

Ecological
Very low power consumption for transaction control

Safe
Cooperatively validated nodes (CVNs, see FAQs)

Fast
Transaction visible in seconds,
confirmation max 3 minutes

Ethical
Supports fair business values

Strong
Strong and growing support from cooperatives worldwide

Controlled growth
Official price is 1 FAIR = 1,20 Euro (see our 'slow' price adoption strategy)

Vibrant
Strong community with daily online collaboration & regular local node meetings

Democratic
 Strategic decision making in monthly assemblies

Join the FairCoin movement

Use FairCoin as a means of exchange. Accept FairCoin in your shop, support your local business or use FairCoin online at FairMarket.  We are constantly looking for skilled people who want to help in developing this beautiful movement.  Join us in our "Ask Anything" Telegram or fairchat group or in the FairCoop Forum and get in contact with us directly. Anyone with a proactive mindset and a good spirit is very welcome.

 

Easily buy FairCoin here and
help fund FairCoop's projects
and merchants.

get FairCoin 

 

 

Visit FairCoop's marketplace to
buy goods with FairCoin online
with joint delivery service.

FairMarket

Map of FairCoin nodes
and merchants in
several countries worldwide.

use FairCoin

 

 

fair.coop ecosystem
Start your journey through the FairCoop ecosystem

 

 

GET YOUR FAIRCOIN WALLET

 

Energy-saving Blockchain for a fair currency

FairCoin is based on an innovative blockchain technology, which has been changed in July 2017 from a 'Proof-of-Stake' protocol to a 'Proof-of-Cooperation' (PoC) mechanism.  Our currency not only requires less energy and enables faster transactions, but also introduces a certain level of trust and democratic values even on the technical level. Here you find the White Paper and more information about PoC and our FairChains project.


White Paper & PoC

We are driven by consensus

In open assemblies, the community decides on how to reach our common goals. Everybody can participate and make proposals for developments, changes, or projects to work on. When a proposal passes via consensus the respective proposal maker and the relevant working groups will take action. We organise both the global online meetings and the physical local node meetings in this way.

 

FairCoop

Our Blog
21 April, 2018 - 00:00
Our first app for transport...
After a few months of work, we can now announce the launch of the Common Routes app for multiple means of transportation. An application to rationalize the use of our cars and so reduce their...
read more
30 March, 2018 - 14:06
Circular use of FairCoin
Introduction We want to familiarize you with the basic uses of FairCoin when offering goods or services as a merchant or service provider. Using FairCoin is not complicated, and in this overview we...
read more
24 March, 2018 - 15:00
Bittrex de-lists FairCoin....
  If you have visited the Bittrex crypto-currency exchange page you may have seen the message "The BTC-FAIR market will be removed on March 30, 2018" and we'd like to clarify the situation. This...
read more

Lend a hand
and be part of the change

Enric Duran

“Instead of relying on humans competing to keep the most value,
FairCoin is based on humans cooperating as equals,
to create value for all of them.”

- Enric Duran

FairCoin is a digital currency that is powered by a cooperative grassroots movement. It is a currency that is global, stable and democratic.

Made with on the decentralized earth.


 All current cryptocurrencies require you to buy the coins, either through mining or through exchanges. This gives the advantage to those who already have capital and mining equipment, and can afford risky investments. FairCoin is the first project where the coins are not bought but rather distributed equally between everyone who wants them regardless of their current financial status, and promotes equality.

FairCoin is a crypto currency like Bitcoin. It is a descendant of Peercoin, meaning the block generation is done by PoW/PoS hybrid.

FairCoin is a decentralised virtual currency, distributed through a vast airdrop process during the 6th and 8th of March, 2014 (view airdrop statistics). An approximate 49,750 addresses were logged for the giveaway, each able to claim 1000 FAIR per hour. Automated airdrop claiming methods had no effect, as each IP address could register once per hour, and 2 different captchas had to be solved. These security precautions were hidden till the day of distribution. FairCoin's vast distribution method allowed a good portion of the cryptocurrency community to claim a little bit of the 50,000,000 FairCoins, each. 

All current cryptocurrencies require you to buy the coins, either through mining or through exchanges. This gives the advantage to those who already have capital and mining equipment, and can afford risky investments. FairCoin is the first project where the coins are not bought but rather distributed equally between everyone who wants them regardless of their current financial status, and promotes equality.

FairCoin is a crypto currency like Bitcoin. It is a descendant of Peercoin, meaning the block generation is done by PoW/PoS hybrid.

FairCoin is a decentralised virtual currency, distributed through a vast airdrop process during the 6th and 8th of March, 2014 (view airdrop statistics). An approximate 49,750 addresses were logged for the giveaway, each able to claim 1000 FAIR per hour. Automated airdrop claiming methods had no effect, as each IP address could register once per hour, and 2 different captchas had to be solved. These security precautions were hidden till the day of distribution. FairCoin's vast distribution method allowed a good portion of the cryptocurrency community to claim a little bit of the 50,000,000 FairCoins, each.
 WHOLE PROJECT MARK
