PROJECT NAME: Sakai
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

Introducing Sakai 11 | Sakai
===============================

Introducing Sakai 11 | Sakai

 Skip to main content 

 About 
 Features 
 Try 
 Community 
 News 

   100% Open for Education 
 Sakai is a fully customizable, 100% open source learning management system. Your needs. Your system. Designed by higher ed for all. 
 More Info 

   Sakai 11 - More Powerful Than Ever 
 The latest version of Sakai provides innovative new functionality and a beautiful new responsive design. Click here to learn more about Sakai 11. Available Summer 2016. 
 Sakai 11 

   Next Generation Digital Learning Environment 
 Sakai s commitment to open standards and remaining 100% open source makes it the most robust and flexible LMS available. 
 More Info 

   Meeting the Needs of a Global Community 
 With a community spanning six continents and 20 countries, Sakai is a truly global solution available in over 20 different languages. 
 More Info 

   Educators are doing remarkable things with Sakai 
 Valencian Internacional University s award-winning ICT in Education uses Sakai to help educators reimagine what s possible with technology-enabled learning. 
 Learn More 

 Introducing Sakai 11 

 Introducing Sakai 11 
 A beautiful new interface, exciting new functionality, and a responsive design make for the best version of Sakai yet. 
 Available Now! 

 Sakai. Beautiful on every screen. 
 Thanks to a responsive design, Sakai looks and works great on every device, from your desktop to your phone. 

 Grade entry, simplified. 
 Grading is a fundamental part of the teaching experience. Too often, however, this process is complicated by clumsy grading software. In Sakai 11, the Gradebook provides simple, spreadsheet-style grade entry, so you ll be able to grade more easily and efficiently. 
 Learn More 

 Favorite sites, just a click away. 
 Users have always been able to add sites in Sakai to their  Favorites  bar to make them easily accessible. Now, this process is even easier. Simply star a site to mark a favorite site, at which point it will display at the top of your screen. 
 Learn More 

 Lessons, enhanced. 
 The Lessons tool, which allows you to construct modules of content for students, is even more powerful in Sakai 11. With an improved user interface and multi-column support, instructors have more flexibility than ever before. 
 Learn More 

 Sakai is used by institutions around the world. 

   University of Virginia and the "Academical" Village   
   The brainchild of a new, virtual  academical village  of leading institutions located throughout the world, Sakai 11 s fully responsive user interface allows students and faculty to do outstanding work no matter where they are or what devices they re using.

 Learn More About the Sakai Community 

 Try Sakai 
 Thanks to our  commercial affiliates , you can get a  cloud-hosted trial Sakai  site set up in minutes. 
 Ready to make the transition to Sakai? You can take advantage of our affiliates  hosted options or  download Sakai s source code  to host yourself. 

	  Apereo Foundation 2014 
	All Rights Reserved 

 About About Us 
 Overview 
 Why Sakai? 
 What does open source mean for Sakai? 
 Interoperability 
 Collaboration 
 Sakai History 
 Apereo Events 

 Features Accessibility 
 Features, Tools   Functionality 
 Learning Management Assignments 
 Calendar 
 Chat 
 Discussion Forum 
 Drop Box 
 Gradebook 
 Lessons 
 Profile 
 Resources 
 Site Roster 
 Syllabus 
 Test and Quizzes 
 Wiki 

 Project Collaboration 
 Research Collaboration 
 Languages 
 Industry Standards 
 Extending Sakai 
 Sakai 11 Tech Brochure 

 Try Explore 
 Download Sakai 
 Deploy 
 Current Release 
 Documentation Sakai Video Help Repository 

 Community Project Leadership 
 Commercial Affiliates 
 Supporters of Sakai 
 Adopters 
 User Stories 
 ATLAS Awards 
 Ways to Contribute Contribute as Maintenance or Q/A Team Member 

 Contact 

 News 

 Sakai is a community of academic institutions, commercial organizations and individuals who work together to develop a common Collaboration and Learning Environment (CLE). 

Sakai is a community of academic institutions, commercial organizations and individuals who work together to develop a common Collaboration and Learning Environment (CLE).
 WHOLE PROJECT MARK