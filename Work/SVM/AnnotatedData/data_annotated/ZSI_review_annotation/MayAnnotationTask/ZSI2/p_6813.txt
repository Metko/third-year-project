PROJECT NAME: One Laptop Per Child (OLPC)
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

One Laptop per Child
===============================

One Laptop per Child

 Skip to Navigation 

				One Laptop per Child			 

 Map 
 Countries 
 Support Strategy 
 Blog 
 About 
 Community 
 Take Action 
 Donate 
 google + 

 hide 

 next photo 

 motivation to explore 

 back to stories next story 

 Nagorno Karabakh and Armenia 

 Nagorno Karabakh and Armenia Building a new future through education Nagorno Karabakh and Armenia Committed to the best education Nagorno Karabakh and Armenia Unleashing our children's potential Nagorno Karabakh and Armenia Learning healthy habits with the XO Nagorno Karabakh and Armenia Learning healthy habits with the XO 

 Charlotte 

 Charlotte Thanks to Knight Foundation in early 2013, over 2,200 laptops were distributed 
to first through fourth grade students in the Project L.I.F.T. Zone of Charlotte 
Mecklenburg Schools. 

 Miami 

 Miami Over 500 students at Holmes Elementary School received their very own connected XO laptop during the 2011-2012 academic school year. A project supported by Knight Foundation, this was OLPC s first U.S. project. 

 Rwanda: transforming society through education 

 Rwanda: transforming society through education Rwanda's deployment began in 2007, supported by  G1G1 donations  and by President Paul Kagame's  vision for the country  that included universal access to the Web.  In 2008 the XO was introduced to the first 10,000 students. Rwanda: transforming society through education Since then, 100,000 children and teachers have joined the  program , supported by the telecom giants who are rolling out connectivity across the country. The  project team  is supported by the country's largest universities and educational institutes.    (Read the  2011 Report ) 

 Nicaragua: a community learning together 

 Nicaragua: a community learning together Bluefields, Nicaragua  was devastated by Hurricane Felix in 2007. The  Miskito community  is embracing the XO laptops as a meaningful tool for its children's future. Thanks to the  Zamora Teran Foundation , every child and teacher in the community is learning together with their XOs.  Nicaragua: a community learning together The first activity the students learned was the  Record activity , to take videos and photos.  A report on their work was published in the regional newspaper. Nicaragua: a community learning together Students of all ages have been  using their laptops  at home, and after school.  The younger students help one another with their work, and share work with their parents.  Nicaragua: a community learning together Bluefields is a port city with a broad bay, and the community spends much of its time outside.  A  sunlight-readable screen  is important!   And in 2012, every child on the  legendary volcanic island of Ometepe  received a laptop. 

 Madagascar: Starting with the youngest 

 Madagascar: Starting with the youngest In Madagascar, OLPC schools have  started with the youngest  children, who learn to read and type with their XOs.  They are also learning how to use and maintain their machines with their own laptop hospital. Madagascar: Starting with the youngest This class received some of the earliest laptops, thanks to a 2007 donation.  Since then, many international educators have come  to work with and learn from  Madagascar   classrooms .  Madagascar: Starting with the youngest Classrooms were actively engaged in the idea of adopting OLPC.  This class arranged themselves so they were facing one another while working.   Madagascar: Starting with the youngest Many students used the  Speak activity  to learn to type and to hear words spoken back to them.  It reads back entered text in English and French, and can respond in a limited way. 

 Paraguay: Focused inspiration 

 Paraguay: Focused inspiration OLPC Paraguay has been led by a national NGO.  Paraguayan software developers and educators have collaboratively developed localized software and class materials that they use today. Paraguay: Focused inspiration Classes incorporate XOs into many of their daily projects. Both teacher and students are engaged in deciding what  tools and activities  to use. Paraguay: Focused inspiration XOs are used widely at home.  This group of students is prepared for the end of the day.  

 India: Music in Khairat  

 India: Music in Khairat  Elementary students in  Khairat, India , have kept their attendance near 100% since their school received XOs.  Their teachers run weekly workshops inviting other teachers to come work with their students and share new ideas. India: Music in Khairat  Their favorite activities include drawing, programming in Etoys, and singing along to TamTam.  One class created some new TamTam libraries to add local instruments, and they are working on their own compositions. 

 Gaza and Ramallah: Learning as a community   

 Gaza and Ramallah: Learning as a community   UNRWA and OLPC  have been working together in Gaza and the West Bank to implement community laptop programs this year.  In many schools in such as this one in Ramallah, students use their XOs in class and out.  These girls are on their way home.   Gaza and Ramallah: Learning as a community   Lessons don't last all day, and children from the nearby towns often spend time outside reading and playing (and sometimes laughing) once school lets out.  Gaza and Ramallah: Learning as a community   This year Ramallah held an  XO Summer Camp  to help hundreds of students learn how to use, repair, and teach with their laptops.  Here two members of the Boys School take a rest in the shade.  Gaza and Ramallah: Learning as a community   In Gaza, the Rafah Refugee Camp saw an intensive and joyful deployment of XOs in the Spring.  It took 10 months to get approval to deliver machines, but only 12 weeks for students and teachers to define their own projects and get families involved, and to hold a delightful  public demonstration . Gaza and Ramallah: Learning as a community   Over 2,000 children and teachers  took part in the deployment, and hundreds of people took part in the summer festival and demonstration.  Teachers at others schools came to learn how they could get involved. 

 Nepal: Children Down Every Path 

 Nepal: Children Down Every Path These young boys on the outskirts of Kathmandu, Nepal work together at home.   They spend as much time with their XOs there as they do at school, and  parents embrace  the change. Nepal: Children Down Every Path OLE Nepal , the NGO organizing the project there, works with many young students who are just starting to read. Nepal: Children Down Every Path Children are welcomed into the program one school at a time; often students teach those at nearby schools that have not yet gotten involved. 

 Afghanistan: Inspiring young women 

 Afghanistan: Inspiring young women Half of the OLPC schools in  Afghanistan  are for girls.  They and their teachers have been particularly eager to explore new ways to work with laptops in the classroom.  Afghanistan: Inspiring young women 4,500 children have XOs through OLPC Afghanistan, including schools in Kandahar and Herat.  The laptops have a dual Dari-Latin keyboard.  All software is localized into Dari, and some is in Pashto as well.  Afghanistan: Inspiring young women The Afghan government has digitized school texts and produced new materials for the XOs.  Even in urban schools, students often have limited access to books.  They love the chance to share great images and texts they discover with one another. 

 Kenya: Joyful collaboration 

 Kenya: Joyful collaboration This class of students in  Takaungu, Kenya  has been working with their XOs for a year.  They have an XO study hall, with seats rearranged in groups, to explore what interests them.   You can see students working on recording video, programming in Turtle Art, and playing favorite Sugar activities.   Kenya: Joyful collaboration These boys are learning how to record video, watching and listening to the results of their latest effort.  Kenya: Joyful collaboration Others read and practice by the door to the yard.  The sunlight is strong in Takaungu year-round, and they are considering using solar power to charge their batteries.    Kenya: Joyful collaboration Mapenzi was often the most outgoing student in class, and helped make classes with the XO a joy for everyone.  Here she is  recording herself  singing her song into the laptop, which her friends will try to play back later.  

 Peru: Learning how to learn  

 Peru: Learning how to learn  Peru's education system is built in part on project-based learning.  When they decided to introduce OLPC across the country and  digitize their classrooms , the Education Ministry developed dozens of longer  projects and activities  that could be done with nothing but an XO. Peru: Learning how to learn  In the most rural areas, schools often meet only a few days a week.  Most learning takes place among the children, or with their parents, many of whom are not literate. Peru: Learning how to learn  Half of the  over 500,000 students in OLPC Peru  live in rural areas. They are exploring new ways to make laptops an engaging part of life and education, including in the traditional classroom.  These young students are learning how to read and  type  in Spanish, which for many their second language. 

 Uruguay: Students in Artigas 

 Uruguay: Students in Artigas Three young students work together on the curb outside school in  Florida State , in Uruguay. Uruguay: Students in Artigas Every primary school student in Uruguay has an XO, and 98% of all students have Internet connectivity at home, provided through  Plan Ceibal . 

 home 
 stories 
 world 
 blog 
 press 
 community 
 about 
 donate 

 Contact:  information@laptop.org  | OLPC, Inc. 200 South Biscayne Blvd, Suite 3550 Miami, Florida 33131, USA, Fax. (305) 374-1374 | Copyright: you may reuse any materials with  proper attribution . 
 Privacy Policy  and  Disclaimer  | Designed by  Pentagram  and  Upstatement  | Hosted by  Gossamer Threads 

 OLPC's mission is to empower the world's poorest children through education. "We aim to provide each child with a rugged, low-cost, low-power, connected laptop. To this end, we have designed hardware, content and software for collaborative, joyful, and self-empowered learning. With access to this type of tool, children are engaged in their own education, and learn, share, and create together. They become connected to each other, to the world and to a brighter future." 

OLPC's mission is to empower the world's poorest children through education. "We aim to provide each child with a rugged, low-cost, low-power, connected laptop. To this end, we have designed hardware, content and software for collaborative, joyful, and self-empowered learning. With access to this type of tool, children are engaged in their own education, and learn, share, and create together. They become connected to each other, to the world and to a brighter future."
 WHOLE PROJECT MARK