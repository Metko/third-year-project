PROJECT NAME: antiX
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

Main Page - antiX
===============================

Main Page - antiX

 Main Page 

 From antiX 

 Jump to:  navigation ,  search 

 Contents 

 1   Translations 
 2   Welcome to antiX. 
 3   About antiX 
 4   News 
 5   Downloads 

 5.1   Torrents 
 5.2   Mirrors/Download 

 6   Video 
 7   Quick Tips 

   Translations 
 Brazilian-Portuguese   Catalan   Dutch   French   German   Greek   Italian   Romanian   Spanish   Russian   Turkish 

   Welcome to antiX. 
   About antiX 
 antiX is a fast, lightweight and easy to install systemd-free linux live CD distribution based on Debian Stable for Intel-AMD x86 compatible systems. 
antiX offers users the "antiX Magic" in an environment suitable for old and new computers. So don't throw away that old computer yet!
The goal of antiX is to provide a light, but fully functional and flexible free
operating system for both newcomers and experienced users of Linux. It should run on most computers,
ranging from 256MB old PIII systems with pre-configured swap to the latest powerful boxes. 256MB RAM is recommended minimum for antiX. The installer needs minimum 2.7GB hard disk size.
antiX can also be used as a fast-booting rescue cd.
Special XFCE editions made in collaboration with the MEPIS Community called MX-15 "Fusion" (released 24 December 2015) and MX-14 "Symbiosis" (released March 24, 2014) are also available. antiX MX series has a separate development schedule to antiX.
 At the moment antiX-16.2 "Berta C ceres" comes as a full distro (c695MB), a base distro (c510MB) and a core-libre distro (c190MB) for 32 bit and 64 bit computers. For those who wish to have total control over the install, use antiX-core and build up.
 Present released  antiX-16.2  versions, 16 June 2017: isos and md5sum files available  'Berta C ceres' 
 To get a taste of  antiX-16 , watch the video by dolphin_oracle:  https://www.youtube.com/watch?v=G8Aw2zzBE-g 
 Useful documentation: antiX-FAQ   Live boot menu options   Detailed guide to antiX-live   MX/antiX wiki   MX Users Manual 

 Donate:  If you would like to help out with the development of antiX by donating money, go here.  PayPal- Euros   PayPal- US Dollars 
 Added on 21 January 2015. For the moment, send donations here:  https://mxlinux.org/donate 
 MX-16.1 "Metamorphosis"  editions released on 8 June 2017 can be downloaded from here:
 https://sourceforge.net/projects/mx-linux/files/Final/MX-16.1/ 
 https://mxlinux.org/download-links 

   News 
 This wiki has had new account creation disabled due to spam. If you wish to have an account on this wiki so you can add or update content you will be required to visit the antiX forum so an account can be made for you.
 Old News 
 NEW WEBSITE and FORUM 
 Website here:  https://antixlinux.com/ 
 New forum here:  https://www.antixforum.com/ 
 Please use the new forums and website from now on.
 anticapitalista, 20 October 2017, Thessaloniki

 antiX-17 released 
 A very quick announcement for now, just to let you all know that antiX-17  Heather Heyer  is now available.
 Get the various iso files from here for now.
 https://sourceforge.net/projects/antix-linux/files/Final/antiX-17/ 
 A full announcement will follow.
 Enjoy!
 Please use the new website and forum.
 anticapitalista, 25 October 2017, Thessaloniki

   Downloads 
   Torrents 
 Please seed torrents once you have downloaded antiX or MX. Thanks.
 antiX 
 32 bit:

 antiX-17_386-full.iso  LinuxTracker 

 antiX-17_386-base.iso  LinuxTracker 

 antiX-17_386-core.iso  LinuxTracker 

 antiX-17_386-net.iso  LinuxTracker 

64 bit:

 antiX-17_x64-full.iso  LinuxTracker 

 antiX-17_x64-base.iso  LinuxTracker 

 antiX-17_x64-core.iso  LinuxTracker 

 antiX-17_x64-net.iso  LinuxTracker 

 MX-16 

  MX-16_386  LinuxTracker 

  MX-16_x64  LinuxTracker 

   Mirrors/Download 
 antiX-17: 

   (St. Louis MO, USA) 

   (Los Angeles CA, USA) 

   (Cuenca, Ecuador) 

   (New Zealand) 

   (Taiwan) 

 In Europe:

   (Heraklion, Crete, Greece) 

   (Italy) 

   (Sweden) 

 MX-16   All download-links 

 Packages 
 You may want to make some changes to the sources.list files to get a location closer to where you are.
 antiX-17 - edit /etc/apt/sources.list.d/antix.list
 Ecuador, South America 

  deb  http://mirror.cedia.org.ec/mx-workspace/antix/stretch  stretch main nosystemd  

 California, USA 

  deb  http://iso.mxrepo.com/antix/stretch/  stretch main nosystemd 

 The Netherlands, Europe 

  deb  http://nl.mxrepo.com/antix/stretch/  stretch main nosystemd 

 Crete, Greece, Europe 

  deb  http://ftp.cc.uoc.gr/mirrors/linux/mx/antix/stretch  stretch main nosystemd 

 Taiwan, Asia 

  deb  http://ftp.yzu.edu.tw/Linux/mx-workspace/antix/stretch/  stretch main stretch nosystemd

   Video 
 antiX video guides by runwiththedolphin

 antiX-17 
 antiX-17 Console Environment 
 Remaster your antiX live-USB 
 Make a live-USB with persistence! 
 antiX-17 - What's New? 

 antiX-16 
 Introducing antiX-16 
 What's new in antiX-16 
 Installing antiX-16 

 antiX-15 
 What's New! 
 1-to-1 assistance 
 1-to-1 Voice 
 Find and Mount Network Shares (includes updated connectshares info) 
 Personal Menu   Menu Manager 

 antiX-13 
 Installing antiX-13 
 Set up wireless with wicd 
 Customise the desktop 
 Browsing and Mounting Samba/Windows shares 
 antiX and Netflix 
 antiX and ROX 
 antiX and SpaceFM 
 Installing AntiX 13.2 
 AntiX 13 upgrade to Antix 13.2 
 Antix 13 - How to Install Updates 

Others.
 Video review of antiX-13 
 Como Instalar AntiX 13 Full 
 antiX-13 
 sneekylinux video 
 Video review of antiX-13-beta1 

Not a video, but a review article of Best-Linux-For-Old-Computer
 http://jponline.hubpages.com/hub/Best-Linux-For-Old-Computer 

   Quick Tips 
 Login as 'demo', password = 'demo'.
 For root access, password = 'root'. Please do not login as root. It is totally unnecessary.
 Sudo is configured by default. When prompted for a password, user your username password, not root.
 To boot from floppy, I suggest  SmartBootManager. 
 antiX can also boot from an .iso file on a hard-drive.   Boot from iso on hard-disk  This is very fast.
 antiX can also be installed as a livecd to usb. Using a live-usb-cd is very fast.
 This site is under construction. For questions, comments, please use the  antiX forum  or the  antiX forum on MepisCommunity 
 There is also a lot of information suitable for antiX in the  MEPIS wiki .
  Link to full list of applications used in antiX.  Installed packages List 
  Useful documentation: antiX-FAQ   Live boot menu options   Detailed guide to antiX-live 

  Link to full list of applications used in antiX.   Installed apps 

Retrieved from " http://antix.mepis.org/index.php?title=Main_Page " 

 Views 

 Page 
 Discussion 
 View source 
 History 

 Personal tools 

 Log in 

 Navigation 

 Main Page 
 Comments 
 User articles 
 Community portal 
 Current events 
 Recent changes 
 Random page 
 Howto articles 
 sitesupport 
 Links 
 Forum 

 Search 

 Toolbox 

 What links here 
 Related changes 
 Special pages 
 Printable version   Permanent link   

  This page was last modified on 22 November 2017, at 17:26. 
 This page has been accessed 5,029,174 times. 
 Content is available under  Attribution 2.5  . 
 Privacy policy 
 About antiX 
 Disclaimers 

 antiX is a fast, lightweight and easy to install linux live CD distribution based on Debian Testing for Intel-AMD x86 compatible systems. antiX offers users the "antiX Magic" in an environment suitable for old computers. 

antiX is a fast, lightweight and easy to install linux live CD distribution based on Debian Testing for Intel-AMD x86 compatible systems. antiX offers users the "antiX Magic" in an environment suitable for old computers.
 WHOLE PROJECT MARK