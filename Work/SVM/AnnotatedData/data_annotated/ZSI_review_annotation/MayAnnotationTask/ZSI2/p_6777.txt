PROJECT NAME: Freenode
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

freenode
===============================

freenode

 Jump to navigation 

 Query: 

 Welcome to  supporting free and open source communities since 1998 

 Chat 

 Register a group 

 Knowledge Base 

 freenode #live 2018 - Call for Proposals now open! 
 christel on 2018-02-26 

 Jorge Oliviera from  JOG , 6x South Brazil National Champion. 
 You do not need to have a black belt in FOSS to come talk at this year's freenode #live conference 
 freenode #live returns to We The Curious in Bristol, UK on Saturday 3 and Sunday 4 November 2018. The CFP is now live, and you can submit a talk over at the  freenode.live  website. 
 The inaugural freenode #live conference last year saw a star-studded speaker line-up including  Deb Nicholson ,  Matthew Garrett ,  Karen Sandler ,  John Sullivan ,  Jelle van der Waa ,  Chris Lamb ,  Neil McGovern ,  Matthew Miller  and many, many more.  
 Matt Parker  from  Standup Maths  and  Festival of the Spoken Nerd  provided excellent entertainment on the Saturday evening, and the feedback from attendees, speakers and volunteers alike was overwhelmingly positive.  
 freenode #live 2017 was possible thanks to the generous support of sponsors such as  Bytemark ,  Falanx ,  openSUSE ,  Private Internet Access ,  Ubuntu  and  Yubico .  Private Internet Access  has already agreed to sponsor the event for another year, and we are currently looking for additional sponsors. Please do not hesitate to get in touch if your company might be interested in supporting freenode #live 2018. 
 We are looking forward to hosting this year's freenode #live conference, and hope that you will join us there. 

 view archive 

 About freenode 

 The project 

 The people 

 The philosophy 

 The policies 

 freenode #live 

 Acknowledgements 

 News Archive 

                    /

                     RSS 

                    /

                     Atom 

 Using freenode 

 Webchat 

 chat.freenode.net:6697 

                    /

                     non-SSL: 6667 

 Channel guidelines 

 Catalyst guidelines 

 Knowledge base 

 Supporting the project 

 Group registration 

 Existing groups 

 Social Media 

   Google+ 

   Twitter 

   Facebook 

 Private Internet Access 

          2016-2018, freenode 

 freenode is a special-purpose, not a general-purpose, discussion network, currently implemented on Internet Relay Chat (IRC). It exists to support specific communities. It provides an interactive environment for coordination and support of peer-directed projects, including those relating to free software and open source. Our aim is to help our participants to improve their communicative and collaborative skills and to maintain a friendly, efficient environment for project coordination and technical support. 

 WHOLE PROJECT MARK
