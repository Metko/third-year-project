PROJECT NAME: Zipcar
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 Zipcar is a US membership-based carsharing company providing automobile reservations to its members, billable by the hour or day. Zipcar was founded in 2000 by Cambridge, Massachusetts residents Antje Danielson and Robin Chase.

On 14 March 2013 Avis Budget Group purchased Zipcar for about US$500 million in cash. Zipcar will operate as a subsidiary of Avis Budget Group. Scott Griffith, who had run the company for the previous 10 years, resigned the day after the acquisition closed, and passed the reins to a new company President, Mark Norman.

As of July 2013, the company had more than 810,000 members and offers nearly 10,000 vehicles throughout the United States, Canada, the United Kingdom, Spain and Austria, making Zipcar the world's leading car sharing network.

Members can reserve Zipcars online or by phone at any time, immediately or up to a year in advance. Zipcar members have automated access to Zipcars using an access card which works with the car's technology to unlock the door, where the keys are already located inside. Zipcar also offers an iPhone or Android application that allows members to honk the horn to locate a Zipcar and unlock the doors. Zipcar charges a one-time application fee, an annual fee, and a reservation charge. Gas, parking, insurance, and maintenance are included in the price.

Recent years have seen the creation of many Zipcar competitors. Traditional car rental companies have replicated Zipcar's short-term car rentals with programs including Hertz on Demand, Enterprise's WeCar, UHaul's Uhaul Car Share, and Daimler's Car2Go. Regional competitors exist, such as City CarShare in the San Francisco Bay Area, Mint in New York and Boston, I-GO in Chicago, among others. Peer-to-peer car sharing startups include RelayRides and Getaround.

In the autumn of 2007, Zipcar merged with Seattle-based rival Flexcar to create a nation-wide car sharing company. The company's IPO was in April 2011. Zipcar common stock traded on NASDAQ under the ticker symbol "ZIP" until 14 March 2013, when Avis Budget Group acquired Zipcar for US$500 million in cash. 

Zipcar is a US membership-based carsharing company providing automobile reservations to its members, billable by the hour or day. Zipcar was founded in 2000 by Cambridge, Massachusetts residents Antje Danielson and Robin Chase.

On 14 March 2013 Avis Budget Group purchased Zipcar for about US$500 million in cash. Zipcar will operate as a subsidiary of Avis Budget Group. Scott Griffith, who had run the company for the previous 10 years, resigned the day after the acquisition closed, and passed the reins to a new company President, Mark Norman.

As of July 2013, the company had more than 810,000 members and offers nearly 10,000 vehicles throughout the United States, Canada, the United Kingdom, Spain and Austria, making Zipcar the world's leading car sharing network.

Members can reserve Zipcars online or by phone at any time, immediately or up to a year in advance. Zipcar members have automated access to Zipcars using an access card which works with the car's technology to unlock the door, where the keys are already located inside. Zipcar also offers an iPhone or Android application that allows members to honk the horn to locate a Zipcar and unlock the doors. Zipcar charges a one-time application fee, an annual fee, and a reservation charge. Gas, parking, insurance, and maintenance are included in the price.

Recent years have seen the creation of many Zipcar competitors. Traditional car rental companies have replicated Zipcar's short-term car rentals with programs including Hertz on Demand, Enterprise's WeCar, UHaul's Uhaul Car Share, and Daimler's Car2Go. Regional competitors exist, such as City CarShare in the San Francisco Bay Area, Mint in New York and Boston, I-GO in Chicago, among others. Peer-to-peer car sharing startups include RelayRides and Getaround.

In the autumn of 2007, Zipcar merged with Seattle-based rival Flexcar to create a nation-wide car sharing company. The company's IPO was in April 2011. Zipcar common stock traded on NASDAQ under the ticker symbol "ZIP" until 14 March 2013, when Avis Budget Group acquired Zipcar for US$500 million in cash.
 WHOLE PROJECT MARK