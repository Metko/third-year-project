PROJECT NAME: ownCloud
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 


Trusted by thousands of organizations worldwide
With over 15 million users worldwide ownCloud is the market leader in enterprise file sharing *
* active installations and users
See why ownCloud is trusted  »
Recent Tweets
Follow ownCloud on Twitter
ownCloud is Community Driven
With over 1300 contributors ownCloud is one of the biggest open source projects worldwide*
* see openhub statistics
Join the community today
Signup for the ownCloud newsletter

Stay up to date about new ownCloud features and releases.
(Our newsletter is sent once a month. We won't spam :) )
Recent Posts
ownCloud storage award platinum 2017
When efforts pay off: ownCloud is nominated again for the IT Storage Insider Award

24.05.2018 by ownCloud

We are excited to announce that for the third year straight, ownCloud has been nominated for Storage Insider Readers’ Choice Award for Enterprise File Sharing! Now we need your help to win this.
read more ...
award community it storage insider security
migrate ownCloud appliance to new Univention aplliance
It’s only 2 steps to bring your ownCloud 9.1 appliance up to date!

17.05.2018 by ownCloud

You still run an ownCloud 9.1 appliance? There are plenty of new features available in ownCloud X. Time to migrate to our new appliance!
read more ...
Appliance howto migration ownCloud X server Univention
ownCloud Password Security
ownCloud forum hack: Why password security is more important than ever

16.05.2018 by ownCloud | 10 Comments

A hack on the social media platform LinkedIn happened in 2012 made the unauthorized access to user data of the ownCloud forum possible. Weak and old passwords are still a major threat to online services.
read more ...
community forum security
ownCloud App in Review Heaven
ownCloud App in Review Heaven

15.05.2018 by David Gonzalez

How did we get from 2-3 to 4-5 stars ratings? David Gonzalez, lead app developer at ownCloud, talks about how we managed to increase the ownCloud app development quality.
read more ...
android app community
ownCloud Server 10.0.8 Release

27.04.2018 by ownCloud | 12 Comments

It is our pleasure to announce the release of ownCloud Server 10.0.8, today! 100+ BugFixes, minor tweaks, and a bunch of new Features prove our path to building high-quality Software. ownCloud is the performant, robust and future-proof Open Source file sync and share solution that you trust your files with.
read more ...
10.0.8 release LDAP ownCloud X server
ownCloud Android app release 2.7.0
Android App 2.7.0 Release – Features, Features, Features!

16.04.2018 by ownCloud | 2 Comments

Beautiful apps don’t write themselves – people do it. They brought you many new features. Learn about all of the improvements!
read more ...
android app community Contribution OpenSource release security
ownCloud-Delta-Sync
Welcome Delta Sync for ownCloud

09.04.2018 by ownCloud

ownCloud announces the implementation of Delta sync for the ownCloud Server and Desktop Client. This speeds up the synchronization of uncompressed files tremendously.
read more ...
client Delta-Sync open source performance server
ownCloud appliance 4 steps improve
The first 4 steps after you installed the ownCloud appliance

28.03.2018 by ownCloud | 1 Comment

Installing the ownCloud appliance leaves you with a recommended setup. But you can get much more out of your appliance. Part 2 of our appliance series focuses on small steps which deliver big improvements.
read more ...
Appliance Collabora How-To marketplace ownCloud X server Tutorial
ownCloud-Phoenix
ownCloud Phoenix – rebirth of the ownCloud user interface

21.03.2018 by Thomas Müller

We proudly announce the birth of ownCloud Phoenix! Following our path of modularisation, we introduce a true web client which holds only HTML, JavaScript and CSS files. The WebUI is unbundled from the ownCloud Backend-Server. The Goal is to have a clean and easy to understand code structure.
read more ...
Contribution core Design Development Front-End open source owncloud ownCloud X Phoenix server
ownCloud success story
Pierpaolo Fantuzzi about ownCloud – a user’s success story

15.03.2018 by ownCloud

Usually it’s none of our business what’s going on in the heads of our users – Pierpaolo explicitly told us though. And we liked what he said!
read more ...
cloud community open source success
See more blog posts
About ownCloud

    Events
    Trademark
    Privacy policy
    Code of conduct
    Imprint

Resources

    Help
    FAQ
    Changelog
    Security
    Admin manual
    User manual

Interact

    IRC channel
    Mailing list
    Forum
    Bug tracker
    Spread the word

© 2018 ownCloud

ownCloud Events

The ownCloud community organizes and participates in many events around the globe. Interested in meeting fellow ownCloud users and contributors? Look for an event that happens close to you or organize one!
Conferences

ownCloud community members everywhere organize ownCloud presence at trade shows and conferences to present our community and technology to visitors.
Below is a list of upcoming tradeshows and conferences where you can find ownCloud represented!

If you have business questions and would like to meet ownCloud GmbH at an event, check out the events page on owncloud.com.
Upcoming Events
ownCloud-TNC18
TNC18 Trondheim

June 12, 2018 - June 14, 2018
ownCloud pixx lounge
2. PIXX LOUNGE 2018

June 13, 2018 - May 13, 2018
ownCloud conference 2018
ownCloud Conference 2018

September 18, 2018 - April 21, 2018

 ownCloud, Inc. provides an open source platform to sync, share, and view data and files. The company’s platform enables to sync and view contacts, calendars, and bookmarks across various devices and allows editing right on the Web. Its platform also enables businesses to host their own, on premises, or remote cloud storage. The company was incorporated in 2011 and is based in Lexington, Massachusetts with an international office in Germany. 

ownCloud, Inc. provides an open source platform to sync, share, and view data and files. The company’s platform enables to sync and view contacts, calendars, and bookmarks across various devices and allows editing right on the Web. Its platform also enables businesses to host their own, on premises, or remote cloud storage. The company was incorporated in 2011 and is based in Lexington, Massachusetts with an international office in Germany.
 WHOLE PROJECT MARK
