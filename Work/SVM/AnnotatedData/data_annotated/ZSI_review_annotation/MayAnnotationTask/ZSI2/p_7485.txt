PROJECT NAME: The Behavioural Insights Team
PROJECT SOURCE: Global Innovation Fund projects
---------------------------------------
NEW PAGE: 

| The Behavioural Insights Team
===============================

 | The Behavioural Insights Team

 About Us  

 Who we are 
 Press 

 Blog 
 Our Work  

 Who We Work With 
 Policy Publications 
 Academic Publications 

 The Team 
 Locations  

 London 
 Manchester 
 New York 
 Singapore 
 Sydney 
 Wellington 

 Jobs 
 BI Ventures 
 Contact Us 

 In partnership with  

 Using data science in policy 
 The untapped potential to improve social care, school inspections and road safety 

 Our work this year 
 Download the Team s update report for 2016-17 

 Improving exam pass rates and university acceptances 
 Our latest education results 

 In the news 
 Recent coverage for the team, including the Times, Guardian and Today programme 

 Meet the team 
 Our team has a broad range of backgrounds. Read their profiles here. 

 Read the blog 
 Latest findings and comment from the world of behavioural science and public policy. 

 What we do 
 We use insights from behavioural science to encourage people to make better choices for themselves and society. 

 Copyright   Behavioural Insights Team 2014 - 2018 
 Terms 
 Privacy policy 
 @B_I_Tweets

The Behavioural Insights Team (BIT) is a social purpose company. We are jointly owned by the UK Government; Nesta (the innovation charity); and our employees.

BIT started life inside 10 Downing Street as the world’s first government institution dedicated to the application of behavioural sciences. Our objectives remain the same as they have always been:

    making public services more cost-effective and easier for citizens to use;
    improving outcomes by introducing a more realistic model of human behaviour to policy; and wherever possible,
    enabling people to make ‘better choices for themselves’.

We do this by redesigning public services and drawing on ideas from the behavioural science literature. We are also highly empirical; we test and trial these ideas before they are scaled up. This enables us to understand what works and (importantly) what does not work.

We have around 150 employees, who have either a strong academic grounding in economics, psychology and research methods or a background in government policymaking. Our headquarters are in London. We also have offices in Manchester, New York, Singapore and Sydney.

We have recently established BI Ventures, a team within BIT, which works on building scalable digital products that address social issues. We currently have three revenue-generating products in our portfolio, with more in development.

Our Academic Advisory Panel includes Richard Thaler, co-author of Nudge; former Cabinet Secretary Lord Gus O’Donnell (the panel’s chair); and senior academics from leading UK Universities.

BIT’s company Board is chaired by Peter Holmes. The Cabinet Office representative is Janet Baker, and the Nesta representative is Nathan Elstub. David Halpern, our Chief Executive, Owain Service, our Director of BI Ventures, and Simon Ruda, our Director of Home Affairs and International Programmes, also sit on the Board.

If you are interested in finding out more about the team, feel free to get in touch.

Or if you’d like to find out more about our policy work, you can find out more about the team’s work on the publications part of our website or you can sign up for our newsletter.


Since the Behavioural Insights Team was created in 2010, there has been considerable media interest in the team's work. Often referring to the team as 'the Nudge Unit' (after the work of Professor Richard Thaler, co-author of Nudge and academic advisor to the team), much of the media interest has focused on the influence the team has had within Whitehall and overseas; and the methods and insights that the team has applied to public policy.

Below are a selection of some of the articles, radio shows and interviews that have been published over the past few years.
 WHOLE PROJECT MARK
