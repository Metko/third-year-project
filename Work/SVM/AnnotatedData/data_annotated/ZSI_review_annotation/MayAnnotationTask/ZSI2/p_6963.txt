PROJECT NAME: Future Everything
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

Home » FutureEverything
===============================

Home   FutureEverything

 Jump to nav 
 Jump to content 

   Projects   

 Festival 

   Events   
   Ideas   
   History   

 About 

   News   
   Services   

   Facebook   
   Twitter   
   Vimeo   
   Instagram   
   Soundcloud   
   Flickr   
   Google+   

 Search the site 

 Home   

 Featured News 

 every thing every time at Great Exhibition of the North 2018 

 Future Sessions: Trust in invisible agents 

 Future Sessions Launch Party: Lucas Gutierrez, Joe Beedles   Seth Scott + Guillaume Dujat 

 Featured Commissions 

 SUPERGESTURES - Ling Tan 

 Positively Charged - Kasia Molga 

 every thing every time - Naho Matsuda 

 Featured Projects 

 FAULT LINES 

 GROW Observatory 

 CityVerve   The Internet of Things and smart city demonstrator 

 News 

   every thing every time at Great Exhibiti 

   Building memories of Manchester amongst  

   An interview with Future Sessions curato 

   Opportunity: Board Member (x5) 

 Projects 

   FAULT LINES 

   GROW Observatory 

   CityVerve   The Internet of Things and 

 Events 

   Future Sessions: Trust in invisible agen 

   Future Sessions Launch Party: Lucas Guti 

   CityVerve Community Forum: Memories of M 

   SUPERGESTURES performance 

 Ideas 

   Community Level Indicators in the Smart  

   New Playgrounds: An Introduction to Hack 

   Smart Citizens Publication 

   Digital Public Spaces 

				  1995 - 2018 FutureEverything

					Design by  Supanaught  | Build by  Ravenous 

 Back to top 

 #futr 
 Mailing List 

Europeana - The Future of Cultural Heritage » FutureEverything
===============================

Europeana - The Future of Cultural Heritage   FutureEverything

 Jump to nav 
 Jump to content 

   Projects   

 Festival 

   Events   
   Ideas   
   History   

 About 

   News   
   Services   

   Facebook   
   Twitter   
   Vimeo   
   Instagram   
   Soundcloud   
   Flickr   
   Google+   

 Search the site 

 Home 
				/

 Manchester 2015 
				/

								Conference							 

			Art/Live/Film		 

			Conference		 

			Festival Lab		 

			Sessions   Workshops		 

			Speakers		 

	Europeana   The Future of Cultural Heritage

 This session is an open discussion of the challenges, opportunities and future scope of digital cultural heritage reuse. What is copyright in the digital age? Where can cultural organisations use digital technologies to better engage audiences? This session will feature expert representatives from the design world, cultural heritage institutions, including Culture24 and Europeana, which looks to make cultural heritage openly accessible in a digital way. 
 The session will end with pitches from six creatives and entrepreneurs actively using digitised cultural heritage content; designer to retailer service Pleathub, 3D printed traditional craft Limerick Lace, physical to digital archive project Digital Souvenirs, mobile game app aimed at exploring museums Digital Story Cubes, online book archive Public Domain City, and digital wallpaper prototype Gallery Dynamic. 
 A panel of judges from the creative industries will be selecting the best applications to take forward to the last stage of the Europeana Creative Design Challenge, a project about encouraging the re-use of digitised cultural heritage content. We also invite you, the audience, to help select the tools   products you would like to see brought to market. By using the audience TouchBack system you can vote for your favorite application, which will be awarded the Audience Prize at FutureEverything 2015 alongside the judges selected application. Follow the link below to add your feedback. 
 Touchback:  tbk.io/fe-ea 

 Times 

									Fri 27 Feb, 10:30																		-
									11:30								 

 Venue 

																Town Hall - Room 5 																								Town Hall, Albert Square, Manchester, Lancashire,
																								 M60 2LA 

 Related items: 
 Events 

  Fireside Chat   Memo Akten 
  PuBliC 
  Sonic Pi   Live Coding in the Classroom 
  Fireside Chat   Alice Bell 

 People 

				  1995 - 2018 FutureEverything

					Design by  Supanaught  | Build by  Ravenous 

 Back to top 

 #futr 
 Mailing List 

Digital Public Spaces » FutureEverything
===============================

Digital Public Spaces   FutureEverything

 Jump to nav 
 Jump to content 

   Projects   

 Festival 

   Events   
   Ideas   
   History   

 About 

   News   
   Services   

   Facebook   
   Twitter   
   Vimeo   
   Instagram   
   Soundcloud   
   Flickr   
   Google+   

 Search the site 

 Home  /  Ideas  /  Digital Public Spaces   

 Themes 
 Publications 
 Latest 

	Digital Public Spaces

 Download Publication 
 This publication gathers a range of short explorations of the idea of the Digital Public Space. The central vision of the Digital Public Space is to give everyone everywhere unrestricted access to an open resource of culture and knowledge. This vision has emerged from ideas around building platforms for engagement around cultural archives to become something wider, which this publication is seeking to hone and explore. 
 This is the first publication to look at the emergence of the Digital Public Space. Contributors include some of the people who are working to make the Digital Public Space happen. 
 The Digital Public Spaces publication has been developed by FutureEverything working with Bill Thompson of the BBC and in association with The Creative Exchange . 
 Editors: Drew Hemment, Bill Thompson, Jos  Luis de Vicente, Professor Rachel Cooper 
 Publisher: FutureEverything, 2013 
 Download Publication 
 Contributions: Tony Ageh (Controller, Archive Development, BBC); James Bridle (Writer, Artist, Technologist); Neville Brody (Royal College of Art); Jill Cousins (Executive Director, Europeana Foundation); Steve Crossan (Head of Google Cultural Institute); Paula Le Dieu (Mozilla Foundation); Drew Hemment (CEO, FutureEverything / Lancaster University); Andrew Hiskens (State Library of Victoria); Naomi Jacobs (Creative Exchange, Lancaster University); Bill Thompson (BBC, Archive Development / Royal College of Art); Jeremy Myerson (Royal College of Art); Kasia Molga (Artist); Mo McRoberts (BBC, Archive Development); Emma Mulqueeny (Rewired State); Jussi Parikka (Winchester School of Art); Paul Caplan (Winchester School of Art); Aaron Straup Cope (Cooper-Hewitt National Design Museum); Marleen Stikker (The Waag Society); Michelle Teran (Artist); Rachel Cooper (Director, Imagination Lancaster, Lancaster University); Charlie Gere (Lancaster University) 
 About FutureEverything Publications 
 Each year FutureEverything proposes, develops and responds to particular themes. These themes are provocations, designed to open up a space for debate and practice, made tangible through art and design projects. FutureEverything Publications seek to contribute to an international dialogue around these themes. 

 Downloads: 
 Related: 
 Projects   Events 

  Mapping the Digital Public Space 

 Previous Item 
 Next Item 

				  1995 - 2018 FutureEverything

					Design by  Supanaught  | Build by  Ravenous 

 Back to top 

 #futr 
 Mailing List 

Conference » FutureEverything
===============================

Conference   FutureEverything

 Jump to nav 
 Jump to content 

   Projects   

 Festival 

   Events   
   Ideas   
   History   

 About 

   News   
   Services   

   Facebook   
   Twitter   
   Vimeo   
   Instagram   
   Soundcloud   
   Flickr   
   Google+   

 Search the site 

 Home  /  2014 Manchester  /  Conference   

						City Fictions Newspaper - Download					 

						Art					 

						FutureEverything Conference Handbook Download					 

						How To Find City Fictions					 

						Information					 

						Live					 

						Conference					 

	Conference

 Monday 31 March   Tuesday 1 April, Manchester Town Hall 
The FutureEverything Conference features inspirational keynotes, participatory workshops and intimate talks from world leading practitioners and thinkers from design, art, urbanism, business, academia and more. 
 Conference Theme   Tools for Unknown Futures 
FutureEverything looks at how we can collaborate on new tools, devices and systems to transform our lives, from the arts to democracy, business and governance. 
 The most original, influential figures at the forefront of powerful currents in today s design scene converged on Manchester Town Hall, to imagine speculative futures around the technologies and innovations likely to change our lives. Leading figures from government and business looked at the next generation of decentralised, data intensive tools and platforms, where the cutting edge of data science and economics meet. 
 People taking control of tools to shape the future has been a familiar trope within digital culture. Drawing on powerful currents in today s design scene, such as speculative design and design fiction, FutureEverything debated our fascination with tools as the most natural path towards social change, and opened up new ways to question, imagine and make the strange, troubled thing called the future. 
 Keynotes, Participation, Workshops   Fireside Chats 
Over two days, attendees were inspired by keynote presentations from world leading speakers, participatory workshops where new ideas come to life, and a brand new  Fireside Chat  programme   a series of intimate, exploratory conversations with internationally renowned artists, designers, thinkers and business leaders. 

				  1995 - 2018 FutureEverything

					Design by  Supanaught  | Build by  Ravenous 

 Back to top 

 #futr 
 Mailing List 

 FutureEverything is an internationally recognised R&D hub for digital culture, and we present industry conferences, innovation projects, artworks and live experiences which showcase a digital future. 

FutureEverything is an internationally recognised R  hub for digital culture, and we present industry conferences, innovation projects, artworks and live experiences which showcase a digital future.
 WHOLE PROJECT MARK