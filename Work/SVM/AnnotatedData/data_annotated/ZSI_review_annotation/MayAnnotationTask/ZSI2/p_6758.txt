PROJECT NAME: Ourproject
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 Ourproject.org is a web-based collaborative free content repository. It acts as a central location for offering web space and tools for projects of any topic, focusing on free knowledge. It aims to extend the ideas and methodology of free software to social areas and free culture in general. Thus, it provides multiple web services (hosting, mailing lists, wiki, ftp, forums…) to social/cultural/artistic projects as long as they share their contents with Creative Commons licenses (or other free/libre licenses). 

Ourproject.org is a web-based collaborative free content repository. It acts as a central location for offering web space and tools for projects of any topic, focusing on free knowledge. It aims to extend the ideas and methodology of free software to social areas and free culture in general. Thus, it provides multiple web services (hosting, mailing lists, wiki, ftp, forums…) to social/cultural/artistic projects as long as they share their contents with Creative Commons licenses (or other free/libre licenses).

The idea behind the ourproject.org initiative is for it to be a tool which encourages the cooperative work effort of all types of people from every part of the world, promoting the coming together of people and the exchange of ideas and solutions to problems, with the condition that the results of the projects will remain freely accessible to whoever may find them useful, within this tool.


About ourproject.org

The idea behind the ourproject.org initiative is for it to be a tool which encourages the cooperative work effort of all types of people from every part of the world, promoting the coming together of people and the exchange of ideas and solutions to problems, with the condition that the results of the projects will remain freely accessible to whoever may find them useful, within this tool.

ourproject.org is a free service to Libre Projects collaborators (of any topic) offering easy access to the projects, mailing lists, message boards/forums, task management, site hosting, permanent file archival, full backups, and total web-based administration.

ourproject.org has as goal the development of libre projects preferably which are not related with software. Although we accept also free software projects we recommend the use of savannah.gnu.org.

Site Feedback and Participation

In order to get the most out of ourproject.org, you'll need to register as a site user. This will allow you to participate fully in all we have to offer. You may of course browse the site without registering, but will not have access to participate fully.

Set Up Your Own Project

Register as a site user, then Login and finally, Register Your Project. Thanks... and enjoy the site. 


Contributions and Collaborations

The best way to help us is with your collaboration in any of our free projects. For this please contact with the administrators of the specific project.

Or you can create a new free project if you don't find any existing project where you can collaborate.

If you are interested in joining us, translating any web page to any language, sending us your feedback, helping us to maintain this site, or doing any other kind of contribution, please contact us at admins@ourproject.org. Anyway, if you want to help us and you don't know how, please contact us and we would try to make some suggests that you may like.


 WHOLE PROJECT MARK
