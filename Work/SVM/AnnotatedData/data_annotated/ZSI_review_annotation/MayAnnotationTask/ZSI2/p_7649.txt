PROJECT NAME: FabLabs
PROJECT SOURCE: TRANSIT social innovation networks
---------------------------------------
NEW PAGE: 

Willkommen | FabLabs
===============================

Willkommen | FabLabs

 Labs 

 Orgs 

 Maschinen 

 Projects 

 Discuss 
 Anmelden 
 Registrieren 

 Zeig mir Fab Labs 

Weltweit

 Ich bin neu hier, was ist ein Fab Lab? 
 Fab Labs bieten Zugang zu modernen Entwicklungsmethoden. Was als Projekt am Center for Bits and Atoms (CBA) des MIT begann, wurde zu einem weltweiten Netzwerk der Zusammenarbeit. Mehr Informationen  ber Fab Labs findest du auf  Fab Foundation . Das untenstehende Foto zeigt die Ausstattung eines typischen Fab Labs. 

Concept and photo from
 Fab Lab Vestmannaeyjar 
,
 Iceland 

 F r wen ist fablabs.io gedacht? 

I'm an
 ENTHUSIAST 
and I want to participate with the global Fab Lab Network. Find labs near me, see what's happening in the labs around the world and maybe one day open my own!

I'm a
 FAB LAB MANAGER 
and I want to add my lab to the network. Then I'll be able to share information and knowledge from my lab and interact with fellow users of the site.

Sign up

 Und wie kann ich mitmachen? 

 Lab Profile 
 Each lab has its own profile page which can be maintained and updated by its staff 

 Weltkarte 
 An interactive map showing the location of all the Fab Labs around the world 

 Maschinen 
 A growing directory/wiki of all of the tools used and recommended by Fab Labs 

 Was kommt als N chstes? 

 ffnungszeiten 
 See at a glance when labs will be open and available for use 

 Veranstaltungen 
 A calendar of all upcoming events within the global Fab Lab Network 

 Diskussion 
 You will have a places to discuss general, lab-specific and tool-specific topics 

 Entwickler API 
 A REST-based API that can be used to fetch and change data on the platform 

 Standort 
 We want the platform to be translated and localised for everyone. More details to follow 

 Ereignisverlauf 
 See what you've missed while you were away, and what's happening right now 

 Und au erdem? 

 We've got lots of ideas that we hope to implement but if you have any suggestions or problems with the site, please click the ? icon at the bottom right of any page. 

 Labs List 
 Labs Map 
 About 
 Discuss 
 The Fab Foundation 
 Cookie Policy 
 Privacy Policy 
 Terms of Service 

 Deutsch 
 English 
 Espa ol 
 Fran ais 
 Italiano 
 Nederlands 
 Portugu s 

Co-funded by the Creative Europe programme of the European Union

 How can we help you? 

 Welcome to Fablabs.io! 

Here are few suggestions about how we can help you and how you can participate
and talk with us if you have any question or problem regarding the Fablabs.io
platform.

 Discuss with the community 

You can ask for support to the administrators and the whole community
in the Fablabs.io Discuss section within the
 Site feedback category .
 Site feedback category 
we would also love to hear your ideas and suggestions regarding Fablabs.io,
its features, development, how it works and how we can improve it.

 Discuss with the community 

 Help with the technical development on GitHub 

If you have instead messages regarding the technical development or bugs
of the platform, we would ask you to discuss these on the Fablabs.io repositories
on GitHub (Fablabs.io is free/open source software!) at
 https://github.com/fablabbcn/fablabs .

 Join the development 

 E-mail the administrators 

If you need instead to contact the administrators of Fablabs.io, please
write to
 webmasters@fablabs.io .

 Write an e-mail to the administrators
 WHOLE PROJECT MARK