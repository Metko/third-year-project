PROJECT NAME: Booktype
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

Everything a book can be

Booktype is web based, single-source publishing software for creating beautiful books, reports, manuals and more. An open source platform ideal for editorial teams working on complex projects, Booktype is very, very fast.

Manuscripts can be imported from Word or EPUB files; the finished publication can be exported for short run or litho print, e-readers, tablets, mobile devices and websites, and to Adobe InDesign or Microsoft Word.

Create once, publish everywhere.

    Try the demo
    See how it works

    Write

    The clean, intuitive authoring environment lets you concentrate on writing. For images and formatting, just drag-and-drop.
    Collaborate

    Authors, editors, and other stakeholders can work and communicate simultaneously, so books come together faster than ever before.
    Publish

    Booktype software gives you seamless publishing for print and digital formats compatible with all major sellers of e-books.

WHO'S USING IT?

    Amnesty International epubli Holtzbrinck digital publishing Books on Demand mikrotext verlag von Nikola Richter Elevforlaget - student publishing 

See more members of the Booktype community
Sydney Morgan Currie

by Sydney Morgan Currie 21.07.2017
Working with the publishing scene of Togo
Micz Flor

by Micz Flor 23.05.2017
Table of contents becomes the book’s dashboard
Micz Flor

by Micz Flor 16.03.2017
Adobe InDesign integration with Booktype’s publishing platform
Julian Sorge

by Julian Sorge 13.03.2017
Amnesty’s Annual Report, a case study in single source publishing
Micz Flor

by Micz Flor 19.10.2016
The social book marks the biggest content shift in publishing
Patricia Marcoccia

by Patricia Marcoccia 29.09.2016
Know your copy rights
Letizia Gambini

by Letizia Gambini 6.09.2016
Elevforlaget: students write and publish their own books
Julian Sorge

by Julian Sorge 9.08.2016
Booktype 2.1 - functionality meets design
Patricia Marcoccia

by Patricia Marcoccia 3.08.2016
The top seven features authors love in Booktype
Letizia Gambini

by Letizia Gambini 27.06.2016
New features for Books on Demand
Julian Sorge

by Julian Sorge 19.06.2016
How NGOs benefit from Single Source Publishing
Micz Flor

by Micz Flor 16.06.2016
Creating layouts for automated book publishing
Get more news


Manage your wealth of ideas

Go from zero to published in minutes.

    Try the demo

    Start today
    We offer managed hosting for companies and publishers. Authors can start for free on Omnibook, our social platform for writing books. You can start today.
    Import, manage, publish
    Import existing MS Word documents or EPUB files into Booktype. Edit content online in the browser and export for print and digital distribution.
    Collaborate online
    Multiple authors, editors, proofreaders and other stakeholders can work and communicate simultaneously. This means books come together faster than ever before.

KEEP YOUR BOOKS ALIVE

Booktype provides companies, publishers and authors with one place to manage all their books. Activity streams, workflow management, MS Word import, granular permissions and customisable exporting of books keep your content alive.

Store your visuals

Booktype also lets you upload and store all your book visual assets – photos, illustrations, charts, cover art – you name it.

Import word files and ebooks

Import your back archive into Booktype to bring it back to life. Booktype imports MS Word and EPUB files. The structure and assets of your document are preserved.

Track changes

Review the latest changes and current state of your book chapter by chapter, line by line. You can set “Track Changes” if more than one person contributes to or edits your book.

Manage editorial teams

Each book has its own overview page, which lets you see who's working on it, recent activity and project history.

Manage your workflow

Inform your team on the status of your book chapter by chapter. This workflow allows everybody to quickly grasp what’s to be done. Workflows can be customised book by book in the administration view for book owners and publishers.

Quick start

Getting started is easy. Just go to https://demo.booktype.pro/ and sign up. Once you have created an account, simply log in and enjoy Booktype.

Go from zero to published in minutes

The Booktype editing screen gives you the tools you need – and nothing more. We've removed unnecessary distractions so you can concentrate on writing and editing.


Make Booktype work for you

As the developer of Booktype, we provide the most dependable managed hosting available. Download the price list for managed Booktype hosting to see our special offers for events and starter packages. Our servers are physically located in Germany with plenty of resources and great availability.

We also provide professional services for Booktype, even if you choose to self-host; from templates and custom features to workflow solutions. We also develop white-label platforms featuring your branding. Download the price list for Booktype platform hosting.

    Registered users
    Number of books
    Backup plan
    Server maintenance

 
Self-hosting
Free

/ forever

    You decide
    You decide
    Your own
    Your own

Office
€49

/ month

    3 users
    20 books
    Daily backups
     

Agency
€99

/ month

    10 users
    100 books
    Daily backups
     

Company
€199

/ month

    50 users
    Unlimited
    Daily backups
     

Platform
€499

/ month

    300 active users *
    Unlimited
    Daily backups
     


* Platform pricing is for active users per month: number of registered users unlimited.

    One-off setup and configuration costs for a custom Booktype instance: €500.
    Hosting subscriptions can be terminated giving two weeks' notice to the end of the month.
    Annual subscriptions paid upfront receive a discount of 5%. Please see the price lists for other special offers.
    Platform clients reaching the user limit can unlock each 100 additional users with an extra monthly charge of €50.


Extending Booktype's functionality to match your needs and integration with your existing IT infrastructure is possible. Contact us for details.

    Managed services

    To make sure you are always up to date, sleep easy and don’t have to get your hands dirty, digitally speaking, we offer scalable hosting of Booktype, provide regular updates, backups and 24/7 uptime monitoring. What’s not to love?
    Implementation and integration

    Integrating Booktype into your existing IT infrastructure might be easier than you think. We have the experience. See how we helped Books on Demand integrate Booktype into their self-publishing platform.
    Features and functionality

    Extend functionality or build bridges between systems. Our expert developers have years of experience to assist you in bringing your ideas to life. See how we helped Amnesty International integrate InDesign to publish their Annual Report.



Who's using it?

Booktype is relied on by publishers all over the world.

    Try the demo

        Amnesty International

        London, United Kingdom

        Amnesty International is a global movement of more than 7 million people who take injustice personally. They are campaigning for a world where human rights are enjoyed by all. See the case study...

        "We’ve been able to make a very significant improvement to Amnesty’s Annual report production workflow. Booktype is intuitive and easy to use, and makes complete sense for an organisation where there are many different stakeholders in different locations." - Richard Swingler, Global Production Services Manager

        Go to www.amnesty.org

    Amnesty International

        epubli - Holtzbrinck Publishing

        Berlin, Germany

        The self-publishing service provider epubli offers Booktype's in-browser book editor to write and manage books or upload manuscripts in Word or EPUB format directly to their platform. epubli is part of the Holtzbrinck Publishing Group.

        Go to epubli (site in German).

    epubli - Holtzbrinck Publishing

        Books on Demand

        Nordestadt, Germany

        BoD is the European market and technology leader for digital book publications and a platform for authors who want to self-publish their books in electronic and printed form. See the case study...

        Go to www.bod.de

    Books on Demand

        mikrotext

        Berlin, Germany

        Mikrotext is a digital publisher for short digital reading founded in 2013 in Berlin. It was awarded the Young Excellence Award 2014by the German booksellers' and publishers' association (Börsenverein des deutschen Buchhandels). Mikrotext is using Booktype to make beautiful e-books. 

        "Booktype enables me to collaboratively create e-books with a team of writers as well as speed up our e-book production." Nikola Richter, publisher of mikrotext

        Go to http://www.mikrotext.de/

    mikrotext

        TV2 | Elevforlaget

        Olso, Norway

        Elevforlaget is an online educational resource for students to write and publish their own printed books and e-books. It was launched by the Norwegian Publishers’ Association and has been acquired by TV2, Norway's most popular private TV channel. Translated into English, Elevforlaget means “student publisher”. Using the online platform, students from all over Norway can work together and collaborate in publishing their own books. The platform is based on Booktype.

        Go to Elevforlaget and TV2.no (sites in Norwegian).

    TV2 | Elevforlaget

        University of Sussex

        Sussex, Great Britain

        The Research Hive at the University of Sussex Library used Booktype for their latest book sprint as the social writing software. Their event was laid out to “want to encourage new ways of thinking about academic writing”.

        Go to the University of Sussex website.

    University of Sussex

        RWCC - Rüdiger Wischenbart

        Vienna, Austria

        RWCC - Rüdiger Wischenbart is a consulting company that maps and analyses the global publishing industry and international book cultures.

        "For the launch of the Global eBook report 2016, we were able to address a full house of interested book professionals in a special session at the London Book Fair. Compiling a report of some 250 pages, with 50 graphs and tables, on market developments from across the world was an exciting experience, as it has been every year since 2011. This year was different as we were able to use Booktype as our authoring system for the first time. Frankly, we were a little nervous at first, but today I can relax and congratulate the Booktype team on their great system and wonderful support!” Rüdiger Wischenbart

        Go to http://www.wischenbart.com/

    RWCC - Rüdiger Wischenbart

        Max Planck Institute of Biochemistry

        Martinsried, Germany

        The Max Planck Society is Germany's most successful research organisation with no fewer than 18 Nobel laureates. The Max Planck Institute of Biochemistry (MPIB) in Martinsried near Munich is one of the largest institutes within the Max Planck Society with about 850 employees coming from 45 nations. They are using Booktype for internal documentation. Due to the high demands on security, Booktype is hosted in their Intranet.

        Go to www.biochem.mpg.de/en

    Max Planck Institute of Biochemistry

        ecosoft

        Bangkok, Thailand

        Ecosoft is a company that produces ERP (Enterprise resource planning) open source software. They decided to adopt Booktype to produce their internal manuals. Their main reasons for choosing Booktype is that it allows them to produce PDF documents which look fantastic when printed and that the Booktype editing interface looks nearly identical to that of Word, which makes it easy to use!

        Go to http://www.ecosoft.co.th/

    ecosoft

        CryptoParty.org

        Dresden, Germany

        CryptoParty promotes knowledge of basic cryptography software to the general public. They use Booktype to write and update their handbook.

        Go to cryptoparty.org

    CryptoParty.org

        Donau University Krems

        Krems an der Donau, Austria

        Donau University Krems is now offering Booktype as a new tool for collaborative writing and publishing of books. Peter Baumgartner is using Booktype for a class to go deeper into "educational technology" as part of the master seminar "eEducation". 

        Go to http://www.donau-uni.ac.at/en/

    Donau University Krems

        The Norwegian Publishers Association

        Oslo, Norway

        The Norwegian Publishers Association is a trade organisation for publishers. Their members represent approx. 85 % of the sales from publishers to booksellers in the country. They manage joint market ventures, provide statistics, offer standard contracts and provide service for their publisher members. They maintain strong links to authorities, domestic organisations in the cultural industry in general and the book industry in particular, as well as sister organisations abroad.

        Go to http://www.forleggerforeningen.no/in-english/

    The Norwegian Publishers Association

        Université de Lyon / ENS

        Lyon, France

        The University of Lyon is promoting Archinfo, a MOOC in "Architecture de l'information / information architecture" that intends to offer an initiation to information architecture, its notions, its methods, and its tools.

        "More than 1000 students of the MOOC Archinfo gathered and documented six different competences in their own digital booklets with Booktype. On top of that, feedback given by four peers led each student to enhance his booklet before the final evaluation." - Pierre Bénech, pedagogical engineer

        Go to: www.ens-lyon.eu

    Université de Lyon / ENS

        Open Knowledge

        Cambridge, United Kingdom

        Open Knowledge is a worldwide non-profit network of people passionate about openness, using advocacy, technology and training to unlock information and enable people to work with it to create and share knowledge.

        Go to https://okfn.org/

    Open Knowledge

        FLOSS Manuals

        Amsterdam, Netherlands

        The international FLOSS Manuals community aims to make it easy to maintain and contribute to manuals, text books and teaching materials by providing an easy to use interface for collaboration. FLOSS Manuals served as the primary implementation of Booktype.  

        Go to en.flossmanuals.net

    FLOSS Manuals

        University College London

        London, United Kingdom

        UCL's Department of Scandinavian Studies used Booktype in their collaborative learning exercise Framed Horizons: Student Writing on Nordic Cinema. Read more about the launch of the book at the Frankfurt Book Fair.

        'I was impressed at how seamless and almost invisible Booktype was in the project. It was very empowering for all involved, it seemed to remove hierarchy and made the whole editing process very transparent.' — Marita Fraser, UCL IT specialist

        Go to www.ucl.ac.uk

 Booktype is a free and open source oftware for authoring, collaborating, editing, and publishing books to PDF, ePub, .mobi, ODT and HTML formats. It was launched by Sourcefabric in February 2012 when Booktype evolved from the Booki software, which powers FLOSS Manuals.---------------------------------------



What is Booktype?

Updated for Booktype 2.3.0

With Booktype you can create books. Real, good-looking paper books you can hold in your hands. You can also use Booktype's output to PDF, EPUB, XML and HTML to export books ready for Amazon, iBooks, or the open web.

Booktype is designed to help you produce books with others, or by yourself. Booktype is also a social environment where you can share notes on producing books, seek assistance from others, or look for projects to contribute to.

Booktype is web-based software which means you do not install it on your computer; rather, you access it through a web browser. It is Free Software (licensed under the GNU Affero GPL), meaning that it can be freely downloaded, re-used and customised. You can install your own copy of Booktype on a server, or alternatively you can set up a cloud-hosted Booktype Pro account and let the Booktype team take care of installation, hosting and security.

Some typical uses of the platform might be:

    Writing a work of fiction, manuals, cookbooks etc.
    Producing printed books
    Producing ebooks
    Writing any content as an individual
    Collaboratively authoring content
    Customising existing content to apply to a very specific context
    Translating a book into another language

While you can use Booktype to support traditional book authoring processes, Booktype also supports the rapid development of content in 'book sprints'. A book sprint is an intensive collaborative event where typically six to ten people can focus on writing a book in three to five days. These collaborators might be in the same room or located around the Internet; in either case, they work together simultaneously to produce a book.

Booktype was originally developed to facilitate the accelerated production models enabled by book sprints. Hence the feature set matches the rapid pace of publishing possible in the era of print-on-demand. Using Booktype, print ready source (book formatted PDF) can be generated in a few minutes, and then uploaded to your favourite print-on-demand service.

Whether you are collaborating, writing solo, rapidly developing books, or taking your time - after you have worked with Booktype, you will never think of publishing the old way again.


Booktype is a free and open source oftware for authoring, collaborating, editing, and publishing books to PDF, ePub, .mobi, ODT and HTML formats. It was launched by Sourcefabric in February 2012 when Booktype evolved from the Booki software, which powers FLOSS Manuals.
 WHOLE PROJECT MARK
