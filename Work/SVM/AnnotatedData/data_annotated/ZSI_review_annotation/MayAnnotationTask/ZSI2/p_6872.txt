PROJECT NAME: Texmaker
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

Texmaker (free cross-platform latex editor) 
===============================

Texmaker (free cross-platform latex editor) 

 Download 
 Documentation 
 Screenshots 
 Changelog 
 Contact 

 TEX MAKER 
 Free cross-platform LaTeX editor since 2003 
 (Windows, MacOsX, Linux) 
 DOWNLOAD version 5.0.2    

                "Powerful, easy to use and elegant" 

 Powerful Editor 
 with unicode support, spell checking, auto-completion, code folding 

 Integrated Pdf viewer 
 with synctex support and continuous view mode 

 Features 

 Texmaker  is a free, modern and cross-platform LaTeX editor for  linux ,  macosx  and  windows  systems that integrates many tools needed to develop documents with LaTeX, in just one application. Texmaker  includes unicode support, spell checking, auto-completion, code folding and a built-in pdf viewer with synctex support and continuous view mode. 
 Texmaker  is easy to use and to configure. 
 Texmaker  is released under the GPL license . 

 Unicode editor Texmaker  is fully unicode and supports a large variety of encodings.

 Spell checker Texmaker  includes spell checking while typing.

 Code folding All \part, \chapter, \section,.., \begin{foo} \end{foo} blocks can be collapsed.

 Code completion The main LaTeX commands can be quickly inserted while typing.

 Fast navigation Texmaker  includes a "structure view" which is automatically updated while typing.

 "Master mode" Texmaker  allows you to work easily onto documents separated in several files with the "master mode".

 "Integrated Pdf viewer" Texmaker  includes a built-in pdf viewer with continuous scrolling and synctex support.

 "Easy" compilation "One-click" compilation with the predefined "Quick build" commands.

 Mathematical symbols 370 mathematical symbols can be inserted in just one click.

 Wizards Texmaker  includes wizards to generate the most standard LateX code ('Quick document', 'Quick Beamer Presentation' , 'Quick letter', tabular, tabbing and array environments.

 LaTeX documentation An extensive LaTeX documentation is furnished with  Texmaker .

 Error Handling Texmaker  automatically locates errors and warnings detected in the log file after a compilation and you can reach the corresponding lines in the document in one-click.

 Rectangular block selection Easy rectangular selection with the mouse+Alt key. Users can easily cut/copy/paste columns of a table.

 Find in folders With  Texmaker  you can search for text in all the latex documents included in a folder (and the subfolders). If you click on a line,  Texmaker  will open the corresponding document at the right line.

 Rotation mode for the pdf viewer The integrated pdf viewer supports rotation mode.

 Regular expressions support You can use regular expressions for "search and replace".

 Full asymptote support (syntax higlighting - commands - compilation in "one click")

 Unlimited number of snippets Users can define an unlimited number of snippets with keyboard triggers

 Contact : Drop Us A Line 

 Your Message 

  2003 / 

  - Pascal Brachet 
 Texmaker : original LaTeX editor since 2003 by the creator of Kile 
This program is licensed to you under the terms of the GNU General Public License Version 2 as published by the Free Software Foundation.
Note : This program is the "authentic" Texmaker editor and there is strictly no relationship between this original program developed since 2003 and another latex editor, not compatible with Texmaker, which pretends to be based on Texmaker to trick users. 

 Texmaker is a free, modern and cross-platform LaTeX editor for linux, macosx and windows systems that integrates many tools needed to develop documents with LaTeX, in just one application. 

Texmaker is a free, modern and cross-platform LaTeX editor for linux, macosx and windows systems that integrates many tools needed to develop documents with LaTeX, in just one application.
 WHOLE PROJECT MARK