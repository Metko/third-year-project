PROJECT NAME: Amul
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 Amul is an Indian dairy cooperative, based at Anand in the state of Gujarat, India.[2] The word amul (अमूल) is derived from the Sanskrit word amulya (अमूल्य), meaning invaluable.[3] The co-operative was initially referred to as Anand Milk Federation Union Limited hence the name AMUL.

Formed in 1946, it is a brand managed by a cooperative body, the Gujarat Co-operative Milk Marketing Federation Ltd. (GCMMF), which today is jointly owned by 3 million milk producers in Gujarat.[4]

Amul spurred India's White Revolution, which made the country the world's largest producer of milk and milk products.[5] In the process Amul became the largest food brand in India and has ventured into markets overseas.

Dr Verghese Kurien, founder-chairman of the GCMMF for more than 30 years (1973–2006), is credited with the success of Amul.[6] 

Amul is an Indian dairy cooperative, based at Anand in the state of Gujarat, India.[2] The word amul (अमूल) is derived from the Sanskrit word amulya (अमूल्य), meaning invaluable.[3] The co-operative was initially referred to as Anand Milk Federation Union Limited hence the name AMUL.

Formed in 1946, it is a brand managed by a cooperative body, the Gujarat Co-operative Milk Marketing Federation Ltd. (GCMMF), which today is jointly owned by 3 million milk producers in Gujarat.[4]

Amul spurred India's White Revolution, which made the country the world's largest producer of milk and milk products.[5] In the process Amul became the largest food brand in India and has ventured into markets overseas.

Dr Verghese Kurien, founder-chairman of the GCMMF for more than 30 years (1973–2006), is credited with the success of Amul.[6]
 WHOLE PROJECT MARK