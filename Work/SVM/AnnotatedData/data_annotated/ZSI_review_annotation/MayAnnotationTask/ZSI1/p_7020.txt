PROJECT NAME: CyanogenMod
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 is an open source operating system for smartphones and tablet computers, based on the Android mobile platform. It is developed as free and open source software based on the official releases of Android by Google, with added original and third-party code. CyanogenMod releases are provided on a nightly, milestone, and "stable version" schedule.

CyanogenMod offers features and options not found in the official firmware distributed by mobile device vendors. Features supported by CyanogenMod include native theming support,[6] FLAC audio codec support, a large Access Point Name list, an OpenVPN client, revoking application permissions, support for Wi-Fi, Bluetooth, and USB tethering, CPU overclocking and other performance enhancements, soft buttons and other "tablet tweaks", toggles in the notification pull-down (such as Wi-Fi, Bluetooth and GPS), app permissions management, as well as other interface enhancements. According to its developers, CyanogenMod does not contain spyware or bloatware.[7][8] CyanogenMod is also stated to increase performance and reliability compared with official firmware releases.[9]

Although only a subset of total CyanogenMod users elect to report their use of the firmware,[10] as of February 2014, CyanogenMod has recorded over 11 million active installs on a multitude of devices.[11][12]

In 2013, project founder Steve Kondik announced that venture funding had been obtained to establish Cyanogen Inc. as a commercial enterprise to develop and market the firmware more widely. This announcement has led to controversy within the community, with some developers asserting that rights and licensing issues, acknowledging/compensating past developers and honoring the original ethos of the community project, are not being adequately addressed. 

is an open source operating system for smartphones and tablet computers, based on the Android mobile platform. It is developed as free and open source software based on the official releases of Android by Google, with added original and third-party code. CyanogenMod releases are provided on a nightly, milestone, and "stable version" schedule.

CyanogenMod offers features and options not found in the official firmware distributed by mobile device vendors. Features supported by CyanogenMod include native theming support,[6] FLAC audio codec support, a large Access Point Name list, an OpenVPN client, revoking application permissions, support for Wi-Fi, Bluetooth, and USB tethering, CPU overclocking and other performance enhancements, soft buttons and other "tablet tweaks", toggles in the notification pull-down (such as Wi-Fi, Bluetooth and GPS), app permissions management, as well as other interface enhancements. According to its developers, CyanogenMod does not contain spyware or bloatware.[7][8] CyanogenMod is also stated to increase performance and reliability compared with official firmware releases.[9]

Although only a subset of total CyanogenMod users elect to report their use of the firmware,[10] as of February 2014, CyanogenMod has recorded over 11 million active installs on a multitude of devices.[11][12]

In 2013, project founder Steve Kondik announced that venture funding had been obtained to establish Cyanogen Inc. as a commercial enterprise to develop and market the firmware more widely. This announcement has led to controversy within the community, with some developers asserting that rights and licensing issues, acknowledging/compensating past developers and honoring the original ethos of the community project, are not being adequately addressed.
 WHOLE PROJECT MARK