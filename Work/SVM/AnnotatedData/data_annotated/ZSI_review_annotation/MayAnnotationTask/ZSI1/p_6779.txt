PROJECT NAME: SETI@home
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

SETI@home
===============================

SETI@home

 Project

 Help 
 Donate 
 Porting 
 Graphics 
 Add-ons 

 Science

 About SETI@home 
 About Astropulse 
 Science newsletters 
 Nebula 

 Computing

 Statistics 
 Server status 
 Technical news 
 Applications 
 Certificate 
 World view 

 Community

 Message boards 
 Questions and Answers 
 Teams 
 Profiles 
 User search 
 Web sites 
 Pictures and music 
 User of the day 

 Site

 Site search 
 Languages 
 Help 

 Sign Up 
 Login 

    SETI@home is temporarily shut down for maintenance.  Please try again later. 

         2018 University of California

        SETI@home and Astropulse are funded by grants from the National Science Foundation, NASA, and donations from SETI@home volunteers. AstroPulse is funded in part by the NSF through grant AST-0307956. 

 SETI@home ("SETI at home") is an Internet-based public volunteer computing project employing the BOINC software platform, hosted by the Space Sciences Laboratory, at the University of California, Berkeley, in the United States. SETI is an acronym for the Search for Extra-Terrestrial Intelligence. Its purpose is to analyze radio signals, searching for signs of extra terrestrial intelligence, and is one of many activities undertaken as part of SETI. Everyone can participate by running a free program that downloads and analyzes radio telescope data obtained.---------------------------------------
NEW PAGE: 

SETI@home
===============================

SETI@home

 Project

 Help 
 Donate 
 Porting 
 Graphics 
 Add-ons 

 Science

 About SETI@home 
 About Astropulse 
 Science newsletters 
 Nebula 

 Computing

 Statistics 
 Server status 
 Technical news 
 Applications 
 Certificate 
 World view 

 Community

 Message boards 
 Questions and Answers 
 Teams 
 Profiles 
 User search 
 Web sites 
 Pictures and music 
 User of the day 

 Site

 Site search 
 Languages 
 Help 

 Sign Up 
 Login 

About SETI@home
The science of SETI@home
SETI (Search for Extraterrestrial Intelligence) is a scientific area whose goal is to detect intelligent life outside Earth. One approach, known as radio SETI, uses radio telescopes to listen for narrow-bandwidth radio signals from space. Such signals are not known to occur naturally, so a detection would provide evidence of extraterrestrial technology.

Radio telescope signals consist primarily of noise (from celestial sources and the receiver's electronics) and man-made signals such as TV stations, radar, and satellites. Modern radio SETI projects analyze the data digitally. More computing power enables searches to cover greater frequency ranges with more sensitivity. Radio SETI, therefore, has an insatiable appetite for computing power.

Previous radio SETI projects have used special-purpose supercomputers, located at the telescope, to do the bulk of the data analysis. In 1995, David Gedye proposed doing radio SETI using a virtual supercomputer composed of large numbers of Internet-connected computers, and he organized the SETI@home project to explore this idea. SETI@home was originally launched in May 1999.

    SETI at Berkeley - a portal to all of U.C. Berkeley's SETI-related projects
    Learn more about how SETI@home works
    Screensaver graphics explained
    Science news
    Technical news
    Server status
    Science status
    Science links
    Bookstore
    BOINC
    Sponsors
    Glossary of terms
    Future plans
    Photo albums
    Arthur C. Clarke Tribute
    SETI@home 10 Year Anniversary (schedule and videos of talks) 

Papers about SETI@home science and computing:

    Status of the UC-Berkeley SETI Efforts (Korpela, et al. 2011)
    New SETI Sky Surveys for Radio Pulses (Siemion, et al. 2008)
    Statistics of One: What Earth can and can't tell us about life in the universe. (Korpela, 2004)
    SETI@home: An Experiment in Public-Resource Computing (Anderson, et al. 2002)
    SETI@home-Massively distributed computing for SETI (Korpela, et al. 2001)
    A new major SETI project based on Project Serendip data and 100,000 personal computers (Sullivan, et al. 1997)

SETI@home is based on BOINC, and is an outgrowth of the original SETI@home (now called 'SETI@home Classic'). Details of the transition are here . 

        SETI@home and Astropulse are funded by grants from the National Science Foundation, NASA, and donations from SETI@home volunteers. AstroPulse is funded in part by the NSF through grant AST-0307956. 

 SETI@home ("SETI at home") is an Internet-based public volunteer computing project employing the BOINC software platform, hosted by the Space Sciences Laboratory, at the University of California, Berkeley, in the United States. SETI is an acronym for the Search for Extra-Terrestrial Intelligence. Its purpose is to analyze radio signals, searching for signs of extra terrestrial intelligence, and is one of many activities undertaken as part of SETI. Everyone can participate by running a free program that downloads and analyzes radio telescope data obtained. 

 WHOLE PROJECT MARK
