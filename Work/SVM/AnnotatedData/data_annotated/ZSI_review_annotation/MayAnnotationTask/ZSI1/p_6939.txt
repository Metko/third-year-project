PROJECT NAME: Hospitality Club
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 Our aim is to bring people together - hosts and guests, travelers and locals. Thousands of Hospitality Club members around the world help each other when they are traveling - be it with a roof for the night or a guided tour through town. Joining is free, takes just a minute and everyone is welcome. Members can look at each other's profiles, send messages and post comments about their experience on the website.

The club is supported by volunteers who believe in one idea: by bringing travelers in touch with people in the place they visit, and by giving "locals" a chance to meet people from other cultures we can increase intercultural understanding and strengthen the peace on our planet.

More on the about us: http://www.hospitalityclub.org/hospitalityclub/about.htm 

Our aim is to bring people together - hosts and guests, travelers and locals. Thousands of Hospitality Club members around the world help each other when they are traveling - be it with a roof for the night or a guided tour through town. Joining is free, takes just a minute and everyone is welcome. Members can look at each other's profiles, send messages and post comments about their experience on the website.

The club is supported by volunteers who believe in one idea: by bringing travelers in touch with people in the place they visit, and by giving "locals" a chance to meet people from other cultures we can increase intercultural understanding and strengthen the peace on our planet.

More on the about us: http://www.hospitalityclub.org/hospitalityclub/about.htm
 WHOLE PROJECT MARK