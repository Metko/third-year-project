PROJECT NAME: BeWelcome
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 Our mission is to provide a platform for hospitality and culture exchange and to manage the volunteers involved. The safety of our members and their privacy is our main concern.

With members from many different countries and cultural backgrounds we highly value the challenge and opportunity to learn from each other and openly listen to different opinions, critics and suggestions.

For us the "how" of working together is of key importance and our work style is reflected in our work ethics and organisational values. It is our desire and measure of success to be seen as an organisation where members work together in an enriching, democratic, innovative, friendly and motivating way. 

Our mission is to provide a platform for hospitality and culture exchange and to manage the volunteers involved. The safety of our members and their privacy is our main concern.

With members from many different countries and cultural backgrounds we highly value the challenge and opportunity to learn from each other and openly listen to different opinions, critics and suggestions.

For us the "how" of working together is of key importance and our work style is reflected in our work ethics and organisational values. It is our desire and measure of success to be seen as an organisation where members work together in an enriching, democratic, innovative, friendly and motivating way.
 WHOLE PROJECT MARK