PROJECT NAME: Lone parent support
PROJECT SOURCE: WILCO cases
---------------------------------------
NEW PAGE: 
 

The lone parent support project was delivered by Employment Needs Training Agency (ENTA) and partners, and
offered a holistic approach to addressing barriers to work for lone parents. Once contacted people were assessed,
agreed an action plan and then received on-going support and mentoring from the project team (this continued
once they had a job or placement). Training had always been an issue for many lone parents as this was not always
child-friendly. The project funded some child care and travel costs to volunteering and training sessions and clients
were able to bring their children into the project (unlike the case for many other statutory agencies) and this helped
with issues surrounding child care. The funding for the project came through the Innovation Fund of the WNF.
 WHOLE PROJECT MARK
