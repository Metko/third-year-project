PROJECT NAME: GROWL - training materials for degrowth
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 GROWL is a pan-European project designed to form a network of  trainers for degrowth.  Academics, practitioners at the grass-roots  level, researchers and political activists, among others, form a part of  this network that aims at a plural and diverse skill and knowledge  exchange, to support a major transition towards degrowth and a  sustainable and fulfilling society.

GROWL's concrete objective is to provide tools for positive change in  individual lives and overall society, in response/as an alternative to  the multi-dimensional crises (economic, social, ecological, human) that  currently confront Europe.

All materials produced in GROWL are open and can be reused under a Creative Commons license. The production of the materials, in particular the modules, relies on collaboration between the different partners of GROWL, as well as future trainers (participants in the courses) of GROWL. 

GROWL is a pan-European project designed to form a network of  trainers for degrowth.  Academics, practitioners at the grass-roots  level, researchers and political activists, among others, form a part of  this network that aims at a plural and diverse skill and knowledge  exchange, to support a major transition towards degrowth and a  sustainable and fulfilling society.

GROWL's concrete objective is to provide tools for positive change in  individual lives and overall society, in response/as an alternative to  the multi-dimensional crises (economic, social, ecological, human) that  currently confront Europe.

All materials produced in GROWL are open and can be reused under a Creative Commons license. The production of the materials, in particular the modules, relies on collaboration between the different partners of GROWL, as well as future trainers (participants in the courses) of GROWL.


About
Living degrowth

GROWL is a pan-European project designed to form a network of  trainers for degrowth.  Academics, practitioners at the grass-roots  level, researchers and political activists, among others, form a part of  this network that aims at a plural and diverse skill and knowledge  exchange, to support a major transition towards degrowth and a  sustainable and fulfilling society.

GROWL's concrete objective is to provide tools for positive change in  individual lives and overall society, in response/as an alternative to  the multi-dimensional crises (economic, social, ecological, human) that  currently confront Europe.
Courses

As part of the EU Grundtvig Learning Partnership, which supports the creation of the GROWL network, 9 courses are taking place between January 2014 and July 2015. These are 3-10 days intensive courses, composed by two core modules modules, as well as a thematic, praxis-oriented module, which is specific to each degrowth course.

One core module is on degrowth content, and provides an introduction to the main degrowth authors and concepts of degrowth. The other core module is called the "Train-the-trainer" and includes methodologies, explanations on the functioning of the GROWL network, how to prepare courses, etc.. This module is optional, and is destined for those participants that intend to become trainers on degrowth as part of the GROWL network.
Commons-based peer production

All materials produced in GROWL are open and can be reused under a Creative Commons license. The production of the materials, in particular the modules, relies on collaboration between the different partners of GROWL, as well as future trainers (participants in the courses) of GROWL.
Becoming a GROWL trainer
For becoming a GROWL trainer, you need to:

    take part in at least two GROWL courses
    prepare an assignment between the two courses, which includes content development and a course/workshop proposal connected with the chosen topic
    present it  to the network and at the second GROWL course
    get a positive feedback from the GROWL community and two assigned reviewers
 WHOLE PROJECT MARK
