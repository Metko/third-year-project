PROJECT NAME: Impact Hub Rwanda
PROJECT SOURCE: ICT for social innovations
---------------------------------------
NEW PAGE: 

Impact Hub Kigali - Where innovation and impact meets
===============================

Impact Hub Kigali - Where innovation and impact meets

 Home 
 Who We Are 
 What We Do 

 Coworking 
 Host Events 

 Contact Us 

 Join our community events 

 Visit our space 

 Host conversations with us 

 Community and space in Kigali! 
 Impact Hubs catalyze a vibrant community, meaningful content, and an inspiring space to help you move from intention to impact. While every Impact Hub offers a collaborative space and supportive working environment, each has its own unique local flavor. 
 Impact Hub Kigali s community has been at the forefront of coworking in Kigali since 2012 and this quarter we offer you a menu of flexible office solutions. 
 Find out More 

 Three Ways to choose us 

 Flexible Office Space 

 Do you need a place to work away from a coffee shop, your home or the pod in your office? We have just the right spot in our space. 

 Connect Membership 

 We invite you to join our community of social enterprises to access hosting and participation in our networking and learning events all year round. 

 Events Partners 

 Our events have a social mission and we bring the community together every month on a thematic area. We invite partners with the same mission. 

 IMPACT HUBS AROUND THE GLOBE 

 Desk space starting from Frw.30,000 
 Are you start up with a social mission? Are you an individual or a team looking for workspace below market prices? 
 VISIT OUR SPACE 

 From our blog 

 23  Nov 

  Admin    0 Comments 

 Solar Entrepreneurship Program 
 Impact Hub Kigali, in collaboration with WWF, launched this week the Solar Entrepreneurship Program, The program, part of the Africa Energy Marketplace, focuses on identifying and supporting solar startups in Rwanda.   As part of this program, Impact Hub Kigali will be carrying out design thinking, business modelling and pitching sessions for university students during [ ] 

 Read More 

 06  Nov 

  Admin    0 Comments 

 Rwanda Build Accelerator Programme 
 Last month three tech solutions were launched at Impact Hub Kigali: YegoBox, Ifeza Florists and Hello Kigali. The three products were developed under a three-month Rwanda Build accelerator progamme hosted at Impact Hub s co-working space. The Rwanda Build programme aims to help create a better culture of software development in Rwanda. The solutions have been [ ] 

 Read More 

 06  Nov 

  Admin    0 Comments 

 Climathon Lusaka 
 How do we create innovative and sustainable solutions to climate-related challenges affecting communities and cities in Zambia and across Africa? This is what, climate advocates, entrepreneurs, economists, NGOs, digital innovators and environmental experts set out to work on during the Climathon at Bongo Hive Lusaka. Organized in collaboration between WWF, the City of Lusaka, Impact [ ] 

 Read More 

 our loving partners 

 Sign up for our community updates 

 Sign up Today 

 Contact Us 

 +250 (0) 788 544 203 
   connect@impacthub.rw 
   events@impacthub.rw 
  Plot 34 KN 41 st. Kiyovu-Kigali, Rwanda 

 Copyright   2015 Impact Hub Kigali. All Right Reserved. 

 Home About Us Our Services Contact Terms   Conditions
 WHOLE PROJECT MARK