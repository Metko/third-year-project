PROJECT NAME: Be My Eyes
PROJECT SOURCE: ICT-enabled social innovation initiatives
---------------------------------------
NEW PAGE: 
Bringing sight to blind and low vision people

Be My Eyes is a free app that connects blind and low vision people with sighted volunteers and company representatives for visual assistance through a live video call.
Download for iOS
Download for Android
NBC: 'Be My Eyes' app let's vision-impaired people crowdsource sight
The Guardian: From braille to Be My Eyes – there's a revolution happening in tech for the blind
CNN: App helps blind people see
BBC: Lend Me Your Eyes
Fast Company: This App Lets You Lend Your Eyes To The Blind When They Need To See Something
CBS News: 'Be My Eyes' app helps blind people do everyday things
Boston Globe: Apps, devices turn their attention to visually impaired
ABC News: Tech Breakthrough Helps The Blind See
Mashable: If you're blind, this app makes strangers your eyes
FOX News: New mobile app offers eyesight to visually-impaired people
TEDx: Be My Eyes Hans Jørgen Wiberg at TEDx Copenhagen
Hufington Post: Help The Blind Through Video Chat With This New App
How it works

Every day, sighted volunteers lend their eyes to solve tasks big and small to help blind and low vision people lead more independent lives.
Blind or low vision person requests assistance

As a blind or low vision person, whenever you need visual assistance, our volunteers are happy to help. Through the live video call, you and a volunteer can communicate directly and solve a problem. The volunteer will help guide which direction to point your camera, what to focus on or when to turn on your torch.
Sighted volunteer receives video call

As a sighted volunteer you can help just by installing the Be My Eyes app. A blind or a low vision user may need help with anything from checking expiry dates, distinguishing colors, reading instructions or navigating new surroundings.
1,247,929
Volunteers
79,051
Blind & low vision
150+
Countries
180+
Languages
Join the community!

Be My Eyes’ goal is to make the world more accessible to people who are blind or have a low level of vision. We couldn’t do this without the help and dedication of our community.
Download Be My Eyes
Download Be My Eyes

Get the Be My Eyes app and join our global community as a blind/low vision user or a sighted volunteer.
Android
iPhone
Translate Be My Eyes
Translate Be My Eyes

Help us make our app accessible for more people in their native language. Getting started is easy and straightforward.
Translate Be My Eyes app
Help us spread the word
Help us spread the word

Helping through Be My Eyes is easy. Invite your friends, family or colleagues.
Share on Facebook
Share on Twitter

Our story
Small acts of kindness with global impact.

    “It's my hope that by helping each other as an online community, Be My Eyes will make a big difference in the everyday lives of blind people all over the world.”

– Hans Jørgen Wiberg, Founder of Be My Eyes

Be My Eyes is a free mobile app with one main goal: to make the world more accessible for the blind and visually impaired. The app connects blind and visually impaired individuals with sighted volunteers from all over the world through a live video call.

Since our launch, on January 15th 2015, 79,051 blind and visually impaired has been assisted by more than 1,247,929 sighted volunteers. The app is used in 150+ countries and can be accessed in more than 180+ languages. This makes Be My Eyes the biggest global online community for the blind and visually impaired, as well as one of the largest micro-volunteering platforms in the world. Every day, volunteers lend their eyes to solve tasks big and small to help the blind and visually impaired lead more independent lives.

The Be My Eyes story started in Denmark in 2012 with Hans Jørgen Wiberg, a Danish furniture craftsman, who is visually impaired himself. Through his work at ‘The Danish Association of the Blind’, he recognized that blind or visually impaired people often needed a little assistance to solve everyday tasks. However, it wasn’t until a blind friend told him that he used video calls to connect with family and friends, who could help him with these tasks, that Hans Jorgen got the idea for Be My Eyes. He believed that the technology of video calls could be used to visually assist blind or visually impaired individuals, without them having to rely on friends and family, but using a network of volunteers.

In April 2012, Hans presented his idea at a Danish startup event, where he got connected with a team that was ready to make Be My Eyes a reality. On January 15th 2015, the Be My Eyes app was released for iOS, and within 24 hours the app had more than 10,000 users. Since the release of the iOS app, an Android version has been in high demand. The Android version was finally released on October 5th 2017.
Timeline

April 2012

Hans Jørgen Wiberg presented his app idea at Startup Weekend Aarhus, Denmark

October 4th 2013

Hans presented the Be My Eyes idea in an impassioned TEDx Talk

January 15th 2015

Launch of the Be My Eyes iOS app

May 2015

Winner of Best Social Entrepreneurial Tech Startup at Nordic Startup Awards

June 2015

Finalist at Index Awards

October 2015

Be My Eyes received 1 of 7 spots in the Singularity University Accelerator program, as the only Scandinavian company

October 2015

Be My Eyes was featured on Popular Science’s list of Best App of 2015

April 2016

Winner of the category ‘Share Resources’ at the Danish Design Award

October 2016

Winner of the category ‘Healthy Life’ at the European Youth Award

June 2017

Be My Eyes appeared as number 10 at the Disrupt 100 2017 list

September 2017

Winner of the Oslo Innovation Award

October 5th 2017

Launch of the Be My Eyes Android app
The Be My Eyes Team

Christian Erfurt

CEO

Hans Jørgen Wiberg 

Founder

Alexander Hauerslev Jensen

Community Director

Henrik Jachobsen 

Senior Software Engineer

André Schmidt

Software Developer

Cecilie Skou Andersen

Communications Officer

Julia Rignot

Communications Officer

Jozef Simo

Designer


Be My Eyes is a free mobile app designed to bring sight to the blind and visually impaired. With the press of a button, the app establishes a live video connection between blind and visually impaired users and sighted volunteers. Every day, volunteers are lending their eyes to solve challenges both big and small in the lives of the blind and visually impaired. With over half a million users across 150 countries, Be My Eyes has grown to become the largest online community for the blind and visually impaired. The app harnesses the power of generosity, technology and human connection to help blind and visually impaired individuals lead more independent lives.
 WHOLE PROJECT MARK
