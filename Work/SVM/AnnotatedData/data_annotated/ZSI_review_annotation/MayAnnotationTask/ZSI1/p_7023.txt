PROJECT NAME: bitmessage
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

Bitmessage Wiki
===============================

Bitmessage Wiki

 Main Page 

 From Bitmessage Wiki 

					Jump to:					 navigation , 					 search 

  Previous Version (Beta)

 0.6.1 

Aug 21, 2016 Changelog 

   Bitmessage 

 A remote code execution vulnerability has been spotted in use against some users running PyBitmessage v0.6.2. The cause was identified and a fix has been added and released as 0.6.3.2  here . If you run PyBitmessage via code, we highly recommend that you upgrade to 0.6.3.2. Alternatively you may downgrade to 0.6.1 which is unaffected. . 
 Bitmessage developer Peter  urda's Bitmessage addresses are to be considered compromised. 
 Bitmessage is a P2P communications  protocol  used to send encrypted messages to another person or to many subscribers. It is decentralized and trustless, meaning that you need-not inherently trust any entities like root certificate authorities. It uses strong authentication which means that the sender of a message cannot be spoofed, and it aims to hide "non-content" data, like the sender and receiver of messages, from passive eavesdroppers like those running warrantless wiretapping programs. If Bitmessage is completely new to you, you may wish to start by reading the  whitepaper .

 Download 
 An open source client is available for free under the very liberal  MIT license . For screenshots and a description of the client, see this CryptoJunky article:  "Setting Up And Using Bitmessage" .
   Download for Windows (32bit)   (64bit) 
   Download for OS X 
   Run the source code 

 Source code 
 You may view the Python  source code on Github . Bitmessage requires PyQt and OpenSSL. Step-by-step instructions on how to run the source code on Linux, Windows, or OSX is  available here .

 Contribute 
 Please follow the  contribution guidelines  when contributing code or translations.

 Security audit needed 
 Bitmessage is in need of an independent audit to verify its security. If you are a researcher capable of reviewing the source code, please  email  the lead developer. You will be helping to create a great privacy option for people everywhere!

 Forum 
  Visit or subscribe to the  Bitmessage subreddit . 
  A community-based forum for questions, feedback, and discussion is also available at  Bitmessage.org/forum . 

						Retrieved from " https://bitmessage.org/w/index.php?title=Main_Page oldid=47596 "					 

 Navigation menu 

 Personal tools 

 Log in   

 Namespaces 

 Page 
 Discussion 

 Variants 

 Views 

 Read 
 View source 
 View history 

 More 

 Search 

 Navigation 

 Main page 
 FAQ 
 Help with the client 
 Feature request list 
 Compiling instructions 
 API Reference 
 Recent changes 
 Random page 
 Help 

 Tools 

 What links here 
 Related changes 
 Special pages 
 Printable version 
 Permanent link 
 Page information 

  This page was last modified on 17 February 2018, at 03:50. 
 Content is available under  Creative Commons Attribution 3.0  unless otherwise noted. 

 Privacy policy 
 About Bitmessage Wiki 
 Disclaimers 

 Bitmessage is a decentralized, encrypted, peer-to-peer, trustless communications protocol that can be used by one person to send encrypted messages to another person, or to multiple subscribers. Bitmessage encrypts each users' message inbox using strong encryption and replicates it inside its P2P network mixing it with inboxes of other users in order to conceal user's identity, prevent eavesdropping and protect the network from any control. The Bitmessage communication protocol avoids sender-spoofing through strong authentication, and hides metadata from wiretapping systems.

In June 2013, the software experienced a surge of new adoptions after news broke of National Security Agency email surveillance activities.

Currently, the network processes several thousand private messages per day. 

Bitmessage is a decentralized, encrypted, peer-to-peer, trustless communications protocol that can be used by one person to send encrypted messages to another person, or to multiple subscribers. Bitmessage encrypts each users' message inbox using strong encryption and replicates it inside its P2P network mixing it with inboxes of other users in order to conceal user's identity, prevent eavesdropping and protect the network from any control. The Bitmessage communication protocol avoids sender-spoofing through strong authentication, and hides metadata from wiretapping systems.

In June 2013, the software experienced a surge of new adoptions after news broke of National Security Agency email surveillance activities.

Currently, the network processes several thousand private messages per day.
 WHOLE PROJECT MARK