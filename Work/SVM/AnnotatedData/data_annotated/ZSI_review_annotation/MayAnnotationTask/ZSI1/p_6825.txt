PROJECT NAME: Joomla
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

Joomla! The CMS Trusted By Millions for their Websites
===============================

Joomla! The CMS Trusted By Millions for their Websites

  Joomla!   

   Joomla!  Home

 Support  Joomla! 
 Contribute 
 The  Joomla!  Shop 
 Sponsorship 

 Try  Joomla! 
 Demo 
 Free Hosted Website 

 About  

 About  Joomla! 
 Core Features 
 The Project 
 Leadership 
 Open Source Matters 

 Download   Extend  

 Joomla! Downloads 
 Extensions Directory 
 Language Packages 
 Showcase Directory 
 Certification Program 

 News  

 Announcements 
 Blogs 
 Magazine 
 Joomla!  Connect 
 Mailing Lists 

 Community  

 Community Portal 
 Joomla!  Events 
 Trademark   Licensing 
 User Groups 
 Showcase Directory 
 Volunteers Portal 

 Support  

 Forum 
 Documentation 
 Issue Tracker 
 Resources Directory 
 Joomla!  Training 

 Developers  

 Developer Network 
 Documentation 
 Bug Squad 
 Security Centre 
 API Documentation 
 JoomlaCode 
 Joomla!  Framework 

 Search 

 Joomla! 

 Download 

 Demo 

 Home About Joomla! Core Features Joomla! 3.8 News Contribute Press Enquiries 

 The Flexible Platform Empowering Website Creators 
 Joomla! is an award-winning content management system (CMS), which enables you to build web sites and powerful online applications. 

   Download Joomla! 
 Download Joomla! 

 Download and Install Joomla! in your own server or development environment. 

 Download Joomla!  

 Use Joomla.com   
 Use Joomla! 

 Quickly launch a basic version of Joomla! on joomla.com. Easily upgrade for additional features. 

 Launch Joomla!  

   Joomla! Demo   
 Try Joomla! 

 Experiment with a full 90 day demo version of Joomla. Great for testing an extension or template. 

 Start Joomla! Demo  

 Joomla! Announcements 

					Joomla 3.8.5 Release				 

Joomla 3.8.5 is now available. This is a bug fix release for the 3.x series of Joomla fixing regressions which were reported after the 3.8.4 release. 

  Read more ...				 

					Joomla 3.8.4 Release				 

Joomla 3.8.4 is now available. This is a security release for the 3.x series of Joomla addressing four security vulnerabilities and including over 100 bug fixes and improvements. 

  Read more ...				 

 Why Joomla? 
 Hundreds of developers have been improving Joomla! since the first version was released in 2005. This immense effort has made Joomla! very popular, easy to use, stable and secure. 
 Joomla! has thousands of free extensions and templates allowing you to customize your site to fit your specific needs. 
 Learn more about Joomla! 3 

 Features   Benefits 

 Search Engine Friendly 

 Mobile Friendly 

 Unlimited Design 

 Multilingual 

 Flexible   Fully Extensible 

 Multi-User Permission Levels 

 See more features 

 Announcements 

 Development News 

						Joomla 4 compatibility layer in 3.8 
 January 26, 2018 by Allon Moritz 

						With the release of Joomla 3.8 a first compatibility layer for Joomla 4 was released. What does this layer contain? Why is such a layer needed?...						 

						Be smart, load but don t unload! 
 December 27, 2017 by Joomla Marketing Team 

						With every new major version release of our popular CMS, we have some points of attention to bring to our developers and users. With the upcoming...						 

 Community News 

						Events Department Coordinator Election Result 
 February 26, 2018 by Collective Joomla! Leadership 

						We are happy to announce that the election for the replacement for the Events Department Coordinator has been completed.
Read More...						 

						Events Department Coordinator - Nomination Announcement 
 February 24, 2018 by Collective Joomla! Leadership 

						The following individuals have confirmed their acceptance of their nomination as replacement for the role of Events Department Coordinator. Included...						 

 Team Reports 

						 OSM, Inc. - Full Board Meeting - March 01, 2018 
 March 5, 2018 by Luca Marzo 

						Open Source Matters, Inc.Full Board of Directors and Officers Meeting
Date: March 01, 2018
Time: 18:00 UTC
Total time of meeting: 1 hour 20...						 

						Strategic Meeting - Marketing   Communication Department 22 February 2018 
 February 23, 2018 by Sandra Decoux 

						Attendees

Sandra Decoux - Department Coordinator
Alison Meeks - Social Media Team Lead
Hans Van der Meer - Social Media Team Lead Assistant
Mike...						 

 Upcoming Events 

 JoomlaDay  Florida  (USA), March 2nd to 4th 
 JoomlaCamp  New York  (USA), March 11th 
 JoomlaDay  Netherlands , April 13th 
 JoomlaDay  Texas  (USA), April 28th 
 JandBeyond , Cologne (Germany), May 11th to 13th 
 JoomlaDay  France , May 18th 

 More events 

 Joomla! Means Community 
 Joomla! is the only major CMS that is built entirely by volunteers from all over the world. We have a strong community bond and all take pleasure in building something that has a large global impact. If you are interested in volunteering please head over to the volunteer portal. 
 Get involved 

 90   + Million Downloads 

 10   + Thousand Extensions  Templates 

 2   + Million Web Sites 

 1200   + Volunteers 

 550   + Thousand Lines of Code 

 74 Languages 

 Who is using Joomla? 
 Joomla! is trusted by some of the world s most well-known companies and much-loved brands as well as millions of websites throughout the world! 

 Lega Nazionale  per la difesa del Cane 
 More websites on the Showcase Directory 
 View more  

 Babyland  Amaliadas 
 More websites on the Showcase Directory 
 View more  

 Schrijversvakschool 
 More websites on the Showcase Directory 
 View more  

 Dorsa  Physical Therapy 
 More websites on the Showcase Directory 
 View more  

 You can see many great examples of quality Joomla! sites in the Showcase Directory 

 View more examples 

 Subscribe to our Newsletter 

 Joomla! on Twitter 
 Joomla! on Facebook 
 Joomla! on Google+ 
 Joomla! on YouTube 
 Joomla! on LinkedIn 
 Joomla! on Pinterest 
 Joomla! on Instagram 
 Joomla! on GitHub 

 Home 
 About 
 Community 
 Forum 
 Extensions 
 Resources 
 Docs 
 Developer 
 Shop 

 Accessibility Statement 
 Privacy Policy 
 Sponsor Joomla! with $5 
 Help Translate 
 Report an Issue 
 Log in 

  2005 - 2018  Open Source Matters, Inc.  All Rights Reserved. 

 Joomla!  Hosting by Rochen 

        We have detected that you are using an ad blocker. The Joomla! Project relies on revenue from these advertisements so please consider disabling the ad blocker for this domain.

Joomla! The CMS Trusted By Millions for their Websites
===============================

Joomla! The CMS Trusted By Millions for their Websites

  Joomla!   

   Joomla!  Home

 Support  Joomla! 
 Contribute 
 The  Joomla!  Shop 
 Sponsorship 

 Try  Joomla! 
 Demo 
 Free Hosted Website 

 About  

 About  Joomla! 
 Core Features 
 The Project 
 Leadership 
 Open Source Matters 

 Download   Extend  

 Joomla! Downloads 
 Extensions Directory 
 Language Packages 
 Showcase Directory 
 Certification Program 

 News  

 Announcements 
 Blogs 
 Magazine 
 Joomla!  Connect 
 Mailing Lists 

 Community  

 Community Portal 
 Joomla!  Events 
 Trademark   Licensing 
 User Groups 
 Showcase Directory 
 Volunteers Portal 

 Support  

 Forum 
 Documentation 
 Issue Tracker 
 Resources Directory 
 Joomla!  Training 

 Developers  

 Developer Network 
 Documentation 
 Bug Squad 
 Security Centre 
 API Documentation 
 JoomlaCode 
 Joomla!  Framework 

 Search 

 Joomla! 

 Download 

 Demo 

 Home About Joomla! Benefits   Features Joomla! 3.8 News Contribute Press Enquiries 

 The Flexible Platform Empowering Website Creators 
 Joomla! is an award-winning content management system (CMS), which enables you to build web sites and powerful online applications. 

   Download Joomla! 
 Download Joomla! 

 Download and Install Joomla! in your own server or development environment. 

 Download Joomla!  

 Use Joomla.com   
 Use Joomla! 

 Quickly launch a basic version of Joomla! on joomla.com. Easily upgrade for additional features. 

 Launch Joomla!  

   Joomla! Demo   
 Try Joomla! 

 Experiment with a full 90 day demo version of Joomla. Great for testing an extension or template. 

 Start Joomla! Demo  

 Joomla! Announcements 

					Joomla 3.8.6 Release				 

Joomla 3.8.6 is now available. This is a security fix release for the 3.x series of Joomla addressing one security vulnerability and including over 60 bug fixes and improvements. 

  Read more ...				 

					Joomla 3.8.5 Release				 

Joomla 3.8.5 is now available. This is a bug fix release for the 3.x series of Joomla fixing regressions which were reported after the 3.8.4 release. 

  Read more ...				 

 Why Joomla? 
 Hundreds of developers have been improving Joomla! since the first version was released in 2005. This immense effort has made Joomla! very popular, easy to use, stable and secure. 
 Joomla! has thousands of free extensions and templates allowing you to customize your site to fit your specific needs. 
 Learn more about Joomla! 3 

 Benefits   Features 

 Search Engine Friendly 

 Mobile Friendly 

 Unlimited Design 

 Multilingual 

 Flexible   Fully Extensible 

 Multi-User Permission Levels 

 See benefits   features 

 Announcements 

 Development News 

						Joomla 4 compatibility layer in 3.8 
 January 26, 2018 by Allon Moritz 

						With the release of Joomla 3.8 a first compatibility layer for Joomla 4 was released. What does this layer contain? Why is such a layer needed?...						 

						Be smart, load but don t unload! 
 December 27, 2017 by Joomla Marketing Team 

						With every new major version release of our popular CMS, we have some points of attention to bring to our developers and users. With the upcoming...						 

 Community News 

						Applications for the jet programme for J and Beyond 2018 are now open 
 March 9, 2018 by Collective Joomla! Leadership 

						The Joomla! Event Traveller Programme (JET) is pleased to announce that we will be taking applications from the worldwide Joomla community for the...						 

						Events Department Coordinator Election Result 
 February 26, 2018 by Collective Joomla! Leadership 

						We are happy to announce that the election for the replacement for the Events Department Coordinator has been completed.
Read More...						 

 Team Reports 

						Team Leader Election 
 March 20, 2018 by David Aswani 

						Event team had an election for the team leader on 16th March 2016. David Opati Aswani was elected as new Event Team Leader taking over from Carlos...						 

						Survey Sent 
 March 20, 2018 by Karen Dunne 

						The survey was sent out to the contacts who have approved listings on the JRD.
Agenda

Survey Status and Results
Submission review status
Turn...						 

 Upcoming Events 

 JoomlaDay  Netherlands , April 13th 
 JoomlaDay  Texas  (USA), April 28th 
 JandBeyond , Cologne (Germany), May 11th to 13th 
 JoomlaDay  France , May 18th 

 More events 

 Joomla! Means Community 
 Joomla! is the only major CMS that is built entirely by volunteers from all over the world. We have a strong community bond and all take pleasure in building something that has a large global impact. If you are interested in volunteering please head over to the volunteer portal. 
 Get involved 

 91   + Million Downloads 

 10   + Thousand Extensions  Templates 

 2   + Million Web Sites 

 1200   + Volunteers 

 550   + Thousand Lines of Code 

 74 Languages 

 Who is using Joomla? 
 Joomla! is trusted by some of the world s most well-known companies and much-loved brands as well as millions of websites throughout the world! 

 JoomlaDay  France 
 More websites on the Showcase Directory 
 View more  

 Twinson 
 More websites on the Showcase Directory 
 View more  

 MykPaczkal 
 More websites on the Showcase Directory 
 View more  

 Maloto  Picture Framing 
 More websites on the Showcase Directory 
 View more  

 You can see many great examples of quality Joomla! sites in the Showcase Directory 

 View more examples 

 Subscribe to our Newsletter 

 Joomla! on Twitter 
 Joomla! on Facebook 
 Joomla! on Google+ 
 Joomla! on YouTube 
 Joomla! on LinkedIn 
 Joomla! on Pinterest 
 Joomla! on Instagram 
 Joomla! on GitHub 

 Home 
 About 
 Community 
 Forum 
 Extensions 
 Resources 
 Docs 
 Developer 
 Shop 

 Accessibility Statement 
 Privacy Policy 
 Sponsor Joomla! with $5 
 Help Translate 
 Report an Issue 
 Log in 

  2005 - 2018  Open Source Matters, Inc.  All Rights Reserved. 

 Joomla!  Hosting by Rochen 

        We have detected that you are using an ad blocker. The Joomla! Project relies on revenue from these advertisements so please consider disabling the ad blocker for this domain.

Joomla! - Content Management System to build websites   apps
===============================

Joomla! - Content Management System to build websites   apps

  Joomla!   

   Joomla!  Home

 Support  Joomla! 
 Contribute 
 The  Joomla!  Shop 
 Sponsorship 

 Try  Joomla! 
 Demo 
 Free Hosted Website 

 About  

 About  Joomla! 
 Core Features 
 The Project 
 Leadership 
 Open Source Matters 

 Download   Extend  

 Joomla! Downloads 
 Extensions Directory 
 Language Packages 
 Showcase Directory 
 Certification Program 

 News  

 Announcements 
 Blogs 
 Magazine 
 Joomla!  Connect 
 Mailing Lists 

 Community  

 Community Portal 
 Joomla!  Events 
 Trademark   Licensing 
 User Groups 
 Showcase Directory 
 Volunteers Portal 

 Support  

 Forum 
 Documentation 
 Issue Tracker 
 Resources Directory 
 Joomla!  Training 

 Developers  

 Developer Network 
 Documentation 
 Bug Squad 
 Security Centre 
 API Documentation 
 JoomlaCode 
 Joomla!  Framework 

 Search 

 Joomla! 

 Download 

 Demo 

 Home About Joomla! Benefits   Features Joomla! 3.8 News Contribute Press Enquiries 

  About Joomla!  

 Joomla! is a free and open-source content management system (CMS) for publishing web content. Over the years Joomla! has won  several awards . It is built on a model view controller web application framework that can be used independently of the CMS that allows you to build powerful online applications. 
 Joomla! is one of the most popular website softwares, thanks to its global community of developers and volunteers, who make sure the platform is user friendly, extendable, multilingual, accessible, responsive, search engine optimized and so much more. 
 What's a content management system (CMS)? 
 The definition of a CMS is an application (web-based), that provides capabilities for multiple users with different permission levels to manage (all or a section of) content, data or information of a website project, or intranet application. Managing content refers to creating, editing, archiving, publishing, collaborating on, reporting, distributing website content, data and information. 
 Joomla! in the real (online) world 
 Joomla! is used all over the world to power millions of websites of all shapes and sizes. Discover examples of companies using Joomla! in the official  Joomla! Showcase Directory . 
 Joomla! can be used for: 

 Corporate websites or portals, intranets and extranets 
 Small business websites 
 Online magazines, newspapers, and publications 
 E-commerce and online reservations 
 Government, non-profit and organisational websites 
 Community-based, school and church websites or portals 
 Personal or family homepages ... 

 Being a web agency, how can Joomla! help me? 
 If you re an agency who develops websites for your customers, Joomla! is the perfect tool for you (have a look at our benefits and  features ). Designed to be easy to install and set up, even if you're not an advanced user. With a short learning curve (we provide  free video training  as well) you ll be able to quickly build sites for your clients. Then, with a minimal amount of instruction, you can empower your clients to easily manage their own sites themselves.  If your clients need specialised functionality, Joomla! is highly extensible and thousands of extensions (most for free under the  GPL license ) are available in the  Joomla! Extensions Directory . 
 As a developer, can I use Joomla! in a more advanced way? 
 Some companies and organisations have requirements that go beyond what is available in the Joomla! Core package. In those cases, Joomla's powerful application framework makes it easy for developers to create sophisticated add-ons that extend the power of Joomla into virtually unlimited directions. 
 The core  Joomla! Framework  enables developers to quickly and easily build: 

 Inventory control systems 
 Data reporting tools 
 Application bridges 
 Custom product catalogs 
 Integrated e-commerce systems 
 Complex business directories 
 Reservation systems 
 Communication tools 

 Since Joomla! is based on PHP and MySQL, you're building powerful applications on an open platform anyone can use, share, and support.  To find out more information on leveraging the Joomla! Framework, visit the  Joomla! Framework  site. 
 Joomla! seems the right solution for me. How do I get started? 
 Joomla! is free, open, and available to anyone under the  GPL license . Read  Getting Started with Joomla!  to find out the basics. 

 If you're ready to install Joomla! by yourself: 
 Download the latest version of Joomla!   You can find it on the  Joomla! Downloads Portal , you'll be up and running in no time. And if you need any help, visit our official  forums  and  documentation . You can also watch our  training videos . Download Joomla! now  

 Joomla! provides also two other ways to get started without having to install it: 
 Free website on Joomla.com This is the easiest way to start your first Joomla! website immediately. You can launch a totally free website in seconds and start publishing your content online immediately.  Try the Joomla.com service now  
 Self-hosted Joomla! website You can also start a Joomla! website on a web hosting account. Most hosts provide a Joomla! auto-installer or you can download Joomla! and install it yourself. On a self-hosted Joomla! website you can extend the core Joomla! functionalities with third-party extensions and templates and built anything from a simple website to a complex system. You can test the self-hosted Joomla! experience through the 90-day free Joomla! demo.  Try a self-hosted Joomla! now  

 About Joomla! 

 What is Joomla? Getting Started Create and Share Get Involved About The Joomla! Project Mission, Vision   Values Code of Conduct Board of Directors Honor Roll Partners Extension Partners Media   Press Conditional Use Logos Joomla! Books Joomla! In Your Language Joomla! Mailing Lists Joomla! Newsletter Joomla! RSS News Feeds Technical Requirements 

 Contributing to Joomla! 

 Sponsorships Global Sponsors Volunteers Portal 

 Newsletter 
 Name    Email   
 Receive   Text HTML 

 Joomla! on Twitter 
 Joomla! on Facebook 
 Joomla! on Google+ 
 Joomla! on YouTube 
 Joomla! on LinkedIn 
 Joomla! on Pinterest 
 Joomla! on Instagram 
 Joomla! on GitHub 

 Home 
 About 
 Community 
 Forum 
 Extensions 
 Resources 
 Docs 
 Developer 
 Shop 

 Accessibility Statement 
 Privacy Policy 
 Sponsor Joomla! with $5 
 Help Translate 
 Report an Issue 
 Log in 

  2005 - 2018  Open Source Matters, Inc.  All Rights Reserved. 

 Joomla!  Hosting by Rochen 

        We have detected that you are using an ad blocker. The Joomla! Project relies on revenue from these advertisements so please consider disabling the ad blocker for this domain. 

 Joomla is an award-winning content management system (CMS), which enables you to build Web sites and powerful online applications. Many aspects, including its ease-of-use and extensibility, have made Joomla the most popular Web site software available. Best of all, Joomla is an open source solution that is freely available to everyone.
As of February 2014, Joomla has been downloaded over 50 million times. Over 7,700 free and commercial extensions are available from the official Joomla! Extension Directory, and more are available from other sources. It is estimated to be the second most used content management system (CMS) on the Internet after WordPress. 
Joomla was the result of a fork of Mambo on August 17, 2005. 

Joomla is an award-winning content management system (CMS), which enables you to build Web sites and powerful online applications. Many aspects, including its ease-of-use and extensibility, have made Joomla the most popular Web site software available. Best of all, Joomla is an open source solution that is freely available to everyone.
As of February 2014, Joomla has been downloaded over 50 million times. Over 7,700 free and commercial extensions are available from the official Joomla! Extension Directory, and more are available from other sources. It is estimated to be the second most used content management system (CMS) on the Internet after WordPress. 
Joomla was the result of a fork of Mambo on August 17, 2005.
 WHOLE PROJECT MARK