PROJECT NAME: WikiEducator
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

Welcome to WikiEducator


About     Using WikiEducator     FAQs    OERu    OER Foundation     OERu planning


We're turning the digital divide into digital dividends using free content and open networks. We hope you will join us.
The purpose of WikiEducator
The WikiEducator is an evolving community intended for the collaborative:

    planning of education projects linked with the development of free content
    development of free content on Wikieducator for e-learning
    work on building open education resources (OERs) on how to create OERs
    networking on funding proposals developed as free content

Join us today ~ You'll be glad you did!
Please join us in developing free and open educational content for the world!

Get started today and register now for free wiki training.
WE15.jpg

WikiEducator is a dynamic and exciting community of educators who believe passionately that learning materials should be free and open to all. Listen to Desmond Tutu on freedom in education and check out the exciting roles in our community.


Featured WikiEducator: Anne Asual

WikiEducator honours its stellar members and innovative community in different ways. We are proud of our fabulous members from all over the world and the contributions they have made to this project. The User Page Expo Award is a monthly feature maintained by Dr. Ramesh Sharma and Dr. Nellie Deutsch, to honour innovative user pages and exemplary members.

Please meet the UPE winner for November, 2015, Anne Asual!

Get Involved ~ There are so many ways...

    create a WikiEducator account & get free wiki skills training (i.e., to edit wiki content) - register here for the next free Wiki Training with Facilitator Support
    help out a specific Project, create your own, or help out on our "things to do" list.
    join our discussion groups and mailing list
    join a Cluster of Interest
    keep your Country or Country News page current
    actively seek donations or contributions of learning materials - from active or retired educators
    build your network of contacts and relationships in the WikiEducator community
    tell the world - and your community about WikiEducator
    Become a WikiAmbassador ~ it's easy!!
    join our Google+ Community
    Connect to us through our WikiEducator Facebook Group page


Talk to us ~ and share your interests

We will connect you with a WikiNeighbour, so you'll feel right at home, right away. Contact Us Now!
	
OERu Partners Meeting October 12 - 13
OERu-Logo-acronym-bottom-green.png
Representatives from the OERu network will meet in Toronto on 12 and 13 October for the 6th international partner's meeting. The OERu Council of Chief Executive Officers will meet on 16 October 2017 prior to the ICDE World Conference on Online Learning. Past meetings...

Tell me more!
Featured OERu Partner


Tesu-logo-s.png
Thomas Edison State University

Thomas Edison State College provides flexible, high-quality, collegiate learning opportunities for self-directed adults, wherever they live or work.

Read more ...
In the news

HBMSU

Hamdan Bin Mohammed Smart University (HBMSU) has become the first OERu partner in the Middle East. Dr Mansoor Al Awar, Chancellor of the University, notes that it is a great honour for HBMSU to contribute to OERu in fulfilling its humanitarian mission to build a parallel learning universe to provide first-class, accessible and affordable education.
 
The Open University

Open Education pioneer The Open University in the United Kingdom has become an OERu partner.


News/Events Archive
Our community values...

    diversity, freedom, innovation, transparency, equality, inclusivity, empowerment, human dignity, wellbeing and sustainability.
WikiEducator:About
Contents

    1 Introduction
    2 Strategy
        2.1 Phase 1: Establishing foundations (May 2006 - Dec 2007)
        2.2 Phase 2: Scaling up free content development (Jan 2008 - Dec 2008)
        2.3 Phase 3: Sustainable implementation of free content in education (Jan 2009 - )
    3 Quick facts and highlights
    4 Record of workshops held

Introduction
Sir John Daniel, Founding Patron of WikiEducator
WikiEducator is a community project working collaboratively with the Free Culture Movement towards incremental development of open educational resources. Driven by the learning for development agenda we focus on:

    building capacity in the use of Mediawiki and related free software technologies for mass-collaboration in the authoring of free content
    developing free content for use in schools, polytechnics, universities, vocational education institutions and informal education settings
    facilitating the establishment of community networks and collaboration with existing free content initiatives in education
    fostering new technologies that will widen access, improve quality and reduce the cost associated with providing education, primarily through the use of free content
    supporting collaborative development of OERu courses to widen access to tertiary education

Sir John Daniel, President and Chief Executive Officer of the Commonwealth of Learning was the founding patron of WikiEducator. The project has adopted a community governance model which is coordinated by WikiEducator's Open Community Council, building on the work of the Interim International Advisory Board. Ambassadors for WikiEducator promote the project around the globe, and our technology roadmap helps us make the future happen.

WikiEducator's technical infrastructure was supported by a financial contribution by the Commonwealth of Learning (COL) to the Open Education Resource Foundation an independent international non-profit headquartered at Otago Polytechnic New Zealand.
Strategy

WikiEducator aims to build a thriving and sustainable global community dedicated to the design, development and delivery of free content for learning in realisation of a free version of the education curriculum by 2015.
Building the WikiEducator community together
In realisation of this vision, the WikiEducator strategy will focus on:

    Building the capacity and skills of the community to engage meaningfully in the mass-collaboration required for the design and development of high quality learning resources, for example capacity building workshops.
    Developing free content and knowledge to support the development of open communities and free content developers so that resources can be reused in multiple contexts, for example the Newbie Tutorials.
    Ensuring smart connections through appropriate networks, ecosystems and the smart implementation of free software solutions to fill the gaps between existing mainstream technologies and the unique requirements of asynchronous learning thus widening the reach and access of free content in the developing world. This is achieved through community nodes on WikiEducator (e.g. VUSSC, FLOSS4Edu), technology think tank meetings (e.g. Tectonic Shift Think Tank) and fostering strategic relationships with the freedom culture (e.g. Wikiversity and the WikiMedia Foundation).

The strategy is divided into three distinct phases.
Phase 1: Establishing foundations (May 2006 - Dec 2007)

This phase involves setting up the technologies and processes to facilitate community development for free content. This is achieved by:

    building capacity to participate;
    developing free content on how to develop free content;
    through a process of self organisation, providing the community the freedom to determine the kinds of projects, structures and communication mechanisms for WikiEducator;
    establishing a democratic governance model from the community for WikiEducator;
    collaborative and democratic development of community policies that support and promote the values of the WikiEducator community;
    strategic relationships networking to ensure the right connections for a sustainable community.

Phase 2: Scaling up free content development (Jan 2008 - Dec 2008)

The prime purpose of Phase 2 is to scale up the rate and quality of free content development building on the foundations established during Phase 1. Phase 2 will be realised through ongoing community development with special emphasis on scalable capacity development, including for example:

    The Learning4Content project which aims to provide free training for educators in return for a lesson of free content on WikiEducator
    Establishing clear pathways for a wide range of skilled individuals to contribute to WikiEducator projects by:
        Developing a one stop "portal" which clearly communicates which projects need help and support;
        Differentiating and communicating the skills requirements for individual projects, for example learning design, content design, multimedia and visual design, linguistic design, research and technical design;
    Policy and practice guidelines for the design and development of learning content drawing on the research and experience of distance education, for example the Learning Design project.
    Establish quality assurance mechanisms appropriate for educational content, for example implementation and refinement of the FlaggedRevs initiative.
    Development and implementation of a fund raising strategy for the WikiEducator community.
    Seeding the establishment of regional and national chapters of mainstream WikiEducator initiatives.
    Ongoing refinements to existing technologies and the search for free software solutions to scale up the rate of free content production.

Phase 3: Sustainable implementation of free content in education (Jan 2009 - )

The purpose of this phase is to prioritise activities which implement free content into mainstream educational activities. The success of the free content movement in education will be measured when real students enroll in real programmes using free content.

The flagship project will be the Commonwealth Computer Navigator's Certificate to collect evidence on the merits and value proposition of free content in real educational contexts.
Quick facts and highlights
Date 	Selected milestones 	No. Registered users
Feb 2006 	Wayne registers the domain names for:

    wikieducator.org
    wikieducator.net
    wikieducator.com

	Nil
Feb 2006 	Installed the Mediawiki software on a desktop machine at the University of Auckland, New Zealand. WikiEducator was originally conceived as an experimental wiki to host content developed by the eXe community and to explore the potential for collabrative development of eLearning content the wiki way. 	10
April 2006 	WikiEducator relocated to a hosted server. Hosting services provided by Erik Möeller and funded by COL 	12
August 2006 	Strategic plan for WikiEducator submitted under the eLearning for Education Sector Development initiative as part of COL's three-year plan, 2006 - 2009. 	17
October 2006 	WikiEducator brochure uploaded to the site. 	80
November 2006 	FLOSS4Edu project launched in Kenya 	142
January 2007 	WikiEducator mailing list created by Brent Simpson 	285
February 2007 	

    Newbie Tutorials completed.
    WikiEducator's first online training session

	491
April 2007 	

    Tectonic Shift Think Tank hosted at COL in Vancouver
    Strategic plan posted on the WikiEducator site
    Interim International Advisory Group established

	803
May 2007 	WikiEducator achieves 1,000 users. 	1,000
October 2007 	William and Flora Hewlett Foundation announce a $100,000 grant for the Learning4Content project. 	1,400
December 2007 	

    Stephen Downes names WikiEducator the best educational wiki.
    Wikis go printable: The Wikimedia Foundation and the Commonwealth of learning announce the partnership to collaborate on wiki ==> pdf technology.
    WikiEducator achieves 2,000 users

	2,165
January 2008 	WikiEducator launches the Learning4Content project 	2,515
May 2008 	WikiEducator is the inaugural recipient of the 2008 Merlot Africa Network and eLearning Africa (MANeLA) award in the Free Software for OER Content Authoring category. 	3,892
June 2008 	

    Open Community Governance Policy draft completed
    Dates for the first WikiEducator Council election announced
    WikiEducator achieves 4,000 users

	4,221
August 2008 	OER Handbook for Educators goes to print 	5,418
September 2008 	Results of the first WikiEducator community elections announced. 	5,941



Record of workshops held

    L4C Online participation by country

    Face-to-Face participation by country




 WikiEducator is a community project working collaboratively with the Free Culture Movement towards a free version of the education curriculum by 2015. Driven by the learning for development agenda WikiEducator focuses on: 
- developing free content for use in schools, polytechnics, universities, vocational education institutions and informal education settings;
- building capacity in the use of Mediawiki and related free software technologies for mass-collaboration in the authoring of free content;
- facilitating the establishment of community networks and collaboration with existing free content initiatives in education;
- fostering new technologies that will widen access, improve quality and reduce the cost associated with providing education, primarily through the use of free content. 

WikiEducator is a community project working collaboratively with the Free Culture Movement towards a free version of the education curriculum by 2015. Driven by the learning for development agenda WikiEducator focuses on: 
- developing free content for use in schools, polytechnics, universities, vocational education institutions and informal education settings;
- building capacity in the use of Mediawiki and related free software technologies for mass-collaboration in the authoring of free content;
- facilitating the establishment of community networks and collaboration with existing free content initiatives in education;
- fostering new technologies that will widen access, improve quality and reduce the cost associated with providing education, primarily through the use of free content.
 WHOLE PROJECT MARK
