PROJECT NAME: Sharing Backyards
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 IMAGINE A WORLD where everyone has access to healthy, local, organic food. The goal of the Sharing Backyards program is to make that a reality by giving anyone who wants to grow food the yard space to do it.

Sharing Backyards encourages urban gardening by connecting those who have the space to garden with those who would like to garden but don’t have the yard space.

It connects neighbours of all kinds in an atmosphere of trust – doing something the beautifies the homewoner’s yard and provides fresh food. Garden partners share the cost of gardening supplies and the abundance of the crops. 

IMAGINE A WORLD where everyone has access to healthy, local, organic food. The goal of the Sharing Backyards program is to make that a reality by giving anyone who wants to grow food the yard space to do it.

Sharing Backyards encourages urban gardening by connecting those who have the space to garden with those who would like to garden but don’t have the yard space.

It connects neighbours of all kinds in an atmosphere of trust – doing something the beautifies the homewoner’s yard and provides fresh food. Garden partners share the cost of gardening supplies and the abundance of the crops.
 WHOLE PROJECT MARK