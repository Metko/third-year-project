PROJECT NAME: DNAdigest
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

DNAdigest.org -
===============================

DNAdigest.org -

 About 

 Vision and mission 
 Who we are 
 How to get involved 
 Our sponsors 
 Contact us 

 Blog 
 Interviews 
 Events 
 Shop 

 Products 
 My Account 
 Cart 
 Checkout 

 DNAdigest.org  Blog 

 Interviews 

 December 12, 2017 

 DNAdigest interviews Laurence Woollard from On The Pulse Consultancy 

 Could you please introduce yourself and your current role? My name is Laurence Woollard, I am the founder and director of On The Pulse Consultancy. We are a relatively new start-up that provides independent... 

 Events 

 November 3, 2017 

 Cambridge Rare Disease Network Summit: rare diseases are rare but rare disease patients are numerous 

 On the 23rd of October, Robinson College of the University of Cambridge hosted the 3rd annual CRDN summit. The summit brought together patient groups, healthcare professionals, academics, researchers, biotech companies, and the pharmaceutical industry... 

 Interviews 

 October 24, 2017 

 DNAdigest interviews Tim Guilliams from Healx 

 Tim Guilliams is the CEO of Healx   social enterprise with the focus on drug repurposing for rare diseases. Tim will be speaking at the BioData World Congress in Hinxton on 2-3 November 2017.... 

 Interviews 

 October 11, 2017 

 DNAdigest interviews Natalie Banner from Wellcome Trust 

 Dr Natalie Banner is a Policy Adviser at the Wellcome Trust; she will be speaking at the BioData World Congress in Hinxton 2-3 November 2017.   Could you please introduce yourself and tell us... 

 Events 

 October 11, 2017 

 3rd DNAdigest guest talk: CRUK s vision on Research Data Management and Sharing 

 20 October 2017, Cambridge Our third invited speaker for 2017 is Dr. Ruchi Chauhan from CRUK, with the talk:  CRUK s vision on Research Data Management and Sharing  Ruchi will talk about the importance of... 

 Guest post 

 October 3, 2017 

 Data Sharing 101   a brief introduction for everyone 

 Written by Spencer Gibson, PhD, Research Associate at the University of Leicester. The DataSharing 101 site aims to be a launching pad for anyone interested in sharing data for biomedical purposes. The site started... 

 Miscellaneous 

 August 31, 2017 

 Perfect discount on  Perfect DNA : 50% this September 

 We are happy to announce that on 1-30 September 2017 we will be offering a 50% discount on the book  Perfect DNA  by Manuel Corpas. Get your copy of Perfect DNA What if a... 

 Interviews 

 August 17, 2017 

 DNAdigest interviews Dipak Kalra from the European Institute for Innovation through Health Data 

 Dipak Kalra will be speaking at the BioData World Congress in Hinxton 2-3 November 2017. Could you please introduce yourself and tell us about your main roles and activities? My name is Dipak Kalra... 

 Guest post 

 June 26, 2017 

 DNACoin: guest post by DNA\/ID 

 The post is originally published here and is reproduced with permission.  I have spent 15 years in academia and industry, created a startup, and am running a second one, dedicated to the progress of... 

 Events 

 June 21, 2017 

 2nd guest talk in Cambridge: Amy Tang from EBI   Data sharing   Open Science 

 It gives us great pleasure to invite you to our 2nd Guest Talk in Cambridge on the 7th of July at 7 pm at the Red Bull pub on Barton road. Our guest speaker... 

 Guest post 

 June 13, 2017 

 Cancer Moonshot and the future of Precision Medicine 

 This guest post is by Emily Walsh, community outreach director at the Mesothelioma Cancer Alliance. The Mesothelioma Cancer Alliance, while focused on raising awareness for mesothelioma and bringing about the ban of asbestos, is working to... 

 Page 1 of 27 1 2 3 4 5 ... 10 20 ... Last  

 Follow: 

 SIGN UP FOR OUR NEWSLETTER     Latest news   

 DNAdigest interviews Laurence Woollard from On The Pulse Consultancy 
 December 12, 2017 

 Cambridge Rare Disease Network Summit: rare diseases are rare but rare disease patients are numerous 
 November 3, 2017 

 DNAdigest interviews Tim Guilliams from Healx 
 October 24, 2017 

 DNAdigest interviews Natalie Banner from Wellcome Trust 
 October 11, 2017 

 3rd DNAdigest guest talk: CRUK s vision on Research Data Management and Sharing 
 October 11, 2017 

 Data Sharing 101   a brief introduction for everyone 
 October 3, 2017 

   OUR SHOP 

   The DNAdigest Mug 

 7.99 

   Perfect DNA: a book by Manuel Corpas 

 8.00 

   Valuable Values T-shirt 

 18.99 

   Code For The Heart T-shirt 

 24.99 
 Our Tweets 

 DNAdigest.org 
 @DNADigest 

 OBF_BOSC

The  #GCCBOSC  abstracts are pouring in--we can't wait to read all of them! It's going to be a great meeting!

  BOSC ( @OBF_BOSC ) March 16, 2018 

 10:57 am   March 17, 2018 

 BOSC 
 @OBF_BOSC 

 The  #GCCBOSC  abstracts are pouring in--we can't wait to read all of them! It's going to be a great meeting! 

 7:53 pm   March 16, 2018     Retweeted by DNAdigest.org 

 DNAdigest.org 
 @DNADigest 

 camraredisease

 #Cambridge s  #RareDisease  champion  #StephenHawking  has died. He lived life to the full with  #motorneurone  disease and used  #technology  to overcome barriers to independence. We thank him for his talk at our 2015 summit. A true  #RareDisease  revolutionary  

 3:07 pm   March 14, 2018 

 Cam Rare Disease 
 @camraredisease 

 #Cambridge s  #RareDisease  champion  #StephenHawking  has died. He lived life to the full with  #motorneurone  disease and used  #technology  to overcome barriers to independence. We thank him for his talk at our 2015 summit. A true  #RareDisease  revolutionary  twitter.com/visualsumo/sta 

 2:40 pm   March 14, 2018     Retweeted by DNAdigest.org 

 Helen Parkinson 
 @DrP_stuff 

 30 Y1 kids extracted DNA this morning. Amazing listening and understanding from 5 and 6 year old kids. Thanks to  @emblebi  and   @wellcomegenome  for supporting science in schools. 

 10:39 pm   March 12, 2018     Retweeted by DNAdigest.org 

 DNAdigest.org 
 @DNADigest 

 OBF_BOSC

BOSC's theme this year is interoperability! One week left to submit your abstract on interoperability or any open* bioinformatics topic!  buff.ly/2FvqVkY 

  BOSC ( @OBF_BOSC ) March 9, 2018 

 10:57 am   March 12, 2018 

 BOSC 
 @OBF_BOSC 

 BOSC's theme this year is interoperability! One week left to submit your abstract on interoperability or any open* bioinformatics topic!  open-bio.org/wiki/BOSC_2018 

 2:16 pm   March 11, 2018     Retweeted by DNAdigest.org 

 Melissa Haendel 
 @ontowonka 

 #WomeninBioinformatics  I'm honored to be nominated for the Ben Franklin Award for Open Access in the Life Sciences, RE open exchange of ideas   contributions. Only 1 woman has ever won, please help me recognize all  #WomenInSTEM .  Anyone can vote   bioinformatics.org/franklin/ 

 2:16 pm   March 11, 2018     Retweeted by DNAdigest.org 

 Follow @dnadigest Upcoming Events 

 More events coming in the future. 

 April 24, 2025 @ 8:00 am  -  5:00 pm   

 View All Events 

 DNAdigest is an independent charity (reg. #1154095) working to promote and enable easier, more efficient sharing of genomic data for research. Thank you for visiting us. 

 The objective of DNAdigest is to provide a simple, secure and effective mechanism for sharing genomics data for research without compromising the data privacy of the individual contributors. DNAdigest is a non-profit organisation founded and located in Cambridge, UK, by a group of individuals from diverse backgrounds who all want to see genomics used to its full potential to aid medical research. 

The objective of DNAdigest is to provide a simple, secure and effective mechanism for sharing genomics data for research without compromising the data privacy of the individual contributors. DNAdigest is a non-profit organisation founded and located in Cambridge, UK, by a group of individuals from diverse backgrounds who all want to see genomics used to its full potential to aid medical research.
 WHOLE PROJECT MARK