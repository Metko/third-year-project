PROJECT NAME: Khan academy
PROJECT SOURCE: ICT-enabled social innovation: cases
---------------------------------------
NEW PAGE: 

Khan Academy | Free Online Courses, Lessons   Practice
===============================

Khan Academy | Free Online Courses, Lessons   Practice

        Learn for free about math, art, computer programming, economics, physics, chemistry, biology, medicine, finance, history, and more. Khan Academy is a nonprofit with the mission of providing a free, world-class education for anyone, anywhere.

 If you're seeing this message, it means we're having trouble loading external resources on our website. 

            If you're behind a web filter, please make sure that the domains  *.kastatic.org  and  *.kasandbox.org  are unblocked.

 Main content 


Our mission is to provide a free, world‑class education for anyone, anywhere.

    About
    Impact
    Team
    Interns
    Content Specialists
    Leadership
    Supporters
    Volunteer
    Blog
    Donate

A personalized learning resource for all ages

Khan Academy offers practice exercises, instructional videos, and a personalized learning dashboard that empower learners to study at their own pace in and outside of the classroom. We tackle math, science, computer programming, history, art history, economics, and more. Our math missions guide learners from kindergarten to calculus using state-of-the-art, adaptive technology that identifies strengths and learning gaps. We've also partnered with institutions like NASA, The Museum of Modern Art, The California Academy of Sciences, and MIT to offer specialized content.
Free tools for parents and teachers

We’re working hard to ensure that Khan Academy empowers coaches of all kinds to better understand what their children or students are up to and how best to help them. See at a glance whether a child or student is struggling or if she hit a streak and is now far ahead of the class. Our coach dashboard provides a summary of class performance as a whole as well as detailed student profiles.
Learn more about coaching
You’re joining a global classroom

Millions of students from all over the world, each with their own unique story, learn at their own pace on Khan Academy every single day. Our resources are being translated into more than 36 languages in addition to the Spanish, French, and Brazilian Portuguese versions of our site.
Learn more about their stories

        Dear Khan Academy, I greatly appreciate your videos! Not only did I pass Calculus last year with a 96% , but I also discovered my love for the subject – something I had never experienced before truly understanding the mathematical content and the uses of it.

    Matt

From humble beginnings to a world-class team

What started as one man tutoring his cousin has grown into a more than 150-person organization. We’re a diverse team that has come together to work on an audacious mission: to provide a free world-class education for anyone, anywhere. We are developers, teachers, designers, strategists, scientists, and content specialists who passionately believe in inspiring the world to learn. A few great people can make a big difference.
Learn more about our team
For free. For everyone. Forever.

No ads, no subscriptions. We are a not‑for‑profit because we believe in a free, world-class education for anyone, anywhere. We rely on our community of thousands of volunteers and donors. Learn more about getting involved today.

Based on multiple studies
People who study on Khan Academy achieve greater-than-expected learning outcomes.
Learning outcomes exceed typical growth.
FSG
Students who complete 60% of their grade-level math on Khan Academy experience 1.8 times their expected growth on the NWEA MAP Test, a popular assessment test.

FSG conducted a study of a statewide pilot of Khan Academy in Idaho with 173 teachers and 10,500 students during the 2013-14 school year.
Full study
STANFORD RESEARCH INSTITUTE
Student use of Khan Academy correlates with score gains on standardized achievement tests.

Stanford Research Institute conducted a two-year study with 20 public, private, and charter schools; 70 teachers; and 2000 students during the 2012-13 school year.
Full study
Graph detailing the higher rate of expected growth that students who completed 40% or 60% of grade level math on KA have achieved.
“I love that Khan Academy shows you your errors. It really helped me in understanding words in context; it really broke it down for me. With the PSAT/NMSQT score I doubted myself, but my teachers showed me with Khan Academy I could do it. If it wasn’t for Khan Academy, I wouldn’t have access to SAT practice at all. It’s a godsend!”
—Tatiana, a senior at Oak Ridge High School in Orlando, Florida
Cartoon of Khan website on laptop with books and apple.
Students who prepare using Official SAT Practice see substantial improvement in their SAT scores.
OFFICIAL SAT PRACTICE
20 hours of practice is associated with a 115-point average score increase from the PSAT/NMSQT to the SAT, nearly double the average gain of students who do not practice on Official SAT Practice.

In 2017, Khan Academy and the College Board, the maker of the SAT, analyzed gains between the PSAT/NMSQT and the SAT for approximately 250,000 students and found a positive relationship between the use of Official SAT Practice on Khan Academy and score improvements on the SAT. Score gains are consistent across gender, family income, race, ethnicity, and parental education level.
Full study
Cartoon of a multiple choice test with a book and pen.
Top university students use Khan Academy.

Stanford Consulting Group ran a survey in 2015 that included the question, “Have you found Khan Academy meaningful to your education?”
Here’s the breakdown of students who replied yes:
65%

of Stanford students
(out of 504 surveyed)
57%

of students in top schools including
Harvard, Princeton, Yale, UPenn, UC Berkeley,
Caltech, and MIT (159 surveyed)
64%

of first-generation college students
(164 surveyed)
“For all the times I couldn’t turn to my parents for homework help, I had Khan Academy videos to help me. Khan Academy was the private tutor that my family could not afford. For all the times I wanted to learn for the sake of learning, I would pick from the hundreds of Khan Academy videos.”
— FIRST-GENERATION STANFORD UNIVERSITY STUDENT
Khan Academy reduces remediation rates.
NEW ENGLAND BOARD OF HIGHER EDUCATION
Khan Academy reduces the number of remedial courses students need to take.

The New England Board of Higher Education conducted a two-year study of Khan Academy with 1,226 students in developmental math classes at 12 community colleges in five states.
Full study
OAKLAND UNITY HIGH SCHOOL
Student performance in algebra increases from the 76th percentile to the 94th percentile.

Oakland Unity, a charter school serving low-income students, increased scores after using Khan Academy for two years, starting in 2013.
Full study
Results span countries and grade levels.
NUMERIC
Students using Khan Academy experience double the growth of their peers.

In 2014, an after-school math program in South Africa found an average 14% improvement in arithmetic and pre-algebra test scores when Khan Academy was used for approximately two hours per week for 10 weeks.
Full study
Cartoon of globe atlas with books.
Want Khan Academy to support you in delivering results for your learners?
Sign up


  To log in and use all the features of Khan Academy, please enable JavaScript in your browser. 

 Khan Academy is a non-profit educational website created in 2006 by educator Salman Khan to 
provide "a free, world-class education for anyone, anywhere." 

Khan Academy is a non-profit educational website created in 2006 by educator Salman Khan to 
provide "a free, world-class education for anyone, anywhere."
 WHOLE PROJECT MARK
