PROJECT NAME: OpenSpending 
PROJECT SOURCE: EMPATIA case studies
---------------------------------------
NEW PAGE: 

===============================

 OS 

          Search over  {{ (num_packages | number) || ' ' }}  data packages from  {{ (num_countries | number) || ' ' }}  countries with over  {{ (num_records | number) || ' ' }}  fiscal records.

 What is OpenSpending? 

 What is OpenSpending? 

          OpenSpending is a free, open and global platform to search, visualise and
          analyse fiscal data in the public sphere. Start searching, or read on to
          contribute data, code, or domain expertise.

 Search fiscal data 

 What else can I do here? 

 Publishers 

          Get started publishing your fiscal data today with the interactive
           Packager , and explore the possibilities of the
          platform s rich API, advanced visualisations, and options for
          integration.

 What is OpenSpending? 

 Hackers 

 Hackers 

          OpenSpending is a modern stack designed to liberate fiscal data for
          good, and we d love your help! Start with the  docs ,  chat with us , or just  start hacking .

 Publishers 

 Civil society 

 Civil society 

          Access a powerful suite of visualisation and analysis tools, running
          on top of a huge database of open fiscal data. Discover facts,
          generate insights, and develop stories.

 Hackers 

 Get involved 

 Get involved 

          Do you want to get involved with the OpenSpending project, so we can
          work together towards a future of fiscal transparency from
          governments and other public bodies?

          If you are a journalist, a developer, a publisher, a CSO or just
          someone who wants to be part of the change, we would love to hear
          from you. Please  introduce yourself in our discussion
          forum , and get started with a range of tutorials on using
          OpenSpending.

 OSPackager 

 OSViewer 

 With support from 

        An Open Knowledge International project in collaboration with  Open Knowledge Germany ,  Open Knowledge Greece , and the wider open fiscal data community.

 arrow_down  arrow_drop_down  box  bracesbulb  chart-barchart-bubble-treechart-dimensionchart-linepiechart-radarchart-sankeychart-tablechart-tree-mapcheckmark  chevron_with_circle_left  circle_plus  cog  downloadeditembed    file  filter  helplocation  moreopenremovesearchshare  sortstartiptrashcan  upload  user
 WHOLE PROJECT MARK