Start the day with incredible coffee from an office coffee machine

The Office Coffee Company supplies a range of easy to use coffee machines for the work place. Your business will enjoy fresh, organic and ethically sourced coffee, everyday. Our free of charge installation and low cost coffee machine rental means your office is up and running in next to no time.

It’s this simple to enjoy great coffee in the office

Our coffee experts are available to come to your office so you can enjoy our range of incredible coffees. They’ll even bring along the coffee machine most suited to your needs for you to try.
Coffee machine
1 Choose a machine
Coffee beans
2 Arrange a tasting
Coffee
3 Enjoy our coffee
Coffee cups
4 Order more supplies

Friends Who Have Great Taste

Businesses in London and all around the UK can’t get enough of our coffee. Here are a few of our caffeine loving friends...

    YouGov
    Mulberry
    NationalHistory
    Claranet
    Abbey Road Studios
    Deliveroo
    Freesat
    YouGov
    Mulberry
    NationalHistory
    Claranet
    Abbey Road Studios
    Deliveroo
    Freesat
    YouGov

Watch our video
Read testimonials	
Bean to cup office coffee machine
Office Coffee Machines

Espresso, capaccino, flat white anyone? Our range of easy to use bean-to-cup and filter coffee machines are designed for all areas in the office.
View Machines
Spacer
Incredible office coffee

We’re are all about fresh and quality in the cup. Our range of hand roasted coffees are organic or ethically sourced and roasted for freshness here in the UK. What’s more, order before 3pm for our next business day delivery.
Visit store
Latest Blog Posts

    London buses powered by your used coffee grounds
    London buses powered by your used coffee grounds
    07 December 2017

    Coffee is the fuel that sets thousands of Londoners up for a busy day in the nation’s capital, and now it's being used as a fuel in a more literal sense.
    Workforces who play together, stay together
    Workforces who play together, stay together
    13 October 2017

    Employers are more aware than ever before that they need to invest in creating a pleasant work environment.

Latest Tweets

    Office Coffee Co @OfficeCoffeeCo

    6 easy ways to green up your office https://hubs.ly/H0d8vTG0  #Recycle #Green #Environment #Organic #EcoFriendly #EcoOffice #OfficeLife #9to5office #coffee #OrganicCoffee #OfficeCoffee

    Posted 1 hour ago
    Office Coffee Co @OfficeCoffeeCo

    We're big on sustainability and the environment which is why we were delighted to receive recognition for a 33% reduction in emissions back in October #ThrowbackThursday #TBT #officecoffee #coolearth #carbon #footprint #sustainability https://hubs.ly/H0d5LYH0 

    Posted on Jul 26, 2018
    Office Coffee Co @OfficeCoffeeCo

    Click here to find out how... #wednesdaywisdom #wednesdaywords #wednesdayhumpday #goodcoffee #officecoffee #officelife #9to5life

    Posted on Jul 24, 2018

Connect with us

Be first to receive all the latest team updates, blogs and online store offers.
Spacer Spacer Spacer Spacer Spacer
Read Our Blog
Meet Our Team
Customer reviews

Coffee and refreshments experts

We know you demand the best. That’s why we only provide a one stop solution for the very best range of office coffee machines, water coolers, coffee and hot drinks.

With a winning combination of fantastic products, free expert advice and service support; we can help you find the best possible coffee and drinks solution for your business.


TotalCare service

Our hassle-free, TotalCare Service is an essential add-on that makes sure you have everything from installation to after-care covered.

Spacer
Full Installation

We fully set up your new coffee machine and take care of all the water connections.
Spacer
In-office Training

So you can get the most out of your new coffee machine from the get go, we provide in-house care and training.
Spacer
Valet & Descale

To keep your coffee machine clean and scale-free, we carry out deep cleaning and filter changing visits.
Spacer
Tender Loving Care

Our in-house service team is on hand if your machine needs some TLC, plus we cover all the costs of parts and labour.
Spacer
Education and Training

We have a put together a fun in-house training plan that will allow you to get the most out of your new coffee experience.
Find Out More
Spacer
Sustainable and Ethical

Is all coffee equal? We think not. Our outstanding range of Fairtrade, Rainforest Alliance and organic coffee is pretty splendid indeed. With every purchase made, trees are saved and CO2 is locked in.
Find Out More
Friends Who Have Great Taste

Businesses in London and all around the UK can’t get enough of our coffee. Here are a few of our caffeine loving friends...

    YouGov
    Mulberry
    NationalHistory
    Claranet
    Abbey Road Studios
    Deliveroo
    Freesat
    YouGov
    Mulberry
    NationalHistory
    Claranet
    Abbey Road Studios
    Deliveroo
    Freesat
    YouGov

Read Testimonials



WHOLE PROJECT MARK
