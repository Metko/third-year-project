schoolreport
===============================

[if lte IE 9]>
<div id="ieWarning">
This site is optimized for <a href="http://www.firefox.com">Firefox</a> or
<a href="http://chrome.google.com">Chrome</a>
browsers. Although it might work in a Microsoft browser, for both security and personal enjoyment, we
encourage you to use a real browser.
</div>
<![endif]
SchoolReport  gives a voice to South African learners.
Reporters
...
Schools
Reports
We provide early warning of
issues, and report on successes.
Built on the popular Mxit platform, the  schoolreport  app reaches
the least resourced schools and the most rural students.
We are exploring partnerships with educational organizations interested
in our data and mobile reach.
We would love to hear from you.
Your Name  Required
Please enter your name.
Your Email  Required
A valid email address is required.
Your Message  Required
You must leave a message.
&copy; 2013 SchoolReport. All Rights Reserved.
WHOLE PROJECT MARK
