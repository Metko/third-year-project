LUMKANI
===============================

Logout
Fire Detection
Partners
Awards
Media
How to Install
Contact
Lumkani is a social enterprise that seeks to address the challenge of shack/slum fires in urban informal settlements in South Africa and across the globe.
Lumkani aims to create social impact by increasing the safety and security of people living in informal settlements with technology-based early-warning systems.
Lumkani is committed to continual innovation and inclusive design practices that are driven by testing and end-user feedback.
Lumkani seeks to promote business and social practices that respect people, ensure sustainability and create value.
Lumkani has developed an early-warning system to reduce the damage and destruction caused by the spread of shack/slum fires in urban informal settlements
Many cooking, lighting and heating methods used by people living in informal settlements produce smoke. For this reason Lumkani detectors use rate-of-rise of temperature technology to accurately measure the incidence of dangerous fires and limit the occurrence of false alarms.
Density is a challenge all urban informal settlements share and is a major risk factor that enables the rapid spread of fires. In order to provide sufficient early-warning, a communal alert is required. Our detectors are networked within a 60-metre radius so that in the event of a fire all devices in this range will ring together, enabling a community-wide response to the danger. This buys time for communities to become proactive in rapidly spreading fire risk situations.
We have taken our system even further.  We have developed and rolled out smart centralised devices, which gather information about the detector mesh network. These devices constantly check the health of the system and in the event of fire, store GPS coordinates and simultaneously send text-message warnings to members of the affected community. Our next phase is to send, in real-time, the coordinates of fires to the municipality’s emergency response personnel
Since November 2014, Lumkani has distributed detectors to over 11 000 households in total. We are already detecting fires and creating the value we envisioned, all the while collecting insightful data around the technology, the fire challenge and the human experience.
<form method="get" action="Lumkani Company Profile.pdf">
<button type="submit" data-wow-delay=".5s" class="btn btn-primary btn-lg wow bounceInRight"><i class="fa fa-info" style="padding-right: 16px;"></i> Download Company Profile</button>
</form>
Download Company Profile
<div class="col-md-6" style="text-align:right;">
<img src="/resources/network.jpg" width="80%"  />
</div>
Lumkani won the best start-up in the Global Innovation through Science & Technology, Tech-I competition at the Global Entrepreneurship Summit 2014.
Lumkani is a finalist in the Chivas Regal "The Venture" social enterprise competition.
Lumkani won the best student business idea in the Western Cape Premier’s Entrepreneurship Recognition Awards.
Lumkani won the overall prize in the Comfortable Home category for Better Living Challenge 2014.
We were proud finalists of the SeedStars business competition.
Lumkani is a proud recipient of a Shuttleworth Foundation Flash Grant.
Lumkani is a Tech Awards Laureate in the Young Innovator category presented by Katherine M. Swanson
How To Install
<form method="get" action="resources/pdf/Lumkani-Installation-Sheet.pdf">
<button type="submit" data-wow-delay=".5s" class="btn btn-primary btn-lg wow bounceInRight"><i class="fa fa-circle-down" style="padding-right: 16px;"></i> Download Installation Sheet</button>
Download Installation Sheet
Get in touch with us below, or send us an email at info@lumkani.com – we’ll write back.
Address
19 Kent Street, Woodstock, Cape Town, 7925
Phone
+27 (0) 63 109 5401
Email
info@lumkani.com
Media Kit
View Lumkani Media Kit
Thanks for sending your message. We'll get back to you shortly.
There was a problem sending your message.
Please complete all the fields in the form before sending.
Send
input#subject.form-control.input-lg.required(type='text', placeholder='Subject of your message', name='subject')
&copy; 2015 LUMKANI
content
Media Resources
Click an image to open full resolution (opens in a new tab)
Lumkani is a start-up manufacturer of low-cost fire alarm systems, which are made to suit the needs of informal settlements - slums and shacks in urban areas.
WHOLE PROJECT MARK
