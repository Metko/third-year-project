Click a Tree: The challenge
===============================

[bg] [cs] [da] [de] [en] [fr] [hu] [pl] [ro] [sv]
Home Login Legal Notice Contact Press Sitemap Europa Click a Tree Click a Tree initiative The challenge
TYPO3SEARCH_begin
CONTENT ELEMENT, uid:14/templavoila_pi1 [begin]
The challenge
Set a trend - Plant a Tree!
We are all responsible for fighting climate change. With the “Energy-Climate Change” public awareness campaign, the European Commission seeks to inform young Europeans about the importance of fighting climate change and inspire them to take action. The campaign highlights the important contributions that every one of us can make to limit the effects of climate change and seeks to empower young individuals from the EU Member States to create change by making on-the-spot actions. With the world facing serious environmental challenges, it is absolutely necessary for everyone to do their part - even YOU! Planting a tree is easy to do and you can watch how a single effort can grow to become something much larger. By taking this symbolic action you can raise awareness of the many actions that we can all take to protect our climate.
Plant a Tree.
Planting a tree is very easy: Once you have found a suitable spot for planting your tree, register with Click a Tree. After your little seedling is comfortably settled in its new home, take a photo of the tree and then upload it onto the website. You can also mark the spot where you planted your tree on our virtual map. Contributors who are not entirely sure where to plant a tree, look out for our  tips and tricks .
CONTENT ELEMENT, uid:14/templavoila_pi1 [end]
TYPO3SEARCH_end
23:52 07.11.10  Frostbitten : planted trees: 1 15:41 28.05.10  di.todorova : planted trees: 2 08:18 16.05.10  laryssukah : planted trees: 1 10:41 08.05.10  adina2010 : planted trees: 1 Total Planters: 8452 TYPO3SEARCH_begin TYPO3SEARCH_end
Recommend this page To Top
The initiative focuses on fighting climate change through a public awareness campaign initiated by the European Commission.
WHOLE PROJECT MARK
