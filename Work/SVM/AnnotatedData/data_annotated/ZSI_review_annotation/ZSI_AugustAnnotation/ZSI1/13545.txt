Project Khuluma:  A mobile phone support group initiative to address the mental health and wellbeing needs of HIV positive adolescents in South Africa
===============================

Brand and toggle get grouped for better mobile display
Toggle navigation
Collect the nav links, forms, and other content for toggling
Home
Projects
Team
News
<li><a href="http://shmfoundation.org/?p=325" >Publications</a></li>
Contact
/.navbar-collapse
/.container-fluid
.column[data-col="1"] .row[data-cols="1"]
Tackling mental health is a major global health challenge. Nearly 75% of the 450 million people worldwide with a mental illness live in the developing world, and 85% of these people have no access to treatment [1] . The social and economic costs are enormous.
Given the immense challenge that there exists when it comes to mental health, the SHM Foundation decided to look at how we could develop a low cost and sustainable solutions. Khuluma was launched in late 2013 in South Africa through clinics in Pretoria and Cape Town and was set up specifically to address the mental health and wellbeing needs of HIV positive adolescents.
What?
The Khuluma model is an mHealth platform that provides an integrated, cost-effective and scalable solution to the growing challenge of mental ill health for chronic illness sufferers, currently in South Africa; where currently, over 15% of young women and 5% of young men aged 15-24 in South Africa are infected with HIV (UNAIDS, 2010).
The Khuluma platform leverages the power of small groups to facilitate interactive, closed support groups of 10 to 15 participants who have been diagnosed with a chronic illness. Currently live with 99 HIV positive adolescents, participants are able to communicate amongst themselves and with a facilitator and a mentor (an HIV positive adolescent who has been through Khuluma) via mobile phone about any topic that they deem important to discuss.
The model has been applied in the UK, Mexico and Guatemala with HIV positive pregnant women, new mothers and parents, and is now expanding into new populations, conditions and countries.
Khuluma is:
Peer led
Anonymous
Accessible, where ever, whenever
Intimate
Khuluma was set up to break down the following barriers to support groups:
Stigma and discrimination.  Despite nation wide campaigns in South Africa many fear disclosing their chronic illness or HIV status and experience stigma and discrimination in the communities that they live in.
Finding the time to attend a support group. HIV positive adolescents can have complex lives, with regular visits to clinics and hospitals to pick up their medication and check ups along with attending school and fulfilling responsibilities at home.
Social Isolation.  Support groups are tailored to them. If they are lucky enough to have access to a support group at their clinic or hospital the support groups tend to cover a wide age range making it challenging to bring up age appropriate topics.
Having the necessary funds such as transport costs available to attend.
How?
The Khuluma model is a ‘ digito-social approach’  to mental illness that brings together two concepts – the power of small support groups, and the power of mHealth – in an integrated platform.  The model breaks down barriers through this integrated platform to make a support group that is accessible, immediate and age appropriate. The digito-social approach to mental illness leverages the ‘digital habitat’ of adolescents today to increase disclosure, medical adherence and collect quality data on mental illness.
Results
For those who we have taken part, there have been significant positive health outcomes:
Decreased levels of anxiety and depression
Increased perceived levels of social support
Decreased perceived levels of internalised stigma
Increased knowledge about their condition and how to access specific services
[1]   http://www.who.int/whr/2001/media_centre/press_release/en/
Click here to access the full list of our Health projects
About us
The SHM Foundation works globally to bring about positive social change through projects in the areas of learning and citizenship, health and the arts.
The Foundation was set up in 2008 by Professor Maurice Biriotti and Professor Henrietta Moore, who co-founded the strategy and insight consultancy SHM Productions, based in central London, in 1996.
<div class="col-md-2">
<a href="#" class="facebook" ><img src="https://shmfoundation.org/wp-content/themes/foundation%200.3/img/FB.png" class="img-responsive center-block" alt="..."  /></a>
</div>
Tweets by @SHMFoundation
<h2>Social</h2>
<span>
<div class="col-md-3">
<div class="col-offset-md-3 col-md-3">
<a href="#" class="twitter" ><img src="https://shmfoundation.org/wp-content/themes/foundation%200.3/img/TW.png" class="img-responsive center-block" alt="..."  /></a>
<div class="col-md-3"></div>
</span>
<div class="row">
<h2>Twitter</h2>
Contact us
Contact Form
Send Message
Alertbox for success
&times;
Voila!  Your message has been sent. We will get back with you soon.
Alertbox for failure
Oops!  We can't send your message.
The SHM Foundation is a charitable foundation (registration number 1126568)
Privacy Policy  |  sitemap
The SHM Foundation Data Principles and Privacy Policy
Principles
The SHM Foundation is committed fully to compliance with the requirements of the General Data Protection Regulation (GDPR). The GDPR applies to all organisations that process data about their employees, as well as others, e.g. customers and clients. It sets out principles which should be followed by those who process data and it gives rights to those whose data is being processed.
To this end, the organisation endorses and adheres fully to observing the eight individual rights set out under the GDPR, these are the following:
The right to be informed. The right of access. The right to rectification. The right to erasure. The right to restrict processing. The right to data portability. The right to object. Rights in relation to automated decision-making and profiling. These rights must be observed at all times when processing or using personal information. Therefore, through appropriate management and strict application of criteria and controls, the organisation will:
Observe fully the conditions regarding having a lawful basis to process personal information Meet its legal obligations to specify the purposes for which information is used Collect and process appropriate information only to the extent that it is necessary to fulfil operational needs or to comply with any legal requirements Ensure the information held is accurate and up to date Ensure that the information is held for no longer than is necessary Ensure that the rights of people about whom information is held can be fully exercised under the GDPR (i.e. the right to be informed that processing is being undertaken, to access personal information on request; to prevent processing in certain circumstances, and to correct, rectify, block or erase information that is regarded as wrong information) Take appropriate technical and organisational security measures to safeguard personal information ensure that personal information is not transferred outside the EU, to third countries or international organisations without an adequate level of protection
Privacy policy
The SHM Foundation website is owned by The SHM Foundation. The SHM Foundation believes you have a right to privacy on the web, and we work to protect your security online as vigorously as possible. The SHM Foundation has developed the following policy to help you understand how your personal information will be treated as you engage with us both on and off-line. The SHM Foundation's policy complies with GDPR regulations. We are committed to using information fairly, keeping information securely, making sure it is accurate, and keeping it up to date. Further details on the GDPR can be found on the Information Commissioner's website
This policy applies to all pages hosted on this site and also to all other The SHM Foundation owned websites except where specifically mentioned on other The SHM Foundation sites. It does not apply to otherorganisationsto which we may link and whose privacy policies may differ.
The SHM Foundation may amend this policy from time to time. If The SHM Foundation makes any substantial changes in the way your personal information is used, we will make that information available by posting a notice on the site.
The SHM Foundation's security measures and privacy policy are reviewed on a regular basis. If you have any questions relating to this policy or the information The SHM Foundation holds about you, please address them to: The Data Officer, Victor Biriotti The SHM Foundation, 20/22 Bedford Row, London, WC1R 4EB Or by email to:  victor@shmfoundation.org
1. What information does The SHM Foundation collect?
The SHM Foundation collects only information which you give to us via email, the various data capture forms on our websites, or information voluntarily given to us via telephone or postal mail. The SHM Foundation may also collect and analyse information about the way you use our websites and communications we may send to you – for example if The SHM Foundation sends a communication which enables you to "click through" to a certain page of a website.
The SHM Foundation websites may use a standard technology called a "cookie" to collect information about how you use the site; this is a small data file that certain websites write to your hard drive when you visit them. A cookie file can contain information such as a user ID that the site uses to track the pages you have visited, but the only personal information a cookie can contain is information you supply yourself. A cookie cannot read data off your hard disk or read cookie files created by other sites.
Some parts of The SHM Foundation websites use cookies to track user traffic patterns. The SHM Foundation does this in order to determine the usefulness of our website information to our users and to see how effective our navigational structure is in helping users reach that information. If you prefer not to receive cookies while browsing The SHM Foundation websites, you can set your browser to warn you before accepting cookies and refuse the cookie when your browser alerts you to its presence. You can also refuse all cookies by turning them off in your browser. You do not need to have cookies turned on to use/navigate through many parts of The SHM Foundation websites. For more information about cookies see  www.allaboutcookies.org .
2. How is your information used?
The SHM Foundation may use the information gathered for the following purposes:
To keep you up to date on the latest announcements or other information related to The SHM Foundation that we think you may like to hear about and to send you communications that may be of interest to you. The SHM Foundation may send this information electronically or otherwise, if you have opted-in to receive information from us. To personalise your experience on The SHM Foundation websites To analyse and improve the services offered on The SHM Foundation websites - e.g. to provide you with the most user friendly navigation experience. To track usage. For "service administration" purposes which means that The SHM Foundation may contact you for reasons related to any services you may have signed up for – e.g. to notify you that a particular service may be suspended for maintenance. 3. Does The SHM Foundation share my information?
By providing personal information you are deemed to consent to your personal information being shared within The SHM Foundation and being shared outside of The SHM Foundation in the following circumstances:
Where The SHM Foundation needs to share your information to provide the service you have requested (such as sending posters or leaflets). Where The SHM Foundation needs to send your information to companies who work on behalf of The SHM Foundation to provide a service to you. We will only provide those companies the information they need to deliver the service, and they are prohibited from using that information for any other purpose. Where The SHM Foundation is required to disclose your personal information by law in urgent circumstances, to protect personal safety, the public or our website(s). 4. How secure is the information you give to The SHM Foundation?
Data collected on The SHM Foundation websites is stored in controlled servers with limited access. Your information may be stored and processed in the United Kingdom or globally, where The SHM Foundation or its partners are located.
5. How can you access, correct or delete your data?
At your request The SHM Foundation will give you all details of personal information held by The SHM Foundation, once identity has been satisfied. Incorrect information will be deleted or amended promptly.
The SHM Foundation can only accept these requests if sent by email to:  victor@shmfoundation.org  ;  or by fax to: 020 7831 7061  or by post to: Data Officer, The SHM Foundation, 20/22 Bedford Row, London, WC1R 4EB;  specifying on which SHM Foundation owned website your personal information has been registered. Once the request has been received The SHM Foundation will take further steps to confirm identity before releasing data.  The SHM Foundation will endeavourto respond to requests within 28 working days unless there are exceptional circumstances when we will let you know what is happening with your request.
6. How can you opt out of receiving messages from The SHM Foundation?
The SHM Foundation will send you communications if you have given us permission to do so.. If you do not wish to receive messages from The SHM Foundation, please send an email asking that your contact information be removed from The SHM Foundation databases to:  info@shmfoundation.org .
7. How long will The SHM Foundation keep your personal information?
The SHM Foundation will hold your personal information on our systems as long as is necessary for the relevant service you have requested, or until you inform us that you wish your information to be removed from our databases (see point 6 above).
This policy was updated in May 2018.
End of involved
jQuery (necessary for Bootstrap's JavaScript plugins)
Include all compiled plugins (below), or include individual files as needed
KhethImpilo specialises in solution development and implementation for health and community systems and services strengthening in marginalized communities.
WHOLE PROJECT MARK
