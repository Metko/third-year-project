CPS :
Early childhood :
Child Parent Center

===============================
SharePoint Header
SharePoint Site Actions Drop-down button
SharePoint Ribbon
Console
END SharePoint Header
MENU
Students
Schools
About
Calendar
Staff
Topics
[if lt IE 8]>
Search
<![endif]
/.container
/.navbar
Sharepoint Breadcrumb - Hide
IE7 Alert
× Close
Did you know that your Internet Explorer is out of date?
To get the best possible experience using our site we recommend that you use Google Chrome. To visit the Chrome download page, click the Get Chrome button below. Not using IE7? Refresh your browser and this message will go away!
Get Chrome
Page Content
About our Programs
The Child-Parent Centers (CPC) program is an early childhood preschool model that emphasizes aligned education and services in high needs communities, for children from pre-kindergarten through the primary grades. The CPCs are a family centered program, focused on the needs of the students and their families to ensure their success in school and beyond. A hallmark of the CPC program is a collaborative team that includes the head teacher, parent resource teacher and the school community representative that aligns and coordinates services and education for students and their families. Additionally, the CPC program promotes aligned curriculum, intensive family supports and services, parent involvement and engagement, effective learning experiences, and a professional development system for teachers.
History of the CPC
The program was designed as a response to three major problems facing Chicago&rsquo;s west side neighborhoods of North Lawndale and West Garfield Park in the mid-1960s: low rates of school attendance, family disengagement with schools, and low student achievement. For example, only eight percent of sixth graders in area schools were at or above the national average in reading achievement.
Based in part on a survey of neighborhood residents, Dr. Lorraine Sullivan, District 8 Superintendent for Chicago&rsquo;s west side schools and CPC founder, believed that the best solution was to design an early childhood program with parents as partners in children&rsquo;s education. In May 1967, the Child-Parent Education Centers were established in four sites serving the most disadvantaged areas of the city. Expansion began soon after.
By approving the establishment of CPCs in 1967, the Chicago Public School District was the first in the nation to allocate Title I funds (from the Elementary and Secondary Education Act of 1965) to preschool. CPC is the second oldest federally funded early childhood program (after Head Start), and the first federally funded comprehensive PreK-3rd grade program.
The original goal of the program (an &ldquo;ESEA Title I, Model Project&rdquo;) was to &ldquo;reach the child and parent early, develop language skills and self-confidence, and to demonstrate that these children, if given a chance, can meet successfully all the demands of today&rsquo;s technological, urban society.&rdquo;
Dr. Sullivan described the CPC philosophy as promoting a nurturing learning climate: &ldquo;In a success-oriented environment in which young children can see themselves as important, they are &lsquo;turned on&rsquo; for learning. Attitudes toward themselves and others, interest in learning, increased activity, conversation, and enthusiasm are all evidences of the change. Parents are increasingly aware of the role of the home in preparing children for school and have renewed hope that education will develop the full potential of their children.&rdquo;
Reynolds, www.humancapitalrc.org, (2014)
CPC Program Elements
The Child-Parent Centers use the Creative Curriculum. The Creative Curriculum for Preschool is an award-winning curriculum for preschool success. It enables children to develop confidence, creativity, and lifelong critical thinking skills, and is based on 38 objectives for development and learning.
CPC requires 2&frac12; hours of parent involvement weekly. Parents sign a School-Home agreement at the time of registration agreeing to participate at least 2&frac12; hours each week. These hours can be a combination of both in school parent activities or at home activities.
The Collaborative Team: The Collaborative Team consists of the Head Teacher (HT), Parent Resource Teacher (PRT), and School Community Representative (SCR). The Collaborative Team works together to ensure the needs of all students, families and teachers are met to promote the success of each child in the CPC.
The Head Teacher supports:
Coaching of teachers
Professional development for teachers
Administration of the program
Recruitment and enrollment
Coordinated curriculum implementation through the primary grades
The Parent Resource Teacher supports:
Parent workshops in our six (6) focus areas of concentration
Child development and parenting
Health, safety, and nutrition
School involvement
Language, math, and science
Field experiences and community resources
Education, career, and personal development
Parents to be advocates for their child
Classroom curriculum with child development workshops to promote home school connections
Families and connects them with community resources
The School Community Representative supports:
Attendance initiatives
Home Visits
Families and connects them with community resources to enhance the success of each family
CPC Partnerships
Investing in Innovation (i3) : The i3 grant has expanded the Child-Parent Centers in the Midwest.
A partnership of 11 education and nonprofit agencies in Illinois, Minnesota, and Wisconsin have been awarded an Investing in Innovation or &ldquo;i3&rdquo; grant of $15 million over five years from the U.S. Department of Education to scale up an integrated school-based early childhood intervention. The grant ends in the 2016-2017 school year with sustainability of all the program improvements supported by the district.
Erikson Institute : Erikson provides high-quality, developmentally appropriate educational modules to all CPC teachers. These models promote the alignment of curriculum between grade levels PreK to 3rd grade. Additionally, Erikson provides coaching support to the Collaborative Team.
Metropolitan Family Services  (MFS): MFS currently partners with the City of Chicago and Chicago Public Schools (CPS) to provide enhancement of family engagement efforts and program support at SIB CPC schools throughout the city of Chicago.
Eligibility
All children must be at least 3 years old by September 1st of a school year.  To enter the program families must apply through the CPS Ready to Learn Process.
Contact Information
Office of Early Childhood Education
42 West Madison Street
Chicago, Illinois 60602
Phone (773) 553-2010
Find a Program
Chicago Early Learning
Related Topics:
Early Childhood
/
Family and Community Engagement
Page Last Modified on Thursday, January 07, 2016
/container
/Page Content >
Start of Footer
Chicago Public Schools
Connect
Facebook
Twitter
Youtube
Linkedin
Blog
Quick Links
Contact Us
Careers
CPS Stats and Facts
News and Press Releases
What's New on CPS.EDU?
Chicago Public Schools is the third largest school district in the United States with more than 600 schools and serves 371,000 children. Our vision is that every student in every neighborhood will be engaged in a rigorous, well-rounded instructional program and will graduate prepared for success in college, career and life.
Non-Discrimination Statement
©  2015    Chicago Public Schools
/Start of Footer
/.footerwrapper
Google Analytics
Google Translate
Scroll to top
The Child-Parent Center (CPC) program provides services to preschool children and their parents residing in primarily low income neighbourhoods and elementary schools.
WHOLE PROJECT MARK
