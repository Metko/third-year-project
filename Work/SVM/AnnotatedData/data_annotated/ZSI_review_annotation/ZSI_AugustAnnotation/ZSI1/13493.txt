What is Community Shop? | Company Shop
===============================

body onload="initialize()"
Header Starts
Top Bar Starts
Staff Shop Members
Nestle
Apply For Membership
Privacy Policy
Top Bar Ends
Logo Bar Starts
Logo Bar Ends
Header Ends
Navigation Starts
Search
end navbar-header
Company Shop
What is Company Shop
Who is Company Shop for?
Company Shop eligibility
Membership application form
Store locations and opening times
Locations
Offers and promotions
News and Enquiries
Enquiries
News Centre
Job Vacancies
Terms and Conditions
Community Shop
What is Community Shop?
Testimonials
Who is Community Shop for?
Case studies
How to become a member
How we work
What we do
How we do it
News and enquiries
Frequently asked questions
Corporate
About Company Shop
Heritage
What does Company Shop do?
Economic
Social
Environmental
Awards and accreditations
News centre
Contact
end nav navbar-nav
end #navbar-collapse-1
end navbar navbar-default fhmm
Navigation Ends
Carousel Starts
Wrapper for slides
class of active since it's the first item
End Carousel
Content Starts
Welcome to Community Shop: More than just food
Community Shop is a social enterprise that is empowering individuals and building stronger communities, by realising the social potential of surplus food.
Members of Community Shop can shop for good food at great prices – easing pressure on family budgets – but also gain access to professional, personal development programmes, to kick-start positive change in their own lives. We call this The Success Plan.
From inspiring motivation and confidence in our members, to improving health and economic outcomes for their families, the impact of Community Shop is already being felt way beyond the aisles of our stores.  Community Shops are rooted in their local communities and actively forge links and partnerships with established local services.
Community Shop is an initiative from Company Shop – the UK’s largest redistributor of surplus food.
Latest News
Community Shop Delivers Greater Impact Than Ever
Lords, MPs and food industry leaders celebrated the impact of Community Shop, the UK’s first social supermarket chain, at a high-profile event in the House of Commons.
The event was held to launch Community Shop’s 2017 Impact Report, which celebrates the positive outcomes that the pioneering community food hubs have had by helping thousands of people across the country. [More]
Jamie Oliver and Jimmy Doherty Get Behind Food Surplus Pioneers, Community Shop
Community Shop, the UK’s first social supermarket chain, has featured on Channel 4’s hit television programme, Jamie and Jimmy’s Friday Night Feast, showcasing its pioneering model to unlock the potential in surplus food. [More]
Community Shop Celebrates Social Enterprise UK Award Shortlisting
Community Shop, the UKs first social supermarket chain, has been shortlisted in the ‘Prove It: Social Impact’ category at the Social Enterprise UK awards, following the launch of its Impact Report in March. [More]
Twitter
Tweets by @CommunityShops
Nav tabs
News
Tab panes
Yorkshire Life Magazine: Behind the scenes at Goldthorpe’s Community Shop
This month’s edition of Yorkshire Life Magazine carries an extensive feature on Community Shop. [More]
BBC Food and Farming Awards
Community Shop amongst Best Initiatives in British Food at BBC Food and Farming Awards. [More]
The Grocer Gold Awards 2014 - Community Shop shortlisted
Community Shop short-listed as ‘Consumer Initiative of the Year’ for The Grocer Gold Awards 2014 [More]
Content Ends
Bottom Bar
Membership
Want to find out about becoming a Community Shop member?
Find out what we do, and how we do it
Store locations &amp; opening times
Find your nearest Community Shop
/Bottom Bar
Footer
Footer Menu Starts
All News
Company Shop Limited
Wentworth Way,
Wentworth Industrial Estate,
Tankersley, Barnsley, Yorkshire
S75 3DH
T: 01226 747121
F: 01226 748324
E:  [email&#160;protected]
For all media enquiries please call 0800 024 6691
Back to top
Footer Menu Ends
Footer Links Starts
&copy; copyright Company Shop Limited 2018
Website Policy
Conditions of Sale
Footer Links Ends
/footer
/outer wrapper
Community Shop is an initiative from Company Shop – the UKs largest redistributor of surplus food, and steps up to two of the biggest challenges – food waste and food poverty.
WHOLE PROJECT MARK
