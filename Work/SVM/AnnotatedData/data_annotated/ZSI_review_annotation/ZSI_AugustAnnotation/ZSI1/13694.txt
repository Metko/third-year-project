Agoge Omikron Omega | Changemakers
===============================

Preloader
<div id="preloader">
<div id="status"> </div>
</div>
Skip to main content
About
Challenges
Projects
Learning
Blog
Changemakers
English Español Português Français
Search form
Search
Log in
FAQs
You are here Home
Agoge Omikron Omega
The Re-imagine Learning Network
Congratulations! This Entry has been selected as a semifinalist.
Agoge Omikron Omega: Skills &amp; Creativity school-game
6 regional public schools network, Greece Kaparelli , Greece
Chrissy Pirounaki <div class="founder">Founder</div>
Year Founded:
2006
Organization type:  hybrid
Project Stage:
Growth
Budget:  &lt; $1,000
Website:
http://omikron-omega.gr
Community development
Early childhood development
Education reform
Play
Project Summary Elevator Pitch
Concise Summary: Help us pitch this solution! Provide an explanation within 3-4 short sentences.
Agoge ΟΩ is a wholistic pedagogical approach born by experiment in primary schools of greek islands. Agoge is based on multiple intelligence theory &amp; "free school" practices. Teachers use ΟΩ tools to inspire children from traditionally bored students into happy self-aware learners :)
WHAT IF - Inspiration: Write one sentence that describes a way that your project dares to ask, &quot;WHAT IF?&quot;
What if we had insight of what makes us unique! We could then find ourselves more useful to eachother.
About Project
Problem: What problem is this project trying to address?
Greek crisis in greek schools ! There is a sense of dull discomfortness in communities of underpaid greek teachers. More &quot;specialist&quot; education &amp; &quot;academic&quot; teaching gives more space to content-approach. Text books seem more important than Skills are. Creativity in classroom &amp; innovation is regarded to be a trouble while effectivity is seldom evaluated.
Solution: What is the proposed solution? Please be specific!
School classrooms can be more vibrant and fresh. Lets say more &quot;Omega&quot; for &quot;playful&quot;. Omikron stands for left brain skills and Omega for right, more creative and artistic.
So we play a year-long &quot;Colour Game&quot; (each one a type of intelligence)with students &amp; parents. Win is discovering identity traits (virtues &amp; weaknesses) &amp; creating peer helping networks while enjoying learning.Allegories &amp; philosophy for children are used 4motivation.
1:where circle desks,book activities &amp; class councils done = Omikron area
2:where carpets, home furniture &amp; structred-play activitites are for working in pairs or self reflection = Omega class area
Children are conscient &amp; creative while being in or out of classroom= Omikron Omega activities:)
Impact: How does it Work
Example: Walk us through a specific example(s) of how this solution makes a difference; include its primary activities.
1st  [email&#160;protected]  parents &amp; children get familiar with ΟΩ skills table + set personal aims. Students mate in working couples. Both motivate eachother for body-concentration, socio-emotional, logical-critical &amp; linguistic skills.
Challenges grow students&#039; self confidence faster, and in an emotional safe community excluded students feel accepted. Arrogant ones grow empathy.
Lessons are gradually presented by students + class councils get more autonomous. Students get enthusiastic &amp; eager for learning. Consciousness + self regulation is growing, while teachers get more time for monitoring &amp; reflecting. The Omikron Omega Class is training happy, creative and active future citizens.
Impact: What is the impact of the work to date? Also describe the projected future impact for the coming years.
An average of 100 students have participated in 6 small regional public schools the past 8 years.
Some old students are adolescents &amp; active young citizens now. We support this innovating teenagers network bringing them together through summer camps, local competitions, even sending them a set of 3G to go online while live in remote areas.
We believe in children in order they believe in themselves. Growing personal strategies we fight social inclusion &amp; types of addiction that is the &quot;big escape&quot; of our times.
Spread Strategies: Moving forward, what are the main strategies for scaling impact?
While Agoge as a creative &amp; effective teaching method brings impressive results, we now try to get some funding in order to train some educators working with this basic idea, in order to give good examples on how this is really working for parents, students &amp; teachers.
Socio-emotional, adaptability and desicion making are skills to effect student&#039;s lifes.
We wish our network makes slow, stable steps to grow properly &amp; make an actual impact, not only as a greek evaluation model but also help west world educators &quot;be a bit more Omega&quot;
Sustainability
Financial Sustainability Plan: What is this solution’s plan to ensure financial sustainability?
At the moment we are actually self-funded, based in a rather romantic belief that really works for the moment!
If we feel ready to grow our impact (clear that we what to stay volunteers) we need to get some funding for expanding our communities and train some more educators.
We applyed for this competition in order to get more insights on how to structure and improve our project.
We also plan to apply for a grant by a greek foundation.
Marketplace: Who else is addressing the problem outlined here? How does the proposed project differ from these approaches?
The innovation of Agoge is that brings multiple intelligence theory into everyday traditional classes. Steiner or Gardner or Montessori schools are independent and some times their students find it difficult to blend into &quot;normal&quot; communities.
Agoge is the greek word to say &quot;natural treatment&quot; for a health issue.
Its amusing that Agoge started as a game i made up for myself &amp; my friends to monitor what activities we need to do more or less following a regulating &quot;colour callendar&quot;
At least in Greece innovation as a systematic good practice and not only as a model to admire is needed.
Team
Founding Story
Trying to classify all different disciplines and combine them with Arts and Sciences , I found color folders useful. Later I decided to blend this colors with Plato&#039; s classification of body - spirit - intellect, Gardner&#039;s theory of multiple intelligences and neuroscience data about lower - mid - upper brain and 2 hemispheres function, in order to understand connections and shades of knowledge.
Όμικρον (Ο) is a solid symbol, answering &quot;what&quot; desires to reach whole (Όλον) and Ωμέγα(Ω) is an open symbol, answering &quot;how&quot; desires to reach beauty (Ωραίο).
Agoge means a way to be led.
Agoge Omikron Omega is a self-awareness path you discover while learning to live in a community.
We are all volunteers, full-time teachers or educators. We all have the common wish to inspire eachother and work together for creating this model in order to be useful for students and teachers.
We are 5 in our basic circle and 7 more as consultant members to help us grow the idea with their insight.
About You Organization:   Agoge Omikron Omega / Future Culture 2030 About You First Name Chrissy
Last Name Pirounaki
Twitter URL Facebook URL https://www.facebook.com/chrissy.pirounaki
About Your Project Organization Name Agoge Omikron Omega / Future Culture 2030
How long has your organization been operating? Project Organization Country , Kaparelli
Country where this project is creating social impact , 6 regional public schools network
What awards or honors has the project received? Funding: How is your project financial supported? Friends and family, Individuals. Supplemental Awards None.
Primary Target Age Group 6 - 12. Your role in Education Teacher. Please specify which of the following best applies: I am applying on behalf of a particular program or initiative. The type of school(s) your solution is affiliated with (if applicable) Public (tuition-free). Intervention Focus Curricular. Does your project utilize any of the innovative design principles below? Creating Peer Support Networks for All Stakeholders in Education: Creating peer-to-peer learning and support for teachers, parents, expecting mothers and more that enables them to transform learning. Is your project targeted at solving any of the following key barriers? Trapped Between Competing Pressures: Educators face a lack of capacity to re-imagine and restructure educational settings. Need Examples from other countries of similar projects
Offer Our experience and insights
What key learning outcomes does your work seek to improve? Self-awareness, meta-cognition, creativity,self-regulation
social skills (purple colour code)
communication skills (blue c/c)
natural &amp; empathy skills (green c/c)
logical skills (yellow c/c)
imagination &amp; critical skills (orange c/c)
body &amp; health skills (red c/c)
Secondary Form PROGRAM DESIGN CLARITY: We are hungry to know more about what exactly your model consists of. Please succinctly list a) what main activities are you doing with your beneficiaries, b) where you carry out the activities? c) how often? d) for how many hours? e) who delivers the services? and f) any other brief details  (c+d)everyday from 8am to 2 pm
(b+e) Teachers teaching in a ΟΩ model, in school classroom, out in the play area, in the garden field, in monthly local travel visits
(a) Omikron:skills-learning activities, Omega:creativity-play activities &amp; ΟΩ performances: engaging mixed technics using drama,music,arts, or technology.
(f) children are evaluated as a team, working in pairs &amp; encouraged individual always along with parents.The evaluation is descriptive + evolving throughout the school year.There is a colour criteria chart with 12 or 24 criteria (according to age) they play with.
INSPIRATION: What do you consider the most important trends or evidence that inspire you to believe the world is ready to Re-imagine Learning? Please elaborate. The most important global problem is ingnorance &amp; addiction as escape. We are afraid of our mistakes, so we dont dare, we dont innovate enough, we dont play anymore..so we dont learn in the most effective and natural way.
It is important we Re-imagine Learning in a way that blends West and East world wisdom. West world is target orientated, rather more left brain connections(O) while East world seems more intuitive, used to enjoy more of &quot;how&quot; to make (Ω), than &quot;what&quot; to finish. The new world doesnt need to fight for the best to succeed. Synthesis of both worlds seems the solution.Balance.
LEARNING THROUGH PLAY: What does “learning through play” mean to you and why it is a must-have, instead of a nice to have?  There is not a more natural &amp; effective way to learn than playing. Nature made it that way 4cats, 4humans, 4all animals : we play to gain experience, make emotional brain networks, memories + connections &gt; we repeat / avoid or forget practices that helps us no more.
If formal education (O) was more informal (Ω) then creativity &amp; joy would lead to more connections, new ideas to be transformed into positive memories and consequently stay longer as an active &quot;successful&quot; knowledge. If Symbolic level (numbers&amp;letters) connected with Life level, schools would contribute positively..
SUSTAINABILITY: Please list a quick breakdown of your funding, indicating the percentage that comes from each source.  We have no funding, except our own wage from public or private primary greek schools we work for. It is average 800 euro per month.
Some parents are able to help giving resources instead money.
Some small businesses donor material we may need.
We share material and ideas : )
MODEL: How does your mission relate to your business model (i.e. Non-profit, for-profit or hybrid)? We don&#039;t have a profit strategy.
We try to keep our project sustainable and growing stable, while we keep its volunteering character. We believe in volunteering joy : We need people with clear motivation for sharing creativity &amp; happiness.
FUNDING PRIORITIES: If your organization were given $20K in unrestricted funding today, how would you use it?  Why?   We would save it + get some mentoring  to make a strategy in order to train more teachers to bring creativity in classroom.
Agoge Omikron Omega vision is a &quot;school reform towards creativity&quot;. We would invest on proposing an evaluation model for greek schools as a tool to effectivity! Adults play!
PARTNERSHIPS: Tell us about your partnerships that enhance your approach. Being innovating in a network is safer and more sustainable, because the working environment we experience in greek public education seems rather hostile for innovation. Because of unsuccessful efforts (of more than 30 years) for school evaluation any &quot;reform&quot; is regarded as a &quot;threat&quot; to the conservative staff. Fear of unemployment don&#039;t leave any space for romantism. Working with partners is multiplying our creativity and keeps us up.
COLLABORATIONS: Have you considered or initiated partnerships with any of the other Challenge Pacesetters?  If so,  please share. We explored and read a lot of projects, wrote some reviews. Made some facebook connections to exchange views, but &quot;SmileUrbo&quot; or &quot;PlayGlobo&quot; seems closer to our project for the moment. We could try skype or see the possibility of a partnership the following days.
VISION: If you had unlimited funding, and you could fast forward 15 years to when your program has been able to achieve wild success - what will it have achieved?  Our students would be educators too and this circle in all professions as a choice of self-awareness would lead to healthier, more democratic and happier communities.
Students of Agoge ΟΩ would be a balanced example of practical life philoshophy, in personal and professional life. Other initiatives would be born by this idea started from our platonic utopia : )
IMPACT - KEY METRICS: Please list the key data points that you would cite as evidence that you are able to achieve lasting learning outcomes. Please also share one data point for which you most hope to see better results over time We are not interested in knowledge as content, but in life skills.
Investing in young people and also creating a network to keep our community in contact, we can achieve lasting learning outcomes.
We also involve local communities and local businesses to create more active attitudes &amp; a sense of responsibility towards younger generations.
IMPACT - REPORTING SAMPLE: Please attach any examples of your impact reporting. [optional]:  RESEARCH AND EVIDENCE: Please link or attach any research or evidence resource you are open to sharing more widely [optional]. Building research and evidence is a key aim of this initiative, and the resources you share may be chosen for listing in the  Center for Education Innovations library :  SOURCE: If applicable - who created the research or evidence you are choosing to share? :  IMPACT - REACH: How many people did your project directly engage in programmatic activities in the last year?  0 to 500 STUDY: Has an external evaluation or study been conducted of your organization? In progress Other (please specify) University os Volos, Greece : Education, leadership &amp; innovation PhD
Number of Employees: Please select Number of Volunteers: 10-100 APPROACHES: Given the complexity of play, it is not surprising that there have been numerous research attempts to categorize the different types and approaches! Please indicate which of the following your project focuses on. Creating a Supportive Socio-Emotional Environment, Providing a Range of Opportunities (providing the equipment and materials needed for various types of play), Educational Structuring (developing playful projects within educational contexts). Other (please specify) Creating a symbolic &amp; reflective game for students&#039; self-awareness
AFFILIATION: Please specify if your organization has any existing affiliations with the LEGO Group. Dieter Carstensen, Head of Digital Child Safety at the Lego Group
Log in  to post comments Printer-friendly version Download PDF
Give Feedback
Your insights will be sent privately to this project to help them improve. Rate This Project
Overall  *
Select rating Give it 1/5 Give it 2/5 Give it 3/5 Give it 4/5 Give it 5/5
Innovation
Impact
Write a Private Feedback  *
Strength Need Improvement
Clarity of model
Financial Sustainability
Idea Originality
Impact Measurement
Impact Potential
Partnerships
Scalability
Understanding of Marketplace
© 2016-2017 Ashoka Changemakers.
Terms  &amp;  Privacy Policy  |  Feedback
What We Do
What You Can Do Here
Our Team
Careers
Partner With Us
Contact Us
WHOLE PROJECT MARK
