
Chaithanya Ayurveda Hospital at Ranny

Chaithanya Ayurveda has started its Ayurveda health care services in 2006 as an out-patient clinic in Vadasserikara, and later started the hospital in Ranny in 2007. Now Chaithanya Ayurveda Hospital is a renowned centre for quality Ayurveda and authentic treatment services. We provide Ayurveda treatments and care in the most scientific way adhering to the scientific principles of Ayurveda. Chaithanya Ayurveda offers the traditional Ayurveda experience blended with all modern amenities. We have experienced Panchakarma therapists to carry out the treatments. Both curative and rejuvenative treatments are carried out after proper medical examination and constitutional analysis
Chaithanya Ayurveda Retreat at Alleppey

Chaithanya has extended its Ayurveda services and joined hands with Green Palace Health Resort , a serene back water Resort situated by the banks of River PUKAITHA, a tributary of Holy River PAMBA, in the Cochin – Quilon National waterway. The retreat is spread out in 6 acres of land facing waterfront, full of palm trees and inland water canal. The rear side of the retreat is lush green paddy fields popularly known as KUTTANAD. The access to Green Palace is only water way.
Ayurjani

Ayurjani-promoted by Green Palace and supported by Chaithanya is the best Ayurveda Centre in Alleppey that offers the best care and cure for our guests. The real experience of Ayurveda in its most natural way will be very much exciting.
Why Ayurveda ?

Ayurveda is the indigenous system of medicine originated in India and now spread to other parts of the globe. It is considered as the mother of all medical sciences due to its holistic and scientific approach to human life and health. Ayurveda is based on the principles of Nature and medicines and treatments are derived from nature and its surroundings. Ayurveda is not just a medical system that handles diseases and treatments, but a complete life science which approaches human life in a different manner. Ayurveda also advises Do’s and Don’ts in our daily life and diet and lifestyle practise to be followed in day to day as well as different seasons for keeping good physical and mental health.

Ayurveda – the science of life – is based on Panchabhootha (Five Elements) theory and principles of Tridosha (three humors). Health is the state when these three humors – Vata, Pitta and Kapha – are in balanced and equilibrium state and disease is the condition when they are imbalanced. Basis of Ayurvedic treatment is to bring back the balance through proper food, lifestyle and medicines. When the imbalance is very high, disease becomes chronic and more advanced procedures like PANCHAKARMA may be required. Panchakarma is the ultimate detox therapy that helps to clean the system, correcting metabolism and regulate immune system. It is carried out in phases of 7, 14, 21 and 28 days preceded by various massage and heating techniques to prepare body for detox proper. Internal medicines that have healing properties are also administered during the course of treatment. They also work as antioxidants and cleansing medicines.

Ayurvedic treatments at chaithanya ayurveda are carried out by qualified and trained therapists under the supervision of Resident Ayurvedic doctors. Therapies are conducted by male and female therapists separately for same sex genders.


Treatments

Treatments are available for
Osteo Arthritis

Osteo arthritis of knee is a degenerative condition of the knee joint ( wear and tear arthritis)affecting either a single joint or both. Mostly seen in aged, especially in women after menopause. .  Read More
Ligament & Tendon Injuries

njuries happening to the soft tissue are very common and long lasting. The damage to the ligaments & tendons include tear or rupture which may be partial or complete. . Read More
Neuro-Muscular Diseases

Neuro Muscular Diseases are the diseases that causes functional impairment of muscles due to neurological issues.  Read More
Lumbar Spondylosis (Low Back Pain)

Lumbar spondylosis is the degenerative arthritis or osteo arthritis of the Spine Diseases Read More
Obesity & Over Weight

Obesity is another major health concern in this era. It is excessive accumulation of body fat resulting severe health problems.  Read More
Lifestyle Disease

It is excessive accumulation of body fat resulting severe health problems.  ReadMore 

The approach of treatment is very much customised based on the assessment of physical and mental constitution and balance of Doshas of the individual.
Doctors
Dr. Rekha S Devi, B.A.M.S, M.D
Dr.Rekha is a post graduate in Ayurveda and the Chief Consultant of Chaitanya Ayurveda. She graduated in Ayurveda i.e. Bachelor in Ayurvedic Medicine & Surgery (B.A.M.S.-Ayurveda acharya) from Govt. Ayurveda College, Thiruvananthapuram
Dr.Vinod Nath, B.A.M.S, M.D
Dr. Vinod N had his Ayurveda Acharya (Bachelor of Ayurvedic Medicine & Surgery- BAMS) From Govt. Ayurveda College, Thiruvananthapuram

WHOLE PROJECT MARK
