The Pursuit of Top Investment Allocation Results

One of the key characteristics of Linden Thomas’s pursuit of top portfolio results is its allocation process. An efficient portfolio is made of several key characteristics intended to maximize results while simultaneously minimizing downside risk. One of the most important components needed when seeking superior long term results is beginning with the pursuit of top asset allocation.

This component, while often discussed, is rarely achieved by investors. The following are some of the key elements in the pursuit of top allocation results:

    Match asset allocation to long term risk and growth objectives
    Implement and maintain proper sector and asset selection
    Minimize unnecessary overlap
    Implement an appropriate re-balancing strategy
    Maintain discipline throughout the market cycle
    Understand and build around the drivers of long term results
    Deploy in an efficient manner
    Understand and build the portfolio to help manage the impact of pullbacks and recovery

To gain a deeper insight into each ingredient in the pursuit of top allocation results, let’s look a little closer at each:

    Match asset allocation to long term risk and growth objectives  In seeking a top allocation portfolio start with your individual goals, growth needs, recovery perspective and ability to assume risk. Properly balancing and planning toward these priorities can lead to shorter recoveries and more consistent results.
    Implement and maintain proper sector and asset selection  An understanding of the benefits and limitations of each investment is vital to proper allocation and investment selection. To position yourself to achieve top allocation results, a process must be used that considers the myriad of factors affecting allocation including their interaction with each other.
    Minimize unnecessary overlap  An efficient asset allocation strategy must be pure; this means that the portfolio must be examined to prevent overlap both on the sector and individual security level. The knowledge and research needed to execute this correctly is often under appreciated by investors which, in turn, can lead to a poorly executed plan or style drift after deployment.
    Implement an appropriate re-balancing strategy  A proper re-balancing strategy, implemented through targeted allocation movements or automation, must be in place if seeking to maintain a top allocation long term. This is because after your initial implementation your allocation may shift, so while at the onset your portfolio might have matched your intended objectives, over time this could change.
    Maintain Discipline throughout the market cycle  Many investors face the urge to chase last year’s results only to later discover they are this year’s disappointment. A well-reasoned and disciplined process is essential when seeking top allocation results as it both keeps you on track and prevents emotions from derailing long term results.
    Understand and build around the drivers of long term results  Past performance is not a basis for allocation decisions; the reason is a strong performance over a short time frame can often impact long term results. While reflection on historical results can be a benefit when measuring historical upside/downside capture risk, basing investment decisions on these past results alone seldom should be used to rotate into tempting areas of the market. Historically the pursuit of top allocation results is built on the recognition that an allocation is about more than past results, or timing; it’s about looking forward not backward.
    Deploy in an efficient manner  Certain investment products can hurt results and get in the way of your pursuit of top allocation results, deployment is about choosing the proper investment vehicles to implement the proper allocation and achieve better results.
    Understand and build a portfolio that focuses on helping to manage the impact of pullbacks and shortening recovery  You must have a thorough understanding of a strategy’s strengths and weaknesses prior to implementation to avoid these pitfalls, as a lack of understanding can lead to concentrating or abandoning an asset class at the wrong time. I believe top allocation results can only be achieved by staying the course, even in volatile markets.

To demonstrate that no two allocations are equal we have constructed two hypothetical 60/40 portfolios. The first is a poorly defined portfolio and the second is a more efficient enhanced portfolio. Many say that less risk is always associated with less returns but historical evidence proves the opposite is true. Below you will notice that not only did the more efficient enhanced portfolio have better long term results but it did so with less risk and a shorter recovery during volatile periods. Both portfolios are built assuming a $2,000,000 starting balance with a 60% allocation to equities and a 40% allocation to fixed income.

Disclaimer: This image does not show actual performance. Past performance is not indicative of future results. The chart is hypothetical and does not represent the actual experience of any client account.

Source: Data obtained through ZephyrstyleADVISOR

Disclaimer: This image does not show actual performance. Past performance is not indicative of future results. The chart is hypothetical and does not represent the actual experience of any client account.

Source: Data obtained through ZephyrstyleADVISOR
A Poorly Allocated 60/40 portfolio
Upside Capture 	77.91%
Downside Capture 	73.21%
Recovery time 	39 months
Enhanced 60/40 Allocation
Upside Capture 	84.31%
Downside Capture 	55.70%
Recovery time 	34 Months

The information above demonstrates three things. First, the enhanced 60/40 portfolio has less risk. We measure this using downside capture, or the amount the portfolio declined in relation to the market. Second, the enhanced 60/40 portfolio had better results. We measure this using upside capture, or the amount the portfolio went up in value when the market was up. The key to these two measures (upside/downside capture) is how they stack up against each other. Put another way, the upside capture is your benefit and the downside capture is the cost to obtain that benefit. Using the information above, in the poorly allocated portfolio you must accept 73% of the downside of the market to receive about 78% of the upside. Compared to the enhanced 60/40 portfolio (56% downside & 84% upside) it is apparent that the net results are far superior to the poorly allocated portfolio. Finally, the enhanced 60/40 portfolio had a shorter recovery. Because shorter recovery is essential to continuing to compound portfolio results, it’s imperative that down market recovery is shortened or minimized. The longer in the valley of poor results, the less likely you will achieve consistent returns. We measure this using recovery time, or when the portfolio declined in value how long it took before it reached its prior peak. In this example the poorly allocated portfolio took 5 more months to recover compared to the enhanced 60/40 portfolio.

Not all portfolios are created equal which can lead to dramatically different results. To see if your portfolio is properly aligned with your goals, and understand exactly what could be hindering your ability to achieve your desired results. To schedule a no-cost, no-obligation consultation, Contact Us or:

Private Wealth Manager – Charlotte, North Carolina

So when Thomas decided to go out on his own, he had a wealth of knowledge on which to base his new venture. Thomas envisioned an enterprise operating free of any conflicts of interest between its own goals and its clients’ needs, and he sought to build an organization that could deliver it. This became the genesis of what is known today as Linden Thomas and Company, an independent financial consulting firm headquartered in Charlotte, NC. Linden Thomas and Company strives to be an outstanding firm and, more importantly, an exceptional partner, assessing and addressing the goals and objectives of its clients.
private wealth manager

Independent Financial Advisor
“Our primary focus is our clients’ needs,” says Thomas, managing principal of the firm. “Everything we do is designed to meet those needs, putting the client first. And to us it is absolutely essential to do this with a high degree of ethics and morals, by both treating the client right and doing what we ourselves know is right. That’s not always easy in such a competitive industry.”

Since its formation, Linden Thomas and Company has evolved into a cohesive group of financial professionals focused on a single objective: to strive to provide the very best investment results and superior service to its clients. The combination results in an organization that is committed to maintaining its independence and delivering the best possible financial results enabling the company to help serve its client’s needs. We utilize a team approach that’s not centered on selling products, but structured holistically around clients, building unique portfolios to fit their particular circumstances.

Linden Thomas and Company is comprised of 20 accomplished professionals with a combined experience of over 100 years managing assets. Well versed in all of the latest industry technology, the firm is especially adept at building fixed income portfolios based on the net cash flow needs of the individual.

We’re likewise proficient at searching for equity managers that present the least amount of risk while helping meeting the client’s growth goals. This is made possible by employing an in-house team that monitors and researches equity managers, focusing on items such as performance, risk and buy-sell discipline. Linden Thomas built this team to allow portfolios to evolve as clients’ needs change over the years.
Independent Investment Consulting Firm

Independence offers Linden Thomas a number of advantages. “At big retail firms I was always hindered from having a solid client support infrastructure,” says Thomas. “A significant part of a large firm’s revenues go to support the firm’s own infrastructure which had little to no net benefit to its clients.

Now, because we’re independent private wealth managers and don’t have that additional overhead, we can use those revenues however we see fit. We choose to spend them on systems, technology and staff in order to better serve our clients and ultimately, to help build better portfolios.”

Our size is a definite plus – big enough to offer an extensive variety of portfolio analytics and other related services, yet small enough to provide personal attention. “We have a client-driven business model, and as it turns out, this ends up working better for us,” says Thomas.

Linden Thomas and Company is fortunate to be experiencing growth and continually adding talented staff. Another direct result of its success is its involvement with various ministries and charities. This is a priority of our firm, which gives away 10 percent of our revenues to causes that are in need. Because Stephen Thomas grew up in a family of little means, he believes that giving back to those in need is a foundation of anyone who has been blessed with abundance. To whom much has been given, much is required! Because of this, Linden Thomas gives a percent of their monthly revenues to several charities and ministries committed to helping those in need, especially children that are underprivileged.

With enhanced client service and integrity as its hallmarks, Linden Thomas and Company has assembled a team of talented individuals whose only priority is the fulfillment of our clients’ financial goals.

WHOLE PROJECT MARK
