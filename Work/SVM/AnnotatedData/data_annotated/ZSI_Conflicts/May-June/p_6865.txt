PROJECT NAME: GNUnet
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

GNUnet | GNU's Framework for Secure Peer-to-Peer Networking
===============================

GNUnet | GNU's Framework for Secure Peer-to-Peer Networking

 Skip to main content 

 GNU's Framework for Secure Peer-to-Peer Networking   

 Main menu Download 
 Report bugs 
 Installation Handbook 
 User Handbook 
 GNUnet e.V. 

 Navigation 

 Contact Impressum 
 Team 

 Downloads 
 Documentation Philosophy 
 Installation Handbook 
 User Handbook 
 Developer Handbook 
 Related Projects 

 Developer Corner Bugtracking (Mantis) 
 Code Coverage 
 Annotated Source Code (Doxygen) 
 Performance (Gauger) 
 Portability (buildbot) 
 Source Code Access (Git) 

 Bibliography Publications about GNUnet 
 Conferences of Interest 
 Publications by Author 
 Publications by Keyword 

 Recent posts Blogs 
 Feed aggregator Categories 
 Sources 

 IRC Logs 

 User login 

 Log in using OpenID  

 What is OpenID? 

 Username  * 

 Password  * 

 Log in using OpenID 
 Cancel OpenID login 
 Request new password 

 About GNUnet 

 GNUnet is an alternative network stack for building secure, decentralized and privacy-preserving distributed applications.  Our goal is to replace the old insecure Internet protocol stack.  Starting from an application for secure publication of files, it has grown to include all kinds of basic protocol components and applications towards the creation of a GNU internet. 
 GNUnet is an official GNU package. GNUnet can be downloaded from  GNU  ( ftp ) and  the GNU mirrors  ( ftp ). 

 Read more  about About GNUnet 
 Deutsch 
 Espa ol 

 Fran ais 

 Toward secure name resolution on the internet 
 Thu, 03/15/2018 - 17:54    Christian Grothoff 

 Grothoff C ,  Wachs M ,  Ermert M ,  Appelbaum J .  Toward secure name resolution on the internet . Computers   Security [Internet]. 2018  . Available from:  http://www.sciencedirect.com/science/article/pii/S0167404818300403   

 Read more  about Toward secure name resolution on the internet 

 Google Scholar 
 DOI 
 BibTex 
 RTF 
 Tagged 
 XML 
 RIS 

 gnURL 7.58.0 
 Sat, 01/27/2018 - 15:48    ng0 

 I'm no longer publishing release announcements on gnunet.org. Read the full  gnURL 7.58.0 release announcement  on our developer mailinglist and on  info-gnu  once my email has passed the moderation. 

 Read more  about gnURL 7.58.0 
 ng0's blog 

 GNUnet e.V. annual meeting 

 Dear all, 
 As every year, we'll be having the annual meeting of GNUnet e.V. at the CCC. All members are invited to join us at the "Komona Blue Princess" room at 16:00 on December 27th at Messe Leipzig (Germany). The agenda includes:   

 Read more  about GNUnet e.V. annual meeting 

 gnURL 7.57.0 released 
 Wed, 12/06/2017 - 16:56    ng0 

 Today gnURL has been released in version 7.57.0, following the release of cURL 7.57.0. 
 The download is available in our directory on the GNU FTP and FTP mirrors ( /gnu/gnunet/ ). 7.57.0 is the last version that will be available at  https://gnunet.org/gnurl , future releases will be on the FTP. 
If you are a distro maintainer for gnURL make sure to read the whole post with details below.   

 Read more  about gnURL 7.57.0 released 
 ng0's blog 

 The GNUnet System 
 Sun, 12/03/2017 - 14:52    Christian Grothoff 

 Grothoff C .  The GNUnet System . Informatique [Internet]. 2017  ;HDR:181. Available from:  https://grothoff.org/christian/habil.pdf   

 Read more  about The GNUnet System 

 Google Scholar 
 DOI 
 BibTex 
 RTF 
 Tagged 
 XML 
 RIS 

 Values of Internet Technologies (VIT) - Announcement and Call for Donations 

 The Internet Society Switzerland Chapter (ISOC-CH) and the Swiss p p Foundation are proud to announce the first workshop out of six around  Values of Internet Technologies (VIT) . This first workshop will focus on Decentralization. Privacy, trust and security are at stake   in a pure technical sense as well as economically and socially. Decentralization is a significant feature for more agility, justice and resilience in our societies and their digital infrastructure.     

 Read more  about Values of Internet Technologies (VIT) - Announcement and Call for Donations 

 gnURL 7.56.1-2 released 
 Mon, 11/27/2017 - 19:42    ng0 

 Today gnURL has been released in version 7.56.1-2. 
 This is a first rough solution to make gnURL build without a long list of configure switches. 
This release fixes https://gnunet.org/bugs/view.php?id=5143 
The download is available at the usual place, https://gnunet.org/gnurl 

 Read more  about gnURL 7.56.1-2 released 
 ng0's blog 

 Berlin 2018, Workshop for a Next Generation Internet 
 Wed, 11/01/2017 - 20:49    ng0 

 To whom it may concern beyond just being affected... 
 We --an association of secushare and GNUnet hackers-- would like to 
invite you after 34C3, to Berlin, to discuss and hack 
"Next Generation Internet" technologies. The kind that actually replaces 
parts of the existing Internet, not just adds complexity to it... 
     Start: Friday, 5th of January, 2018 
      End: Sunday, 7th of January, 2018 
 Location: Onionspace, Gottschedstra e 4, Aufgang 4, 13357 Berlin 

 Read more  about Berlin 2018, Workshop for a Next Generation Internet 
 ng0's blog 

 gnURL 7.56.1 released 
 Tue, 10/24/2017 - 20:25    ng0 

 Today gnURL has been released in version 7.56.1. 
 This merges cURL 7.56.1 from the upstream release into gnURL. 
For more info you can read the git log or the generated CHANGELOG file (present in the tarball). 

 Read more  about gnURL 7.56.1 released 
 ng0's blog 

 Pages 1 
 2 
 3 
 4 
 5 
 6 
 7 
 8 
 9 

 next  
 last  

 Hosted By 

 Funded by 

 Previous funding 

GNUnet | GNU's Framework for Secure Peer-to-Peer Networking
===============================

GNUnet | GNU's Framework for Secure Peer-to-Peer Networking

 Skip to main content 

 GNU's Framework for Secure Peer-to-Peer Networking   

 Main menu Download 
 Report bugs 
 Installation Handbook 
 User Handbook 
 GNUnet e.V. 

 Navigation 

 Contact Impressum 
 Team 

 Downloads 
 Documentation Philosophy 
 Installation Handbook 
 User Handbook 
 Developer Handbook 
 Related Projects 

 Developer Corner Bugtracking (Mantis) 
 Code Coverage 
 Annotated Source Code (Doxygen) 
 Performance (Gauger) 
 Portability (buildbot) 
 Source Code Access (Git) 

 Bibliography Publications about GNUnet 
 Conferences of Interest 
 Publications by Author 
 Publications by Keyword 

 Recent posts Blogs 
 Feed aggregator Categories 
 Sources 

 IRC Logs 

 User login 

 Log in using OpenID  

 What is OpenID? 

 Username  * 

 Password  * 

 Log in using OpenID 
 Cancel OpenID login 
 Request new password 

 About GNUnet 

 GNUnet is an alternative network stack for building secure, decentralized and privacy-preserving distributed applications.  Our goal is to replace the old insecure Internet protocol stack.  Starting from an application for secure publication of files, it has grown to include all kinds of basic protocol components and applications towards the creation of a GNU internet. 
 GNUnet is an official GNU package. GNUnet can be downloaded from  GNU  ( ftp ) and  the GNU mirrors  ( ftp ). 

 Read more  about About GNUnet 
 Deutsch 
 Espa ol 

 Fran ais 

 Toward secure name resolution on the internet 
 Thu, 03/15/2018 - 17:54    Christian Grothoff 

 Grothoff C ,  Wachs M ,  Ermert M ,  Appelbaum J .  Toward secure name resolution on the internet . Computers   Security [Internet]. 2018  . Available from:  http://www.sciencedirect.com/science/article/pii/S0167404818300403   

 Read more  about Toward secure name resolution on the internet 

 Google Scholar 
 DOI 
 BibTex 
 RTF 
 Tagged 
 XML 
 RIS 

 gnURL 7.58.0 
 Sat, 01/27/2018 - 15:48    ng0 

 I'm no longer publishing release announcements on gnunet.org. Read the full  gnURL 7.58.0 release announcement  on our developer mailinglist and on  info-gnu  once my email has passed the moderation. 

 Read more  about gnURL 7.58.0 
 ng0's blog 

 GNUnet e.V. annual meeting 

 Dear all, 
 As every year, we'll be having the annual meeting of GNUnet e.V. at the CCC. All members are invited to join us at the "Komona Blue Princess" room at 16:00 on December 27th at Messe Leipzig (Germany). The agenda includes:   

 Read more  about GNUnet e.V. annual meeting 

 gnURL 7.57.0 released 
 Wed, 12/06/2017 - 16:56    ng0 

 Today gnURL has been released in version 7.57.0, following the release of cURL 7.57.0. 
 The download is available in our directory on the GNU FTP and FTP mirrors ( /gnu/gnunet/ ). 7.57.0 is the last version that will be available at  https://gnunet.org/gnurl , future releases will be on the FTP. 
If you are a distro maintainer for gnURL make sure to read the whole post with details below.   

 Read more  about gnURL 7.57.0 released 
 ng0's blog 

 The GNUnet System 
 Sun, 12/03/2017 - 14:52    Christian Grothoff 

 Grothoff C .  The GNUnet System . Informatique [Internet]. 2017  ;HDR:181. Available from:  https://grothoff.org/christian/habil.pdf   

 Read more  about The GNUnet System 

 Google Scholar 
 DOI 
 BibTex 
 RTF 
 Tagged 
 XML 
 RIS 

 Values of Internet Technologies (VIT) - Announcement and Call for Donations 

 The Internet Society Switzerland Chapter (ISOC-CH) and the Swiss p p Foundation are proud to announce the first workshop out of six around  Values of Internet Technologies (VIT) . This first workshop will focus on Decentralization. Privacy, trust and security are at stake   in a pure technical sense as well as economically and socially. Decentralization is a significant feature for more agility, justice and resilience in our societies and their digital infrastructure.     

 Read more  about Values of Internet Technologies (VIT) - Announcement and Call for Donations 

 gnURL 7.56.1-2 released 
 Mon, 11/27/2017 - 19:42    ng0 

 Today gnURL has been released in version 7.56.1-2. 
 This is a first rough solution to make gnURL build without a long list of configure switches. 
This release fixes https://gnunet.org/bugs/view.php?id=5143 
The download is available at the usual place, https://gnunet.org/gnurl 

 Read more  about gnURL 7.56.1-2 released 
 ng0's blog 

 Berlin 2018, Workshop for a Next Generation Internet 
 Wed, 11/01/2017 - 20:49    ng0 

 To whom it may concern beyond just being affected... 
 We --an association of secushare and GNUnet hackers-- would like to 
invite you after 34C3, to Berlin, to discuss and hack 
"Next Generation Internet" technologies. The kind that actually replaces 
parts of the existing Internet, not just adds complexity to it... 
     Start: Friday, 5th of January, 2018 
      End: Sunday, 7th of January, 2018 
 Location: Onionspace, Gottschedstra e 4, Aufgang 4, 13357 Berlin 

 Read more  about Berlin 2018, Workshop for a Next Generation Internet 
 ng0's blog 

 gnURL 7.56.1 released 
 Tue, 10/24/2017 - 20:25    ng0 

 Today gnURL has been released in version 7.56.1. 
 This merges cURL 7.56.1 from the upstream release into gnURL. 
For more info you can read the git log or the generated CHANGELOG file (present in the tarball). 

 Read more  about gnURL 7.56.1 released 
 ng0's blog 

 Pages 1 
 2 
 3 
 4 
 5 
 6 
 7 
 8 
 9 

 next  
 last  

 Hosted By 

 Funded by 

 Previous funding 

GNUnet | GNU's Framework for Secure Peer-to-Peer Networking
===============================

GNUnet | GNU's Framework for Secure Peer-to-Peer Networking

 Skip to main content 

 GNU's Framework for Secure Peer-to-Peer Networking   

 Main menu Download 
 Report bugs 
 Installation Handbook 
 User Handbook 
 GNUnet e.V. 

 Navigation 

 Contact Impressum 
 Team 

 Downloads 
 Documentation Philosophy 
 Installation Handbook 
 User Handbook 
 Developer Handbook 
 Related Projects 

 Developer Corner Bugtracking (Mantis) 
 Code Coverage 
 Annotated Source Code (Doxygen) 
 Performance (Gauger) 
 Portability (buildbot) 
 Source Code Access (Git) 

 Bibliography Publications about GNUnet 
 Conferences of Interest 
 Publications by Author 
 Publications by Keyword 

 Recent posts Blogs 
 Feed aggregator Categories 
 Sources 

 IRC Logs 

 User login 

 Log in using OpenID  

 What is OpenID? 

 Username  * 

 Password  * 

 Log in using OpenID 
 Cancel OpenID login 
 Request new password 

 About GNUnet 

 GNUnet is an alternative network stack for building secure, decentralized and privacy-preserving distributed applications.  Our goal is to replace the old insecure Internet protocol stack.  Starting from an application for secure publication of files, it has grown to include all kinds of basic protocol components and applications towards the creation of a GNU internet. 
 GNUnet is an official GNU package. GNUnet can be downloaded from  GNU  ( ftp ) and  the GNU mirrors  ( ftp ). 

 Read more  about About GNUnet 
 Deutsch 
 Espa ol 

 Fran ais 

 Toward secure name resolution on the internet 
 Thu, 03/15/2018 - 17:54    Christian Grothoff 

 Grothoff C ,  Wachs M ,  Ermert M ,  Appelbaum J .  Toward secure name resolution on the internet . Computers   Security [Internet]. 2018  . Available from:  http://www.sciencedirect.com/science/article/pii/S0167404818300403   

 Read more  about Toward secure name resolution on the internet 

 Google Scholar 
 DOI 
 BibTex 
 RTF 
 Tagged 
 XML 
 RIS 

 gnURL 7.58.0 
 Sat, 01/27/2018 - 15:48    ng0 

 I'm no longer publishing release announcements on gnunet.org. Read the full  gnURL 7.58.0 release announcement  on our developer mailinglist and on  info-gnu  once my email has passed the moderation. 

 Read more  about gnURL 7.58.0 
 ng0's blog 

 GNUnet e.V. annual meeting 

 Dear all, 
 As every year, we'll be having the annual meeting of GNUnet e.V. at the CCC. All members are invited to join us at the "Komona Blue Princess" room at 16:00 on December 27th at Messe Leipzig (Germany). The agenda includes:   

 Read more  about GNUnet e.V. annual meeting 

 gnURL 7.57.0 released 
 Wed, 12/06/2017 - 16:56    ng0 

 Today gnURL has been released in version 7.57.0, following the release of cURL 7.57.0. 
 The download is available in our directory on the GNU FTP and FTP mirrors ( /gnu/gnunet/ ). 7.57.0 is the last version that will be available at  https://gnunet.org/gnurl , future releases will be on the FTP. 
If you are a distro maintainer for gnURL make sure to read the whole post with details below.   

 Read more  about gnURL 7.57.0 released 
 ng0's blog 

 The GNUnet System 
 Sun, 12/03/2017 - 14:52    Christian Grothoff 

 Grothoff C .  The GNUnet System . Informatique [Internet]. 2017  ;HDR:181. Available from:  https://grothoff.org/christian/habil.pdf   

 Read more  about The GNUnet System 

 Google Scholar 
 DOI 
 BibTex 
 RTF 
 Tagged 
 XML 
 RIS 

 Values of Internet Technologies (VIT) - Announcement and Call for Donations 

 The Internet Society Switzerland Chapter (ISOC-CH) and the Swiss p p Foundation are proud to announce the first workshop out of six around  Values of Internet Technologies (VIT) . This first workshop will focus on Decentralization. Privacy, trust and security are at stake   in a pure technical sense as well as economically and socially. Decentralization is a significant feature for more agility, justice and resilience in our societies and their digital infrastructure.     

 Read more  about Values of Internet Technologies (VIT) - Announcement and Call for Donations 

 gnURL 7.56.1-2 released 
 Mon, 11/27/2017 - 19:42    ng0 

 Today gnURL has been released in version 7.56.1-2. 
 This is a first rough solution to make gnURL build without a long list of configure switches. 
This release fixes https://gnunet.org/bugs/view.php?id=5143 
The download is available at the usual place, https://gnunet.org/gnurl 

 Read more  about gnURL 7.56.1-2 released 
 ng0's blog 

 Berlin 2018, Workshop for a Next Generation Internet 
 Wed, 11/01/2017 - 20:49    ng0 

 To whom it may concern beyond just being affected... 
 We --an association of secushare and GNUnet hackers-- would like to 
invite you after 34C3, to Berlin, to discuss and hack 
"Next Generation Internet" technologies. The kind that actually replaces 
parts of the existing Internet, not just adds complexity to it... 
     Start: Friday, 5th of January, 2018 
      End: Sunday, 7th of January, 2018 
 Location: Onionspace, Gottschedstra e 4, Aufgang 4, 13357 Berlin 

 Read more  about Berlin 2018, Workshop for a Next Generation Internet 
 ng0's blog 

 gnURL 7.56.1 released 
 Tue, 10/24/2017 - 20:25    ng0 

 Today gnURL has been released in version 7.56.1. 
 This merges cURL 7.56.1 from the upstream release into gnURL. 
For more info you can read the git log or the generated CHANGELOG file (present in the tarball). 

 Read more  about gnURL 7.56.1 released 
 ng0's blog 

 Pages 1 
 2 
 3 
 4 
 5 
 6 
 7 
 8 
 9 

 next  
 last  

 Hosted By 

 Funded by 

 Previous funding 

 GNUnet is a framework for secure peer-to-peer networking that does not use any centralized or otherwise trusted services. Our high-level goal is to provide a strong free software foundation for a global network that provides security and in particular respects privacy.

GNUnet started with an idea for anonymous censorship-resistant file-sharing, but has grown to incorporate other applications as well as many generic building blocks for secure networking applications. In particular, GNUnet now includes the GNU Name System, a privacy-preserving, decentralized public key infrastructure. 

GNUnet is a framework for secure peer-to-peer networking that does not use any centralized or otherwise trusted services. Our high-level goal is to provide a strong free software foundation for a global network that provides security and in particular respects privacy.

GNUnet started with an idea for anonymous censorship-resistant file-sharing, but has grown to incorporate other applications as well as many generic building blocks for secure networking applications. In particular, GNUnet now includes the GNU Name System, a privacy-preserving, decentralized public key infrastructure.
 WHOLE PROJECT MARK