PROJECT NAME: The Common House
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 The Common House is an experiment in building a commons – by which we mean a resource that is organised and structured by our collective activity as a community and not by money or property rights. We always want new groups and people to get involved in running the space. It is only people, together, that can create commons.

In 2013 we got together and collectively rented The Common House because we had a common problem. Most spaces in London cost too much, were not open the hours we needed and we wanted to have food and kids at our meetings. We also wanted a space to have parties in, eat together, watch films, make banners, props and we needed a home for our printing press.

By imagining The Common House as a commons we are able think about how to manage and reproduce the resources that we need. We can imagine these resources not as objects, commodities or things we own – but instead as processes of our activity and labour. The Common House is not just a space to be used or consumed, but is a collective attempt to organise and maintain infrastructure and resources for radical ideas and practices. A commons is different to private space (individually owned) and public space (state maintained). It is maintained by the people who use it. It has members, not consumers.

Commons are not open in the sense that anyone or anything goes. Instead, it allows us to think about how to reproduce what we need within the constraints that we face and to manage resources outside of the regimes of both the state and capital. The commons are a pre-condition for, and an experiment in, building struggles capable of winning and bringing about the world that we want: one that is collectively owned and decided. 

The Common House is an experiment in building a commons – by which we mean a resource that is organised and structured by our collective activity as a community and not by money or property rights. We always want new groups and people to get involved in running the space. It is only people, together, that can create commons.

In 2013 we got together and collectively rented The Common House because we had a common problem. Most spaces in London cost too much, were not open the hours we needed and we wanted to have food and kids at our meetings. We also wanted a space to have parties in, eat together, watch films, make banners, props and we needed a home for our printing press.

By imagining The Common House as a commons we are able think about how to manage and reproduce the resources that we need. We can imagine these resources not as objects, commodities or things we own – but instead as processes of our activity and labour. The Common House is not just a space to be used or consumed, but is a collective attempt to organise and maintain infrastructure and resources for radical ideas and practices. A commons is different to private space (individually owned) and public space (state maintained). It is maintained by the people who use it. It has members, not consumers.

Commons are not open in the sense that anyone or anything goes. Instead, it allows us to think about how to reproduce what we need within the constraints that we face and to manage resources outside of the regimes of both the state and capital. The commons are a pre-condition for, and an experiment in, building struggles capable of winning and bringing about the world that we want: one that is collectively owned and decided.


Who’s Involved?

Common House is a collectively managed space. Currently it is organised by the following groups and projects (some groups need to remain anonymous due to safety and security).
XTalk
The x:talk project
The x:talk project is a sex worker-led workers co-operative which approaches language teaching as knowledge sharing between equals and regards the ability to communicate as a fundamental tool for sex workers to work in safer conditions, to organise and to socialise with each other.
Screen Shot 2016-07-18 at 13.12.50Feminist Fightback

We’re inspired by the politics of a range of anti-capitalist feminist struggles, and believe that no single oppression can be challenged in isolation from all other forms of exploitation that intersect with it. We are committed to fighting for a feminist perspective and awareness of gender issues everywhere in our movement – not marginalising ‘women’s rights’ as a separate issue.

Screen Shot 2017-02-17 at 21.27.19Sex Worker Open University                                                                        SWOU is a project created by and for sex workers. You might be working as an escort, rent boy, porn actress/actor, professional dominatrix or submissive, cam model, erotic masseuse, sexual healer or street worker; this is a place to socialise, learn new skills, and create events together. Our aim is to empower our community through workshops, debates, actions and art projects as well as fighting against our criminalisation.

1662680_1495244670735001_2639875404248054563_nPlan C London
Plan C is an organisation of people who are politically active in their workplaces and communities. We work together to support each other, amplify our struggles and think strategically. We want to go beyond network-based organisation, without falling back on the model of a party. We are committed to ongoing experimentation to find the forms of collective activity needed to build a world beyond capitalism.

Screen Shot 2017-02-17 at 21.26.18Radical Education Forum
The RadEd Forum is a group of people working in a wide range of educational settings who meet monthly to discuss radical pedagogical theories and techniques, and contemporary issues of interest to those involved in education. We are interested in how these theories and questions can inform our practice. RadEd has published the Radical Education Workbook, available for free from the website.

282857_181438895370772_1044250154_nNo Fly on the Wall
No Fly on the WALL is a platform for Women of Colour and other marginalized groups, aiming to offer a fresh intersectional perspective on Feminism today. With an Intersectional Feminist focus and praxis, we provide thoughtful, woke, engaging, and thought-provoking content on our website and run workshops and masterclasses and hold events throughout the year via our No Fly on the WALL Academy.

 

crypt page 1Autonomous Tech Fetish
Autonomous Tech Fetish (ATF) is an open space for gathering, sharing and making. ATF runs a monthly Make & Do club and hosts artists and technologist workshops that look at topics such as encryption, biometrics and health policy through making, embodied learning and open discussion.

 

Screen Shot 2017-02-17 at 21.28.32Politics in Love, Sex and Relationships
A monthly discussion, peer support, and solidarity group (usually last Sunday of each month) concentrating on thinking and practicing relationships “differently” (that usually means we talk a lot about relationship anarchy, ethical non-monogamy, relationships that don’t conform to the “relationship escalator” idea or other ideas promoted by the capitalist heteropatriarchy.)

Screen Shot 2017-02-17 at 21.23.51East London Radical Assembly
Radical Assembly meets to plan action to link and strengthen community-led, working class, liberation and social justice campaigns. The assembly aims to actively contribute to building a truly democratic, bottom-up movement that supports the diversity of existing struggles while respecting their independence. Out of the assemblies have come a network of local groups all now taking action and linking up existing campaigns across London, on issues such as housing, college closures, immigration detention, abortion rights and homelessness.

Action East End 
Action East End is a local group based in East London that wants to contribute to resistance, both defending ourselves in our communities and workplaces, and ultimately helping co-ordinate activities to improve our quality of life. We publish, The Howler, a community newspaper.

 

Babels BlessingBabel’s Blessing
Radical London-based language school running fun, affordable classes in Yiddish, Arabic, Hebrew, Sign Language & other languages, and also free English classes for migrants and refugees.

 

Screen Shot 2014-12-29 at 15.09.05 In Sight Theatre
We are a mixed learning disabled and non learning disabled acting troupe consisting of 8 adults. Our goal at In Sight Theatre is to provide talented disabled artists with the chance to launch their careers through performing at mainstream venues and festivals and break down barriers between disabled and non disabled performers on stage.

Screen Shot 2016-07-18 at 13.03.39London Gender Support
LGS is a non-professional, not-for-profit organisation whose sole purpose is to support people in their journeys of self-discovery and gender exploration. Anyone is welcome to attend an LGS group meeting, no matter how early or how late into their gender journey and where they see themselves in the grand gender spectrum.

 

 
Regular community services and projects that happen at Common House (check the links for more info and the calender for updates):

Screen Shot 2017-02-17 at 21.30.56Community Massage Project – The Community Massage Project provides high quality massage therapy on a sliding-scale fee basis to the local community, enabling more people to access regular massage. Our sliding-scale is from (high income) £45 -£25 (unwaged) for one hour and (high income) £20 – £10 (unwaged) for 30 minutes

We hold clinics twice each month on the second Thursday evening and
third Tuesday afternoon of each month at Common House, Unit 5E, Pundersons Gardens, Bethnal Green, E2 9QG.

We offer various types of bodywork – Deep Tissue Massage, Sports Massage, Holistic Massage, Indian Head Massage, Pregnancy Massage and Aromatherapy andThai Yoga Massage which is through clothes.

More details can be found on our website:
communitymassageproject.wordpress.com

For bookings or more information contact:
communitymassageproject@riseup.net
https://www.facebook.com/groups/352139881658960/

 

Screen Shot 2015-07-29 at 00.10.56Riso Print Club – currently not running
Riso Print Club is an affiliation of printmakers who are asking the question, “What constitutes a radical print resource?” We aim to counter the norm of ‘contracting out’ print and design, and see it as a playful way for activist groups to explore their politics further through the visual language used to communicate. Introduction sessions for those new to risograph printers will happen in the first hour.

Screen Shot 2015-04-02 at 23.02.45

Numerous film clubs run by different groups (Feminist Fightback Film Club, Radical Education Forum Film Club, East London Capitalist Propaganda Appreciation Society, …)

 

 
The following are some of the groups and projects that have used the Common House since its opening in May 2013:

Novara, Liberate Tate, Occupy London, Right to Remain, Ain’t I a Woman Collective, Critisticuffs, BP or not BP, Food not Bombs London, No One is Illegal, Radical London, Tower Hamlets Jenin Friendship Association, AltGen, Empty Cages Collective, Fossil Free London, Gaza fundraiser, London Birth Gathering, Jewdas, AntiUniversity, Caketastic, Dost Center for Young Refugees and Migrants, Queerspace East, Friends of Joiners Arms campaign, Radical Housing Network, gng Black Queer Women in London, ACT UP London, Kids Yoga, State of Education, Political Action Reflection Group, Capital reading group, Commons Course, People and Planet, Wendy Brown School, Radical Retreat, Coalition of Grassroots Educators, um’s Ganze!, Green and Black Cross, Breaking the Frame, Fuel Poverty Action, DIY Space for London, National Coalition of Anti-Deportations Campaigns, AFed Day School, Radical Anthropology, Unite the Youth, Trauma and Wellbeing Group, SolFed, Black Rose, Self Defense Class, Nanopolitics Group, Icarus Project, Frack Off London, Coffee in Common, various talks, book launches and fundraising parties.
If you are a group or project that is looking for a permanent home please get in touch! info@commonhouse.org.uk

 WHOLE PROJECT MARK
