PROJECT NAME: Debian
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

Debian -- The Universal Operating System 
===============================

Debian -- The Universal Operating System 

 Skip Quicknav 

 About Debian 
 Getting Debian 
 Support 
 Developers' Corner 

 Download Debian 9.4 (64-bit PC Network installer)   

 Debian 

 Debian  is a
 free  operating system (OS) for your computer.
An operating system is the set of basic programs and utilities that make
your computer run.

 Debian provides more than a pure OS: it comes with over
51000  packages , precompiled software bundled
up in a nice format for easy installation on your machine.  Read more... 

 About 

 Social Contract 
 Code of Conduct 
 Free Software 
 Partners 
 Donations 
 Contact Us 

 Help Debian 

 Getting Debian 

 Network install 
 CD/USB ISO images 
 CD vendors 
 Pre-installed 

 Pure Blends 
 Debian Packages 
 Developers' Corner 

 News 

 Project News 
 Events 

 Documentation 

 Release Info 
 Installation manual 
 Debian Books 
 Debian Wiki 

 Support 

 Debian International 
 Security Information 
 Bug reports 
 Mailing Lists 
 Mailing List Archives 
 Ports/Architectures 

 Miscellaneous 
 Site map 
 Search 
 The Debian Blog 

 The  latest stable release of Debian  is
9.4. The last update to this release was made on
March 10th, 2018. Read more about  available
versions of Debian . 
 Getting Started 

 If you'd like to start using Debian, you can easily  obtain a copy , and then follow the  installation instructions  to
  install it. 
 If you're upgrading to the latest stable release
  from a previous version, please read  the release notes  before
  proceeding. 
 To get help in using or setting up Debian, see
  our  documentation  and  support 
  pages. 
 Users that speak languages other than English should
  check the  international  section. 
 People who use systems other than Intel x86 should check the  ports  section. 

 RSS 
 News 
 [10 Mar 2018]   Updated Debian 9: 9.4 released 
 [09 Dec 2017]   Updated Debian 8: 8.10 released 
 [09 Dec 2017]   Updated Debian 9: 9.3 released 
 [07 Oct 2017]   Updated Debian 9: 9.2 released 
 [12 Aug 2017]   DebConf17 closes in Montreal and DebConf18 dates announced 
 [22 Jul 2017]   Updated Debian 9: 9.1 released 

 For older news items see the  News Page .
If you would like to receive mail whenever new Debian news comes out, subscribe to the
 debian-announce mailing list . 

 RSS 
 Security Advisories 
 [18 Mar 2018]   DSA-4145 gitlab  - security update  
 [17 Mar 2018]   DSA-4144 openjdk-8  - security update  
 [17 Mar 2018]   DSA-4143 firefox-esr  - security update  
 [17 Mar 2018]   DSA-4142 uwsgi  - security update  
 [16 Mar 2018]   DSA-4141 libvorbisidec  - security update  
 [16 Mar 2018]   DSA-4140 libvorbis  - security update  
 [15 Mar 2018]   DSA-4139 firefox-esr  - security update  
 [15 Mar 2018]   DSA-4138 mbedtls  - security update  
 [14 Mar 2018]   DSA-4137 libvirt  - security update  
 [14 Mar 2018]   DSA-4136 curl  - security update  
 [13 Mar 2018]   DSA-4135 samba  - security update  
 [10 Mar 2018]   DSA-4134 util-linux  - security update  
 [07 Mar 2018]   DSA-4133 isc-dhcp  - security update  

 For older security advisories see the  
Security Page .
If you would like to receive security advisories as soon as they're announced, subscribe to the
 debian-security-announce
mailing list . 

This page is also available in the following languages:

 (Arabiya) 
 (B lgarski) 
 catal 
 esky 
 dansk 
 Deutsch 
 (Ellinika) 
 espa ol 
 Esperanto 
 #x0641; #x0627; #x0631; #x0633; #x06cc; (Farsi) 
 fran ais 
 Galego 
 (hayeren) 
 hrvatski 
 Indonesia 
 Italiano 
 (ivrit) 
 (Korean) 
 Lietuvi 
 magyar 
 Nederlands 
 (Nihongo) 
 norsk (bokm l) 
 polski 
 Portugu s 
 rom n 
 (Russkij) 
 sloven ina 
 suomi 
 svenska 
 (Tamil) 
 Ti ng Vi t 
 T rk e 
 (ukrajins'ka) 
 ( ) 
 (HK) 
 ( ) 

How to set  the default document language 

 To report a problem with the web site, e-mail our publicly archived mailing list  debian-www@lists.debian.org . For other contact information, see the Debian  contact page . Web site source code is  available . 

Last Modified: Thu, Jun 8 15:25:09 UTC 2017

  Copyright   1997-2017
  SPI  and others; See  license terms 
  Debian is a registered  trademark  of Software in the Public Interest, Inc.

Debian -- The Universal Operating System 
===============================

Debian -- The Universal Operating System 

 Skip Quicknav 

 About Debian 
 Getting Debian 
 Support 
 Developers' Corner 

 Download Debian 9.4 (64-bit PC Network installer)   

 Debian 

 Debian  is a
 free  operating system (OS) for your computer.
An operating system is the set of basic programs and utilities that make
your computer run.

 Debian provides more than a pure OS: it comes with over
51000  packages , precompiled software bundled
up in a nice format for easy installation on your machine.  Read more... 

 About 

 Social Contract 
 Code of Conduct 
 Free Software 
 Partners 
 Donations 
 Contact Us 

 Help Debian 

 Getting Debian 

 Network install 
 CD/USB ISO images 
 CD vendors 
 Pre-installed 

 Pure Blends 
 Debian Packages 
 Developers' Corner 

 News 

 Project News 
 Events 

 Documentation 

 Release Info 
 Installation manual 
 Debian Books 
 Debian Wiki 

 Support 

 Debian International 
 Security Information 
 Bug reports 
 Mailing Lists 
 Mailing List Archives 
 Ports/Architectures 

 Miscellaneous 
 Site map 
 Search 
 The Debian Blog 

 The  latest stable release of Debian  is
9.4. The last update to this release was made on
March 10th, 2018. Read more about  available
versions of Debian . 
 Getting Started 

 If you'd like to start using Debian, you can easily  obtain a copy , and then follow the  installation instructions  to
  install it. 
 If you're upgrading to the latest stable release
  from a previous version, please read  the release notes  before
  proceeding. 
 To get help in using or setting up Debian, see
  our  documentation  and  support 
  pages. 
 Users that speak languages other than English should
  check the  international  section. 
 People who use systems other than Intel x86 should check the  ports  section. 

 RSS 
 News 
 [10 Mar 2018]   Updated Debian 9: 9.4 released 
 [09 Dec 2017]   Updated Debian 8: 8.10 released 
 [09 Dec 2017]   Updated Debian 9: 9.3 released 
 [07 Oct 2017]   Updated Debian 9: 9.2 released 
 [12 Aug 2017]   DebConf17 closes in Montreal and DebConf18 dates announced 
 [22 Jul 2017]   Updated Debian 9: 9.1 released 

 For older news items see the  News Page .
If you would like to receive mail whenever new Debian news comes out, subscribe to the
 debian-announce mailing list . 

 RSS 
 Security Advisories 
 [18 Mar 2018]   DSA-4145 gitlab  - security update  
 [17 Mar 2018]   DSA-4144 openjdk-8  - security update  
 [17 Mar 2018]   DSA-4143 firefox-esr  - security update  
 [17 Mar 2018]   DSA-4142 uwsgi  - security update  
 [16 Mar 2018]   DSA-4141 libvorbisidec  - security update  
 [16 Mar 2018]   DSA-4140 libvorbis  - security update  
 [15 Mar 2018]   DSA-4139 firefox-esr  - security update  
 [15 Mar 2018]   DSA-4138 mbedtls  - security update  
 [14 Mar 2018]   DSA-4137 libvirt  - security update  
 [14 Mar 2018]   DSA-4136 curl  - security update  
 [13 Mar 2018]   DSA-4135 samba  - security update  
 [10 Mar 2018]   DSA-4134 util-linux  - security update  
 [07 Mar 2018]   DSA-4133 isc-dhcp  - security update  

 For older security advisories see the  
Security Page .
If you would like to receive security advisories as soon as they're announced, subscribe to the
 debian-security-announce
mailing list . 

This page is also available in the following languages:

 (Arabiya) 
 (B lgarski) 
 catal 
 esky 
 dansk 
 Deutsch 
 (Ellinika) 
 espa ol 
 Esperanto 
 #x0641; #x0627; #x0631; #x0633; #x06cc; (Farsi) 
 fran ais 
 Galego 
 (hayeren) 
 hrvatski 
 Indonesia 
 Italiano 
 (ivrit) 
 (Korean) 
 Lietuvi 
 magyar 
 Nederlands 
 (Nihongo) 
 norsk (bokm l) 
 polski 
 Portugu s 
 rom n 
 (Russkij) 
 sloven ina 
 suomi 
 svenska 
 (Tamil) 
 Ti ng Vi t 
 T rk e 
 (ukrajins'ka) 
 ( ) 
 (HK) 
 ( ) 

How to set  the default document language 

 To report a problem with the web site, e-mail our publicly archived mailing list  debian-www@lists.debian.org . For other contact information, see the Debian  contact page . Web site source code is  available . 

Last Modified: Thu, Jun 8 15:25:09 UTC 2017

  Copyright   1997-2017
  SPI  and others; See  license terms 
  Debian is a registered  trademark  of Software in the Public Interest, Inc.

Debian -- Getting Debian 
===============================

Debian -- Getting Debian 

 Skip Quicknav 

 About Debian 
 Getting Debian 
 Support 
 Developers' Corner 

Getting Debian 

 Getting Debian 
 Debian is distributed  freely 
over Internet. You can download all of it from any of our
 mirrors .
The  Installation Manual 
contains detailed installation instructions.

 If you simply want to install Debian, these are your options: 

 Download an installation image 
 Depending on your Internet connection, you may download either of the following: 

 A  small installation image :
	    can be downloaded quickly and should be recorded onto a removable
	    disk. To use this, you will need a machine with an Internet
	    connection.

 64-bit
	      PC netinst iso 
 32-bit
	      PC netinst iso 

 A larger  complete installation
	image : contains more packages, making it easier to install
	machines without an Internet connection.

 64-bit PC torrents (DVD) 
 32-bit PC torrents (DVD) 
 64-bit PC torrents (CD) 
 32-bit PC torrents (CD) 

 Try Debian live before installing 

      You can try Debian by booting a live system from a CD, DVD or USB key
      without installing any files to the computer. When you are ready, you can
      run the included installer. Provided the images meet your size, language,
      and package selection requirements, this method may be suitable for you.
      Read more  information about this method 
      to help you decide.

 64-bit PC live torrents 
 32-bit PC live torrents 

 Buy a set of CDs or DVDs from one of the
      vendors selling Debian CDs 

      Many of the vendors sell the distribution for less than US$5 plus
      shipping (check their web page to see if they ship internationally).

      Some of the  books about Debian  come with
      CDs, too.

 Here are the basic advantages of CDs: 

 Installation from a CD set is more straightforward. 
 You can install on machines without an Internet connection. 
 You can install Debian (on as many machines as you like) without downloading
	 all packages yourself. 
 The CD can be used to more easily rescue a damaged Debian system. 

 Buy a computer with Debian
      pre-installed 
 There are a number of advantages to this: 

 You don't have to install Debian. 
 The installation is pre-configured to match the hardware. 
 The vendor may provide technical support. 

 Back to the  Debian Project homepage . 

This page is also available in the following languages:

 (B lgarski) 
 catal 
 esky 
 dansk 
 Deutsch 
 espa ol 
 fran ais 
 Galego 
 hrvatski 
 Indonesia 
 Italiano 
 (Korean) 
 magyar 
 Nederlands 
 (Nihongo) 
 norsk (bokm l) 
 polski 
 Portugu s 
 rom n 
 (Russkij) 
 sloven ina 
 suomi 
 svenska 
 Ti ng Vi t 
 T rk e 
 (ukrajins'ka) 
 ( ) 
 (HK) 
 ( ) 

How to set  the default document language 

 Home 

 About 

 Social Contract 
 Code of Conduct 
 Free Software 
 Partners 
 Donations 
 Contact Us 

 Help Debian 

 Getting Debian 

 Network install 
 CD/USB ISO images 
 CD vendors 
 Pre-installed 

 Pure Blends 
 Debian Packages 
 Developers' Corner 

 News 

 Project News 
 Events 

 Documentation 

 Release Info 
 Installation manual 
 Debian Books 
 Debian Wiki 

 Support 

 Debian International 
 Security Information 
 Bug reports 
 Mailing Lists 
 Mailing List Archives 
 Ports/Architectures 

 Miscellaneous 
 Site map 
 Search 
 The Debian Blog 

 To report a problem with the web site, e-mail our publicly archived mailing list  debian-www@lists.debian.org . For other contact information, see the Debian  contact page . Web site source code is  available . 

Last Modified: Sun, Oct 15 12:36:31 UTC 2017

  Copyright   1997-2017
  SPI  and others; See  license terms 
  Debian is a registered  trademark  of Software in the Public Interest, Inc. 

 The Debian Project is an association of individuals who have made common cause to create a free operating system. This operating system that we have created is called Debian.

An operating system is the set of basic programs and utilities that make your computer run. At the core of an operating system is the kernel. The kernel is the most fundamental program on the computer and does all the basic housekeeping and lets you start other programs. Debian GNU/Linux is one of the most popular Linux distributions for personal computers and network servers. 

Debian systems currently use the Linux kernel or the FreeBSD kernel. Linux is a piece of software started by Linus Torvalds and supported by thousands of programmers worldwide. FreeBSD is an operating system including a kernel and other software.

However, work is in progress to provide Debian for other kernels, primarily for the Hurd. The Hurd is a collection of servers that run on top of a microkernel (such as Mach) to implement different features. The Hurd is free software produced by the GNU project.

A large part of the basic tools that fill out the operating system come from the GNU project; hence the names: GNU/Linux, GNU/kFreeBSD and GNU/Hurd. These tools are also free.

Of course, the thing that people want is application software: programs to help them get what they want to do done, from editing documents to running a business to playing games to writing more software. Debian comes with over 37500 packages (precompiled software that is bundled up in a nice format for easy installation on your machine) — all of it free.

It's a bit like a tower. At the base is the kernel. On top of that are all the basic tools. Next is all the software that you run on the computer. At the top of the tower is Debian — carefully organizing and fitting everything so it all works together. 

The Debian Project is an association of individuals who have made common cause to create a free operating system. This operating system that we have created is called Debian.

An operating system is the set of basic programs and utilities that make your computer run. At the core of an operating system is the kernel. The kernel is the most fundamental program on the computer and does all the basic housekeeping and lets you start other programs. Debian GNU/Linux is one of the most popular Linux distributions for personal computers and network servers. 

Debian systems currently use the Linux kernel or the FreeBSD kernel. Linux is a piece of software started by Linus Torvalds and supported by thousands of programmers worldwide. FreeBSD is an operating system including a kernel and other software.

However, work is in progress to provide Debian for other kernels, primarily for the Hurd. The Hurd is a collection of servers that run on top of a microkernel (such as Mach) to implement different features. The Hurd is free software produced by the GNU project.

A large part of the basic tools that fill out the operating system come from the GNU project; hence the names: GNU/Linux, GNU/kFreeBSD and GNU/Hurd. These tools are also free.

Of course, the thing that people want is application software: programs to help them get what they want to do done, from editing documents to running a business to playing games to writing more software. Debian comes with over 37500 packages (precompiled software that is bundled up in a nice format for easy installation on your machine) — all of it free.

It's a bit like a tower. At the base is the kernel. On top of that are all the basic tools. Next is all the software that you run on the computer. At the top of the tower is Debian — carefully organizing and fitting everything so it all works together.
 WHOLE PROJECT MARK