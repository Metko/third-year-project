PROJECT NAME: CrowdtiltOpen 
PROJECT SOURCE: P2P Value directory
---------------------------------------
NEW PAGE: 

 Crowdfunding solution for brands, businesses, and organizations. Self-hosting a campaign allows you to use advanced tools to collect more funds, build your brand, and capture long-term value and traffic. CrowdtiltOpen brings that power to everyone, even non-engineers.

 CrowdtiltOpen is an end-to-end solution that makes it easy to launch your own professional white-labeled crowdfunding, presale, or donation page. CrowdtiltOpen will host your page, handle payment processing, and leave you to focus on what your goal: raising funds. You'll control the look and feel, plug in third-party services you love, and manage your backers—all without writing a line of code.

Unlike most third-party platforms, there are no arbitrary guidelines here. CrowdtiltOpen can be used for collecting pre-orders, raising funds for charity, a group experience, or anything else you can imagine.

You can launch a campaign on your own domain (say, campaign.soylent.me), run multiple campaigns at once, and change your page whenever you want. And with first-ever features like recurring-billing based rewards and Bitcoin integration, your imagination's the only limit. 

Crowdfunding solution for brands, businesses, and organizations. Self-hosting a campaign allows you to use advanced tools to collect more funds, build your brand, and capture long-term value and traffic. CrowdtiltOpen brings that power to everyone, even non-engineers.

 CrowdtiltOpen is an end-to-end solution that makes it easy to launch your own professional white-labeled crowdfunding, presale, or donation page. CrowdtiltOpen will host your page, handle payment processing, and leave you to focus on what your goal: raising funds. You'll control the look and feel, plug in third-party services you love, and manage your backers—all without writing a line of code.

Unlike most third-party platforms, there are no arbitrary guidelines here. CrowdtiltOpen can be used for collecting pre-orders, raising funds for charity, a group experience, or anything else you can imagine.

You can launch a campaign on your own domain (say, campaign.soylent.me), run multiple campaigns at once, and change your page whenever you want. And with first-ever features like recurring-billing based rewards and Bitcoin integration, your imagination's the only limit.
 WHOLE PROJECT MARK