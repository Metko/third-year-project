About us 

 You may be trying to access this site from a secured browser on the server. Please enable scripts and reload this page. 

	Turn on more accessible mode 

	Turn off more accessible mode 

	Skip Ribbon Commands 

	Skip to main content 

	Turn off Animations 

	Turn on Animations 

 SharePoint 

 Sign In 

 Follow 

 Contacts 

 LV 
 EN 

 Activities 
 About Us 

 LIKTA Currently selected 

 It looks like your browser does not have JavaScript enabled. Please turn on JavaScript and try again. 

 Activities About us Currently selected 
 Vision Board Organisational members Individual members Partners Contact Information 

 Page Content Latvian Information and communications technology association    LIKTA 
 Latvian Information and communications technology association (LIKTA) was founded in 1998 and it unites leading industry companies and organizations, as well as ICT professionals   more than 160 members in total. The goal of LIKTA is to foster growth of ICT sector in Latvia by promoting the development of information society and ICT education thus increasing the competitiveness of Latvia on a global scale. The association provides professional opinion to government institutions on legislation and other issues related to the industry, while also maintaining close relationships with other Latvian NGOs and international ICT associations. In 2012 six main goals for ICT industry were defined in the  Charter of LIKTA , whi ch will help the society to become more innovative and technologically educated. 1. Empowered e-citizen:  using digital technologies an e-environment, which would allow the citizens of Latvia to use all the services that are available in real environment   in electronic environment, thus reducing the bureaucratic burden and improving administrative processes. 2. Smart e-citizen:  it is necessary to develop e-skills of the general public to allow everyone to use e-environment infrastructure and benefits provided by it. 3. The most modern e-government in Europe:  a modern e-government provides access to public services to everyone, including the groups of people that do not use modern technologies in their day to day lives. Efficient e-government on municipal and state level will be ensured by integrated and intuitive services. 4. Competitive business environment:  organized and efficient technologies provide business infrastructure and environment that free the entrepreneurs from waste of resources by promoting productivity, competitiveness, growth of exporting ability and contribution to state economy. 5. Active ageing:  by promoting the technology as an instrument to improve quality of life in all fields and at any age, not only the principles of inclusive society would be developed in e-environment, but also the access and security of health care data would be ensured, as well as the opportunities for lifelong education. 6. Easy access to cultural heritage:  by identifying and popularising the cultural heritage both in Latvia and abroad the digital technologies not only increase the national self-confidence, but also integrate the cultural heritage in education. Technology development significantly improves the competitiveness of all fields of economy. Information and communication technologies have increased added value and has one of the highest growth potential in Latvian export. During its years of operation LIKTA has grown from a small organisation to a representative association talking on behalf of the whole industry. Our organization is unique due to the fact that both individual ICT professionals and the largest businesses of the field are members of LIKTA. 

 Stabu iela 47-1, R ga LV-1011 
 Phone: +371 67311821, +371 67291896 
 Email: Office@likta.lv 
 All rights reserved 
  2009- 

	About us 

 You may be trying to access this site from a secured browser on the server. Please enable scripts and reload this page. 

	Turn on more accessible mode 

	Turn off more accessible mode 

	Skip Ribbon Commands 

	Skip to main content 

	Turn off Animations 

	Turn on Animations 

 SharePoint 

 Sign In 

 Follow 

 Contacts 

 LV 
 EN 

 Activities 
 About Us 

 LIKTA Currently selected 

 It looks like your browser does not have JavaScript enabled. Please turn on JavaScript and try again. 

 Activities About us Currently selected 
 Vision Board Organisational members Individual members Partners Contact Information 

 Page Content Latvian Information and communications technology association    LIKTA 
 Latvian Information and communications technology association (LIKTA) was founded in 1998 and it unites leading industry companies and organizations, as well as ICT professionals   more than 160 members in total. The goal of LIKTA is to foster growth of ICT sector in Latvia by promoting the development of information society and ICT education thus increasing the competitiveness of Latvia on a global scale. The association provides professional opinion to government institutions on legislation and other issues related to the industry, while also maintaining close relationships with other Latvian NGOs and international ICT associations. In 2012 six main goals for ICT industry were defined in the  Charter of LIKTA , whi ch will help the society to become more innovative and technologically educated. 1. Empowered e-citizen:  using digital technologies an e-environment, which would allow the citizens of Latvia to use all the services that are available in real environment   in electronic environment, thus reducing the bureaucratic burden and improving administrative processes. 2. Smart e-citizen:  it is necessary to develop e-skills of the general public to allow everyone to use e-environment infrastructure and benefits provided by it. 3. The most modern e-government in Europe:  a modern e-government provides access to public services to everyone, including the groups of people that do not use modern technologies in their day to day lives. Efficient e-government on municipal and state level will be ensured by integrated and intuitive services. 4. Competitive business environment:  organized and efficient technologies provide business infrastructure and environment that free the entrepreneurs from waste of resources by promoting productivity, competitiveness, growth of exporting ability and contribution to state economy. 5. Active ageing:  by promoting the technology as an instrument to improve quality of life in all fields and at any age, not only the principles of inclusive society would be developed in e-environment, but also the access and security of health care data would be ensured, as well as the opportunities for lifelong education. 6. Easy access to cultural heritage:  by identifying and popularising the cultural heritage both in Latvia and abroad the digital technologies not only increase the national self-confidence, but also integrate the cultural heritage in education. Technology development significantly improves the competitiveness of all fields of economy. Information and communication technologies have increased added value and has one of the highest growth potential in Latvian export. During its years of operation LIKTA has grown from a small organisation to a representative association talking on behalf of the whole industry. Our organization is unique due to the fact that both individual ICT professionals and the largest businesses of the field are members of LIKTA. 

 Stabu iela 47-1, R ga LV-1011 
 Phone: +371 67311821, +371 67291896 
 Email: Office@likta.lv 
 All rights reserved 
  2009- 

The increased use of ICT in ever more parts of daily life means that it is important that any digital divides between young and old or rich and poor people do not become permanent features of society. ICT is also one of the key drivers of economic performance and needs to be harnessed in a socially inclusive fashion to create good quality jobs that will contribute to national income and individual wellbeing. There are many ways that ICT already contributes to active ageing, through telecare and telehealth, but it is vital that the social dimension of how people of all ages engage with ICT is not forgotten and that innovative ways of engagement are developed.
The Latvian Information and Communications Technology Association (LIKTA) is a non-governmental association that was established in 1998. It has charitable status, is a licensed educational institution that provides ICT training across Latvia and has more than 200 individual professional members and over 100 corporate members. It is active in pan-European work such as Telecentre Europe and the European Driving Licence Foundation. In 2012 it drew up a Charter for ICT for Better Living with six priorities: Well equipped e-citizen, Smart e-citizen, Most advanced e-government in Europe, Competitive business environment, Active ageing and Easily accessible cultural heritage. The commitment to active ageing is based on the promotion of technology as an instrument to improve quality of life in all fields and at any age, not only the principles of inclusive society would be developed in e-environment, but also the access and security of health care data would be ensured, as well as the opportunities for lifelong education. The annual Get Online week to encourage the wider use of the internet across all parts of Latvia is a major part of their work to promote ICT for all. While most of the participants during Get Online week are younger people, there are also a proportion of older people who take the opportunity to develop their ICT skills.
LIKTA also run several programme to improve the economic performance of micro, small and medium sized enterprises through training courses for small business owners and employees. This innovative initiative is run in partnership with the Ministry of the Economy, the State Economic Development Agency along with Swedbank Latvia and Microsoft Latvia with financing from the European Social Fund and support in king from the IT industry. Another innovative programme is the My Future Work in IT project that is a partnership initiative with the State employment agency network, schools and regional ICT training centres to encourage and promote young people to train and re-train to work in the ICT industry.
In relation to active ageing, LIKTA is engaged with the promotion of the active ageing agenda through the use of ICT among people of all ages and developing innovative ways of improving quality of life. LIKTAs work is directly relevant to the innovative use of ICT across Latvia and is also pertinent to lifelong learning and increasing employability in the industry. Promoting the widespread use of the internet across society can also contribute to building social connectedness for new users.
WHOLE PROJECT MARK