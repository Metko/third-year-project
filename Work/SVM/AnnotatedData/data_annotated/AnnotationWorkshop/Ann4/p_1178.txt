RefuChat   Unterst tzung f r Helfer und Fl chtlinge

 Primary Menu 
 iPhone 
 Android 
 About 

 Facebook 
 Forum 
 Refugee Phrasebook 

 Language: 

 Deutsch 
 English 

 Search for: 

 Skip to content 

 iPhone 
 Android 
 About 

 Facebook 
 Forum 
 Refugee Phrasebook 

 Language: 

 Deutsch 
 English 

 Android   

 Full Version 
(newer devices) 

 Lite Version 
(older devices) 

 iPhone

 Looking for iPhone? Please continue  here 

 When helpers or paramedics deal with refugees, they often have communication issues. Theoretically they could consult dictionaries, or browse the web, but during emergency situations, there is no time. Therefore we have created  refuchat  (from refugees + chat). 
 Usage notes: 

 Each sentence in the most used items can be clicked. 
 If you click an item, it will be translated and read out. 
 Alternatively you can enter a text manually or click on the button  speak  to record your live voice. 
 If you click on  play again  the text will be read out again. 
 To change a recorded voice, you can click on  record  and record your own voice. 

 Internally it uses the official  google translate API , and we will cover the costs for the translation. The app allows for easy translation and reading out of sentences that are requested most often. 
 If you have an integrated  text-to-speach  component on your Android device, it will be used instead. 
 If you have additional questions or ideas for improvement please let us know by sending an email to kontakt@refuchat.com . 

 Full Version 
(newer devices) 

 Lite Version 
(older devices) 

 iPhone

 Looking for iPhone? Please continue  here 

 Proudly powered by WordPress 
  |  
				Theme: Zircone by  @JuanJavier1979 .			 

RefuChat   Unterst tzung f r Helfer und Fl chtlinge

 Primary Menu 
 iPhone 
 Android 
 About 

 Facebook 
 Forum 
 Refugee Phrasebook 

 Language: 

 Deutsch 
 English 

 Search for: 

 Skip to content 

 iPhone 
 Android 
 About 

 Facebook 
 Forum 
 Refugee Phrasebook 

 Language: 

 Deutsch 
 English 

 Android   

 Full Version 
(newer devices) 

 Lite Version 
(older devices) 

 iPhone

 Looking for iPhone? Please continue  here 

 When helpers or paramedics deal with refugees, they often have communication issues. Theoretically they could consult dictionaries, or browse the web, but during emergency situations, there is no time. Therefore we have created  refuchat  (from refugees + chat). 
 Usage notes: 

 Each sentence in the most used items can be clicked. 
 If you click an item, it will be translated and read out. 
 Alternatively you can enter a text manually or click on the button  speak  to record your live voice. 
 If you click on  play again  the text will be read out again. 
 To change a recorded voice, you can click on  record  and record your own voice. 

 Internally it uses the official  google translate API , and we will cover the costs for the translation. The app allows for easy translation and reading out of sentences that are requested most often. 
 If you have an integrated  text-to-speach  component on your Android device, it will be used instead. 
 If you have additional questions or ideas for improvement please let us know by sending an email to kontakt@refuchat.com . 

 Full Version 
(newer devices) 

 Lite Version 
(older devices) 

 iPhone

 Looking for iPhone? Please continue  here 

 Proudly powered by WordPress 
  |  
				Theme: Zircone by  @JuanJavier1979 .			 

A translation app for helpers to communicate with refugees which anticipates most commonly used phrases using the Google Translate API.
WHOLE PROJECT MARK