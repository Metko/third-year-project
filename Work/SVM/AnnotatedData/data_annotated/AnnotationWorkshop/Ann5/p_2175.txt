R.O.S.A.   Network for Employment and Care Services: Promoting the regulation of undeclared work and increasing quality of care | Interlinks

 Jump to Navigation 

     	Health systems and long-term care for older people in Europe. Modelling the interfaces and links between prevention, rehabilitation, quality of services and informal care

 Identity/Concept of Long Term Care 
 Mission Statements 
 organisations that explicitly address problems at the interfaces between, for example, formal/informal and health and social care, prevention and rehabilitation or the use of migrant care workers 

 COUNTRY / LOCATION:  Italy 
 Keywords:  migrant care workers, informal carers, qualification programme, women, social inclusion, illegal home care, labour market 	 

 R.O.S.A.   Network for Employment and Care Services: Promoting the regulation of undeclared work and increasing quality of care 

 Summary 

 R.O.S.A.   Network for Employment and Care Services, is a project approved by the Equal Opportunities and Rights Department of the Italian Government Council of Ministers and is hosted by the Puglia Region. It involves six provinces of Puglia, the Equality Councillors of Lecce and Taranto Province, the National Association of Italian Municipalities (ANCI) and three main National Trade Unions (CGIL, CISL UIL). 
  In response to the large number of home care assistance requests in Puglia, R.O.S.A. aims to increase home assistance services by including migrant care workers (MCW) into local home care service networks and by guaranteeing adequate and appropriate professional solutions. The objectives are, firstly to build a public network of services able to promote welfare and social inclusion in the regional catchment area and secondly, to encourage the regulation of undeclared work in home care, principally provided by migrant women. These objectives are being achieved through the creation of interventions and services that can respond to continuous care demands by:  creating a public system that aims to match supply and demand in home care work (i.e. for migrant care workers); guaranteeing quality of care by developing migrant care workers  skills to be in line with labour market requirements (lifelong learning); and creating a system of services that allows women to balance work and family life. The complexity of different aims and interests called for strong involvement of all local stakeholders: public bodies, trade unions, older people and family associations. In every province a forum has been held in order to negotiate the start and monitoring of the whole project. To encourage families to make regular contracts, a care voucher benefits scheme has been set up in the project, together with an information campaign aimed at families and workers. 
 What is the main benefit for people in need of care and/or carers? 

 Families that manage the needs of care dependent people autonomously by directly paying private assistants can also use professional services. R.O.S.A provides: services that facilitate the matching of supply and demand in home care work through the availability of lists of qualified home care workers (Italians and migrants); information and consultation points about employment contracts; training programmes for migrant care workers; and care voucher benefits for families who want to employ a qualified home care worker. 
  Moreover R.O.S.A helps the families of older people (especially wives and daughters) to balance work and family life. 
 What is the main message for practice and/or policy in relation to this sub-theme? 
 R.O.S.A. is based on a stable collaboration between different institutional levels (i.e. Regions, Provinces, Municipalities) and among local organisations (trade unions, local associations) in order to add private care work provided by migrant women into local services network.  

 Why was this example implemented? 
 In the last three years Puglia Region has redefined regional laws for social inclusion, setting as strategic priorities the quality of care work through promoting a better work/life balance and implementing home facilities for frail people needing LTC services. 
 In addition, it increased investment in care services in order to enhance the female employment rate by improving working conditions, giving value to personal competences and professional qualifications, and helping women to manage the burden of care. 
 R.O.S.A. is an acronym standing for  Network for Employment and Care Services , because, through a network linking different institutional bodies, it established a system to support people who want to receive or to undertake care work. 
 The general aim of the ROSA Project is creating a public network of services able to promote well-being and social inclusion for every citizen. This aim is carried out by implementing high quality home care services, by guaranteeing  transparency in matching supply and demand in home care work and by combating undeclared work. In particular, the project s specific aims are: 

 supporting families to find and choose qualified Italian and migrant care workers; 
 sustaining the supply and demand of home care services (qualified and declared labour market); 
 providing private (Italian and migrant) care workers with continuing education in order to guarantee quality of care work; 
 improving knowledge about undeclared work in home care services in order to combat it; 
 creating a system that allows working women to balance work and family life. 

 Description 
 R.O.S.A. is an experimental project that started in 2008 and is still in progress, co-financed by Puglia Region and the Italian Government. It entails implementing four methods which aim to improve care for older people in their own homes, and promote an integrated system of home care services for older people needing LTC. These four methods are: 

 provincial lists of Italian and migrant care workers; 
 financial incentives dedicated to informal carers to encourage employment of migrant care workers (MCW) with regular contracts; 
 information and consultation points about employment contracts in each local social district; 
 information campaigns involving  Patronati  (offices run by trade unions where people get free or cheap administrative/bureaucratic help). 

 In response to the large number of home care assistance requests in Puglia, the project aims to increase home care services by including migrant care workers into local home care services networks and by guaranteeing adequate and appropriate professional solutions. 
 People who benefit are: 
 1. families of frail people (older or disabled) needing home care services; 
 2. qualified Italian and migrant care workers who offer home care services. 
 Puglia Region has defined and formally acknowledged the Migrant Care Worker profession by a specific regulation. Workers who have attended a special training course and whose competences have been certified by the Public Employment Service, are enrolled in an official list of home care workers maintained in each of Puglia s Provinces. 
 Workers who do not have the necessary competences for the certification  of their professional qualifications, are enrolled on a special list and take part in training activities provided by the ROSA Project. Once they have completed the training (300h), they can be placed on the official list of home care workers. 
 Families who want to employ a qualified home care worker can apply to the Public Employment Service and search the list for the Italian/migrant care worker who meets their needs. Public Employment Service staff also offer consultancy and advice in recruitment. 
 In order to encourage the regulation of the employment contracts for MCW, Puglia Region provides a care voucher benefit, amounting to  2,500 per family (one-off payment). The benefit is assigned through a special public call and on the basis of a specified income indicator (ISEE   Indicator of equivalent economic situation). If ISEE exceeds  15,000 per year, the care voucher amount is reduced to 60% for ISEE values equal or lower than  25,000 per year. 
 The total cost of the ROSA Project is  1,740,000, of which  1,010,000 is financed by the Equal Opportunities and Rights Department of the Italian Government Council of Ministers. Costs are allocated as follows: 

 executive planning and project coordination:  120,000 
 research on the families  needs and social context:  10,000 
 training programmes for migrant care workers:  80,000 
 creation of services for matching demand and supply:  1,405,000 
 communication and promotion activities:  110,000 
 evaluation and monitoring:  15,000 

 What are/were the effects? 
 The project is part of a wider re-organisation process of the whole integrated regional system for continuity of care in Puglia Region. Evaluation is still on-going, but two important results are already evident: 
 1. Occupational increase 
 Home care in Italy depends mainly on private and informal initiatives. Family care is traditionally down to women, who, because of the current negative trend, are less and less involved in the labour market and have fewer opportunities to attend training programmes. The project aims to both provide women with qualifications in the home care field and to improve their access to the labour market. As a result of the ROSA project, from March 2010 about 2,000 people have been placed on the list of home care workers: 800 are fit to work and have been certified to do home care work and 1,200 are on the list to take part in training programmes provided by Puglia Region. 
 2. Regulation of undeclared work 
 Since the 1990s, for historical and geographical reasons, the Puglia Region has been an important centre of migration, with a high percentage of migrants finding jobs as home care workers. In most cases families do not draw up a regular contract to avoid the expense. The project s aim is to stabilise the occupational positions of at least 800 migrant care workers and to support this effort by giving care vouchers to families which can partly cover social security taxes needed for employment contracts. 
 Until March 2011, only 82 families had requested a voucher. Data on the number of total employment contracts that have been signed are not yet available because the project is still in progress. This low number is partly due to the small amount of the voucher which does not act as enough of an incentive. However there is considerable resistance among families to making contracts because they are then obliged to comply with legal conditions such as base wages and rest times. For this reason the project has an important additional impact in bringing about cultural change.  

 What are the strengths and limitations? 
 Puglia Region wanted to innovate the structure and organisation of the home care services network for frail people needing LTC. It stated as strategic priorities increasing the qualification of care work, improving the balance of work and family life, and implementing home facilities for frail people needing LTC services. The problem of the regulation of undeclared work still remains, because families have to face higher costs for assistance.  
 Strengths 

 the project promotes quality home care facilities for frail people needing LTC 
 it supports home care work and informal carers by employing MCWs 
 it promotes the matching of supply and demand, the regulation of undeclared work and female employment 
 it promotes work and family life balance 
 it improves the culture of work contracts. 

 Weaknesses 

 There are no Municipality mechanisms to support the services, such as monitoring and home tutoring, respite services, emergency care, etc. 
 The amount of the care voucher may not affect the regulation process because it only partially covers costs needed to assist frail older people at home and some families will be increasingly unable to afford family assistance. 
 Older people and informal carers are not used to choosing migrant care workers or other kinds of services through a  special list . Puglia Region needs to make lists and registers more user-friendly and to inform not only older people and families, but also all other people who might play a functional role in the relationship between the user and his/her migrant care worker.  
 The time allocated to MCWs to take care of themselves is limited to a few hours per week: lots of migrant care workers do not give any priority to their own healthcare; some health care services are completely unknown to them. 

 Opportunities 

 The project is undoubtedly a positive experience, because it incorporates private care work by MCW into the social services network, supports informal carers, promotes a better legal framework for this type of care work, improves work and family life balance, and improves female employment. 

 Risks 

 Italian/Migrant care workers continue to be chosen through other channels such as word-of-mouth recommendations among neighbours, friends, acquaintances and parish members, not taking into account the special list. 
 Another issue in this context concerns the acknowledgement of the qualifications that migrant women have achieved in their countries of origin, in particular social and health qualifications. At the moment, procedures are such that a MCW is unable to get this acknowledged on their own. 

 These initiatives to integrate MCWs in the LTC system are just a first step on a very long pathway of reforms. Thanks to migrant care workers, the Region can implement home care facilities for older people needing LTC, but unfortunately this care  model  may also be a barrier to social policy innovation. 
 This project, while having positive effects for families in terms of tailored care and immediate access to services, will have to face a wide range of issues concerning long-term sustainability , in terms of both demand and supply of care work. In a context of progressive regulation, some families will be increasingly unable to easily afford family assistance. Additionally, it should not be taken for granted that the future supply of migrant care workers will remain stable,  as the countries of origin, in particular Eastern Europe, are trying to stop women s emigration. 

 Credits 
 Author:  Patrizia Di Santo, Studio Come s.r.l 
 Reviewer 1:  Thomas Emilsson 
 Reviewer 2:  Anja Dieterich 
 Verified by:  Cristina Sunna, key person of Progetto Rosa in; Vito Belladonna Regional Project Manager Progetto Rosa 
 External Links and References 

 http://www.sistema.puglia.it/portal/page/portal/SolidarietaSociale 
 D.D. n. 577 del 03.09.2010 (BURP n. 146 del 16.09.2010),  D.G.R. n. 1889 del 06.08.2010  Deliberazione di Giunta regionale n. 2083/2008   Progetto R.O.S.A.   Rete per l Occupazione e i Servizi di Assistenza   Approvazione Schema di Avviso pubblico per l erogazione di incentivi all assunzione di assistenti familiari    Pubblicazione Avviso e Impegno di spesa   Cap. 786030 del Bilancio regionale 2010. 
 D.G.R. n. 1889 del 06.08.2010 (BURP n. 143 del 07.09.2010),  Deliberazione di Giunta regionale n. 2083/2008   Progetto R.O.S.A.  Rete Occupazione Servizi Assistenziali    Approvazione Schema di Avviso pubblico per l erogazione di incentivi all assunzione di assistenti familiari ; 
 D.G.R. n. 1765 del 27.07.2010 (BURP n. 128 del 03.08.2010),  Approvazione del  Protocollo di Intesa  tra Regione Puglia e  INAIL  per il coordinamento delle attivit  formative nell ambito del Progetto R.O.S.A. (Rete per l Occupazione e i Servizi di Assistenza) e per l attivazione di forme di collaborazione interistituzionale ; 
 D.G.R. n. 2366 del 01.12.2009 (BURP n. 209 del 30.12.2009), approvazione delle  Linee guida integrate per l istituzione e la gestione di elenchi di assistenti familiari   Approvazione dello Schema di Avviso manifestazione di interesse rivolto ad Agenzie di somministrazione ; 
 D.G.R. n. 2496 del 15.12.2009 (BURP n. 11 del 19.01.2010),  D.G.R. n. 2083/2008   Progetto R.O.S.A.   Approvazione Schema di Avviso pubblico rivolto a Patronati per la selezione di Progetti per l attivit  di animazione e sensibilizzazione territoriale ; 
 D.G.R. n 1270 del 21.07.2009 (BURP n. 124 del 12.08.2009), approvazione delle Linee guida per la istituzione e la gestione degli elenchi delle assistenti familiari; - PROFILO ASSISTENTE FAMILIARE - 
 D.G.R. n. 93 del 31.01.2008, Avviso pubblico del Dipartimento per i Diritti e le Pari opportunit  della Presidenza del Consiglio dei Ministri per il finanziamento di progetti pilota finalizzati all emersione del lavoro sommerso nel campo del lavoro di cura domiciliare - Ratifica della candidatura della Regione Puglia Ente capofila del Progetto R.O.S.A.   Rete Occupazione Servizi Assistenziali 

 Please log in to comment 

 THE PROJECT THE FRAMEWORK FOR LTC COUNTRY INFORMATION NEWS CONTACT 

 VIEW FRAMEWORK 
 BY THEMES 

 Identity of Long Term Care 

 Policy and Governance 

 Pathways and Processes 

 Management and Leadership 

 Organisational Structures 

 Means and Resources 

 BY KEYWORDS 
 BY COUNTRIES 

 PROPOSE AND REGISTER YOUR EXAMPLE 

 Search entire site 

 Search this site:  

 Username:  * 

 Password:  * 

 Request new password 

 Funded by the European Commission under the Seventh Framework Programme Grant agreement no. 223037 and supported by the Austrian Ministry of Science 

R.O.S.A.   Network for Employment and Care Services: Promoting the regulation of undeclared work and increasing quality of care | Interlinks

 Jump to Navigation 

     	Health systems and long-term care for older people in Europe. Modelling the interfaces and links between prevention, rehabilitation, quality of services and informal care

 Identity/Concept of Long Term Care 
 Mission Statements 
 organisations that explicitly address problems at the interfaces between, for example, formal/informal and health and social care, prevention and rehabilitation or the use of migrant care workers 

 COUNTRY / LOCATION:  Italy 
 Keywords:  migrant care workers, informal carers, qualification programme, women, social inclusion, illegal home care, labour market 	 

 R.O.S.A.   Network for Employment and Care Services: Promoting the regulation of undeclared work and increasing quality of care 

 Summary 

 R.O.S.A.   Network for Employment and Care Services, is a project approved by the Equal Opportunities and Rights Department of the Italian Government Council of Ministers and is hosted by the Puglia Region. It involves six provinces of Puglia, the Equality Councillors of Lecce and Taranto Province, the National Association of Italian Municipalities (ANCI) and three main National Trade Unions (CGIL, CISL UIL). 
  In response to the large number of home care assistance requests in Puglia, R.O.S.A. aims to increase home assistance services by including migrant care workers (MCW) into local home care service networks and by guaranteeing adequate and appropriate professional solutions. The objectives are, firstly to build a public network of services able to promote welfare and social inclusion in the regional catchment area and secondly, to encourage the regulation of undeclared work in home care, principally provided by migrant women. These objectives are being achieved through the creation of interventions and services that can respond to continuous care demands by:  creating a public system that aims to match supply and demand in home care work (i.e. for migrant care workers); guaranteeing quality of care by developing migrant care workers  skills to be in line with labour market requirements (lifelong learning); and creating a system of services that allows women to balance work and family life. The complexity of different aims and interests called for strong involvement of all local stakeholders: public bodies, trade unions, older people and family associations. In every province a forum has been held in order to negotiate the start and monitoring of the whole project. To encourage families to make regular contracts, a care voucher benefits scheme has been set up in the project, together with an information campaign aimed at families and workers. 
 What is the main benefit for people in need of care and/or carers? 

 Families that manage the needs of care dependent people autonomously by directly paying private assistants can also use professional services. R.O.S.A provides: services that facilitate the matching of supply and demand in home care work through the availability of lists of qualified home care workers (Italians and migrants); information and consultation points about employment contracts; training programmes for migrant care workers; and care voucher benefits for families who want to employ a qualified home care worker. 
  Moreover R.O.S.A helps the families of older people (especially wives and daughters) to balance work and family life. 
 What is the main message for practice and/or policy in relation to this sub-theme? 
 R.O.S.A. is based on a stable collaboration between different institutional levels (i.e. Regions, Provinces, Municipalities) and among local organisations (trade unions, local associations) in order to add private care work provided by migrant women into local services network.  

 Why was this example implemented? 
 In the last three years Puglia Region has redefined regional laws for social inclusion, setting as strategic priorities the quality of care work through promoting a better work/life balance and implementing home facilities for frail people needing LTC services. 
 In addition, it increased investment in care services in order to enhance the female employment rate by improving working conditions, giving value to personal competences and professional qualifications, and helping women to manage the burden of care. 
 R.O.S.A. is an acronym standing for  Network for Employment and Care Services , because, through a network linking different institutional bodies, it established a system to support people who want to receive or to undertake care work. 
 The general aim of the ROSA Project is creating a public network of services able to promote well-being and social inclusion for every citizen. This aim is carried out by implementing high quality home care services, by guaranteeing  transparency in matching supply and demand in home care work and by combating undeclared work. In particular, the project s specific aims are: 

 supporting families to find and choose qualified Italian and migrant care workers; 
 sustaining the supply and demand of home care services (qualified and declared labour market); 
 providing private (Italian and migrant) care workers with continuing education in order to guarantee quality of care work; 
 improving knowledge about undeclared work in home care services in order to combat it; 
 creating a system that allows working women to balance work and family life. 

 Description 
 R.O.S.A. is an experimental project that started in 2008 and is still in progress, co-financed by Puglia Region and the Italian Government. It entails implementing four methods which aim to improve care for older people in their own homes, and promote an integrated system of home care services for older people needing LTC. These four methods are: 

 provincial lists of Italian and migrant care workers; 
 financial incentives dedicated to informal carers to encourage employment of migrant care workers (MCW) with regular contracts; 
 information and consultation points about employment contracts in each local social district; 
 information campaigns involving  Patronati  (offices run by trade unions where people get free or cheap administrative/bureaucratic help). 

 In response to the large number of home care assistance requests in Puglia, the project aims to increase home care services by including migrant care workers into local home care services networks and by guaranteeing adequate and appropriate professional solutions. 
 People who benefit are: 
 1. families of frail people (older or disabled) needing home care services; 
 2. qualified Italian and migrant care workers who offer home care services. 
 Puglia Region has defined and formally acknowledged the Migrant Care Worker profession by a specific regulation. Workers who have attended a special training course and whose competences have been certified by the Public Employment Service, are enrolled in an official list of home care workers maintained in each of Puglia s Provinces. 
 Workers who do not have the necessary competences for the certification  of their professional qualifications, are enrolled on a special list and take part in training activities provided by the ROSA Project. Once they have completed the training (300h), they can be placed on the official list of home care workers. 
 Families who want to employ a qualified home care worker can apply to the Public Employment Service and search the list for the Italian/migrant care worker who meets their needs. Public Employment Service staff also offer consultancy and advice in recruitment. 
 In order to encourage the regulation of the employment contracts for MCW, Puglia Region provides a care voucher benefit, amounting to  2,500 per family (one-off payment). The benefit is assigned through a special public call and on the basis of a specified income indicator (ISEE   Indicator of equivalent economic situation). If ISEE exceeds  15,000 per year, the care voucher amount is reduced to 60% for ISEE values equal or lower than  25,000 per year. 
 The total cost of the ROSA Project is  1,740,000, of which  1,010,000 is financed by the Equal Opportunities and Rights Department of the Italian Government Council of Ministers. Costs are allocated as follows: 

 executive planning and project coordination:  120,000 
 research on the families  needs and social context:  10,000 
 training programmes for migrant care workers:  80,000 
 creation of services for matching demand and supply:  1,405,000 
 communication and promotion activities:  110,000 
 evaluation and monitoring:  15,000 

 What are/were the effects? 
 The project is part of a wider re-organisation process of the whole integrated regional system for continuity of care in Puglia Region. Evaluation is still on-going, but two important results are already evident: 
 1. Occupational increase 
 Home care in Italy depends mainly on private and informal initiatives. Family care is traditionally down to women, who, because of the current negative trend, are less and less involved in the labour market and have fewer opportunities to attend training programmes. The project aims to both provide women with qualifications in the home care field and to improve their access to the labour market. As a result of the ROSA project, from March 2010 about 2,000 people have been placed on the list of home care workers: 800 are fit to work and have been certified to do home care work and 1,200 are on the list to take part in training programmes provided by Puglia Region. 
 2. Regulation of undeclared work 
 Since the 1990s, for historical and geographical reasons, the Puglia Region has been an important centre of migration, with a high percentage of migrants finding jobs as home care workers. In most cases families do not draw up a regular contract to avoid the expense. The project s aim is to stabilise the occupational positions of at least 800 migrant care workers and to support this effort by giving care vouchers to families which can partly cover social security taxes needed for employment contracts. 
 Until March 2011, only 82 families had requested a voucher. Data on the number of total employment contracts that have been signed are not yet available because the project is still in progress. This low number is partly due to the small amount of the voucher which does not act as enough of an incentive. However there is considerable resistance among families to making contracts because they are then obliged to comply with legal conditions such as base wages and rest times. For this reason the project has an important additional impact in bringing about cultural change.  

 What are the strengths and limitations? 
 Puglia Region wanted to innovate the structure and organisation of the home care services network for frail people needing LTC. It stated as strategic priorities increasing the qualification of care work, improving the balance of work and family life, and implementing home facilities for frail people needing LTC services. The problem of the regulation of undeclared work still remains, because families have to face higher costs for assistance.  
 Strengths 

 the project promotes quality home care facilities for frail people needing LTC 
 it supports home care work and informal carers by employing MCWs 
 it promotes the matching of supply and demand, the regulation of undeclared work and female employment 
 it promotes work and family life balance 
 it improves the culture of work contracts. 

 Weaknesses 

 There are no Municipality mechanisms to support the services, such as monitoring and home tutoring, respite services, emergency care, etc. 
 The amount of the care voucher may not affect the regulation process because it only partially covers costs needed to assist frail older people at home and some families will be increasingly unable to afford family assistance. 
 Older people and informal carers are not used to choosing migrant care workers or other kinds of services through a  special list . Puglia Region needs to make lists and registers more user-friendly and to inform not only older people and families, but also all other people who might play a functional role in the relationship between the user and his/her migrant care worker.  
 The time allocated to MCWs to take care of themselves is limited to a few hours per week: lots of migrant care workers do not give any priority to their own healthcare; some health care services are completely unknown to them. 

 Opportunities 

 The project is undoubtedly a positive experience, because it incorporates private care work by MCW into the social services network, supports informal carers, promotes a better legal framework for this type of care work, improves work and family life balance, and improves female employment. 

 Risks 

 Italian/Migrant care workers continue to be chosen through other channels such as word-of-mouth recommendations among neighbours, friends, acquaintances and parish members, not taking into account the special list. 
 Another issue in this context concerns the acknowledgement of the qualifications that migrant women have achieved in their countries of origin, in particular social and health qualifications. At the moment, procedures are such that a MCW is unable to get this acknowledged on their own. 

 These initiatives to integrate MCWs in the LTC system are just a first step on a very long pathway of reforms. Thanks to migrant care workers, the Region can implement home care facilities for older people needing LTC, but unfortunately this care  model  may also be a barrier to social policy innovation. 
 This project, while having positive effects for families in terms of tailored care and immediate access to services, will have to face a wide range of issues concerning long-term sustainability , in terms of both demand and supply of care work. In a context of progressive regulation, some families will be increasingly unable to easily afford family assistance. Additionally, it should not be taken for granted that the future supply of migrant care workers will remain stable,  as the countries of origin, in particular Eastern Europe, are trying to stop women s emigration. 

 Credits 
 Author:  Patrizia Di Santo, Studio Come s.r.l 
 Reviewer 1:  Thomas Emilsson 
 Reviewer 2:  Anja Dieterich 
 Verified by:  Cristina Sunna, key person of Progetto Rosa in; Vito Belladonna Regional Project Manager Progetto Rosa 
 External Links and References 

 http://www.sistema.puglia.it/portal/page/portal/SolidarietaSociale 
 D.D. n. 577 del 03.09.2010 (BURP n. 146 del 16.09.2010),  D.G.R. n. 1889 del 06.08.2010  Deliberazione di Giunta regionale n. 2083/2008   Progetto R.O.S.A.   Rete per l Occupazione e i Servizi di Assistenza   Approvazione Schema di Avviso pubblico per l erogazione di incentivi all assunzione di assistenti familiari    Pubblicazione Avviso e Impegno di spesa   Cap. 786030 del Bilancio regionale 2010. 
 D.G.R. n. 1889 del 06.08.2010 (BURP n. 143 del 07.09.2010),  Deliberazione di Giunta regionale n. 2083/2008   Progetto R.O.S.A.  Rete Occupazione Servizi Assistenziali    Approvazione Schema di Avviso pubblico per l erogazione di incentivi all assunzione di assistenti familiari ; 
 D.G.R. n. 1765 del 27.07.2010 (BURP n. 128 del 03.08.2010),  Approvazione del  Protocollo di Intesa  tra Regione Puglia e  INAIL  per il coordinamento delle attivit  formative nell ambito del Progetto R.O.S.A. (Rete per l Occupazione e i Servizi di Assistenza) e per l attivazione di forme di collaborazione interistituzionale ; 
 D.G.R. n. 2366 del 01.12.2009 (BURP n. 209 del 30.12.2009), approvazione delle  Linee guida integrate per l istituzione e la gestione di elenchi di assistenti familiari   Approvazione dello Schema di Avviso manifestazione di interesse rivolto ad Agenzie di somministrazione ; 
 D.G.R. n. 2496 del 15.12.2009 (BURP n. 11 del 19.01.2010),  D.G.R. n. 2083/2008   Progetto R.O.S.A.   Approvazione Schema di Avviso pubblico rivolto a Patronati per la selezione di Progetti per l attivit  di animazione e sensibilizzazione territoriale ; 
 D.G.R. n 1270 del 21.07.2009 (BURP n. 124 del 12.08.2009), approvazione delle Linee guida per la istituzione e la gestione degli elenchi delle assistenti familiari; - PROFILO ASSISTENTE FAMILIARE - 
 D.G.R. n. 93 del 31.01.2008, Avviso pubblico del Dipartimento per i Diritti e le Pari opportunit  della Presidenza del Consiglio dei Ministri per il finanziamento di progetti pilota finalizzati all emersione del lavoro sommerso nel campo del lavoro di cura domiciliare - Ratifica della candidatura della Regione Puglia Ente capofila del Progetto R.O.S.A.   Rete Occupazione Servizi Assistenziali 

 Please log in to comment 

 THE PROJECT THE FRAMEWORK FOR LTC COUNTRY INFORMATION NEWS CONTACT 

 VIEW FRAMEWORK 
 BY THEMES 

 Identity of Long Term Care 

 Policy and Governance 

 Pathways and Processes 

 Management and Leadership 

 Organisational Structures 

 Means and Resources 

 BY KEYWORDS 
 BY COUNTRIES 

 PROPOSE AND REGISTER YOUR EXAMPLE 

 Search entire site 

 Search this site:  

 Username:  * 

 Password:  * 

 Request new password 

 Funded by the European Commission under the Seventh Framework Programme Grant agreement no. 223037 and supported by the Austrian Ministry of Science 

All European societies face ageing populations and Italy is a country that is experiencing a particularly difficult set of challenges. Italy has a growing number of older people in need of social care and this in the context of a welfare system and society that is less able to provide informal care due to the pressures on women to be in paid employment. There is still a familial orientation towards care for older people  the expectation that older people should be cared for by family members  and a cash orientated system of care benefits. As a result of these factors there has been a significant growth in the number of migrant care workers who have come to Italy to meet this care gap from eastern Europe, east Asia, south America and north Africa. In 1991 there were estimated to be approximately 35,000 care workers in with foreign nationality making up approximately 16% of all home care workers but by 2007 this number had increased to an estimated 700,000 people and 90% of all home care workers. This trend is likely to have continued in the years after this as push factors in foreign countries increased and the demand for care in Italy has continued to increase.
Many home care workers provide care on a live-in 24 hours a day 7 days per week basis without a regular contract of employment and remit a significant proportion of their earnings to their families at home. There are many potential issues for migrant care workers involved in such arrangements including cases of severe exploitation due to lack of employment rights and the risk of deportation. Clearly there is a need for a socially innovative solution to address this increasingly important issue for the mutual benefit of older people in need of care, their families and migrant care workers.
The Network for Employment and Care Services (ROSA) was established in the region of Puglia in southern Italy in 2008 and was a partnership of the six provinces in the region, the National Association of Italian Municipalities and the three main national trade unions with the Equal Opportunities and Rights Department of the Italian government. It aimed to establish a system of supporting people who need or provide care by matching supply with demand through a network of stakeholders. It was socially innovative by attempting to meet the need for qualified Italian and migrant care workers among families with older relatives by acknowledging and encouraging the use of regular employment contracts. By doing so it also aimed to improve knowledge about undeclared home care work in order to combat it and the sometimes extreme examples of exploitation that this can involve.
A register of all care workers offering care in households was established by the Province and financial incentives have been designed to encourage families to employ migrant care workers on regular employment contracts. Migrant care workers need to complete a specialist training course to be enrolled on the register for care workers. Families who then hire registered migrant care workers are eligible for a means-tested one-off care voucher of up to €2,500 with the costs financed by the Equal Opportunities and Rights Department of the Italy government. However, only a relatively small number of families had taken up the offer of the care voucher indicating a reluctance for many people to engage with this provision. There were also information and support services for formal employment contracts in localities across the region administered by trade unions to make the process of regularising employment easier. There have been mixed results from the evaluation of ROSA, which ran up until 2012, with a growth in the number of registered home care workers, increasing numbers of formal contracts of employment and improvements in the quality of home care for older people. However, there are also numerous weaknesses as shown by the small number of families who have taken up to care voucher offer, a lack of municipality capacity to support home care services and the reluctance of many people to go through a bureaucratic process.
In relation to active ageing, ROSA probably improved the quality of care for older adults and to have increased the employment rate for home care workers and women in families with caring responsibilities for older adults. It should have also contributed to financial security for home care workers by regularising their employment status in what is still a precarious and highly demanding occupation.
WHOLE PROJECT MARK