Sensors4Bins

  https://mobicycle.co.uk 
  info@mobicycle.co.uk 

 Sensors4Bins 

 Home 
 Business Case 

 Investors  (by invite) 
 Customers 

 Technology 

 Areas 

 Connectivity 
 Engineering 
 Data Visualisation 

 Related Solutions 

 E-Advisor 
 MobiBins 

 Pilots 
 Contact Us 

 What is Sensors4Bins? 
 We place sensors in to industrial waste containers. Sensors stream data to the cloud for analysis and review. Public and private sector customers can benefit from more efficient collections and cleaner waste streams; i.e., less commingling of organic and inorganic waste. Bin level reporting allows for more targeted marketing campaigns in local communities on a block by block basis, in order to stimulate greater reuse and recycling. Please note: This page is currently under construction. 
 Get Started 

 Local Authorities 
 get real time insights into bin contents 

 Retailers 
 ensure your employees minimise waste created 

 Manufacturers 
 meet your regulatory requirements for segregating hazardous waste 

 Marketing Professionals 
 monitor community participation in recycling programmes at block level 

 Turnkey Solutions 

 Pilots 
 for Smarter Cities 
 View our Explainer Video 

 Telecommunications 
 We use low power, wide area networks to send sensor data to the cloud and edge devices. 

 Engineering 
 Our engineering specialists retrofit your existing industrial waste container. 

 Data Visualisation 
 Choose from a range of top rated platforms to monitor and manage your bins. 

 Blog Posts 
 Care to learn more about what motivates MobiCycle beyond the cutting edge technology? 

 06   Apr 

 Working with a limited budget? MobiCycle can help you apply for funding your pilot. 
 Read More 

 MobiCycle Ltd's Special Sauce 
 Along with Smart Cities, our for-profit social enterprise, MobiCycle Ltd, is generally known for 'smart' bins. MobiCycle's waste containers,  
 Read More  

 Why Embedded Devices are not Embedded IoT Devices 
 Last night, I attended a meetup run by the Embedded Engineering group here in London at a posh City address. On a floor with a.... 
 Read More  

 MobiCycle Ltd's Barriers to Entry 
 Every entrepreneur who seeks funding faces a similar question about their impending grand entrance into a competitive industry, "What are your industry's barriers to entry?" 
 Read More  

  Latest News 

 Contact Us 

            64 Long Acre, London WC2E 9JD United Kingdom 

  +44 (123) 456 7890 
  sales@mobicycle.co.uk 

 About MobiCycle 

 MobiCycle Ltd's Special Sauce 
 Friday, 22 th  December 2016 
 ...Many smart bin companies rely on fill-level sensors with ultrasonic and infrared capabilities as their main selling point. The more ambitious operators may combine ultrasonic   infrared sensors with geo-location/GPS, temperature and tilt sensors for added impact. MobiCycle, on the other hand... 

 Meet the Team in Person 

 London - ongoing 
 Sweden in May 2017 
 Cambridge, UK in June 2017 
 Germany in July 2017 
 Reading, UK in October 2017 
 NYC in November 2017 
 Las Vegas in December 2017 
 China planning for 2018 

 Copyright   2015 - All Rights Reserved -  MobiCycle Ltd  
 Template by  OS Templates 

Sensors4Bins

  https://mobicycle.co.uk 
  info@mobicycle.co.uk 

 Sensors4Bins 

 Home 
 Business Case 

 Investors  (by invite) 
 Customers 

 Technology 

 Areas 

 Connectivity 
 Engineering 
 Data Visualisation 

 Related Solutions 

 E-Advisor 
 MobiBins 

 Pilots 
 Contact Us 

 What is Sensors4Bins? 
 We place sensors in to industrial waste containers. Sensors stream data to the cloud for analysis and review. Public and private sector customers can benefit from more efficient collections and cleaner waste streams; i.e., less commingling of organic and inorganic waste. Bin level reporting allows for more targeted marketing campaigns in local communities on a block by block basis, in order to stimulate greater reuse and recycling. Please note: This page is currently under construction. 
 Get Started 

 Local Authorities 
 get real time insights into bin contents 

 Retailers 
 ensure your employees minimise waste created 

 Manufacturers 
 meet your regulatory requirements for segregating hazardous waste 

 Marketing Professionals 
 monitor community participation in recycling programmes at block level 

 Turnkey Solutions 

 Pilots 
 for Smarter Cities 
 View our Explainer Video 

 Telecommunications 
 We use low power, wide area networks to send sensor data to the cloud and edge devices. 

 Engineering 
 Our engineering specialists retrofit your existing industrial waste container. 

 Data Visualisation 
 Choose from a range of top rated platforms to monitor and manage your bins. 

 Blog Posts 
 Care to learn more about what motivates MobiCycle beyond the cutting edge technology? 

 06   Apr 

 Working with a limited budget? MobiCycle can help you apply for funding your pilot. 
 Read More 

 MobiCycle Ltd's Special Sauce 
 Along with Smart Cities, our for-profit social enterprise, MobiCycle Ltd, is generally known for 'smart' bins. MobiCycle's waste containers,  
 Read More  

 Why Embedded Devices are not Embedded IoT Devices 
 Last night, I attended a meetup run by the Embedded Engineering group here in London at a posh City address. On a floor with a.... 
 Read More  

 MobiCycle Ltd's Barriers to Entry 
 Every entrepreneur who seeks funding faces a similar question about their impending grand entrance into a competitive industry, "What are your industry's barriers to entry?" 
 Read More  

  Latest News 

 Contact Us 

            64 Long Acre, London WC2E 9JD United Kingdom 

  +44 (123) 456 7890 
  sales@mobicycle.co.uk 

 About MobiCycle 

 MobiCycle Ltd's Special Sauce 
 Friday, 22 th  December 2016 
 ...Many smart bin companies rely on fill-level sensors with ultrasonic and infrared capabilities as their main selling point. The more ambitious operators may combine ultrasonic   infrared sensors with geo-location/GPS, temperature and tilt sensors for added impact. MobiCycle, on the other hand... 

 Meet the Team in Person 

 London - ongoing 
 Sweden in May 2017 
 Cambridge, UK in June 2017 
 Germany in July 2017 
 Reading, UK in October 2017 
 NYC in November 2017 
 Las Vegas in December 2017 
 China planning for 2018 

 Copyright   2015 - All Rights Reserved -  MobiCycle Ltd  
 Template by  OS Templates 

Sensors4Bins

  https://mobicycle.co.uk 
  info@mobicycle.co.uk 

 Sensors4Bins 

 Home 
 Business Case 

 Investors  (by invite) 
 Customers 

 Technology 

 Areas 

 Connectivity 
 Engineering 
 Data Visualisation 

 Related Solutions 

 E-Advisor 
 MobiBins 

 Pilots 
 Contact Us 

 What is Sensors4Bins? 
 We place sensors in to industrial waste containers. Sensors stream data to the cloud for analysis and review. Public and private sector customers can benefit from more efficient collections and cleaner waste streams; i.e., less commingling of organic and inorganic waste. Bin level reporting allows for more targeted marketing campaigns in local communities on a block by block basis, in order to stimulate greater reuse and recycling. Please note: This page is currently under construction. 
 Get Started 

 Local Authorities 
 get real time insights into bin contents 

 Retailers 
 ensure your employees minimise waste created 

 Manufacturers 
 meet your regulatory requirements for segregating hazardous waste 

 Marketing Professionals 
 monitor community participation in recycling programmes at block level 

 Turnkey Solutions 

 Pilots 
 for Smarter Cities 
 View our Explainer Video 

 Telecommunications 
 We use low power, wide area networks to send sensor data to the cloud and edge devices. 

 Engineering 
 Our engineering specialists retrofit your existing industrial waste container. 

 Data Visualisation 
 Choose from a range of top rated platforms to monitor and manage your bins. 

 Blog Posts 
 Care to learn more about what motivates MobiCycle beyond the cutting edge technology? 

 06   Apr 

 Working with a limited budget? MobiCycle can help you apply for funding your pilot. 
 Read More 

 MobiCycle Ltd's Special Sauce 
 Along with Smart Cities, our for-profit social enterprise, MobiCycle Ltd, is generally known for 'smart' bins. MobiCycle's waste containers,  
 Read More  

 Why Embedded Devices are not Embedded IoT Devices 
 Last night, I attended a meetup run by the Embedded Engineering group here in London at a posh City address. On a floor with a.... 
 Read More  

 MobiCycle Ltd's Barriers to Entry 
 Every entrepreneur who seeks funding faces a similar question about their impending grand entrance into a competitive industry, "What are your industry's barriers to entry?" 
 Read More  

  Latest News 

 Contact Us 

            64 Long Acre, London WC2E 9JD United Kingdom 

  +44 (123) 456 7890 
  sales@mobicycle.co.uk 

 About MobiCycle 

 MobiCycle Ltd's Special Sauce 
 Friday, 22 th  December 2016 
 ...Many smart bin companies rely on fill-level sensors with ultrasonic and infrared capabilities as their main selling point. The more ambitious operators may combine ultrasonic   infrared sensors with geo-location/GPS, temperature and tilt sensors for added impact. MobiCycle, on the other hand... 

 Meet the Team in Person 

 London - ongoing 
 Sweden in May 2017 
 Cambridge, UK in June 2017 
 Germany in July 2017 
 Reading, UK in October 2017 
 NYC in November 2017 
 Las Vegas in December 2017 
 China planning for 2018 

 Copyright   2015 - All Rights Reserved -  MobiCycle Ltd  
 Template by  OS Templates 

Stream data from waste bins to the cloud.  Compare recycling bin data to standard bin data.  Optimise collections. 

<p>Along with Smart Cities, my for-profit social enterprise, MobiCycle Ltd, is generally known for 'smart' bins. MobiCycle's waste containers, MobiBins, are designed to hold abandoned electronics and electricals. The IoT versions of MobiBins, with their embedded sensors and internet connectivity, are undoubtedly smart. Are you aware, however, that MobiCycle retrofits most industrial waste containers with sensors and low power, wide area connections? Sensors4Bins by MobiCycle can transform your standard industrial waste bin into a smart bin. Smart bins are not a new concept, however. Several firms offer them.</p>
<p>Many smart bin companies rely on fill-level sensors with ultrasonic and infrared capabilities as their main selling point. The more ambitious operators may combine ultrasonic &amp; infrared sensors with geo-location/GPS, temperature and tilt sensors for added impact. The benefits to fill level sensors are reasonably tested and documented in trials across Europe, Asia, et al.</p>
<div class="slate-resizable-image-embed slate-image-embed__resize-right" data-imgsrc="https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAiCAAAAJDIwMzA0MTVjLWQ3YmYtNDc5NC04ZmZiLTNiOGEzMWI1ZGMzOQ.png"><img src="https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAiCAAAAJDIwMzA0MTVjLWQ3YmYtNDc5NC04ZmZiLTNiOGEzMWI1ZGMzOQ.png" /></div>
<p>Fill level sensors send out a signal every so often to determine the height of the trash contained within the bin. As trash accumulates, the wireless transceiver streams data to an online dashboard. When the trash reaches a desired volume or height, the bin triggers an alert, and the system schedules a collection. The city of Glasgow in Scotland, for example, recently announced a pilot for smart bins. Despite the hype, the big problem troubling waste sectors globally is not the fill level of trash within a given container.</p>
<p>&nbsp;</p>
<p>What keeps many (if not most) councils and waste collectors up at night is&nbsp;the degree of purity of&nbsp;the collected waste; i.e., the amounts of mixed or commingled waste. Commingling results when consumers combine &lsquo;incompatible&rsquo; waste streams such as dry with wet waste, glass with paper, or electronics with food. Per Sarfaraz Khan, the Joint Commissioner for solid waste management in Bengaluru, India, &ldquo;The problem is&nbsp;<em>not with collection</em>&nbsp;but with sending mixed waste to these units. Any amount of composting we try to do will generate smell and there will be protests.&rdquo; What can be done? The typical mistake made by local councils and waste companies is to vacillate between 'carrots' and 'sticks'.</p>
<div class="slate-resizable-image-embed slate-image-embed__resize-right" data-imgsrc="https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAk7AAAAJGEzZWJkOWE0LTBkZTUtNDY3MS1iMDVhLWZiOGEzZjFmZGMxNg.png"><img src="https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAk7AAAAJGEzZWJkOWE0LTBkZTUtNDY3MS1iMDVhLWZiOGEzZjFmZGMxNg.png" /></div>
<p><em>Sticks</em> generally refer to legal incentives to reduce waste. On the positive side, Italy and France passed laws in 2016 to make it easier for surplus food stock to be donated rather than binned. Post Brexit grumblings are beginning to emerge in the UK, however. Some have expressed concerns that <strong>environmental standards may be compromised in the pursuit of new trade deals</strong>. Given the transient nature of environmental laws, perhaps financial incentives (aka carrots) present a safer bet?</p>
<p>Put another way, can coupons or cash encourage people to avoid commingling their waste? Despite the prevalence of finance schemes, we continue to see recycling rates stagnate or decline. In 2010, just 36% of total waste production in the EU was recycled, amounting to 2,5 billion tons. The rest was landfilled or burned. As Ian Yolles of US headquartered Recyclebank admits, &Prime;We were focused at first on providing financial incentives to motivate consumers. However, different people are motivated in different ways. We&prime;re now experimenting with social currency and information currency to motivate behavioral change.&Prime;</p>
<div class="slate-resizable-image-embed slate-image-embed__resize-right" data-imgsrc="https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAjiAAAAJGVhZmEzNjQyLWIyYTAtNGFiNS1iMzFmLTNhZTdiZjI0NzdhMQ.jpg"><img src="https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAjiAAAAJGVhZmEzNjQyLWIyYTAtNGFiNS1iMzFmLTNhZTdiZjI0NzdhMQ.jpg" /></div>
<p>At MobiCycle, we recognise that effective behavioural change requires convenient solutions. We stand by the notion that, for many consumers, time and effort generally trump financial incentives(carrots) or fear of legal sanctions(sticks). That's why we engage consumers at home via spoken conversations. Our smart home solution, <a href="http://e-advisor.mobicycle.co.uk/" target="_blank" rel="nofollow noopener noreferrer">E(lectronic) Advisor</a>, is launching worldwide via leading providers such as Amazon(Alexa), Google(Home) and Apple(HomeKit). E-Advisor is a conversational agent that guides consumers through a seamless process to repair, recycle or sell their electronics. Nevertheless, an emphasis on changing consumer behaviour is not enough to reduce commingling.</p>
<p>We nudge corporate and public sector clients to align their smart bin/sensor choices with their overall waste strategy. Too often, we witness procurement decisions underpinned by secondary considerations such as the sensor's dimensions, power consumption or unit cost - none of which fundamentally impacts the bin's ability to recognise or take action against commingling. Admittedly, low power, ultrasonic sensors costing six British pounds (ten US dollars) per unit may suffice if your primary goal is to avoid overflowing waste bins.</p>
<div class="slate-resizable-image-embed slate-image-embed__resize-right" data-imgsrc="https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAe_AAAAJDE2MTVhYWE4LThmYmUtNGEyYy1iOGYwLTU4NmMyNGEyMjUwYg.jpg"><img src="https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAe_AAAAJDE2MTVhYWE4LThmYmUtNGEyYy1iOGYwLTU4NmMyNGEyMjUwYg.jpg" /></div>
<p>But maybe you want to know the weight of the waste in your bin prior to collection, to understand how effective your recycling marketing campaigns are within a five block radius? Perhaps you want to be confident your truck will be able to complete its designated collection route because the weight of the trash along that route does not exceed the vehicle's carrying capacity?</p>
<div class="slate-resizable-image-embed slate-image-embed__resize-left" data-imgsrc="https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAenAAAAJDRjOTY0ZmNlLWVkNjYtNGMxZS1hY2JiLTU1OGUyYzJiMDlhMg.jpg"><img src="https://media.licdn.com/mpr/mpr/AAEAAQAAAAAAAAenAAAAJDRjOTY0ZmNlLWVkNjYtNGMxZS1hY2JiLTU1OGUyYzJiMDlhMg.jpg" /></div>
<p>Or, perhaps you want to determine which damaged newspaper recycling bin let in rain before your next bulk collection? We help you avoid contaminating dry papers located within multiple functioning recycling bins, with wet papers from one damaged bin. This sensor add-on eventually pays for itself because the alternative- wet paper- has no resale value. For these key considerations and others, you may want to consider a <a href="http://mobicycle.co.uk/pilots.html" target="_blank" rel="nofollow noopener noreferrer">pilot with MobiCycle</a>. Our pilots cover not only smart bins and the smart home. We also offer visualisation tools.</p>
<p>Like other smart bin companies, our online dashboard offers exceptional insights. We stream data in real time to platforms such as <a href="http://dzone.com/articles/analyzing-data-with-ibm-watson-internet-of-things" target="_blank" rel="nofollow noopener noreferrer">IBM's Bluemix</a>. IBM's Predictive Analytics service on Bluemix not only defines models. The algorithms predict behaviour to save you time and money during your pilot phase and beyond. Just as we help our clients choose <em>smart bin sensors</em> to effectively tackle commingling, we also help clients assemble software packages equipped to confront some of the world's most wicked problems around waste.</p>
<p>So whether you choose fill level sensors from our competitors or tonnage sensors et al by MobiCycle, my overriding message is that the benefits of smart bins are substantial. As SunBeom Gwon, CEO and founder of South Korean based Ecube Labs summarises, "If the 5,000 bins around Seoul are replaced with our bins, waste removal will decrease by over 20% within a year and a half. This, effectively, is the same as planting 150,000 trees around Seoul by decreasing CO2 emissions from reducing fleet operations for waste removal." Wonderful stats, yes? To extend Ecube Labs' upbeat vibe, I conclude by revealing MobiCycle's unique value proposition.</p>
<p>&nbsp;</p>
WHOLE PROJECT MARK