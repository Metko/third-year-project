Work At Height Courses   ASRETEC

 Company 
 Clientele 
 News   Updates 
 Contact Us 
 Multi Skilling Scheme 
 Course Registration 
   Consultancy 
 Solutions  

 All Solutions 
 Work At Height (WAH) 
 Fall Prevention System 
 Rescue Kit 

 Training  

 All Courses 
 Work At Height Courses 
 Confined Space Courses 
 Rescue Courses 
 Tower Climbing Courses 
 Equipment Inspector Courses 
 Risk Management Courses 
 Metal Scaffold Erection Courses 
 Overseas On-Site Courses 

 Services  

 All Services 
 Installation of Fall Protection System 
 Difficult Access 
 Facade Cleaning 
 Safety Awareness 

 WSH Events 

 Work At Height Courses 

 Work At Height Courses ASRETEC Work At Height Courses Work At Height Courses 
 Confined Space Courses 
 Rescue Courses 
 Tower Climbing Courses 
 Equipment Inspector Course 
 Risk Management Courses 
 Metal Scaffold Erection Course 
 Overseas On-Site Courses 

 ASRETEC has taught Work At Height courses before they became Singapore MOM Accredited Courses. Our Work At Height courses for workers, supervisors, assessors are all MOM Accredited. 
 All courses include the need for correct selection, care and use of equipment, the need for rescue provision and the key requirements of a safe system of work. 

 Work At Height Course for Workers (Learning Service Provider) Work At Height Course for Supervisors (Learning Service Provider) Work At Height Course for Manager Integrated (Learning Service Provider) Work At Height Course for Assessor Integrated (Learning Service Provider) Managing Work At Height Course (Learning Service Provider) 

 Post navigation 

 Next Post Confined Space Courses   

 Safety Consultancy, Solutions   Training 

 Accreditation     

 Download Our Service And Training Brochure   

 Be on your way to experience the unrivaled training and consultancy that thousands of satisfied customers have benefitted from. 

 If you are a human and are seeing this field, please leave it blank.			

 Fields marked with an  *  are required 

 Textbox  *   

 Email  *   

 Phone 				 

 Textbox 				 

 By ticking this box, you agree that ASRETEC will be sending you information pertaining to its courses and services to your inbox. Your contact details are kept confidential with ASRETEC.  *   

 2006   2017 ASRETEC Pte Ltd. 
 Privacy Statement 
 Terms of Use 
 Copyright Notice 
 Website Disclaimer 

 Company 
 Overview 
 Leadership 
 Affiliations   Certifications 
 Clients 
 Careers 
 Blog 

 Services 
 Installation of Fall Protection System 
 Difficult Access 
 Facade Cleaning 
 Safety Awareness 

 Courses 
 Work At Height Courses 
 Confined Space Courses 
 Rescue Courses 
 Tower Climbing Courses 
 Equipment Inspector Course 
 Risk Management Courses 
 Metal Scaffold Erection Course 
 Overseas On-Site Courses 

 Contact Us 

 Training Centre: 
Fluorotech Building #05-01, 
No. 23 Neythal Road, 628588 Singapore
Office: 
121 Neythal Road, #02-00, 628606
   (+65) 6795 9522   (+65) 6795 6581   info@asretec.org   

 Find Us On 

 Enquire Now 

 If you are a human and are seeing this field, please leave it blank.			

 Fields marked with an  *  are required 

 Full Name  *   

 Company 				 

 Email Adress  *   

 Contact No 				 

 Enquiry  *   

 Security  *   

Work At Height Courses   ASRETEC

 Company 
 Clientele 
 News   Updates 
 Contact Us 
 Multi Skilling Scheme 
 Course Registration 
   Consultancy 
 Solutions  

 All Solutions 
 Work At Height (WAH) 
 Fall Prevention System 
 Rescue Kit 

 Training  

 All Courses 
 Work At Height Courses 
 Confined Space Courses 
 Rescue Courses 
 Tower Climbing Courses 
 Equipment Inspector Courses 
 Risk Management Courses 
 Metal Scaffold Erection Courses 
 Overseas On-Site Courses 

 Services  

 All Services 
 Installation of Fall Protection System 
 Difficult Access 
 Facade Cleaning 
 Safety Awareness 

 WSH Events 

 Work At Height Courses 

 Work At Height Courses ASRETEC Work At Height Courses Work At Height Courses 
 Confined Space Courses 
 Rescue Courses 
 Tower Climbing Courses 
 Equipment Inspector Course 
 Risk Management Courses 
 Metal Scaffold Erection Course 
 Overseas On-Site Courses 

 ASRETEC has taught Work At Height courses before they became Singapore MOM Accredited Courses. Our Work At Height courses for workers, supervisors, assessors are all MOM Accredited. 
 All courses include the need for correct selection, care and use of equipment, the need for rescue provision and the key requirements of a safe system of work. 

 Work At Height Course for Workers (Learning Service Provider) Work At Height Course for Supervisors (Learning Service Provider) Work At Height Course for Manager Integrated (Learning Service Provider) Work At Height Course for Assessor Integrated (Learning Service Provider) Managing Work At Height Course (Learning Service Provider) 

 Post navigation 

 Next Post Confined Space Courses   

 Safety Consultancy, Solutions   Training 

 Accreditation     

 Download Our Service And Training Brochure   

 Be on your way to experience the unrivaled training and consultancy that thousands of satisfied customers have benefitted from. 

 If you are a human and are seeing this field, please leave it blank.			

 Fields marked with an  *  are required 

 Textbox  *   

 Email  *   

 Phone 				 

 Textbox 				 

 By ticking this box, you agree that ASRETEC will be sending you information pertaining to its courses and services to your inbox. Your contact details are kept confidential with ASRETEC.  *   

 2006   2017 ASRETEC Pte Ltd. 
 Privacy Statement 
 Terms of Use 
 Copyright Notice 
 Website Disclaimer 

 Company 
 Overview 
 Leadership 
 Affiliations   Certifications 
 Clients 
 Careers 
 Blog 

 Services 
 Installation of Fall Protection System 
 Difficult Access 
 Facade Cleaning 
 Safety Awareness 

 Courses 
 Work At Height Courses 
 Confined Space Courses 
 Rescue Courses 
 Tower Climbing Courses 
 Equipment Inspector Course 
 Risk Management Courses 
 Metal Scaffold Erection Course 
 Overseas On-Site Courses 

 Contact Us 

 Training Centre: 
Fluorotech Building #05-01, 
No. 23 Neythal Road, 628588 Singapore
Office: 
121 Neythal Road, #02-00, 628606
   (+65) 6795 9522   (+65) 6795 6581   info@asretec.org   

 Find Us On 

 Enquire Now 

 If you are a human and are seeing this field, please leave it blank.			

 Fields marked with an  *  are required 

 Full Name  *   

 Company 				 

 Email Adress  *   

 Contact No 				 

 Enquiry  *   

 Security  *   

A Variety of Courses for Workplace Safety 

<p>Employers have a responsibility to ensure workplace safety for all workers, as well as visitors and the general public. In most cases, that entails basic procedures and protocols. Cleaning spills quickly, training on proper body mechanics for lifting, evacuation routes, and how to use a fire extinguisher are fine for many work situations.</p>
<p>Safety glasses, a back belt, and work gloves are the most common types of personal protective equipment (PPE) required for workers. A helmet is also needed for construction, welding, on-site planning or assessments, and other occupations.</p>
<p>Some working conditions, such as confined spaces, high-voltage areas, and heights, require specialized training to comply with oversight agency regulations, insurance stipulations, and industry standards. Lack of training can result in injuries, death, equipment or property damage, fines, penalties, and increased liability risks.</p>
<p><strong>Courses Available</strong></p>
<p>Companies such as <a href="http://asretec.org/">Asretec</a> provide courses that combine classroom work, learning practical skills, and performance assessments to certified workers, supervisors, and managers as competent to work in specific conditions or places.</p>
<p>Work At Height is one course example. Versions are offered for workers, assessors, supervisors, and business owners. The course for workers, for example, is at a low cost per person and lasts one full day.</p>
<p>The theory is minimal and includes selecting and properly utilizing the correct equipment, harness, or technique for every situation. Practical skills learned include harness fitting, climbing with twin lanyards, using horizontal lifelines, and the most efficient use of a work restraint system, among others.</p>
<p><img src="https://s13.postimg.org/ikuy08non/Asretec_2.jpg" alt="Asretec" width="437" height="291" /></p>
<p>The version for supervisors is one and one-half days and includes identifying relevant regulations, conducting thorough risk assessments, and implementing fall prevention plans. All height courses are assessed via written work, practical skills, and overall performance.</p>
<p>Courses are designed to meet regulations and can be conducted on-site globally. Specific syllabi for each height course can be viewed at asretec.org/course/work-at-height-courses/. Tower climbing, rescue courses, risk assessment, and putting together metal scaffolding are also offered. Full details on all courses can be found at <a href="http://asretec.org/course/work-at-height-courses/">Asretec.org</a>.</p>
<p><strong>Human Resource (HR) Responsibilities</strong></p>
<p>Safety in the workplace is an important matter that requires constant consideration. Keeping track of renewal dates for employees, making sure new employees are properly trained before working in the field, and providing documentation to prove compliance. This is typically the duty of the HR director or department.</p>
<p>Employers who utilize the same company for all <a href="http://www.healthyworkinglives.com/advice/work-equipment/working-at-height">specialty training</a> can reduce much of that paperwork. Documentation requirements can be fulfilled with reports that include the attendees, the skills learned, and assessment scores. The date and location will also be displayed as well.</p>
WHOLE PROJECT MARK