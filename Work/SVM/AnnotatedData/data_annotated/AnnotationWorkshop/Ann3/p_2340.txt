Home - North London Cares

 Got it 
 This site uses cookies. By continuing to browse we assume you are happy to receive cookies, you can read more on our cookie policy page. 

 Menu  
 Donate 

                                      The charity                  

 About us 

 The difference we make 

 The story so far 

 The team 

 Press   awards 

 Funders   endorsements 

 Contact us 

                                      Publications                  

 News   blog 

 Publications   reports 

 Hidden Heroes 

                                      Volunteering                  

 Activities 

 FAQs 

 Volunteer handbook 

 Meet our volunteers 

 Meet our older neighbours 

 Get involved 

                                      Other ways to help                  

 Fundraise 

 For local businesses 

 For corporations 

 Trusts   foundations 

 Wish list 

 Legacy giving 

 Make a donation 

 A community network of young professionals and older neighbours hanging out and helping one another in our rapidly changing city. 
 Support our work this summer 

 Make a difference this autumn 
 With autumn upon us, it's increasingly important we find opportunities for laughter and play during the cold, shorter days. A  20 donation could help Londoners of all ages build new relationships and experiences in our rapidly changing capital. 

											Donate to North London Cares today

 Help neighbours share time this autumn 
 Yoga, choir, storytelling and more   your  20 donation could give the joy of friendship and connection to neighbours who otherwise might not get out much. Be part of it and make a donation today. 

											Support North London Cares

 Fancy a challenge?  
 If you want to go the extra mile for North London Cares, then check out our upcoming challenge events. 
 From 5k fun runs to marathons, and swims to cycles, there's something for you to brave this year. 

											Check out our upcoming challenges

 It made me feel like a VIP! 
 In August, North London Cares celebrated its sixth birthday. 
 Love Your Neighbour pairs got together to celebrate the occasion by enjoying tea parties at home with an afternoon tea hamper. 

											Read about cake and company here

 Support our work this autumn 
 This autumn, North London Cares is bringing older and younger people together to share new experiences   so that neighbours who live side-by-side but too infrequently interact can feel better connected. Through our social clubs and one-to-one friendships, neighbours will enjoy dances, choirs and outings to our local parks. 

											Donate  20 to help fund our activities this autumn

 1  / 5 

 Our Stories 

 We're looking for two brilliant new trustees   join us 

 Posted by Alex Smith  
 The Cares Family are looking for two brand new trustees   with particular expertise in fundraising and law  to support the expansion of our vision, geography and impact over the coming years. Working with our current trustees and executive team who have already created North London Cares, South London Cares and Manchester Cares, we need someone committed to tackling the issues we work on; someone strategic and experienced supporting small teams to succeed. 

 North London Stories Volume 8: My Grandpa's Boat 

 Posted by Mike Evans  
 Back in July Emma, Will and Tony's shared stories of growing up amongst their siblings . There were adventures, accidents and plenty of laughs along the way. In the same social club , North London Stories regular Becca told the gang a lovely tale about her Grandpa...and his boat. 

 More Stories 

 Photo Feed 

 See more of our photos on: 

 View all of our supporters  North London Cares is partnered with... 

 Get involved 

 North London Cares 

 Home 
 Blog 
 Publications   Reports 
 Contact 
 Donate 
 Sitemap 

 Connect With Us 

  Follow us on Twitter 
  Like us on Facebook 
  Watch us on YouTube 
  View our Instagram snaps 

 Get involved 

 Volunteering in Camden 
 Volunteering in Islington 
 Volunteering in North London 
 Volunteer's Handbook 

 Legal 
 North London Cares is the public name for NL Cares Ltd, company number 07737818, charity number 1153137. Registered at North London Cares, C/o ND2, Floor One, 1 Triton Square, London, NW1 3HG. Designed   Built by Mutual 

 Living in Southwark or Lambeth? Check out our sister charity    South London Cares 

 A voluntary organisation operating in the London boroughs of Camden  Islington, run by a voluntary board of directors and trustees with advisory group support.
WHOLE PROJECT MARK