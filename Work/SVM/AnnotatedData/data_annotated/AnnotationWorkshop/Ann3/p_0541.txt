Field Trip - An open source documentary

 Field Trip 

 en 
 de 

 Field Trip is an open source documentary about an unconventional place: Tempelhof Field - an airfield in the heart of Berlin metropolis. Field Trip tells the story of what, for some, is a 300 hectare free urban paradise, for others a place of forced labour, and for yet more a safe haven. By using the technique of open hypervideo and intense on-site and online community development, Field Trip aims at becoming a living documentary.                  

 This project is supported by MIZ-Babelsberg 

 Eva Stotz 
 Author   Director 

 eva@fieldtrip.berlin 

 Fr d ric Dubois 
 Author   UX Designer 

 frederic@fieldtrip.berlin 

 Joscha J ger 
 Creative Technologist 

 joscha@fieldtrip.berlin 

 09 May 2017 
 Pitch Media Convention Berlin 

 15 Jun 2017 
 Project Kickoff 

 15 Aug 2017 
 Research shoots 

 15 Jan 2018 
 Graphic novel illustrations 

 15 Mar 2018 
 Beta Prototype 

 15 Jun 2018 
 Sound environment 

 15 Oct 2018 
 Full project live! 

 Prev 
 Next 

 ronjafilm / Peter kommt auch 
                   Am Weichselplatz 3 
                   12045 Berlin  

 Represented by: Eva Stotz 
                   Contact:  info@fieldtrip.berlin 

 Design   Programming: 
 Olivier Guillard 

 The following legal information is only in the original German language of the applicable jurisdiction. Sorry. 
 Haftungsausschluss: 
 Haftung f r Inhalte 
 Die Inhalte unserer Seiten wurden mit gr ter Sorgfalt erstellt. F r die Richtigkeit, Vollst ndigkeit und Aktualit t der Inhalte k nnen wir jedoch keine Gew hr  bernehmen. Als Diensteanbieter sind wir gem    7 Abs.1 TMG f r eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach   8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet,  bermittelte oder gespeicherte fremde Informationen zu  berwachen oder nach Umst nden zu forschen, die auf eine rechtswidrige T tigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unber hrt. Eine diesbez gliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung m glich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen. 
 Haftung f r Links 
 Unser Angebot enth lt Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb k nnen wir f r diese fremden Inhalte auch keine Gew hr  bernehmen. F r die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf m gliche Rechtsverst e  berpr ft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen. 
 Urheberrecht 
 Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielf ltigung, Bearbeitung, Verbreitung und jede Art der Verwertung au erhalb der Grenzen des Urheberrechtes bed rfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur f r den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen. 
 Datenschutz 
 Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten m glich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, soweit m glich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdr ckliche Zustimmung nicht an Dritte weitergegeben.  
                   Wir weisen darauf hin, dass die Daten bertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitsl cken aufweisen kann. Ein l ckenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht m glich.  
                   Der Nutzung von im Rahmen der Impressumspflicht ver ffentlichten Kontaktdaten durch Dritte zur  bersendung von nicht ausdr cklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdr cklich widersprochen. Die Betreiber der Seiten behalten sich ausdr cklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor. 
 Google Analytics 
 Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. (''Google''). Google Analytics verwendet sog. ''Cookies'', Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie erm glicht. Die durch den Cookie erzeugten Informationen  ber Ihre Benutzung diese Website (einschlie lich Ihrer IP-Adresse) wird an einen Server von Google in den USA  bertragen und dort gespeichert. Google wird diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports  ber die Websiteaktivit ten f r die Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte  bertragen, sofern dies gesetzlich vorgeschrieben oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Google wird in keinem Fall Ihre IP-Adresse mit anderen Daten der Google in Verbindung bringen. Sie k nnen die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht s mtliche Funktionen dieser Website voll umf nglich nutzen k nnen. Durch die Nutzung dieser Website erkl ren Sie sich mit der Bearbeitung der  ber Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden. 

 Timeline 

 Close 

 Team 

 Close 

 About 

 Close 

 Imprint 

 Close 

 An open source documentary 

 For a better user experience, please switch your device to portrait mode. 

Field Trip - An open source documentary

 Field Trip 

 en 
 de 

 Field Trip is an open source documentary about an unconventional place: Tempelhof Field - an airfield in the heart of Berlin metropolis. Field Trip tells the story of what, for some, is a 300 hectare free urban paradise, for others a place of forced labour, and for yet more a safe haven. By using the technique of open hypervideo and intense on-site and online community development, Field Trip aims at becoming a living documentary.                  

 This project is supported by MIZ-Babelsberg 

 Eva Stotz 
 Author   Director 

 eva@fieldtrip.berlin 

 Fr d ric Dubois 
 Author   UX Designer 

 frederic@fieldtrip.berlin 

 Joscha J ger 
 Creative Technologist 

 joscha@fieldtrip.berlin 

 09 May 2017 
 Pitch Media Convention Berlin 

 15 Jun 2017 
 Project Kickoff 

 15 Aug 2017 
 Research shoots 

 15 Jan 2018 
 Graphic novel illustrations 

 15 Mar 2018 
 Beta Prototype 

 15 Jun 2018 
 Sound environment 

 15 Oct 2018 
 Full project live! 

 Prev 
 Next 

 ronjafilm / Peter kommt auch 
                   Am Weichselplatz 3 
                   12045 Berlin  

 Represented by: Eva Stotz 
                   Contact:  info@fieldtrip.berlin 

 Design   Programming: 
 Olivier Guillard 

 The following legal information is only in the original German language of the applicable jurisdiction. Sorry. 
 Haftungsausschluss: 
 Haftung f r Inhalte 
 Die Inhalte unserer Seiten wurden mit gr ter Sorgfalt erstellt. F r die Richtigkeit, Vollst ndigkeit und Aktualit t der Inhalte k nnen wir jedoch keine Gew hr  bernehmen. Als Diensteanbieter sind wir gem    7 Abs.1 TMG f r eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach   8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet,  bermittelte oder gespeicherte fremde Informationen zu  berwachen oder nach Umst nden zu forschen, die auf eine rechtswidrige T tigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unber hrt. Eine diesbez gliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung m glich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen. 
 Haftung f r Links 
 Unser Angebot enth lt Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb k nnen wir f r diese fremden Inhalte auch keine Gew hr  bernehmen. F r die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf m gliche Rechtsverst e  berpr ft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen. 
 Urheberrecht 
 Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielf ltigung, Bearbeitung, Verbreitung und jede Art der Verwertung au erhalb der Grenzen des Urheberrechtes bed rfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur f r den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen. 
 Datenschutz 
 Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten m glich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, soweit m glich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdr ckliche Zustimmung nicht an Dritte weitergegeben.  
                   Wir weisen darauf hin, dass die Daten bertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitsl cken aufweisen kann. Ein l ckenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht m glich.  
                   Der Nutzung von im Rahmen der Impressumspflicht ver ffentlichten Kontaktdaten durch Dritte zur  bersendung von nicht ausdr cklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdr cklich widersprochen. Die Betreiber der Seiten behalten sich ausdr cklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor. 
 Google Analytics 
 Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. (''Google''). Google Analytics verwendet sog. ''Cookies'', Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie erm glicht. Die durch den Cookie erzeugten Informationen  ber Ihre Benutzung diese Website (einschlie lich Ihrer IP-Adresse) wird an einen Server von Google in den USA  bertragen und dort gespeichert. Google wird diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports  ber die Websiteaktivit ten f r die Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte  bertragen, sofern dies gesetzlich vorgeschrieben oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Google wird in keinem Fall Ihre IP-Adresse mit anderen Daten der Google in Verbindung bringen. Sie k nnen die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht s mtliche Funktionen dieser Website voll umf nglich nutzen k nnen. Durch die Nutzung dieser Website erkl ren Sie sich mit der Bearbeitung der  ber Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden. 

 Timeline 

 Close 

 Team 

 Close 

 About 

 Close 

 Imprint 

 Close 

 An open source documentary 

 For a better user experience, please switch your device to portrait mode. 

Field Trip - Ein interaktiver Dokumentarfilm

 Field Trip 

 en 
 de 

 Field Trip ist ein interaktiver Dokumentarfilm  ber einen unkonventionellen Ort: das Tempelhofer Feld   ein Rollfeld im Herzen Berlins. Field Trip erz hlt die Geschichte von dem, was f r die Einen ein 300 ha freies Stadtparadies voller M glichkeiten ist, f r die Anderen ein Ort der Zwangsarbeit, eine neue Heimat f r jemanden Dritten. Mit der Technik des Open Hypervideo und durch intensive Online und Onsite Community Entwicklung hat Field Trip die Ambition zu einem lebenden Dokumentarfilm zu werden.                  

 Gef rdert durch das MIZ Babelsberg 

 Eva Stotz 
 Autorin   Regisseurin 

 eva@fieldtrip.berlin 

 Fr d ric Dubois 
 Autor   UX-Designer 

 frederic@fieldtrip.berlin 

 Joscha J ger 
 Technische Konzeption 

 joscha@fieldtrip.berlin 

 09 May 2017 
 Pitch Media Convention Berlin 

 15 Jun 2017 
 Projektstart 

 15 Aug 2017 
 Erste Dreharbeiten 

 15 Jan 2018 
 Graphic novel Illustrationen 

 15 Mar 2018 
 Beta Prototyp 

 15 Jun 2018 
 Tongestaltung 

 15 Oct 2018 
 Projekt geht online! 

 Prev 
 Next 

 ronjafilm / Peter kommt auch 
                   Am Weichselplatz 3 
                   12045 Berlin  

 Vertreten durch: Eva Stotz 
                   Kontakt:  info@fieldtrip.berlin 

 Design   Programmierung: 
 Olivier Guillard 

 Haftungsausschluss: 
 Haftung f r Inhalte 
 Die Inhalte unserer Seiten wurden mit gr ter Sorgfalt erstellt. F r die Richtigkeit, Vollst ndigkeit und Aktualit t der Inhalte k nnen wir jedoch keine Gew hr  bernehmen. Als Diensteanbieter sind wir gem    7 Abs.1 TMG f r eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach   8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet,  bermittelte oder gespeicherte fremde Informationen zu  berwachen oder nach Umst nden zu forschen, die auf eine rechtswidrige T tigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unber hrt. Eine diesbez gliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung m glich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen. 
 Haftung f r Links 
 Unser Angebot enth lt Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb k nnen wir f r diese fremden Inhalte auch keine Gew hr  bernehmen. F r die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf m gliche Rechtsverst e  berpr ft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen. 
 Urheberrecht 
 Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielf ltigung, Bearbeitung, Verbreitung und jede Art der Verwertung au erhalb der Grenzen des Urheberrechtes bed rfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur f r den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen. 
 Datenschutz 
 Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten m glich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, soweit m glich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdr ckliche Zustimmung nicht an Dritte weitergegeben.  
                   Wir weisen darauf hin, dass die Daten bertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitsl cken aufweisen kann. Ein l ckenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht m glich.  
                   Der Nutzung von im Rahmen der Impressumspflicht ver ffentlichten Kontaktdaten durch Dritte zur  bersendung von nicht ausdr cklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdr cklich widersprochen. Die Betreiber der Seiten behalten sich ausdr cklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor. 
 Google Analytics 
 Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. (''Google''). Google Analytics verwendet sog. ''Cookies'', Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie erm glicht. Die durch den Cookie erzeugten Informationen  ber Ihre Benutzung diese Website (einschlie lich Ihrer IP-Adresse) wird an einen Server von Google in den USA  bertragen und dort gespeichert. Google wird diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports  ber die Websiteaktivit ten f r die Websitebetreiber zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen zu erbringen. Auch wird Google diese Informationen gegebenenfalls an Dritte  bertragen, sofern dies gesetzlich vorgeschrieben oder soweit Dritte diese Daten im Auftrag von Google verarbeiten. Google wird in keinem Fall Ihre IP-Adresse mit anderen Daten der Google in Verbindung bringen. Sie k nnen die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht s mtliche Funktionen dieser Website voll umf nglich nutzen k nnen. Durch die Nutzung dieser Website erkl ren Sie sich mit der Bearbeitung der  ber Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und Weise und zu dem zuvor benannten Zweck einverstanden. 

 Timeline 

 Close 

 Team 

 Close 

 das Projekt 

 Close 

 Impressum 

 Close 

 Ein interaktiver Dokumentarfilm 

 For a better user experience, please switch your device to portrait mode. 

https://digitalsocial.eu/project/1310/field-trip 

<p><img src="https://pbs.twimg.com/profile_images/861512076556214272/chpLLWge_400x400.jpg" width="400" height="400" /></p>
<p><a href="http://fieldtrip.berlin">Field Trip</a> is an open source documentary about an unconventional place: Tempelhof Field - an airfield in the heart of Berlin metropolis. Field Trip tells the story of what, for some, is a 300 hectare free urban paradise, for others a place of forced labour, and for yet more a safe haven. By using the technique of open hypervideo and intense on-site and online community development, Field Trip aims at becoming a living documentary.</p>
WHOLE PROJECT MARK