Fuel

format('U');? "/ 

 Without stories our lives would be meaningless. It is only through storied accounts that we are able to contemplate our own existence, inform our decisions and give sense to the intricacies of social life.

 Fuel  lets you read and react on short, bite-sized fictional accounts written in the literary form of screenplays. It is not about informing a friend that you are driving to work, making coffee or watching the game.  Fuel  is about simulating human social interaction.

	Further particulars will be announced as we near the launch of this social writing network.

 Meanwhile,  share  this page or  follow  us on Twitter  

 English Deutsch Italiano 
 Data Privacy 
   Fuel Socl LLC. Site Notice 

 The pilot phase is endorsed by 

Fuel

	Unser Leben h tte ohne Geschichten keine Bedeutung. Nur durch sie k nnen wir  ber unser Dasein reflektieren, Entscheidungen f llen und den Unannehmlichkeiten im Alltag einen Sinn geben. 
	Auf  Fuel  k nnen Besucher kurze Szenen in Form eines Drehbuchs lesen und darauf reagieren. Es geht also nicht darum einem Freundeskreis mitzuteilen, dass man gerade zur Arbeit f hrt oder sich das Fu ballspiel ansieht, sondern um die soziale Interaktion in einer fiktiven Welt. 
    Weitere Einzelheiten wird es geben, sobald die Lancierung dieses  social writing  Netzwerks bevorsteht. 
 Unterdessen,  teile  diese Seite oder  folge  uns auf Twitter  

 English Deutsch Italiano 
 Datenschutz  (engl.) 
   Fuel Socl GmbH. Impressum 

 Die Pilotphase wird unterst tzt von 

Fuel - Legal

 DATA PRIVACY 

Last update: May 24, 2017 
 Fuel Socl LLC  (also referred to as  Fuel ,  we ,  our , or  us ) wants you to be informed about the data it collects from the users of this website (the  Site ). Our data privacy policy ( Policy ) describes the policies and procedures regarding the collection, use, transfer, and protection of this data. Please take a moment to familiarize yourself with our privacy practices. 

The  Last update  date above indicates the effective date of this Policy.  Fuel  reserves the right to revise this Policy at its discretion. In such a case, we will change the  Last update  date above. 

We will not use or share your data with anyone except as described in this Policy. By using the Site, you agree to the collection and use of data in accordance with this Policy. 
 Web analysis 

We employ a tool called  Google Analytics , a web analytics service provided by Google, Inc. ( Google ), to collect data such as how often users visit the Site, what pages they visit when they do so, and what other websites they used prior to coming to this Site. 

Google Analytics uses  cookies . A cookie is a short text file that enables the exchange of data between computer programs. These cookies do not contain any personal information. 

The data generated by the cookie about your use of the Site will be transmitted to and stored by Google on servers in the United States. Google will use this data for the purpose of evaluating your use of the Site, compiling reports on site activity and providing other services relating to internet usage. Google may also transfer this data to third parties where required to do so by law, or where such third parties process the data on Google's behalf. Google will not associate your IP address with any other information held by Google. 

We use the data we get from Google Analytics to improve this Site. Google Analytics collects only the IP address assigned to you on the date you visit this Site, rather than your name or other identifying information. We do not combine the data collected through the use of Google Analytics with personally identifiable information. Although Google Analytics plants a permanent cookie on your web browser to identify you as a unique user the next time you visit this Site, the cookie cannot be used by anyone but Google. 

If you do not wish to have your information collected using cookies, you can typically disable the use of cookies (or disable the use of specific cookies) by adjusting your browser settings. Please note that certain features of this Site will not be available once cookies are disabled. 
 Third parties 

The  Fuel  website includes links to other websites maintained by third parties. We are not responsible for the privacy practices or the content of those websites. Before providing any information to such third parties, you should carefully read their own privacy policies. 
 Social media buttons 

Our Site contains links or buttons that enable you to share or post content to third-party social media platforms. If you click on one of these buttons (e.g. the 'share' plug-in from Twitter), a connection to the servers of that website may be established automatically. This may result in transmission of information about your visit to this Site, and in some cases it may be possible to associate the data with your user account (e.g. your Twitter account). 

The privacy policy of such social media platforms will apply to any content you share or post, and  Fuel  is not responsible for the privacy practices of such third-party platforms. 
 Security 

Remember that no method of transmission over the internet, or method of electronic storage is fully secure against interception. Although we have taken every reasonable step to prevent your data from being disclosed through data transfer errors and/or unauthorised access by third parties, we cannot accept any liability for such unwanted incidents. 

Fuel - Legal

 SITE NOTICE 
 Fuel Socl LLC 
Burgunderstrasse 36 
4051 Basel 
Switzerland

 Email 

 Commercial register 
CHE-457.052.433

Fuel

	La vita senza racconti non avrebbe un significato;   solo tramite essi che riusciamo a riflettere sulla nostra esistenza, a prendere decisioni e dare un senso alle contrariet  della vita quotidiana. 
 Fuel  rende possibile agli utenti di leggere e reagire a dei racconti brevi in forma di sceneggiatura. Quindi non si tratta di far sapere a un giro di amici che andiamo a lavoro o che stiamo guardando la partita di calcio, bens  di una interazione sociale in un mondo fittizio. 
    Ulteriori particolari verranno resi noti non appena sar  imminente il lancio di questo  social writing network . 
 Nel frattempo  condividi  questa pagina o  seguici  su Twitter  

 English Deutsch Italiano 
 Privacy  (ingl.) 
   Fuel Socl Srl. Nota Legale 

 La fase pilota   sostenuta da 

Fuel

format('U');? "/ 

 Without stories our lives would be meaningless. It is only through storied accounts that we are able to contemplate our own existence, inform our decisions and give sense to the intricacies of social life.

 Fuel  lets you read and react on short, bite-sized fictional accounts written in the literary form of screenplays. It is not about informing a friend that you are driving to work, making coffee or watching the game.  Fuel  is about simulating human social interaction.

	Further particulars will be announced as we near the launch of this social writing network.

 Meanwhile,  share  this page or  follow  us on Twitter  

 English Deutsch Italiano 
 Data Privacy 
   Fuel Socl LLC. Site Notice 

 The pilot phase is endorsed by 

Fuel - Legal

 IMPRESSUM 
 Fuel Socl GmbH 
Burgunderstrasse 36 
4051 Basel 
Schweiz

 E-Mail 

 Handelsregister 
CHE-457.052.433

Fuel - Legal

 NOTA LEGALE 
 Fuel Socl Srl 
Burgunderstrasse 36 
4051 Basilea 
Svizzera

 E-mail 

 Registro di commercio 
CHE-457.052.433

Fuel is a social writing network that lets users read and react on short fictional accounts written in the literary form of screenplays. 

<p><em>Fuel</em> is an online social writing network that enables users to read and participate in the continuation of short, bite-sized fictional accounts written in the literary form of a screenplay.</p>
<p><em>Fuel</em> is about enacting different roles in a given fictional space and exploring the many possibilities that can arise from participating in the continuation of an unfinished scene. By asking members to answer the question "What would you do, if&hellip;?", <em>Fuel</em> nourishes our imagination and gives us safe and compelling worlds in which to practice.</p>
WHOLE PROJECT MARK