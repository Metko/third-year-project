Home | Piazza, The smart city co-creation platform

   EN DE FR IT EN DE FR IT   

			Home		 

 Salem Tirane   

			2016-11-15T11:20:18+00:00		 

 Loading... 

 Where we all imagine our city of tomorrow   

 Going real with LE TUBA in Lyon   

 SMART CITY CO-CREATION 

 Piazza is an online platform to bring together Citizens, Businesses and the City to co-create together their city of tomorrow. As one community, they will join on a dedicated website to share ideas, discuss, comment and vote to build a better and smarter city. 
 Piazza provides more than ideas: 

 It helps city decision makers better understand citizens need thanks to its data analytics features. 
 It gives a voice to all citizens in an easy and fun way by rewarding them based on their engagement. 
 It connects businesses and citizens to invent and improve product and services. 

 WHAT IS PIAZZA? 
 Piazza is a Digital Living Lab platform for (medium) cities allowing city authorities and service providers to design and test new urban infrastructure or services with citizen before entering the planning or implementation phase. 

 WHY PIAZZA? 
 Smart city is usually defined as citizen centric with ICT at its core but most decision makers don t know what this actually means and how to achieve it. 

 WHO USES PIAZZA? 
 Piazza makes the nebulous concept of Smart City real and relevant for cities, citizens and businesses. Through the ICT-based participation of all stakeholders in creating sustainable city services various positive effects are achieved. 

 WHAT MAKES PIAZZA DIFFERENT TO THE SMART DATA CITY PLATFORMS? 

 Classic data platforms are often  Smart  Administration centric with a top-down planning approach, technically focused on processing sensor data and aiming the maximum efficiency of its components (life environment, energy, mobility, etc.) in time, cost and performance. PIAZZA is Smart Citizen centric with a bottom-up participatory or collaborative approach, focused on Citizens demand, opinion and behavior and aiming maximum effectivity in creating life comfort, social and ecological values. Both approaches and platform types are complementarily interconnected , supported by state-of-the-art technologies (e.g. mobile tech), impact each other and define a fundamental holistic concept that ensure  the sustainable success of the Smart City concept. The Smart City Administration hereby provides information   proposals on all relevant Smart City topics, i.e. planned measures, sensor based data information and other, whereas the Smart Citizens participate collaboratively with behavioral issues (ideas, needs, problems, wishes, etc.), which are individually elicited, analyzed, discussed, or provide validation   verification, decisions (e.g. acceptance) or valuable feedback. 
 PIAZZA allows for an incremental on-boarding and engagement of citizens, lowering the entry barrier to the conversation between the city stakeholders, by using innovative and fun interaction means (i.e. citizens informally  talk  to urban objects to send messages to the official city counterparts) 

 PIAZZA HIGHLIGHTS 

 Turn key and Tailor Made collaborative platform Integrated or dedicated website Gamification ready with rewards and badges 

 Analytical Dashboard Easy moderation and user management On demand Text Mining and Semantic Analysis 

 TALKIN  PIAZZA 

 Geo-located application to chat with urban objects 

 Collection of citizens  contributions while on-the-go 

 Fun and informal conversation with the city stakeholders 

 Download our factsheet PARTNERS 

 EIT DIGITAL INNOVATION FRAMEWORK 

 From smart cities to smart citizens 
 Digital Cities Action Line deals with Smart Cities  
  But there will be no Smart Cities if there are no Smart Citizens. Emergence of new behaviours   the way people move, work, entertain, socialize   will be at the origin of new upcoming business models. When I say  people  it is not a generic term. It is you, it is me, it is all of us. 

 GOING REAL 

 Living lab for technology and experimentation Community for entrepreneurs and citizens Inspiration for cultural transformation 

 Proven process to accompany enterprises, SMEs and startups from problem definition to tested commercial solutions 

 Enable collaboration among partners, SMEs, enterprises, clusters, government, startups, academics, innovators, citizens and end-users,  

 Specialized in smart city and big data related innovations within the context of quality of life in an urban and suburban setting 

 Events 
 Thanks to the  EIT Digital , we  will be exhibiting at  Metropolitan Solution 2016 . 
 Come and see us in Berlin from May 31st to  June 2nd 2016. 
 Booth: Halle B, Stand A40 

 Want to see a live demo of the Piazza platform. Join us  at Innovative Cities in Nice, France on June 16-17th. 
 Tweets by @piazza_eu 

 Contact us 

 Your Name (required) 

 Your Email (required) 

 Your are: 

 Subject 

 Your Message 

 Follow us 

 Copyright 2016 PIAZZA | All Rights Reserved | Supported by  EIT DIGITAL 

 EN 

 DE 

 FR 

 IT 

 We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. OK 

Salem Tirane, Author at Piazza, The smart city co-creation platform

   EN EN   

			About  Salem Tirane   
		This author has not yet filled in any details. So far Salem Tirane has created 0 blog entries.	 

 Follow us 

 Copyright 2016 PIAZZA | All Rights Reserved | Supported by  EIT DIGITAL 

 EN 

 We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. OK 

Home | Piazza, The smart city co-creation platform

   EN DE FR IT EN DE FR IT   

			Home		 

 Salem Tirane   

			2016-11-15T11:17:31+00:00		 

 Loading... 

 Sperimentazione con Le TUBA di Lione   

 Dove noi tutti immaginiamo la nostra citt  di domani   

 PANORAMICA DI PIAZZA 

 Piazza   una piattaforma per la co-creazione della smart city, in grado di offrire un approccio unico per soddisfare la richiesta di smart city partecipativa. Piazza percepisce i bisogni dei cittadini sulla base dei loro feedback e delle loro aspettative. 

 COS  PIAZZA? 
 Piazza   una piattaforma che si caratterizza come  Digital Living Lab  per le citt  (di medie dimensioni) che permette alle autorit  cittadine e ai fornitori di servizi di progettare e sperimentare con i cittadini nuove infrastrutture o servizi urbani prima di entrare nella fase di pianificazione o implementazione. 

 PERCH  PIAZZA? 
 L espressione  smart city  di solito indica una citt  che mette al centro il cittadino con l ICT come fulcro, ma molti decisori non sanno cosa questo significhi realmente e come ottenerlo. 

 CHI USA PIAZZA? 
 Piazza rende il nebuloso concetto di Smart City concreto e rilevante per le citt , i cittadini e le attivit  di business. Tramite la partecipazione, incentrata sull ICT, di tutti gli stakeholder nel creare servizi cittadini sostenibili, si possono ottenere diversi effetti positivi. 

 COSA RENDE PIAZZA DIVERSA DALLE PIATTAFORME CITTADINE DI SMART DATA? 

 Le piattaforme di dati classiche sono spesso incentrate su una Amministrazione  smart  con un approccio alla pianificazione di tipo top-down, tecnicamente focalizzato sull elaborazione di dati provenienti dai sensori e puntando alla massima efficienza dei suoi componenti (ambiente di vita, energia, mobilit , ecc.) dal punto di vista di tempo, costo e prestazione. 

 PIAZZA   incentrata sul cittadino  smart  con un approccio di tipo partecipativo bottom-up o collaborativo, focalizzato sulle richieste, opinioni e sui comportamenti dei cittadini e puntando alla massima efficienza nel creare comfort di vita, valori sociali ed ecologici. 

 Entrambi gli approcci e i tipi di piattaforma sono interconnessi in modo complementare, supportati da tecnologie all avanguardia (per es. la tecnologia mobile),impattano l uno sull altro e definiscono un concetto olistico fondamentale che assicura il successo sostenibile del concetto di Smart City. 

 L Amministrazione della Smart City fornisce informazioni e proposte su tutti gli argomenti rilevanti della Smart City, vale a dire provvedimenti pianificati, informazioni legate ai dati provenienti dai sensori e altro, mentre i cittadini  smart  partecipano in modo collaborativo con contributi comportamentali (idee, bisogni, problemi, desideri, ecc.), che sono stimolati individualmente, analizzati, discussi, o forniscono conferme   verifiche, decisioni (per es. approvazioni) o feedback di valore. 

 PUNTI DI FORZA DI PIAZZA 

 Piattaforma collaborativa preconfigurata e realizzata su misura 

 Sito web integrato o dedicato 

 Gamification pronta con ricompense e riconoscimenti 

 Dashboard analitico 

 Moderazione semplice e gestione degli utilizzatori 

 Su richiesta mining del testo e analisi semantica 

 TALKIN  PIAZZA 

 Applicazione geo-localizzata per chiacchierare con gli oggetti urbani 

 Raccolta dei contributi dai cittadini in movimento 

 Comunicazione divertente ed informale con gli stakeholder cittadini 

 Scarica la nostra scheda informativa PARTNER 

 FRAMEWORK DI INNOVAZIONE DI EIT DIGITAL 

 Dalle citt  smart ai cittadini smart 
 La Linea di Azione EIT  Digital Cities  si occupa di Smart Cities  
  Ma non possono esistere Citt  Intelligenti senza Cittadini Intelligenti. I nuovi comportamenti emergenti   le modalit  con cui la gente si muove, lavora, si diverte e socializza   saranno alla base dei prossimi nuovi modelli di business. E quando diciamo  gente , non intendiamo un termine generico. Si tratta di te, di me, di tutti noi. 

 SPERIMENTAZIONE 

 Laboratorio vivo per la tecnologia e la sperimentazione Comunit  per gli imprenditori e i cittadini Ispirazione per la trasformazione culturale. 

 Processo verificato per accompagnare le imprese, PMI e startup dalla definizione del problema alle soluzioni commerciali testate. 

 Abilitare la collaborazione tra i partner, le PMI, le imprese, i cluster, il governo, le startup, le universit , gli innovatori, i cittadini e gli utenti finali,  

 Specializzata in innovazioni legate a smart city e big data nell ambito della qualit della vita in un contesto urbano e suburbano 

 Eventi 
 Grazie a  EIT Digital ,   saremo presenti al  Metropolitan Solution 2016 . 
  Vieni a trovarci a Berlino dal 31 maggio al 2 giugno 2016. 
 Postazione: Halle B, Stand A40 

 Se ti interessa vedere una dimostrazione dal vivo della piattaforma Piazza. Inisciti a noi a  Innovative City  a Nizza, Francia, il 16 e 17 giugno. 
 Tweets by @piazza_eu 

 Contattaci 

 Nome  

 Email  

 Sei: 

 Oggetto 

 il tuo messaggio 

 Follow us 

 Copyright 2016 PIAZZA | All Rights Reserved | Supported by  EIT DIGITAL 

 EN 

 DE 

 FR 

 IT 

 We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. OK 

Home | Piazza, The smart city co-creation platform

   EN DE FR IT EN DE FR IT   

			Home		 

 Salem Tirane   

			2016-11-15T11:20:18+00:00		 

 Loading... 

 Where we all imagine our city of tomorrow   

 Going real with LE TUBA in Lyon   

 SMART CITY CO-CREATION 

 Piazza is an online platform to bring together Citizens, Businesses and the City to co-create together their city of tomorrow. As one community, they will join on a dedicated website to share ideas, discuss, comment and vote to build a better and smarter city. 
 Piazza provides more than ideas: 

 It helps city decision makers better understand citizens need thanks to its data analytics features. 
 It gives a voice to all citizens in an easy and fun way by rewarding them based on their engagement. 
 It connects businesses and citizens to invent and improve product and services. 

 WHAT IS PIAZZA? 
 Piazza is a Digital Living Lab platform for (medium) cities allowing city authorities and service providers to design and test new urban infrastructure or services with citizen before entering the planning or implementation phase. 

 WHY PIAZZA? 
 Smart city is usually defined as citizen centric with ICT at its core but most decision makers don t know what this actually means and how to achieve it. 

 WHO USES PIAZZA? 
 Piazza makes the nebulous concept of Smart City real and relevant for cities, citizens and businesses. Through the ICT-based participation of all stakeholders in creating sustainable city services various positive effects are achieved. 

 WHAT MAKES PIAZZA DIFFERENT TO THE SMART DATA CITY PLATFORMS? 

 Classic data platforms are often  Smart  Administration centric with a top-down planning approach, technically focused on processing sensor data and aiming the maximum efficiency of its components (life environment, energy, mobility, etc.) in time, cost and performance. PIAZZA is Smart Citizen centric with a bottom-up participatory or collaborative approach, focused on Citizens demand, opinion and behavior and aiming maximum effectivity in creating life comfort, social and ecological values. Both approaches and platform types are complementarily interconnected , supported by state-of-the-art technologies (e.g. mobile tech), impact each other and define a fundamental holistic concept that ensure  the sustainable success of the Smart City concept. The Smart City Administration hereby provides information   proposals on all relevant Smart City topics, i.e. planned measures, sensor based data information and other, whereas the Smart Citizens participate collaboratively with behavioral issues (ideas, needs, problems, wishes, etc.), which are individually elicited, analyzed, discussed, or provide validation   verification, decisions (e.g. acceptance) or valuable feedback. 
 PIAZZA allows for an incremental on-boarding and engagement of citizens, lowering the entry barrier to the conversation between the city stakeholders, by using innovative and fun interaction means (i.e. citizens informally  talk  to urban objects to send messages to the official city counterparts) 

 PIAZZA HIGHLIGHTS 

 Turn key and Tailor Made collaborative platform Integrated or dedicated website Gamification ready with rewards and badges 

 Analytical Dashboard Easy moderation and user management On demand Text Mining and Semantic Analysis 

 TALKIN  PIAZZA 

 Geo-located application to chat with urban objects 

 Collection of citizens  contributions while on-the-go 

 Fun and informal conversation with the city stakeholders 

 Download our factsheet PARTNERS 

 EIT DIGITAL INNOVATION FRAMEWORK 

 From smart cities to smart citizens 
 Digital Cities Action Line deals with Smart Cities  
  But there will be no Smart Cities if there are no Smart Citizens. Emergence of new behaviours   the way people move, work, entertain, socialize   will be at the origin of new upcoming business models. When I say  people  it is not a generic term. It is you, it is me, it is all of us. 

 GOING REAL 

 Living lab for technology and experimentation Community for entrepreneurs and citizens Inspiration for cultural transformation 

 Proven process to accompany enterprises, SMEs and startups from problem definition to tested commercial solutions 

 Enable collaboration among partners, SMEs, enterprises, clusters, government, startups, academics, innovators, citizens and end-users,  

 Specialized in smart city and big data related innovations within the context of quality of life in an urban and suburban setting 

 Events 
 Thanks to the  EIT Digital , we  will be exhibiting at  Metropolitan Solution 2016 . 
 Come and see us in Berlin from May 31st to  June 2nd 2016. 
 Booth: Halle B, Stand A40 

 Want to see a live demo of the Piazza platform. Join us  at Innovative Cities in Nice, France on June 16-17th. 
 Tweets by @piazza_eu 

 Contact us 

 Your Name (required) 

 Your Email (required) 

 Your are: 

 Subject 

 Your Message 

 Follow us 

 Copyright 2016 PIAZZA | All Rights Reserved | Supported by  EIT DIGITAL 

 EN 

 DE 

 FR 

 IT 

 We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. OK 

Home | Piazza, The smart city co-creation platform

   EN DE FR IT EN DE FR IT   

			Home		 

 Salem Tirane   

			2017-01-23T09:51:22+00:00		 

 Loading... 

 Going real mit Le TUBA in Lyon   

 wo wir uns alle unsere Stadt von morgen vorstellen   

 PIAZZA  BERSICHT 

 Piazza ist eine Plattform, die es erm glicht die zuk nftige Smart City gemeinsam mit B rgern zu gestalten. Piazza erfasst die Bed rfnisse der B rger durch ihre eigenen R ckmeldungen und bietet die M glichkeit daraus in Zusammenarbeit mit Entscheidungstr gern innovative Ideen und L sungsans tze f r die Zukunft zu entwickeln. 

 WAS IST PIAZZA ? 
 Piazza ist eine digitale Plattform, die zurzeit unter realen Bedingungen getestet wird. Sie unterst tzt die Kommunikation an der Schnittstelle zwischen Stadtbeh rden und Dienstleistern, um bedarfsgerecht neue st dtische Infrastrukturen und Dienste mit B rgern zu gestalten und zu erproben, bevor diese in die Durchf hrungsphase gehen. 

 WARUM PIAZZA ? 
 Piazza hilft dabei, den Begriff und die Vision der Smart City konkreter zu erfassen und umzusetzen sowie mit der einfachen Handhabung des multifunktionalen Tools die Realisierung von gemeinsam erdachten smarten Projekten zu unterst tzen. 

 WER NUTZT PIAZZA ? 
 St dte, B rger und Unternehmen profitieren von der Idee von Piazza! Durch die IT-basierte Beteiligung aller Akteure an der Gestaltung nachhaltiger und innovativer Stadtdienstleistungen sollen bestm gliche effektive L sungsans tze und Konzepte erarbeitet werden. 

 WAS MACHTPIAZZA BESONDERS IM VERGLEICH ZU ANDEREN SMART CITY KO-KREATION PLATTFORMEN? 

 Klassischen Datenplattformen im Smart City Bereich sind oft zu verwaltungszentriert und reproduzieren klassische top-down Prozesse. Dar ber hinaus ist ihr Ziel die Effizienzmaximierung bestehender L sungen in Bezug auf Zeitaufwand, Kosten und Leistungsertrag. 
 Piazzas Ausgangspunkt ist der B rger selbst. Bei der Plattform liegt der Fokus auf einem bottom-up Ansatz, der durch kollaborative Ideenfindung B rgerbed rfnissen nachkommt. Die Steigerung der Lebensqualit t sowie die Ber cksichtigung von sozialen und  kologischen Werten sind die Hauptziele. 
 Durch Piazza werden bestehende Ans tze und neue Formate auf komplement re Weise miteinander vereint. Von state-of-the-art Technologien unterst tzt (z.B. mobile Technologie), beeinflussen sie sich gegenseitig und definieren ein fundamentales und ganzheitliches Konzept, das zu dem nachhaltigen Erfolg der Smart City Vision beitr gt. 

 Als Teil der smarten Verwaltungsstrukturen bietet Piazza Informationen und Ideenfindungsprozesse zu allen Smart City bezogenen Themen wie z.B. geplante Ma nahmen oder sensorbasierte Dateninformationen. Dabei beteiligt sich der B rger kollaborativ indem er Ideen, Probleme und W nsche  ber Piazza mitteilt und diese mit anderen diskutiert, analysiert und weiterbearbeitet. Mit Piazza ist die M glichkeit einer Abstimmung und R ckkopplung gegeben, um Vorschl ge zu konkretisieren und Meinungsbilder zu erfassen. 

 PIAZZA HIGHLIGHTS 

 Ko-Kreation- App-basiert mitgestalten 
 ber Vorschl ge und Vorhaben mobil abstimmen 

 Mit spielerischem Ansatz die Stadt gestalten 

 Leicht bedienbare Analyse- und Auswertungswerkzeuge 

 Einfaches Nutzungsmanagement und Moderationst tigkeit 

 Text mining und semantische Analyse auf Anfrage 

  Daten   Fakten herunterladen  GOING REAL 

 Reallabor f r Technologie und Experimente, Gemeinschaft f r Unternehmer und B rger, Inspiration f r kulturelle Transformationen 

 Bew hrte Prozesse zur Begleitung (von/durch) Unternehmen, KMUs und Startups von der Problembeschreibung bis zur getesteten kommerziellen L sungen 

 Erm glicht die Kollaboration zwischen Partnern, KMUs, Unternehmen, Clusters, Regierungen, Wissenschaftlern, Innovatoren, B rger und Endnutzern 

 Spezialisiert auf Smart City und Big Data Innovationen mit Bezug auf Lebensqualit t in st dtischem und vorst dtischem Kontext 

EIT DIGITAL INNOVATIONSRAHMEN 

 Von smart cities zu smart citizens 
 Digital Cities Action Line setzt sich mit Smart Cities auseinander  
  Aber keine Smart Cities ohne Smart Citizens. Emergenz neuer Verhalten   die Art wie Menschen sich bewegen, arbeiten und sozialisieren   wird der Ursprung neuer Business Models. Wenn ich  Menschen  sage, ist das kein Oberbegriff. Das bist Du, das bin ich. Das sind wir alle. 

 PARTNER 

 Tweets by @piazza_eu 
 Veranstaltungen 
  Dank des  EIT Digital , werden wir  im  Metropolitan Solution 2016 ausstellen . 
 Besuchen Sie uns in Berlin vom 31. Mai bis zum 2. Juni 2016. 
 Standort: Halle B, Stand A40 

 M chten Sie eine Live-Demo der Piazza Plattform erleben. Besuchen Sie uns auf die Messe  Innovative City  in Nice, Frankreich am 16-17 Juni. 

 Kontaktiere uns 

 Dein Name (erforderlich) 

 DeinEmail (erforderlich) 

 Sie sind: 

 Subjekt 

 Ihre Nachricht 

 Follow us 

 Copyright 2016 PIAZZA | All Rights Reserved | Supported by  EIT DIGITAL 

 EN 

 DE 

 FR 

 IT 

 We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. OK 

Home | Piazza, The smart city co-creation platform

   EN DE FR IT EN DE FR IT   

			Home		 

 Salem Tirane   

			2016-11-15T11:11:49+00:00		 

 Loading... 

 Imaginons ensemble notre ville de demain !   

 Exp rimentation avec LE TUBA   Lyon   

 APER U  

 Piazza est une plate-forme collaborative d di e   la co-cr ation de la ville intelligente apportant une fa on unique pour r pondre   la demande de la   smart city   participative. 
Piazza d tecte les besoins des citoyens en fonction de leurs commentaires et leurs attentes. 
 Piazza fournit plus que des id es: 

 Il aide les d cideurs urbains   mieux comprendre les besoins des citoyens gr ce   ses fonctionnalit s d analyse de donn es. 
 Il donne une voix   tous les citoyens d une mani re facile et amusante en les r compensant sur la base de leur engagement. 
 Il relie les entreprises et les citoyens pour inventer et am liorer les produits et services. 

 QU EST-CE QUE PIAZZA? 
 Piazza est une plate-forme/Living Lab num rique pour les villes (de tailles moyennes) permettant aux autorit s de la ville et aux fournisseurs de services de concevoir et de tester de nouvelles infrastructures urbaines ou des services avec les citoyens avant d entrer dans la phase de planification ou de mise en  uvre. 

 POURQUOI UTILISER PIAZZA? 
 Une ville intelligente est g n ralement d finie comme une centr e sur les citoyens avec un noyau de TIC. Malheureusement, la plupart des d cideurs ne savent pas ce que cela signifie r ellement et comment y parvenir. 

 QUI UTILISE PIAZZA? 
 Avec Piazza, le concept de Smart City devient r el et pertinent pour les villes, les citoyens et les entreprises. 
Gr ce   une participation   bas e sur les TIC- de tous les acteurs dans la cr ation de services durables de la ville, divers effets positifs sont obtenus. 

 CE QUI DIFF RENCIE PIAZZA DES AUTRES PLATEFORMES DEDI ES AUX VILLES INTELLIGENTES? 

 Les plateformes de donn es classiques sont souvent centr es sur une administration intelligente avec une approche de planification descendante, qui met l accent, techniquement, sur le traitement des donn es de capteurs tout en visant l efficacit  maximale de ses composantes (environnement de vie,  nergie, mobilit , etc.) selon les param tres suivants: le temps, le co t et la performance. PIAZZA est centr  sur le citoyen intelligent et bas  sur une participation ascendante ou une approche collaborative, orient  vers la demande, l opinion et le comportement des citoyens et visant l effectivit  maximale dans la cr ation du confort de vie ainsi que des valeurs sociales et  cologiques. Les approches et les plateformes sont compl mentairement interconnect s. Soutenues par les technologies state-of-the-art (par exemple les technologies mobiles), elles impactent les unes les autres et d finissent un concept fondamental et global qui assure le succ s durable de la notion de la ville intelligente. L administration de la ville intelligente procure des informations et des propositions sur tous les sujets pertinents des villes intelligentes, c est   dire des mesures plannifi s, des capteurs bas s sur l informations de donn es, etc. Alors que les citoyens intelligents participent en collaboration avec des probl mes de comportement (des id es, des besoins, des probl mes, des souhaits, etc.), qui sont individuellement suscit s, analys s, discut s, ou fournissent la validation et la v rification, les d cisions (par exemple, l acceptation) ou des pr cieux commentaires. 

 LES POINTS FORTS DE PIAZZA 

 Plateforme web collaborative de co-cr ation. Site web d di  et int gr . Gamification via des r compenses et des badges 

 Tableau de bord analytique Mod ration facile et gestion des utilisateurs Text Mining / Analyse s mantique. 

 T l chargez notre Fiche d information PARTENAIRES 

  FRAMEWORK EIT D INNOVATION DIGITALE 

 Des villes intelligentes aux citoyens intelligents 
 La ligne d action des villes num riques explore les villes intelligentes 
  Mais il n y aura pas de Villes Intelligentes s il n y a pas des citoyens intelligents. L mergence de nouveaux comportements   la fa on dont les gens se d placent, le travail, se divertir, de socialiser   sera   l origine de nouveaux mod les d affaires   venir. Quand nous disons  peuple , il ne s agit pas du terme g n rique. C est vous, c est moi, c est nous tous. 

 EXPERIMENTATIONS 

 Le living lab pour la communaut  de la technologie et de l exp rimentation, pour les entrepreneurs et les citoyens, pour la transformation culturelle. 

 Processus  prouv  pour accompagner les entreprises, les PME et les start-ups de la d finition des probl mes aux testes des solutions commerciales. 

 Activer la collaboration entre les partenaires, les PME, les entreprises, les clusters, le gouvernement, les start-ups, les universitaires, les innovateurs, les citoyens et les utilisateurs finaux,  

 Sp cialis  dans la ville intelligente et les  big data  li es aux innovations, dans un contexte d une qualit  de vie dans un milieu urbain et suburbain. 

 Tweets by @piazza_eu 
 V NEMENTS 
 Gr ce    l EIT Digital , nous   tions pr sent au salon  Metropolitan Solution 2016 , d roul    Berlin du 31 mai au 2 Juin 2016. 

 Vous voulez voir une d monstration en direct de la plate-forme Piazza. Rejoignez-nous   Innovative City   Nice, France les 16  17 Juin. 

 Contactez-nous 

 Votre Nom (obligatoire) 

 Votre Email (obligatoire) 

 Vous  tes: 

 Sujet 

 Votre Message 

 Follow us 

 Copyright 2012 - 2016 Avada | All Rights Reserved | Powered by  WordPress  |  Theme Fusion 

 EN 

 DE 

 FR 

 IT 

 We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. OK 

Salem Tirane, Author at Piazza, The smart city co-creation platform

   DE DE   

			About  Salem Tirane   
		This author has not yet filled in any details. So far Salem Tirane has created 0 blog entries.	 

 Follow us 

 Copyright 2016 PIAZZA | All Rights Reserved | Supported by  EIT DIGITAL 

 DE 

 We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. OK 

Salem Tirane, Author at Piazza, The smart city co-creation platform

   FR FR   

			About  Salem Tirane   
		This author has not yet filled in any details. So far Salem Tirane has created 0 blog entries.	 

 Follow us 

 Copyright 2012 - 2016 Avada | All Rights Reserved | Powered by  WordPress  |  Theme Fusion 

 FR 

 We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. OK 

Salem Tirane, Author at Piazza, The smart city co-creation platform

   IT IT   

			About  Salem Tirane   
		This author has not yet filled in any details. So far Salem Tirane has created 0 blog entries.	 

 Follow us 

 Copyright 2016 PIAZZA | All Rights Reserved | Supported by  EIT DIGITAL 

 IT 

 We use cookies to ensure that we give you the best experience on our website. If you continue to use this site we will assume that you are happy with it. OK 

Digital Living Lab platform allowing city authorities and service providers to design and test new urban services with citizens
WHOLE PROJECT MARK