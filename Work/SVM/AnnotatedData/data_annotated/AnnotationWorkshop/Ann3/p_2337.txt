ARE ON Emergency Care | Social Services |  ivot90

 About Us   

 Our Mission 
 Our Story in Facts and Figures 
 Statutes 
 Membership 
 Organizational Chart 

 Contact   

 Registered address 
 Reception Desk 
 Secretariat 
 Information and Consultancy Centre 

 Annual Reports   

 2010 
 2009 

 Archive   

 Pictures 

 Sponsors and Donors   

 For the Press   

 Downloads   

 Social Services 
 ARE ON Emergency Care 

 Are on Emergency Care 
 ARE ON Emergency care  is a comprehensive social service, the main purpose of which is to reduce health and social risks in senior citizens and handicapped people. The chief merit of the service is in helping handicapped people and seniors of advanced age to live in dignity, according to their wishes and in their natural environment   at home.    Are on: Past and Present: In 2009, the  IVOT 90 Civic Association marked 17 years of providing this comprehensive social service. By the end of 1992, the  IVOT 90 Control Room was connected with the first in a series of user terminal stations, installed in the homes of clients of the social service its authors chose to call ARE ON   Messenger of Quick Help. The ARE ON Emergency Care Service is today provided through the main control room in Prague and regional control rooms in Hradec Kr lov , Kutn  Hora and Jihlava to almost  1,300 clients in 34 towns in six Czech regions . In 2005,  IVOT 90 was awarded the Ministry of Health Makropoulos Prize.  The prize was awarded in recognition of an exceptional, practically asserted project, solving healthcare and health-related social requirements of seniors in the Czech Republic. Being the first provider of emergency care in the Czech Republic ever since early November 1992,  IVOT 90 has supplied high-quality care with emphasis on effective service provision, thanks largely to user equipment enabling two-way voice communication in case of emergency calls. As the founders and pioneers of the service in the Czech Republic, we have managed to incorporate it in legislation on social services, Act 108/2006 Coll., in the care services category. This enables the user to pay monthly service fees using his/her care bonuses.   Who the service is intended for: Seniors Handicapped persons    seniors and handicapped persons in adverse life situations, facing health and social risks    families of seniors and handicapped citizens    persons taking care of seniors and handicapped citizens  other entities providing social and health services to seniors and handicapped citizens   municipalities, city boroughs and communities   ARE ON emergency care comprises the following activities:   Provision or mediation of emergency care in:  unexpected situations (injury, fall etc.)  unforeseen critical situations (sudden deterioration of health etc.)  emergency situations (assault or threat by another person)  social consultancy   social therapeutic work  arranging contacts with social environment  assistance in exercising rights, legitimate interests and procurement of personal matters       Objectives of ARE ON emergency care: The objective of emergency care is to reduce social, health and security risks brought about by these citizens' way of life:  maximally preserve self-sufficiency and independence of elderly persons and handicapped people  ensure maximum possible longevity of people of advanced age and handicapped people in their natural social environment  do away with social exclusion of elderly people living in loneliness  reduce the ever-growing number of risks (health, social and criminal)  help senior and handicapped people to assert their rights and interest and to keep in touch with their social environment  provide social, health and general consultancy  reduce rate of admission to healthcare institutions for social reasons  reduce the number of persons waiting to be admitted to social care institutions  help families take care of their senior or handicapped members (respite care)   Criteria of provision of service:  the applicant's ability to take care of him/herself and his/her household is reduced due to health handicap and psychosomatic problems  impaired mobility or old age  applicant is older than 60 years  living alone  applicant is younger than 60 and handicapped  remote place of residence exposing a senior to considerable psychical stress  admission based on the opinion of regional and municipal social departments; in many cases service is provided at their request   Criteria of refusal to provide service:  applicant's mental condition would contraindicate provision of service  combined visual and hearing disorders or severe hearing problems prevent long-distance voice communication Parties working together to provide emergency care:  integrated rescue system (Czech Police, fire chief, rescue service)  ambulance service  town police  close person (family, neighbour etc.) specified by applicant's request, made upon provider's spot inspection  general practitioner or attending physician  community care service, personal assistance and any social service indicated by the application   another person or entity, vital for quality provision of emergency care service to the user in conjunction with providers of telecommunication services  boroughs and municipalities of the provider of social services (subsidized organizations, NNOs, public benefit organizations etc.)   Service fees:  Are on Emergency Care is paid by the user. User's technical conditions of provision of service:  SIM card from cell phone operator (with paid monthly fees) OR  Stationary telephone line   CONTACTS: Address: Karol ny Sv tl  18 110 00, Praha 1, Czech Republic Director of Emergency Helpline:  
 Bc. Lenka Ry av Tel: 222 333 570 E-mail: lenka.rysava@zivot90.cz   Emergency Helpline Control Desk: Tel: 222 333 540   3 E-mail:  tisnovapece@zivot90.cz 

 Seniors' Telephone 

 ARE ON Emergency Care 

 Daily Services Centre 

 Stationary and Rehabilitation Centre 

 Care Service 

 Publications 

  Copyright  ivot90  2008 - 2009.   Copyright  Jakub Kon ek . Publikov n  nebo dal   en  obsahu serveru Zivot90.cz je bez p semn ho souhlasu ob ansk ho sdru en   ivot90 zak z no. Provozovatel  port lu zaru uj  v em u ivatel m ochranu jejich osobn ch  daj . Provozovatel  nesb raj   dn  osobn   daje, kter  jim u ivatel  port l  sami dobrovoln  neposkytnou. 

ARE ON Emergency Care | Social Services |  ivot90

 About Us   

 Our Mission 
 Our Story in Facts and Figures 
 Statutes 
 Membership 
 Organizational Chart 

 Contact   

 Registered address 
 Reception Desk 
 Secretariat 
 Information and Consultancy Centre 

 Annual Reports   

 2010 
 2009 

 Archive   

 Pictures 

 Sponsors and Donors   

 For the Press   

 Downloads   

 Social Services 
 ARE ON Emergency Care 

 Are on Emergency Care 
 ARE ON Emergency care  is a comprehensive social service, the main purpose of which is to reduce health and social risks in senior citizens and handicapped people. The chief merit of the service is in helping handicapped people and seniors of advanced age to live in dignity, according to their wishes and in their natural environment   at home.    Are on: Past and Present: In 2009, the  IVOT 90 Civic Association marked 17 years of providing this comprehensive social service. By the end of 1992, the  IVOT 90 Control Room was connected with the first in a series of user terminal stations, installed in the homes of clients of the social service its authors chose to call ARE ON   Messenger of Quick Help. The ARE ON Emergency Care Service is today provided through the main control room in Prague and regional control rooms in Hradec Kr lov , Kutn  Hora and Jihlava to almost  1,300 clients in 34 towns in six Czech regions . In 2005,  IVOT 90 was awarded the Ministry of Health Makropoulos Prize.  The prize was awarded in recognition of an exceptional, practically asserted project, solving healthcare and health-related social requirements of seniors in the Czech Republic. Being the first provider of emergency care in the Czech Republic ever since early November 1992,  IVOT 90 has supplied high-quality care with emphasis on effective service provision, thanks largely to user equipment enabling two-way voice communication in case of emergency calls. As the founders and pioneers of the service in the Czech Republic, we have managed to incorporate it in legislation on social services, Act 108/2006 Coll., in the care services category. This enables the user to pay monthly service fees using his/her care bonuses.   Who the service is intended for: Seniors Handicapped persons    seniors and handicapped persons in adverse life situations, facing health and social risks    families of seniors and handicapped citizens    persons taking care of seniors and handicapped citizens  other entities providing social and health services to seniors and handicapped citizens   municipalities, city boroughs and communities   ARE ON emergency care comprises the following activities:   Provision or mediation of emergency care in:  unexpected situations (injury, fall etc.)  unforeseen critical situations (sudden deterioration of health etc.)  emergency situations (assault or threat by another person)  social consultancy   social therapeutic work  arranging contacts with social environment  assistance in exercising rights, legitimate interests and procurement of personal matters       Objectives of ARE ON emergency care: The objective of emergency care is to reduce social, health and security risks brought about by these citizens' way of life:  maximally preserve self-sufficiency and independence of elderly persons and handicapped people  ensure maximum possible longevity of people of advanced age and handicapped people in their natural social environment  do away with social exclusion of elderly people living in loneliness  reduce the ever-growing number of risks (health, social and criminal)  help senior and handicapped people to assert their rights and interest and to keep in touch with their social environment  provide social, health and general consultancy  reduce rate of admission to healthcare institutions for social reasons  reduce the number of persons waiting to be admitted to social care institutions  help families take care of their senior or handicapped members (respite care)   Criteria of provision of service:  the applicant's ability to take care of him/herself and his/her household is reduced due to health handicap and psychosomatic problems  impaired mobility or old age  applicant is older than 60 years  living alone  applicant is younger than 60 and handicapped  remote place of residence exposing a senior to considerable psychical stress  admission based on the opinion of regional and municipal social departments; in many cases service is provided at their request   Criteria of refusal to provide service:  applicant's mental condition would contraindicate provision of service  combined visual and hearing disorders or severe hearing problems prevent long-distance voice communication Parties working together to provide emergency care:  integrated rescue system (Czech Police, fire chief, rescue service)  ambulance service  town police  close person (family, neighbour etc.) specified by applicant's request, made upon provider's spot inspection  general practitioner or attending physician  community care service, personal assistance and any social service indicated by the application   another person or entity, vital for quality provision of emergency care service to the user in conjunction with providers of telecommunication services  boroughs and municipalities of the provider of social services (subsidized organizations, NNOs, public benefit organizations etc.)   Service fees:  Are on Emergency Care is paid by the user. User's technical conditions of provision of service:  SIM card from cell phone operator (with paid monthly fees) OR  Stationary telephone line   CONTACTS: Address: Karol ny Sv tl  18 110 00, Praha 1, Czech Republic Director of Emergency Helpline:  
 Bc. Lenka Ry av Tel: 222 333 570 E-mail: lenka.rysava@zivot90.cz   Emergency Helpline Control Desk: Tel: 222 333 540   3 E-mail:  tisnovapece@zivot90.cz 

 Seniors' Telephone 

 ARE ON Emergency Care 

 Daily Services Centre 

 Stationary and Rehabilitation Centre 

 Care Service 

 Publications 

  Copyright  ivot90  2008 - 2009.   Copyright  Jakub Kon ek . Publikov n  nebo dal   en  obsahu serveru Zivot90.cz je bez p semn ho souhlasu ob ansk ho sdru en   ivot90 zak z no. Provozovatel  port lu zaru uj  v em u ivatel m ochranu jejich osobn ch  daj . Provozovatel  nesb raj   dn  osobn   daje, kter  jim u ivatel  port l  sami dobrovoln  neposkytnou. 

 A national voluntary organisation
WHOLE PROJECT MARK