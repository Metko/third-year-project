plus-citykids

+CityKids Learning Lab is a Smart School Project to improve knowledge on a Syrian Refugee Camp in Jordan 

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +<strong>CityKids ABSTRACT</strong></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>+CityKids Learning Lab is a Smart School Project to improve knowledge through Education, modern digital tools and innovative methodologies for Syrian Children on Zaatari Refugee Camp in Jordan, impacting on more than 6.000 K5 to K10 students, inspiring dreams and preparing their future as citizens.</p>
<p>&nbsp;</p>
<p>When Anaisa Rodrigues (Social Design) and Maria Rodrigues (Digital Marketing) become extremely appreciative with the drama of Syrian Refugees in Europe but much more serious the situation on the Syrian Refugee Camps in the Middle East, specially in what concern to the thousands of children with several limitations to general education besides other important social needs, and these Portuguese sisters co-owners of MtoM Consulting in Portugal and offices in Brazil, decide to create, design and to develop +CityKids, a Learning Lab platform designed to provide these children at the Refugees Camps, complementary and emergencial education using the more modern and efficient digital tools available</p>
<p>&nbsp;</p>
<p>+CityKids rapidly was going for a second stage, when gets the attention of important international entities and partners, and following step was to become focused in one of the most important refugees camps in middle east, the Zaatari Syrian Refugee Camp located in Mafraq a city 80km far from Amman capital of Jordan, a Camp managed by UNHCR and with more than 80.000 refugees distribute by 5,4km/&rdquo; and 30.000 shelters, being more than 50% children with less than 18y old, including 8.000 children form K5 to K11.</p>
<p>&nbsp;</p>
<p>The project aims to be a complementary activity with objective to reduce the impact of the condition as refugee of these students on the existing Educational Infra Structures in the Refugee Camp, impacting positively on their behavior, dreams and visions about their future, improving their self esteem, building new values and knowledge about the world issues and preserving their cultural identity, but helping to transform them on better citizens in the future, besides their dramatic situation.</p>
<p>&nbsp;</p>
<p>The Human factor of the +CityKids Smart School , is one of the most relevant dimension considering that the project is designed for children on a very dramatic situation, with important affective needs, some values affected from the critical situation, enough arguments to engage the partners involved , University Yamourk and University Majdalawi both in Jordan that will become very relevant for the +citykids implementation, Vital Group International in Poland very important for the fund negotiations in EU Finance Institutions, and proudly with the Institutional support of Her Excellency Madelenie Bernadotte Princess of Sweden and His Excellency Highness Emir of Kuwait Sheik Jabar AL Sabah His Highness Sheik Nasser Al Sabah on behalf of Kuwaiti State.</p>
<p>&nbsp;</p>
<p>Process</p>
<p>&nbsp;</p>
<p>Over 8 months 34 weekly themes to be provided from the platform to 30 teachers in the camp, over digital kits of daily workshops, for one hour a day activity on complementary schedule, the collected content will bring knowledge and experiences to 6000 students in the camp over interaction with other students in EU schools, and will improve professional skills of teacher and formers.</p>
<p>&nbsp;</p>
<p>This content will be previously checked and adjusted not just to the level of comprehension of the cjildren in the Camp, but also teking in consideration all the cultural and social specifics, being this important task responsibility of the Yamourk University and the Center of Studies for Refugees, and the Madjalawi University responsible to the Educational supervision, and the content collection and organization by themes responsibility of MtoM Consulting.</p>
<p>&nbsp;</p>
<p>Tablets will be distributed on each school room for the 30 teachers and 30 formers, using local network in place and the internet access for the video conferencing.</p>
<p>&nbsp;</p>
<p>Content</p>
<p>&nbsp;</p>
<p>Provided by world wide experts as +CityKids Ambassadors donating small slots of their expertise to be the base of each of the 34 weekly themes, over small interviews, speeches dedicated to the refugee children, presentations amongst other forms of support to use their knowledge about the various themes adapted to the daily digital workshops, themes as recycling, sports, entertainment, coding, natural resources, internet of things, citizenship, entrepreneurship, education, nutrition or mobility, as the important educational process on the +CityKids Learning Lab Platform.</p>
<p>&nbsp;</p>
<p>This process for engagement of such large group of +CityKids Ambassadors will have another extremely important dimension, when each of these experts will indicate a school of their city to participate on a weekly videoconferencing between the students of the school and the children at the refugee camp of Zaatary, being a exponential experience for both sides, particularly to the kids at the camp to interact with other children, to understand how the world can be normal on their next future after the horrors of the civil war, and making these kids understand that there is a hope out side, but also for the children on the worldwide schools participating, because thei can get a important conscience about the refugees, their situation, and also the importance of their participation for the children on the camp.</p>
<p>&nbsp;</p>
<p>Refugee Camp</p>
<p>&nbsp;</p>
<p>Zaatari Refugee Camp is located 10km far from Mafraq City 80km north of Amman in Jordan close to the Syrian board, is one of the largest refugee camps managed by UNHCR in the region, with more than 80.000 refugees, 50% are children less than 18y old, almost 30 schools and around 4000 students between K5 and K10, are the focus of +CityKids Learning Lab.</p>
<p>&nbsp;</p>
<p>Current state of implementation,</p>
<p>&nbsp;</p>
<p>Project created and developed by MtoM Consulting Portugal Team, gets the partnership for implementation of two large Jordan Universities, institutional support of Kuwait Government &amp; Sweden Royal Authority, and currently we start the selection of Ambassadors and content collection by the 34 themes with all the necessary information to be collected and organized, as the schedule for the daily workshops with expected conclusion on final 2ndQ OF 2017.</p>
<p>&nbsp;</p>
<p>Simultaneously the two Universities partnering on the project are focused on found raising for the first phase of implementation, expected to be raised until 3rdQ 2017, and starting Camp implementation on 4rdQ2017, already some contacts with Camp Authorities have been developed by Yamourk University.</p>
<p>&nbsp;</p>
<p>Innovation</p>
<p>&nbsp;</p>
<p>+CityKids Learning Lab have been designed using all the modern digital tools available, and creating a emergencial education platform for the refugee children in the camp, providing a comprehensive understanding and knowledge about the outside world, inspiring their dreams as future citizens</p>
<p>&nbsp;</p>
<p>Expected Results</p>
<p>&nbsp;</p>
<p>Zaatari Refugee Camp as more than 80.000 refugees, more than 6.000 are K5 to K10 children some of them born in the camp, +CityKids will bring to these children, knowledge and inspiration to become better citizens next future, providing to 30 teachers in the camp more than 150 daily digital workshops over the 34 weeks.</p>
<p>&nbsp;</p>
<p>Feasibility</p>
<p>&nbsp;</p>
<p>+CityKids have been developed on top of a very balanced public-private-social partnership, involving international institutions at government level, Universities, Private Partners and the Educational Entities at the Refugee Camp, funding from European Investment Institutions and other sources under conversation on the found raising process in course.</p>
<p>&nbsp;</p>
<p>Under evaluation the possibility of found raising on the worldwide technological companies from the Ambassadors network, we are currently scooping a process that can be efficient to complete the found raising process.</p>
<p>&nbsp;</p>
<p>Partners</p>
<p>&nbsp;</p>
<p><strong>1)Anaisa Rodrigues/VP Social Design/MtoM </strong></p>
<p><strong>2)Maria Rodrigues/VP Marketing/ MtoM </strong></p>
<p><strong>3)Miguel Rodrigues /CEO/ MtoM as Project Manager</strong></p>
<p><strong>4)Prof Hanan Malkawi/International Projects Coordinator /Yamourk University Jordan&nbsp;&nbsp;&nbsp;&nbsp; and Vice President for the Jordan Scientific Comite as Project Coordinator</strong></p>
<p><strong>5)Dr Mohamed Dado/ CEO/Poland Vital Group </strong></p>
<p><strong>6)DrFarouk Majdalawi/CEO /Majdalawi University Services Jordan as educational Coordinator</strong></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
WHOLE PROJECT MARK