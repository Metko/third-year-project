Oomph

 Oomph! - Our Organisation Makes People Happy 

 Subscribe to our newsletter 
 Facebook 
 Twitter 
 YouTube 

 Home Our Approach 

 Our Approach 
 What we do 
 Where we do it 
 Why we do it 
 Our Training and Support 
 Oomph! and the CQC 

 Our Impact 

 Our Impact 
 Our vision 
 Our mission 

 Case studies 

 Get Some Oomph! 

 Our Services 

 Out   About  
 Support every step of the way 
 Bespoke Services 

 About Us 

 About Us 
 Our story 
 Our Team 
 Partners and Supporters 
 Work for Oomph! 

 News 

 Introducing a radical solution to get care home residents Out   About 
                             JUMP ON BOARD!  

 We're an award-winning social enterprise dedicated to enhancing the mental, physical and emotional wellbeing of older   vulnerable adults
                             Find out more  

 We provide best in class training and support to enable your staff to deliver exceptional activity programmes
                             Find out more  

 We're committed to tracking and sharing the impact of our work on the health and wellbeing of the people that matter
                             Find out more  

 We're working with the care industry to transform the culture of older adult care across the UK
                             Find out more  

 About Oomph! 
 Oomph! is an award-winning social enterprise dedicated to enhancing the mental, physical and emotional wellbeing of older adults. 
 The videos here provide an introduction to our work and its impact.  Click on the icon in the top left of the video panel to select which video to watch or simply press play to watch all videos. 

 Transforming the Quality of life in Older Adults 
 We are producing some remarkable results 
 Oomph! have now trained over a thousand care workers in all corners of the UK who are delivering hundreds of thousands of Oomph! designed and inspired activity sessions to older adults. 
 We provide tried and tested research tools to track and map health and wellbeing outcomes, enabling individuals and organisations to better understand what works and why. 

 1,598   Staff trained 
 59,575   Sessions delivered per year 
 14%   Average improvement in quality of life 
 4.6/5   Average participant rating for Out   About trips 

 Review us on  CareHome.co.uk 

 Putting Oomph! into your Care Setting 
 Contact Us 
 Join the Oomph! movement 
 Care homes, local authorities, community sport organisations, home care and social care providers, gyms...if you are committed and passionate about improving the health and quality of life of older people, get in touch. 

 It's amazing the difference to residents and team members alike that Oomph! training makes! Having experienced it myself it's so much fun and the health benefits are unquestionable 
 Avnish Goyal, Owner Hallmark Care Homes 

 Care home residents will thoroughly enjoy, and benefit from, Oomph!   a great way to exercise and have fun too. Oomph! has developed an excellent training and support programme that should give activity staff the confidence to lead sessions. The regular flow of updated material will also be welcomed as a way of keeping the activity fresh and lively. 
 Sylvie Silver, NAPA 

 The Oomph programme of training and support has proven to be a great inspiration to the staff and has shown immediate benefits to the residents in our care.  The enthusiasm and energy of everyone in the Oomph business is infectious and has had a positive impact on all our staff who have been involved.  We are very much looking forward to continuing to work with the Oomph team as a key business partner going forwards 
 Mark Greaves, Managing Director, Ideal Care Homes 

 Living life to the full shouldn't stop at the care home door and a great way to promote the well-being of residents is through exercise and fun. It's all part of the person-centred approach that my inspectors are looking for. Congratulations to Oomph! for the enthusiasm and energy they have brought with their creative approach. 
 Andrea Sutcliffe, Chief Inspector of Adult Social Care, CQC 

 It is invigorating for the staff, residents and their families too. We are seeing even more interaction, fun and laughter in our homes, which is a key element of our culture and benefits staff and customers alike. We haven t seen anything in living memory that has created such a positive response from people   residents, families and staff!   
 Christine Asbury, Chief Executive, WCS Care Group 

 I enjoyed the Oomph! training course.  The theory sections really made us all think and the practical exercises were great fun.  I ve been an activity co-ordinator for 7 years so I interact with residents every day but I still learnt so much.  The Oomph! instructors were great, they were patient, helpful and we felt we could ask them anything. I m looking forward to getting back to my home and delivering my first ever Oomph! exercise class! 
 Jo Angell Sedgwick, Activity Co-ordinator, Brookview Nursing and Residential Centre, Alderley Edge  

 Regular exercise and activity have been proven to have significant impact on the long term health, well-being, and independence of care home residents. Investment in high quality activity and exercise training for care staff - such as that provided by Oomph! Wellness   can yield positive outcomes and return on investment. In my view, Oomph!'s commitment to tracking and mapping the impact of their work makes them stand out in the care sector. 
 Des Kelly, CEO National Care Forum 

 Residents, and even their families, love taking part in the exercises provided by Oomph! and the energy and enthusiasm of the instructor is very infectious. Residents  mobility and dexterity has improved and all in all the classes provide a positive experience for everyone in the home. 
 Lynn Buxton, Manager of Ashdale Lodge Care Home, Hull 

 Regular exercise and meaningful activities are so important for the quality of life of residents in care homes up and down the country.  Care England encourage care homes not just to invest in these services but to really evidence the impact that these interventions have.  Oomph! has  impact measures and a commitment to person-centred interventions built on best practice and research. 
 Professor Martin Green, Chief Executive, Care England 

 Caring Homes are in the process of rolling out Oomph! training and support across all of our homes.  Oomph! are a professional team who deliver a really high quality programme.  Our residents - both in dementia and non-dementia settings - absolutely love it, and the impact on them is evident for all to see.  The staff team are all very engaged in the programme and making a real difference to our homes.  I thoroughly recommend Oomph! to all. 
 Laird MacKay, Managing Director, Caring Homes 

 Oomph! partners 
 Some of the 900+ care settings Oomph! have worked with: 

 Home 
 Our Approach 
 Our Impact 
 Get Some Oomph 
 About Us 
 News 

 Contact Us 
 0845 689 0066 or 0203 601 6363 
 hello@oomph-wellness.org 
 Send us a message 
 c/o Oomph! Wellness, Hill Place House, 55a High Street, Wimbledon, London SW19 5BA 
 Facebook 
 Twitter 

  Oomph! 2017. All rights reserved. 

 Social enterprise providing exercise classes to older people in long-term care settings
WHOLE PROJECT MARK