FreeTimePays 

   Timeline Projects Sign in 

You are in: Home library 

		Social media for social impact 

 Sign in 
 Register for free 

								Dreams and ideas... 

								when shared... 

								creates impact. 

 The Demand 

 Summary (1min 23sec) 

 Introduction (6min 40sec) 

 The Product 

 About FreeTimePays product  

 The Product (6min 35sec) 

 Show   Tell 

										Book a Show   Tell 

 Communities of Influence 

										Connect with us 

 Contact Us 

 Phone : 0121 410 5520 
									Email :  Email 

 Follow Us 

							About the Product 

 FreeTimePays is a digital space for those people who want to make a difference   for themselves, for their families, for their communities and for the things they really care about. 

 There are three components to FreeTimePays. 
 There s Community Passport; Community Workspace and Community Matchmaker.  Operating right across the platform in recognition of the valuable contribution being made by users is FreeTimePays gamification.  This takes the form of points and rewards for passions shared. 
 FreeTimePays is here for people who really want to become involved in their community or with their particular passion and for those people who are really serious about making a difference. It s our job at FreeTimePays to provide the tools and functionality that helps bring together; those who create the great ideas with those who have the potential to turn an idea into something that really does make a difference. 
 Community Passport 
 Passport is a personal space which registered members can make their own. 
 With a passport, members can choose to get involved with their passion and participate in many different ways. 
 They can view regular content and posts; sort and save this content by type or by passion; they can collect points for giving their views through polls and surveys, attend events or even join a discussion. 
 With a FreeTimePays Community Passport, members can follow inspiring people and they can learn more about their community and their passion by following regular  Did you Know  features.  And the more they decide to do and the more they get involved, the more points they collect and the greater the opportunity to take up offers and win prizes. 
 People can register and obtain a Community Passport at a growing number of websites.  All of the FreeTimePays sites are being launched from Spring 2017.  Together with our partners, our sponsors and our champions, FreeTimePays are planning to promote at least 10 communities and 10 passions every 3 months, and as you would expect, the FreeTimePays App will soon be available. 
 Members looking to contribute and add content are given additional access rights so they can submit posts.  Posts can be created on any device and it s quick and it s easy to do. 
 Their contribution can be a photo, a video, a poll or a quick survey.  It could be news, a  Did you Know  article or something inspirational.  A title can be added to the post as well as additional text and links to other documents. Tags can be included, a publish date added and a preferred location for the post selected. 
 Once prepared, it is then submitted.  All content that is submitted is viewed and approved prior to publication.  And once published, the contributor can sit back and watch others who share their passion respond through likes and further contact. 
 Community Workspace 
 With their unique Community Workspace, FreeTimePays is able help those who are inspired and serious about taking things to the next level.  FreeTimePays will give these people their own access rights environment where they can work on their idea or project. 
 In this digital space they can work alone, or bring in others to share in building evidence, acquiring knowledge and developing plans.  This is the ideal space for working on the business;  working on the idea; working on the initiative. 
 A range of facilities and tools can be found in workspace and users can effectively utilise this space for collating documents, photos, videos and web links, for opening up discussion and chat with others, or for running surveys and analysing results. 
 In this space people are in complete control with what they store and who they decide to invite in and join them. 
 The FreeTimePays doors are not just for those thinking of running a business or taking a venture forward.  For those pursuing their passion on a voluntary basis, there is so much to offer. 
 Whatever the content, whether photos, images, videos, documents, or links to places on the web,  people can  build their very own protected resource, deciding to share it with others whenever and if ever they feel the time is right. 
 Community Matchmaker 
 The whole focus and rationale for FreeTimePays is MAKING A DIFFERENCE.   It s our job at FreeTimePays to provide the tools and functionality that helps bring together those who create the GREAT IDEAS with those who have the potential to turn an IDEA into something that really does MAKE A DIFFERENCE. 
 Matchmaker is where the dreamers can join with the dream makers   with those who are more than happy to put their support, their resources, their connections, and their wealth of experience behind the idea and behind the passionate people responsible for coming up with the idea. 
 These are the community drivers, the investors, the philanthropists, the funders of great initiatives, the Lottery, and those from local government and the public sector who are responsible for the provision of public services. 
 These are the people and the organisations who are in positions of making things happen for those who are passionate and inspired to want to make a difference. 

 That s a very brief look at FreeTimePays but there s so much more to share with you. 
 Let s arrange a time to discuss things further so we can show you just what together we can achieve and deliver with FreeTimePays. 

							            You are in: Home library 

						Powered by People Matters Network 

 Copyright    People Matters Network  Ltd , 2017 

	FreeTimePays 

   Timeline Projects Sign in 

You are in: Login library 

 Please Sign in 

 FreeTimePays gives you access to your very own Community Passport so you can follow your passions and make a difference for yourself and for others.  With your Community Passport, you can also start to collect passion points. 
 Members with access to Community Workspace may also sign in here. 

 Email address 

 Password 

Stay signed in 

  No passport yet?  Register here   
By logging in you have accepted our conditions of use and agree to respect the privacy of other users. Please view  terms and conditions . 

							            You are in: Login library 

						Powered by People Matters Network 

 Copyright    People Matters Network  Ltd , 2017 

	FreeTimePays 

   Timeline Projects Sign in 

	                                    You are in: User Registration UserRegistration search 

 Export these results to Excel 

 Search 

 Advanced Search 

 Search: 

 To use all the great functions and to collect Passion Points, you must first register for FreeTimePays.  It's completely free!  Register now for your very own Community Passport. 
 Register for free or  Sign In 

 First name 

 Last name 

 Email 

 Confirm email 

 Type the number shown in image 

						                You are in: User Registration UserRegistration search 

						Powered by People Matters Network 

 Copyright    People Matters Network  Ltd , 2017 

	FreeTimePays 

   Timeline Projects Sign in 

You are in: enquiryform library 

 CONNECT WITH US 
 So that we can deal with your interest and respond accordingly, please complete the following. 
 First, of all could you explain the nature of your interest by selecting from the following list 

					I/We want to find out more about FreeTimePays 

					I/We would like to book a  Show and Tell  to learn more 

					I/We want to set up a FreeTimePays community 

					I/We want to run an engagement campaign with FreeTimePays 

 Second, if you have a particular passion, community or domain that is of interest, please provide details here. 

 Finally, is there anything you would like to add before we make contact. 

 Your details 

 Name * 

 Organisation 

 Contact number 

 Email address * 

						Type the number shown in image 

							            You are in: enquiryform library 

						Powered by People Matters Network 

 Copyright    People Matters Network  Ltd , 2017 

	FreeTimePays 

   Timeline Projects Sign in 

You are in: Home library 

		Social media for social impact 

 Sign in 
 Register for free 

								Dreams and ideas... 

								when shared... 

								creates impact. 

 The Demand 

 Summary (1min 23sec) 

 Introduction (6min 40sec) 

 The Product 

 About FreeTimePays product  

 The Product (6min 35sec) 

 Show   Tell 

										Book a Show   Tell 

 Communities of Influence 

										Connect with us 

 Contact Us 

 Phone : 0121 410 5520 
									Email :  Email 

 Follow Us 

							About the Product 

 FreeTimePays is a digital space for those people who want to make a difference   for themselves, for their families, for their communities and for the things they really care about. 

 There are three components to FreeTimePays. 
 There s Community Passport; Community Workspace and Community Matchmaker.  Operating right across the platform in recognition of the valuable contribution being made by users is FreeTimePays gamification.  This takes the form of points and rewards for passions shared. 
 FreeTimePays is here for people who really want to become involved in their community or with their particular passion and for those people who are really serious about making a difference. It s our job at FreeTimePays to provide the tools and functionality that helps bring together; those who create the great ideas with those who have the potential to turn an idea into something that really does make a difference. 
 Community Passport 
 Passport is a personal space which registered members can make their own. 
 With a passport, members can choose to get involved with their passion and participate in many different ways. 
 They can view regular content and posts; sort and save this content by type or by passion; they can collect points for giving their views through polls and surveys, attend events or even join a discussion. 
 With a FreeTimePays Community Passport, members can follow inspiring people and they can learn more about their community and their passion by following regular  Did you Know  features.  And the more they decide to do and the more they get involved, the more points they collect and the greater the opportunity to take up offers and win prizes. 
 People can register and obtain a Community Passport at a growing number of websites.  All of the FreeTimePays sites are being launched from Spring 2017.  Together with our partners, our sponsors and our champions, FreeTimePays are planning to promote at least 10 communities and 10 passions every 3 months, and as you would expect, the FreeTimePays App will soon be available. 
 Members looking to contribute and add content are given additional access rights so they can submit posts.  Posts can be created on any device and it s quick and it s easy to do. 
 Their contribution can be a photo, a video, a poll or a quick survey.  It could be news, a  Did you Know  article or something inspirational.  A title can be added to the post as well as additional text and links to other documents. Tags can be included, a publish date added and a preferred location for the post selected. 
 Once prepared, it is then submitted.  All content that is submitted is viewed and approved prior to publication.  And once published, the contributor can sit back and watch others who share their passion respond through likes and further contact. 
 Community Workspace 
 With their unique Community Workspace, FreeTimePays is able help those who are inspired and serious about taking things to the next level.  FreeTimePays will give these people their own access rights environment where they can work on their idea or project. 
 In this digital space they can work alone, or bring in others to share in building evidence, acquiring knowledge and developing plans.  This is the ideal space for working on the business;  working on the idea; working on the initiative. 
 A range of facilities and tools can be found in workspace and users can effectively utilise this space for collating documents, photos, videos and web links, for opening up discussion and chat with others, or for running surveys and analysing results. 
 In this space people are in complete control with what they store and who they decide to invite in and join them. 
 The FreeTimePays doors are not just for those thinking of running a business or taking a venture forward.  For those pursuing their passion on a voluntary basis, there is so much to offer. 
 Whatever the content, whether photos, images, videos, documents, or links to places on the web,  people can  build their very own protected resource, deciding to share it with others whenever and if ever they feel the time is right. 
 Community Matchmaker 
 The whole focus and rationale for FreeTimePays is MAKING A DIFFERENCE.   It s our job at FreeTimePays to provide the tools and functionality that helps bring together those who create the GREAT IDEAS with those who have the potential to turn an IDEA into something that really does MAKE A DIFFERENCE. 
 Matchmaker is where the dreamers can join with the dream makers   with those who are more than happy to put their support, their resources, their connections, and their wealth of experience behind the idea and behind the passionate people responsible for coming up with the idea. 
 These are the community drivers, the investors, the philanthropists, the funders of great initiatives, the Lottery, and those from local government and the public sector who are responsible for the provision of public services. 
 These are the people and the organisations who are in positions of making things happen for those who are passionate and inspired to want to make a difference. 

 That s a very brief look at FreeTimePays but there s so much more to share with you. 
 Let s arrange a time to discuss things further so we can show you just what together we can achieve and deliver with FreeTimePays. 

							            You are in: Home library 

						Powered by People Matters Network 

 Copyright    People Matters Network  Ltd , 2017 

	FreeTimePays 

   Timeline Projects Sign in 

	                                    You are in: User Registration UserRegistration filter 

 Export these results to Excel 

 Search 

 Search: 

						                You are in: User Registration UserRegistration filter 

						Powered by People Matters Network 

 Copyright    People Matters Network  Ltd , 2017 

	FreeTimePays 

   Timeline Projects Sign in 

You are in: login library 

 Please Sign in 

 FreeTimePays gives you access to your very own Community Passport so you can follow your passions and make a difference for yourself and for others.  With your Community Passport, you can also start to collect passion points. 
 Members with access to Community Workspace may also sign in here. 

 Email address 

 Password 

Stay signed in 

  No passport yet?  Register here   
By logging in you have accepted our conditions of use and agree to respect the privacy of other users. Please view  terms and conditions . 

							            You are in: login library 

						Powered by People Matters Network 

 Copyright    People Matters Network  Ltd , 2017 

	FreeTimePays 

   Timeline Projects Sign in 

	                                    You are in: User Registration UserRegistration search 

 Export these results to Excel 

 Search 

 Advanced Search 

 Search: 

 To use all the great functions and to collect Passion Points, you must first register for FreeTimePays.  It's completely free!  Register now for your very own Community Passport. 
 Register for free or  Sign In 

 First name 

 Last name 

 Email 

 Confirm email 

 Type the number shown in image 

						                You are in: User Registration UserRegistration search 

						Powered by People Matters Network 

 Copyright    People Matters Network  Ltd , 2017 

	FreeTimePays 

   Timeline Projects Sign in 

You are in: Agreement library 

 Terms and Conditions of use 
 By registering for your  Community Passport , you are agreeing to the following conditions. 
We therefore politely ask that you read though our conditions of use. 
 Terms and conditions of use for invited members of our Community of Influence 
 With a Community Passport, members can interact and exchange views with other members.  This may be through surveys, through views expressed in discussions, through general chat or through posts submitted for publication. 
 Members should be mindful that these views may be seen by others.  Anonymity is very much for the member to protect and FreeTimePays and PMNet will not be liable and responsible for maintaining anonymity. 
 Please ensure that : 
 Information given about yourself is correct and that you alone can receive and read emails sent to the email address you supply. 
 You take full responsibility for any content you submit for publication and that others will not be able to use your account. You inform the administrators if you see content that breaks these rules. 
 FreeTimePays and PMNet will not be liable for the authenticity, accuracy or ownership of content submitted by members and published on a FreeTimePays platform. 
 You acknowledge, consent and agree that the administrators of FreeTimePays may access, preserve and disclose your account information and content if required to do so by law or in a good faith belief that such access preservation or disclosure is reasonably necessary to: 

 Comply with legal process 
 Enforce the rules 
 Respond to claims that any content violates the rights of third parties 
 Respond to your requests for customer service; or protect the rights, property or personal safety of any other member. 

 Further rules and guidelines for members who participate in discussions and submit content for publication: 
 Those members of the community who wish to join and participate in one of the many discussions open to community members will do so with the full knowledge that the views they express will be shared with other participants. 
 Those members who submit content will do so with the full knowledge that the content will be seen by other members. 
 Participants of discussions and contributors of content are politely asked to respect all other members. 
 Some guidelines for use: 
 Please keep comments relevant to the topic. 
 Be courteous and endeavour to keep any submission objective and factual. 
 Do not submit defamatory comments (i.e. comments that are untrue and capable of damaging the reputation of a person or an organisation). 
 Do not condone illegal activity or incite people to commit any crime, including incitement of racial hatred. 
 Do not submit comments that could prejudice on-going or forthcoming court proceedings (contempt of court) or break a court injunction. 
 Do not submit comments containing someone else s copyright material. 
 Do not swear or use language that could offend other participants. 
 Do not otherwise submit comments that are unlawful, harassing, abusive, threatening, harmful, obscene, profane, sexually orientated or racially offensive. This includes comments that are offensive to others with regards to religion, gender, nationality or other personal characteristic. 
 Do not impersonate other members or falsely claim to represent a person or an organisation. 
 Do not submit comments or add personal information that would identify yourself or others. 
 Do not post comments in languages other than English. 
 Do not advertise or promote products or services. 
 Do not spam or flood the discussions. Only submit a comment once. Do not resubmit the same, or similar, comments. Keep the number of comments you submit on a topic at a reasonable level. Multiple comments from the same individual, or a small number of individuals, may discourage others from contributing. 
 Do not add inappropriate references (vulgar, offensive etc.). 
 If you are under the age of 16, please obtain your parent/guardian s permission before participating in any discussion. Users without this consent are not allowed to participate or provide us with personal information. 
 The awarding of Passion Points 
 Any decision to award points for taking part in activities and any decision to run prize draws or offers and recognise the contributions of members through the award of prizes or certificates will be taken by FreeTimePays and PMNet. 
 Individual members of FreeTimePays are not paid for their services and any support is freely given in the recognition that their time is voluntary. 
 Members of the community of influence will be informed of all awards. 
 People Matters Network (PMNet) oversees activities and reserves the right to: 
 Respond to all violations of rules or local laws in any way needed, including removing content and reporting any misuse to any appropriate authority. 
 Close the account of any member that repeatedly breaks the terms of use. 
 Change or amend the rules of use without prior notice. 
 Return to Sign in 
 Community Workspace users (including Approvers and Publishers) 
 These additional terms and conditions apply to those people who sign in and register for workspace access.  Including those who take on a license or use the platform in the capacity of an Approver or a Publisher. 
 Confidentiality and Security Agreement 
 FreeTimePays and PMNet takes the security and confidentiality of its software (software known as Together Solutions), its software users, data and all information held within the software extremely seriously. Users must treat information and any data contained within or extracted from this site as confidential. 
 Information and data may only be disclosed if permitted by members of the Workspace project or if stated as "public". At all times you must preserve the confidentiality of, and not attempt to identify, individuals, households or organisations in the data. If information or data is taken or downloaded from the site, you must ensure that all data confidentiality is maintained and that the information is stored in a physically and electronically secure condition. 
 All data held on the site is provided without warranty or liability of any kind and all responsibility for accuracy in its use is taken by the Workspace user. FreeTimePays and PMNet will not accept any liability of any kind for any use or consequences of use of the data. You must comply with the Data Protection Act 1998 and all other applicable codes of practice and security measures appropriate to your business and/or organisation. 
 You may not modify or copy the software. You may not disassemble, decompile or reverse engineer the software, or otherwise attempt to discover the source code of the software. You shall retain all copyright information and propriety notices found on the software and documentation provided by FreeTimePays and PMNet. 
 You agree to protect the confidentiality of the software, the licence being for you or nominated individuals or organisations only as agreed with FreeTimePays and PMNet. You agree not to translate or to prepare derivative works of the software and to take reasonable steps to prevent others from so doing. 
 FreeTimePays and PMNet uses all reasonable efforts to ensure that the content of the site is accurate and up to date and that the site and tools are available 24 hours per day. However, the material or data may contain inaccuracies or typographical errors, and FreeTimePays and PMNet make no representations about the accuracy, reliability, completeness, or timeliness of the material, software or links. 
 Changes are periodically made to the website and may be made at any time. The site may be unavailable or access may be interrupted from time to time owing to technical failure.  FreeTimePays and PMNet do not warrant that the site or its server is free of computer viruses, infection, worms and other harmful codes or properties. If your use of the site or materials or data results in the need for servicing or replacement of equipment or data, FreeTimePays and PMNet are not responsible for those costs. 
 In no event will FreeTimePays and PMNet be liable to you for any loss of profits, revenue, contracts, business, anticipated savings, data, or for any incidental or consequential damage to your organisation, goodwill or reputation, for any lost opportunities or for any special, indirect or consequential loss arising out of or in connection with this agreement or its subject matter. 
 By logging into this site for the first time, you are agreeing to use this software and any information contained within this site within the terms of this Confidentiality and Security Agreement. 
 Breach of these terms and conditions will lead to immediate and future termination of access to the software and data and could result in legal action against you. 
 FreeTimePays and PMNet are based in England. People Matters Network, an English company, whose registered office address is 68 Caroline Street, Jewellery Quarter, Birmingham, West Midlands. B3 1UG. 
 These terms and your use of the site, the tools and the material and data is governed by and shall be construed in accordance with the laws of England and any matter or dispute arising concerning these terms or any other matter related to the site, material or data, shall be submitted to the exclusive jurisdiction of the English courts. 
 Return to Sign in 

							            You are in: Agreement library 

						Powered by People Matters Network 

 Copyright    People Matters Network  Ltd , 2017 

FreeTimePays is social media for social impact that engages, empowers and inspires people to share passions. 

<p>FreeTimePays is social media for social impact that engages, empowers and inspires people to share passions.</p>
<p>Link to Demand video: &nbsp;<a href="https://www.youtube.com/watch?v=SAGtMnirLLY">https://www.youtube.com/watch?v=SAGtMnirLLY</a></p>
<p>Link to Product video : <a href="https://www.youtube.com/watch?v=PxyCqFE1kN0">https://www.youtube.com/watch?v=PxyCqFE1kN0</a></p>
<p>This is achieved via a Community Passport, a Community Workspace and a Community Matchmaker.</p>
<p>With Community Passport, individuals can personalise their view to suit their own interests and passions and the greater their involvement, the greater the recognition and the rewards they receive.</p>
<p>With Community Workspace, individuals, communities, organisations and investors can collaborate and actively pursue shared passions.</p>
<p>With a Community Matchmaker, a Community of real influence is formed by bringing together people with great ideas with those people and organisations that are keen to fund those ideas.</p>
<p><img src="http://www.freetimepays.com/uploadedfiles/FreetimePaysSystem.jpg" alt="FreeTimePays" /></p>
<p>&nbsp;</p>
<p><img src="http://www.freetimepays.com/images/ftp3.png" width="194" height="194" /><img src="http://www.freetimepays.com/images/ftp2.png" width="193" height="193" /><img src="http://www.freetimepays.com/images/ftp1.png" width="193" height="193" /></p>
<p>Dreams and Ideas .... when shared .... creates impact</p>
<p>Contact: &nbsp;</p>
<p>Jonathan Bostock</p>
<p>FreeTimePays initiative</p>
<p>e: <a href="mailto:jonathan.bostock@freetimepays.com">jonathan.bostock@freetimepays.com</a></p>
<p>m: 07432637322</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
WHOLE PROJECT MARK