Get  10,000 To Help The Environment  | GoThinkBig

 GoThinkBig.co.uk 
 Helping young people achieve their career dreams 

 Log in 
 Register 

 Together with  O2 

 Keywords 

 Find us: 

 Facebook 
 Twitter 
 Youtube 
 info 

 About All about Go Think Big 
 Features Helpful tips,  work experience and job market advice 
 Opportunities Hot work experience opportunities and events 
 Tools Web tools to help you land your dream job 
 Win Chances to Win with GoThinkBig! 

 Get  10,000 To Help The Environment         Save 

 Author Emily Mitchinson 
 Date Wednesday 1 February 2017 
 Tags environment O2 Think Big The Environment Now 

 Environmental issues are already presenting challenges to how we live, but what are we doing about it? Digital technology can help create solutions and lead us to a greener, better world. As a generation, we have the innovation, skills and passion to drive positive change, so we want you to get thinking about the environmental problems we all face, and how digital tech can help solve them. 
 If you have got an idea to help the environment, such as improving energy efficiency, reducing waste or increasing recycling, and you are aged between 17 and 24, then you could apply for up to  10,000 from The Environment Now to bring your idea to life! 
 So what exactly is The Environment Now? 
 The Environment Now is an exciting new opportunity from our friends at  O2 s Think Big  that will fund 17-24 years olds up to  10,000 to create their unique digital ideas to help the environment, such as improving energy efficiency, reducing waste, or recycling. Successful applicants will get support from The Environment Now team, their own professional mentor and other sustainability partners and industry professionals. 
 The Environment Now programme is funded by O2 and the National Lottery through the Big Lottery Fund, and is part of the  Our Bright Future  programme. It is managed by the  National Youth Agency . 

 What s in it for me? 
 As well as offering a whopping 50 funding grants to get you lot started making your innovative ideas a reality, The Environment Now will also connect you with mentors and give you opportunities to gain work experience. 
 The team are hoping to go to as many colleges, universities and youth groups as they can to run their workshops (they ve already hit Leeds, Birmingham and Grimsby and are planning visits to Edinburgh, Belfest, Bristol, Lincoln and Keele!) Once there, they hope to inspire the entrepreneurs of the future to use their knowledge and experience to create ideas that could make their world a better place. 
 The Thinkspiration workshops are informal and relaxed sessions, and are full of open discussion to encourage you to think outside the box and innovate together! 

 What kind of projects can get funding? 
 If you have an idea of how digital technology could help the environment, The Environment Now want to hear from you! 
 They ve already funded projects like 
 LettusGrow : a company on a mission to reduce food waste around the world through soil-free gardening. 
 Filamentive : a 3D printing filament business that needs money for experimentation, research and development into new recycled and recyclable materials. 
 Virtually There :  an app that will contain different virtual reality experiences highlighting the impact of global warming, waste and deforestation. 
 For more information on other projects The Environment Now have already funded,  take a look here . 

 How can I apply? 
 Applications for funding are open until 14th November. Go to  The Environment Now website  to read the full criteria and apply for funding! 
 If you have any questions about funding or Thinkspiration events, you can  contact the team at The Environment Now here. 
 Liked this? You might also enjoy 
 Jessica Okoro:  Don t Let Anyone Tell You What You Can Do 
 Under 25? Here s Why You Should BYOB 
 Seek Forgiveness, Not Permission 

 Please enable JavaScript to view the  a href="http://disqus.com/?ref_noscript" comments powered by Disqus. /a 

 Features See all   

   7 Smart student saving tips to get you through University 

   4 Freshers mistakes it s ok to make (and how to fix them) 

 Opportunities See all   

 Next opportunity ending in 

 Days 2 
 Hrs 11 
 Mins 50 
 Sec 41 

   Mastering the Art of a Video Interview 

 Where Online 
 Starts 27 September 2017 
 Duration 5 days 

 Apply now 

 Latest 

 Event: O2 Accessibility Hackathon 
 Closing  Wednesday 18 October 2017 - 22:59 

 Funding   Youth Project: Potential Unlocked project 
 Closing  Saturday 28 October 2017 - 22:59 

 Work Experience: Deliver a Project with O2 s IT Team! 
 Closing  Sunday 29 October 2017 - 23:59 

 More Opportunities   

 Competitions See more   

 Win with GoThinkBig! 

 Helpful Tools See all   

   Gro your CV 

   Hack the Front Page 

   Careers infographics 

   Take a live interview 

 Tweets             Follow @GoThinkBig 

 Tweets by @GoThinkBig 

 About Us 
 Contact Us 
 Partners 
 Help 
 Accessibility 
 Terms   Conditions 
 Privacy Policy 

 GoThinkBig.co.uk is brought to you by 

 O2 Website 
 Telefonica Website 
 Think Big - Telef nica Foundation 

The Environment  Now is offering young people aged 17-24 a grant for up to £10,000 to use digital technology to tackle environmental issues. 

<p><span style="font-size: 10.5pt; font-family: 'Verdana','sans-serif'; color: black;"><a title="The Environment Now" href="https://www.gothinkbig.co.uk/features/the-environment-now" target="_blank" rel="noopener noreferrer">The Environment Now</a><span class="apple-converted-space">&nbsp;</span>is an exciting new opportunity from O2&rsquo;s Think Big that will fund 17-24 years old with up to &pound;10,000 to create their unique digital ideas to help the environment, such as improving energy efficiency, reducing waste, or recycling.</span></p>
<p style="font-variant-ligatures: normal; font-variant-caps: normal; orphans: 2; text-align: start; widows: 2; -webkit-text-stroke-width: 0px; word-spacing: 0px;"><span style="font-size: 10.5pt; font-family: 'Verdana','sans-serif'; color: black;">Successful applicants will be supported by The Environment Now team, their own professional mentor and other sustainability partners. We also offer support, work experience and insight sessions with industry professionals to help bring young people's ideas to life.</span></p>
<p style="font-variant-ligatures: normal; font-variant-caps: normal; orphans: 2; text-align: start; widows: 2; -webkit-text-stroke-width: 0px; word-spacing: 0px;"><span style="font-size: 10.5pt; font-family: 'Verdana','sans-serif'; color: black;"><br /> The Environment Now programme is funded by O2 and the National Lottery through the Big Lottery Fund, and is part of the<span class="apple-converted-space">&nbsp;</span><a title="Our Bright Future" href="https://www.ourbrightfuture.co.uk" target="_blank" rel="noopener noreferrer">Our Bright Future</a><span class="apple-converted-space">&nbsp;</span>programme. It is managed by the<span class="apple-converted-space">&nbsp;</span><a title="NYA" href="https://www.nya.org.uk" target="_blank" rel="noopener noreferrer">National Youth Agency</a>.</span></p>
<p style="font-variant-ligatures: normal; font-variant-caps: normal; orphans: 2; text-align: start; widows: 2; -webkit-text-stroke-width: 0px; word-spacing: 0px;"><span style="font-size: 10.5pt; font-family: 'Verdana','sans-serif'; color: black;"><br /> We have funded a variety of digital technology projects with VR, 3D printing, mobile applications and more. If you want to read more about projects we&rsquo;ve funded check out<span class="apple-converted-space">&nbsp;</span><a title="NYA's press release" href="https://www.nya.org.uk/2017/01/first-grants-announced-green-tech-grant-programme-environment-now/" target="_blank" rel="noopener noreferrer">NYA&rsquo;s press release</a>.</span></p>
<p style="text-align: center;" align="center"><span style="font-size: 10.5pt; font-family: 'Verdana','sans-serif'; color: black;"><br /> <strong><span style="font-family: 'Verdana','sans-serif';">Applications are open until July 14th for The Environment Now funding!</span></strong>&nbsp;</span></p>
<p style="text-align: center;" align="center"><span style="font-size: 10.5pt; font-family: 'Verdana','sans-serif'; color: black;">Go to<span class="apple-converted-space">&nbsp;</span><a title="The Environment Now" href="https://www.o2thinkbig.co.uk/the-environment-now" target="_blank" rel="noopener noreferrer">The Environment Now</a><span class="apple-converted-space">&nbsp;</span>website to read our criteria and apply for our funding!</span></p>
WHOLE PROJECT MARK