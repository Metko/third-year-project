Digitalsocial.eu

 It 

 Fr 

 Es 

 En 

 De 

 Ca 

 Login 

 Create account 

 About DSI4EU 

                            About the project                         

                            Partners                         

                            Open data, research   resources                         

                            Contact DSI4EU                         

 1,846  projects                     
 View all projects 
 Case Studies 

 1,982  organisations                     

                        View all organisations                     

 Funding   support 

                            See all funding opportunities                         

 Events 

                            See upcoming DSI events                         

 News   blogs 

                            See all news   blogs                         

 Menu 

 Login 

 Search 

 Catal 
 Deutsch 
 English 
 Espa ol 
 Fran ais 
 Italiano 

 Digital social innovation 

                    SHOWCASE YOUR PROJECT, MEET COLLABORATORS AND FIND FUNDING                 

                    JOIN THE COMMUNITY OF  1,982 organisations  AND  1,846 projects  USING DIGITAL TECHNOLOGIES TO TACKLE SOCIAL CHALLENGES                 

 JOIN NOW 

 FUNDING 

 Use our funding directory to find opportunities for your project 

 READ MORE 

 Events 

 Explore DSI events happening around Europe 

 READ MORE 

 News   blogs 

                    Our blog features stories of the people and projects pioneering digital social innovation                 

 READ MORE 

            EXPLORE EUROPE S GROWING NETWORK OF DIGITAL SOCIAL INNOVATION         

 1,982 
 Organisations 

 have collaborated on 

 1,846 
 Projects 

 Data visualisation 

                    Explore Europe s network of digital social innovation

                    There are 1982 organisations and 1846 projects working on DSI
                    across Europe.

                    With our interactive data visualisation, you can explore the organisations and projects working on
                    DSI across Europe. Check it out now to understand what s going on across the continent and how you
                    fit into it!

 Explore Europe s DSI network 

 Case Studies 
 IN NEED OF INSPIRATION? 

            Short stories introducing digital social innovations which we love         

 Prusa Research 
 The maker company open sourcing the 3D printing industry 

 READ MORE 

 Smart Citizen 
 A kit and platform which engages citizens in solving local environmental problems. 

 READ MORE 

 OpenCorporates 
 OpenCorporates makes corporate information more accessible, discoverable, and usable. 

 READ MORE 

 See all case studies 

 Newsletter 
 Sign up to stay up to date with DSI4EU 

                    To keep in touch with the project and DSI in Europe, you can follow  @DSI4EU  on Twitter                 

 People 

 Join DSI4EU 

 Terms of use 

 Privacy policy 

 Projects 

 View projects 

 Organisations 

 View organisations 

 Development 

 Updates 

 Feedback 

 DSI4EU is funded by the European Union 

                                All our work is licensed under a  

                                            Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License 
 , unless it says otherwise.                             

        Nesta is a registered charity in England and Wales 1144091 and Scotland SC042833. Our main address is 58 Victoria Embankment, London, EC4Y 0DS     

 + 
 Search 

 Cancel 

 = 3" 

 News   blogs 

                    {{post.name}}

 No blog posts found 

 Projects 

                    {{project.name}}

  0"
                   href="/projects?q={{search.entry}}" 
                    View all project results                 
 No projects found 

 Organisations 

                    {{organisation.name}}

  0"
                   href="/organisations?q={{search.entry}}" 
                    View all organisation results                 
 No organisations found 

 Case Studies 

                    {{caseStudy.name}}

 No case studies found 

 See all results 

                    We use cookies to help us improve this site and your experience. Continue to use the site if you re happy with this or click to find out more.                 

 Find out more 
 Continue 

Digitalsocial.eu

 It 

 Fr 

 Es 

 En 

 De 

 Ca 

 Login 

 Create account 

 About DSI4EU 

                            About the project                         

                            Partners                         

                            Open data, research   resources                         

                            Contact DSI4EU                         

 1,849  projects                     
 View all projects 
 Case Studies 

 1,982  organisations                     

                        View all organisations                     

 Funding   support 

                            See all funding opportunities                         

 Events 

                            See upcoming DSI events                         

 News   blogs 

                            See all news   blogs                         

 Menu 

 Login 

 Search 

 Catal 
 Deutsch 
 English 
 Espa ol 
 Fran ais 
 Italiano 

 Digital social innovation 

                    SHOWCASE YOUR PROJECT, MEET COLLABORATORS AND FIND FUNDING                 

                    JOIN THE COMMUNITY OF  1,982 organisations  AND  1,849 projects  USING DIGITAL TECHNOLOGIES TO TACKLE SOCIAL CHALLENGES                 

 JOIN NOW 

 FUNDING 

 Use our funding directory to find opportunities for your project 

 READ MORE 

 Events 

 Explore DSI events happening around Europe 

 READ MORE 

 News   blogs 

                    Our blog features stories of the people and projects pioneering digital social innovation                 

 READ MORE 

            EXPLORE EUROPE S GROWING NETWORK OF DIGITAL SOCIAL INNOVATION         

 1,982 
 Organisations 

 have collaborated on 

 1,849 
 Projects 

 Data visualisation 

                    Explore Europe s network of digital social innovation

                    There are 1982 organisations and 1849 projects working on DSI
                    across Europe.

                    With our interactive data visualisation, you can explore the organisations and projects working on
                    DSI across Europe. Check it out now to understand what s going on across the continent and how you
                    fit into it!

 Explore Europe s DSI network 

 Case Studies 
 IN NEED OF INSPIRATION? 

            Short stories introducing digital social innovations which we love         

 Prusa Research 
 The maker company open sourcing the 3D printing industry 

 READ MORE 

 Smart Citizen 
 A kit and platform which engages citizens in solving local environmental problems. 

 READ MORE 

 OpenCorporates 
 OpenCorporates makes corporate information more accessible, discoverable, and usable. 

 READ MORE 

 See all case studies 

 Newsletter 
 Sign up to stay up to date with DSI4EU 

                    To keep in touch with the project and DSI in Europe, you can follow  @DSI4EU  on Twitter                 

 People 

 Join DSI4EU 

 Terms of use 

 Privacy policy 

 Projects 

 View projects 

 Organisations 

 View organisations 

 Development 

 Updates 

 Feedback 

 DSI4EU is funded by the European Union 

                                All our work is licensed under a  

                                            Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License 
 , unless it says otherwise.                             

        Nesta is a registered charity in England and Wales 1144091 and Scotland SC042833. Our main address is 58 Victoria Embankment, London, EC4Y 0DS     

 + 
 Search 

 Cancel 

 = 3" 

 News   blogs 


 No blog posts found 

 Projects 

              
 No projects found 

 Organisations 
              
 No organisations found 

 Case Studies 


 No case studies found 

 See all results 

                    We use cookies to help us improve this site and your experience. Continue to use the site if you re happy with this or click to find out more.                 

 Find out more 
 Continue 

DSI4EU is a project helping to grow the digital social innovation community across Europe. 

<p class="intro">Digital technologies and the internet have transformed many areas of business &ndash; from Google and Amazon to Airbnb and Kickstarter. Huge sums of public money have supported digital innovation in business, as well as in fields ranging from the military to espionage. But there has been much less systematic support for innovations that use digital technology to address social challenges.</p>
<p>Across Europe a growing movement of tech entrepreneurs is developing inspiring digital solutions to social challenges. These range from social networks for those living with chronic health conditions, to online platforms for citizen participation in policymaking, to using open data to create more transparency about public spending. We call this digital social innovation (DSI). We have set up digitalsocial.eu to build a network of digital social innovation in Europe. People and projects interested in DSI can use the site to showcase their work, learn about DSI, find collaborators for projects and find information on events and funding opportunities for DSI.</p>
<p>DSI4EU is being managed by a consortium of partners: Nesta (UK), Waag Society (Netherlands) and SUPSI (Switzerland).&nbsp;</p>
<div class="login-li long menu-li readmore-li">&nbsp;</div>
<p class="bold-p"><strong>Specifically, the project:</strong></p>
<ul>
<li>Creates an online resource on digitalsocial.eu that enables those interested in, and working on, digital social innovation in Europe to learn about it, showcase their work, find new collaborators and learn about events and new funding opportunities.</li>
<li>Engages the existing communities of digital social innovators in the network.</li>
<li>Carries out research on how to grow DSI in Europe.</li>
<li>Uses insights about the European DSI network to develop a set of recommendations about how policymakers and funders can best support, engage with and make the most of DSI and ensure it can continue to grow in the future.</li>
<li class="li-bottom">Develops a set of tools that supports people and organisations interested in develop digital solutions that address social challenges.</li>
</ul>
<p>DSI4EU is supported by the European Union and funded under the Horizon 2020 Programme. Grant agreement no 688192.</p>
WHOLE PROJECT MARK