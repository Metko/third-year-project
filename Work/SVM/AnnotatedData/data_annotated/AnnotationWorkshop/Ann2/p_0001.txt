#germanforrefugees

 Home 
 Press Release 
 busuu.com 
 ICOON 
 English 
 Deutsch 

 #germanforrefugees 
 Free language learning for refugees 

 Two European education start-ups teamed up for supporting refugees with free access to German and English online courses 

        All German and English language courses by busuu are now available for free to Arab-speaking users. Classes contain more than 200 audiovisual course units per language and cover everyday topics like  food ,  at the supermarket ,  clothing  or  at the doctor . These free classes are currently available for mobile Android devices.

        The free android app ICOON for refugees contains a selection of a thousand of the most important symbols and photos from the picture dictionary ICOON. A specifically developed leaflet for the needs of refugees and a German-learning poster is free for download. Additionally 6 different editions of ICOON picture dictionaries are available. 

 To activate the premium membership: 
        1. Download  the app 
        2. In the app, click on 'settings' and then 'voucher' codes 
        3. Enter the code  Germanforrefugees  or " "  (Note: the voucher code only works within an Arabic interface)

	        Download the free app in Google Play and the  ICOON - First Communication Guide as PDF  here .
             Find all information to the project at  www.icoonforrefugees.com 

 Language skills are crucial to finding your way in a new environment. They are the key to integration. With the current influx of refugees into Europe there is great demand for easy access language acquisition, something both our companies excel in. busuu and ICOON have teamed up to offer free German and English courses to refugees in Europe , Karsten Warrink of Ambermedia, Gosia Warrink of Amberdesign and Amberpress and Bernhard Niesner of busuu  

 About  busuu 
 busuu was founded in 2008 with the vision to enable people from all over the world to learn a new language. The company offers audiovisual language classes for 12 different languages for desktop PC and as apps for mobile devices. In addition, users can practice acquired language skills with native speakers via the online platform. Every user of busuu is not only a student of a foreign language, but also a tutor in his/her native language. With more than 60 million users, busuu belongs to the world s leading providers of digital language learning apps. 
 About  ICOON/AMBERPRESS-Publishing 
 ICOON is a picture dictionary for all languages in the world with over 2000 essential everyday symbols. Simply pointing to the right icon suffices to put your conversation partner in the picture. Regardless of whether you need a toothbrush, safety pin or swimming shorts, or want to explain hay fever without using words, ICOON speaks the world s most universal language: pictures. Gosia Warrink first published the ICOON global picture dictionary in 2007 at her own publishing house AMBERPRESS. Today, there are 5 ICOON book editions available as well as an app for iPhone devices. 

  2016 busuu ltd. 

#germanforrefugees

 Home 
 Press Release 
 busuu.com 
 ICOON 
 English 
 Deutsch 

 #germanforrefugees 
 Free language learning for refugees 

 Two European education start-ups teamed up for supporting refugees with free access to German and English online courses 

        All German and English language courses by busuu are now available for free to Arab-speaking users. Classes contain more than 200 audiovisual course units per language and cover everyday topics like  food ,  at the supermarket ,  clothing  or  at the doctor . These free classes are currently available for mobile Android devices.

        The free android app ICOON for refugees contains a selection of a thousand of the most important symbols and photos from the picture dictionary ICOON. A specifically developed leaflet for the needs of refugees and a German-learning poster is free for download. Additionally 6 different editions of ICOON picture dictionaries are available. 

 To activate the premium membership: 
        1. Download  the app 
        2. In the app, click on 'settings' and then 'voucher' codes 
        3. Enter the code  Germanforrefugees  or " "  (Note: the voucher code only works within an Arabic interface)

	        Download the free app in Google Play and the  ICOON - First Communication Guide as PDF  here .
             Find all information to the project at  www.icoonforrefugees.com 

 Language skills are crucial to finding your way in a new environment. They are the key to integration. With the current influx of refugees into Europe there is great demand for easy access language acquisition, something both our companies excel in. busuu and ICOON have teamed up to offer free German and English courses to refugees in Europe , Karsten Warrink of Ambermedia, Gosia Warrink of Amberdesign and Amberpress and Bernhard Niesner of busuu  

 About  busuu 
 busuu was founded in 2008 with the vision to enable people from all over the world to learn a new language. The company offers audiovisual language classes for 12 different languages for desktop PC and as apps for mobile devices. In addition, users can practice acquired language skills with native speakers via the online platform. Every user of busuu is not only a student of a foreign language, but also a tutor in his/her native language. With more than 60 million users, busuu belongs to the world s leading providers of digital language learning apps. 
 About  ICOON/AMBERPRESS-Publishing 
 ICOON is a picture dictionary for all languages in the world with over 2000 essential everyday symbols. Simply pointing to the right icon suffices to put your conversation partner in the picture. Regardless of whether you need a toothbrush, safety pin or swimming shorts, or want to explain hay fever without using words, ICOON speaks the world s most universal language: pictures. Gosia Warrink first published the ICOON global picture dictionary in 2007 at her own publishing house AMBERPRESS. Today, there are 5 ICOON book editions available as well as an app for iPhone devices. 

  2016 busuu ltd. 

#germanforrefugees

 busuu.com 
 ICOON 
 English 
 Deutsch 

 #germanforrefugees 

          busuu                    .                                " "   "     "   " "   "   ".            .

              ICOON               .                                  . 

    Premium      : 
 .    
         .         " "     " " 
         .      " " 

      Google Play           PDF  .

                         .                                  .     ICOON  busuu                              Ambermedia        Amberdesign        busuu  

   busuu 
    busuu                          .                                .                            .       busuu                        .               busuu                   . 
   ICOON/AMBERPRESS   
 ICOON                            .                  .                                                   ICOON            .         ICOON                     AMBERPRESS.             ICOON            iPhone. 

  2016 busuu ltd. 

#germanforrefugees

 Home 
 Press Release 
 busuu.com 
 ICOON 
 English 
 Deutsch 

 #germanforrefugees 
 Free language learning for refugees 

 Leading educational startups to support refugees with free access to German and English courses online 
 London/Berlin/Vienna, 30 September 2015    busuu, the world s largest social network for language learning with more than 55 million users, and ICOON, the globally successful picture dictionary open to all languages of the world, are now available for refugees free of charge. 
 Initiator of the campaign Karsten Warrink of Ambermedia, Gosia Warrink of Amberdesign and Amberpress, and Bernhard Niesner of busuu explain their motivation:  Language skills are crucial to finding your way in a new environment. They are the key to integration. With the current influx of refugees into Europe there is great demand for easy access language acquisition, something both our companies excel in. busuu and ICOON have teamed up to offer free German and English courses to refugees in Europe . 
 With immediate effect all German and English language courses by busuu are now available for free to Arab-speaking users. These classes contain more than 200 audiovisual course units per language and cover everyday topics like  food ,  at the supermarket ,  clothing  or  at the doctor . These free classes are currently available for mobile Android devices. To activate their free premium membership users of the Arab-language version of the busuu Android app will need to enter the code  Germanforrefugees . 
 The picture dictionary app ICOON contains a selection of a thousand of the most important symbols and photos from the picture dictionary. A version specifically developed for the needs of refugees with more than 2.000 symbols, photos and phrases as well as an app for Android devices will become available shortly. 
 The apps can be acquired directly via the Android or Apple app stores. Refugees, aid organizations as well as helpers can find further information on the central website  www.germanforrefugees.com , where there is also a picture dictionary with the most important symbols available for printing. 
 Press contact 
 #germanforrefugees 
Karsten Warrink 
c/o AMBERMEDIA 
+49 30 288 84 85   50 
warrink@ambermedia.de 
 European entrepreneurs join forces 
 Initiator of the joint campaign is  Karsten Warrink , founder of the marketing agencies AMBERMEDIA and jubelkind, and member of the international Entrepreneurs  Organization and the Entrepreneur s Pledge. 
 To a large number of entrepreneurs in Germany and Europa, it is important to help in a fast, easy and comprehensive manner. With the language apps by busuu und ICOON, we can improve verbal communication and language skills of refugees immediately and on a large scale. I am very happy we were able to launch something so quickly. 
 Bernhard Niesner , CEO   Co-Founder of busuu, states:  It is our responsibility to help these people in need. Language skills are an essential part of successful integration and we at busuu are glad to make this contribution. 
 Gosia Warrink , owner of AMBERDESIGN and AMBERPRESS, says:  Refugees need first assistance with communication upon their arrival in order to be able to communicate. They arrive exhausted and without luggage. Often with health problems. They need clothing, sanitary products, food and medical attention.   
 About  ICOON/AMBERPRESS-Publishing 
 ICOON is a picture dictionary for all languages in the world with over 2000 essential everyday symbols. Simply pointing to the right icon suffices to put your conversation partner in the picture. Regardless of whether you need a toothbrush, safety pin or swimming shorts, or want to explain hay fever without using words, ICOON speaks the world s most universal language: pictures. Gosia Warrink first published the ICOON global picture dictionary in 2007 at her own publishing house AMBERPRESS. Today, there are 5 ICOON book editions available as well as an app for iPhone devices. 
 About  busuu 
 busuu was founded in 2008 with the vision to enable people from all over the world to learn a new language. The company offers audiovisual language classes for 12 different languages for desktop PC and as apps for mobile devices. In addition, users can practice acquired language skills with native speakers via the online platform. Every user of busuu is not only a student of a foreign language, but also a tutor in his/her native language. With more than 55 million users, busuu belongs to the world s leading providers of digital language learning apps. 
 #germanforrefugees     
 #refugeeswelcome  

  2016 busuu ltd. 

#germanforrefugees

 Home 
 Pressemitteilung 
 busuu.com 
 ICOON 
 English 
 Deutsch 

 #germanforrefugees 
 Kostenlos Sprachenlernen f r Fl chtlinge 

 Internet Startups stellen kostenlose Online-Sprachkurse und Bildw rterb cher zum schnellen Spracherwerb f r Fl chtlinge bereit 

        Alle Deutsch und Englisch Kurse von busuu sind nun f r arabisch sprechende Benutzer gratis verf gbar. Diese Kurse beinhalten mehr als 200 audiovisuelle Lerneinheiten pro Sprache und decken allt gliche Themen wie "Lebensmittel", "Im Supermarkt", "Kleider" oder "Beim Arzt" ab. Die kostenlosen Kurse sind derzeit auf mobilen Android-Ger ten verf gbar.

        Die kostenlose ICOON for refugees App f r Android beinhaltet tausend wichtigste Symbole und Fotos aus dem Bildw rterbuch ICOON. Ein speziell f r die Bed rfnisse der Gefl chteten erstelltes Flugblatt und ein Deutsch Lernhilfe Poster stehen kostenlos zum Download bereit. Zus tzlich gibt es 6 verschiedene Editionen der ICOON Bildw rterb cher. 

 Um die Premium Mitgliedschaft zu aktivieren: 
        1.  Die App  herunterladen 
        2. In der App 'Einstellungen' und dann 'Gutscheincodes' w hlen 
        3. Folgenden Code eingeben:  Germanforrefugees  oder " "  (Anmerkung: der Gutscheincode funktioniert nur mit arabischer Smartphone Einstellung)

	        Den   ICOON - Kommunikationsf hrer  hier herunterladen . 
 Alle Informationen zum Projekt unter  www.icoonforrefugees.com 

 Sprachkenntnisse sind entscheidend, um sich in einer neuen Umgebung zurecht zu finden und der Schl ssel zur Integration. Um dies den Fl chtlingen m glichst schnell und einfach zu erm glichen, nehmen wir als Unternehmer unsere soziale Verantwortung wahr , erkl ren Initiator der Aktion, Karsten Warrink, sowie Bernhard Niesner von busuu und Gosia Warrink von ICOON. 

 ber  busuu 
 busuu wurde im Jahr 2008 mit der Vision gegr ndet, es jedem Menschen auf der Welt zu erm glichen, eine neue Sprache zu lernen. Das Unternehmen bietet audiovisuelle Sprachkurse f r 12 verschiedene Sprachen am Desktop und via mobile Apps an. Zus tzlich k nnen die erworbenen Sprachkenntnisse direkt mit anderen Muttersprachlern der Onlineplattform praktiziert werden. Jeder Benutzer von busuu ist nicht nur Student einer Fremdsprache, sondern auch Tutor seiner eigenen Muttersprache. Mit  ber 60 Millionen Nutzern geh rt busuu zu den weltweit f hrenden Anbietern digitaler Sprachlern-Apps.  
 ber  ICOON/AMBERPRESS-Publishing 
 Gosia Warrink brachte das ICOON Bildw rterbuch erstmals 2007 im Eigenverlag AMBERPRESS heraus. Mittlerweile gibt es 5 ICOON-Bucheditionen sowie eine App f rs iPhone.  

  2016 busuu ltd. 

#germanforrefugees

 Home 
 Pressemitteilung 
 busuu.com 
 ICOON 
 English 
 Deutsch 

 #germanforrefugees 
 Kostenlos Sprachenlernen f r Fl chtlinge 

 Internet Startups stellen kostenlose Online-Sprachkurse und Bildw rterb cher zum schnellen Spracherwerb f r Fl chtlinge bereit 
 London/Berlin/Wien, 30. September 2015    busuu, die weltweit gr te Sprachlerncommunity mit mehr als 55 Millionen Nutzern, und ICOON, das weltweit erfolgreiche Bildw rterbuch f r alle Sprachen dieser Welt, stellen ihre Produkte ab sofort kostenlos f r Fl chtlinge bereit. 
 Sprachkenntnisse sind entscheidend, um sich in einer neuen Umgebung zurecht zu finden und der Schl ssel zur Integration. Um dies den Fl chtlingen m glichst schnell und einfach zu erm glichen, nehmen wir als Unternehmer unsere soziale Verantwortung wahr , erkl ren Initiator der Aktion, Karsten Warrink, sowie Bernhard Niesner von busuu und Gosia Warrink von ICOON ihre Motivation. 
 Die weltweit f hrende Sprachlernplattform busuu stellt alle Deutsch- und Englischkurse f r arabischsprachige Benutzer ab sofort kostenlos zur Verf gung. Diese Kurse beinhalten mehr als 200 audiovisuelle Lerneinheiten pro Sprache und decken allt gliche Themen wie  Lebensmittel ,  Im Supermarkt ,  Kleidung  oder  Beim Arzt  ab. Die kostenlosen Kurse sind derzeit auf mobilen Android-Ger ten verf gbar. Nutzer auf der arabischsprachigen Version der Android-App m ssen einfach den Code  Germanforrefugees  eingeben und schalten damit die Premium-Mitgliedschaft der beliebten App frei. 
 ICOON, das Bildw rterbuch f r alle Sprachen dieser Welt, unterst tzt das Projekt mit der kostenlosen iPhone-App f r Fl chtlinge und hat bislang mehr als 2.000 Bildw rterb cher gespendet. Die App beinhaltet eine Auswahl der tausend wichtigsten Symbole und Fotos aus dem Bildw rterbuch. Ein speziell f r die Bed rfnisse der Gefl chteten zusammengestelltes Bildw rterbuch mit  ber 2.000 Symbolen, Fotos und Phrasen in ihren wichtigsten Sprachen ebenso wie eine Android-App werden in K rze erscheinen. 
 Die Verteilung der Apps erfolgt direkt  ber die Android und Apple App Stores. Auf einer zentralen Internetseite unter  www.germanforrefugees.com  finden Fl chtlinge, Hilfsorganisationen sowie Helferinnen und Helfer zudem weitere Informationen. Hier findet sich auch das Bildw rterbuch mit den wichtigsten Symbolen zum Ausdrucken. 
 Pressekontakt 
 #germanforrefugees 
Karsten Warrink 
c/o AMBERMEDIA 
+49 30 288 84 85   50 
warrink@ambermedia.de 
 Europ ische Unternehmer schlie en sich zusammen 
 Initiator der gemeinsamen Aktion ist  Karsten Warrink , Gr nder der Marketingagenturen AMBERMEDIA und jubelkind, und Mitglied der internationalen Entrepreneurs  Organization und bei Entrepreneur s Pledge. 
 Einer gro en Zahl an Unternehmern in Deutschland und Europa ist es wichtig, schnell, unkompliziert und fl chendeckend zu helfen. Mit den Sprach-Apps von busuu und ICOON k nnen wir sofort und in gro em Stil Kommunikation und Sprachkenntnisse der Fl chtlinge verbessern. Ich bin sehr gl cklich, dass wir so schnell etwas auf die Beine stellen konnten.   
 Bernhard Niesner , CEO   Co-Gr nder von busuu, sagt:  Es ist unsere Verantwortung, diesen Menschen in Not zu helfen. Sprachkenntnisse sind ein wesentlicher Bestandteil der erfolgreiche Integration und wir von busuu freuen uns dar ber, unseren Beitrag zu leisten. 
 Gosia Warrink , Inhaber AMBERDESIGN und AMBERPRESS sagt:  Die Gefl chteten ben tigen bei ihrer Ankunft eine erste Kommunikationshilfe, um sich verst ndigen zu k nnen. Sie kommen ersch pft und ohne Gep ck an. Oft mit gesundheitlichen Problemen. Sie brauchen Kleidung, Hygieneartikel, Nahrung und  rztliche Hilfe. 
 ber  ICOON/AMBERPRESS-Verlag 
 Gosia Warrink brachte das ICOON Bildw rterbuch erstmals 2007 im Eigenverlag AMBERPRESS heraus. Mittlerweile gibt es 5 ICOON-Bucheditionen sowie eine App f rs iPhone.  
 ber  busuu 
 busuu wurde im Jahr 2008 mit der Vision gegr ndet, es jedem Menschen auf der Welt zu erm glichen, eine neue Sprache zu lernen. Das Unternehmen bietet audiovisuelle Sprachkurse f r 12 verschiedene Sprachen am Desktop und via mobile Apps an. Zus tzlich k nnen die erworbenen Sprachkenntnisse direkt mit anderen Muttersprachlern der Onlineplattform praktiziert werden. Jeder Benutzer von busuu ist nicht nur Student einer Fremdsprache, sondern auch Tutor seiner eigenen Muttersprache. Mit  ber 55 Millionen Nutzern geh rt busuu zu den weltweit f hrenden Anbietern digitaler Sprachlern-Apps.  
 #germanforrefugees     
 #refugeeswelcome  

  2016 busuu ltd. 

Digital language courses in English and German free of charge.

WHOLE PROJECT MARK