Trace the Face - Photo-based online tracing in Europe  | Deutsches Rotes Kreuz - Suchdienst

 Skip to main content 

 English Deutsch   
 Main menu Frontpage 
 Contact 
 DRK.de 
 Intern 

 Deutsches Rotes Kreuz - Suchdienst 

 Initiate Tracing Requests What we do About us Publications Press 

 Search form 

 Search  

 Trace the Face  - Photo-based online tracing   

 Trace the Face - also on Facebook     

 Link to the international Tracing Service Network  

 GRC Tracing Service in your area: 

            Region           

            Name Staff Member           

                          You are here:  Home  / Trace the Face - Photo-based online tracing in Europe                        

 Trace the Face - Photo-based online tracing in Europe    

 Depiction of a poster displaying information on the new option of online tracing in Europe 

 Since September 2013 persons who lost contact with their family members involuntarily because of armed conflict, catastrophes, flight, expulsion or migration can search online for their relatives with their own photo. The joint project  Trace the Face  of the ICRC and now 23 European Red Cross Tracing Services is offering to persons concerned the option to turn directly to the Red Cross Society of their country of residence and ask for publication of a photography showing themselves as a searching person. This photography will be displayed on the Tracing Service Website of the ICRC,  www.tracetheface.org , and be coupled with information as to the person s relatives that are being searched for, i.e. a brother or the mother etc. Users can narrow down the number of the displayed pictures by applying research criteria such as age, gender and country of origin. As soon as the person being searched for or persons in contact with him or her learn about the search, they can use a prepared reply form to contact the Tracing Service of the Red Cross that is already in contact with the person that initiated the search. The country where this Red Cross Tracing Service is based is not revealed in order to protect the person who initiated the search from unwanted approaches. The whereabouts of the persons initiating a search remain anonymous in the context of photo-based online tracing. In July 2015 385 persons were already using the new possibility of photo-based online tracing with their photo. 
 On a monthly basis the ICRC is using a selection of photographs displayed on the website to design a new poster, also referring to the new option of online search, and furthermore, including contact details of the responsible Red Cross Tracing Service. This poster will be distributed as widely as possible within the countries participating in the scheme and will be put up conspicuously at places known to be frequented by refugees and migrants. 
 First successes of the project are becoming apparent 
 Owing to the photo-based online tracing, as early as two months after the initiation of the project,  a Syrian woman who had fled to Germany was able to find her parents  from whom she had been separated during her flight. 
 In March 2014 a man from Afghanistan recognized his wife on one of the photos displayed on the ICRC s Tracing Service website. The wife and two daughters had been separated from the other family members during the flight when they were crossing the river Evros in Turkey. She had already feared that her husband and her sons had drowned. 
 In December 2014 an Afghan mother was able to find her son via the photo-based online tracing . Neighbors in Kabul had recognized her son on one of the photos. She had already been looking for her son for eight years. Her son had fled on to Europe after the family had to go to Iran because of the conflict in Afghanistan. 
 In February 2015 the GRC Tracing Service helped a young man from Afghanistan to find his family after months of uncertainty . The family was fleeing the country and was brutally separated in Turkey near the border with Iran and since then he knew nothing about the whereabouts of his father, mother and brothers. The son contacted the GRC Tracing Service with a tracing request, while the father had independently contacted the ICRC in Peshawar, Pakistan. Due to various similarities in the accounts they gave, and thanks to the online tracing system with photos, it was possible to reunite the family again. 
 In March 2015, two more men from Afghanistan were reunited with their families thanks to the online tracing with photos. Read their stories  here . 
 You will find further information regarding  Trace the Face , photo-based online tracing, (in Arabic, English, French and Spanish) here:  www.tracetheface.org . 
 Do you want to publish your own photo on the website? In this case please contact  the staff of your local GRC Tracing Service support centre . 

 Secondary menu Privacy 
 Legal Notice 
 Sitemap 

Person-finding platform for refugees. 

A European sub-project of the Red Cross's "Restoring Family Links" initiative. People searching for family members can upload details and a photo in the hope that the person they are seeking will also use the service and see the post.
WHOLE PROJECT MARK