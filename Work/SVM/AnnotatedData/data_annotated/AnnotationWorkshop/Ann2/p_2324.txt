FA Mars Just Play kickabouts free for a month

 skip to main content 

 My account 
 Log out 

 Sign in 

 Search the FA 

 England 

 England 

 Men's Senior 

 Men's Senior 

 Fixtures 

 Results 

 Squad 

 Women's Senior 

 Women's Senior 

 Fixtures 

 Results 

 Squad 

 Men's Under 21s 

 Men's Under 21s 

 Fixtures 

 Results 

 Squad 

 Youth Teams 

 Youth Teams 

 Fixtures 

 Results 

 Disability Teams 

 Futsal 

 Futsal 

 Fixtures 

 Results 

 Squad 

 England Supporters Club 

 Popular Pages 

 World Cup squad named 

 Lions visit lifts local school 

 For Girls 

 For Girls 

 Play 

 Support 

 Competitions 

 Competitions 

 The Emirates FA Cup 

 The Emirates FA Cup 

 Fixtures 

 Results 

 Live 

 gamers 

 #CUPSTORY 

 The FA People's Cup 

 The FA People's Cup 

 About 

 FAQs 

 Terms and Conditions 

 The Buildbase FA Trophy 

 The Buildbase FA Trophy 

 Fixtures 

 Results 

 About 

 The Buildbase FA Vase 

 The Buildbase FA Vase 

 Fixtures 

 Results 

 About 

 Women's 

 Women's 

 The FA Women's Super League 

 The FA Women's Premier League 

 The SSE Women's FA Cup 

 The FA WSL Continental Tyres Cup 

 The FA Girls' Youth Cup 

 The FA WSL Development League 

 Youth Football 

 Youth Football 

 The FA Youth Cup 

 The FA County Youth Cup 

 The FA Girls' Youth Cup 

 Grassroots 

 Grassroots 

 The FA Sunday Cup 

 The FA Inter-League Cup 

 The FA Community Shield 

 Popular Pages 

 Daggers v Orient 

 Mariners stun City 

 Get Involved 

 Get Involved 

 Play 

 Play 

 Facilities 

 Disability 

 Coach 

 Coach 

 Courses 

 Continue Learning 

 Referee 

 Community 

 Community 

 FA Community Awards Supported by McDonald s 

 McDonalds FA Charter Standard Kit Scheme 

 McDonald s FA Community Football Days 

 Popular Pages 

 Grassroots heroes honoured 

 And the winner is... 

 Women's 

 Women's 

 Leagues and Competitions 

 Get involved 

 Talent Pathway 

 Women's Senior 

 For Girls 

 Popular Pages 

 Back with a bang 

 Women's Cup Final tickets 

 Rules and Regulations 

 Rules and Regulations 

 Laws of the Game   FA Rules  

 Discipline 

 Discipline 

 Suspensions 

 Fair Play tables 

 Written Reasons 

 Player Essentials 

 Safeguarding 

 Inclusion and anti-discrimination 

 Anti-doping 

 Policies 

 Policies 

 Betting Rules 

 Player Status - Intermediaries 

 Player Status - Registrations 

 Popular Pages 

 Deception panel named 

 'Sin bins' to be trialled 

 About the FA 

 About the FA 

 What we do 

 What we do 

 Strategy 

 Sustainability 

 Financial Statements 

 Who we are 

 Who we are 

 The FA Board 

 The FA Council 

 FA Committees 

 The FA Management 

 County FAs 

 St. George's Park 

 St. George's Park 

 Discover 

 Experiences 

 Hilton Hotel 

 Performance 

 Outdoor Leadership Centre 

 Wembley Stadium 

 Partners 

 Careers 

 Contact us 

 Popular Pages 

 FA statement 

 FA will bid to host Euro 2021 

 Tickets 

 Merchandise 

 Wembley 

 St. George's Park 

 More FA Sites 

 Go back 

 More FA Sites 
 Other Sites 

 For Players 

 Go back 

 For Players 

 County FA 

 FA Skills 

 Football Mashup 

 Full Time 

 Just Play 

 Whole Game 

 For Coaches 

 Go back 

 For Coaches 

 Coach Store 

 County FA 

 FA Charter Standard 

 Full Time 

 Learning 

 Licensed Coaches Club 

 Tutor Stores 

 Whole Game 

 For Referees 

 Go back 

 For Referees 

 Anti Doping 

 County FA 

 Learning 

 MOAS 

 Whole Game 

 Young Referees 

 For Club League Officials 

 Go back 

 For Club League Officials 

 3G Football Turf Pitch Register 

 Anti Doping 

 County FA 

 Football Facility Enquiry 

 Full Time 

 Whole Game 

 For Parents and Volunteers 

 Go back 

 For Parents and Volunteers 

 County FA 

 FA Schools 

 Respect Guide 

 Club Wembley 

 FA Careers 

 The FA 

 QUICK LINKS 

 Full Time 
 FULL-TIME offers you an easy way to manage your football leagues online.  

 Whole Game 
 Enables clubs and referees to administer their day to day activities 

 My Account 
 Log out 

 Sign in 

 Get Involved 

 Play 

 Coach 

 Referee 

 Community 

 Home 

 Get Involved 

 Just Play walking football 

 Mars Just Play walking football sessions prove to be a hit 

 Monday 16 Jun 2014 

  Shares 

 There are 20 walking football centres nationwide 

 Partner Message 

 The month-long FA Mars Just Play for free campaign is under way   and adults up and down the country are being encouraged to dust off their boots and get playing again. 

 A key aim of the nationwide initiative is also to show that football is accessible to everyone and anybody can get involved in the beautiful game. 
 As part of that commitment a special brand of walking football Just Play sessions are also on offer. 
 Walking football is a slower version of the game and is designed to give players who may not be able to play regular football the opportunity to play.  

 FA Mars Just Play 

 1,400 kickabouts nationwide 
 Free to take part until 13 July 
 20,000 places 
 Female-only and walking football sessions available 

 The rules are exactly the same as the regular version of the game, and there are no age restrictions placed on walking football, so individuals who may not have played for years or perhaps suffer from a mobility injury can play again in confidence. 
 The FA Mars Just Play campaign kicked-off on the 12 June and runs until 13 July. 
 Adults of any age, ability or gender can get involved by simply visiting the Just Play home page, searching for their nearest session and signing up. 
 The sessions are recreational, light-hearted and most importantly   fun! 
 Since the campaign launched a week ago the walking football sessions have proved particularly popular   with players ranging from their 40s right up to their 80s, showing the variation of those wanting to get involved in the game, and demonstrating the campaign truly does make it accessible to all. 

 FA Mars Just Play kickabouts are free until 13 July 

 Participants so far have commented on how the slower nature of the game allows older players to return to a sport they once loved - but have had no means to get back involved or methods of finding likeminded people. 
 Walking football also provides an opportunity for participants to be able to kick a football again and re-live their years of being part of a team. 
 The cup of tea after the game and the post-match banter is another enjoyable part of the sessions  adding a social element to a thoroughly enjoyable experience. 
 FA Mars Just Play have 20 centres nationwide that are putting on walking football sessions. 
 There are also 1,400 additional kickabouts available across the country   offering regular football for all. 
 So to get involved   in a walking or regular football session   simply click on the link below and find your nearest session! 
 Click here to sign up for the FA Mars Just Play for free. 

 By FA Staff 

  Shares 

 Just Play for free 

 Just Play for free 

 A celebration of football 

 A celebration of football 

 FA Mars Just Play for free 

 FA Mars Just Play for free 

 Plan your journey to Wembley 

 Plan your journey to Wembley 

 Sir Bobby's birthday treat 

 Sir Bobby's birthday treat 

 Daggers v Orient 

 Daggers v Orient 

 Cooper calls on contacts 

 Cooper calls on contacts 

 Social links: 

 England 

 Facebook 
 Twitter 
 YouTube 

 Emirates FA Cup 

 Facebook 
 Twitter 
 YouTube 

 FAWSL 

 Facebook 
 Twitter 
 YouTube 

 SSE Women's FA Cup 

 Facebook 
 Twitter 
 YouTube 

 St. George's Park 

 Twitter 
 YouTube 

 About The FA 

 Twitter 
 YouTube 

 All the latest directly to your inbox 
 Get all the latest football news sent directly to your inbox 

 Email address 

 Contact Us 
 Privacy policy 
 Terms of use 
 Anti-Slavery 

 The Football Association   2001 - 2017. All Rights Reserved 

 Our website uses cookies to help improve your experience 

                    A functional cookie (which expires when you close your browser) has already been placed on your machine. 
                     More details 

 FIFA and national Football Associations, local teams tend to play in tournaments although leagues likely to be set up.
WHOLE PROJECT MARK