Welcome - The Silver Line | The free, 24 hour, confidential helpline for older people.

 Navigate Welcome What We Do Our story Get Involved Media Our Partners Contact Us 

 The Silver Line 

 Contact Us 

 Welcome 
 What We Do 

 Latest News 
 Referrals 
 Our helpline 
 Growing The Silver Line Helpline 
 Jo Cox Commission on Loneliness 
 Resources and leaflets 
 Annual reports 
 Films 
 Sign up to our newsletter 
 Calling from a Mobile 
 Join the Team 

 Who We Are 

 Our story 
 Our Founder 
 Our People 
 Our Partners 

 Get Involved 

 Donate 
 Other ways to donate 
 Christmas Shop 
 Give with confidence 
 Volunteering 
 Introducing The Silver Line Bears 
 Fundraise   getting started 
 Silver Week 2018 
 Challenge Events 
 Giving in memory 
 Leave a gift in your will 
 Corporate Partnerships 

 Corporate Volunteering 

 Shop 

 Recycling 

 Donate now   

 On your marks.......bake! 

 Please help to support older people like Jack. Donate now 

 Need help? Call us ANYTIME on:  0800 4 70 80 90 

 The Silver Line is the only free confidential helpline providing information, friendship and advice to older people, open 24 hours a day, every day of the year. 

 Change a life 
 By donating just  5, you'll pay for 1 call to our helpline from a lonely older person who may not have spoken to anyone for days  

 Donate 

 Silver Bake Off Anyone? 
 From biscuits to buns. Create your own show stoppers for the Silver Line 

 Get Silver Baking 

 Get your own Silver Line Bear 
 The perfect furry companion for any sport enthusiast - with four teds to choose from. Get your paws on them fast!  

 Get yours now 

 What the service means  
 I ve called late at night when I ve felt anxious and not been able to sleep   it felt so good to chat through a worry or problem. 

 The difference it makes 

 Christmas Shop  
 Christmas is coming! We have a wide range of Christmas items available in our online shop, including cards, gifts, wrapping paper and crackers.  

 Shop 

 Get involved  
 Find out more about how you can make a difference to older people across the UK 

 Find Out More 

 If you would like to sign up to our seasonal newsletter please fill in your details below: Email address * 

 CAPTCHA Email This field is for validation purposes and should be left unchanged. 

 Copyright 2013 The Silver Line - All rights reserved 

 Facebook   Twitter   Google+   Twitter   
 Follow Us 

 Site Links 
 What We Do 
 Our story 
 Get Involved 
 Contact Us 

 Quick Links 
 Contact Us 
 Our Partners 
 Media 
 Get Involved 
 VCC 

 Media 
 Contact Us 
 Our Partners 
 What We Do 
 Welcome 

 This site uses cookies. By continuing to browse the site you are agreeing to our use of cookies. See our  Cookie Policy . Our  Privacy Policy  is also available 
 The Silver Line Helpline 
 Registered in England   Wales as a Charity with number 1147330   as a Company limited by guarantee with number 08000807. Also registered in Scotland as Charity number SC044467. 
 Registered office address: Trade Tower, Calico Row, London, SW11 3YH  

 A UK national telephone service which is a charity and company limited by guarantee established in 2012.
WHOLE PROJECT MARK