Leve HELE LIVET - Stavanger kommune 

 G  til s k 

 G  til innhold 

 G  til toppnavigasjon 

 G  til lokalnavigasjon 

 Til startsiden 

 Talende Web 
 Personvern og sikkerhet 
 Nettstedskart 

		Endre skrift 

 Startsiden 

 Tilbud og tjenester 
 Politikk og demokrati 

 Jobb og karriere 

 Om kommunen 

 English 

 S k 

 Startsiden Tilbud og tjenester Helse- og sosialtjenester Helsehuset Stavanger Leve HELE LIVET 

 Frisklivssentralen 

 Forebyggende bes k 

 Leve HELE LIVET 

 Brukerhistorier 

 Temasamlinger  

 Hverdagsrehabilitering 

 Trening 

 Sykepleieklinikk 

 Utviklingssenter (USHT) 

 Velferdsteknologi 

 Psykologtjeneste voksne 

 Hverdagsrehabilitering 

 P r rendekoordinator 

 Leve HELE LIVET 

 Publisert av 

 Publisert 
 17.12.2013 
 Sist endret 
 26.07.2016 

 Skriv ut 

 Del innhold 

 Tips en venn 

 Mottakers e-postadresse 

 * 

 Din e-postadresse 

 * 

 Tittel 

 * 

 Beskjed 

 * 

 Del artikkelen 

 Facebook 
 Twitter 
 Origo 

 Leve HELE LIVET er et omstillingsprosjekt der m let er forn yde brukere som gjennom forebygging, rehabilitering, teknologi og sosiale nettverk klarer seg selv i stedet for   bli passive mottakere av hjelp og pleie. 
 For at Stavanger kommune skal lykkes med prosjektet, m  holdninger endres hos brukere, p r rende, ansatte og i organisasjonen ellers. Ansatte innen helse- og sosialtjenesten skal ikke lenger gi kompenserende tjenester som fratar brukerne even til egenomsorg. I stedet skal ansatte trene og st tte brukerne i   mestre hverdagen slik at de kan leve hjemme lengst mulig, opprettholde funksjonsniv et sitt, delta aktivt i samfunnet og dermed f   kt livskvalitet. 
 Leve HELE LIVET best r av fire delprosjekter som alle har fokus p  tidlig oppsporing av sykdom/funksjonssvikt, forebygging og rehabilitering: 

 Helsefremmende og forebyggende tiltak (forebyggende hjemmebes k) 
 Hverdagsrehabilitering 
 Hverdagsmestring 
 Velferdsteknologi 

 K ommuneplanen 2010-2025  har god helse for alle som et delm l. En av strategiene er   forebygge og begrense sykdom og tilrettelegge for at flere brukere skal kunne bo hjemme lengst mulig. Kommuneplanen peker ogs  p  samhandlingsreformen og at det skal l nne seg   forebygge sykdom framfor   reparere i etterkant. 

 Her finner du oss: Gamle Stavanger sykehus Arkitekt Eckhoffs gate Inngang 7, 3. etasje 4010 Stavanger  Tlf: 51 50 81 00 Epost:  helsehuset@stavanger.kommune.no 

 Aktivitets og treningskatalog 

 Kommuneplanen for 2010-2025 

 Strategi for implementering av velferdsteknologi 2014-2017 

 Omsorg 2025 

 Samhandlingsreformen 

 Analyse av hverdagsrehabiliteringsteamet 

 Evalueringsrapport pilot hverdagsrehabilitering 

 Evalueringsrapport Hverdagsrehabilitering 2012-15 

 Stavanger kommune,  vre Kleivegate 15, postboks 8001, 4068 Stavanger,  Org.nr: 964 965 226 
 Tlf: +47 51 50 70 90 

 Ansvarlig redakt r: Kommunikasjonssjef Marianne J rgensen 
 Nettredakt r: Tone Gaard   Nyhetsredakt r:  yvind Berekvam 

 Facebook 

 Informasjonskapsler p  www.stavanger.kommune.no 

 Ved   bruke nettstedet v rt samtykker du i v r bruk av informasjonskapsler i henhold til v r informasjonskapsel-policy. Les mer p  v re  nettsider 

	Leve HELE LIVET - Stavanger kommune 

 G  til s k 

 G  til innhold 

 G  til toppnavigasjon 

 G  til lokalnavigasjon 

 Til startsiden 

 Talende Web 
 Personvern og sikkerhet 
 Nettstedskart 

		Endre skrift 

 Startsiden 

 Tilbud og tjenester 
 Politikk og demokrati 

 Jobb og karriere 

 Om kommunen 

 English 

 S k 

 Startsiden Tilbud og tjenester Helse- og sosialtjenester Helsehuset Stavanger Leve HELE LIVET 

 Frisklivssentralen 

 Forebyggende bes k 

 Leve HELE LIVET 

 Brukerhistorier 

 Temasamlinger  

 Hverdagsrehabilitering 

 Trening 

 Sykepleieklinikk 

 Utviklingssenter (USHT) 

 Velferdsteknologi 

 Psykologtjeneste voksne 

 Hverdagsrehabilitering 

 P r rendekoordinator 

 Leve HELE LIVET 

 Publisert av 

 Publisert 
 17.12.2013 
 Sist endret 
 26.07.2016 

 Skriv ut 

 Del innhold 

 Tips en venn 

 Mottakers e-postadresse 

 * 

 Din e-postadresse 

 * 

 Tittel 

 * 

 Beskjed 

 * 

 Del artikkelen 

 Facebook 
 Twitter 
 Origo 

 Leve HELE LIVET er et omstillingsprosjekt der m let er forn yde brukere som gjennom forebygging, rehabilitering, teknologi og sosiale nettverk klarer seg selv i stedet for   bli passive mottakere av hjelp og pleie. 
 For at Stavanger kommune skal lykkes med prosjektet, m  holdninger endres hos brukere, p r rende, ansatte og i organisasjonen ellers. Ansatte innen helse- og sosialtjenesten skal ikke lenger gi kompenserende tjenester som fratar brukerne even til egenomsorg. I stedet skal ansatte trene og st tte brukerne i   mestre hverdagen slik at de kan leve hjemme lengst mulig, opprettholde funksjonsniv et sitt, delta aktivt i samfunnet og dermed f   kt livskvalitet. 
 Leve HELE LIVET best r av fire delprosjekter som alle har fokus p  tidlig oppsporing av sykdom/funksjonssvikt, forebygging og rehabilitering: 

 Helsefremmende og forebyggende tiltak (forebyggende hjemmebes k) 
 Hverdagsrehabilitering 
 Hverdagsmestring 
 Velferdsteknologi 

 K ommuneplanen 2010-2025  har god helse for alle som et delm l. En av strategiene er   forebygge og begrense sykdom og tilrettelegge for at flere brukere skal kunne bo hjemme lengst mulig. Kommuneplanen peker ogs  p  samhandlingsreformen og at det skal l nne seg   forebygge sykdom framfor   reparere i etterkant. 

 Her finner du oss: Gamle Stavanger sykehus Arkitekt Eckhoffs gate Inngang 7, 3. etasje 4010 Stavanger  Tlf: 51 50 81 00 Epost:  helsehuset@stavanger.kommune.no 

 Aktivitets og treningskatalog 

 Kommuneplanen for 2010-2025 

 Strategi for implementering av velferdsteknologi 2014-2017 

 Omsorg 2025 

 Samhandlingsreformen 

 Analyse av hverdagsrehabiliteringsteamet 

 Evalueringsrapport pilot hverdagsrehabilitering 

 Evalueringsrapport Hverdagsrehabilitering 2012-15 

 Stavanger kommune,  vre Kleivegate 15, postboks 8001, 4068 Stavanger,  Org.nr: 964 965 226 
 Tlf: +47 51 50 70 90 

 Ansvarlig redakt r: Kommunikasjonssjef Marianne J rgensen 
 Nettredakt r: Tone Gaard   Nyhetsredakt r:  yvind Berekvam 

 Facebook 

 Informasjonskapsler p  www.stavanger.kommune.no 

 Ved   bruke nettstedet v rt samtykker du i v r bruk av informasjonskapsler i henhold til v r informasjonskapsel-policy. Les mer p  v re  nettsider 

The active ageing agenda is becoming increasingly recognised at local, national and international levels. It is now nearly 15 years since the World Health Organisations Active Ageing: A Policy Framework was published in 2002 and it will soon be a decade since the launch of the Age-Friendly Network of Cities and Communities to assist local government and communities around the world to adapt for an ageing population. There are many challenges to be faced and to some extent they vary depending on the prevailing level of affluence and social welfare provision. In wealthy Norway with its highly developed social welfare system there is a large degree of economic and social security for people of all ages across the life course but sustaining this situation is likely to require socially innovative change.
The city of Stavanger in south west Norway is a city of approximately 130,000 people within the city municipality and around 320,000 in the greater metropolitan area. The city has been at the forefront of active ageing policy within Norway for the last decade, and in 2012 launched the Leve Heve Livet (Lifelong Living) programme to promote active ageing by empowering people of all ages to take greater responsibility for their health and well-being. In 2013, Stavanger was one of 31 cities from around the world selected by competitive bidding to work with IBM, the multinational IT company, to be part of the IBM Smarter City programme. This programme develops ICT based solutions to a range of issues facing cities around the world including transport, sustainability and using big data to improve public services. Stavanagers Lifelong Living programme included several social innovations including the Everyday Rehabilitation programme that had been initiated in the Danish municipality of Fredericia in 2008 and produced impressive results in enabling older people to live independently following a programme of intensive rehabilitation.
The Stavanger version was piloted in the Madla district from November 2012 to April 2013 and an evaluation of the 34 older service users showed considerably better functioning after intensive rehabilitation and improved worker satisfaction. Preventive home visits for older people aged 80 and over have also been initiated and health centres have developed wellness programmes focusing on mental health and well-being alongside obesity and weight management programmes for people of all ages. After interviews with a range of stakeholders, the IBM consultants made six key points about active ageing in Stavanger. The first was the importance of communication to change the mind set of people from the expectation of receiving services to living independently and self-sufficiently for longer through active ageing over the life course. They advocated an office for active ageing to improve the management and coordination of activities and becoming a member of the WHO age-friendly city network. A more holistic and elder-centric approach to health care with greater use of ICT for sharing data and e-health such as consultations over the web were also advocated. An innovative budget for the next five years to pilot and evaluate new approaches was advocated and the further development of welfare technologies, notably tele-health and e-health, through the citys fibre-optic network.
In relation to active ageing, Stavangers Lifelong Living programme is a fine example of a locally developed active ageing strategy that has encouraged several social innovations including Everyday Rehabilitation and Preventive Home Visits which have both been shown to be effective in Denmark that effectively improve access to health and social care services.
The IBM Smart City dimension indicates increasing use of ICT in the provision of a range of health and social care services. The primary aim of the Lifelong Living programme and the Smart City programme is to enable active ageing over the life course so that older people are more capable of living independently for longer for their own benefit and to contain the costs of social welfare provision in meeting the long term needs of older people in an ageing population. This will be achieved when a greater share of life expectancy in older age (after 55 years) is spent in good health rather than limited by long term health conditions.
WHOLE PROJECT MARK