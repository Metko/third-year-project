HeadEst - Summary 

 headest 
 tegevused 
 uudised 
 galerii 
 headIdeed 
 kontakt 

 Summary  
 Youth exchange "The Story of My Life"  
   July 30 - August 7, 2012  
   Tallinn and Venevere, Estonia  
 The project  The Story of My Life  created an environment for old and young people to meet and share their views of life and exchange different experiences. Youth exchange was initiated with the idea to celebrate and support European Year of Active Ageing and Solidarity between Generations. During time spent together a common understanding of intergenerational relationships with its positive and negative aspects was created. The influence of the project is still visible as the participants carry on the ideas of active citizenship and personal development. 
   Young people from Estonia, Finland, Austria, Portugal, Italy, Belgium, Slovenia and Germany met active Estonian elderly people from two organisations N mme Vanameeste Klubi (N mme Old Men s Club) and Kohila Reuma hing (Kohila Union of Reumatic diseases). 
 Who were we? 
 Project  The Story of My Life  brought together international group of young people (from Estonia, Finland, Austria, Portugal, Italy, Belgium, Slovenia and Germany) and active Estonian elderly people from two organisations as N mme Vanameeste Klubi (N mme Old Men s Club) and Kohila Reuma hing (Kohila Union of Reumatic diseases). 
 What was it all about? 
 During ten days last summer the youth exchange  The Story of My Life  took place but unlike all the other youth exchanges it also included senior participants. Much discussion has been around the topic that different generations are not able to communicate with each other due to lack of common language and understanding. Keeping that in mind, this youth exchange put two different age groups together in order to enforce the communication and prove that in reality differences are not so big. 
 What we did? 
 The project consisted of three parts. In the beginning only young people met in order to get to know each other and discuss age and society matters in all Europe. During this time general overview of situation in Europe and youth way of thinking got much clearer. Although the countries participating were different the problems in Europe are quite similar   many countries face the questions of ageing and its impact on society, communicative gap between generations, youth problems like unemployment etc. 
   Italian participants showing stereotypes about young and old people in their country. 
 During the second phase of the project young people met elderly people. Different activities made the groups interact and discuss topics as active ageing, keys to long and happy life, need for intercultural and intergenerational dialogue. During this time also personal information was shared   all the people shared their travel stories, childhood dreams, work experiences etc. Additionally some new skills were learned and young people got part of history that happened before they were born (tools that were used decades ago, cars and their maintenance, career paths etc). As it came out, there is skepticism from both sides but in the process of dialogue the groups discovered that there are as many similarities as there are differences. 
   The poster depicting childhood dream jobs of old and young participants. 
 The highlight of the project was public event happening in N mme turg (local market) where idea of active ageing and intergenerational dialogue was introduced in an entertaining way. Elderly people introduced their activities to give good example also to the local people, young people shared their intercultural experience and introduced both the project and their home countries. The event was created and prepared by the whole group and met a great interest by people visiting the market. 
   Together preparing for the public event 
 The last phase of the project involved only young people again to sum up the experience. There were a lot of emotions and ideas to develop back at home. Long term results have shown that the project had a strong impact both on young and old participants. Pan-European contacts were made, but more importantly   many of the participants got motivation for self development. Elderly people have started to learn English and create projects with other similar organisations, younger participants have developed international projects. 
 What we learned from the project? 
 Understanding that people and cultures are different 
 Thanks to this youth exchange everybody experienced in reality how different cultures can be. And what is the most important part about this   it is a really great and amazing thing that there are these small and big differences. This camp and the people participating in it taught that people should always try to do their best to accept these differences and should also try to benefit from them   there is so much to learn from other cultures. 
 Understanding that the people and cultures are similar 
 Even though we came from different backgrounds and cultures (and our senior participants even from different generations!) we understood that many of our problems and joys are the same. Everybody realized that even if we might want different things in life but we all get sad and happy the same way. We are all equal, even though we are really different (considering our ages/cultures and so on). 
 Spending time with the senior participants 
 It was an extraordinary chance to meet and do things together with the seniors. It was really great to see that even when you get older, your life does not have to get boring! During this camp many of us realized for the first time that getting older is not a bad thing, it is just a normal thing that we all go through. Seniors taught us all that the key to be happy at any age is to be positive (no complaining!) and active also. 
 Getting rid of stereotypes 
 The biggest effect of this camp was getting rid of many stereotypes in our minds. If a person does not travel a lot or does not communicate with people from other cultures, she or he might have some kind of an image of certain nationalities. Not all Italians love pasta and wine and not all Estonians are really quiet and modest. 
 The same appears to be with different generations   older people seem to think that youngsters don t know much about life. The same goes for youngsters   many of us seem to think that older people are boring and very strict. This camp proved this really is not the case! We all are different and rather than making generalizations based on age or nationality everybody should make an effort to get to know each other. 
   Creating a world map   as it came out, many elderly people have been to places youngsters aren t even aware of. 
 Being active 
 This camp taught us how to be independent and dream big! We can really do anything. It does not matter how old you are or where you come from or what do you believe in! The only thing that does matter is that you have courage do dream big and also do something in order to make your dreams come true! 
   Group dancing at the public event in the market place, lead by senior ladies. 

 Some additional personal comments: 

 Its unbelievable how much      strangers can change the way you see your life. I am more confident after      the project and believe in myself 100%. The main message I got from the      project   everything is possible!  (told by a young participant) 
 You have to be open, free of      limitations and bold or at least aim at all of these. It goes both about      young and old people. Don t be alone!  (told by a senior participant) 
 I have never seen so cool      elderly people and as I have mentioned repeatedly to my friends   with      those old ladies I can always have party with. With uttermost certainty I      can tell that I don t look at any old person with the same attitude as I      did before.  (told by a young participant) 

 Explore more! 
 Gallery of the project www.headest.ee/home1/galerii/album-35/29/ 
 Video made by participants  http://www.youtube.com/watch?feature=player_embedded v=XHkVcqUvPJw 

 Noortevahetused 

 Sound Out Saksa15 

 Toimunud laagrid 

 Being Active Eestis 2007 

 Nature freaks Eesti08 

 Innovatsioon Eestis 09 

 Masters of Life Eestis10 

 Yes-We Can Eestis10 

 Woman Itaalia10 

 Care and Act Eesti11 

 Story of My Life 2012 

 The project in short 

 Pressiteated 

 Diary 

 Summary 

 quiz 

 Active aging promotion 

 Muljed projektist  

 pressiteade 

 Sports Itaalia13 

 Bridge It L ti13 

 Film Camp CZ13 

 Culture Point Lux13 

 Circus Saksa13 

 Be Inclusive Port13 

 Culture Gruusia13 

 Rolling Eesti13 

 The Rolling Citizens 

 Colonists Belgia14 

 Youth on board gruusia14 

 Circus saksa14 

 Saksa New Year 14 

 Expo itaalia15 

 coolinary kreeka15 

 "ICAYU" K pros15 

 Waste Belgia15 

 OUTDOOR Lux15 

 Eesti keele laagrid 

 Kooliprojektid 

 Koolitused 

 Kaasamisprojektid 

 Vabatahtlikud 

 kogemused ja muljed 

 MT  Headest 
 tel +372 641 4526 
 headest(at)headest.ee 

Active ageing takes place over the whole of the life course and this was recognised in the European Unions Year of Active Ageing and Solidarity between Generations that was marked in 2012 by a wide range of events across EU member states. The Story of My Life has been selected as an innovative example of an international and inter-generational project from a new member state that represents the potential benefits of this type of initiative.
The Story of My Life involved young people from Estonia, Finland, Portugal, Italy, Belgium, Slovenia and Germany spending 10 days together in Estonia in the summer of 2012 and consisted of three distinct elements. The first element involved the younger participants exchanging their experiences and views on the issues that they faced in their lives, ageing and how society needed to change. The second element, involved older people from Nõmme Vanameeste Klubi (Nõmme Old Men’s Club) and Kohila Reumaühing (Kohila Union of rheumatic diseases) sharing their life stories and experiences with younger participants.. The Nõmme Old Mens Club was formed in 2009 in the Tallinn district of Nõmme to provide opportunities for older men to meet up for social interaction and monthly lectures by invited speakers while the latter is a non-governmental organisation that works to improve provision for people with rheumatic diseases such as arthritis.
The middle element promoted greater inter-generational communication and understanding and paved the way for the final stage of the project, a public event on active ageing in the local market place to highlight active ageing and intergenerational solidarity among the wider population. Although this was a short term project it had some long-term benefits in encouraging some older people to learn English, to create new projects and be more active. For younger participants, this (type of) project increases their awareness of social issues, experience of different cultures and appreciate the similarities and differences across EU member states.
In relation to the active ageing index, the Story of My Life project encouraged social connectedness based on inter-generational communication and encouraged lifelong learning among older people. This type of pan-European project can be viewed as a way of promoting greater understanding across generations and member states that has a wide range of tangible and intangible benefits for participants.
WHOLE PROJECT MARK