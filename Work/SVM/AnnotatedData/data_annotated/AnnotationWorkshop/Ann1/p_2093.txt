Public sector innovation and local leadership in the UK and The Netherlands | JRF

 Jump to main content 

 JRF The Joseph Rowntree Foundation 

 Secondary Menu 
 About us 
 Our work 
 Media centre 

 Search 

 Site menu 
 Cities, towns and neighbourhoods Cities 
 Climate Change 
 Devolution 
 Economic development and local growth 
 Flooding 
 Inclusive Growth 
 Loneliness 
 Neighbourhoods 
 Planning 
 Regeneration 
 Services and local government 

 Housing Affordable housing 
 Care homes 
 Home ownership 
 Homelessness 
 Housing Regeneration 
 Housing market 
 Housing providers 
 Private rented sector 
 Retirement Housing 
 Social Housing 
 Tenant Participation 
 Tenants 

 Income and benefits Costs 
 Debt 
 Food 
 Living Standards 
 Living Wage 
 Minimum Income Standards 
 Pay 
 Pensions 
 Tax 
 Unemployment 
 Universal Credit 

 People Active Citizenship 
 Ageing Population 
 Carers 
 Child Poverty 
 Children 
 Dementia 
 Disabled People 
 Divorce / Family Breakdown 
 Education 
 Ethnicity 
 Gender 
 Independent Living 
 Lone Parents 
 Mental Health 
 Migration 
 Older People 
 Social Care 
 Young People 

 Society Ageing Society 
 Austerity 
 Civic Participation 
 Crime / Anti-social Behaviour 
 Faith and Religion 
 Government 
 Immigration 
 Local Government 
 Localism 
 Slavery 
 Social Exclusion 
 Voluntary Sector 

 Work Employment 
 Equality 
 Forced Labour 
 Labour Markets 
 Low Pay 
 Retirement 
 Skills 
 Working with Employers 

 Cities, towns and neighbourhoods Cities 
 Climate Change 
 Devolution 
 Economic development and local growth 
 Flooding 
 Inclusive Growth 
 Loneliness 
 Neighbourhoods 
 Planning 
 Regeneration 
 Services and local government 

 Housing Affordable housing 
 Care homes 
 Home ownership 
 Homelessness 
 Housing Regeneration 
 Housing market 
 Housing providers 
 Private rented sector 
 Retirement Housing 
 Social Housing 
 Tenant Participation 
 Tenants 

 Income and benefits Costs 
 Debt 
 Food 
 Living Standards 
 Living Wage 
 Minimum Income Standards 
 Pay 
 Pensions 
 Tax 
 Unemployment 
 Universal Credit 

 People Active Citizenship 
 Ageing Population 
 Carers 
 Child Poverty 
 Children 
 Dementia 
 Disabled People 
 Divorce / Family Breakdown 
 Education 
 Ethnicity 
 Gender 
 Independent Living 
 Lone Parents 
 Mental Health 
 Migration 
 Older People 
 Social Care 
 Young People 

 Society Ageing Society 
 Austerity 
 Civic Participation 
 Crime / Anti-social Behaviour 
 Faith and Religion 
 Government 
 Immigration 
 Local Government 
 Localism 
 Slavery 
 Social Exclusion 
 Voluntary Sector 

 Work Employment 
 Equality 
 Forced Labour 
 Labour Markets 
 Low Pay 
 Retirement 
 Skills 
 Working with Employers 

 About us 
 Our work 
 Media centre 

 You are here Home     Reports     Public sector innovation and local leadership in the UK and The Netherlands 

   Public sector innovation and local leadership in the UK and The Netherlands   Robin Hambleton and Joanna Howard   28th Jun 2012 
 Related topics: 

 Society   

 Local government   

 Localism   

 Services and local government   

   Can civic leaders tackle social exclusion and austerity cuts by engaging in radical public service innovation? 
 Public sector innovation and local leadership in the UK and The Netherlands   Downloads   Related content   
 The current economic and political climate has little room for alternatives to cuts and austerity. However, radical initiatives which stress the importance of place-based leadership in advancing social inclusion are in development in the UK and elsewhere.   Exploring the role of place-based leadership in Bristol and Swindon in the UK, and Enschede in The Netherlands, the study found:   that the narrow vision of cutback management lacks wisdom.   that leaders with an emotional commitment to social inclusion enable innovation to flourish and can encourage others.   That 'Innovation Stories' recording practical experiences on the ground offer inspiration to other areas.     This study offers an alternative to the dominant 'do more with less' philosophy dominating public policy making in many EU countries. As such, it makes a valuable contribution to the debate about the nature of post-austerity society.   
 Downloads   Downloads 

 Findings Public sector innovation and local leadership in the UK and the Netherlands  (206.92 KB) 

 Full Report Public sector innovation and local leadership in the UK and the Netherlands  (1.14 MB) 

   Related content   Related content   

   Community assets: emerging learning, challenges and questions   26th Sep 2011   What have we learned from JRF's series of seminars on community assets? 
 Community assets: emerging learning, challenges and questions 

   Serving deprived communities in a recession   26th Jan 2012   What are the early impacts of English local authority budget cuts since 2010? 
 Serving deprived communities in a recession 

   Data use in voluntary and community organisations   6th Feb 2009   This study assesses the need and demand within voluntary and community organisations (VCOs) for relevant, analysed and well-presented data to support their work tackling poverty. 
 Data use in voluntary and community organisations 

   Share 

 JRF Twitter 

 JRF Email 

 Keep up to date   Be the first to hear about our work. 
 If you are a researcher, employer, policy maker, academic, campaigner or just an interested supporter you can sign up here to receive regular updates from the Joseph Rowntree Foundation. 
 Subscribe 

 A unique organisation   The Joseph Rowntree Foundation is an independent organisation working to inspire social change through research, policy and practice. 
 Uniquely, we also run a housing association and care provider, the Joseph Rowntree Housing Trust. 
 About us 

 Our Work 
 Our work 
 Events 
 Reports 
 Blog 
 News 
 Funding 
 Social investment 
 Data 
 Video 

 About us 
 About us 
 Our heritage 
 Joseph Rowntree Housing Trust 
 Jobs 
 Internships 
 Our experts 
 Our people 
 Frequently asked questions 

 Legal 
 Legal 
 Annual reports and accounts 
 Terms and conditions 
 Privacy 
 Cookies 
 Accessibility 
 Acceptable use policy 

 Contact us 
 Contact us   The Homestead 40 Water End York   YO30 6WP   Tel: 01904 629 241 Email:   [email protected] 

 Social menu 
 Twitter 
 LinkedIn 
 YouTube 
 Facebook 
 Vimeo 
 RSS 
 Flickr 

 Joseph Rowntree Foundation is a registered charity no 210169 (England and Wales).   

Public sector innovation and local leadership in the UK and The Netherlands | JRF

 Jump to main content 

 JRF The Joseph Rowntree Foundation 

 Secondary Menu 
 About us 
 Our work 
 Media centre 

 Search 

 Site menu 
 Cities, towns and neighbourhoods Cities 
 Climate Change 
 Devolution 
 Economic development and local growth 
 Flooding 
 Inclusive Growth 
 Loneliness 
 Neighbourhoods 
 Planning 
 Regeneration 
 Services and local government 

 Housing Affordable housing 
 Care homes 
 Home ownership 
 Homelessness 
 Housing Regeneration 
 Housing market 
 Housing providers 
 Private rented sector 
 Retirement Housing 
 Social Housing 
 Tenant Participation 
 Tenants 

 Income and benefits Costs 
 Debt 
 Food 
 Living Standards 
 Living Wage 
 Minimum Income Standards 
 Pay 
 Pensions 
 Tax 
 Unemployment 
 Universal Credit 

 People Active Citizenship 
 Ageing Population 
 Carers 
 Child Poverty 
 Children 
 Dementia 
 Disabled People 
 Divorce / Family Breakdown 
 Education 
 Ethnicity 
 Gender 
 Independent Living 
 Lone Parents 
 Mental Health 
 Migration 
 Older People 
 Social Care 
 Young People 

 Society Ageing Society 
 Austerity 
 Civic Participation 
 Crime / Anti-social Behaviour 
 Faith and Religion 
 Government 
 Immigration 
 Local Government 
 Localism 
 Slavery 
 Social Exclusion 
 Voluntary Sector 

 Work Employment 
 Equality 
 Forced Labour 
 Labour Markets 
 Low Pay 
 Retirement 
 Skills 
 Working with Employers 

 Cities, towns and neighbourhoods Cities 
 Climate Change 
 Devolution 
 Economic development and local growth 
 Flooding 
 Inclusive Growth 
 Loneliness 
 Neighbourhoods 
 Planning 
 Regeneration 
 Services and local government 

 Housing Affordable housing 
 Care homes 
 Home ownership 
 Homelessness 
 Housing Regeneration 
 Housing market 
 Housing providers 
 Private rented sector 
 Retirement Housing 
 Social Housing 
 Tenant Participation 
 Tenants 

 Income and benefits Costs 
 Debt 
 Food 
 Living Standards 
 Living Wage 
 Minimum Income Standards 
 Pay 
 Pensions 
 Tax 
 Unemployment 
 Universal Credit 

 People Active Citizenship 
 Ageing Population 
 Carers 
 Child Poverty 
 Children 
 Dementia 
 Disabled People 
 Divorce / Family Breakdown 
 Education 
 Ethnicity 
 Gender 
 Independent Living 
 Lone Parents 
 Mental Health 
 Migration 
 Older People 
 Social Care 
 Young People 

 Society Ageing Society 
 Austerity 
 Civic Participation 
 Crime / Anti-social Behaviour 
 Faith and Religion 
 Government 
 Immigration 
 Local Government 
 Localism 
 Slavery 
 Social Exclusion 
 Voluntary Sector 

 Work Employment 
 Equality 
 Forced Labour 
 Labour Markets 
 Low Pay 
 Retirement 
 Skills 
 Working with Employers 

 About us 
 Our work 
 Media centre 

 You are here Home     Reports     Public sector innovation and local leadership in the UK and The Netherlands 

   Public sector innovation and local leadership in the UK and The Netherlands   Robin Hambleton and Joanna Howard   28th Jun 2012 
 Related topics: 

 Society   

 Local government   

 Localism   

 Services and local government   

   Can civic leaders tackle social exclusion and austerity cuts by engaging in radical public service innovation? 
 Public sector innovation and local leadership in the UK and The Netherlands   Downloads   Related content   
 The current economic and political climate has little room for alternatives to cuts and austerity. However, radical initiatives which stress the importance of place-based leadership in advancing social inclusion are in development in the UK and elsewhere.   Exploring the role of place-based leadership in Bristol and Swindon in the UK, and Enschede in The Netherlands, the study found:   that the narrow vision of cutback management lacks wisdom.   that leaders with an emotional commitment to social inclusion enable innovation to flourish and can encourage others.   That 'Innovation Stories' recording practical experiences on the ground offer inspiration to other areas.     This study offers an alternative to the dominant 'do more with less' philosophy dominating public policy making in many EU countries. As such, it makes a valuable contribution to the debate about the nature of post-austerity society.   
 Downloads   Downloads 

 Findings Public sector innovation and local leadership in the UK and the Netherlands  (206.92 KB) 

 Full Report Public sector innovation and local leadership in the UK and the Netherlands  (1.14 MB) 

   Related content   Related content   

   Community assets: emerging learning, challenges and questions   26th Sep 2011   What have we learned from JRF's series of seminars on community assets? 
 Community assets: emerging learning, challenges and questions 

   Serving deprived communities in a recession   26th Jan 2012   What are the early impacts of English local authority budget cuts since 2010? 
 Serving deprived communities in a recession 

   Data use in voluntary and community organisations   6th Feb 2009   This study assesses the need and demand within voluntary and community organisations (VCOs) for relevant, analysed and well-presented data to support their work tackling poverty. 
 Data use in voluntary and community organisations 

   Share 

 JRF Twitter 

 JRF Email 

 Keep up to date   Be the first to hear about our work. 
 If you are a researcher, employer, policy maker, academic, campaigner or just an interested supporter you can sign up here to receive regular updates from the Joseph Rowntree Foundation. 
 Subscribe 

 A unique organisation   The Joseph Rowntree Foundation is an independent organisation working to inspire social change through research, policy and practice. 
 Uniquely, we also run a housing association and care provider, the Joseph Rowntree Housing Trust. 
 About us 

 Our Work 
 Our work 
 Events 
 Reports 
 Blog 
 News 
 Funding 
 Social investment 
 Data 
 Video 

 About us 
 About us 
 Our heritage 
 Joseph Rowntree Housing Trust 
 Jobs 
 Internships 
 Our experts 
 Our people 
 Frequently asked questions 

 Legal 
 Legal 
 Annual reports and accounts 
 Terms and conditions 
 Privacy 
 Cookies 
 Accessibility 
 Acceptable use policy 

 Contact us 
 Contact us   The Homestead 40 Water End York   YO30 6WP   Tel: 01904 629 241 Email:   [email protected] 

 Social menu 
 Twitter 
 LinkedIn 
 YouTube 
 Facebook 
 Vimeo 
 RSS 
 Flickr 

 Joseph Rowntree Foundation is a registered charity no 210169 (England and Wales).   

There is strong evidence that how long and well people will live is influenced by the social conditions into which people are born, grow and live over the whole of their life course. Those people who experience relative individual and household deprivation and experience the numerous facets of social exclusion often live in neighbourhoods that are also relatively deprived. There have been countless urban regeneration initiatives in the Netherlands and other EU member states but the problems of relative deprivation, social exclusion and resulting health inequalities continues to mean that some people lead shorter and less healthy lives than their more affluent fellow citizens. In affluent states with highly developed systems of welfare provision there are still persistent problems of social exclusion that prevent equitable active ageing to occur despite significant levels of social expenditure.
An innovative approach to improving how well people live over the life course that went beyond the traditional approaches of improving the physical infrastructure or seeking to create a more resilient social environment is the Neighbourhood Coach (or Social GP) model that has been operating in Velve-Lindenhof neighbourhood of the Dutch city of Enschede since 2009. Velve-Lindenhof was identified as one of the 40 most deprived areas in the Netherlands in 2008 and as part of a national initiative to address social exclusion and urban regeneration these areas were assigned extra resources to develop innovative solutions to seemingly intractable problems of urban deprivation and the effects that this had on the health and well-being of people in these local areas. The solution adopted for approximately 600 residents of all ages in Velve-Lindenhof built on the experiences of a home visits programme involving teams of social workers, police officers and employment officers to try to detect emerging problems by engaging with local residents and an agreement by local social welfare and public service organisations to provide a more integrated approach to the people and place of Velve-Lindenhof. This combination of factors underpinned the development of Neighbourhood Coaches who aim to supersede the array of professionals that people often have to deal with (or not if they slip through the safety net) and who can be highly paternalistic and may even create dependency.
The ethos of Neighbourhood Coaches is one professional, one plan of action, one system and was based on working collaboratively with people with the aim of empowering them to be independent and take charge of their lives. The 25 organisations that provided a range of public services  housing, health, social welfare, employment and so on  agreed to empower the Neighbourhood Coaches to take decisions while they maintained formal powers and responsibilities. A more open and participative local political context assisted in making this intervention more acceptable and the four coaches came from different local service organisations rather than being employees of the municipality. The coaches could call on specialist professionals if they were needed but they developed plans with individual and small area strategies for localities while calling on resources from a multiplicity of agencies. The programme was process evaluated by researchers who found that most participants viewed the intervention in a favourable fashion and showed indications of promise. This is not to suggest that Neighbourhood Coaches can have a significant impact in an area of deprivation on their own but they can play an important role in personalising wider urban regeneration programmes so that they make a difference to peoples lives by working with the people living in the area.
In relation to active ageing, the Neighbourhood Coaches programme provides opportunities for independent living and physical safety in a locality which was affected by crime and fear of crime. It also provided improved access to health and other services that should contribute to people in the neighbourhood being empowered to improve their own situation and that of the locality. This is a pilot project that covered just a small area and enjoyed the benefits of additional resources to make a difference to the quality of life of people and the social quality of the area by developing ideas and solutions together. Whether it has sustained effects and can be used on a larger scale, as it has subsequently across Enschede although with fewer resources, has yet to be determined but it does provide a potentially promising model.
WHOLE PROJECT MARK