beyondblue - Home 

 skip to content skip to secondary navigation 

 Search 

     beyondblue  Support Service 
Support. Advice. Action   1300 22 4636         Chat online       Email us         

 my profile 

 my account 

 blueVoices 

 log out 

 Join forum 

 Register 
 Login 
 or 
 Login   show 

 Login 

 You are already logged in as . 
 Logout 

 Login 

 Email / Username 

 Click to add 

 (?) 

 Password 

 Click to add 

 (?) 

 Remember me 

 Login 

 Need help signing in? 

     Home       Toggle visibility of main navigation     

 Home Get support 

 Get immediate support Who can assist 

 Getting support   how much does it cost? 

 Find a professional National help lines and websites Online forums 

 Sign up Login Community rules 

 Treatment options NewAccess - Coaching you through tough times 

 Where are your Access coaches located? Background to NewAccess NewAccess testimonials NewAccess-Frequently Asked Questions NewAccess Information for PHNs and Service Providers NewAccess in PHNs: Critical success features 

 Have the conversation 

 What to say and why Talk about it Know your options Talking to someone you are worried about Talking to a young person  Chat Laps 

 BeyondNow - Suicide safety planning 

 Create your BeyondNow safety plan online Getting started Information for health professionals Information for family and friends 

 beyondblue  Connect  

 beyondblue  Connect toolkit beyondblue  Connect FAQs 

 Staying well 

 Recovering from a mental health condition Journey to wellness Keeping active Sleeping well Eating well Reducing stress Relaxation exercises 

 Publications to download or order Get started now 

 The facts 

 What is mental health? Anxiety and depression checklist (K10) Depression 

 What causes depression? Signs and symptoms Types of depression Treatments for depression Recovery and staying well Who can assist Other sources of support 

 Anxiety 

 What causes anxiety?  Signs and symptoms Types of  anxiety Treatments for anxiety Recovery and staying well Who can assist Other sources of support 

 Suicide prevention 

 Get support now Worried about suicide Recovery and support strategies Myths and facts Stories of recovery and hope Understanding suicide and grief Helpful contacts and websites Personal stories about suicide 

 Supporting someone 

 Supporting someone with anxiety or depression Supporting someone to see a health professional Looking after yourself Parents and guardians 

 Self-harm and self-injury Pregnancy and early parenthood 

 The baby blues Emotional health Mental health conditions Treatment options Helping yourself and others Becoming a parent: what to expect Maternal mental health and wellbeing Dadvice: for new and expectant dads Just speak up national awareness campaign 

 Grief and loss Drugs, alcohol and mental health 

 Who does it affect? 

 Young people 

 Helpful contacts and websites 

 Men 

 Know the signs and symptoms Looking out for yourself Looking out for your mates Taking action 

 Women 

 Factors affecting women 

 Older people 

 Risk factors for older people Signs and symptoms of anxiety and depression in older people Have the conversation with older people Connections matter Life starts at sixty OBE campaign 

 Pregnancy and early parenthood 

 Becoming a parent: what to expect Maternal mental health and wellbeing Dadvice: for new and expectant dads Just speak up national awareness campaign 

 Aboriginal and Torres Strait Islander people 

 Protective and risk factors Strength and wellbeing Helpful contacts and websites Support after a suicide attempt Close The Gap 

 Lesbian, gay, bi, trans, intersex (LGBTI), bodily, gender and sexuality diverse people 

 Take action before the blue takes over Factors affecting LGBTI people The impact of discrimination Stop. Think. Respect. Resilience in the face of change: stories of transmen Families like mine Wingmen   for gay guys, by gay guys  Report discrimination Helpful contacts and websites 

 Multicultural people 

 Having the conversation with multicultural people Translated mental health resources 

 The Invisible Discriminator 

 Educate yourself about racism Create change at work Create change at your school Respond to racism 

 Get involved 

 Make a donation 

 One time donation Monthly giving In memoriam In celebration Gift in your will Workplace giving 

 Fundraise for us 

 General fundraising application beyondblue  Bash Join Team  beyondblue 

 Volunteer with us 

 Go to Volunteers Hub 

 Business and corporate support Join our online community blueVoices 

 blueVoices registration 

 Our Speakers Bureau Events Sign up to our eNewsletter 

 Our stories 

 Ambassadors  Personal stories News 

 Healthy places 

 At home - everything you need for a healthy family Early childhood and primary schools Secondary schools and tertiary 

 MindMatters SenseAbility Secondary schools program thedesk 

 Helpful contacts and websites for educators In the workplace - Heads Up Aged care 

 Media 

 Media releases Media contacts Mental health reporting guidelines 

 Make a donation 

 Donate now Monthly giving Workplace giving In memoriam 

 beyondblue  Support Service 
 Support. Advice. Action 

 Give us a call 
 Chat online 
 Email us 
 Visit our forums 

 Already a forum member? 
 Register 
 Login 

 Support  beyondblue 
 Please help us improve the lives of people affected by anxiety, depression and suicide 

 Make a donation 

 Beyondblue, Depression and Anxiety     

 Read transcript 

 Read transcript 

 Read transcript 

 Read transcript 

 Read transcript 

 Read transcript 

 3 million Australians are living with anxiety or depression 

 beyondblue provides information and support to help everyone in Australia achieve their best possible mental health, whatever their age and wherever they live. 

 Learn about anxiety 

 Learn about depression 

 Learn about suicide 

 In focus 
 Read our Board's 2017 statement on marriage equality 

 If you are a member of the LGBTI community, and you are experiencing distress, our Helpful contacts and websites page has a list of respectful and inclusive organisations who can support you 

 Transcript of ABC Lateline interview with Julia Gillard 

 Latest news 

 Latest news 

 Steel Blue launches new blue boots in support of  beyondblue 
 18 September 2017 

 $10 of every pair of boot boots sold will support  beyondblue 's 24/7 Support Service 

 Transcript of ABC Lateline interview with Julia Gillard 
 05 July 2017 

 Transcript of ABC Lateline interviewing  beyondblue  Chair, Julia Gillard.  

 Find beyondblue on: 

 Go to news 
 Sign up to our newsletters 

 Information for 

                        Young people 

                        Women 

                        Men 

                        Older people 

                        Multicultural people 

                        Aboriginal and Torres Strait Islander people 

                        Lesbian, gay, bi, trans, intersex (LGBTI) people 

                        Pregnancy and early parenthood 

 Information for me 

 Resources for 

              	Schools and Universities

              	Workplaces

              	Aged care

              	Health professionals

              	Research

 Get involved 

 beyondblue  is all about people - people just like you. Here are some ways you can support our work. 

                        Help us improve the lives of individuals, families and communities affected by anxiety, depression and suicide. 

 Donate now 

                        Become a Fundraising Legend   host a  beyondblue  Bash or other fundraising event to support our work. 

 Fundraise for us 

                        Join our reference group and online community for people impacted by anxiety, depression or suicide. 

 Join blueVoices 

                        Join  Team  beyondblue  in a running, cycling or swimming event  and help raise vital funds to support our work. 

 Join Team  beyondblue 

                        Register to become a  beyondblue  Volunteer and support our work at events across the country.  

 Become a volunteer 

                        Share your personal experience and provide hope and encouragement to others in our supportive online forums.  

 Join our community 

                        There are many ways organisations can get involved with  beyondblue  and help support the work we do. 

 Partner with us 

 Join our discussion 

 Read thread 

 Join discussion 

 1. For a full list of references for the statistics on this page, and any others across the website, please visit the  references page and search through the relevant category.   This website has been established to provide information about anxiety, depression and suicide to the Australian community. The website is not intended to be a substitute for professional medical advice, diagnosis or treatment. You should seek the advice of an appropriately qualified healthcare professional before making decisions about your own circumstances. You should not disregard professional medical advice, or delay seeking it, because of any information contained on this website.       

 Your session is about to expire. You have 2 minutes left before being logged out. Please select 'ok' to extend your session and prevent losing any content you are working on from being lost. 

     Talk it through with us, we'll point you in the right direction             Call 1300 22 4636   24 hours  a day  / 7 days a week                 Chat online   3pm - 12am  (AEST)  / 7 days a week                 Email us   Get a response in 24 hours                 Online forums   24 hours / 7 days a week             
   Find beyondblue on:                             Make a donation 

 Get support 

 Get immediate support 

 Find a professional 

 Get started now 

 The facts 

 Depression 

 Anxiety 

 Pregnancy and early parenthood 

 Treatment options 

 Who can assist 

 Recovery and staying well 

 Get support 

 Get immediate support 

 Who can assist 

 Find a professional 

 National helplines and websites 

 Online forums 

 Treatment options 

 Have the conversation 

 BeyondNow - Suicide safety planning 

 Recovery and staying well 

 Order information resources 

 The facts 

 What is mental health? 

 Anxiety and depression checklist 

 Depression 

 Anxiety 

 Suicide 

 Self-harm and self-injury 

 Grief and loss 

 Who does it affect? 

 Young people 

 Men 

 Women 

 Older people 

 Pregnancy and early parenthood 

 Aboriginal and Torres Strait Islander people 

 Lesbian, gay, bi, trans, intersex (LGBTI) people 

 Multicultural people 

 Supporting someone 

 Supporting someone with depression or anxiety 

 Looking after yourself 

 Parents and guardians 

 Discrimination 

 Insurance  

 About  beyondblue 

 Who we are and what we do 

 Our governance structure 

 Our Board of directors 

 Our funding 

 Annual reports 

 Independent evaluations 

 Policy and advocacy 

 About our work 

 Procurement 

 Careers 

 For... 

 Workplaces 

 Early childhood and primary schools 

 Secondary schools and tertiary  

 Health professionals 

 Researchers 

 Aged care 

 Media 

 Copyright    2016  Beyond Blue Ltd       Contact us   Site map   Terms of use   Privacy policy     

	beyondblue - Home 

 skip to content skip to secondary navigation 

 Search 

     beyondblue  Support Service 
Support. Advice. Action   1300 22 4636         Chat online       Email us         

 my profile 

 my account 

 blueVoices 

 log out 

 Join forum 

 Register 
 Login 
 or 
 Login   show 

 Login 

 You are already logged in as . 
 Logout 

 Login 

 Email / Username 

 Click to add 

 (?) 

 Password 

 Click to add 

 (?) 

 Remember me 

 Login 

 Need help signing in? 

     Home       Toggle visibility of main navigation     

 Home Get support 

 Get immediate support Who can assist 

 Getting support   how much does it cost? 

 Find a professional National help lines and websites Online forums 

 Sign up Login Community rules 

 Treatment options NewAccess - Coaching you through tough times 

 Where are your Access coaches located? Background to NewAccess NewAccess testimonials NewAccess-Frequently Asked Questions NewAccess Information for PHNs and Service Providers NewAccess in PHNs: Critical success features 

 Have the conversation 

 What to say and why Talk about it Know your options Talking to someone you are worried about Talking to a young person  Chat Laps 

 BeyondNow - Suicide safety planning 

 Create your BeyondNow safety plan online Getting started Information for health professionals Information for family and friends 

 beyondblue  Connect  

 beyondblue  Connect toolkit beyondblue  Connect FAQs 

 Staying well 

 Recovering from a mental health condition Journey to wellness Keeping active Sleeping well Eating well Reducing stress Relaxation exercises 

 Publications to download or order Get started now 

 The facts 

 What is mental health? Anxiety and depression checklist (K10) Depression 

 What causes depression? Signs and symptoms Types of depression Treatments for depression Recovery and staying well Who can assist Other sources of support 

 Anxiety 

 What causes anxiety?  Signs and symptoms Types of  anxiety Treatments for anxiety Recovery and staying well Who can assist Other sources of support 

 Suicide prevention 

 Get support now Worried about suicide Recovery and support strategies Myths and facts Stories of recovery and hope Understanding suicide and grief Helpful contacts and websites Personal stories about suicide 

 Supporting someone 

 Supporting someone with anxiety or depression Supporting someone to see a health professional Looking after yourself Parents and guardians 

 Self-harm and self-injury Pregnancy and early parenthood 

 The baby blues Emotional health Mental health conditions Treatment options Helping yourself and others Becoming a parent: what to expect Maternal mental health and wellbeing Dadvice: for new and expectant dads Just speak up national awareness campaign 

 Grief and loss Drugs, alcohol and mental health 

 Who does it affect? 

 Young people 

 Helpful contacts and websites 

 Men 

 Know the signs and symptoms Looking out for yourself Looking out for your mates Taking action 

 Women 

 Factors affecting women 

 Older people 

 Risk factors for older people Signs and symptoms of anxiety and depression in older people Have the conversation with older people Connections matter Life starts at sixty OBE campaign 

 Pregnancy and early parenthood 

 Becoming a parent: what to expect Maternal mental health and wellbeing Dadvice: for new and expectant dads Just speak up national awareness campaign 

 Aboriginal and Torres Strait Islander people 

 Protective and risk factors Strength and wellbeing Helpful contacts and websites Support after a suicide attempt Close The Gap 

 Lesbian, gay, bi, trans, intersex (LGBTI), bodily, gender and sexuality diverse people 

 Take action before the blue takes over Factors affecting LGBTI people The impact of discrimination Stop. Think. Respect. Resilience in the face of change: stories of transmen Families like mine Wingmen   for gay guys, by gay guys  Report discrimination Helpful contacts and websites 

 Multicultural people 

 Having the conversation with multicultural people Translated mental health resources 

 The Invisible Discriminator 

 Educate yourself about racism Create change at work Create change at your school Respond to racism 

 Get involved 

 Make a donation 

 One time donation Monthly giving In memoriam In celebration Gift in your will Workplace giving 

 Fundraise for us 

 General fundraising application beyondblue  Bash Join Team  beyondblue 

 Volunteer with us 

 Go to Volunteers Hub 

 Business and corporate support Join our online community blueVoices 

 blueVoices registration 

 Our Speakers Bureau Events Sign up to our eNewsletter 

 Our stories 

 Ambassadors  Personal stories News 

 Healthy places 

 At home - everything you need for a healthy family Early childhood and primary schools Secondary schools and tertiary 

 MindMatters SenseAbility Secondary schools program thedesk 

 Helpful contacts and websites for educators In the workplace - Heads Up Aged care 

 Media 

 Media releases Media contacts Mental health reporting guidelines 

 Make a donation 

 Donate now Monthly giving Workplace giving In memoriam 

 beyondblue  Support Service 
 Support. Advice. Action 

 Give us a call 
 Chat online 
 Email us 
 Visit our forums 

 Already a forum member? 
 Register 
 Login 

 Support  beyondblue 
 Please help us improve the lives of people affected by anxiety, depression and suicide 

 Make a donation 

 Beyondblue, Depression and Anxiety     

 Read transcript 

 Read transcript 

 Read transcript 

 Read transcript 

 Read transcript 

 Read transcript 

 3 million Australians are living with anxiety or depression 

 beyondblue provides information and support to help everyone in Australia achieve their best possible mental health, whatever their age and wherever they live. 

 Learn about anxiety 

 Learn about depression 

 Learn about suicide 

 In focus 
 Read our Board's 2017 statement on marriage equality 

 If you are a member of the LGBTI community, and you are experiencing distress, our Helpful contacts and websites page has a list of respectful and inclusive organisations who can support you 

 Transcript of ABC Lateline interview with Julia Gillard 

 Latest news 

 Latest news 

 Steel Blue launches new blue boots in support of  beyondblue 
 18 September 2017 

 $10 of every pair of boot boots sold will support  beyondblue 's 24/7 Support Service 

 Transcript of ABC Lateline interview with Julia Gillard 
 05 July 2017 

 Transcript of ABC Lateline interviewing  beyondblue  Chair, Julia Gillard.  

 Find beyondblue on: 

 Go to news 
 Sign up to our newsletters 

 Information for 

                        Young people 

                        Women 

                        Men 

                        Older people 

                        Multicultural people 

                        Aboriginal and Torres Strait Islander people 

                        Lesbian, gay, bi, trans, intersex (LGBTI) people 

                        Pregnancy and early parenthood 

 Information for me 

 Resources for 

              	Schools and Universities

              	Workplaces

              	Aged care

              	Health professionals

              	Research

 Get involved 

 beyondblue  is all about people - people just like you. Here are some ways you can support our work. 

                        Help us improve the lives of individuals, families and communities affected by anxiety, depression and suicide. 

 Donate now 

                        Become a Fundraising Legend   host a  beyondblue  Bash or other fundraising event to support our work. 

 Fundraise for us 

                        Join our reference group and online community for people impacted by anxiety, depression or suicide. 

 Join blueVoices 

                        Join  Team  beyondblue  in a running, cycling or swimming event  and help raise vital funds to support our work. 

 Join Team  beyondblue 

                        Register to become a  beyondblue  Volunteer and support our work at events across the country.  

 Become a volunteer 

                        Share your personal experience and provide hope and encouragement to others in our supportive online forums.  

 Join our community 

                        There are many ways organisations can get involved with  beyondblue  and help support the work we do. 

 Partner with us 

 Join our discussion 

 Read thread 

 Join discussion 

 1. For a full list of references for the statistics on this page, and any others across the website, please visit the  references page and search through the relevant category.   This website has been established to provide information about anxiety, depression and suicide to the Australian community. The website is not intended to be a substitute for professional medical advice, diagnosis or treatment. You should seek the advice of an appropriately qualified healthcare professional before making decisions about your own circumstances. You should not disregard professional medical advice, or delay seeking it, because of any information contained on this website.       

 Your session is about to expire. You have 2 minutes left before being logged out. Please select 'ok' to extend your session and prevent losing any content you are working on from being lost. 

     Talk it through with us, we'll point you in the right direction             Call 1300 22 4636   24 hours  a day  / 7 days a week                 Chat online   3pm - 12am  (AEST)  / 7 days a week                 Email us   Get a response in 24 hours                 Online forums   24 hours / 7 days a week             
   Find beyondblue on:                             Make a donation 

 Get support 

 Get immediate support 

 Find a professional 

 Get started now 

 The facts 

 Depression 

 Anxiety 

 Pregnancy and early parenthood 

 Treatment options 

 Who can assist 

 Recovery and staying well 

 Get support 

 Get immediate support 

 Who can assist 

 Find a professional 

 National helplines and websites 

 Online forums 

 Treatment options 

 Have the conversation 

 BeyondNow - Suicide safety planning 

 Recovery and staying well 

 Order information resources 

 The facts 

 What is mental health? 

 Anxiety and depression checklist 

 Depression 

 Anxiety 

 Suicide 

 Self-harm and self-injury 

 Grief and loss 

 Who does it affect? 

 Young people 

 Men 

 Women 

 Older people 

 Pregnancy and early parenthood 

 Aboriginal and Torres Strait Islander people 

 Lesbian, gay, bi, trans, intersex (LGBTI) people 

 Multicultural people 

 Supporting someone 

 Supporting someone with depression or anxiety 

 Looking after yourself 

 Parents and guardians 

 Discrimination 

 Insurance  

 About  beyondblue 

 Who we are and what we do 

 Our governance structure 

 Our Board of directors 

 Our funding 

 Annual reports 

 Independent evaluations 

 Policy and advocacy 

 About our work 

 Procurement 

 Careers 

 For... 

 Workplaces 

 Early childhood and primary schools 

 Secondary schools and tertiary  

 Health professionals 

 Researchers 

 Aged care 

 Media 

 Copyright    2016  Beyond Blue Ltd       Contact us   Site map   Terms of use   Privacy policy     

 beyondblue is an independent, not-for-profit health promotion organisation that is a partnership initiative of the Commonwealth, State and Territory governments. Founded by former Victorian Prime Minister Jeff Kennett, who is still chairman, it has Board of Governors who act as trustees in accordance with charity laws.
WHOLE PROJECT MARK