Casserole Club

 Sign in 
 Suggest a Diner 
 Become a Cook 
 About us 
 Visit Casserole Australia 

 Doing something great with an extra plate 

 Featured by 

 Join Casserole Club 

 Become a Cook 
 Suggest a Diner 

Casserole Club volunteers share extra portions of home-cooked food with people in their
area who aren t always able to cook for themselves. They share once a week, once a month,
or whenever works best for them.

 Be part of the community! 

 Share a plate 
 Cook an extra portion and deliver it to a neighbour who needs it. 
 Read more 

 Get a plate 
 Share the name of someone who could do with a home-cooked meal. 
 Read more 

 Spread the word 
 Let people know about who we are and what we do. 
 Spread the word 

 The informality of Casserole Club makes it work. Pam lives about 5 minutes from me.
She s very chatty and we have a laugh. She likes my pork, apple and apricot casserole. 

 Maggie 

 Join the community on Facebook 

 Making a difference to real people 

 70% 
 of those receiving meals count their volunteer Cooks as friends. 

 80% 
 of those receiving meals wouldn t have as much social contact without Casserole Club. 

 90% 
 of our volunteer cooks would recommend Casserole Club to a friend. 

 Sign up as a Cook 

 Are we are in your area? Search a postcode. 

 Suggest a Diner 

 Spread the word 

Facebook

Tweet

Email to a friend

 More about us 
 Becoming a Cook 
 Becoming a Diner 
 Blog 

 FAQ 
 hello@casseroleclub.com 
 Help 
 Terms 

 For organisations 
 Casserole Club is a service created by FutureGov and provided with the support of local authorities and third-sector organisations. 

A product from

 Futuregov 

Casserole Club

 Sign in 
 Suggest a Diner 
 Become a Cook 
 About us 
 Visit Casserole Australia 

 Terms   Conditions 

Casserole Club is a project of FutureGov Ltd. Where  Casserole Club ,  Casserole ,  us ,  the project  or  the club  is referred to below, we are referring to FutureGov Ltd, specifically the Casserole Club project. The terms below cover our relationship with individuals who sign-up on this website - referred to below as  Cooks  and  you .

If you are looking for answers to specific questions, you may find our FAQ page more helpful. You can also contact us at  hello@casseroleclub.com .

Casserole Club is a free food-sharing project. We connect those who love cooking and want to meet more people in their area (Cooks), to others who would appreciate having a home cooked meal delivered by a friendly neighbour (Diners).

To take part in Casserole Club, Cooks are required to:

Complete and pass a Disclosure and Barring Service (DBS) criminal records check (previously known as a CRB check). This involves your consent to share your personal information with a third party security partner.

Meet a member of the Casserole Club team to present identity documents as part of the DBS check process, and to discuss their interest in joining Casserole Club and other relevant information.

Watch the provided food hygiene presentation and correctly answer all questions of a food hygiene quiz on this website.

Complete a personal profile on this website, and provide any further relevant information to the Casserole Club team if and when asked.

Agree to these terms and conditions.

At any point during a Cook s relationship with Casserole Club, we can, at our discretion:

Terminate a Cook s relationship with us, cancel all scheduled meal shares, and request that contact with all other members of the Casserole Club project (Cooks and Diners) ends immediately.

Ask you not to contact a specific Diner again.

Cancel a scheduled mealshare.

Pass any relevant information to any law enforcement organisation with relevant jurisdiction.

Unless necessary, we may not notify the Cook of any of the undertaking of any of the above actions. Cooks must comply with any of the requests above as soon as reasonably possible.

During your time as a Casserole Cook, you are required to:

Notify us immediately if any of your contact details change.

Read and abide by the  Cook s Handbook  (and any updates) which is provided by us before you cook your first Casserole meal.

Treat other members of the Casserole Club (Cooks and Diners) with respect and dignity - treat them as you would a friend.

Abide by all relevant legislation.

Notify us of any concerns you have about the wellbeing, behaviour or conduct of another Casserole Club member (Cook or Diner).

Casserole Club is not and cannot be held responsible for and is not liable for any loss, damage or harm resulting from your activity associated with, enabled by, or initiated from, through or with Casserole Club.

Note: The terms of business of online DBS providers also apply to users when undertaking a DBS check.

 Privacy 

This website belongs to FutureGov, and our full details are below. This privacy policy explains how we use the personal information we collect via this site, as well as what cookies we use.

1. What kind of information do we collect?

If you register or sign up for our service, or to receive more information from us, we may ask you for personal details such as your name, address, date of birth or other related information.
We may record your activity and preferences when visiting the sites (see "Cookies", below). If you post content or communicate via the site, we may also store and monitor that information.

2. What do we do with the information we collect?

We will use your personal information to operate this site, to contact you, to send you newsletters, publications and other information about Casserole Club and FutureGov, and to process applications you make via the site. We may also use your information to carry out analysis and research to improve our service, publications, events and activities, to prevent and detect fraud and abuse, and to protect other users.

Please make sure that any personal details you provide are accurate and up to date, and let us know about any changes at  hello@casseroleclub.com . Please get consent first before giving us anyone else s information, especially for any Diners you may suggest.

3. How to unsubscribe

If you no longer want to receive communications from us, please contact us at  hello@casseroleclub.com .

4. Who else has access to your information?

We may share your information with our partners and with companies who help us to operate this site and to organise events and other activities.

Comments, blogs and other information which you post on the site will be displayed publicly and to other users. Please be careful when disclosing personal information which may identify you or anyone else. We are not responsible for the protection or security of information which you post in public areas.

We may disclose your personal information if required by law, or to protect or defend ourselves or others against illegal or harmful activities, or as part of a reorganisation or restructuring.

5. Cookies

This site contains cookies. Cookies are small text files that are placed on your computer by websites you visit. They are widely used to make websites work, or work more efficiently, as well as to provide information to site owners. Most web browsers allow some control of most cookies through browser settings. To find out more about cookies, including how to see what cookies have been set and how to manage and delete them, visit www.aboutcookies.org or www.allaboutcookies.org.

This site uses cookies that are strictly necessary to enable you to move around the site or to provide certain basic features, such as logging into secure areas.

The site also uses performance cookies which collect information about how you use the site, such as how you are referred to it and how long you stay on certain pages. This information is aggregated and therefore anonymous and is only used to improve the performance of the site. Some of these performance cookies are Google Analytics web cookies. To opt out of being tracked by Google Analytics across all websites visit http://tools.google.com/dlpage/gaoptout.

6. Security

We take steps to protect your personal information and follow procedures designed to minimise unauthorised access or disclosure of your information. However, we can t guarantee to eliminate all risk of misuse. If you have a password for an account on this site, please keep this safe and don t share it with anyone else. You are responsible for all activity on your account and must contact us immediately if you are aware of any unauthorised use of your password or other security breach.

7. Contacting us

You are legally entitled to know what personal information we hold about you and how that information is processed. If you would like to know what information we hold about you, please write to FutureGov, 50 Wharf Road, London N1 7EU, UK. We will ask you for proof of identity and may charge a  10 fee.

FutureGov is a company limited by guarantee registered in England with company number 6472420. Registered office: 3 King Street, Halstead, England, C09 3ER.

 More about us 
 Becoming a Cook 
 Becoming a Diner 
 Blog 

 FAQ 
 hello@casseroleclub.com 
 Help 
 Terms 

 For organisations 
 Casserole Club is a service created by FutureGov and provided with the support of local authorities and third-sector organisations. 

A product from

 Futuregov 

Casserole Club

 Sign in 
 Suggest a Diner 
 Become a Cook 
 About us 
 Visit Casserole Australia 

 The Casserole Club Story 

 Back in 2011 

Back in 2011
 FutureGov 
created a new kind of community project -- specially designed to connect people who like to cook with their older neighbours who aren t always able to cook for themselves.

 We started with just one person 

We started with just one person, a mobile phone and an Excel spreadsheet to see if we could bring neighbours and communities together over meal. We had a hunch that Casserole Club could be a positive and very human way to tackle some of the health and social care challenges we face today.

 Four years later 

Now, more than four years later, there are Casserole Clubs in local areas across England and Australia. More than 7,000 people have signed up to take part in the service to share thousands of tasty, home-cooked meals with their neighbours who need it most.

 It's convenient 

Casserole works because it takes something volunteers are already doing -- cooking meals at home -- and transforms it into a service that helps make local communities stronger. And since there are few requirements about how and when extra portions of food are shared, our Cooks and Diners are able to choose the meal sharing experiences that work for them.

 It makes people happy 

Through this simple act of sharing food, we ve seen new friendships and connections develop that make us smile everyday. Check out
 Maria and Olena. 
or
 Kate and Jeanette. 
But whether our Cooks and Diners have been sharing for years or just a few months, nearly everyone who takes part in Casserole is happier for having done so.

 More about us 
 Becoming a Cook 
 Becoming a Diner 
 Blog 

 FAQ 
 hello@casseroleclub.com 
 Help 
 Terms 

 For organisations 
 Casserole Club is a service created by FutureGov and provided with the support of local authorities and third-sector organisations. 

A product from

 Futuregov 

Casserole Club

 Sign in 
 Suggest a Diner 
 Become a Cook 
 About us 
 Visit Casserole Australia 

Frequently Asked Questions

 What is Casserole Club? 

Casserole is a project that connects people who like to cook and are happy to share an extra portion of a delicious home-cooked meal, with older neighbours living close by who could really benefit from a meal and a friendly chat.

Like a local, community-led take-away, Casserole Club members serve up tasty, home-cooked food to their neighbours, getting more people eating and cooking fresh meals while strengthening local neighbourhood relationships with every bite.

Cooks are required to sign up on the site and undertake a short safeguarding process before they can search and contact local Diners. The Casserole team works with local organisations to help reach Diners, we also take direct referrals including from friends and relatives. Visit our  suggest a Diner  page to tell us about someone.

 When will Casserole Club be in my area? 

The Casserole project is looking to grow quickly across the UK. At the moment we are currently active in Brighton and Hove, Calderdale, and Staffordshire - if you live in one of these areas and want to find out more please email your local club:

Brighton and Hove -
 casserole@bhfood.org.uk 

Calderdale -
 casserole@calderdale.gov.uk 

Staffordshire -
 casserole@ageukstaffs.org 

We are also live in Australia - visit  Casserole Club down under  to learn more

 Why are you not in my area yet? 

We ve learnt that to rollout the Casserole Club in a new area we need to work closely with local organisations like councils, housing associations and charities to sign up Diners and Cooks. It s also really important that we provide excellent customer service to our Cooks and Diners, and so we need access to funding to grow in new areas.

We are always keen to talk with new organisations about how we might rollout in new areas, and are working hard to access funding and roll out in new areas of the UK. We ll keep you updated as soon as we are ready to roll out in your area.

We also really keen to hear from people who might help us to make the case for a Casserole Club roll out in their area too - please get in touch at  hello@casseroleclub.com  if you can help or you want to get involved locally.

 Am I required to cook a certain amount each month? 

Nope! In fact you are only required to share one meal when you sign up (though we d obviously love you to share more than once).

We understand that people are busy, and Casserole is a flexible volunteering project. We make sure that Diners understand that they shouldn t expect to receive a meal from a Cook on a regular, routine basis. We want to make sure that Casserole works best for you, so how often and how much you cook is entirely up to you. Before you cook a meal for your Diner, we ll put you in touch by phone so that you can discuss what you might cook and when.

 What type of food do I need to cook? 

Our Diners  tastes are as diverse as our Cooks. We ask all our Diners if they have any specific dietary requirements and things they really like to eat when they sign up, so these will be on their profile which will give you some idea of what you might be cooking. However, when you get in touch to arrange a meal, we encourage you to ask what they like to eat/tell them what you like to cook and work it out between you. The idea of Casserole Club is that you are just making an extra portion rather than a meal just for your Diner, so hopefully you will find something you both like. A lot of our Cooks have said that they really enjoy the opportunity to try cooking different things.

 How should I deliver my meal? 

We don t want to dictate how you deliver your meals, so do what makes the most sense for you. We would recommend using a disposable take-away box or sturdy tupperware. As you get to know your Diner and if you are cooking regularly for them you might want to use crockery and pick it up when you next go round, however this is totally up to you. Microwave-safe containers are best if possible.

 What about food hygiene? 

Food hygiene is really important to us. Every Casserole Cook is asked to read through our food hygiene presentation and pass a quiz before serving up their first meal. Alongside this though, we want to develop a level of trust and accountability within our community. The whole ethos behind Casserole is that people are serving extra portions of their own food, so we trust that you will only be serving food you would be happy to eat yourself.

 What is a DBS check and why do I need to do it? 

A DBS check (Disclosure and Barring Service) is the new version of the Criminal Records Bureau check (or CRB). As you will be visiting the homes of older and potentially more vulnerable people, we have to take safety very seriously. All our volunteer Cooks have to complete a DBS check before they are introduced to Diners.

We work with recognised providers who can process DBS checks online. We will need to check your ID as part of the DBS check, which we will arrange with you personally at a time that suits you.

 What ID do I need for the DBS check? 

We need to see three pieces of ID, which could be one of the following combinations:

Either: Passport + Driving Licence + Letter with your address e.g. utility bill, letter from the Council, P45 or P60

Or: Passport + 2 x letters with your address e.g. utility bill, letter from the Council, P45 or P60.

 Can non-internet users use Casserole? 

As much as possible we encourage Cooks to sign up to Casserole using the website, as this makes our safeguarding process much simpler. The website also gives Cooks the ability to search for local Diners and to see who else is cooking in their area, and so is a really useful tool to help you get cooking quickly.

We understand that not everyone has access to the internet, and this shouldn t stop you from being able to take part, if you can t access this internet but do want to cook - please get in touch at  hello@casseroleclub.com .

 What if there are no Diners near to me? 

We are signing up as many Diners as fast as we can, but we are still growing so we may not have any Diners close by straight away. New Diners are joining Casserole all the time, so check back regularly and hopefully it won t be long until there is someone nearby. Also, if you are prepared to travel a little further, by car or bike, boat or bus, that would be great too.

 I know somebody who could really benefit from Casserole Club - what do I do? 

If you know of somebody living in one of our live areas (Staffordshire and Cheshire West and Chester), that could benefit as a Casserole Diner, please  suggest them here .

 What if something goes wrong? 

If something doesn t go to plan (eg. you need to cancel, your can t get in touch with your Diner etc) please contact your local Casserole Club. If it's an emergency always call 999 first.

 Do I have to go into the Diner s home? 

What happens when you go round to deliver a meal is totally up to you and your Diner. You may just deliver food and have a chat at the door, they might ask you in. Do whatever feels right for you. Some Cooks and Diners may want to get to know each other a little better first, others may well be up for a cuppa and a chat!

 What if I have to cancel a scheduled meal? 

If for some reason you cannot cook a scheduled meal, please let your Diner know as soon as you can. You can also contact your local Casserole Club to pass on a message.

 Further Information 

Have any questions that haven't been answered here?
Get in touch with us at  hello@casseroleclub.com .

 More about us 
 Becoming a Cook 
 Becoming a Diner 
 Blog 

 FAQ 
 hello@casseroleclub.com 
 Help 
 Terms 

 For organisations 
 Casserole Club is a service created by FutureGov and provided with the support of local authorities and third-sector organisations. 

A product from

 Futuregov 

Casserole Club

 Sign in 
 Suggest a Diner 
 Become a Cook 
 About us 
 Visit Casserole Australia 

 Help 

 Find support 

Casserole Club has been designed to deliver the best possible experience for everyone, if you need additional support using the site, the BBC provide easy to use guidance.

 You can find guidance from the BBC about: 

Making your mouse easier to use

Using your keyboard to control your mouse

Alternatives to a keyboard and mouse

Increasing the size of the text in your web browser

Changing text and background colours

How to magnify your screen

Screen readers and talking browsers

 Having problems? 

If you are having any problems using this site, or wish to leave us feedback get in touch.
 hello@casseroleclub.com 

 More about us 
 Becoming a Cook 
 Becoming a Diner 
 Blog 

 FAQ 
 hello@casseroleclub.com 
 Help 
 Terms 

 For organisations 
 Casserole Club is a service created by FutureGov and provided with the support of local authorities and third-sector organisations. 

A product from

 Futuregov 

Casserole Club

 Sign in 
 Suggest a Diner 
 Become a Cook 
 About us 
 Visit Casserole Australia 

 Become a Casserole Club Cook 

If you can cook an extra portion of food every once in a while, become a Casserole Club Cook today. We can put you in touch with an older neighbour who would benefit from one of your meals and a friendly chat.

 The once-a-month Cook 
 Murtz doesn t have as much time as he d like to share food with Tony. Once a month he goes round with a shepherd s pie and chats with Tony about gardening or travelling.

 New local friends 
 Pam and Maggie live close to each other and would never have met if it wasn t for Casserole. Sharing food has been a great way for both of them to make new friends in the community.

 The weekly laugh 
 Busy mum Angela has been sharing food with Diane for over a year. Angela always has such fun seeing Diane; they're always laughing from the moment she steps in the house.

 Knits the community together 
 Vijaya has been visiting Tom for over two years. Being a Casserole Cook enables Vijaya to give back to her community whilst sharing her delicious food. 

 Sharing a plate is simple 

 1 
 Check we re live in your area. 

 2 
 Create an account and sign in. 

 3 
 Learn a bit about food safety. 

 4 
 Complete our safety checks. 

 Joining us usually takes about 20 minutes.   You ll need your address history and some ID such as a passport or driving licence.

 Are we are in your area? Check your postcode 

 Building a safe community 

 Our Cooks have passed background checks 
 All our Cooks have to have a DBS criminal records check. It allows us to verify your ID and have a chat about what you re looking for in the project. After about a week, you will be able to start sharing food. 

 We're here every step of the way 
 We take extra care to make sure that our Cooks and Diners are happy and safe. We re always here to help with any questions or issues you may have and we keep in contact by phone or email. 

 Making a difference to real people 

 I enjoy seeing Sophie every week and we have really hit it off. It's helpful to me because I can't go out easily and enables me to eat better.

 Edith 
 Enjoys outings 

 The food is good but sharing stories and having a good conversation is very important.

 Gledsey 
 Texts her Cook 

 It s lovely having a visitor every now and then. We don t meet every week but when we do, I enjoy having a chat.

 Sid 
 Has lots of stories 

 Are we are in your area? Check your postcode 

 More about us 
 Becoming a Cook 
 Becoming a Diner 
 Blog 

 FAQ 
 hello@casseroleclub.com 
 Help 
 Terms 

 For organisations 
 Casserole Club is a service created by FutureGov and provided with the support of local authorities and third-sector organisations. 

A product from

 Futuregov 

Casserole Club

 Sign in 
 Suggest a Diner 
 Become a Cook 
 About us 
 Visit Casserole Australia 

 Suggest a Diner 

If you know an older person who might benefit from a home-cooked meal delivered to them by a friendly neighbour, tell them about Casserole Club then let us know.

 Firm friends 
 Edith and Sophie have been sharing food for over a year now and love having a catch up over a home-cooked meal every week. As well as the food, Edith enjoys going on outings with Sophie and meeting some of her family.

 Good conversations 
 Gledsey loves a chat and keeps in touch with her Cook Georgia by texting every week. They ve shared lots of delicious meals and recipes since they first met and are looking forward to sharing more despite Georgia s busy schedule.

 Every now and then 
 Sid sees his Cook Zoe every month or so and really enjoys sharing stories with her about his life and hearing about hers. Zoe has been visiting Sid since last year and they ve shared lots of different meals, including a casserole which Sid loved!

 Sounds good, what's next 

 1 
 Check to see if Casserole is live where the person lives. 

 2 
 Ask them if they d like a neighbour to deliver them a meal every now and then. 

 3 
 If they d like to meet a neighbour, refer them to us here. 

 4 
 Let them know we ll be in touch by phone to introduce them to Casserole Club. 

 Are we live where the person lives? Check the postcode 

 Building a safe community 

 We keep in touch with our Diners 
 Our Diners are important to us and we take extra care to make sure they feel safe and comfortable with the food and company they are receiving from their Cook. We check in with Diners over the phone and always tell them about a new Cook before we introduce them. 

 We check our Cooks 
 Before our volunteers become Casserole Cooks they go through a safeguarding process that includes a food safety quiz and a DBS criminal records check. We also keep in contact with them to see that everything is going ok with their Diner. 

 We need your help 
 We know that potential Diners can get upset if they get a call that they aren t expecting. Help them (and us) by telling them about Casserole before you refer them. Even if Diners aren t sure they want to join, we can still have a chat and answer any questions they may have over the phone.  

 Some of our Cooks 

 I only have the time to meet Tony once a month but I always enjoy our long chats about travelling and gardening.

 Murtz 
 Shares once a month 

 It s nice to give back to the community after benefitting from it for so long, and I really enjoy Tom s company.

 Vijaya 
 Loves her community 

 It s a great way to make new friends in the community. Pam and I may never have met had it not been for Casserole Club.

 Maggie 
 Paired up for 3 years 

 Are we live where the person lives? Check the postcode 

 More about us 
 Becoming a Cook 
 Becoming a Diner 
 Blog 

 FAQ 
 hello@casseroleclub.com 
 Help 
 Terms 

 For organisations 
 Casserole Club is a service created by FutureGov and provided with the support of local authorities and third-sector organisations. 

A product from

 Futuregov 

Casserole Club

 Sign in 
 Suggest a Diner 
 Become a Cook 
 About us 
 Visit Casserole Australia 

 Doing something great with an extra plate 

 Featured by 

 Join Casserole Club 

 Become a Cook 
 Suggest a Diner 

Casserole Club volunteers share extra portions of home-cooked food with people in their
area who aren t always able to cook for themselves. They share once a week, once a month,
or whenever works best for them.

 Be part of the community! 

 Share a plate 
 Cook an extra portion and deliver it to a neighbour who needs it. 
 Read more 

 Get a plate 
 Share the name of someone who could do with a home-cooked meal. 
 Read more 

 Spread the word 
 Let people know about who we are and what we do. 
 Spread the word 

 The informality of Casserole Club makes it work. Pam lives about 5 minutes from me.
She s very chatty and we have a laugh. She likes my pork, apple and apricot casserole. 

 Maggie 

 Join the community on Facebook 

 Making a difference to real people 

 70% 
 of those receiving meals count their volunteer Cooks as friends. 

 80% 
 of those receiving meals wouldn t have as much social contact without Casserole Club. 

 90% 
 of our volunteer cooks would recommend Casserole Club to a friend. 

 Sign up as a Cook 

 Are we are in your area? Search a postcode. 

 Suggest a Diner 

 Spread the word 

Facebook

Tweet

Email to a friend

 More about us 
 Becoming a Cook 
 Becoming a Diner 
 Blog 

 FAQ 
 hello@casseroleclub.com 
 Help 
 Terms 

 For organisations 
 Casserole Club is a service created by FutureGov and provided with the support of local authorities and third-sector organisations. 

A product from

 Futuregov 

Casserole Club

 Sign in 
 Suggest a Diner 
 Become a Cook 
 About us 
 Visit Casserole Australia 

 Sign in 

 Email address 

 Password 

 Remember me 

 Forgot your password? 

Not signed up yet?

Start cooking with Casserole

 More about us 
 Becoming a Cook 
 Becoming a Diner 
 Blog 

 FAQ 
 hello@casseroleclub.com 
 Help 
 Terms 

 For organisations 
 Casserole Club is a service created by FutureGov and provided with the support of local authorities and third-sector organisations. 

A product from

 Futuregov 

Casserole Club

 Sign in 
 Suggest a Diner 
 Become a Cook 
 About us 
 Visit Casserole Australia 

 Forgot your password? 

 Enter your email address and we'll email you you instructions to reset your password. 

 Email address 

 More about us 
 Becoming a Cook 
 Becoming a Diner 
 Blog 

 FAQ 
 hello@casseroleclub.com 
 Help 
 Terms 

 For organisations 
 Casserole Club is a service created by FutureGov and provided with the support of local authorities and third-sector organisations. 

A product from

 Futuregov 

 A social enterprise that organises local meals exchange service. They are commissioned to organise this activity by local authorities.
WHOLE PROJECT MARK