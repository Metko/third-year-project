NHS Kernow | Hospital admissions plummet, thanks to pioneering Living Well

            Adjust Text Size 
             Close 

 NHS Choices 

 NHS Choices website with full NHS services and health information 

 Report it 

                            Please report an incident you witnessed to the National Reporting and Learning System (NRLS) of the NHS 

 Low vision options 

 Text Only 

 Sitemap 

 External Links 

 Log in 

 Email 

 Password 

            Remember me 

 Forgotten your login details? 

 Register for new account? 

 GP Zone 

 Email 

 Password 

            Remember me 

 Forgotten your login details? 

 Register for new account? 

 Staff Zone 

 Email 

 Password 

            Remember me 

 Forgotten your login details? 

 Register for new account? 

 Kernow Clinical Commissioning Group 

 Home 

 Localities 

                                    Coastal Cluster

                                    Falmouth and Penryn

                                    East Cornwall

                                    Newquay

                                    Mid Cornwall

                                    North Cornwall

                                    North Kerrier

                                    Penwith

                                    South Kerrier and Isles of Scilly

                                    Truro

 Your health 

 A to Z of health conditions 

                                    Anxiety, depression and mental health

                                    Carers

                                    Children and young people

                                    Continuing healthcare

                                    Elective care programme

                                    End of life care

                                    Infection prevention and control

                                    Learning disabilities

                                    Long term conditions

                                    Maternity

                                    Older people

                                    Patient transport

                                    Personal health budgets

                                    Stroke

 Get involved 

                                    Community services listings

                                    Consultation

                                    Engagement

                                    Events and submit an event form

 Expert Patient Programme 

 Healthwatch Cornwall 

 Healthwatch Isles of Scilly 

 Get info 

                                    Choose well

                                    Cornwall Quality Care Collaborative

                                    Equality and Diversity

                                    Governing Body meetings

                                    Individual Funding Requests

                                    Information Governance

                                    Referral Management Service

                                    Safeguarding

                                    Treatment policies

 About us 

                                    Annual report

                                    Media centre

                                    Our principles

                                    Our priorities

                                    Policies and procedures

                                    What we spend and how we spend it

                                    Who we are and what we do

                                    Working with us

 Contact us 

                Search 

 You are here: 

 Home News 2015 08 Hospital admissions plummet, thanks to pioneering Living Well 

                    Hospital admissions plummet, thanks to pioneering Living Well

 The award-winning Living Well, which helps people take control of their lives and reduce their dependency on health and care services, has led to a 34 per cent* reduction in emergency hospital admissions. 
 Living Well is an innovative health and care approach that brings together people working in health, social care, the voluntary sector and the community to support people with two or more long-term conditions or are receiving social care. It has been developed with the Age UK integrated care programme and is supporting 1572 people in Penwith, Newquay and east Cornwall. Age UK has rolled our similar schemes across England. 
 New figures among people being supported in Cornwall show: 

 a 34 per cent reduction in emergency hospital admissions; 
 a 21 per cent reduction in emergency department attendances; 
 a 32 per cent reduction in hospital admissions overall. 

 The results show what can be achieved when people join forces and provide tailored care to meet people s needs; helping to change people s lives for the better and avoid unnecessary hospital admissions. 
 The approach identifies what s important to the person through a  guided conversation  with a voluntary sector worker and someone who has become highly dependent on formal care, often combined with social isolation. This leads to small steps that build confidence and social activity, which have a positive effect on the person s health, independence and wellbeing. 
 Community and Social Care Minister Alistair Burt said:  I am delighted at the excellent results shown by Living Well, congratulations to everyone who has been involved. As one of our Integration Pioneers, Cornwall is leading the way in joining-up health and social care services and this report shows more patients are getting better care, closer to home. 
 Nationally, our  5.3 billion Better Care Fund is getting every local NHS and council working together to prevent people from becoming ill and provide care services seven days a week so we avoid unnecessary stays in hospital. 
 Joy Youart, NHS Kernow s Managing Director, said:  We are at the start of a journey to transform the way care is coordinated by bringing together health, social care, the voluntary sector and communities. Our vision is for an integrated system that enables people to access seamless care to help them live the lives they want. 
 We are beginning to see improvements across the entire system to support people from being admitted to hospital and, when they do, getting them home as soon as possible, with high levels of care in place to reduce their risk of being readmitted. 
 Pam Creaven, Director of Services at Age UK said:  Continuing positive results in reducing hospital admissions prove that pioneering programmes of this nature are vital in tackling the ongoing and spiralling crisis in care. By relying on conventional health and care services we ve seen time and time again older people are being let down and having to struggle on alone or being admitted to hospital when it could be prevented. 
 Living Well shows that when professional groups are committed to working successfully together, pressures across the health and care system can be dramatically eased and, with support, people can live better quality lives. 
 In October 2014, Living Well received a glowing endorsement in NHS England s Five Year Forward View, which sets out a vision for the future of the NHS. 
 You can find out more about Living Well online at  www.knowledgebucket.org where you can watch films and read stories from people who have benefited from the approach. 
 *The evaluation figures are based on a cohort of 325 people who were supported by Living Well in Penwith from January 2014 to January 2015. The interactive graphic  to illustrate Living Well s achievements allows you to read and listen to people s experiences of Living Well. 

 Contact us 

                The Sedgemoor Centre 
                Priory Road, St Austell, Cornwall PL25 5AS 

                01726 627800 

                    kccg dot contactus (at) nhs dot net 

 Links 

                        Accessibility 

                        FOIs 

                        Media Centre 

                        Privacy 

                        Terms   conditions 

 Join 

                        Job vacancies 

                        Facebook 

                        Twitter 

                        You Tube 

 Page tools 

 Print 

 Email page to a friend 

 BrowseAloud 

 Share this page 

          Kernow Clinical Commissioning Group 2017 - All Rights Reserved  . 

    Our chosen agency is:  Connect Internet Solutions 

NHS Kernow | Hospital admissions plummet, thanks to pioneering Living Well

            Adjust Text Size 
             Close 

 NHS Choices 

 NHS Choices website with full NHS services and health information 

 Report it 

                            Please report an incident you witnessed to the National Reporting and Learning System (NRLS) of the NHS 

 Low vision options 

 Text Only 

 Sitemap 

 External Links 

 Log in 

 Email 

 Password 

            Remember me 

 Forgotten your login details? 

 Register for new account? 

 GP Zone 

 Email 

 Password 

            Remember me 

 Forgotten your login details? 

 Register for new account? 

 Staff Zone 

 Email 

 Password 

            Remember me 

 Forgotten your login details? 

 Register for new account? 

 Kernow Clinical Commissioning Group 

 Home 

 Localities 

                                    Coastal Cluster

                                    Falmouth and Penryn

                                    East Cornwall

                                    Newquay

                                    Mid Cornwall

                                    North Cornwall

                                    North Kerrier

                                    Penwith

                                    South Kerrier and Isles of Scilly

                                    Truro

 Your health 

 A to Z of health conditions 

                                    Anxiety, depression and mental health

                                    Carers

                                    Children and young people

                                    Continuing healthcare

                                    Elective care programme

                                    End of life care

                                    Infection prevention and control

                                    Learning disabilities

                                    Long term conditions

                                    Maternity

                                    Older people

                                    Patient transport

                                    Personal health budgets

                                    Stroke

 Get involved 

                                    Community services listings

                                    Consultation

                                    Engagement

                                    Events and submit an event form

 Expert Patient Programme 

 Healthwatch Cornwall 

 Healthwatch Isles of Scilly 

 Get info 

                                    Choose well

                                    Cornwall Quality Care Collaborative

                                    Equality and Diversity

                                    Governing Body meetings

                                    Individual Funding Requests

                                    Information Governance

                                    Referral Management Service

                                    Safeguarding

                                    Treatment policies

 About us 

                                    Annual report

                                    Media centre

                                    Our principles

                                    Our priorities

                                    Policies and procedures

                                    What we spend and how we spend it

                                    Who we are and what we do

                                    Working with us

 Contact us 

                Search 

 You are here: 

 Home News 2015 08 Hospital admissions plummet, thanks to pioneering Living Well 

                    Hospital admissions plummet, thanks to pioneering Living Well

 The award-winning Living Well, which helps people take control of their lives and reduce their dependency on health and care services, has led to a 34 per cent* reduction in emergency hospital admissions. 
 Living Well is an innovative health and care approach that brings together people working in health, social care, the voluntary sector and the community to support people with two or more long-term conditions or are receiving social care. It has been developed with the Age UK integrated care programme and is supporting 1572 people in Penwith, Newquay and east Cornwall. Age UK has rolled our similar schemes across England. 
 New figures among people being supported in Cornwall show: 

 a 34 per cent reduction in emergency hospital admissions; 
 a 21 per cent reduction in emergency department attendances; 
 a 32 per cent reduction in hospital admissions overall. 

 The results show what can be achieved when people join forces and provide tailored care to meet people s needs; helping to change people s lives for the better and avoid unnecessary hospital admissions. 
 The approach identifies what s important to the person through a  guided conversation  with a voluntary sector worker and someone who has become highly dependent on formal care, often combined with social isolation. This leads to small steps that build confidence and social activity, which have a positive effect on the person s health, independence and wellbeing. 
 Community and Social Care Minister Alistair Burt said:  I am delighted at the excellent results shown by Living Well, congratulations to everyone who has been involved. As one of our Integration Pioneers, Cornwall is leading the way in joining-up health and social care services and this report shows more patients are getting better care, closer to home. 
 Nationally, our  5.3 billion Better Care Fund is getting every local NHS and council working together to prevent people from becoming ill and provide care services seven days a week so we avoid unnecessary stays in hospital. 
 Joy Youart, NHS Kernow s Managing Director, said:  We are at the start of a journey to transform the way care is coordinated by bringing together health, social care, the voluntary sector and communities. Our vision is for an integrated system that enables people to access seamless care to help them live the lives they want. 
 We are beginning to see improvements across the entire system to support people from being admitted to hospital and, when they do, getting them home as soon as possible, with high levels of care in place to reduce their risk of being readmitted. 
 Pam Creaven, Director of Services at Age UK said:  Continuing positive results in reducing hospital admissions prove that pioneering programmes of this nature are vital in tackling the ongoing and spiralling crisis in care. By relying on conventional health and care services we ve seen time and time again older people are being let down and having to struggle on alone or being admitted to hospital when it could be prevented. 
 Living Well shows that when professional groups are committed to working successfully together, pressures across the health and care system can be dramatically eased and, with support, people can live better quality lives. 
 In October 2014, Living Well received a glowing endorsement in NHS England s Five Year Forward View, which sets out a vision for the future of the NHS. 
 You can find out more about Living Well online at  www.knowledgebucket.org where you can watch films and read stories from people who have benefited from the approach. 
 *The evaluation figures are based on a cohort of 325 people who were supported by Living Well in Penwith from January 2014 to January 2015. The interactive graphic  to illustrate Living Well s achievements allows you to read and listen to people s experiences of Living Well. 

 Contact us 

                The Sedgemoor Centre 
                Priory Road, St Austell, Cornwall PL25 5AS 

                01726 627800 

                    kccg dot contactus (at) nhs dot net 

 Links 

                        Accessibility 

                        FOIs 

                        Media Centre 

                        Privacy 

                        Terms   conditions 

 Join 

                        Job vacancies 

                        Facebook 

                        Twitter 

                        You Tube 

 Page tools 

 Print 

 Email page to a friend 

 BrowseAloud 

 Share this page 

          Kernow Clinical Commissioning Group 2017 - All Rights Reserved  . 

    Our chosen agency is:  Connect Internet Solutions 

Cornwall is one of the most deprived counties in the United Kingdom with an ageing population and facing greater demands on health and social care services due to the increasing number of people with long-term conditions. People with long-term conditions such as type 2 diabetes, chronic obstructive pulmonary disease or heart failure are frequent users of high cost hospital services that may treat their medical needs but not address the wider social causes for their illness. Living Well is an integrated health and social care innovation that is a partnership between the local National Health Service, municipality and Age UK, the largest national voluntary organisation for older people, that has been operating since 2014. It targets people with two long-term conditions, who are usually but far from exclusively people over the age of 50 years, through referrals from General Practitioners and hospital services. They are invited to have a guided conversation with one of twelve Age UK co-ordinators that cover the three pilot areas of Newquay, Penwith and east Cornwall. The guided conversation covers what is happening in the patients life, what is important to them and how they can make lifestyle changes that will help them to more effectively self-manage their condition. They can then receive support from a local volunteer in the community or when they engage with health care services.
The three aims of Living Well are to improve the health and wellbeing of people with long-term conditions, to improve the experience of care service and to reduce the cost of care by preventing emergency hospital admissions. More than 800 patients with long-term conditions have engaged with the Living Well programme and compared to a matched cohort of patients there has been a reduction in emergency hospital admissions of 34 per cent and a 21 per cent reduction in emergency department attendances. More than 60 volunteers have been recruited to support people with long-term conditions and a community mapping exercise has been undertaken to identify the opportunities that people have to build stronger social connections in the community. The mental wellbeing of patients who have engaged with Living Well has improved by 20 per cent according to the validated Short Warwick and Edinburgh Well being Scale. The innovation is being evaluated by a range of partners including Age UK and local universities as well as health and social care organisations to assess its impact on patients and the cost of care. Patients who engage with Living Well cost approximately £400 per case and the estimated return on this investment is three times that amount due to the reduction in emergency hospital attendances and admissions. This is a pilot project that is being funded from the pooling of public budgets, the Duchy of Cornwall Health Charity and Age UK with a view to learning from this experience to roll it out in other areas where integrated health and social care would benefit patients and lower costs by preventing frequent emergency use of health care services.
There are multiple active ageing domains that the Living Well initiative is likely to influence and improve. These include improving access to health and care services that will enable people with long-term conditions to live independently in their own home while receiving a modest level of social support from volunteers. It is worthwhile noting that around 20% of patients who are being supported by Living Well go on to become volunteers themselves and provide guidance and support to people in a similar situation. There is evidence of improvements in mental well-being and the community mapping exercise is building strong social connections in communities that is likely to reduce social isolation and loneliness and thus improve health and well-being.
WHOLE PROJECT MARK