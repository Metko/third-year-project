NowWeMove

 About NowWeMOVE 

 The Challenge 
 Our Approach 
 The Vision 
 Your Role 
 Our Team 
 Our Partners 

 Contact us 

 What We Do 

 Blog 

 Close 

 ISCA EU Parlament 
 European Panna Tour 
 Inactivity Time Bomb 
 No Elevators Day 
 FlashMOVE 
 NowWeBike 
 European Fitness Day 
 MOVE Week 

 MOVE Congress 
 MOVEment Pills 

 MOVE Quality 
 MOVE Transfer 

						  MORE 

 European Fitness Day 
 MOVE Week 

 MOVE Congress 
 MOVEment Pills 

 MOVE Quality 
 MOVE Transfer 

 About NowWeMOVE 
 What We Do 

 Blog 

 The Challenge 
 Our Approach 
 The Vision 
 Your Role 
 Our Team 
 Our Partners 

 Contact us 

 #NOWWEMOVE 
 ISCA launched the NowWeMOVE campaign in 2012, with the aim of bringing the sport for all sector and a variety of other sectors together to tackle the physical inactivity epidemic across Europe.  

 #NOWWEMOVE 

 Two thirds of Europe s adult population do not meet the levels of physical activity recommended for their health and wellbeing (WHO Europe, 2008). That is why ISCA launched the NowWeMOVE campaign in 2012, with the aim of bringing the sport for all sector and a variety of other sectors together to tackle the physical inactivity epidemic across Europe.   
 NowWeMOVE is now Europe s biggest campaign promoting sport and physical activity. Its vision is to get  100 million more Europeans active in sport and physical activity by 2020 . The campaign s overall objectives are to raise awareness of the benefits of sport and physical activity among European citizens; promote opportunities to be active in sport and physical activity; and enable sustainable and innovative capacity building for providers of physical activity initiatives through open-source solutions and advocacy. 
 When the NowWeMOVE campaign started in 2012, it established MOVE Week as its flagship event promoting the benefits of physical activity to citizens all around Europe. It called for MOVE Agents to organise events in their communities and register them on the official website to celebrate the collective impact they were having in their own countries and as a combined European movement. The numbers were impressive:  
						250 MOVE Agents organised 120 events in 23 countries attracting 140,000 participants. In 2013 they were even better: 600 MOVE Agents, 1259 events, 30 countries and 500,000 participants. In 2014 they went through the roof: 2350 MOVE Agents, 5601 events, 38 countries and 1 million participants! In 2015 they went through the roof again: 2917 MOVE Agents, 7125 events, 38 countries and 1 804 930 participants! And 2016 is expected to be even bigger.  
 MOVE Week has proven to be a valuable platform not only to raise awareness about physical activity, but also to advocate for change among policy makers and empower MOVE Agents in achieving better exposure and more support for their initiatives.   

 ISCA - International Sports and Culture Association  
  ISCA is a global platform open to organisations working within the field of sport for all, recreational sports and physical activity.  Created in 1995, ISCA is today a global actor closely cooperating with its   180 member organisations worldwide 

 The #NowWeMOVE campaign expands
 The NowWeMOVE campaign isn t just about MOVE Week. In 2013, it launched two new capacity building initiatives, MOVE Quality and MOVE Transfer. This year, the it will feature a year-round calendar of NowWeMOVE events including a European Panna Tour in May/June, a Europe-wide No Elevators Day in June and a cross-border cycling event in July-September  

 we know there is a growing threat that is ready to overtake smoking: lack of physical activity! 
 Read about The Challenge 

 MOVE Week is an annual Europe-wide event showcasing the benefits of being active and participating regularly in sport and physical activity. 
 Learn more 

 The most active day in Europe is coming: European Fitness Day. 
 Learn more 

 The European School Sport Day (ESSD) is a school day dedicated to promoting physical activity and health for everyone. 
 Learn more 

 MOVE Congress is one of the few conferences in the world, which focuses solely on recreational sport and physical activity. 
 Learn more 

 PHYSICAL ACTIVITY MATTERS 
 Due to the threat of the physical inactivity  pandemic  and related serious health issues, physical activity has climbed international and national political agendas. However, there is still a gap between political agendas and actions. 
				The goals to make people more physically active can only be reached by innovative and practical solutions which we are working to implement on European level.  

 TAKE ACTION 

 home 
 About NowWeMove   

 About #NowWeMOVE 

 The Challenge 
 Our Approach 
 The Vision 
 Your Role 
 Our Team 
 Our Partners 

 What We Do 
 MOVE Week 
 No Elevators Day 
 European Fitness Day 
 Inactivity Time Bomb 
 MOVEment Pills 
 MOVE Congress 
 NowWeMOVE goes to EU Parliament 

 What We Did 
 NowWeBike 
 Panna Tour 
 FlashMOVE 
 MOVE Transfer 
 MOVE Quality 

 Find out more at 
 NowWeMOVE Blog 
 Contact 

 Follow NowWeMOVE at 
 Facebook 
 Google+ 
 Twitter 
 YouTube 

				 2017 NowWeMove. All rights reserved. 

NowWeMove

 About NowWeMOVE 

 The Challenge 
 Our Approach 
 The Vision 
 Your Role 
 Our Team 
 Our Partners 

 Contact us 

 What We Do 

 Blog 

 Close 

 ISCA EU Parlament 
 European Panna Tour 
 Inactivity Time Bomb 
 No Elevators Day 
 FlashMOVE 
 NowWeBike 
 European Fitness Day 
 MOVE Week 

 MOVE Congress 
 MOVEment Pills 

 MOVE Quality 
 MOVE Transfer 

						  MORE 

 European Fitness Day 
 MOVE Week 

 MOVE Congress 
 MOVEment Pills 

 MOVE Quality 
 MOVE Transfer 

 About NowWeMOVE 
 What We Do 

 Blog 

 The Challenge 
 Our Approach 
 The Vision 
 Your Role 
 Our Team 
 Our Partners 

 Contact us 

 #NOWWEMOVE 
 ISCA launched the NowWeMOVE campaign in 2012, with the aim of bringing the sport for all sector and a variety of other sectors together to tackle the physical inactivity epidemic across Europe.  

 #NOWWEMOVE 

 Two thirds of Europe s adult population do not meet the levels of physical activity recommended for their health and wellbeing (WHO Europe, 2008). That is why ISCA launched the NowWeMOVE campaign in 2012, with the aim of bringing the sport for all sector and a variety of other sectors together to tackle the physical inactivity epidemic across Europe.   
 NowWeMOVE is now Europe s biggest campaign promoting sport and physical activity. Its vision is to get  100 million more Europeans active in sport and physical activity by 2020 . The campaign s overall objectives are to raise awareness of the benefits of sport and physical activity among European citizens; promote opportunities to be active in sport and physical activity; and enable sustainable and innovative capacity building for providers of physical activity initiatives through open-source solutions and advocacy. 
 When the NowWeMOVE campaign started in 2012, it established MOVE Week as its flagship event promoting the benefits of physical activity to citizens all around Europe. It called for MOVE Agents to organise events in their communities and register them on the official website to celebrate the collective impact they were having in their own countries and as a combined European movement. The numbers were impressive:  
						250 MOVE Agents organised 120 events in 23 countries attracting 140,000 participants. In 2013 they were even better: 600 MOVE Agents, 1259 events, 30 countries and 500,000 participants. In 2014 they went through the roof: 2350 MOVE Agents, 5601 events, 38 countries and 1 million participants! In 2015 they went through the roof again: 2917 MOVE Agents, 7125 events, 38 countries and 1 804 930 participants! And 2016 is expected to be even bigger.  
 MOVE Week has proven to be a valuable platform not only to raise awareness about physical activity, but also to advocate for change among policy makers and empower MOVE Agents in achieving better exposure and more support for their initiatives.   

 ISCA - International Sports and Culture Association  
  ISCA is a global platform open to organisations working within the field of sport for all, recreational sports and physical activity.  Created in 1995, ISCA is today a global actor closely cooperating with its   180 member organisations worldwide 

 The #NowWeMOVE campaign expands
 The NowWeMOVE campaign isn t just about MOVE Week. In 2013, it launched two new capacity building initiatives, MOVE Quality and MOVE Transfer. This year, the it will feature a year-round calendar of NowWeMOVE events including a European Panna Tour in May/June, a Europe-wide No Elevators Day in June and a cross-border cycling event in July-September  

 we know there is a growing threat that is ready to overtake smoking: lack of physical activity! 
 Read about The Challenge 

 MOVE Week is an annual Europe-wide event showcasing the benefits of being active and participating regularly in sport and physical activity. 
 Learn more 

 The most active day in Europe is coming: European Fitness Day. 
 Learn more 

 The European School Sport Day (ESSD) is a school day dedicated to promoting physical activity and health for everyone. 
 Learn more 

 MOVE Congress is one of the few conferences in the world, which focuses solely on recreational sport and physical activity. 
 Learn more 

 PHYSICAL ACTIVITY MATTERS 
 Due to the threat of the physical inactivity  pandemic  and related serious health issues, physical activity has climbed international and national political agendas. However, there is still a gap between political agendas and actions. 
				The goals to make people more physically active can only be reached by innovative and practical solutions which we are working to implement on European level.  

 TAKE ACTION 

 home 
 About NowWeMove   

 About #NowWeMOVE 

 The Challenge 
 Our Approach 
 The Vision 
 Your Role 
 Our Team 
 Our Partners 

 What We Do 
 MOVE Week 
 No Elevators Day 
 European Fitness Day 
 Inactivity Time Bomb 
 MOVEment Pills 
 MOVE Congress 
 NowWeMOVE goes to EU Parliament 

 What We Did 
 NowWeBike 
 Panna Tour 
 FlashMOVE 
 MOVE Transfer 
 MOVE Quality 

 Find out more at 
 NowWeMOVE Blog 
 Contact 

 Follow NowWeMOVE at 
 Facebook 
 Google+ 
 Twitter 
 YouTube 

				 2017 NowWeMove. All rights reserved. 

It is a feature of contemporary life that the majority of adults lead sedentary lifestyles with not enough physical activity for healthy ageing. The risk to public health of physical inactivity across Europe is estimated to have surpassed that posed by smoking tobacco with an estimated 500,000 premature deaths per year attributed to lack of physical activity due to a myriad of causes including heart disease, cancer and stroke. It is also estimated that the costs of physical inactivity are approximately €80 billion per year across Europe due to higher health care spending and indirect costs to the economy and wider society. However, it is important to realise that the many health conditions attributable to physical inactivity are not inevitable and can be changed through a variety of measures including socially innovative projects.
An ambitious and large scale social innovation is NowWeMOVE (NWM) who are based in Copenhagen and aim to get 100 million more Europeans active in sport and physical activity by 2020. NWM started in 2012 and have the financial support of the European Union and a range of corporate partners including the European Cycling Federation and the International Sports and Culture Association, the global platform for promoting sport for all with 180 member organisations worldwide, to get people of all ages across all European countries moving. It is estimated that around two thirds of the adult population do not meet the recommended guidelines of 30 minutes per day on five days in each week so persuading tens of millions of people to be more active is a colossal challenge. MOVE week started in 2012 as NWMs flagship event across Europe to promote physical activity and involves MOVE agents in communities registering their event on the official website. In 2012 this involved 250 MOVE agents organising 120 events in 23 countries that attracted an estimated 140,000 participants. By 2015 this had grown to 2917 MOVE agents organising 7125 events in 38 European countries with over 1,800,000 participants with aim of making MOVE week in 2016 (September 29-October 5) a larger event.
MOVE agents can be individuals, sports club, voluntary groups, a company or a municipality who register at MOVEWeek.eu and can then use the toolkits, posters and promotional material for a wide range of events that encourage people to try a new sport or recreational physical activity. Building capacity for physical activity is vital to getting more people to be physically active so in 2013 NWM introduced MOVE Transfer which aims to scale up and transfer good examples of grassroots sports initiatives both nationally and internationally. Initiatives that are aimed at hard to reach population such as young people with difficult backgrounds, ethnic minorities, older people, people with disabilities and women and girls are particularly valued. NWM also introduced MOVE Quality as a year-long accreditation process based on evaluation research of the initiative, building partnerships to sustain and scale up successful projects and developing the next wave of MOVE agents. NWM also declares an annual Europe-wide No Elevators Day, encourages and supports MOVE agents to hold FlashMOVE events with choreographed group performances to raise awareness of MOVE Week and the wider campaign. There is also an annual MOVE Congress that attracts more than 300 grassroots sports organisations to focus on increasing participation in recreational sport and physical activity. There are also MOVE Awards to recognise the achievements of the many organisations who have taken part and to build positive media attention for the wider NowWeMOVE campaign.
In relation to active ageing, NWM is primarily concerned with increasing the level of physical exercise among people of all ages. Recreational sports clubs are based on the voluntary efforts of people to organise events so NWM is also relevant to increasing voluntary activities. It is also likely to have positive effects on social connections as the vast majority of events and physical exercise involves participating in groups.
WHOLE PROJECT MARK