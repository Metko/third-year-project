Wayfindr - Empowering vision impaired people

 Accessibility links 

 Skip to main content 
 Accessibility Help 

 Home 
 Open Standard 
 Community 
 About 
 Contact 

 We are an award-winning startup organisation, with ambitions to help the global community of 285 million blind people navigate indoor environments independently. 
 Formed in 2015 and based in London, our mission is to empower vision impaired people to overcome isolation, by setting the standard for audio based navigation. We have developed the world's first internationally-approved standard for accessible audio navigation. 

 Explore the Open Standard 

                                        Tech 4 Good Awards                                     

                                        Accessibility Award                                     

                                        2016 Winner                                     

                                        Vision Pioneer Awards                                     

                                        Innovation Award                                     

                                        2016 Winner                                     

                                        Interaction Awards                                     

                                        Best in Show                                     

                                        2016 Winner                                     

                                        The Third Sector Awards                                     

                                        Breakthrough of the Year                                     

                                        2016 Winner                                     

                                        Nesta New Radicals                                     

                                        2016                                     

                                        Nominet Trust 100                                     

                                        2016 Winner                                     

                                        Impact Awards                                     

                                        Smart Places, DigitalAgenda                                     

                                        2017 Winner                                     

                                        Zero Project                                     

                                        2016 Innovative Practice                                     

                                        D-Lab                                     

                                        Empowering Through Mobile Technologies                                      

                                        2017 Winner                                     

 Our mission 

 Emerging indoor navigation technologies hold the key to a revolution in independent navigation for blind and partially sighted people. In order to achieve the greatest impact globally, we need to develop a consistent standard across wayfinding systems.  
 This will truly open up a world where vision impaired people are no longer held back by their sight loss, removing barriers to employment, to meeting friends and family and engaging in their community.  
 The Wayfindr Open Standard aims to do just that. 

 What people say 
 The first time testing (Wayfindr) at Pimlico was like we were just  normal people  - well - we are normal people but we were just able to travel on our own through a station, without another human being there to help us. I like to travel, so being able to travel through Euston station alone would be incredible.  

 - Courtney Nugent 

 Wayfindr Community 

 We want to see audio wayfinding solutions implemented across the world, in transport networks, shopping centres, hospitals and other places.  We can t do this alone.  Through the Wayfindr Community, we are uniting venue owners, digital navigation services, experts in vision impairment and other interested parties. 

 Alliance Members 

 Friends of Wayfindr 

 Learn more about the Wayfindr Community 

 Learn more 

 How Wayfindr can support you 
 Our Open Standard gives you the tools to create inclusive and consistent experiences for your vision impaired customers. From transport networks and shopping centres, to hospitals and any other indoor space - we can help. Through our on-site trials and consultancy we will work together with you to understand how digital wayfinding can make your estate accessible.  

 Contact Wayfindr 

 News and Blog 

 Blog 

 Wayfindr s Public Transport Hackathon 
 Over the 21st and 22nd January a group of 30 generous people gave up their weekends to do something that could very well end up benefitting every commuter in London.  
 Read More 

 Guest Blog 

 Why openness is key to the Wayfindr Open Standard 
 An open standard provides startups, and social enterprises, with an opportunity to empower communities from around the world - by... 
 Read More 

 Blog 

 6 Reasons Wayfindr is an Open Standard, not an App 
 When ustwo and the RLSB Youth Forum started working on the project which became Wayfindr, we saw the solution as an app. As our understanding of... 
 Read More 

 Sign up to the newsletter 
 Get the latest on how you can be a part of making Wayfindr a reality, including the Wayfindr Open Standard and our upcoming research trials. 

 Blog 
 Accessibility 
 Privacy and Cookies 
 Press 

 Wayfindr, 52-58 Arcola St, London E8 2DJ, (+44) 7557943608, hello@wayfindr.net 
 Wayfindr is a company limited by guarantee registered in England and Wales (09839997) and is a subsidiary of the Royal Society for Blind Children (RSBC), a charity registered in England and Wales (307892). 

 Wayfindr is funded by 

 Website designed and developed by   Elementary Digital 

 Join the Community 

 Download the Open Standard as PDF 

 This Working Draft is the first public release of the Wayfindr Open Standard. We would like to keep in touch so that we can keep improving and tailoring the Open Standard to your needs. 

 Help us improve the Open Standard 

 This Working Draft is the first public release of the Wayfindr Open Standard. We would like to keep in touch and listen to your feedback so that we can keep improving and tailoring the Open Standard to your needs. We promise we won t spam and don't worry, you can always unsubscribe. 

 I'm using the Open Standard as: 
 Designer or developer Venue owner or access consultant Just someone who is interested 

Community - Wayfindr

 Accessibility links 

 Skip to main content 
 Accessibility Help 

 Home 
 Open Standard 
 Community 
 About 
 Contact 

 Community 

 The Wayfindr Community is a group of people and organisations who share a passion to 
change the lives of vision impaired people, tackling poverty and isolation through enabling independent navigation and social inclusion. The Wayfindr Community actively promotes the adoption of indoor audio wayfinding solutions to venue owners, app developers and other relevant parties. 
 Together we can make sure the next generation of digital navigation has accessibility for all built in right from the start, not something we have to fight for afterward. 

 Become part of the Wayfindr Community 

 Join now 

 The Community connects like-minded individuals and groups, and forms the basis for work to update the Wayfindr open standard. The Community also acts as a forum for networking and furthering business opportunities, allowing members to present their ideas and products to peers and customers. By bringing together all stakeholders, the Community also acts as a powerful voice for the indoor audio navigation ecosystem. 
  - Venue owners and their consultants 
 - Digital navigation software developers 
 - Indoor navigation hardware and software providers 
 - Research institutions and universities 
 - Vision impairment charities and their members 
 - Other interested organisations and individuals. 

 Join the Wayfindr Community 

 Wayfindr Community 

 The Wayfindr Community also help us to shape the future of the Open Standard through their contributions to our Working Groups. 

 If you want to become a Community member then you can fill in this  typeform  and join today. 

 Our Community 

 Wayfindr Alliance 

 The Wayfindr Alliance is there to ensure that the Wayfindr Open Standard stays relevant, focused, and timely. They will also ensure that the ambition of the Wayfindr community is achieved. It brings together industry experts, who have an active role in the development and adoption of the Wayfindr Open Standard in their venues, products and services. 

 Sign up to the newsletter 
 Get the latest on how you can be a part of making Wayfindr a reality, including the Wayfindr Open Standard and our upcoming research trials. 

 Blog 
 Accessibility 
 Privacy and Cookies 
 Press 

 Wayfindr, 52-58 Arcola St, London E8 2DJ, (+44) 7557943608, hello@wayfindr.net 
 Wayfindr is a company limited by guarantee registered in England and Wales (09839997) and is a subsidiary of the Royal Society for Blind Children (RSBC), a charity registered in England and Wales (307892). 

 Wayfindr is funded by 

 Website designed and developed by   Elementary Digital 

 Join the Community 

 Download the Open Standard as PDF 

 This Working Draft is the first public release of the Wayfindr Open Standard. We would like to keep in touch so that we can keep improving and tailoring the Open Standard to your needs. 

 Help us improve the Open Standard 

 This Working Draft is the first public release of the Wayfindr Open Standard. We would like to keep in touch and listen to your feedback so that we can keep improving and tailoring the Open Standard to your needs. We promise we won t spam and don't worry, you can always unsubscribe. 

 I'm using the Open Standard as: 
 Designer or developer Venue owner or access consultant Just someone who is interested 

Open Standard - Wayfindr

 Accessibility links 

 Skip to main content 
 Accessibility Help 

 Home 
 Open Standard 
 Community 
 About 
 Contact 

                                 Open Standard                             

 Version:   Recommendation 1.0                             
 Published:  April 19, 2017 

                                                Recommendation 1.0                                             

                                                Candidate Recommendation 1.0                                             

                                                Working Draft 1.0                                             

  Current Version

 Why an Open Standard? 

 When individuals and organisations get behind a purposeful vision, solutions to what previously seemed like big challenges become attainable. 

 The aim is that this Open Standard will help lower the barrier for built-environment owners and digital navigation services to make their environments, products and services inclusive from the outset as we continue to weave technology into our cities. 

 Once the Open Standard is adopted across the built-environment and digital navigation services alike, vision impaired people will benefit from a consistent, reliable and seamless navigation experience. 
 Emerging indoor navigation technologies such as Bluetooth Low Energy (BLE) Beacons and 5G hold the key to opening up the world for vision impaired people. However, in order to achieve the greatest impact globally, there is a pressing need to develop a consistent standard to be implemented across wayfinding systems. This will truly open up a world where vision impaired people are no longer held back by their sight loss, removing barriers to employment, to seeing friends and family and engaging in their community. 
 The Wayfindr Open Standard aims to do just that. As the Open Standard develops it will give venue owners and digital navigation services the tools to implement high quality, consistent, audio wayfinding solutions. It includes an open-source demo app that enables people who download it to use BLE beacons to understand and implement the open standard with real users, in real contexts, in real time. 
 Our Open Standard has now been approved by the International Telecommunications Union as  ITU-T F.921 . We are still working to add to and improve the Open Standard. If you would like to be involved please  contact us . 

 Download the Open Standard as a PDF 

 Download PDF 

 Contents 

 1 Getting Started 
 An introduction to the structure and the content of the Open Standard 

 2 Learning about mobility of vision impaired people 
 An introduction to the world of vision impaired people and their navigation and mobility techniques 

 3 Designing for vision impaired people 
 The design principles that underpin the Wayfindr Open Standard, the different types of audio instructions and the various structural elements of an audio instruction 

 4 Guidelines 
 Guidelines with various techniques for providing effective navigation instructions and creating inclusive digital navigation services 

 5 Wayfinding technologies 
 Recommendations and best practices about the whole lifecycle of a wayfinding technology, such as Bluetooth Low Energy beacons, from the installation to long-term maintenance 

 6 Open Source Wayfindr Demo mobile app 
 Information about the aim of the open-source Wayfindr Demo app, along with a link to access the source code 

 Using the Open Standard 

 As a designer, researcher or developer 
 Find principles and guidelines on how to provide audio instructions for wayfinding, and create inclusive navigation services 
 Get Started 

 As a venue owner, or access consultant 
 Find best practices for the installation, configuration and maintenance of wayfinding technologies in your environment 
 Get Started 

 As anyone else who is interested 
 Explore the Wayfindr Open Standard to find out how it can support you 
 Get Started 

 Open Standard principles 

 Openness and transparency 
 All the processes related to the Wayfindr Open Standard development are open to the public. Any individual or organisation is invited to participate. There will be a public notice about the periods for public comment.  The material of records from meetings and discussions in the Working Groups will be publicly available. 
 Due process 
 The Wayfindr Open Standard is maintained by Wayfindr.org Ltd, which is a nonprofit organisation. There will be an ongoing process for testing and revision of the Wayfindr Open Standard to ensure quality. The process for reviews and updates in the Wayfindr Open Standard will be well defined.  
 Availability 
 The Wayfindr Open Standard is available to the public at no cost both during its development (e.g. via Working Drafts and minutes) and its final stage (via the Recommendations). 
 Collaboration and Consensus 
 The maintenance of the Wayfindr Open Standard is done through collaborative decision making in the Working Groups.  All views and opinions will be considered and there will be equal treatment to all parties involved in the decision making process.  

 Licensing 

 Wayfindr Open Standard is licensed under a 
 Creative Commons Attribution-ShareAlike 4.0 International License . 

 Sign up to the newsletter 
 Get the latest on how you can be a part of making Wayfindr a reality, including the Wayfindr Open Standard and our upcoming research trials. 

 Blog 
 Accessibility 
 Privacy and Cookies 
 Press 

 Wayfindr, 52-58 Arcola St, London E8 2DJ, (+44) 7557943608, hello@wayfindr.net 
 Wayfindr is a company limited by guarantee registered in England and Wales (09839997) and is a subsidiary of the Royal Society for Blind Children (RSBC), a charity registered in England and Wales (307892). 

 Wayfindr is funded by 

 Website designed and developed by   Elementary Digital 

 Join the Community 

 Download the Open Standard as PDF 

 This Working Draft is the first public release of the Wayfindr Open Standard. We would like to keep in touch so that we can keep improving and tailoring the Open Standard to your needs. 

 Help us improve the Open Standard 

 This Working Draft is the first public release of the Wayfindr Open Standard. We would like to keep in touch and listen to your feedback so that we can keep improving and tailoring the Open Standard to your needs. We promise we won t spam and don't worry, you can always unsubscribe. 

 I'm using the Open Standard as: 
 Designer or developer Venue owner or access consultant Just someone who is interested 

Wayfindr will enable visually impaired people across the world to navigate independently using audio instructions from their smart phones.

Eight million people are Visually Impaired (VI) across the EU. Many are denied the opportunity to participate in the digital economy because they’re unable to navigate independently. A journey to work is feared and avoided, representing a key barrier to employment. The ‘Wayfindr Open Standard’ provides digital navigation makers and owners of built environments with the tools and technology to provide high quality digital navigation services to VI people. As a result, every navigation app will provide VI people with a high quality, consistent experience, giving VI people the confidence to travel to work and participate in the digital economy.
WHOLE PROJECT MARK