Asset Allocation Strategy   Linden Thomas

 Interested in getting the most from your portfolio? 

 Learn how your portfolio compares 

 Linden Thomas 

 Linden Thomas 

 Home 
 Do I Qualify? 
 About 

 Independent Investment Consulting 
 Why Choose Us? 
 Community Involvement 

 Our Process 

 Step 1   Establish Your Financial Goals 
 Step 2   Develop Your Personal Investment Plan 
 Step 3   Implementation of Your Investment Plan 
 Step 4   Review Your Investment Results 
 Step 5   Reporting and Managing Your Expectations 

 Awards 
 Discipline 

 Asset Allocation Strategy 
 Fixed Income Versus Using Bond Funds 

 Our Perspective 
 Contact 
 Login 

 One of America s Top Wealth Managers 

 The Pursuit of Top Investment Allocation Results 
 One of the key characteristics of Linden Thomas s pursuit of top portfolio results is its allocation process. An efficient portfolio is made of several key characteristics intended to maximize results while simultaneously minimizing downside risk. One of the most important components needed when seeking superior long term results is beginning with the pursuit of top asset allocation. 
 This component, while often discussed, is rarely achieved by investors. The following are some of the key elements in the pursuit of top allocation results: 

 Match asset allocation to long term risk and growth objectives 
 Implement and maintain proper sector and asset selection 
 Minimize unnecessary overlap 
 Implement an appropriate re-balancing strategy 
 Maintain discipline throughout the market cycle 
 Understand and build around the drivers of long term results 
 Deploy in an efficient manner 
 Understand and build the portfolio to help manage the impact of pullbacks and recovery 

 To gain a deeper insight into each ingredient in the pursuit of top allocation results, let s look a little closer at each: 

 Match asset allocation to long term risk and growth objectives   In seeking a top allocation portfolio start with your individual goals, growth needs, recovery perspective and ability to assume risk. Properly balancing and planning toward these priorities can lead to shorter recoveries and more consistent results. 
 Implement and maintain proper sector and asset selection   An understanding of the benefits and limitations of each investment is vital to proper allocation and investment selection. To position yourself to achieve top allocation results, a process must be used that considers the myriad of factors affecting allocation including their interaction with each other. 
 Minimize unnecessary overlap   An efficient asset allocation strategy must be pure; this means that the portfolio must be examined to prevent overlap both on the sector and individual security level. The knowledge and research needed to execute this correctly is often under appreciated by investors which, in turn, can lead to a poorly executed plan or style drift after deployment. 
 Implement an appropriate re-balancing strategy   A proper re-balancing strategy, implemented through targeted allocation movements or automation, must be in place if seeking to maintain a top allocation long term. This is because after your initial implementation your allocation may shift, so while at the onset your portfolio might have matched your intended objectives, over time this could change. 
 Maintain Discipline throughout the market cycle   Many investors face the urge to chase last year s results only to later discover they are this year s disappointment. A well-reasoned and disciplined process is essential when seeking top allocation results as it both keeps you on track and prevents emotions from derailing long term results. 
 Understand and build around the drivers of long term results   Past performance is not a basis for allocation decisions; the reason is a strong performance over a short time frame can often impact long term results. While reflection on historical results can be a benefit when measuring historical upside/downside capture risk, basing investment decisions on these past results alone seldom should be used to rotate into tempting areas of the market. Historically the pursuit of top allocation results is built on the recognition that an allocation is about more than past results, or timing; it s about looking forward not backward. 
 Deploy in an efficient manner   Certain investment products can hurt results and get in the way of your pursuit of top allocation results, deployment is about choosing the proper investment vehicles to implement the proper allocation and achieve better results. 
 Understand and build a portfolio that focuses on helping to manage the impact of pullbacks and shortening recovery   You must have a thorough understanding of a strategy s strengths and weaknesses prior to implementation to avoid these pitfalls, as a lack of understanding can lead to concentrating or abandoning an asset class at the wrong time. I believe top allocation results can only be achieved by staying the course, even in volatile markets. 

 To demonstrate that no two allocations are equal we have constructed two hypothetical 60/40 portfolios. The first is a poorly defined portfolio and the second is a more efficient enhanced portfolio. Many say that less risk is always associated with less returns but historical evidence proves the opposite is true. Below you will notice that not only did the more efficient enhanced portfolio have better long term results but it did so with less risk and a shorter recovery during volatile periods. Both portfolios are built assuming a $2,000,000 starting balance with a 60% allocation to equities and a 40% allocation to fixed income. 

 Disclaimer: This image does not show actual performance. Past performance is not indicative of future results. The chart is hypothetical and does not represent the actual experience of any client account. 
 Source: Data obtained through ZephyrstyleADVISOR 

 Disclaimer: This image does not show actual performance. Past performance is not indicative of future results. The chart is hypothetical and does not represent the actual experience of any client account. 
 Source: Data obtained through ZephyrstyleADVISOR 

 A Poorly Allocated 60/40 portfolio 

 Upside Capture 
 77.91% 

 Downside Capture 
 73.21% 

 Recovery time 
 39 months 

 Enhanced 60/40 Allocation 

 Upside Capture 
 84.31% 

 Downside Capture 
 55.70% 

 Recovery time 
 34 Months 

 The information above demonstrates three things. First, the enhanced 60/40 portfolio has less risk. We measure this using downside capture, or the amount the portfolio declined in relation to the market. Second, the enhanced 60/40 portfolio had better results. We measure this using upside capture, or the amount the portfolio went up in value when the market was up. The key to these two measures (upside/downside capture) is how they stack up against each other. Put another way, the upside capture is your benefit and the downside capture is the cost to obtain that benefit. Using the information above, in the poorly allocated portfolio you must accept 73% of the downside of the market to receive about 78% of the upside. Compared to the enhanced 60/40 portfolio (56% downside   84% upside) it is apparent that the net results are far superior to the poorly allocated portfolio. Finally, the enhanced 60/40 portfolio had a shorter recovery. Because shorter recovery is essential to continuing to compound portfolio results, it s imperative that down market recovery is shortened or minimized. The longer in the valley of poor results, the less likely you will achieve consistent returns. We measure this using recovery time, or when the portfolio declined in value how long it took before it reached its prior peak. In this example the poorly allocated portfolio took 5 more months to recover compared to the enhanced 60/40 portfolio. 
 Not all portfolios are created equal which can lead to dramatically different results. To see if your portfolio is properly aligned with your goals, and understand exactly what could be hindering your ability to achieve your desired results. To schedule a no-cost, no-obligation consultation,  Contact Us  or: 

  Call: 
 (877)554-8150 

 Email: 
 info@lindenthomas.com 

 Past performance is no guarantee of future results. Investing involves risk including loss of principal. 
Asset allocation cannot eliminate the risk of fluctuating prices and uncertain returns 
Wells Fargo Advisors Financial Network did not assist in the preparation of this report, and its accuracy and completeness are not guaranteed. The opinions expressed in this report are those of the author(s) and are not necessarily those of Wells Fargo Advisors Financial Network or its affiliates. The material has been prepared or is distributed solely for information purposes and is not a solicitation or an offer to buy any security or instrument or to participate in any trading strategy. Additional information is available upon request. 
Investment products and services are offered through Wells Fargo Advisors Financial Network, LLC (WFAFN), Member SIPC. Linden Thomas and Company is a separate entity from WFAFN. 

 		Featured 8 years in a row 
		 America s Top 1,200 
		Financial Advisors 

 		Featured in  Financial Times 
		400 Top Advisors 

 		Featured in  
 		Forbes 8 consecutive 
 		years as one of the 
leading advisors of the  
Southeastern United States. 

 		Featured in 
 		Registered Rep. 
 		 'America's Top 100  
Independent B/D Advisors'   

 		Featured 8 years in a row 
		 America s Top 1,200 
		Financial Advisors 

 		Featured in  Financial Times 
		400 Top Advisors 

 		Featured in  
 		Forbes 8 consecutive 
 		years as one of the 
leading advisors of the  
Southeastern United States. 

 		Featured in 
 		Registered Rep. 
 		 'America's Top 100  
Independent B/D Advisors'   

 Get our Free whitepaper and learn more about our investment process. 
 Enter your name and email address in the fields below and click through to receive your free whitepaper 

 Get immediate access to our informative paper. We take your privacy extremely seriously, and we never sell lists or email addresses. 

 What is your time worth? 
   Call us:  (704) 554-8150   We're  Independent Financial Advisors  who build personalized fixed income portfolios for high net worth investors. 

 READY TO GET STARTED? We re  Independent Financial Advisors  who build personalized fixed income portfolios for high net worth investors. CALL US:  704-554.8150   

 Charlotte Location 
 Linden Thomas and Company 
516 N Tryon Street 
Charlotte, NC 28202 
 CONTACT US 

  2016 Linden Thomas and Company
All rights reserved. 

 Phone: (704) 554.8150 Toll Free: (877) 554-8150 email us:  info@lindenthomas.com Click here for directions/map 

 Charlotte Location Linden Thomas and Company 516 N Tryon Street Charlotte, NC 28202 CONTACT US 

 Phone: (704) 554.8150 Toll Free: (877) 554-8150 email us:  info@lindenthomas.com Click here for directions/map 

  2016 Linden Thomas and Company
All rights reserved. 

 FINRA s BrokerCheck Obtain More Information About Our Firm and Financial Professionals 

	INVESTMENTS IN SECURITIES AND INSURANCE PRODUCTS:

 Not Insured by FDIC or any  Federal Government Agency 

 May Lose Value 

 Not a Deposit of or Guaranteed by a Bank  or Any Bank Affiliate 

 Privacy Policy  |  Legal Disclosures  |  Online Security 

 LINDEN THOMAS AND COMPANY  - Securities-related service may be provided only to individuals residing in: AL, AR, AZ, CA, CO, CT, DC, DE, FL, GA, ID, IL, IN, KY, LA, MA, MD, MI, MO, MT, NC, NJ, NM, NY, OR, PA, SC, SD, TN, TX, VA, and WA.

 Investment and Insurance Products : Not Insured by FDIC or any Federal Government Agency May Lose Value Not a Deposit of or Guaranteed by the Bank or any Bank affiliate.Investment products and services are offered through Wells Fargo Advisors Financial Network, LLC (WFAFN). 

 Wells Fargo Advisors  is the trade name used by two separate registered broker-dealers: Wells Fargo Advisors is a trade name used by Wells Fargo Clearing Services, LLC (WFCS) and Wells Fargo Advisors Financial Network, LLC, Members  SIPC , separate registered broker-dealers and non-bank affiliates of Wells Fargo   Company. WellsTrade brokerage accounts are offered through WFCS. (05/10) 

 Barron's Top 1,000 and Top 1,200 Financial Advisors : The rankings are based on data provided by thousands of advisors and financial services firms. Factors included in the rankings were assets under management, revenue produced for the firm, regulatory record, quality of practice and philanthropic work. Investment performance isn't an explicit component. 

 Financial Times Top 400 Advisors : Rankings are based on data provided by Investment firms, Factors include assets under management, experience, industry certifications and compliance record. Investment performance and financial advisor productions are not explicit components.Registered Rep America's Top 100 Independent B/D Advisors Rankings based on assets under management for Independent broker/dealer reps only. The ranking is not indicative of past or future investment performance.

Award received by Stephen Thomas. Rankings are based on data provided by over 4,000 of the nation's most productive advisors. Factors included in the rankings were assets under management, revenue produced for the firm, regulatory record, quality of practice and philanthropic work. Investment performance isn't an explicit component.

Asset Allocation Strategy   Linden Thomas

 Interested in getting the most from your portfolio? 

 Learn how your portfolio compares 

 Linden Thomas 

 Linden Thomas 

 Home 
 Do I Qualify? 
 About 

 Independent Investment Consulting 
 Why Choose Us? 
 Community Involvement 

 Our Process 

 Step 1   Establish Your Financial Goals 
 Step 2   Develop Your Personal Investment Plan 
 Step 3   Implementation of Your Investment Plan 
 Step 4   Review Your Investment Results 
 Step 5   Reporting and Managing Your Expectations 

 Awards 
 Discipline 

 Asset Allocation Strategy 
 Fixed Income Versus Using Bond Funds 

 Our Perspective 
 Contact 
 Login 

 One of America s Top Wealth Managers 

 The Pursuit of Top Investment Allocation Results 
 One of the key characteristics of Linden Thomas s pursuit of top portfolio results is its allocation process. An efficient portfolio is made of several key characteristics intended to maximize results while simultaneously minimizing downside risk. One of the most important components needed when seeking superior long term results is beginning with the pursuit of top asset allocation. 
 This component, while often discussed, is rarely achieved by investors. The following are some of the key elements in the pursuit of top allocation results: 

 Match asset allocation to long term risk and growth objectives 
 Implement and maintain proper sector and asset selection 
 Minimize unnecessary overlap 
 Implement an appropriate re-balancing strategy 
 Maintain discipline throughout the market cycle 
 Understand and build around the drivers of long term results 
 Deploy in an efficient manner 
 Understand and build the portfolio to help manage the impact of pullbacks and recovery 

 To gain a deeper insight into each ingredient in the pursuit of top allocation results, let s look a little closer at each: 

 Match asset allocation to long term risk and growth objectives   In seeking a top allocation portfolio start with your individual goals, growth needs, recovery perspective and ability to assume risk. Properly balancing and planning toward these priorities can lead to shorter recoveries and more consistent results. 
 Implement and maintain proper sector and asset selection   An understanding of the benefits and limitations of each investment is vital to proper allocation and investment selection. To position yourself to achieve top allocation results, a process must be used that considers the myriad of factors affecting allocation including their interaction with each other. 
 Minimize unnecessary overlap   An efficient asset allocation strategy must be pure; this means that the portfolio must be examined to prevent overlap both on the sector and individual security level. The knowledge and research needed to execute this correctly is often under appreciated by investors which, in turn, can lead to a poorly executed plan or style drift after deployment. 
 Implement an appropriate re-balancing strategy   A proper re-balancing strategy, implemented through targeted allocation movements or automation, must be in place if seeking to maintain a top allocation long term. This is because after your initial implementation your allocation may shift, so while at the onset your portfolio might have matched your intended objectives, over time this could change. 
 Maintain Discipline throughout the market cycle   Many investors face the urge to chase last year s results only to later discover they are this year s disappointment. A well-reasoned and disciplined process is essential when seeking top allocation results as it both keeps you on track and prevents emotions from derailing long term results. 
 Understand and build around the drivers of long term results   Past performance is not a basis for allocation decisions; the reason is a strong performance over a short time frame can often impact long term results. While reflection on historical results can be a benefit when measuring historical upside/downside capture risk, basing investment decisions on these past results alone seldom should be used to rotate into tempting areas of the market. Historically the pursuit of top allocation results is built on the recognition that an allocation is about more than past results, or timing; it s about looking forward not backward. 
 Deploy in an efficient manner   Certain investment products can hurt results and get in the way of your pursuit of top allocation results, deployment is about choosing the proper investment vehicles to implement the proper allocation and achieve better results. 
 Understand and build a portfolio that focuses on helping to manage the impact of pullbacks and shortening recovery   You must have a thorough understanding of a strategy s strengths and weaknesses prior to implementation to avoid these pitfalls, as a lack of understanding can lead to concentrating or abandoning an asset class at the wrong time. I believe top allocation results can only be achieved by staying the course, even in volatile markets. 

 To demonstrate that no two allocations are equal we have constructed two hypothetical 60/40 portfolios. The first is a poorly defined portfolio and the second is a more efficient enhanced portfolio. Many say that less risk is always associated with less returns but historical evidence proves the opposite is true. Below you will notice that not only did the more efficient enhanced portfolio have better long term results but it did so with less risk and a shorter recovery during volatile periods. Both portfolios are built assuming a $2,000,000 starting balance with a 60% allocation to equities and a 40% allocation to fixed income. 

 Disclaimer: This image does not show actual performance. Past performance is not indicative of future results. The chart is hypothetical and does not represent the actual experience of any client account. 
 Source: Data obtained through ZephyrstyleADVISOR 

 Disclaimer: This image does not show actual performance. Past performance is not indicative of future results. The chart is hypothetical and does not represent the actual experience of any client account. 
 Source: Data obtained through ZephyrstyleADVISOR 

 A Poorly Allocated 60/40 portfolio 

 Upside Capture 
 77.91% 

 Downside Capture 
 73.21% 

 Recovery time 
 39 months 

 Enhanced 60/40 Allocation 

 Upside Capture 
 84.31% 

 Downside Capture 
 55.70% 

 Recovery time 
 34 Months 

 The information above demonstrates three things. First, the enhanced 60/40 portfolio has less risk. We measure this using downside capture, or the amount the portfolio declined in relation to the market. Second, the enhanced 60/40 portfolio had better results. We measure this using upside capture, or the amount the portfolio went up in value when the market was up. The key to these two measures (upside/downside capture) is how they stack up against each other. Put another way, the upside capture is your benefit and the downside capture is the cost to obtain that benefit. Using the information above, in the poorly allocated portfolio you must accept 73% of the downside of the market to receive about 78% of the upside. Compared to the enhanced 60/40 portfolio (56% downside   84% upside) it is apparent that the net results are far superior to the poorly allocated portfolio. Finally, the enhanced 60/40 portfolio had a shorter recovery. Because shorter recovery is essential to continuing to compound portfolio results, it s imperative that down market recovery is shortened or minimized. The longer in the valley of poor results, the less likely you will achieve consistent returns. We measure this using recovery time, or when the portfolio declined in value how long it took before it reached its prior peak. In this example the poorly allocated portfolio took 5 more months to recover compared to the enhanced 60/40 portfolio. 
 Not all portfolios are created equal which can lead to dramatically different results. To see if your portfolio is properly aligned with your goals, and understand exactly what could be hindering your ability to achieve your desired results. To schedule a no-cost, no-obligation consultation,  Contact Us  or: 

  Call: 
 (877)554-8150 

 Email: 
 info@lindenthomas.com 

 Past performance is no guarantee of future results. Investing involves risk including loss of principal. 
Asset allocation cannot eliminate the risk of fluctuating prices and uncertain returns 
Wells Fargo Advisors Financial Network did not assist in the preparation of this report, and its accuracy and completeness are not guaranteed. The opinions expressed in this report are those of the author(s) and are not necessarily those of Wells Fargo Advisors Financial Network or its affiliates. The material has been prepared or is distributed solely for information purposes and is not a solicitation or an offer to buy any security or instrument or to participate in any trading strategy. Additional information is available upon request. 
Investment products and services are offered through Wells Fargo Advisors Financial Network, LLC (WFAFN), Member SIPC. Linden Thomas and Company is a separate entity from WFAFN. 

 		Featured 8 years in a row 
		 America s Top 1,200 
		Financial Advisors 

 		Featured in  Financial Times 
		400 Top Advisors 

 		Featured in  
 		Forbes 8 consecutive 
 		years as one of the 
leading advisors of the  
Southeastern United States. 

 		Featured in 
 		Registered Rep. 
 		 'America's Top 100  
Independent B/D Advisors'   

 		Featured 8 years in a row 
		 America s Top 1,200 
		Financial Advisors 

 		Featured in  Financial Times 
		400 Top Advisors 

 		Featured in  
 		Forbes 8 consecutive 
 		years as one of the 
leading advisors of the  
Southeastern United States. 

 		Featured in 
 		Registered Rep. 
 		 'America's Top 100  
Independent B/D Advisors'   

 Get our Free whitepaper and learn more about our investment process. 
 Enter your name and email address in the fields below and click through to receive your free whitepaper 

 Get immediate access to our informative paper. We take your privacy extremely seriously, and we never sell lists or email addresses. 

 What is your time worth? 
   Call us:  (704) 554-8150   We're  Independent Financial Advisors  who build personalized fixed income portfolios for high net worth investors. 

 READY TO GET STARTED? We re  Independent Financial Advisors  who build personalized fixed income portfolios for high net worth investors. CALL US:  704-554.8150   

 Charlotte Location 
 Linden Thomas and Company 
516 N Tryon Street 
Charlotte, NC 28202 
 CONTACT US 

  2016 Linden Thomas and Company
All rights reserved. 

 Phone: (704) 554.8150 Toll Free: (877) 554-8150 email us:  info@lindenthomas.com Click here for directions/map 

 Charlotte Location Linden Thomas and Company 516 N Tryon Street Charlotte, NC 28202 CONTACT US 

 Phone: (704) 554.8150 Toll Free: (877) 554-8150 email us:  info@lindenthomas.com Click here for directions/map 

  2016 Linden Thomas and Company
All rights reserved. 

 FINRA s BrokerCheck Obtain More Information About Our Firm and Financial Professionals 

	INVESTMENTS IN SECURITIES AND INSURANCE PRODUCTS:

 Not Insured by FDIC or any  Federal Government Agency 

 May Lose Value 

 Not a Deposit of or Guaranteed by a Bank  or Any Bank Affiliate 

 Privacy Policy  |  Legal Disclosures  |  Online Security 

 LINDEN THOMAS AND COMPANY  - Securities-related service may be provided only to individuals residing in: AL, AR, AZ, CA, CO, CT, DC, DE, FL, GA, ID, IL, IN, KY, LA, MA, MD, MI, MO, MT, NC, NJ, NM, NY, OR, PA, SC, SD, TN, TX, VA, and WA.

 Investment and Insurance Products : Not Insured by FDIC or any Federal Government Agency May Lose Value Not a Deposit of or Guaranteed by the Bank or any Bank affiliate.Investment products and services are offered through Wells Fargo Advisors Financial Network, LLC (WFAFN). 

 Wells Fargo Advisors  is the trade name used by two separate registered broker-dealers: Wells Fargo Advisors is a trade name used by Wells Fargo Clearing Services, LLC (WFCS) and Wells Fargo Advisors Financial Network, LLC, Members  SIPC , separate registered broker-dealers and non-bank affiliates of Wells Fargo   Company. WellsTrade brokerage accounts are offered through WFCS. (05/10) 

 Barron's Top 1,000 and Top 1,200 Financial Advisors : The rankings are based on data provided by thousands of advisors and financial services firms. Factors included in the rankings were assets under management, revenue produced for the firm, regulatory record, quality of practice and philanthropic work. Investment performance isn't an explicit component. 

 Financial Times Top 400 Advisors : Rankings are based on data provided by Investment firms, Factors include assets under management, experience, industry certifications and compliance record. Investment performance and financial advisor productions are not explicit components.Registered Rep America's Top 100 Independent B/D Advisors Rankings based on assets under management for Independent broker/dealer reps only. The ranking is not indicative of past or future investment performance.

Award received by Stephen Thomas. Rankings are based on data provided by over 4,000 of the nation's most productive advisors. Factors included in the rankings were assets under management, revenue produced for the firm, regulatory record, quality of practice and philanthropic work. Investment performance isn't an explicit component.

Tips To Help Allocate Investments For Maximum Return 

<p>A retirement account is only as good as the chosen investments. Investors have so many options when purchasing stock, that it can be overwhelming to make a wise decision that will allow a retirement account to grow without experiencing significant decreases. Some losses are unavoidable, but others are caused by investments that are made on imperfect knowledge. Before determining the best retirement <a href="http://lindenthomas.com/asset-allocation-strategy/">asset allocation</a>, be sure to consider the following. These tips can help buffer against predictable market shifts, and ensure an investor has the money they need to retire when the time approaches.</p>
<p><strong>Avoid Overlapping Investments</strong></p>
<p>One of the riskiest stock market purchases any investor can make is placing too much money in one company or a single industry. It is important to research the various stocks available and ensure that no more than 10 percent of an investment account is in a given sector. This helps reduce the severity of losses should one sector experience a market upheaval, and can act as a buffer against losses if market prices experience an unexpected decline.</p>
<p><img src="https://s12.postimg.org/omfsaanfx/ID-100529434.jpg" alt="" width="400" height="400" /></p>
<p><strong>Determine Growth Objectives</strong></p>
<p>The first step in establishing an Asset Allocation Strategy is to identify the growth objectives of a retirement account. Long term growth investments are typically more stable but provide smaller returns. Individuals looking for quick growth can benefit from a more risky portfolio that offers increased returns, but with a greater opportunity for losses. To determine which stocks are best for an individual's growth objectives, they should start by tracking the investments history and determine if it is better suited for long or short term gains.</p>
<p><strong>Know When To Buy And Sell</strong></p>
<p>A key factor in successful Asset Allocation is knowing when to buy and sell. The biggest mistake most investors make is selling due to fear and panic. A knee jerk reaction of this nature can cause an investor to lose money. In <a href="https://www.fool.com/retirement/introduction-to-asset-allocation.aspx">situations</a> when the market is in a decrease pattern, it is best to stay the course and allow investments to rebound before making a change. A time of market turmoil can be a perfect time to buy additional stock, as it allows an investor to purchase shares for a reduced amount and increases the propensity for greater returns.</p>
<p>These tips are just the beginning to achieving Top Investment Allocation Results. For more information on establishing a stable retirement account, be sure to contact Linden Thomas and Company. Their team of experienced investors can help anyone prepare for retirement, and mitigate some of the risk associated with the stock market. Contact them today to learn more and take control of the future once and for all.</p>
WHOLE PROJECT MARK