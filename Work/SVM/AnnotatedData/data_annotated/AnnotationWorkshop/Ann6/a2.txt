ABOUT US
We are a non-profit organisation supporting refugees with the dream of becoming developers.
In their journey of interrupted lives, unfinished studies and integration challenges, many asylum seekers and refugees yearn to update their tech skills, but lack learning opportunities. We want to change this.
Last year we launched the first cohort of our 6-month web development programme, coached by a group of professional developers. Today we are running new classes in London and Glasgow. This is just the beginning. With your help we will be expanding to other regions and cities.
If you are interested in participating as a student, coach or volunteer, sign up!
For all other inquiries please contact us at contact@codeyourfuture.co
JOIN US
Welcome to a community where your knowledge and experience will help to transform the lives of refugees in the UK.
We are looking for experienced web developers (HTML/CSS, JavaScript, Node, Angular, React, Database) to participate in our classes. We are also looking for technology managers and entrepreneurs to give workshops on modern software practices and methodologies.
PARTNER WITH US
The tech industry is facing a big challenge to find developers to fill the ever increasing number of vacancies. At the same time, asylum seekers and refugees lack the necessary opportunities to update their skills and integrate better in society.
Together we’ll help to solve both of these issues.
JOIN US
Welcome to a community where your knowledge and experience will help to transform the lives of refugees in the UK.
We are looking for experienced web developers (HTML/CSS, JavaScript, Node, Angular, React, Database) to participate in our classes.
We are also looking for technology managers and entrepreneurs to give workshops on modern software practices and methodologies.
Become a Mentor
Or you can become a volunteer and help us support the expansion of the organisation. No matter where you are in the UK, with your help we’ll create a local program to serve local refugee communities.
The origins of CodeYourFuture told through a graduate
Imagine that you are the founder of a social enterprise. You spend time planning and working on setting the foundations of your project. You work hard and the day arrives when you start offering a service to a group of people, becoming a day of celebration in our collective memory. In the case of CodeYourFuture, it was on the 9th of October 2016, our first class with ten refugees and asylum seekers. Previous to that, CYF existed only in our imagination. Or so we thought so. Reflecting back on those days, there was a crucial moment, before that first meeting, when our organisation was already offering hope in a person’s life, and therefore creating a positive impact.
Just over a year ago CodeYourFuture existed only in the minds of a couple of people that wanted to follow the lead of HackYourFuture and create a coding refugee programme in the UK. We had no mentors, no students and we didn’t know any refugee organisations that could help us in finding them. We only had a version of a syllabus and little more. So, before taking any further actions, we set ourselves the goal of finding an initial group of students committed to becoming developers in our coding school. We took to social media and the first response we got was from Ahmed.
Ahmed is a Syrian refugee. In Aleppo, he had been accepted to study Economics at University, but he never even got to set foot on campus due to the civil war that had started and was forced to flee the country and seek sanctuary here in the UK. He had plans to pursue his programming interest alongside economics and find a way to become a professional developer. Instead, he spent a few years trying to start a life and be allowed to be safe and secure a job. Any job.
In his first e-mail to CYF in August 2016, Ahmed explained that becoming a software developer had been his lifelong dream, and although he was currently struggling, he said:
“I don’t see myself better at anything except being a developer”
We didn’t know back then, but the moment that Ahmed decided to respond to our call, CodeYourFuture was not anymore an idea, it had become real.
It was real because the ‘coding school for refugees in the UK’ did not exist only in the imaginary realm of its creators, but was now present in the mind of a client. We had an applicant who was ready to start studying, to complete the selection process, even before we had any mentors or a space to teach.
During most of the course, Ahmed was learning programming while working full-time on a minimum wage job. But thanks to the guidance of mentors like Mozafar, the founder of CYF Scotland, he managed to complete the six-month course and graduate with the rest of the class. As he mentions in his video, he only aspired to keep learning programming and of being part of a community, nothing else. But the future had bigger plans for him.
Today, Ahmed is a junior software developer at tech start-up WeGotPop. It took him only a few weeks after graduation to start working there, first as an intern, now in a permanent position. This first hire of one of our graduates closes a full cycle for us. It was the moment when, thanks to the support of our mentors, it altered the course of a person’s life. It was a path that had been distorted by external and powerful forces, and which the CYF community had to rectify. Ahmed didn’t imagine that a year after he wrote those first lines to us he would be a professional software developer. Perhaps he didn’t think of that because after so many setbacks it is hard to think big. But what matters is that he was ready to learn, to believe, and to give himself a chance by joining CodeYourFuture.
His triumph puts a smile in all the volunteers and mentors that helped him. We want to share this story with all the refugees in the UK, to let them know that there are many people, in London, Glasgow and soon in Manchester, ready to support them in their pursuit of becoming software developers, no matter the challenges they face. Together with tech companies we can bring change to many more people, while simultaneously making the industry more diverse and inclusive.
CodeYourFuture runs a six-months training programme  in London, Glasgow and Manchester helping refugees become software developers . It provides laptops and aims to cover all the essential expenses to support their training thanks to the sponsorship and donations.

WHOLE PROJECT MARK

