Early Intervention Granny (Baba) Orphan Program | Seniors Raising Orphans | Programs for Elderly Features Innovative Worldwide Programs and Services for Seniors | Programs for Seniors | Elder Care | Caregiving

     Aging Programs   Documentaries   Stop Elder Abuse   How to Help Elderly   About Us   Submissions to our Site   Link to Us    Awards   

       Elder Abuse Prevention      Aging Awareness   Sensitivity   Caregiver and Eldercare   Senior Contributions   Dementia and Memory Loss   Senior Healthcare   End of Life      In-Home Care     Senior Housing    Elderly Nursing    Senior Safety   Senior Activities    Cool Aging Programs Senior Discounts,Coupons, Deals, Rewards     Aging Films Caregiving Films   Death   Dying Films   Dementia Films Elder Abuse Films Inspiring Seniors Films Nursing  Films Gay Seniors LGBT Films 

 Follow Us! 

 Aging Programs  |  Senior Contributions Programs  |
        Intergenerational Programs 
 "EARLY INTERVENTION GRANNY (BABA) ORPHAN" PROGRAM 

 WHAT Retired women in several countries become "grannies" to orphans and assist in their development.  
 WHY To improve the lives of orphaned children and provide them with the resources and support needed so that they can become healthy and productive members of the community. 
 WHERE  
		Bulgaria, Vietnam,  
		      Azerbaijan  
		      Worldwide Orphans Foundation (Based in the USA, services overseas.)

 Program Description 

  "We need an army of Babas...and we almost have that army.   
 -Dr. Jane Aronson,Worldwide Orphan Foundation  
 The Worldwide Orphan Foundation provides direct services in many areas to care for the health of children living in orphanages around the world. One of these services is the Early Intervention Granny (Baba) Orphan" Program.  
 Retired women from local communities with a range of industry experience, some of which include educators, librarians and health care providers, are trained by the organization.  
 The  grannies  work with orphans to provide activities including arts, music and sports and keep records of how the child is developing.  
 Part of the organization's mission is making sure that orphaned children are not defined by the lack of a mother and father.  
 The program is a  creative way to support children so that they can grow up to be productive members of the community.  
  source:wwo.org 

 Learn more about the "Early Intervention Granny (Baba) Orphan "Program 
FIND MORE SENIOR PROGRAMS LIKE THIS: 

 -More Intergenerational Seniors Helping Youth  
 -Intergenerational Social Activities 
 -Senior Employment Programs  
 -Seniors Helping Seniors Programs 
 -Senior Volunteer Opportunities Abroad 
 -Senior  Contributions in General 

   ELDER ABUSE  PREVENTION Abuse Prevention Training Physical Elder Abuse Financial Elder Abuse General Elder Abuse Adopt-A-Senior Programs Temporary, Shelter Housing AGING AWARENESS Sensitivity Training Senior Marketing Gay Senior Sensitivity Training Senior Self-Confidence SENIOR CONTRIBUTIONS Intergenerational Programs Senior Employment Seniors Helping Seniors Seniors Volunteer Abroad General Senior Contributions     DEMENTIA   Dementia Assistance   Activities   Dementia Caregivers   Memory Recall   Mind Fitness   Dementia Training   Advocacy   END OF LIFE CARE   Palliative Care Training   General End of Life Care   Terminally Ill   Their Pets   SENIOR HEALTH Depression Suicide Prevention Drug Abuse Medical Health Health Insurance Benefits  Senior Hunger   Nutrition Elderly Exercise Senior Wellness Poverty     SENIOR IN-HOME CARE In-Home Care Medical In-Home Care Service Dogs for Elderly Visually Impaired Support SENIOR HOUSING Home Sharing, CoHousing Assisted Living   Affordable Housing   Home Modification  Design for Aging in Place Senior Friendly Communities Gay LGBT Senior Housing Temporary, Shelter Housing  ELDERLY NURSING Improving Nursing Care Nursing Home Activities Speciality Nursing Homes Animal Assisted Therapy Admission and Discharge from Nursing Care     SENIOR SAFETY Checking In Service Emergency Medical Response   Senior Driver   Transportation General Senior Safety SENIOR ACTIVITIES Senior Theater   Senior Arts Senior Social Senior Dating  Pets for Elderly Life-Long Learning Intergenerational ELDERLY CAREGIVER Caregiver Support Caregiver Training Grandparents as Parents  INNOVATIVE AGING  SENIOR DISCOUNTS   REWARDS      DOCUMENTARIES   FILMS Aging Films Caregiving Films Death   Dying Films Dementia Films Elder Abuse Films Inspiring Senior Films Nursing  Care Films Gay LGBT Senior Films  GET INVOLVED Stop Elder Abuse   How to Help Elderly       Home  |    About Us  |   Submissions to Our Site  |   Link to Our Site  |   Disclaimer  |  Awards   copyright    www.programsforelderly.com     

The World Health Organisations Global Commission on the Social Determinants of Health report contended that how long and well we live is influenced before we are born, how we grow through early childhood years and adolescence into adulthood. They firmly argued for equity from the start of life in order to lay the foundations for health and well-being
The Worldwide Orphan Foundation has been working in Bulgaria since 2004 developing a range of early intervention programmes to address the developmental progress and health and well-being of children in several orphanages. In 2010, the Bulgarian government adopted a new vision for the care of orphans and at-risk children by mandating that the countrys 130 orphanages should be closed by 2025 and be replaced by a more developed system of foster care, family reunifications and small group homes. The majority of children in Bulgarias orphanages are Roma, one of the countrys largest minority groups, and many such children experience the effects of attachment disorder and developmental delays. The Granny and Orphan programme matches disadvantaged orphans with older women  grannies  who have a wide range of life experiences and wisdom to share with young children. The grannies are vetted, trained and supported to provide one on one support to children through sharing activities such as art, music, reading, games and sport for four hours per day five days a week. The grannies work under the guidance of a psychologist and are required to keep a journal recording the activities and progress of each child. For their efforts, the grannies receive a modest stipend that can be a useful addition to household incomes. The programme has over 100 grannies working with more than 200 children in 11 orphanages across Bulgaria and a similar intervention programme operates in Vietnam. Both are small scale innovations that have the potential to have a significant impact on a relatively modest sized groups but have potential to be scaled up over the life course, but there are still many millions of children who face great disadvantages.
Among that group of disadvantaged children are orphans in Bulgaria facing the prospect of their childhood years in an institution lacking resources. The plight of young children in the post-communist states of eastern European lead to a range of responses from international charities. The Worldwide Orphan Foundation, an American charity founded in 1997 by the renowned paediatrician Dr Jane Aronson, is committed to improving the life chances of orphans around the world so that they can learn to play, grow and lead productive lives.
There is good evidence from neuroscience to show that early intervention work with children at risk of severe disadvantage can make a difference to their developing brain in terms of developing a weak or strong foundation for future learning, behaviour and health. The granny and orphan programme provides a good example of a social innovation that mobilises the skills of older women for the benefit of children experiencing severe disadvantage.
In relation to active ageing, the Granny and Orphan programme clearly provides specialist care to children and through the payment of a stipend to the Grannies contributes to their financial security (technically it is not employment). However, the core of the Granny and Orphan programme is building social connectedness with and for children who are particularly disadvantaged and without which they will not have the social skills to live and thrive in society.
WHOLE PROJECT MARK