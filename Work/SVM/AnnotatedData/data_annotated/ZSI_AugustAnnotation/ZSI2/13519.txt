Jumpido: Educational games for Kinect
===============================

Home
Case Studies
Resellers
How to Buy
About
Contacts
EN
BG
MK
Download
Mathematics made fun  with Kinect for Windows
Watch Video
&times;
Discover the exciting new world of mathematics for kids
Remember mathematics when you were at school? It wasn't exciting, was it? Just a textbook that stared up at you with lots of numbers.
Thanks to Jumpido, children of all abilities can enjoy math using game-based learning!
Jumpido's revolutionary educational software brings the classroom to life with stunning animation.
And at an age where experimentation and exploration mean everything to a child's development - it’s the perfect way to teach math!
Using Microsoft's Kinect motion-sensor, pupils need to combine intelligent thinking with physical gestures in order to solve problems.
There are tonnes of awesome adventures too! It's just up to the teacher to decide what they’re based on.
Jumpido - the modern-day assistant for primary school teachers
Jumpido is great learning platform, suitable for primary school students aged 6 to 12.
All you need to use Jumpido in your classroom is a multimedia projector (or large TV), laptop or PC* and Kinect for Windows.
So who comes up with the content?
Well, the people who matter most in education - the teachers. Jumpido works with local publishers to translate content
in a way that's tailored to your curriculum and easy to understand.
* Please note: Jumpido is only compatible with Windows 7 or newer
Engaging activities
Balloons
Pop all the balloons you think are wrong!
See the Balloons activity
Sorting
Whoa! Apples are falling from the sky! Make sure they fall in the right tree!
See the Sorting activity
Soccer
Jump, squat and kick your way to victory!
See the Soccer activity
Events
Copy the smart elf to win!
See the Events activity
Baskets
Solve problems with the magical baskets!
See the Baskets activity
Routes
Work together to help the hedgehogs go home!
See the Routes activity
Kinect in the classroom
Kinect for Windows is a next-generation motion-sensing device that enables you to control video games without touching a controller!
To play, users execute a series of body gestures. It also promotes physical activity in the classroom and helps break through
learning barriers with fun, energetic, and easy-to-play games!
"Kinect transforms ordinary classroom experiences into extraordinary immersive education"
- Cameron Evans
National and Chief Technology Officer for U.S Education, Microsoft
Learn more about using Kinect at school
Jumps
Hand raised
Squats
Sideways hand movements
Physical exercises
Kids love to jump around and have fun - we all know that. So why not harness all of that positive energy to fuel their desire for learning?
Children love to play together...
Research shows that games are an excellent educational tool. With Jumpido, students can use their math skills
to compete against each other. Or they can join forces and work together to smash problems and win!
At the end of a game, feedback is generated that is intentionally designed not to  be too detailed, and encourages
students to work together. Teachers can use this feedback to understand how they can help pupils improve their math skills.
"The gaming element significantly improves the motivation and the engagement of children in math classes"
- Nikolay Tsanev
PhD, Assoc. Prof, Sofia University, Vice Dean of The Faculty of Preschool and Primary School Education
Adaptive learning
Jumpido is a crazy world that's always changing! And there’s no chance of students getting bored because every time you play,
the problems are different. What's more, children of all abilities can play! Thanks to Jumpido’s adaptive content, difficulty
increases when pupils answer correctly and decreases when problems are answered incorrectly.
Learn more about adaptive learning
Engaging kids in the classroom
Nowadays, primary school children are digital natives - they were born to use smart technology!
This means traditional teaching methods just aren't going to cut it. Kids feel more at home with interactive content.
And with game-based learning, pupils can't complete game objectives without understanding the math.
That's why Jumpido makes learning mathematics effortless.
Learn more about game-based learning
Testimonials
“We recently started to work with Jumpido and I am happy to share that it has established itself as one of the preferred and most effective ways of classroom learning. We noticed significant improvement in the engagement and math skills of children. In my opinion, Jumpido is a great asset to every primary school. I strongly recommend it to anyone who is looking for ways to improve the results and the overall experience of students.”
- Mariana Zakova
School principal
“I like the program because it makes math interesting!”
- Stilko
7 years old
“Regular physical activity is important for children because it helps to prevent obesity and improves energy levels. The motions that Jumpido requires have been carefully selected in order to be safe for pupils. Playing Jumpido helps children to have an active lifestyle and enjoy mathematics.”
- Krasimir Rusev
Kinesiologist
“Jumpido represents what educational technologies should bring to the classroom - increased engagement of students, real value in terms of content and a product which is suitable for all students.”
- Ivan Ivanov
Advisor to the Minister of Education and Science, Republic of Bulgaria
“In 2011, the team behind Jumpido was the first company in Central and Eastern Europe to become partner of Microsoft's Technology Adoption Program (TAP) for Kinect for Windows. This recognition is offered to only the most motivated and innovative companies after a careful selection process. This makes Jumpido one of the pioneers of using the technology.”
- Teodora Varbanova
Education Industry Lead, Microsoft
Try Jumpido for free today!
for free or  See package prices
39,388
Happy Kids
326
Schools
517
Downloads
How to buy
Legal
Kinect quiz
History
Team
Careers
Partners
Press
Awards
Activities
All trademarks referenced herein are the properties of their respective owners.
Copyright © 2013-2018 Jumpido OOD. All rights reserved.
Design by Despark
Jumpido creates software for children which teaches math through interactive games. The software incorporates motion sensors, and in order to solve problems, pupils need to use physical gestures.
WHOLE PROJECT MARK
