Integra ® Association  ::: Projects ::: ABE CAMPUS - VIRTUAL ADULT BASIC EDUCATION COMMUNITIES IN EUROPE
===============================

<a href="../bg/homebg.html"><img src="../pics/bgflag.jpg" alt="Българска версия" width="24" height="17" border="0" class="flag"></a>
Home  |  About us  |  Projects  |  Useful links  |  Staff  |  Forum  |  Contact us
Български
##### Main Window #####
Being one of the most active NGOs in Bulgaria, since 2000 Integra &reg;  Association joint following international projects under
Lifelong learning program (2007 - 2013) ;
Socrates Program ;
Leonardo da Vinchi Program ;
Joint Action Program - Ldv,Socr,Youth ;
Community Action Program to Combat Social Exclusion 2002 - 2006 ;
ABE CAMPUS - VIRTUAL ADULT BASIC EDUCATION COMMUNITIES IN EUROPE
Socrates program, action MINERVA
September 2003-October 2005
Coordinator: Asociacion de Alumnos y Ex-alumnos AGORA, Spain
Partners form Sweden, Austria, Denmark, Bulgaria, Romania and Belgium.
Background
Realisation
Final products/Results
The project responds to the need to foster access to the information and communication technologies (ICTs) and to adult basic education (ABE) for those groups in society with lower levels of formal education and at high risk of social exclusion. To this aim the project will contribute to developing the educational application of ICTs within ABE.
The international team will produce a virtual environment specialized in ABE which will include seven ABE virtual campuses and a common space. This European virtual ABE community will contribute to the adaptation of ABE to the new needs of the knowledge society. This virtual environment and its contents will be developed in co-operation with ABE learners/participants, ABE professionals and experts in the educational applications of ICTs. A process of validation of the campuses and its analysis will provide a new understanding of the initial impact of the use of ICTs in ABE.
The project also includes activities to foster public debate on the use of ICTs in ABE such as Public Celebrations, Debates or  Weeks of the ICTs in ABE.
The main objectives of the project are:
To improve understanding of the impact of ICT on ABE, concretely, on
the organisation of teaching/learning needs,
the promotion of a more participatory role of all parties involved (above all the adult learners themselves) in the organisation and planning of the education provision,
collaborative and communicative learning based on NICT,
multicultural co-operation and cultural awareness,
the evolution of the role of the teachers and other professionals in adult education,
the learning processes and
fostering access to NICT and to distance learning of social groups, which face greater difficulties to participate in cultural and educational opportunities.
To develop a virtual adult basic education community (4 virtual campus for ABE) at European level combining on-site education facilities with distance and open learning through NICT.
To contribute to the creation of a European area of lifelong learning in which adult basic education is included as a strategy to include the disadvantaged groups.
To use the NICT to achieve the goal of quality education for all.
Final Products/Results
Some of the main tasks developed from the partners, during the period in which the project is working, are: interventions in the forum of the project regarding the technical development of ABE CAMPUS and the Digitalisation of Materials, creation and selection of the digital materials for the campus, dissemination activities as the creation of a database and others concrete activities, etc.
Would you like to test the developed platform and materials?
Vizit  http://www.basicampus.net/integra-bg/
and enter:
Username: test
Password: bulgaria
Summary of the steering meetings and the working sessions of the Bulgarian mixed commission
FINAL REPORT & CONCLUSIONS WHY AND HOW TO USE ABE CAMPUS
Guidelines_to_encourage_the_participative_use_of_NICT_in_ABE
IT course for ABE
Back to projects
Top
Copyright  &copy;    2003 - 2005      Webmaster:  webmaster@integra.bg
##### End Main Window #####
WHOLE PROJECT MARK
