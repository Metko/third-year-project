Quality You Can Trust!

For over 70 years, Aero Manufacturing has been an industry leader. We specialize in the manufacture of stainless steel sinks, tables, dishtables, cabinets, shelving, and custom solutions for different environments.

With a reputation for the fastest lead times, a comprehensive environmental policy, and cutting edge manufacturing techniques, Aero Manufacturing offers true reliability and quality you can count on.
We manufacture with the environment in mind.
Aero Manufacturing is the industry leader in stainless steel water conservation products

We understand how important water conservation is today. We're leading the way in the foodservice and plumbing industries with an extensive line of stainless steel products that meet all water conservation requirements without compromising performance. All of our models are manufactured to deliver the consistent flow you need, while maintaining the water and energy savings your industry demands. We're committed to conserving water through innovation.

At Aero Manufacturing, we're not only interested in what we make, but also in how we make it. Find out how we're making a difference!
GREEN POLICY
Aero Manufacturing Casework Configurator

Aero Manufacturing is proud to announce the launch of our new Casework Configurator. This simple drag and drop tool allows you to easily design your custom setup of casework cabinets. Simply browse the cabinets you want from several categories and drag them into your "project". When you've finished, fill out the simple 5 field form and we'll send you a fast, no-risk pricing quote.

Our CompanyCustomer Service

    About Us
    Contact Us

    CAD Library
    Green Policy

    Caring for Stainless
    Terms & Conditions

Visit us on Facebook

    Toll Free: 800.631.8378
    Phone: 973.473.5300

    Fax: 973.473.3794
    sales@aeromfg.com
From Our Family to Yours

 

Founded in 1946 by my grandfather Bernie, a Hungarian sheet metal mechanic, Aero Manufacturing started as a general sheet metal shop specializing in ventilation equipment. In 1956, my father Jerry, a Korean war veteran and mechanical engineer, joined the Aero team.  He used first-generation computer technology to introduce production assembly fabrication to Aero, and began producing stainless steel foodservice equipment.

 

As the third generation in my family to join Aero, I am honored to continue the legacy of innovation. We’ve evolved into an industry leader in the manufacture of commercial and consumer stainless steel products, including cabinets, sinks, shelving and tables and custom fabrication. Originally started in a 1,700 sq. foot general sheet metal shop, we are now headquartered in a 150,000 square foot facility on a 5-½ acre campus in Clifton, NJ, with an office in Dallas TX. These expansions have exponentially increased our stainless steel fabrication capabilities, and Aero Manufacturing is positioned to continue growing. 

 

I do not rest on my laurels, however, and continue to invest in the latest technologies in solid modeling, robotics, and lasers to keep Aero Manufacturing on the cutting edge of our industries. Aero is proud to lead the stainless steel fabrication industry in lead times and pricing, thanks in large part to the state of the art manufacturing technologies in our facilities. We stock thousands of standard stainless steel products for immediate shipment, making Aero the top choice for large providers of commercial stainless steel equipment buyers across the U.S. 

 
The Right Stainless Steel for Every Project

 

Our experience and commitment to customer satisfaction is the reason why professionals across a broad range of industries have come to rely on Aero Manufacturing for their stainless steel needs, including foodservice, healthcare, education, residential, governmental, architectural, construction, clean room, and industrial environments. We are one of the few facilities in the U.S. who are able to produce custom fabricated stainless steel products to fit any size project. Our experienced project managers and designers will work with you to make sure your custom stainless steel product is exactly what your project needs, and our manufacturing team will make sure that it meets your specifications.  I personally oversee our day to day business to ensure that all of our customers, big and small, are 100% satisfied with our product and our service. 

 
Our Commitment to Sustainability

 

My father and grandfather built this business with integrity, and I am committed to living up to the standards that they set. It is for this reason that Aero Manufacturing continues to employ hundreds of Union workers and keeps 100% of our manufacturing process located here in the U.S.A. We also firmly believe that protecting the environment is everyone’s responsibility, and we have actively implemented green business practices across all aspects of our company. To find out more about our eco-friendly manufacturing practices, check out our Green Policy.

 

We understand that you have a choice of who to partner with.  Aero will do everything possible to make sure that we are your “go to” fabricator. Please Contact Us to let us know how we can help you make your current project a reality with stainless steel. I look forward to hearing from you.

 

Wayne Phillips

President & CEO

Aero Manufacturing

WHOLE PROJECT MARK

