Hangaboutz Hammocks | Participant | Open Africa - Do Travel Differently
===============================

<![endif]
Home
Routes
Impact
News
Support
About
What We Do
Who We Are
How We Work
Our History
Contact
- Navigation -
Experiences
uMngeni Footprint Route
Hangaboutz Hammocks
Send Enquiry
Description
Map   Reviews
Enquire Now   Tell a Friend
Incorrect Details?
Hangaboutz Hammocks is owned and operated by Struan &amp; Blake Gilson out of Howick which is situated in the heart of the KwaZulu-Natal Midlands in South Africa.
We have all our material specially woven for us and it is then dyed according to our specifications.
The quality of our hammocks has become legendary and we often have repeat orders for people both locally and internationally.
Products for sale:
Hammocks;
Power Kites;
Kite Accessories; and
Stunt Kites .
Please enable JavaScript to view the  comments powered by Disqus.
You can enquire about bookings, rates, and availability by filling out the form below.
Your name
Your email address
Please enter a valid email address.
Your mobile number
Please enter a valid mobile number.
Your message
This field is required.
What's 10 divided by two?
Subscribe to our newsletter?
Send
To share this with a friend, please fill out the form below.
Your friend's name
Your friend's email address
Your friend's mobile number
You can optionally enter a message for your friend
If these details are incorrect, please fill out the form below to notify us.
Please detail the incorrect information for this record
Route Services
Previous
Next
Belongs to Route(s)
Contact Details
Name:
Struan Gilson
Mobile:   Click here
Website:   Click here
Blake Gilson
Contact Person:
Mobile Number:
+27 82 312 7604
+27 82 454 1169
Address:
Howick, KwaZulu-Natal
GPS Coordinates:
-29.488024, 30.230255
(Click to download GPX file)
Budget:
Tags
hammocks
kwazulu-natal
howick
kite-accessories
power-kites
Report Incorrect Details
Submit a Review
Send to a Friend
About Open Africa
Open Africa is a social enterprise that works with small businesses to establish rural tourism routes that offer travellers authentic experiences, while generating income and jobs for local people.
Link to Open Africa
Click here  to see how you can add a link to Open Africa on your website.
Featured Routes
Namaqua Coastal Route
Rixile Culture to Kruger Route
Southern Overberg Fynbos Route
Arid Eden Route
Latest news
Kalahari communities benefit through new tourism offerings
Why do you travel?
Best places to see snow in South Africa
5 autumn road trip ideas
5 Reasons to use a local tour guide
Three new hikes to explore in South Africa
Newsletter signup
Receive our quarterly update on new destinations and travel tips from Open Africa.
Go
Connect with Open Africa
Copyright &copy; Open Africa. All Rights Reserved.  Terms &amp; Conditions.
START GOOGLE REMARKETING TAG
END GOOGLE REMARKETING TAG
Open Africa is a social enterprise, which helps develop tourism routes together with rural people. It helps create jobs for entrepreneurial people by providing training and internships.
WHOLE PROJECT MARK
