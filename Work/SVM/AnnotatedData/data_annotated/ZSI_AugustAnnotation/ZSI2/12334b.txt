Over 30 million people have signed up for Bumble to start building valuable relationships, finding friends, and making empowered connections.

Creating new connections has never been easier. Bumble is working to lift the stigma of online dating by employing unprecedented standards for respectful behavior. Because of this relentless dedication, millions of people are using Bumble to build valuable relationships every single day.

DATE, MEET FRIENDS, NETWORK

Bumble is at the forefront of matchmaking technology by providing an app that allows users to foster more than just romantic connections. The industry-leading app empowers users to swipe through potential connections across three different modes:

Bumble: On Bumble, women make the first move. We’ve changed the archaic rules of the dating game so that you can form meaningful relationships in a respectful way.
Bumble BFF: Life is better with friends. Whether you’re new to a city or looking to expand your circle, Bumble BFF is the easiest way to make new friends.
Bumble Bizz: Now we’re in business. Use Bumble Bizz to network, find mentors, and create new career opportunities.


Bumble is the first app of its kind to bring dating, friend-finding, and career-building into a single social networking platform.

CHANGING THE RULES OF THE GAME

At Bumble, women make the first move. In heterosexual matches, the woman has 24 hours to make the first move and the man has 24 hours to respond. In same-sex matches, either person has 24 hours to make the first move, while the other individual has 24 hours to respond, or else, the connection expires. By prompting our users to be bold and make the first move we’ve seen over 3 billion messages sent to date.

THE BUZZ IS REAL

"Bumble exists to empower women..." (Fast Company)
"Bumble is just an app: but it's changing the discussion." (Wired)
"Bumble offers an alternative that prioritises meaningful connections, with women calling the shots:" (UK Sunday Times)
"Bumble rejects hate speech to make users feel safe on its dating app" (Texas Standard)
"What makes Bumble different from other dating apps...is its focus on giving women all the power. " (Business Insider)

---


Bumble is free to download and use. However, we also offer an optional subscription package (Bumble Boost) and non-subscription, single and multi-use paid features (BumbleCoins).

https://bumble.com/en/privacy
https://bumble.com/en/terms
