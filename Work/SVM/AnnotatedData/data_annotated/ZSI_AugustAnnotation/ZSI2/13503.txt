REHABILITÁCIÓS CENTRUM, CSÖMÖR
===============================

Principles &amp; main Services
Directors, Leaders
Report of the Limited-liab.
The Representation of the    Interest of the Disabled
Projects
Work Activities
Institutional Care with Housin Services
Health Care
Free Time Activities
Product Order/ Buy
Gallery
Our
Supporters
Links
Address
Press
Home
Principles and main services of the organizations
The Rehabilitation Centre
in Csömör is a combination of two organizations:  Alliance Rehabilitation
Nonprofit limited-liability Company for Public Goods , providing jobs,
and Foundation for Equal Rights!  , providing institutional care
for our clients. Both of the two organizations are working in accordance with
the principles of normalization. We think, disabled persons have to get all we
would think for a "normal" person. So we take particularly care about:
Homeliness
Client-centrism
Openness
Flexibility
Alliance Rehabilitation Nonprofit
limited-liability Company for Public Goods
This is the name of the successor of the Alliance Industrial
Cooperative (founded 22 years ago). It provides work for people with sever
disabilities (for mentally disabled persons, as well as for the physically
disabled). On its Csömör department we employ approximately 300 persons, while
in the rural branches we give work to 270 persons.
Activities:
Hand-made candle production,
Ceramics,
Needlework,
Cloth-mill,
Industrial crafts,
Gardening (growing garden plants),
Cleaning,
Cooking,
Maintaining
Stock-raising on our farm in Bakonykúti.
Foundation for Equal Rights!
It was founded in 2000; now it provides housing for 76 clients.
Institutions run by the Foundation:
Residential home  (2 buildings): a
community living together in a family-house like building, with an individual
room and a bathroom for each person, a common dining room (with the function of
a living room as well) and a common kitchen, all under a 24 hours supervision of
a helper. Home for the Disabled  (5 buildings): Similar
to the residential home, but each room with the bathroom has its own
kitchen. Integrated condominium : youngsters with
non-severe disabilities, and, if needed, their non-disabled family members can
live in flats on their own (we sell the flats in the condominium to them), with
the help (care), they need. Daily Care : Here we provide
help for youngsters with multiple, and/or severe disabilities, in improving
their skills needed to be employed at a sheltered
workplace. Service for supporting : Cooperating with the
self-government in the XVI. District of Budapest, we provide help and support in home care, and transporting the disabled living in this area.
Services
Health care : doctors, chief nurse, various
kinds of therapists,
for example: Massage, special gymnastics, BEMER
(electromagnetic field therapy), homeopathy, Hellinger family-therapy;
Developing education : planned,
systematic education and special education, skill-developing/improving program;
Psychological care:  Hellinger
family-therapy, personal psychotherapy, psychological advice (advice/suggestion given by
a psychotherapist)
Free time activities : clubs, hiking, chess
class, playback theater, music therapy, sports: football, table tennis,
jogging, holiday trips. In each years we have two great happenings: June
festival (a kind of summer party), and Harvest Ball.
The Baltazár Theatre , being also run by a foundation, is now a
relevant factor in Hungarian cultural life.
The son of
Erzsébet Szekeres was born in 1976, with multiple disabilities. Soon, she
recognized, that something has to be done, to make it possible for Tibor to live
a life of a human being. When her son became 6, she realized, that he is going
to be an adult some day, and by then, his future life has to be built. However,
similarly to all the other youngsters with disabilities, the conditions gave no
possibility to live a normal life by then. There were no workplaces, where they
could be employed, there were no schools for them, where they could be taught,
and there were no places (flats, houses), where they could live their adult
life. That was the moment, when she recognized, that the children, as well, as
their parents, have to fight for their rights, their place in the society, and
for the possibility to live a life of a human being.  In the first few years,
she spent a month abroad on each summer, where she could see other developing
projects like the one, she has built by now. 3-4 years later, she managed to
obtain the buildings of a kindergarten, previously owned by the local
self-government in Budapest. In 1982, on her way home from Budapest, she has
seen enormous unused pieces of land, owned by the government. She managed to
obtain 5 acres of these, to build up her project. In 1986, when the law for
small cooperatives came out, she established the Alliance Industrial
Cooperative, with 13 youngsters with disabilities, and 2 parents as employees.
This happened on the very first day of the law, using a quarter million HUF of
credit. By this, she started to build a countrywide network, aiming to help the
disabled, providing training courses, jobs, and housing services to them. At her
village, Csömör, she built a complex rehabilitation centre for people with
disabilities, which is also a centre of countrywide education, and provides
accommodation for the disabled.  By now, the Alliance Rehabilitation Ltd.,
legal successor of the Alliance Industrial Cooperative (both lead by the
founder, Erzsébet Szekeres) employs 570 people with severe intellectual or
physical disabilities on 16 departments/rural branches in 22 village/town in
Hungary. Foundation for Equal Rights! , also founded and lead by Erzsébet
Szekeres, provides institutional care and housing services for 76 persons with
disabilities in Hungary. These two organizations are in strong cooperation, due
to the fact, that their activities are complementary to each other.
E-mail:
otthon01@invitel.hu
Design: Artual
The centre helps the physically and mentally disabled by ensuring the possibility of employment. The Equal Rights Foundation provides them with accommodation.
WHOLE PROJECT MARK
