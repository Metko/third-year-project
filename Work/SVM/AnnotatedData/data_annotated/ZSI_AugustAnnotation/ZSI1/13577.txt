Shop now - Cyclescheme
===============================

<![endif]
Sign in to My Cyclescheme
img role="banner" src="/images/logo-cyclescheme.png" alt="Cyclescheme" class="png" /
Shop now
How it works
For employers
Help &amp; support
Blog
Ready to begin?
Let's get started
Enter your employer's code
If you know your employer hasn't registered with Cyclescheme you can
invite your employer .
Searching for your employer
Success, we've found your employer
Your employer has registered with Cyclescheme. You're all set to start your application.
Oops, we can't find your employer's code
If you know your employer is on the scheme please double check your code.
Alternatively, feel free to
invite your employer
to join.
Decide what you need   &amp; where you want to shop
Which bike should I go for?
Where can I get my package from?
Next steps
Road Bikes
Good for:  Long-distance commuting. Fitness riding. Sportives. Racing.
Hybrid
Good for:  Versatility. Anyone who just wants ‘a bike’. Hills, due to a wide gear range.
Folding Bikes
Good for:  The last leg of train commutes. Easy storage at home/work. Short journeys.
Mountain Bikes
Good for:  All kinds of off-road terrain. Sturdiness – they’re built to take abuse. Hills.
Roadster/Dutch Bikes
Electric Bikes
Good for:  Flattening hills and shrinking distances. Heavy loads. Less fit cyclists.
Touring Bikes
Good for:  Carrying panniers. Comfort. Tackling hills, due to wide gear range. Cycling holidays.
Trikes
Good for:  Anyone looking to enjoy cycling with the security that only three wheels can provide.
Cyclo-cross/Gravel Bikes
Good for:  Versatility. Comfortable and capable on bad roads and good tracks.
Show more options
Show less options
Any bike. Any brand. Anywhere
Over  2,000  retailers - in-store, online, click & collect and brand direct
If you want the benefit of face-to-face service, head to a nearby Cyclescheme shop. If you
already know what you want, or your local shop doesn't have what you are looking for, you might
be better off looking online.
Local retailers
Online retailers
Please Note: You can apply now and  pick your retailer later
Next Steps
About
Introduction
How Cyclescheme works
How to get started
Is it a Government Scheme?
Quick links
Offers
Earn Rewards
Cycle to Work Day
Why we do this
Advertising
Privacy Notice
Cookies Information Page
Contact Cyclescheme
Retailer Application
Retailer B2B
Our products
Cyclescheme
Techscheme
Interact
Facebook
Twitter
Instagram
Youtube
Vimeo
Google+
LinkedIn
We're proud of&hellip;
Fast track 100, 2009
Smarta 100, 2010
BikeBiz Best Retail Services '12, '13 & '14
ISO9001 Certification
&copy; 2018 Cyclescheme Limited, P.O. BOX 3809, Bath, BA1 1WX. Part of  Blackhawk Network .
It is a tax exemption scheme initiated by the UK government in the interest of promoting healthier environment and living.
WHOLE PROJECT MARK
