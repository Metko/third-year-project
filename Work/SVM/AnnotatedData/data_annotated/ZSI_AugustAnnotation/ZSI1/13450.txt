Habitat for Humanity Macedonia
===============================

InstanceBegin template="/Templates/eng.dwt" codeOutsideHTMLIsLocked="false"
Македонски
Learn About Habitat
What We Do
Get Involved
Donate
Faces and Places
InstanceBeginEditable name="leftsubmenu"
Projects
Micro loans
Home Improvement Fund
Roma Housing Program
Builds
InstanceEndEditable
Donate to Habitat
Join our mailing list
Online store
Contact us
Our network
Our partners
Our U.S. tithing partners
Corporate donors
<div id="leftbanner"><a href="http://www.moneygram.com" target="_blank"><img src="../images/moneygram.jpg" alt="Money Gram" width="160" height="61" border="0"></a></div>
<div id="leftbanner"><a href="http://www.sonyericssonwtatour.com" target="_blank"><img src="../images/wta.jpg" alt="Women's Tennis Association" width="160" height="80" border="0"></a></div>
InstanceBeginEditable name="leftcorner"   InstanceEndEditable
InstanceBeginEditable name="mainheading"
InstanceBeginEditable name="content"
Together with equal financial par we created the  Roma Housing Program . The program was designed as pilot in its initial year, with clear vision to be extended in the near future in many different ways.
OUR GOALS
The main goal of the partnership is to improve the housing conditions for the Roma community in one of the municipalities of the capital Skopje, through provision of micro-loans for reconstruction/renovation/repair. In line with the basic principles of Habitat for Humanity, other ethnic groups from the same municipality should not be excluded, as long as they comply with the loan conditions. Specific objectives of the project are development of credit line, which should:
be designed and adapted to the needs of a specific and sensitive target group;
provide financial sustainability of the credit line;
OUR TARGET
Roma/marginalized families served through loans disbursed
50 new reconstruction loans disbursed
Roma/marginalized families served through reconstructions completed
34 reconstructions/renovations/repairs completed
OUR PRODUCT
The Roma Housing Fund is implemented through credit line that offers loans from 500 – 1.700 Euros with maximum repayment period of 30 months. The families that are eligible for this phase must already be Horizonti customers.
©
2007 Habitat for Humanity® Macedonia. All rights reserved. &quot;Habitat
for Humanity&quot; is a registered service mark owned by Habitat for
Humanity International.
Home | Learn About Habitat | What We Do | Get Involved | Donate | Faces &amp; Places
Newsletter | Contact Us
InstanceEnd Habitat for Humanity Macedonia
InstanceEnd
WHOLE PROJECT MARK
