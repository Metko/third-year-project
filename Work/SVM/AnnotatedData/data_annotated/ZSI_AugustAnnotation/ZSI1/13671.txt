Casserole Club
===============================

Sign in
Suggest a Diner
Become a Cook
About us
Visit Casserole Australia
Doing something great with an extra plate
Featured by
Join Casserole Club
Casserole Club volunteers share extra portions of home-cooked food with people in their
area who aren’t always able to cook for themselves. They share once a week, once a month,
or whenever works best for them.
Be part of the community!
Share a plate
Cook an extra portion and deliver it to a neighbour who needs it.
Read more
Get a plate
Share the name of someone who could do with a home-cooked meal.
Spread the word
Let people know about who we are and what we do.
“The informality of Casserole Club makes it work. Pam lives about 5 minutes from me.
She’s very chatty and we have a laugh. She likes my pork, apple and apricot casserole.”
Maggie
Join the community on Facebook
/%p 27 of your friends like Casserole Club
Making a difference to real people
70%
of those receiving meals count their volunteer Cooks as friends.
80%
of those receiving meals wouldn’t have as much social contact without Casserole Club.
90%
of our volunteer cooks would recommend Casserole Club to a friend.
Sign up as a Cook
Are we are in your area? Search a postcode.
Facebook
Tweet
Email to a friend
More about us
Becoming a Cook
Becoming a Diner
Blog
/= link_to 'Become a partner'
FAQ
[email&#160;protected]
Help
Terms
For organisations
Casserole Club is a service created by FutureGov and provided with the support of local authorities and third-sector organisations.
/= link_to 'Bring Casserole to your community'
A product from
Futuregov
Privacy Policy
CC connects isolated people who find cooking difficult with neighbours who are able to share a portion of home cooked food with them.
WHOLE PROJECT MARK
