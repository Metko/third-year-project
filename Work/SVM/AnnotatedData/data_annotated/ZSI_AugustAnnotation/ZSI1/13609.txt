Urban Farm Lease | Living map of jobs innovators – BETA
===============================

<![endif]
BEGIN WAYBACK TOOLBAR INSERT
success
fail
NEXT/PREV MONTH NAV AND MONTH INDICATOR
Dec
FEB
Oct
NEXT/PREV CAPTURE NAV AND DAY OF MONTH INDICATOR
16
NEXT/PREV YEAR NAV AND YEAR INDICATOR
2014
2015
2016
4 captures
03 Sep 2014 - 09 Oct 2015
About this capture
COLLECTED BY
Organization:  Internet Archive
The Internet Archive discovers and captures web pages through many different web crawls.
At any given time several distinct crawls are running, some for months, and some every day or longer.
View the web archive through the  Wayback Machine .
Collection:  Survey Crawl Number 2 - Started Dec 17th, 2014 - Ended Jul 31st, 2015
The seed for this crawl was a list of every host in the Wayback Machine
This crawl was run at a level 1 (URLs including their embeds, plus the URLs of all outbound links including their embeds)
The WARC files associated with this crawl are not currently available to the general public.
TIMESTAMPS
END WAYBACK TOOLBAR INSERT
Living map of jobs innovators &#8211; BETA
A project from Nesta, the UK&#039;s innovation foundation
#branding
Skip to content
Home About Search by theme Search by development stage Search by evidence Search by location Add example Contact us
#access
#masthead
#header
&larr;  Voidstarter
#nav-above
Urban Farm Lease
Posted on   May 30, 2014   by   nestajobsinnovations      .entry-meta
Urban Farm Lease  is a matching platform and legal framework for farmers and building owners to create green jobs in our cities.
The Urban Farm Lease project was designed by TroPeople ASBL in Belgium. The idea is based around a team of agricultural engineers, a programmer and legal specialists who propose to develop a standard contract that ensures security for people who will run farming businesses on unused urban spaces. The project offers free training, connection and consultancy to young people for high-added-value urban techniques and food processing in order to fill the “winter gap” and provide a year-round workload.
Urban agriculture could provide 6,000 direct jobs in Brussels, and an additional 1,500 jobs considering indirect employment (distribution, waste management, training or events).
This innovation was one of the Winners of the  European Social Innovation Competition  2014 run by the European Commission with support from Nesta.
Share this: Twitter Facebook Like this: Like   Loading...
Related
.entry-content
This entry was posted in  1. Exploring opportunities and challenges ,  3. International ,  Blurring work and learning ,  L1 - Can describe benefits  and tagged  EU Social Innovation Competition finalist ,  young people . Bookmark the  permalink .											  .entry-utility
#post-##
#nav-below
Leave a Reply  Cancel reply
Enter your comment here...
Fill in your details below or click an icon to log in:
Email  (required)   (Address never made public)
Name  (required)
Website
You are commenting using your WordPress.com account.  (  Log Out  /  Change  )
You are commenting using your Twitter account.  (  Log Out  /  Change  )
You are commenting using your Facebook account.  (  Log Out  /  Change  )
You are commenting using your Google+ account.  (  Log Out  /  Change  )
Cancel
Connecting to %s
Notify me of new comments via email.
#respond
#comments
#content
#container
Search the living map
Search for:
Keywords apprenticeships
apps
BAME groups
business support
careers advice
childcare
colleges
community
crowdfunding
disability
employers
employment pooling
EU Social Innovation Competition finalist
ex-service personnel
grants
housing
hyper-flexible work
in-work support
incubators
internships
investment
job centre
loans
low income
mentoring
mobilising capital
networks
new markets
offending
older people
parenting
part time
play
procurement
recruitment
resilience
schools
showcasing skills
social impact bond
subsidies
supporting growth sectors
technology
training
volunteering
women
work experience
young people Select by category
Development stage
1. Exploring opportunities and challenges
2. Generating ideas
3. Developing and testing
4. Making the case
5. Delivering and implementing
6. Growing and scaling
Evidence standard
L1 &#8211; Can describe benefits
L2 &#8211; Can show positive change
L3 &#8211; Can show causality
L5 &#8211; Have systems to ensure consistent replication
Location
1. City / regional
2. National
3. International
Theme
21st century employability
Blurring work and learning
Creating and shaping new markets
Intermediaries that improve matching
Supporting entrepreneurship and enterprise
Uncategorized
Enter your email address to get new posts by email:
Follow Nesta on Twitter
From the horses&#039; mouths: hear directly from the US @ CitiesOfService  officers mentoring @ CitiesServiceUK    bit.ly/1J05GTy  @ Lillie101   4 hours ago
The economic trickle-down theory is substantially wrong in parts, explains Nesta’s @ geoffmulgan    bit.ly/1J7CeeA   #inequality   8 hours ago
Smart lessons for the UK from @ jaideepprabhu  &amp; @ naviradjou &#039;s Frugal Innovation   bit.ly/1yoc6ka   #frugaleconomy   http://t.co/D9lH7Pnvxg   11 hours ago
What isn&#039;t the purpose of  #education ? Every vision needs limits &amp; clarity so what’s out of scope? asks @ OliverQuinlan    bit.ly/1yo7SJh   1 day ago
Learn about Volition, volunteer programme by @ ManCathedral , this Sunday 7am @ BBCRadio4   2 days ago
#primary .widget-area
#main
#site-info
The Twenty Ten Theme .				 Blog at WordPress.com .
#site-generator
#colophon
#footer
#wrapper
wpcom_wp_footer
Follow
Follow &ldquo;Living map of jobs innovators - BETA&rdquo;
Get every new post delivered to your Inbox.
Join 53 other followers
Build a website with WordPress.com
#bitsubscribe
#bit
%d  bloggers like this:
Provide an online platform and consultancy for property owners and unemployed youth to take advantage of the large surfaces available for agriculture in Brussels. They will also train the youth.
WHOLE PROJECT MARK
