Bike2Work - Smart choices for commuters
===============================

SiteMap Contact
About
Campaigns
Cycle-friendly employers
Public Policies
News
Resources
Internal Area
Bike2Work
Smart choices for commuters.
Employers support Bike2Work: Get ideas!
Bike2Work campaigns
Behavior-change campaigns from European countries
See how Bike2Work is promoted around Europe
CfE Certification
Contact us
Find out how to certify your company.
Find out what countries are already doing to encourage employees to take up sustainable mobility as part of Bike2Work.
For more information, find contact details for the managing partner (European Cyclists' Federation) who will be happy to answer your questions.
More
About Bike2Work
The Bike2Work project actively promotes a significant energy-efficient modal shift from motorized transport to cycling. To achieve this goal, it encourages employers to introduce behavioural change programs that shift commuters habits to take up more sustainable forms of transport.
Besides leading to extensive energy savings and CO2 reductions, Bike2Work has huge potential to improve employees personal health and reduce money spent on transport.
Tweets
Tweets by @Bike2Work
The project is co-funded by the Intelligent Energy Programme of the European Union.  The sole responsibility for the content of this project  lies with the authors. It does not necessarily reflect the opinion of the European Union. The European Commission is not responsible for any use that may be made of the information contained therein.
Map
Search
Profile + Notifications
Lost password
Bike2Work is an EU co-funded project lead by the European Cyclists Federation. The main objective of Bike2Work is to encourage a significant modal shift from motorised commuting to cycling.
WHOLE PROJECT MARK
