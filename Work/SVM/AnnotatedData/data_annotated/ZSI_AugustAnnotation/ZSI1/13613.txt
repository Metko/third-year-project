Telemedicine through Mobile
===============================

<![endif]
START: Container
START: header
<div class="rhombus"></div>
Menu
Search
Basket
Shqip
My World
Business
Vodafone E-Bill
a class="btn btn-sml" title="Sign in" href="https://ebill.vodafone.al/ebilling" style=" margin-bottom:10px;">Sign in</a
div class="hr">
<h5 style="margin-bottom: 10px;"><span style="color:#e60000;">Previous</span> Vodafone E-Bill</h5>
<a class="btn btn-sml" title="Sign in" href="https://www.vodafone.al/ebill/login.php" style=" margin-bottom:10px;">Sign in</a>
</div
li><a title="Forgot your password?" href="https://ebill.vodafone.al/ebilling/nonAuth/ForgotPasswordLnk.action"><i class="i-arrow-right-black"></i> Forgot your password?</a> </li
Sign in to E-Bill
E-Bill Support
START: Main Nav
Careers
Foundation
Media relations
Social Responsibility
Added in include_js/lib/lib.min.js
<script type="text/javascript" language="javascript1.2" src="https://www.vodafone.al/templates/NEModules/SearchModule/SearchModule.js"></script>
<script type="text/javascript" language="javascript1.2" src="https://www.vodafone.al/templates/NEModules/SearchModule/ajax_search.js"></script>
START: Nav Secondary
_return_main_menu
Close
END: Nav Secondary
<h5><span style="color:#e60000;">Previous</span> Vodafone E-Bill</h5>
<a class="btn btn-em btn-sml" title="Sign in" href="https://www.vodafone.al/ebill/login.php" style=" margin-bottom:10px;">Sign in</a></div
END: header
START: main
NEW BANNER
START: nav-vertical
Projects and Impacts
Archive
About the Foundation
Foundation News
Contact Us
END: nav-vertical
Telemedicine is an initiative that uses the latest technology in the service of health care of the remote areas population. Designed and funded by
Vodafone Albania Foundation , the project performs free medical tests (oxygen in the blood, cholesterol, triglycerides, diabetes, tension arterial, EKG and respiratory test /spirometry) for the most vulnerable groups, including people with disabilities, people with chronic disease and elderly to over
310 villages  across the country. Testing apparatuses are synchronized for immediate delivery of test data to the hospital, where the specialist doctor provides an opinion on each case which enables the exchange of health information useful for diagnosis, treatment and prevention. During the first two years, Telemedicine is present in those rural areas where there are no health centers. An estimated total of over
58,500 medical tests  for more than
11,800 people  who have been advised by a doctor in real time thanks to the possibility that technology offers today.
Over 40%  of the total tests results are identified as risk cases for further examination, which are followed by the family doctor.
END: main
START: footer
Site links
Vodafone Albania
Vodafone Albania Foundation
Social Responsability
Public Policy
About Vodafone
Most popular
Vodafone Club
Standard bundles
Roaming
Internet
Vodafone One
Support
Configurations
Services
Vodafone apps
Get in touch
Vodafone Shops
Customer Care
Sitemap
Terms &amp; Conditions
Privacy Statement
©
2018
Vodafone Albania. Registered office: Autostrada Tiranë-Durrës, Rr. "Pavarësia", Nr.61, Kashar, Tirana, Albania.
END: footer
.spring
.wrapper
END: Container
Javascript async load
WHOLE PROJECT MARK
