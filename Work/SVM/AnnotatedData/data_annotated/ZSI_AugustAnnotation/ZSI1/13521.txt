Integra ® Association  ::: Projects ::: MEIN – Migration empowerment integration network
===============================

<a href="../bg/homebg.html"><img src="../pics/bgflag.jpg" alt="Българска версия" width="24" height="17" border="0" class="flag"></a>
Home  |  About us  |  Projects  |  Useful links  |  Staff  |  Forum  |  Contact us
Български
##### Main Window #####
Being one of the most active NGOs in Bulgaria, since 2000 Integra &reg;  Association joint following international projects under
Lifelong learning program (2007 - 2013) ;
Socrates Program ;
Leonardo da Vinchi Program ;
Joint Action Program - Ldv,Socr,Youth ;
Community Action Program to Combat Social Exclusion 2002 - 2006 ;
MEIN – Migration empowerment integration network
Lifelong learning program
October 2007 – September 2009
Coordinator: Paritatisches Bildungswerk, Germany
Partners from Italy, Spain, Austria, Lithuania and Bulgaria
Background
Realisation
Final products/Results
Education is not only a human right but also functions as a vital key to successful integration.  Governmental measures which realize the first stages of integration stop short of reaching the goal.  What are often missing are continual counseling services and further education programs that specifically target the needs of migrants and that enable them to control their own individual integration process.  Precisely such programs are lacking in many countries of the European Union.  Hence, the goal of the transnational project M.E.I.N. is to develop a new instructional concept for intercultural adult education and social work with migrants across Europe.
M.E.I.N. – Migration Empowerment Integration Network – is a new further education and training concept for the education and counseling of migrants.  What is so innovative about it? The concept builds upon actual life situations migrants may find themselves in as well as their potentialities and possibilities.  Assistance and counseling to migrants are provided in such a way that they will want to and independently be able to develop their competencies in their professional and everyday life. Moreover, instructional courses are provided that offer new learning concepts and contents. The goal is a sustainable and social integration of migrants.
Using a self-learn approach, participants of this program will acquire key skills in the following areas:
intercultural instruction;
methods of empowerment;
identification of skills and competencies;
application of case management concepts;
methods of self-directed learning.
Final Products/Results
You can find reports, learning materials and further information on the project M.E.I.N. at  www.meinproject.eu .
Back to projects
Top
Copyright  &copy;    2003 - 2005      Webmaster:  webmaster@integra.bg
##### End Main Window #####
The Network for Integration of Migrants through Empowerment aims to educate and train migrants by using real world scenarios in which migrants find themselves as well as ways of exploiting opportunities that are available to them.
WHOLE PROJECT MARK
