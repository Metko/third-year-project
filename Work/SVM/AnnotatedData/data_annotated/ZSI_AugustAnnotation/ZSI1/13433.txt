Bullying Awareness R.O.S.
===============================

Bullying Awareness R.O.S. Search this site
Home Bullying Awareness: Reclaiming Our Schools© About Us Contact Us Developments &amp; News In-School Awareness Campaign Teacher and Support Staff Resource Book Upcoming PD and Student-Centered Workshops Bullying Related News &amp; Research Bullying?  What is Bullying Anyway? CBC Learning &amp; BA:ROS Community Relationships and BA:ROS Effective Communication and Conflict Resolution LGBTQ Issues &amp; Awareness Mentoring &amp; BA:ROS Positive Coaching and Team Building Restorative Justice and Community Building Supporting Mental Health Teacher Resources Lesson Plans Sitemap
Home
Bullying Awareness: Reclaiming Our Schools © , is a comprehensive teacher and support staff resource, an in-school awareness campaign, and a series of student-centered and professional development workshops, that was co-created and co-authored by two Ontario Certified Teachers, Vanessa Hamilton, OCT and Jeff Reati, OCT, as a means of educating people on the reality of the adverse, and often long-term, effects of bullying on our students and families, schools, and greater communities.  Through education and the regular integration of bullying awareness into classrooms and curriculum, we aim to establish an awareness of bullying behaviour in all of its forms, while providing sound strategies for the implementation of education and intervention in schools, and tangible, teacher friendly lesson plans and resources.   Bullying Awareness: Reclaiming Our Schools ©  is supported by the Ontario Ministry of Education, the Ontario Teachers Federation, The Ontario College of Teachers, Thames Valley District School Board, amongst other local, provincial and national programs and initiatives.  For information on additional supporters and partners with BA:ROS, please see Community Relationships and BA:ROS.
To access our comprehensive teacher and support staff resource, click on the "Teacher and Support Staff Resource Book" under the drop down menu on the left!  Please feel free to e-mail any feedback, suggestions, and experiences you have had with the resource!  We appreciate your support!  
 
Č Updating...
Ċ Current Bullying Statistics in Ontario 2012.pdf   (65k) VH &amp; JR,  Oct 31, 2012, 3:10 PM v.1 ď
Comments
Sign in | Report Abuse | Print Page | Powered By   Google Sites
WHOLE PROJECT MARK
