Hyllie - Malmö stad
===============================

Kris
Just-nu
sektionsToppMeddelande
Script vars
breadcrumbs  eri-no-index
eri-desc
Start
Nice to know about Malmö
Technical visits
Theme Sustainable City
Sustainable Urban Development
Hyllie
/eri-desc
/eri-no-index
Page content starts here   Mittenspalt   Bild 1  photo: Perry Nordeng
Rubrik  Hyllie
Ingress  The ambition of Malmö's biggest development area is  to create a mixed city full of life  and to become a role model for climate smart living. 
Text  During a visit we will show the expansion of Hyllie, a future-oriented part of town with the potential to promote growth in both Malmö and the Öresund region. Ideas to expand southern Malmö have been entertained since the 1960s. Yet it was only in connection with the decision about the City Tunnel, and thus a new train station in Hyllie, that expansion plans began in earnest. Fully developed, Hyllie will boast nine thousand homes and the same number of new jobs. Energy supplies to Hyllie will be purely renewable or recycled energy and the energy system will be based on so called smart infrastructure. Ease of commuting and communications associated with urban development, green surroundings and a southern Swedish landscape make Hyllie an attractive center for residents, visitors and businesses.
Text 1  This technical visit involves: Current plans and current projects in Hyllie center - generally about the development of the area   Possible focus areas: The city landscape  - a unique development concept with green qualities as a basis. (Key words: Landscape Architecture, Architecture, Urban Planning)  How do you sell a place that does not exist?  - a joint effort by all the area's stakeholders. (Key words: On-site marketing, Branding, Businesses, Marketing)  Malmö's biggest growth area  - Hyllie as an important growth factor in the region. (Key words: Society development, Growth perspectives)  Hyllie's vision  of becoming the most climate-smart city area in the Öresund Region and a benchmark model for the rest of the world. (Key words: Environmental strategies, Climate issues)  Hyllie's green areas and public spaces  (Key words: Urban environmental programs, Green areas, Landscape architecture, Lighting)  Hyllie as a communications center  (Key words: Transport, Infrastructure)  Practical info location:  Hyllie Info Center ,  Malmö Arena, Hyllie Boulevard 26  l ength:  1-2 hours   type of study:  lecture and tour     max number of participants:  50 people on lecture, 20 on tour website: hyllie.com Indicate which focus area you are interested in when filling in the booking form. Make a booking request
Page content stops here   Fakta
Dotlist
Läs mer
Ja eller nej-fråga
Meddelandeformulär
Berätta vad vi kan göra bättre på den här sidan:
Vi har ingen möjlighet att svara, men dina synpunkter är värdefulla för oss.
Om du vill ha svar på en fråga hittar du  kontaktuppgifter här .
Starttagg för att stoppa sökindex.  eri-no-index
menu-js  Sustainable Urban Development
Western Harbour
Sluttagg för att stoppa sökindex.  /eri-no-index
Page content starts here   Kontaktruta   kontaktruta-stadsomraden
Kontakta oss
Studiebesöksverksamhet/Study
Skriv till oss
E-post:
studiebesok@malmo.se
Namn:*
Telefon dagtid:
Meddelandet avser:*
Felanmälan
Fråga
Annat
Meddelande:*
Vill du vara säker på att vara anonym rekommenderar vi att du istället ringer till oss.
Page content stops here
Senast uppdaterad  Senast ändrad:  2016-08-15 16:38
Nyheter  Nyheter
News
Råd för demokrati och trygghet (31/5)
Anlagd brand på Gånglåtens förskola (31/5)
Halverad restid till Köpenhamn med Öresundsmetron (30/5)
Skolavslutning – fest, frihet och tyvärr vissa risker (31/5)
Uppdatering kring branden på Fosiedals förskola (31/5)
moreNews
Fler nyheter...
Händer i Malmö  Händer i Malmö
Sugen  Sugen att hitta på något? I vår evenemangskalender hittar du massor av tips om vad som händer i Malmö just nu - både för besökare och Malmöbor.
Länk Evenemangskalendern
Till Evenemangskalendern
Kul i Malmö  Kul i Malmö
Gratis  Gratisaktiviteter för barn och unga – året om.
Länk Kul i Malmö
Till Kul i Malmö
Besök oss gärna på  Besök oss gärna på
Social menu
Malmö stad
Jobba i Malmö stad
@malmostad
Våra bloggar
Här finns vi också
Mer på malmo.se  Mer på malmo.se
More menu
Felanmäla något i staden
Anslagstavla
Göra ett studiebesök
Hitta bostad
Karta
Lediga jobb
Press
Ny i Sverige
A-Ö
Script
GDPR-consent
eri-no-index
Page content starts here   Logo
Brödtext  Välkommen till Malmö stad och malmo.se Vi vill att malmo.se ska fungera så bra som möjligt för dig som besökare. För att kunna ta reda på vad vi kan göra bättre analyserar vi hur våra besökare använder webbplatsen och använder webbkakor (cookies).
Mer om hur vi arbetar med kakor och personuppgifter på malmo.se   Rubrik
Mer om hur vi arbetar med kakor och personuppgifter på malmo.se
+
&ndash;
Denna information finns också att läsa på sidan  Om webbplatsen , som du når via en länk i sidfoten . Webbkakor (cookies) på malmo.se På malmo.se använder vi webbkakor (cookies). En webbkaka är en liten textfil med information som lagras på besökarens dator för att förbättra webbplatsen för dig som besökare. Vi använder webbkakor för att analysera hur webbplatsen används, genom att webbkakorna till exempel håller ordning på hur snabb webbplatsen varit för dig och vilka sidor som besökts. Läs mer om webbkakor och vilka som används på malmo.se på sidan  Om webbkakor (cookies)  som du når via en länk i sidfoten . Personuppgifter på malmo.se Ett IP-nummer är din unika adress på internet. IP-numret används för att information som skickas över internet ska nå fram till rätt plats. Det enskilda IP-numret är ointressant för oss och vi avidentifierar all information i så stor utsträckning som möjligt. Den information vi har i våra systemloggar sparas endast i syfte att webbplatsen ska fungera bra. Om du fyller i formulär på malmo.se sparas den information du valt att uppge i våra systemloggar, för att säkerställa att informationen når fram. Information om hur dina uppgifter hanteras får du i samband med att du använder det specifika formuläret. Informationen rensas från våra systemloggar var sjätte månad. Mer information om dina rättigheter Dina rättigheter regleras i dataskyddsförordningen (DSF/GDPR).  Du kan läsa mer om förordningen på Datainspektionens webbplats .
Klientscript
Jag förstår och vill fortsätta mitt besök på malmo.se
Vizzit-tag start
Vizzit-tag end
The project will be a most sustainable district which will use efficient means of renewable energy and provide the most climate friendly environment to the inhabitants.
WHOLE PROJECT MARK
