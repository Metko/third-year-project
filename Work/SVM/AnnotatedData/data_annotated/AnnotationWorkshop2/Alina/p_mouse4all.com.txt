---------------------------------------
NEW PAGE: Mouse4all - Mouse4all

Mouse4all - Mouse4all

 info@mouse4all.com   

 Twitter Facebook   

   Testimonials Support Quick-start guide Frequently Asked Questions (FAQ) About us Contact 

 Testimonials Support Quick-start guide Frequently Asked Questions (FAQ) About us Contact 

 Mouse4all  

 Accesibilidad para Android 
Utiliza tu tableta o tel fono sin tocar la pantalla  

 Ver V deo   

			Mouse4all		 

 Diego   

			2017-07-24T12:03:14+00:00		 

 Mouse4all - Lanzamiento comercial - 11 Julio 2017 (corto) 
 What is Mouse4all? It is an accessibility solution that allows the use of an Android tablet or smartphone without touching the screen. It enables access to Internet, social networks, games and any application. 
 Mouse4all works with a connection box and an Android app. It is very simple to install. It can be used with one or two switches, a trackball mouse or a joystick. 

 For whom? It targets persons with physical disabilities that find it difficult to work with a touch screen: cerebral palsy, spinal cord injuty, tetrplegia, multiple sclerosis, ALS or neuromuscular disease. 
 Mouse4all improves the quality of life of its uses by boosting their autonomy, privacy and personal development. 

 Benefits for our users 

 Digital revolution for all 
 Thanks to Mouse4all you can use exactly the same WhatsApp, Facebook, YouTube and Instagram as your friends and family. 

 Personal autonomy 
 You are in control. Do you want to play a game? Browse the Internet? Read an e-book? Listen to flamenco music? 
 Explore the apps in Google Play. Many of them can work with switch control or with a mouse. 

 Privacy 
 We all have the right to privacy. 
 No need anymore to dictate messages to a support person. Send your messages by yourself without any supervision. 

 What our users say 

 I can now communicate with my family and friends without any issue. 
 Dani ,  user with cerebral palsy 

 Tablets and Mouse4all have opened a window to the world for them. 
 Eva ,  Occupational Thrapist at APAM (Madrid) 

 We did not even dream that our users could access tablets and smartphones. 
 Mila ,  Speech Therapist at APAM (Madrid) 

 Mouse4all is an assistive technology product with a user-centered design. 
 Clara and  ngela ,  Therapists at CEAPAT (Madrid) 

 Features 

 Aumented Cursor 
 Visual help with crossing axes, to easily identify the position of the cursor on the screen. 
 Up to two switches 
 Switch Control with one or two switches (3.5 mm connector). Reuse your switches. 
 Trackball and joystick 
 Compatible with most USB input devices in the market. Use your preferred one. 
 Customizable 
 Adjust timings and scanning speed to your needs. Choose colour and size of the cursor. 
 Battery-less 
 The Mouse4all box does not require a battery. It is powered through the connection cable from your Android device. 
 Easy to install 
 You can start using Mouse4all in a couple of minutes following the instructions in the quick-start guide. 

 Specification 

 Connection box (48 x 47 x 16 mm) 

 Two standard 3.5 mm connectors to plug in one or two switches. 

 One type A USB connector to plug in an adapted mouse, a trackball or a joystick. 

 Requires Android device 4.4 or higher. 

 Free app downloadable from Google Play. 
 Comprar 

 Partners 

 Fill in your email address and we will keep you updated about Mouse4all . 

 Nombre 

 Email 

 Comprar | Contacto    | Qui nes Somos 
 Mouse4all  2016  |  Condiciones Generales de Uso   y   Pol tica de privacidad y Cookies   

 Twitter Facebook   

 Utilizamos cookies propias y de terceros para mejorar nuestros servicios. Si continua navegando, consideramos que acepta su uso. Ok Leer m s



About us

Mouse4all is a startup created tby José Ángel Jiménez Vadillo and Javier Montaner Gutiérrez to change the world. At least a tiny little bit of it. We are engneers and we like to apply technology to solve relevant problems.  We enjoy creating hardware (the Mouse4all box) and software (the Android app), but watching our happy users playing with our inventions is what we like most: their joyful faces when they choose the video they want to watch, or when they receive and read in their privacy a WhatsApp message, or when they shoot a selfie all by themselves!. They are our greatest motivation to move forward.

We are also social entrepreneurs, since we cannot only live on motivation. Till now we have funded Mouse4all with our work and savings, but our objective is to make it a sustainable business in the long term. After two years developing, testing and optimising the product working together with users ans therapists, we officially launched Mouse4all in the summer of 2017.

Our challenge is that the next social network that will appear after Facebook, Instagram or WhatsApp is accessible for all from the first minute. Are you also motivated by this vision? Tell the world about us and help us to make it real.
Acknowledgements

    Winners “Vodafone Innovation Prize 2016” in the category Physical accessibility
    Third Prize “Printing Real Lives 2017”, Saxoprint España.
    We are currently selected among the 30 semifinalists of  “The European Social Innovation Competition 2017” oganised by the European Comission.

Partners

Mouse4all would not be possible without the users, families and therapists that have helped us from day one, testing, improving and validating the iterative versions of the product. They are the real heroes of this story. Thank you very much Rafa, César, Monchi, Ana, Julio, Ángel, Lourdes, Teresa, Eva, Mila, Clara, Ángela… and the tenths of persons that have helped us along the way.

Many thanks also to the centres and associatations that partner with us.


Quick-start
Video guide

With this video you can easily start using Mouse4all. In just 4 minutes, we explain how to plug in the connection box and how to download and install the app. Then we show the first steps to work with a switch.
Video Thumbnail
Mouse4all - Guía rápida de puesta en marcha (modo pulsador)

In the video we show, step by step, how to set up Mouse4all to use an Android tablet and smartphone with a switch.

We explain how to install the app, the initial configuration and the connection of the cables. From this point, things become very simple. Next time that you want to use Mouse4all, you just need to plug the box and go (plug&play)

Once you have watched the previous video you can go on with the video tutorials: short videos that answer specific questions about the use and configuration of Mouse4all.

 

    Short video tutorials about the use and configuration of Mouse4all

Quick-start Guide

Visual guide in PDF format to print in paper or watch on the screen. It explains, step by step, how to start Mouse4all in switch mode or in mouse mode.

 WHOLE PROJECT MARK
