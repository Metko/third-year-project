---------------------------------------
NEW PAGE: About Mattecentrum – Mattecentrum, gratis läxhjälp i matematik för alla

About Mattecentrum   Mattecentrum, gratis l xhj lp i matematik f r alla

 Meny 
 Meny 

 Mattecentrum, gratis l xhj lp i matematik f r alla 

 Mattecentrum, gratis l xhj lp i matematik f r alla 

 R knestugor 

 Hj lp till 

 Bli volont r 

 Ge en g va 

 Bli medlem 

 Engagera ditt f retag 

 Bli projektledare 

 Om oss 

 Om oss 

 About Mattecentrum 

 Verksamhet, verktyg och systerf reningar 

 Bli medlem i Mattecentrum 

 FAQ 

 Konvent 

 Styrelse, ledning och dokument 

 Mattecentrum i media 

 Lediga tj nster 

 Du ska k nna dig trygg hos Mattecentrum 

 Kontakt 

 Kansli 

 Projektledare f r st der 

 Press 

 Finansi rer 

 Bli ett st df retag! 

 Huvudsponsorer 

 F retagssamarbeten 

 Stugv rdar 

 Mattecentrums v nner 

 Fonder och stiftelser 

 G vor fr n privatpersoner 

 Nyheter och blogginl gg 

 Stugv rdar 

 Matteboken 

 Formelsamlingen 

 Mattecentrum p  Twitter 

 V r sida p  Facebook 

 V r instagramsida 

 Mattecentrum p  Youtube 

 Mattecentrum p  Linkedin 

 About Mattecentrum 
 Mattecentrum is a Swedish non-profit organisation  aiming  to stimulate students' interest in mathematics and increase their mathematical understanding. By giving our students confidence through positive experiences with mathematics, we hope to help them to a good education and career , in the future, a rewarding career . 
 Mattecentrum was founded in 2008. Our mission is to work for equal opportunities to learn, improve students knowledge in math and stimulate their interest in mathematics. Today Mattecentrum operates both online and offline. All of our services are free of charge for the user/participant. 
 Mattecentrum are financed by   public funds ,   corporate sponsors , foundations , and by   donations from  individuals . 
 Math labs 
 We arrange tutoring sessions, so-called  maths labs , in cities all over Sweden, where students are offered help with their homework by volunteers. Everyone is welcome, regardless of their knowledge, curriculum or school. Today Mattecentrum arrange math labs in over 30 cities, and in total we have over 100 math labs every week. Every month the math labs are visited by 5.000 students. 
 Click here for a complete list of cities and math labs 
 www.matteboken.se 
 The online tutoring site  www.matteboken.se  we have gathered theory, video lessons and interactive exercises to all math courses, from year 3 in compulsory school to upper secondary school.  www.matteboken.se  is also offered in an Arabic language edition:  arabiska.matteboken.se . 
 www.pluggakuten.se 
 www.pluggakuten.se  is an online forum for everyone who is studying mathematics and other subjects in compulsory school, upper secondary school and at universities. 
 www.formelsamlingen.se 
 On  www.formelsamlingen.se  you will find formulas for mathematics, physics and chemistry. 
 Math is fun! 
 Mattecentrum strongly belives that mathematics is fun: it's fun to learn, to know and to experience math together with others. 
 Every year we collect as many volunteers as possible and create big exam caf s in cities all over Sweden just before the national exams in mathematics. The national exams is obligatory for all students in upper secondary school in Sweden. At the exam caf s we help around 2.500 students (the number is growing every year) to prepare and practice for the exams. 
 In 2015 we set a new Guinness World Records for the largest math class with 3.611 participants. 
 What we do and why we do it 
 Education is the foundation that a skilled workforce must be built on. In the future we will see a shortage of graduates from the higher scientific and technical educations, and a shortage of skilled workers within the vocational area. If we want Sweden to be competitive in the future, we need a workforce consisting of the brightest minds and hands. They are the ones who will to develop new medicines, new technologies and in the long run create growth and development for their employees and for a sustainable Sweden. 
 Through Mattecentrums math tutoring and online tools we wish to increase young people s math skills and make them interested in developing their potential to a maximum. We help everyone, regardless of whether the student is struggling or is looking for a challenge. 
 Volunteers 
 All of Mattecentrums volunteers have a high professional standard and enthusiasm for mathematics. They want to share their knowledge of and passion for mathematics with others and they have at least studied mathematics in upper secondary school and lots of them have chosen a mathematical path within studies and work. Mattecentrum recruits, organizes and guarantees the quality of the help. In order to guarantee the quality of our math labs, we conduct interviews with all new volunteers, to be sure of their abilities as well as their commitment to the sharing of knowledge. 
 Are you good at math and are native speaker of another languuage than Swedish?  Become a volonteer at Mattecentrum!   Click here for contact information. 

 Dela p  Facebook 

 Alla kan hj lpa till! 
 Vill du hj lpa Mattecentrum att hj lpa fler barn och unga med matematik? Kul! Det finns flera s tt du kan g ra det p . All hj lp beh vs! 

 Bli volont r 
 Bli medlem 
 Ge en g va 
 Bli sponsorf retag 

 Mattecentrum har 90-konto och kontrolleras av  Svensk insamlingskontroll . Mattecentrum  r medlem i FRII och f ljer  FRIIs kvalitetskod. Mattecentrum  r med p   Givarguidens gr na lista  som trygghet f r dig som givare. 

 V ra huvudsponsorer 

 R knestugor 

 Arboga 

 Boden 

 Bor s 

 Falun 

 G vle 

 G teborg 

 Halmstad 

 Helsingborg 

 J nk ping 

 Kalmar 

 Karlskrona 

 Karlstad 

 Kiruna 

 Kungsbacka 

 Landskrona 

 Link ping 

 Ludvika 

 Lule 

 Lund 

 Malm 

 Norrk ping 

 R ne 

 Sandviken 

 Skellefte 

 Stockholm 

 Sundsvall 

 S dert lje 

 Ume 

 Uppsala 

 V ster s 

 V xj 

 rebro 

 stersund 

 Hj lp till 

 Bli volont r 

 Ge en g va 

 Bli medlem 

 Engagera ditt f retag 

 Bli projektledare 

 Om oss 

 Om oss 

 About Mattecentrum 

 Verksamhet, verktyg och systerf reningar 

 Bli medlem i Mattecentrum 

 Konvent 

 Styrelse, ledning och dokument 

 Mattecentrum i media 

 Lediga tj nster 

 Finansi rer 

 Huvudsponsorer 

 F retagssamarbeten 

 Fonder och stiftelser
 WHOLE PROJECT MARK