---------------------------------------
NEW PAGE: MELAWEAR | Fairtrade clothing

MELAWEAR | Fairtrade clothing       

 Navigation 
 Shop Fairtrade clothing Fairtrade clothing Ansvar Service Custom printing Custom production Responsibility GOTS at mela wear Fairtrade at mela wear Sustainable textile production Organic cotton HiPsy mela wear About Research   Development Press Deutsch Shop Fairtrade clothing Fairtrade clothing Ansvar Service Custom printing Custom production Responsibility GOTS at mela wear Fairtrade at mela wear Sustainable textile production Organic cotton HiPsy mela wear About Research   Development Press Deutsch     mela wear | Fairtrade clothing 
 Fairtrade clothing MELAWEAR. Organic and fair. Mela is Hindi for  acting together,  a core value of our Fairtrade certified and environmentally-friendly clothing line. We believe in living up to our name, so we make sure that everything we do is environmentally friendly, good for our partners, and of course, Fairtrade. At MELAWEAR we handpick our partners and visit their sites regularly to ensure that they create according to our values. We are stringent in ensuring that our Fairtrade and GOTS certification is in order, and work to ensure our partners do the same. Discover MELAWEAR products. WHERE TO SHOP MELAWEAR 
 Can t find your favorite store listed as one of our partners? 
 Talk to them and suggest stocking MELAWEAR as their newest sustainable fashion brand. OUR OFFER Fairtrade clothing made in India 
 MELAWEAR. Quality and design. MELAWEAR is a Fairtrade collection of clothing created out of organic cotton. Comprised of t-shirts, hoodies and backbacks, our collection offers a wide range of colors and styles. Compliance with environmental and social standards are key in the selection of our producers. Discover the MELAWEAR collection Custom printing 
 MELAWEAR. Customized and unique. Custom printing will make your garment unique, and MELAWEAR offers high quality screen-printing using organic colors. Compliant with GOTS certification, our printing partners are experts in their field who care about organic clothing just as much as we do. Discover more about custom printing MELAWEAR. Full of color   joy. Our aim at MELAWEAR is to help everyone experience the joy of wearing organic and Fairtrade clothing! A  mela  is an Indian festival where, farmers, merchants, and everyday citizens celebrate together and establish important personal relationships for future business arrangements. Together with our Indian partners, we want to share the spirit of a mela with our customers in Europe. CERTIFICATES Certified Fairtrade clothing MELAWEAR. Fair and transparent. All MELAWEAR suppliers are certified by Fairtrade Germany. Our partner factories and their suppliers are audited by FLO CERT on an annual basis to ensure that every step in the creation of our clothing adheres to the Fairtrade standard. MELAWEAR is a registered licensee of Fairtrade Germany. The Fairtrade Association works hard to improve living and working conditions in developing countries. Learn more about Fairtrade The GOTS certificate MELAWEAR. Natural and organic. The Global Organic Textile Standard (GOTS) is widely regarded as the world s leading standard for textile production from organic fibers and yarns. It sets high ecological and social standards for all elements of textile production, and GOTS criteria are considered higher than most other standards. Compliance with GOTS is enforced through an independent end-to-end certification process, and MELAWEAR s garments are audited on a regularly to ensure GOTS compliance. Learn more about GOTS Address mela wear GmbH 
Ladenzeile am Campus 
Scharnhorststrasse 1 
21335 L neburg 
Germany Phone: +49.4131.927.9757 
E-Mail:  info@melawear.de Contact us 

 Name* 
 Email* 
 Subject* 
 Message* 

 Contact mela wear GmbH 
Phone: +49.4131.927.9757 
E-Mail:
 info@melawear.de Address:  Ladenzeile am Campus 
Scharnhorststrasse 1 
21335 L neburg 
Germany  To our wholesale shop 
 Toggle the Widgetbar 

         Newsletter 

 Shop Fairtrade clothing Service Responsibility Legal details Terms and conditions Data privacy statement mela wear

--------------------------
Our Vision

MELAWEAR. Joined in dedication.

We are convinced that the economy can and must play an active role in improving environmental and social conditions around the globe. We at MELAWEAR are dedicated to working towards significant social and environmental change in the production of clothing.

Our business model is inspired by the personal relationships and friendships with our Indian and German partners that are at the heart of our business.
Our Goal

MELAWEAR. Active and shaping.

The founding team of MELAWEAR wants to build bridges between Europe and emerging markets to foster the creation of Fairtrade and organic products. To us transparency in the production and quality of our goods is key.

We want to offer a high-quality product range that caters to various needs. Timeless designs and colors characterize MELAWEAR’s collection, offering a stylish alternative to the conventional fashion industry.

Discover MELAWEAR products
------------------------------
mela“ is Hindi and means „acting together“. It is our aim to contribute to a sustainable development in the textile industry through our way of conducting business. Therefore research and development takes an important part as research and development embody important drivers for sustainability innovations. In the following we present parts of our collaborations and research partners:

Acting together…

… with Leuphana University Lüneburg

MELAWEAR was founded in the scientific environment of the Leuphana University Lüneburg. Since then the University and the young enterprise maintains close contact that is characterized by a high degree of cooperation. Hence, mela wear played an essential role in the appointment of the Leuphana University as one of four Fairtrade Universities in Germany.

The scientific collaboration enables both students and researchers from the university to gain unique experiences in the entrepreneurial context of a sustainable enterprise like MELAWEAR. At the same time scientific papers are written that bring a significant added value for both the company and the university research.

 WHOLE PROJECT MARK
