---------------------------------------
NEW PAGE: Home - Simprints

Home - Simprints

          Menu

 Technology 
 Projects 
 About 
 Partners 
 Careers 
 Contact 

          Over 1.1 billion people worldwide         

 lack formal identification, preventing access to essential services for the people who need them most 

 We re building open source software and biometric hardware to empower mobile tools used by researchers, NGOs, and governments fighting poverty around the world. 

 We re building open source software and biometric hardware to empower mobile tools used by researchers, NGOs, and governments fighting poverty around the world. 

 Technology 
 Projects 
 About 
 Partners 
 Careers 
 Contact 

      Biometrics for Development     

 Mobile Durable Fast Secure 
 We are a nonprofit tech company building low-cost, rugged fingerprint scanners for frontline workers. Our technology integrates seamlessly with the mobile platforms used across healthcare, finance, education, and beyond. 

        Find out more       

        How Simprints works       

              Simple user interface guides collection             

              Frontline worker places a beneficiary's finger             

              System runs matching algorithm             

              Unique ID links to records and next steps in care             

      Projects     

              Maternal health             

              Aid Distribution             

              Microfinance             

              Education             

              Immunisations             

              Data Collection             

      Our partners     

          Find out more         

          Meet the Team         
 Passionate. Driven. Creative. We are determined to change the world. 

              Find out more             

        The Chesterton Tower, Chapel Street, CB4 1DZ, Cambridge, UK. 
          2016   2017 Simprints. All rights reserved. Website by  Runway .

---------------------------------------
NEW PAGE: Contact us - Simprints

Contact us - Simprints

          Menu

 Technology 
 Projects 
 About 
 Partners 
 Careers 
 Contact 

          Contact us         

 We are based in a beautiful 14th century rectory in Cambridge which we have turned into a Technology 4 Development Hub 

 Technology 
 Projects 
 About 
 Partners 
 Careers 
 Contact 

 Simprints 

 The Chesterton Tower 
Chapel Street 
Cambridge, UK 
CB4 1DZ 

 Want to work together? Click here . 

 Research or academic partnerships? 
 research@simprints.com 

 Press and media enquiries? 
 media@simprints.com 

 Any other inquiries? 
 info@simprints.com 

        The Chesterton Tower, Chapel Street, CB4 1DZ, Cambridge, UK. 
          2016   2017 Simprints. All rights reserved. Website by  Runway .

---------------------------------------
NEW PAGE: Careers - Simprints

Careers - Simprints

          Menu

 Technology 
 Projects 
 About 
 Partners 
 Careers 
 Contact 

          Careers         

 Join us, and redefine what's possible 

 Technology 
 Projects 
 About 
 Partners 
 Careers 
 Contact 

      Our Team     

 Life is short. So we believe work should be awesome, meaningful, and fun. 

          What we do: building tech that matters         
 No matter what role you take, from writing code to running field deployments, you re playing a crucial part in improving lives in the poorest parts of the world. Because at Simprints all paths lead to a common goal: building incredible technology to fight disease and global poverty. Your challenge is to help us get there. 

          What s it like to work at Simprints?         
 At Simprints we don t have time for  boring . Instead we re building a team of the most passionate, adventurous, and creative people you can imagine and giving them the space to take on global challenges. This is what going to work means for us. 

      6 Reasons to work for Simprints     

          1         

 Travel the world 

 We work on the frontlines of global health. With a growing global network of partners our work takes us from the dusty savannahs of Zambia to the narrow slums of Dhaka, or the pristine beaches of Benin to the political corridors of DC. No assignment, or trip, is ever quite the same. And we make sure that every role from software engineer to communications lead gets a chance to travel with the team and see our impact up close. 

          2         

 Freedom and flexibility 

 We understand that brilliant people hate pointless rules and hierarchy, and want the freedom to do great work in their own way. This is no 9-5 job. We work incredibly hard, but have a lot of control about where and when. We care about results. Join a fast-paced Scrum culture with unlimited vacation and no fixed hours. Throw in our free gym memberships and you can build the work day that works for you. 

          3         

 Learn like it s your job 

 Because here it is. Self-directed learning objectives, external mentors, and regular 360 feedback keeps continual learning at the heart of our talent strategy. And we put our money where our mouth is with an additional  1,600 on top of your annual salary available for any courses, trips, or projects that power your personal and professional development. Join us and you will grow faster and learn more than in nearly any other job. 

          4         

 Unique office 

 A lot of tech companies brag about their office. But no-one else we know can boast a 14th century castle, actually the 3rd oldest building in Cambridge. However, don t let the stone walls fool you. Inside, we ve built a fully equipped 21st century office with powerful computers, prototyping facilities, 3D printers, and tons of creative space. We re working with ARM and the Centre for Global Equality to build this into a  Tech4Dev Hub  with regular hack nights and great workshops. 

          5         

 Amazing team 

 We re recruiting the best and brightest from around the world. We know  A  players attract  A  players, make it in and you ll be working alongside colleagues who truly share your drive and ability. And effective   dull. From our legendary  Lego Days  where once a month you can work on ANY tech projects you want novel sensors, Arduino powered trains, you name it to offsites and team outings, we love to work hard and play hard. We also know the best sources of Bavarian beer in Cambridge. 

          6         

 Purpose 

 Above all, underlying the diverse backgrounds, interests, and perspectives our team brings to their work, we hold a shared purpose. We believe that every person counts no matter the place or circumstances they were born to. So we re fighting to improve global health and development through better tools, better technology, and better systems. We re taking on the biometrics industry to build open-source tools for frontline community health workers. It s incredibly challenging. But real lives are on the line. Join us, and do work that really matters. 

      How to join the team     
 We are looking for smart, passionate people ready for the challenge of a lifetime. We also very occasionally consider unsolicited applications for other roles. Our selection process typically includes technical tests, phone calls, face-to-face interviews, and  trial days . Our goal is to really get to know you, and give you an equal chance to really get to know us. Make it in, and you ll be part of a team committed to seeing you succeed. 
 We also respect every applicant s time. Our team will respond to every application, successful or not, and we aim to get back to 95% of applications within 1 week. 
 Current Positions Available: 

              Software Engineer 
 Deadline: Rolling 

                Find out more               

              Electronics Engineer 
 Deadline: Rolling 

                Find out more               

              Field Support Engineer 
 Deadline: Rolling 

                Find out more               

      Volunteer     
 Not looking for full-time work but still want to get involved? Join our volunteering email listserve, where engineers, professionals, and students receive updates about exciting projects where you can contribute. We frequently get together over pizza on week nights to solve pressing challenges in global health   development. Our volunteers are working on range of exciting projects spanning mobile development, SMS messaging, Web APIs, market sizing, global health reports, and more. 

 Volunteer work is hard but rewarding, just ask the two volunteers who spent a week with us in Zambia working alongside our team! If you have a passion for combining technology with worthy causes we would love to meet you 

          Join the list here         

        The Chesterton Tower, Chapel Street, CB4 1DZ, Cambridge, UK. 
          2016   2017 Simprints. All rights reserved. Website by  Runway .

---------------------------------------
NEW PAGE: Projects - Simprints

Projects - Simprints

          Menu

 Technology 
 Projects 
 About 
 Partners 
 Careers 
 Contact 

          Projects         

 Empowering NGO's, governments, and businesses to fight poverty 

 Technology 
 Projects 
 About 
 Partners 
 Careers 
 Contact 

 The  Sustainable Development Goals  call for the end of extreme poverty and preventable newborn deaths. To achieve these goals we need to be able to accurately measure who we have reached and critically who we haven t. Simprints  system overcomes these challenges by ensuring accurate identification of any individual, regardless of access to formal ID. 

        Maternal health       
 99% of maternal deaths occur in developing countries, the vast majority of which are preventable. The World Health Organisation recommends a minimum of four check-ups before birth, however only 39% of all mothers are receiving these visits due to challenges in identification and accountability. Biometrics provide a powerful tool to identify patients, instantly finding the right record with the tap of a finger. Simprints is working with  BRAC  in the slums of Dhaka to ensure every mother and child have access to care. 

        Education       
 Teacher absenteeism is a huge bottleneck in delivering education to all, especially poor and marginalised communities. Research from MIT has shown that attendance monitoring can cut absenteeism over 20% and boost graduation rates by 40%. A simple fingerprint can be the difference between students learning and empty classrooms. Simprints is working with the  Impact Network  in Zambia to improve attendance tracking for teachers and students, ensuring access to education is a reality for the communities they serve. 

        Data collection       
 Mobile data collection has massively improved the speed that researchers and NGOs can gather critical data on the front lines. However, the identification bottleneck reduces quality and prevents organisations from consistently linking respondents across time. Biometrics can help bridge that gap with robust, reliable data collection in even the most far-flung communities. Simprints is working with  Dimagi  and  Possible  in rural Nepal to link their 30,000 beneficiaries across mountainous communities with a consistent ID. 

        Microfinance       
 Microfinance has penetrated nearly all corners of the developing world. However, screening borrowers who take multiple loans remains a key industry challenge, helping drive the market collapses in Bosnia, Nicaragua, and Andhra Pradesh. Randomised control trials by Yale have shown that biometrics can increase repayment rates from 44% to 85% among high-risk borrowers, and research at the Cambridge IfM suggests Simprints could help drive down interest rates and save microfinance institutions $25m a year. 

        Immunisations       
 An overwhelming body of evidence shows that immunisations are one of the most successful and cost-effective health interventions ever known. Yet still in the developing world 1.5 million children die of vaccine preventable diseases every year, accounting for 17% of deaths in children under 5 years old. Simprints is partnering with  VaxTrac  in Benin to design better biometric hardware and software for NGOs and Ministries of Health fighting to save lives through better vaccination coverage. 

        Aid Distribution       
 Developing countries lose billions of dollars a year to corruption and the misdistribution of aid. A World Bank study in 2011 found that only 41% of grain handed out for the poor through Indian food programs reached its intended target, and estimates put the loss of other subsidies at 20% costing the government $10B a year. But what if every claim required a fingerprint taken at the point-of-distribution? Simprints is partnering with NGOs and policy makers to fight corruption and make sure aid reaches its intended targets. 

      Get in touch     
 Why not send us a message about your project, we d love to work with you. 

          Contact Us         

        The Chesterton Tower, Chapel Street, CB4 1DZ, Cambridge, UK. 
          2016   2017 Simprints. All rights reserved. Website by  Runway .

---------------------------------------
NEW PAGE: Partners - Simprints

Partners - Simprints

          Menu

 Technology 
 Projects 
 About 
 Partners 
 Careers 
 Contact 

          Partners         

 We collaborate with leading global organisations 

 Technology 
 Projects 
 About 
 Partners 
 Careers 
 Contact 

      Sponsors     

      Partners     

 BRAC is the world s largest NGO, running health, education, finance, and other programs that improve hundreds of millions of lives globally. BRAC supported the very first field tests of Simprints technology, and we closely partner with their health and research teams on biometric-empowered mobile healthcare projects in the slums of Dhaka. 

 The Global mHealth Initiative at Johns Hopkins University (GmI) develops responsive innovations and provide rigorous, evidence-based support for mobile ICTs to improve global health with a focus on resource-limited settings where the global burden of disease and mortality are highest. 

 The idea behind Simprints originates from an ARM sponsored Hackathon and we have been working closely with them ever since. They provided our very first significant funding ($150k) and their embedded engineers have helped us develop the fingerprint collection platform we used to collect 120,000 fingerprint images in 2015. We also co-authored a chapter for a World Economic Forum Report. 

 SEARAN is the maker of dotstack TM , an ultra small Bluetooth stack that enables new classes of low cost and low power embedded devices. SEARAN is powering Simprints  connectivity across multiple platforms with their robust, Bluetooth SIG qualified stack. SEARAN provides free software kit and exceptional, experienced and expedient hands-on free support to Simprints developers. 

 An incredible team of SMART has helped us develop wireframes for our app to make it as user-friendly and intuitive as possible. Currently, they are drafting UX/UI user flows that we will take with us to Bangladesh and Nepal to test our hardware and software in the field (total of 35 designer days). 

 Kemp Little is a leading UK law firm specialised in the field of technology. In 2017, among other awards, it was named  Commercial Technology Law Firm of the Year  in the UK. With their support, Simprints has been able to successfully navigate the huge challenge of deploying complex projects in some of the world s most challenging legislative environments.  

 Fen Technology Ltd provides electronics product design support. Fen s IoT and instrumentation experience was essential in bridging the critical connectivity challenges in this project. Integrating Fen s development team with Simprints allowed us to maintain ownership, while bringing Fen s considerable product development experience to bear on taking an idea from concept to production. 

 therefore are internationally renowned, London based, product designers and engineers. They are the only consultancy to win the D AD  Yellow ,  White , and coveted  Black  pencil awards for product. therefore s strategic support for Simprints includes concept creation to user led ergonomics and detailed design for manufacture of the fingerprint scanner. 

      Supporters     
 We are very grateful for the generous support we are receiving from the following companies. 

          Join us         
 Do you want to support our mission? Feel free to reach out, we are always looking for great new partners. 

              Get in touch             

        The Chesterton Tower, Chapel Street, CB4 1DZ, Cambridge, UK. 
          2016   2017 Simprints. All rights reserved. Website by  Runway .

---------------------------------------
NEW PAGE: About - Simprints

About - Simprints

          Menu

 Technology 
 Projects 
 About 
 Partners 
 Careers 
 Contact 

          About         

 Let's work together to change the world 

 Technology 
 Projects 
 About 
 Partners 
 Careers 
 Contact 

      We are Simprints     

 Simprints is a nonprofit tech company from the University of Cambridge that seeks to break the identification bottleneck. Recognising a significant gap in current biometric technology, our team is building an affordable, secure, rugged, open-source fingerprint system that works in the world s toughest settings. 

      Values     

 Relentless Commitment to Impact 

 Simprints exists for one purpose: impact. It drives our culture and evidence-based approach. No matter how crazy things get, we never forget why we do this. 

 Robust as Fudge 

 We set super high standards because quality matters. We never cut corners when it comes to our beneficiaries. We build to last. 

 Be Surprisingly Bold 

 We push boundaries and challenge the assumptions others take for granted. We are not afraid of being shameless for social impact. 

 Get Back Up 

 When you take on great challenges, failure is inevitable. That doesn t scare us. The minute we get knocked down, we get straight back up. Every time. 

 Make it Happen 

 We do whatever it takes to fight poverty. We figure out a way forward, even if it has never been done before. There is no instruction manual for impact, so we are creative and relentless. 

 Don't be a Jerk 

 This journey is not about us or our egos. We put mission first, then team, then self. We stay humble. 

 Confront the Grey 

 We speak up when our ethical compass is challenged. We practice transparency and radical candor. We believe that action requires integrity, and integrity requires action. 

 Laugh Together, Grow Together 

 This journey is tough, but we have each other s backs. We remember to have fun and to laugh at ourselves. We never stop learning. 

        Recognition       

 Simprints wins $2M to prevent maternal deaths in hardest-to-reach world regions 

                  Read More                 

 Best Tech: Changing Children's Lives for Good at the 2015 Europas 

                  Read more                 

 Our founders listed on Forbes 30 Under 30 Social Entrepreneurs 

                  Read More                 

 Our team was declared Business Weekly's 2015 Startup of the Year 

                  Read More                 

 Our CEO was selected as 2017 Social Entrepreneur of the Year 

                  Watch More                 

      Meet the team     

                Alex Grigore               

                Dan Storisteanu               

                Tristram Norman               

                Toby Norman               

                Julia Kraus               

                Sebastian Manhart               

                Helen Lundebye               

                Christine Kim               

                Ridwan Farouki               

                Etienne Thiery               

                Karina Arrowsmith               

                Angela Au               

      Advisory board     

 Ken Banks 

 Founder of Kiwanja.net, creator FrontlineSMS, and a National Geographic Emerging Explorer. He is a PopTech Fellow, a Tech Awards Laureate, an Ashoka Fellow and has been internationally recognised for his technology-based work. In 2013 he was nominated for the TED Prize. Author of  The Rise of the Reluctant Innovator . 

 Darrin Disley 

 CEO of Horizon Discovery Group and 2015 Cambridge Business Person of the Year. He has a track record of securing over $320 million business financing from grant, angel, corporate, venture capital, and public market sources as well as closing over $500 million of product, service, licensing, and M A deals. 

 Sankar Krishnan 

 CEO of Tesseract Consulting, Sankar focuses on the nonprofit and developmental sector in Asia. A former partner at McKinsey   Company, he led their healthcare practice for India and Greater China. He is a strategic advisor to nonprofits and start-ups in the education, healthcare and technology spaces, and holds an MBA from IIM Ahmedabad. 

 Stephanie Yung 

 Creative director at design and innovation consultancy, Smart Design. Expertise in user-centered design practices and strategies to create positive impact and behaviour change in underserved communities. A leader of award-winning multidisciplinary teams in strategy, product development, and interaction, she s shaped the vision and experience solutions for a diverse set of Fortune 500 brands including Nike, BMW Mini, and Pepsico. 

 Alain Labrique 

 Founding Executive Director of Johns Hopkins University Global mHealth Initiative. Fellow at NIH mHealth Summer Institute. Labrique was recognised as one of the Top 11 mHealth Innovators in 2011 by the Rockefeller Foundation, and serves as an mHealth advisor to numerous institutions including the World Health Organisation, GSMA, USAID, and the mHealth Alliance. 

 Dominic Vergine 

 Head of Sustainability and Corporate Responsibility for ARM Holdings Plc. Board member of Electronics Technology Network and The Centre for Global Equality. Founding Director of Iceni Mobile Ltd, Aptivate, and Heligon. Builder of industry-leading teams in technology and international development. 

      Volunteers     

                Pawel Moll               
 ARM   

                Sam Briggs               
 Cambridge Consultants   

                Tom Daley               
 Aptivate   

                Katherine Bobroske               
 PhD Student   

                Jordan Hrycaj               
 mjh-it ltd   

                Alasdair North               
 viaLibri   

                Kevin Lemagnen               
 Qualcomm   

                Raghul Parthipan               
 Student   

                Odhr n Lenaghan               
 Salesforce   

        Resources       

 Read our report on legal models for social enterprises co-authored with Taylor Vinters 

                  Building a Social Enterprise: The Legal Landscape                 

 Read how biometric ID systems can break the identification bottleneck worldwide (published by Elsevier) 

                  Can Biometrics Beat Development Challenges?                 

 Read why development organisations should comply with key aspects of EU data protection 

                  The Case for Better Privacy Standards                 

          Join us         
 Want to get involved? 

              Find out more             

        The Chesterton Tower, Chapel Street, CB4 1DZ, Cambridge, UK. 
          2016   2017 Simprints. All rights reserved. Website by  Runway .

---------------------------------------
NEW PAGE: Technology - Simprints

Technology - Simprints

          Menu

 Technology 
 Projects 
 About 
 Partners 
 Careers 
 Contact 

          Technology         

 Identification tools for the world's harshest settings 

 Technology 
 Projects 
 About 
 Partners 
 Careers 
 Contact 

        How Simprints works       

              Simple user interface guides collection             

                Can be seamlessly integrated into any existing workflow and application               

              Frontline worker places a beneficiary's finger             

                Rugged, waterproof, mobile fingerprint scanner built for the toughest conditions on the frontlines               

              System runs matching algorithm             

                Offline or online. Rapid matching that reduces errors and improves accuracy, compliance and quality               

              Unique ID links to records and next steps in care             

                Fingerprint serves as a globally unique and secure ID that cannot be forgotten or lost               

            Fingerprint Scanner           
 Leveraging our research at the University of Cambridge, we are building fingerprint scanners that are 228% more accurate than existing mobile scanners in low-resource settings. 

            Mobile           

 Wireless   ergonomic scanner makes data a finger swipe away. 

            Durable           

 Water resistant. Shock proof. Built to withstand the toughest conditions. 

            Fast           

 Highly accurate, power-efficient, and fast identification anywhere. 

            Secure           

 Robust encryption. Our RSA protected records are safer than paper. 

 Rugged water resistant casing Bluetooth connectivity Long lasting battery Optical high accuracy sensor Ergonomic handheld design 

      Seamless Mobile Integration     

 Simprints uses standard Android calls and a Cloud-based RESTful API, making it compatible with web and Android 3.4+ based applications. Our cloud platform allows organisations to securely sync unique ID s across multiple devices. This allows seamless integration with nearly any system, including popular tools such as: 

        Robust Security and Privacy       

            TLS (SSL)           

 Secure communications between the phone   server 

            AES 128-bit           

 Encrypted data between the scanner   the phone 

            Anonymous           

 No personal details are stored alongside fingerprints 

            OAuth 2.0           

 Authentication keys are never passed over the web 

 Transparency is a core value to us. We voluntarily make our policies on privacy and data use public for all projects, and translate them into the local languages where we work. 

 Policy on Data Use 

                  Bengali                 

 Privacy Notices 

                  Bengali                 

 Privacy Impact Assessments 

                  BRAC 2016                 

                  Nepali                 

                  Nepali                 

                  Possible 2016                 

                  English                 

                  English                 

          Co-created with users         
 We have created the first fingerprint scanner designed for and by frontline workers. Simprints forges over 300 hours of user-centered design and co-creation into a seamless biometric workflow. Every feature, from the lights to the buttons, have been designed by users to meet their needs. Testing across 3 continents and 6 countries has shown that frontline workers can learn to use Simprints with less than 30 minutes of training. Our intense focus on real use cases and iterative field testing ensures that the technology adds tangible value to user s daily work, accelerating their impact on the frontlines. 

          Field tested         
 We have designed hardware and software for the worn, scarred fingerprints of the poorest populations that commonly defeat Western-designed biometric systems. With the help of our partners at BRAC and the University of Cambridge, we have tested Simprints with 125,000 fingerprints across Zambia, Benin, Nepal, and Bangladesh in both urban and rural contexts. We pitted a wide range of sensors, templates, and matching algorithms against the toughest fingerprints we could find to optimise our system in terms of power, speed, ruggedness, and affordability. 

              Find out more             

        The Chesterton Tower, Chapel Street, CB4 1DZ, Cambridge, UK. 
          2016   2017 Simprints. All rights reserved. Website by  Runway .
 WHOLE PROJECT MARK