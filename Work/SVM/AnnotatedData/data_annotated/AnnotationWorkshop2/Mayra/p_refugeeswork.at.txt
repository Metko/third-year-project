---------------------------------------
NEW PAGE: RefugeesWork.at - We connect refugees   employers in Austria.

RefugeesWork.at - We connect refugees   employers in Austria.

 EN  
 | 
 DE  

                            Online job platform 

                            for  refugees  in Austria 

                                    REGISTER FOR FREE 

 Menu 

 Refugees 
 Employers 
 Supporters 
 Login 

 For Refugees 

 We help you find work and connect with employers. It s free! 

 Get your resumee 
 We help you find work and connect with employers. It s free! 

 Preparation for job interviews   the labor market 
 We prepare you for job interviews and the labor market and inform you about further eduaction and promotional offers. 

 Find work 
 We suggest open positions to you that are in line with your skills, in order to help you find work. 

                        Note: Asylum seekers do not have free access to the labour market. However, under certrain conditions we match asylum seekers with volunteering positions or apprenticeships. 

 Register now 

 See the jobs 

 More legal information 
 Hide information 

 1.  For  asylum seekers  (refugees in a pending asylum procedure),   volunteering is one option to work  in companies and NGOs. The following conditions need to be met: 

                                        Max. of  3 months per year 

 Education as sole purpose 

 No compensation  of any kind 

 Note: Employers need to notify the AMS (2 weeks before start) 

 2. Recognized refugees   people granted subsidiary protection 

                                        have  free access to the labour market . 
They do not face any restrictions. 

 Employment of recognized refugees is  financially supported by the AMS. 
 Find more legal information here 

 + 275 
 Employers 

 + 4.500 
 Candidates 

 + 4.300 
 Likes 

 FOR EMPLOYERS 
 FOR EMPLOYERS 

 Discover talent, add more diversity to your team and meet people with extraordinary stories: 

 Adib, 24 
                                Financial analyst   accountant 

 Motassam, 21 
                                Student   cook 

 Fateme, 28 
                                Artist   painter 

 Obaida, 38 
                                Power engineer 

                    Find hidden talents 

                    Increase corporate diversity 

                    Improve soft skills of your employees 

                    Financial support due to subsidies 

                    Free training on the job   trial 

                    Foster integration   change lives 

 Service 
 We provide information on bureaucracy, public subsidies   concepts that support the organisational integration of refugees (e.g. culture, religion, communication). Moreover we prepair your candidates for the interview. 

 More information  

 Legal 
 Recognized refugees and people granted subsidiary protection have free access to the labour market. Only asylum seekers (people in a running asylum procedure) face restrictions. 

 More legal information 
 Hide information 

 Pricing 
 We provide solutions for employers of all sizes. Small organisations (up to 25 employees) get free access to the platform. After a succesful matching you can decide the price by yourself. 

 More information  

 1.  For  asylum seekers  (refugees in a pending asylum procedure),   volunteering is one option to work  in companies and NGOs. The following conditions need to be met: 

                                Max. of  3 months per year 

 Education as sole purpose 

 No compensation  of any kind 

 Note: Employers need to notify the AMS (2 weeks before start) 

 2. Recognized refugees   people granted subsidiary protection 

                                have  free access to the labour market . 
They do not face any restrictions. 

 Join  more than 275 employers  and benefit from the potential of refugees!  

 Learn more 

 Register now 

 Known from: 

 Our partners: 

 Awards   Nominations 

 Nominee 
 Nominee 

 Social Development Goals 
 We are committed to the social development goals of the United Nations 

 FOR SUPPORTERS 
 FOR SUPPORTERS 
 Do you support people who had to flee their country? Are you engaged in the asylum sector? Support us and inform refugees about our platform! 

 Infosheet for supporters 
 Do you support people who had to flee their country? Are you engaged in the asylum sector? Support us and inform refugees about our platform! 
 Download Infosheet 

 Flyer of Refugeeswork.at 
 Help us to inform refugees about our website by printing our flyer and sharing it with refugees. 
 Download Flyer 

 Support with the registration process 
 Support refugees to register on our platform and to create an online resume. Find a guide for the registration and the creation of resumes on our platform here:  
 Download guide for the registration 

 Create an email account  
 In order to register and communicate with employers, having a professional email address is essential Find a guide on how to create a free email account ("Gmail") here:  
 Download Guide 

 More legal information 
 Hide information 

 1.  For  asylum seekers  (refugees in a pending asylum procedure),   volunteering is one option to work  in companies and NGOs. The following conditions need to be met: 

                                        Max. of  3 months per year 

 Education as sole purpose 

 No compensation  of any kind 

 Note: Employers need to notify the AMS (2 weeks before start) 

 2. Recognized refugees   people granted subsidiary protection 

                                        have  free access to the labour market . 
They do not face any restrictions. 

 Employment of recognized refugees is  financially supported by the AMS. 
 Find more legal information here 

 Information for supporters 

                Become part of our network   receive exclusive information for volunteers   social workers in the asylum area. 

 e-mail address 

  Vienna, Austria 
   office@refugeeswork.at 
   +43 680 5010653 

 Links 

 Home 
 Refugees 
 Employers 
 Supporters 
 Press 
 About us 

 Contact Us 

                            We have received your message, we will contact you very soon. 

                            Oops! Something went wrong please refresh the page and try again. 

 Name 

 Email address 

 Message 

 Copyright   2017 Refugeeswork.at - All Rights Reserved. 

 Icons designed by Freepic 

 Home 
 About us 
 Press 
 Impress 
 Terms 

---------------------------------------
NEW PAGE: Terms | RefugeesWork.at - We connect refugees   employers in Austria.

Terms | RefugeesWork.at - We connect refugees   employers in Austria.

 BETA 

 EN  
 | 
 DE  

 Menu 

        Infos 

 Legal 
 Refugees 
 Employers 

        About us 

 About Refugeeswork.at 
 Press 
 Impress 
 Terms 

 Login 
 Sign Up 

 ALLGEMEINE GESCH FTSBEDINGUNGEN (AGB) 

 Refugee 
 AGB's anzeigen 

 Employer 
 AGB's anzeigen 

  Vienna, Austria 
   office@refugeeswork.at 
   +43 680 5010653 

 Links 

 Home 
 Refugees 
 Employers 
 Supporters 
 Press 
 About us 

 Contact Us 

                            We have received your message, we will contact you very soon. 

                            Oops! Something went wrong please refresh the page and try again. 

 Name 

 Email address 

 Message 

 Copyright   2017 Refugeeswork.at - All Rights Reserved. 

 Icons designed by Freepic 

 Home 
 About us 
 Press 
 Impress 
 Terms 

---------------------------------------
NEW PAGE: Information for employers | RefugeesWork.at - We connect refugees   employers in Austria.

Information for employers | RefugeesWork.at - We connect refugees   employers in Austria.

 BETA 

 EN  
 | 
 DE  

 Menu 

        Infos 

 Legal 
 Refugees 
 Employers 

        About us 

 About Refugeeswork.at 
 Press 
 Impress 
 Terms 

 Login 
 Sign Up 

 How it works 

 Einfach Stellen inserieren oder direkt nach versteckten Talenten suchen. 
Mit unserer Hilfe finden Sie neue MitarbeiterInnen, die genau zu Ihnen passen. 

 List vacancies 

  1. List vacancies 
 List traineeships, internships, apprenticeships and jobs within minutes using our online-forms. You can list as many vacancies as you like. 

 2. Receive applications 

                            We propose your vacancies to promising applicants, taking into account their language skills, professional background, asylum status etc. and informing you about every application. 

 3. Pre-Screen   get in touch 
 Check the applicants  profiles and contact your favourite candidates directly on our platform. An integrated calendar and our messaging system makes scheduling interviews and communicating with your applicants incredibly easy. 

 OR 

 Talentsearch 

 1. Search 
 Use our search engine to find hidden talents that match you expectations. Automatic prescreening will ease your search for promising candidates. 

 2. Get in touch 
 After prescreening the applicants profiles you can contact the most promising candidates directly on our platform. 

 3. Interview 
 Schedule an interview and meet your favourite candidates in person. 

            We are always there for you   Even after the successful interview: 

 Downloads of bureaucratic forms incl. help texts 

 Guides for bureaucratic procedures 

 Downloads of contract templates 

 Concepts for organisational integration 

 Concepts for communication   language 

 Templates for letters of recommendation 

 Pricing 

 Small 

 1-25 employees 

 Choose your price 

 Medium 

 26 - 50 employees 

  500  p. a. 

 Large 

 50 employees 

 Call us 

 Sign up now 

  Vienna, Austria 
   office@refugeeswork.at 
   +43 680 5010653 

 Links 

 Home 
 Refugees 
 Employers 
 Supporters 
 Press 
 About us 

 Contact Us 

                            We have received your message, we will contact you very soon. 

                            Oops! Something went wrong please refresh the page and try again. 

 Name 

 Email address 

 Message 

 Copyright   2017 Refugeeswork.at - All Rights Reserved. 

 Icons designed by Freepic 

 Home 
 About us 
 Press 
 Impress 
 Terms 

---------------------------------------
NEW PAGE: Legal | RefugeesWork.at - We connect refugees   employers in Austria.

Legal | RefugeesWork.at - We connect refugees   employers in Austria.

 BETA 

 EN  
 | 
 DE  

 Menu 

        Infos 

 Legal 
 Refugees 
 Employers 

        About us 

 About Refugeeswork.at 
 Press 
 Impress 
 Terms 

 Login 
 Sign Up 

 Are refugees allowed to work? 

 You need to make a difference between 1. asylum seekers as well as 2. recognized refugees and people granted subsidiary protection. 

 Asylum seeker 
 Recognized refugees and people granted subsidiary protection 
 Volunteering Positions 

 Asylum seekers 
 Asylum seekers have limited access to the labour market. Without work permit they are e.g. not allowed to recieve compensation. 

 Refugeeswork.at matches: 

 Traineeships / Volunteering positions   More information 
 Apprenticeships  (until the age of 25 years with work permit)  More information 
 Seasonal work  (with work permit)  More information 
 Easy tasks realted to their housing and charitable work for the governmelnt ("Bund"), the state ("Land")   communities ("Gemeinden") 

 Who are "asylum seekers"? 
 Asylum seekers = people in a pending asylum procedure. 
 Asylum seekers recieve a  white card , that proves their procedure is pending. 

 Recognized refugees and people granted subsidiary protection 
 Recognized refugees and people granted subsidiary protection have  free access to the labour market.  They do not face any restrictions just like Austrian or EU citizens. 

 Refugeeswork.at matches: 

 Internships 
 Apprenticeships 
 other employment relationships (e.g. paid jobs) 

 Who are "recognized refugees"? 
 Recognized refugees are people whos request for asylum has been granted ( refugees in the sense of the Convention and Protocol Relating to the Status of Refugees by the UN ). 
 Recognized refugees recieve :  "Anerkennungsbescheid"  or "Anerkennungserkenntnis",  convetion passport ("Konventionsreisepass")  and/or an  Identity card for foreigners  ("Identita tskarte fu r Fremde") 

 Who are "people granted subsidiary protection"? 
 People granted subsidiary protection are people who are not recognized as refugees (in the sense of the Refugee Convetion), but are in need of protection due to other reasons (e.g. torture, civil war). 
 People granted subsidiary protection recieven: a  "grey card" ,  "Anerkennungsbescheid"  or "Anerkennungserkenntnis",  passport for foreigners ("Fremdenpass")  and/or   identity card for foreigners  ("Identita tskarte") 

 Volunteering Positions 
 Conditions for a traineeship / a volunteering positions: 

 Limitation on  3 months  per year 
 Eductional character  (No normal working relationship) 
 No compensation  of any kind 
 Notification to the AMS  2 weeks in advance 

 Asylum seekers are allowed to do traineeships / volunteering positions in companies or NGOs after 3 months of the beginning of their asylum procedure (issue date of the white card). 
 3 month rule 
 Volunteering positions cannot last longer than  3 months per employer   per year. 
 Education 
 Educational character: 

 No or hardly minor or simple tasks ("blo e Hilfs- oder einfache angelernte Ta tigkeiten") or work on construction sides. 
 No obligation to work. 
 No fixed integration into the organisational structure (e.g. no obligatory working hours) 
 No tasks that (entirely or in parts) replaces another employee. (Otherwise the volunteering position might be a normal working relationsihp.) 
 Suggestion: Appoint a mentor, who supports the refugee and guarantees the educational character of the volunteering position. 

 Volunteering positions are merely  educational relationships  and not normal working relationsships or "occupational therapies". 
 Volunteering positions only serve the purpose to broaden and apply knowledge and skills for practice. 
 There needs to be connection between the education and work experience in the refugees homecountry and the content of the volunteering position, meaning that: The refugee shall learn something new which complements his education in the past. 
 Example: A syrian confessioneer learns during his volunteering position in an Austrian confectionery hot to make typical Austria sweets and cakes. 
 No compensation 
 Compensation is strictly forbidden at volunteering positions. Employers are not allowed to pay a salary or other benefits. 
 problem with compensations: The compensation is not only against the law and may lead to different consequences for the employer (fines etc.). 
 Any kind of compansation may be taken into account regarding the "Grundversorgung" of the asylum seeker, which might leads to a situation where the asylum seeker does not only lose "Grundversorgung" but also housing. 
 AMS Notification 
 Overview: 

 When:  2 weeks before the volunteering position shall begin 
 Where:  the AMS office competent for the region where of the employer 
 What:  AMS notification (according to  3 para. 5 AuslBG) and a notification of the competent insurance authority (AUVA Landesstellen) 
 How:  Downloading the forms on our platform and using our help texts and tips 
 Who:  Employer 
 How much:  A volunteering position for 3 months amounts to 45 EUR (incl. insurance and all public duties) 

 The notification shall enable the AMS to make a basic assesment of legitimacy  of the volunteering position. 
 If the volunteering position is  not prohibited within 2 weeks  the  volunteer can start . An explicit approval is not necessary. 
 If the AMS prohibits the volunteering position afterwards it has to be ended   latest within 1 week. 

  Vienna, Austria 
   office@refugeeswork.at 
   +43 680 5010653 

 Links 

 Home 
 Refugees 
 Employers 
 Supporters 
 Press 
 About us 

 Contact Us 

                            We have received your message, we will contact you very soon. 

                            Oops! Something went wrong please refresh the page and try again. 

 Name 

 Email address 

 Message 

 Copyright   2017 Refugeeswork.at - All Rights Reserved. 

 Icons designed by Freepic 

 Home 
 About us 
 Press 
 Impress 
 Terms 

---------------------------------------
NEW PAGE: About | RefugeesWork.at - We connect refugees   employers in Austria.

About | RefugeesWork.at - We connect refugees   employers in Austria.

 BETA 

 EN  
 | 
 DE  

 Menu 

        Infos 

 Legal 
 Refugees 
 Employers 

        About us 

 About Refugeeswork.at 
 Press 
 Impress 
 Terms 

 Login 
 Sign Up 

 Mission 
 Our mission is to make refugee's potential accesible to the labor market   to break down negative stereotypes towards refugees. 

 About our company 
 Refugeeswork.at is a social enterprise that pursues the goal of creating equal chances on the labour market for migrants. 

 Mag. Dominik Beron 
 Founder   CEO 
 Dominik is the brilliant mind behind our awesome product. Not only that but he's also the greatest teammate you can possibly have, basically the CEO every company wishes to have.  

 dominik@refugeeswork.at 

 Fatima  Almukhtar  B.Sc. 
 Founder   COO 
 Pure awesomeness in one person. Fatima is our heart   soul. She is responsible for our daily operations. 

 fatima@refugeeswork.at 

 Christoph Hauer  B.Sc. 
 Founder   CTO 
 If it s about technology, then it is this guy s territory. Chris is the mastermind behind our platform   everything else we are developing. 

 christoph@refugeeswork.at 

 Luca Augustin 
 Customer   Community Happiness 
 Luca is a really cool, relaxed   fun dude with a great mind. He s honest, direct, reliable and a critical thinker with awesome ideas... and a hell of a cat sitter! 

 luca@refugeeswork.at 

 Verena Hanna  B.Sc. 
 Online Marketing (Volunteer) 
 Verena supported us from the very first day besides her studies and her job. She is one of the nicest persons on this beautiful little earth and still works with us on projects. 

 office@refugeeswork.at 

 Jacob  Wagner B.Sc. 
 Co-Founder 
 Jacob is and will always be part of the founding team of Refugeeswork.at. He s really into numbers, excel sheets and impact evaluation, which is why he still works with us on stuff like financial planning, impact evaluations and helps us to leverage data to create impact! 

 office@refugeeswork.at 

 Farshad Salehi 
 Translator 
 Our dear friend Farshad, came to Austria 4 years ago. He is probably the best Farsi translator in the country and we are incredibly happy to have him. 

 office@refugeeswork.at 

 Lara Kriwan  B.A. 
 Alumni 
 Lara is a sunshine    Wunderwutzi . She worked with us on client   community happiness and many other things too. 

 office@refugeeswork.at 

  Vienna, Austria 
   office@refugeeswork.at 
   +43 680 5010653 

 Links 

 Home 
 Refugees 
 Employers 
 Supporters 
 Press 
 About us 

 Contact Us 

                            We have received your message, we will contact you very soon. 

                            Oops! Something went wrong please refresh the page and try again. 

 Name 

 Email address 

 Message 

 Copyright   2017 Refugeeswork.at - All Rights Reserved. 

 Icons designed by Freepic 

 Home 
 About us 
 Press 
 Impress 
 Terms 

---------------------------------------
NEW PAGE: Impress | RefugeesWork.at - We connect refugees   employers in Austria.

Impress | RefugeesWork.at - We connect refugees   employers in Austria.

 BETA 

 EN  
 | 
 DE  

 Menu 

        Infos 

 Legal 
 Refugees 
 Employers 

        About us 

 About Refugeeswork.at 
 Press 
 Impress 
 Terms 

 Login 
 Sign Up 

 Impress 

 Medieninhaber 

                    Refugeeswork.at ist eine Initiative der  Talent and Diversity GmbH . 
 (Gemeinn tzige Bildungsinitiativen werden durch den Verein Integrationswerk e.V. abgewickelt. ZVR-Zahl: 732501785.) 

                    Gesch ftsf hrung: Mag. Dominik Beron 
                    Kontakt: office@refugeeswork.at 
                    Sitz: S dtirolerplatz 9/20, 1040 Wien,  sterreich   (Keine Besuche ohne Voranmeldung. No visits without appointment.) 

                    UID Nummer: ATU 70952579 
                    FN Nummer: FN 451638z 
                    DVR Nummer: 4015604 

 Haftungsausschluss 

                    Wir sind f r den Inhalt von Seiten, auf die wir verlinken, nicht verantwortlich. Gleichzeitig erkl ren wir, dass wir uns den Inhalt jener Seiten, auf die wir verlinken, nicht zu eigen machen. 

 Alle Rechte vorbehalten. 

  Vienna, Austria 
   office@refugeeswork.at 
   +43 680 5010653 

 Links 

 Home 
 Refugees 
 Employers 
 Supporters 
 Press 
 About us 

 Contact Us 

                            We have received your message, we will contact you very soon. 

                            Oops! Something went wrong please refresh the page and try again. 

 Name 

 Email address 

 Message 

 Copyright   2017 Refugeeswork.at - All Rights Reserved. 

 Icons designed by Freepic 

 Home 
 About us 
 Press 
 Impress 
 Terms 

---------------------------------------
NEW PAGE: Press | RefugeesWork.at - We connect refugees   employers in Austria.

Press | RefugeesWork.at - We connect refugees   employers in Austria.

 BETA 

 EN  
 | 
 DE  

 Menu 

        Infos 

 Legal 
 Refugees 
 Employers 

        About us 

 About Refugeeswork.at 
 Press 
 Impress 
 Terms 

 Login 
 Sign Up 

 Presse 

                    Haben Sie Interesse mehr  ber Refugeeswork.at zu erfahren oder wollen  ber uns berichten? 
                    Wir freuen uns schon auf Ihren Anruf oder Ihre E-Mail. 

 Logo 

 Team 

 Kontakt 

 Mag. Dominik Beron 
 Founder 

 +43 699 180 214 16 
 dominik@refugeeswork.at 

  Vienna, Austria 
   office@refugeeswork.at 
   +43 680 5010653 

 Links 

 Home 
 Refugees 
 Employers 
 Supporters 
 Press 
 About us 

 Contact Us 

                            We have received your message, we will contact you very soon. 

                            Oops! Something went wrong please refresh the page and try again. 

 Name 

 Email address 

 Message 

 Copyright   2017 Refugeeswork.at - All Rights Reserved. 

 Icons designed by Freepic 

 Home 
 About us 
 Press 
 Impress 
 Terms 

---------------------------------------
NEW PAGE: Login | RefugeesWork.at - We connect refugees   employers in Austria.

Login | RefugeesWork.at - We connect refugees   employers in Austria.

 BETA 

 EN  
 | 
 DE  

 Menu 

        Infos 

 Legal 
 Refugees 
 Employers 

        About us 

 About Refugeeswork.at 
 Press 
 Impress 
 Terms 

 Login 
 Sign Up 

 Login 

 E-Mail 

 Kennwort 

 Keep me logged in? 

 Forgot password? 

 Login with 

                   No account yet?  Register here! 

---------------------------------------
NEW PAGE: Registration | RefugeesWork.at - We connect refugees   employers in Austria.

Registration | RefugeesWork.at - We connect refugees   employers in Austria.

 BETA 

 EN  
 | 
 DE  

 Menu 

        Infos 

 Legal 
 Refugees 
 Employers 

        About us 

 About Refugeeswork.at 
 Press 
 Impress 
 Terms 

 Login 
 Sign Up 

 Registration 

  Refugee 

  Employer 

 e-mail 

 password 

 li id='passwordlowercase'  i class='fa fa-check hidden text-success' /  Mind. 1 lower case letter /li 
 li  id='passwordnumber'  i class='fa fa-check hidden text-success' /  Min. 1 digit /li 
 li id='passwordlength'  i class='fa fa-check hidden text-success' /  Min. 6 letters /li 
 /ul " type="password" / 

 Confirm Password 

                                        I agree to the general general terms and conditions and the privacy policy . 

 Login with 

---------------------------------------
NEW PAGE: ABG | RefugeesWork.at - We connect refugees   employers in Austria.

ABG | RefugeesWork.at - We connect refugees   employers in Austria.

 BETA 

 EN  
 | 
 DE  

 Menu 

        Infos 

 Legal 
 Refugees 
 Employers 

        About us 

 About Refugeeswork.at 
 Press 
 Impress 
 Terms 

 Login 
 Sign Up 

 Download PDF 

 AGB f r Arbeitgeber 

  Vienna, Austria 
   office@refugeeswork.at 
   +43 680 5010653 

 Links 

 Home 
 Refugees 
 Employers 
 Supporters 
 Press 
 About us 

 Contact Us 

                            We have received your message, we will contact you very soon. 

                            Oops! Something went wrong please refresh the page and try again. 

 Name 

 Email address 

 Message 

 Copyright   2017 Refugeeswork.at - All Rights Reserved. 

 Icons designed by Freepic 

 Home 
 About us 
 Press 
 Impress 
 Terms 

---------------------------------------
NEW PAGE: ABG | RefugeesWork.at - We connect refugees   employers in Austria.

ABG | RefugeesWork.at - We connect refugees   employers in Austria.

 BETA 

 EN  
 | 
 DE  

 Menu 

        Infos 

 Legal 
 Refugees 
 Employers 

        About us 

 About Refugeeswork.at 
 Press 
 Impress 
 Terms 

 Login 
 Sign Up 

 Download PDF 

 AGB f r Bewerber 

  Vienna, Austria 
   office@refugeeswork.at 
   +43 680 5010653 

 Links 

 Home 
 Refugees 
 Employers 
 Supporters 
 Press 
 About us 

 Contact Us 

                            We have received your message, we will contact you very soon. 

                            Oops! Something went wrong please refresh the page and try again. 

 Name 

 Email address 

 Message 

 Copyright   2017 Refugeeswork.at - All Rights Reserved. 

 Icons designed by Freepic 

 Home 
 About us 
 Press 
 Impress 
 Terms 

---------------------------------------
NEW PAGE: Informationen für Flüchtlinge | RefugeesWork.at - We connect refugees   employers in Austria.

Informationen f r Fl chtlinge | RefugeesWork.at - We connect refugees   employers in Austria.

 BETA 

 EN  
 | 
 DE  

 Menu 

        Infos 

 Legal 
 Refugees 
 Employers 

        About us 

 About Refugeeswork.at 
 Press 
 Impress 
 Terms 

 Login 
 Sign Up 

 Coming Soon 

  Vienna, Austria 
   office@refugeeswork.at 
   +43 680 5010653 

 Links 

 Home 
 Refugees 
 Employers 
 Supporters 
 Press 
 About us 

 Contact Us 

                            We have received your message, we will contact you very soon. 

                            Oops! Something went wrong please refresh the page and try again. 

 Name 

 Email address 

 Message 

 Copyright   2017 Refugeeswork.at - All Rights Reserved. 

 Icons designed by Freepic 

 Home 
 About us 
 Press 
 Impress 
 Terms 

---------------------------------------
NEW PAGE: Forgot Password? | RefugeesWork.at - We connect refugees   employers in Austria.

Forgot Password? | RefugeesWork.at - We connect refugees   employers in Austria.

 BETA 

 EN  
 | 
 DE  

 Menu 

        Infos 

 Legal 
 Refugees 
 Employers 

        About us 

 About Refugeeswork.at 
 Press 
 Impress 
 Terms 

 Login 
 Sign Up 

 Forgot Password? 

  Enter your e-mail address. 

 E-Mail 

  Vienna, Austria 
   office@refugeeswork.at 
   +43 680 5010653 

 Links 

 Home 
 Refugees 
 Employers 
 Supporters 
 Press 
 About us 

 Contact Us 

                            We have received your message, we will contact you very soon. 

                            Oops! Something went wrong please refresh the page and try again. 

 Name 

 Email address 

 Message 

 Copyright   2017 Refugeeswork.at - All Rights Reserved. 

 Icons designed by Freepic 

 Home 
 About us 
 Press 
 Impress 
 Terms
 WHOLE PROJECT MARK