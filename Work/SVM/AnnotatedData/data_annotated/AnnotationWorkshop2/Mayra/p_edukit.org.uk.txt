---------------------------------------
NEW PAGE: EduKit - Homepage

EduKit - Homepage

 ABOUT 
 HOW WE HELP 
 TESTIMONIALS 
 CONTACT 

 LOGIN 

 Find the right support for students 
 Search a powerful database of organisations classified by impact 

 I am looking for help with 

 My local authority is 

 Free 

 Free wellbeing surveys for your school: Identify areas for support and evidence the impact of your spend 

 TRY EDUKIT   INSIGHT FOR FREE 

 FIND RELEVANT SUPPORT 
 Browse a unique database of thousands of youth development organisations classified by their
                        location and their impact on students  

 SAVE TIME 
 Listings have videos, maps, testimonials and pricing information to help you to quickly identify
                        which services are most appropriate 

 TRACK RESULTS 
 Ensure no child misses out through an easy to use Interventions tracker to help you report on
                        outcomes and progress in real-time 

 WATCH VIDEO 

 Introducing EduKit 
 EduKit is an innovative social enterprise committed to ensuring that all students can achieve their
                    potential with particular focus on those from disadvantaged or difficult backgrounds.

                    Our team has developed online and analytical tools to help schools raise student attainment by
                    making it easy to find appropriate support for students and to track the results whether academic or
                    behavioural.

 FIND OUT MORE 

 Partners   Awards 

 TRACK IMPACT 
 Access a powerful online interventions tracking tool and basic well-being survey for an unlimited
                    number of students. Include comparison and analysis of cohorts of students by gender and year
                    group. 

 FREE TRIAL 

 What People Are Saying About EduKit Connect 

                        Assistant Headteacher

                        Southfields Academy

 EduKit is an amazing tool for schools. The interventions that were found were completely relevant 

                        Pastoral Manager

                        Eltham Hill School

 We have been extremely pleased with the range of programmes. The focus on free and low-cost programmes has been so helpful in ensuring the needs of our students are being met 

 RUN A SEARCH 

 As featured in 

 Still have questions, get in touch. 

 Please use the form below to contact us. We look forward to working with you and will get back to
                        you as soon as possible. All fields are required. 

 SEND MESSAGE 

 Prefer to speak to someone? Phone us on +44 (0)20 3191 9696. 

 About EduKit 
 EduKit saves teachers time by helping them understand school-wide priorities in pastoral care, identifying gaps in provision and providing instant access to impactful local supplementary support. 

 Contact Us 
 +44 (0)20 3191 9696 
 info@edukit.org.uk 

 Useful Links 
 About 
 Terms   Conditions 
 Log Out 
 Privacy Policy 

 Go to top  

  Edukit Solutions Ltd 2016. | All rights reserved.

 Follow us online:  

 EdukitUK 

 @edukitters 

 Follow us online:  

 EdukitUK 

 @edukitters 

  Edukit Solutions Ltd 2016.   All rights reserved.

---------------------------------------
NEW PAGE:     Login

    Login

 List a Programme 
 Sign Up for Free 
 About us 
 Blog 
 Contact us 
 Sign In 

 Sign-in to your account 

  Forgot password? 

 Register for free 
 EduKit is a free resource for schools and parents to find impactful and life-changing opportunities for children and young adults. 

    Edukit Solutions Ltd 2016. | All rights reserved.

 We use cookies to ensure that we give you the best experience of our website. By continuing to use the website, you are agreeing to our use of cookies.
                 Ok 

---------------------------------------
NEW PAGE: Welcome!

Welcome!

 List a Programme 
 Sign Up for Free 
 About us 
 Blog 
 Contact us 
 Sign In 

 Reset password 

    Edukit Solutions Ltd 2016. | All rights reserved.

 We use cookies to ensure that we give you the best experience of our website. By continuing to use the website, you are agreeing to our use of cookies.
                 Ok 

---------------------------------------
NEW PAGE: Edukit - Register A Programme for Free

Edukit - Register A Programme for Free

 Contact us 
 Sign In 

 Register a programme 

 1 
 Step 1 

 2 
 Step 2 

 3 
 Step 3 

 Name of service provider 

 Website 

 Name of programme 

 Programme available online 
yes
						no

 Programme available worldwide 
yes
						no

 Country 

 Choose Region(s) 

 All England 
 All Wales  
 All Scotland  
 All Northern Ireland 

 London 
 South West 
 South East 
 East 
 West Midlands 
 East Midlands 
 Yorkshire and the Humber 
 North West 
 North East 

 Where is the programme delivered? 

 Post code 

 Price 

 How do you charge? 

  Please specify what is UNIQUE or most impressive about your programme in 30-50 words. 

  This is what teachers will see first, so please make your summary short, punchy and waffle-free so it really stands out. 

 Please outline the specific impact that you have had, or expect to have on pupils. How do you make a difference? Why are you better than other interventions?

  Again, keep your responses brief - bullet points are best. Please also add any facts and figures on results achieved in the past, as these are useful for schools. 

 Your name 

 Your surname 

 Best number to reach you on 

 Your email address 

 Password 

 Confirm password 

 Type of organisation 

 Company registration number	 

 Code of conduct 

 We have all required safeguarding policies in place i.e. a safe from harm policy and DBS screening for staff and volunteers 
 For each programme that we wish to upload, we will provide details of at least one teacher or education professional who is willing to provide visible feedback on their experience to be displayed on our site 
 We have all necessary insurance in place i.e. public liability insurance 
 We are using this service in good faith and agree to provide honest, accurate information in response to all questions. We will advise the EduKit team immediately should our circumstances change. 

 Please tick to confirm that you are OK to sign up to the following Code of conduct 

 We'd like to thank our funders for their generous support 

 020 3191 9696 

 info@edukit.org.uk 

 EdukitUK 

 @edukitters 

    Edukit Solutions Ltd 2016. | All rights reserved.

 test 

 We use cookies to ensure that we give you the best experience of our website. By continuing to use the website, you are agreeing to our use of cookies.
             Ok 

---------------------------------------
NEW PAGE: Edukit - Register Yourself and Your School for Free

Edukit - Register Yourself and Your School for Free

 Contact us 
 Sign In 

 Join EduKit 

 Registering with EduKit means: 

 instant access  to development programmes for children and young people 
 ratings and reviews  from other schools who have used programmes before 
 invites to  free and low-cost events  in your area that can help you and your school 

 Name 

 Surname 

 Country 

 School post code 

 Role(s) 

 It is possible to choose more than one 

 School name 

 Email address 

 Phone number  

 Password 

 Confirm password 

 Disclaimer 
 PLEASE NOTE: EduKit is a service that saves schools time by giving them the information they need to connect to thousands of local development programmes for their pupils. However, whilst every provider we register has been asked to sign up to our code of conduct, WE DO NOT VET OR VOUCH FOR ANY OF OUR PROVIDERS. It is therefore the responsibility of each school to ensure that it has performed all required due diligence and safeguarding checks prior to engaging providers. Should you have any questions at all, please call us on 020 3191 9696. Please click Here to confirm that you have understood this. 

 I have read the disclaimer 

 Phone number 

 From 

 To 

 I prefer to be called at this time  

 020 3191 9696 

 info@edukit.org.uk 

 EdukitUK 

 @edukitters 

    Edukit Solutions Ltd 2016. | All rights reserved.

 We use cookies to ensure that we give you the best experience of our website. By continuing to use the website, you are agreeing to our use of cookies.
			 Ok
 WHOLE PROJECT MARK