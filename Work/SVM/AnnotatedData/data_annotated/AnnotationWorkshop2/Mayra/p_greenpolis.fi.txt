---------------------------------------
NEW PAGE: InnoHiili - Innovative low carbon public services - Greenpolis

InnoHiili - Innovative low carbon public services - Greenpolis

 Avaa valikko 
 Sulje valikko 

 Home 
 Projects 
 Services 
 Contact 
 English Suomi   

 InnoHiili   Innovative low carbon public services 

 Edellinen - V h Hiku 
 Seuraava - Arctic Energy 

 InnoHiili   Innovative low carbon public services 
 Timetable: 1.12.2015 - 30.11.2017 

 Innovative Low-Carbon Public Services -project has won the RegioStars Awards in the  Energy Union: Climate action  in Brussels 10.10.2017 
 Out of 103 applications, an independent expert jury first shortlisted 24 finalists. 
 More information:  http://ec.europa.eu/regional_policy/en/regio-stars-awards/#1 

 Receiving the EU REGIOSTARS 2017 award in Belgium, Brussels (second from left): Project Manager Kristiina Nurmenniemi, Heikki Laukkanen and Johannes Helama. (  European Union) 
 See RegioStars_brochure2017_Ii 
 The aim of the project is to define, formulate and pilot six new low-carbon services and consumer-driven concepts in the Municipality of Ii that aims at becoming a pioneer and trendsetter in achieving a carbon-neutral society. 
 Promoting organisational culture change in the public sector 
 This means that residents, companies, associations and public sector of Ii municipality are co-designing and co-producing services they are using. Our mission is also to promote the region as a forerunner in low-carbon solutions nationally and as well as internationally. 
 Low-carbon service concepts are defined, created and piloted through three different service design operations which are free trials, community initiatives and an innovation competition. Among these include: 

 Study how to enhance public transportation and cycling: 40 % of all the CO2 emissions of the area is caused by traffic (average in Finland 28 %). Almost half of the population commute to work. The project organized a mapping-based electronic interactive query to which 485 people answered. As result the first cycle path proposed by citizens is in planning phase. 
 Open Innovation Competition to find new solutions for better logistics: The Innovation Competition in 2017 gave proposals for new kind of transports in the time of shared economy. We have started negotiations with two proposers aiming at completely innovative way or commuting: the one serving staff of county hospital and the other one enhancing the shared use of electronic vehicles. 
 Co-design of a town plan: Traditionally opinions of the citizens are collected in a couple of hearings during the zoning process. We decided to co-design the new town plan in the beginning of the process. Methods included: Deep interviews of citizens by service design company: evenings at families  homes, Map sent by official post to every citizen with a request to mark their favourite places, Enquiry for the inhabitants on local newspaper, Municipality Council seminar managed by service designers, Innovative sessions with youth and elders. 

 See our video  https://vimeo.com/215963722 
 4. Digitalisation of the Public Services: Public services need to step in to the time of digitalisation. Innohiili created the framework for Ii s Digital Agenda, which is based on the idea of citizens creating their own services. The agenda was approved by the municipal board. Ii is one of the Digital Pilot Municipalities of the Ministry of Finance with the consept of the service design created in the project. 
 5. Local Economy Certificate: The ecolabel is created with the Association of the Ii Entrepreneurs to foster local economy. Every certificated company gives an Environment Promise how to save energy or environmental resources. Currently 26 Certificates, objective for 2017 is 50. The Association of Ii Entrepreneurs is committed to maintaining the label after the project. 
 6. Distance Measurement of the Use of Electricity, Water and Heat: Towns own lots of premises. It takes lots of man work to collect measured data. InnoHiili Project has created a solution that gathers information automatically into database through the net. This novel solution based on open data emonCMS-system is now collecting data from 15 premises   and will be expanded to all main premises of the municipality (appr. 100 premises). 
 For more information contact: 
 Project Manager Kristiina Nurmenniemi 
tel. +358 400 986 365, kristiina.nurmenniemi@micropolis.fi 
 Project Designer Johannes Helama 
tel. +358 447 739 262, johannes.helama@proto.fi 
 Laita jakoon jos tykk sit  0 0 0   

 In cooperation: 

 Projects Kes bisnes   Vitality and Business to the Rural Areas Arctic Energy InnoHiili   Innovative low carbon public services V h Hiku EnergiaPlus 

 Order our newsletter here 

                                 Lue uutiskirjett          

 Tiedotteita ei l ytynyt. 

                                 Katso uutiskirje arkisto         

 Order our newsletter 
 Order our free newsletter and recieve news about our activities and environment related matters. 

 Contact info   Iin Micropolis Oy 
 Piisilta 1, 91100 Ii 
FINLAND 
info@micropolis.fi 

 greenpolis.fi 

 Pages   
 Home 
 Projects 
 Contact 

 Kotisivut yritykselle:  Sivututka 

 English 
 Suomi 

---------------------------------------
NEW PAGE: InnoHiili - Innovative low carbon public services - Greenpolis

InnoHiili - Innovative low carbon public services - Greenpolis

 Avaa valikko 
 Sulje valikko 

 Home 
 Projects 
 Services 
 Contact 
 English Suomi   

 InnoHiili   Innovative low carbon public services 

 Edellinen - V h Hiku 
 Seuraava - Arctic Energy 

 InnoHiili   Innovative low carbon public services 
 Timetable: 1.12.2015 - 30.11.2017 

 Innovative Low-Carbon Public Services -project has won the RegioStars Awards in the  Energy Union: Climate action  in Brussels 10.10.2017 
 Out of 103 applications, an independent expert jury first shortlisted 24 finalists. 
 More information:  http://ec.europa.eu/regional_policy/en/regio-stars-awards/#1 

 Receiving the EU REGIOSTARS 2017 award in Belgium, Brussels (second from left): Project Manager Kristiina Nurmenniemi, Heikki Laukkanen and Johannes Helama. (  European Union) 
 See RegioStars_brochure2017_Ii 
 The aim of the project is to define, formulate and pilot six new low-carbon services and consumer-driven concepts in the Municipality of Ii that aims at becoming a pioneer and trendsetter in achieving a carbon-neutral society. 
 Promoting organisational culture change in the public sector 
 This means that residents, companies, associations and public sector of Ii municipality are co-designing and co-producing services they are using. Our mission is also to promote the region as a forerunner in low-carbon solutions nationally and as well as internationally. 
 Low-carbon service concepts are defined, created and piloted through three different service design operations which are free trials, community initiatives and an innovation competition. Among these include: 

 Study how to enhance public transportation and cycling: 40 % of all the CO2 emissions of the area is caused by traffic (average in Finland 28 %). Almost half of the population commute to work. The project organized a mapping-based electronic interactive query to which 485 people answered. As result the first cycle path proposed by citizens is in planning phase. 
 Open Innovation Competition to find new solutions for better logistics: The Innovation Competition in 2017 gave proposals for new kind of transports in the time of shared economy. We have started negotiations with two proposers aiming at completely innovative way or commuting: the one serving staff of county hospital and the other one enhancing the shared use of electronic vehicles. 
 Co-design of a town plan: Traditionally opinions of the citizens are collected in a couple of hearings during the zoning process. We decided to co-design the new town plan in the beginning of the process. Methods included: Deep interviews of citizens by service design company: evenings at families  homes, Map sent by official post to every citizen with a request to mark their favourite places, Enquiry for the inhabitants on local newspaper, Municipality Council seminar managed by service designers, Innovative sessions with youth and elders. 

 See our video  https://vimeo.com/215963722 
 4. Digitalisation of the Public Services: Public services need to step in to the time of digitalisation. Innohiili created the framework for Ii s Digital Agenda, which is based on the idea of citizens creating their own services. The agenda was approved by the municipal board. Ii is one of the Digital Pilot Municipalities of the Ministry of Finance with the consept of the service design created in the project. 
 5. Local Economy Certificate: The ecolabel is created with the Association of the Ii Entrepreneurs to foster local economy. Every certificated company gives an Environment Promise how to save energy or environmental resources. Currently 26 Certificates, objective for 2017 is 50. The Association of Ii Entrepreneurs is committed to maintaining the label after the project. 
 6. Distance Measurement of the Use of Electricity, Water and Heat: Towns own lots of premises. It takes lots of man work to collect measured data. InnoHiili Project has created a solution that gathers information automatically into database through the net. This novel solution based on open data emonCMS-system is now collecting data from 15 premises   and will be expanded to all main premises of the municipality (appr. 100 premises). 
 For more information contact: 
 Project Manager Kristiina Nurmenniemi 
tel. +358 400 986 365, kristiina.nurmenniemi@micropolis.fi 
 Project Designer Johannes Helama 
tel. +358 447 739 262, johannes.helama@proto.fi 
 Laita jakoon jos tykk sit  0 0 0   

 In cooperation: 

 Projects Kes bisnes   Vitality and Business to the Rural Areas Arctic Energy InnoHiili   Innovative low carbon public services V h Hiku EnergiaPlus 

 Order our newsletter here 

                                 Lue uutiskirjett          

 Ii on Euroopan paras 
 Uutisia Iist 
 Uutisia Iin seudulta 

                                 Katso uutiskirje arkisto         

 Order our newsletter 
 Order our free newsletter and recieve news about our activities and environment related matters. 

 Contact info   Iin Micropolis Oy 
 Piisilta 1, 91100 Ii 
FINLAND 
info@micropolis.fi 

 greenpolis.fi 

 Pages   
 Home 
 Projects 
 Contact 

 Kotisivut yritykselle:  Sivututka 

 English 
 Suomi
 WHOLE PROJECT MARK