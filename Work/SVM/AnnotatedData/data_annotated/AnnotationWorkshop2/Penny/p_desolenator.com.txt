---------------------------------------
NEW PAGE: Desolenator: Water Independence. Just Add Sun!

Desolenator: Water Independence. Just Add Sun!

 Water Crisis 
 Product 
 Contact 

 Desolenator is a clean technology venture that has developed and patented what s on track to becoming the most affordable and environmentally friendly method of water purification on the market. 

 Play Now 

 "I think what resonates with me most is when you see people living without clean water and they're forced to scavenge for water and basically use up all of their time just doing that..." 
 Matt Damon 

 Over half of the world s hospitals beds are occupied with people suffering from illnesses linked with contaminated water and more people die as a result of polluted water than are killed by all forms of violence including wars. 
 Achim Steiner, Executive Director, UNEP 

 "The Desolenator is the best solution to overcome the global water crisis." 
 William Janssen, Founder 

 "This historic drought demands unprecedented action, We have to act differently.   The idea of your nice little green grass getting lots of water every day, that s going to be a thing of the past. 
 Jerry Brown, Governor of California 

 California has about one year's worth of water left in its reservoirs 
 Jay Famiglietti, Water Research, NASA 

 A Billion People do not have access to clean water 
 There is not enough water to go around.  The causes are common knowledge - groundwater reserves are increasingly contaminated and becoming saline through pollution and rising sea levels.  This, coupled with a growing population, the effects of climate change and industrialisation has created one of the greatest challenges facing humanity. 97% of the Earth s water is in its oceans, but current desalination technologies are highly inefficient and dependent upon non-renewable sources of energy. 

 Interactive Water Stress Map 
 Water Conflicts 

 The Desolenator 

 Our patented technology uses only solar power to purify water from any source, including sea water; which is especially critical in regions where natural groundwater reserves have been polluted or poisoned or where seawater is the only water source available. Desolenator has a lifespan of up to 20 years and requires very little maintenance; it uses no filters, no membranes, and no pre-treatment chemicals. 

 Use Cases 

 Seawater Desalination 

 Independent Drinking Water 

 Village Supply 

 Household Use 

 About Us 
 Desolenator is a clean technology venture that provides the first real solution to the global water crisis, with the vision to operate on a global scale from the outset. Our holistic approach to the challenge, working across public, private and third sectors, has enabled the development of a solution with the potential to expand multiple markets and improve the lives of thousands if not millions of people in a profitable and sustainable way. 

 Awards 

 Meet the Team 

 William Janssen 
					CEO 

 Jiajun Cen 
					CTO 

 Alexei Levene 
					Rainmaker 

 Catriona McGill 
					Project Manager 

 Louise Bleach Business Development 

 Gal Moore Business Development 

 Shulli Clinton-Davis 
					Miss Sunshine 

 Jorge Xelhuantzi 
					Design Guru 

 In the Press 

 UK Office 
 1st Floor  
					 		81 Palace Gardens Terrace 
					  		Notting Hill W8 4AT  
					   		United Kingdom 

 NL Office 
 Orionsingel 2  
					 		6418KK Heerlen  
					  		The Netherlands  

 info@desolenator.com 

 (c) 2015 Desolenator 

 Tweets von @desolenator  

 Desolenator   

---------------------------------------
NEW PAGE: Desolenator: Water Independence. Just Add Sun!

Desolenator: Water Independence. Just Add Sun!

 Water Crisis 
 Product 
 Contact 

 Desolenator is een cleantech onderneming die een gepatenteerde technologie ontwikkeld en hard op weg is om de meest betaalbare en milieuvriendelijke manier van waterzuivering op de markt te worden. 

 Afspelen 

 "I think what resonates with me most is when you see people living without clean water and they're forced to scavenge for water and basically use up all of their time just doing that..." 
 Matt Damon 

 Over half of the world s hospitals beds are occupied with people suffering from illnesses linked with contaminated water and more people die as a result of polluted water than are killed by all forms of violence including wars. 
 Achim Steiner, Executive Director, UNEP 

 "Desolenator is de beste oplossing voor de water crisis." 
 Pim Janssen, CEO 

 "This historic drought demands unprecedented action, We have to act differently.   The idea of your nice little green grass getting lots of water every day, that s going to be a thing of the past. 
 Jerry Brown, Governor of California 

 California has about one year's worth of water left in its reservoirs 
 Jay Famiglietti, Water Research, NASA 

 Een miljard mensen ontberen toegang tot schoon drinkwater 
 Er is niet genoeg water beschikbaar. De oorzaken zijn algemeen bekend - grondwater reserves raken verontreinigd en verzilten door vervuiling en een stijgende zeespiegel. Dit, in combinatie met een groeiende bevolking, de gevolgen van klimaatverandering en industrialisatie is een van de grootste uitdagingen voor de mensheid. 97% van het water op aarde is in de oceanen, maar de huidige ontziltingstechnologie n zijn zeer ineffici nt en afhankelijk van fossiele brandstoffen. 

 Interactieve Water Stress kaart 
 Water Conflicten 

 De Desolenator 

 Onze gepatenteerde technologie gebruikt alleen zonne-energie om water te zuiveren van elke bron, met inbegrip van zeewater; dat is vooral van belang in regio's waar de natuurlijke grondwater reserves zijn vervuild of vergiftigd of wanneer zeewater de enige beschikbare waterbron is. Desolenator heeft een levensduur tot 20 jaar en vergt weinig onderhoud; gebruikt geen filters, geen membranen en werkt zonder voorbehandeling met chemicali n. 

 Mogelijkheden 

 Zeewater ontzilting 

 Onafhankelijke water voorziening 

 Gemeenschappelijke installatie 

 Huishoud toepassing 

 Over ons 
 Desolenator is een cleantech onderneming die de eerste echte oplossing voor de wereldwijde watercrisis biedt, met de visie om van meet af aan te werken op een wereldwijde schaal. Onze holistische benadering van deze uitdaging en het werken met de publieke, private en non-profit sectoren, heeft de ontwikkeling van de oplossing mogelijk gemaakt, om vele markten te betreden en het leven van duizenden, zo niet miljoenen mensen te verbeteren, op een winstgevende en duurzame manier. 

 Prijzen 

 Kennis maken 

 William Janssen 
					CEO 

 Jiajun Cen 
					CTO 

 Alexei Levene 
					Rainmaker 

 Catriona McGill 
					Project Manager 

 Louise Bleach Business Development 

 Gal Moore Business Development 

 Shulli Clinton-Davis 
					Miss Sunshine 

 Jorge Xelhuantzi 
					Design Guru 

 In de pers 

 NL kantoor 
 Orionsingel 2  
					 		6418KK Heerlen  
					  		The Netherlands  

 UK Office 
 1st Floor  
					 		81 Palace Gardens Terrace 
					  		Notting Hill W8 4AT  
					   		United Kingdom 

 info@desolenator.com 

 (c) 2015 Desolenator 

 Tweets von @desolenator  

 Desolenator
 WHOLE PROJECT MARK