---------------------------------------
NEW PAGE: Ask a medical online consultation to our specialists

 Ask a medical online consultation to our specialists

 Search 

 About us 

 Team 
 Our history 
 Prizes and Recognitions 
 Press review 

 Our service 

 Medical opinion 
 Telemedicine 

 Doctors 

 Surgeons 
 Ophthalmologists 
 Oncologists 

 FAQ 
 Contacts 
 Login 

 Home 

 About us 

 Team 
 Our history 
 Prizes and Recognitions 
 Press review 

 Our service 

 Medical opinion 
 Telemedicine 

 Doctors 

 Surgeons 
 Ophthalmologists 
 Oncologists 

 FAQ 
 Contacts 
 Login 

 THE MEDICAL OPINION YOU WERE LOOKING FOR 

 We assist you in searching a medical  Ultraspecialista 
and in receiving a specialized consultation while staying comfortably at home. 
 How does it work 
 Sign Up We Break Down Geographic Barriers You can receive a consultation without having to leave your home 
 We Select The Best Doctors Our evaluation criteria guarantee the selection of top specialists 
 We Assist You Instantly Our doctors will respond within 5 business days 
 ABOUT US 

 Ultraspecialisti  Ultraspecialisti is an  innovative start-up with a social mission in healthcare . It was born with the objective to break down all barriers that obstruct patients from receiving the best diagnosis, medical assistance and technological advancements provided by Telemedicine. 
 The idea was born from the direct experience of one of the founders,  Vanesa Gregorc , Oncologist at the San Raffaele Hospital in Milan. Her everyday experience highlighted the need to facilitate the encounter and interaction between a medical expert and whoever was in need of an urgent medical consultation. 
 Our mission is to help patients identify the most qualified medical specialists and receive the best medical opinion for their illness while staying comfortably at home or wherever there is Internet access.  Ultraspecialisti.com  is the web platform that connects patients and caregivers with the top medical experts of various fields and allows patients to share their medical documents (Clinical Folders and DICOM images) in a safe environment. 

 LEARN MORE 
 Quick and Easy Steps to get in touch with your Specialist. 

 Sign Up Few and Easy Steps to access Ultraspecialisti.com 
 Choose the Specialist for your Pathology Select Specialty and Pathology for your Second Opinion 
Make a pre-authorized payment of 240   (PayPal/CreditCard) 
Choose a medical specialist  Ultraspecialista 
 Upload Files Fill out your Medical History 
Upload your Medical Reports 
 Request Consultation Send Consultation request to your Ultraspecialista 
The specialist will analyze your documents in the reserved area 
If necessary, the specialist will ask for more medical reports 
 Consultation within 5 business day Receive email notifications when consultation is ready 
Access reserved area to print or save in pdf format specialist s opinion 
The medical expert will be available for any clarifications 
Payment is completed upon received consultation 

 WHY OPT FOR AN ONLINE SECOND OPINION 
 Asking and receiving a second opinion from the best specialist in your pathology is every patient s right. 

 Unknown or Uncertain Diagnosis If the symptoms persist and the diagnosis is still uncertain, ask a specialist for an opinion based on your medical reports (First Opinion) 
 Confirmation or further analysis of the therapy The specialist can confirm the prescribed therapy based on previous medical reports but also suggest additional treatments 
 Confirmation or further analysis of the diagnosis The specialist can confirm the current diagnosis based on previous medical reports, add information, suggest additional analysis to choose the best treatment. 
 Experimental protocols The specialist can direct a patient towards experimental and/or innovative therapies and currently available technologies 
 Additional Information on Prognosis and/or a multidisciplinary Opinion The Specialist can evaluate the current patient s journey and/or seek a multidisciplinary opinion that involves more specialists 
 Avoid unnecessary trips  A patient can ask for an initial assessment of his condition by a selected specialist that operates remotely and avoid unnecessary trips; obtain indications on which medical tests are needed during the visit to avoid multiple trips; send the medical tests prescribed by the specialists during the visit and receive an opinion. 

 SELECTION OF MEDICAL SPECIALISTS 
 Ultraspecialisti.com selects and recruits doctors that are highly qualified and specialized in a discipline or pathology for which they have a national or international recognition. The chosen and adopted criteria are based on selecting highly competent profiles that have experience in clinical and research environments. 

 Medical and Surgical Degree 
 Certified Medical Professional,  Ordine dei Medici  Member 
 Medical Specialist 
 At least 10 years of clinical activity in multidisciplinary or accredited hospital facilities 
 Involvement in Update courses regarding their specialization 
 Presentations at national or international events or publications on the subject 
 Contributions in clinical research about a specific pathology, treatment or technique 
 References of at least two colleagues with the same profile 

 FOUNDERS 

 ANTONIO SAMMARCO 
 Founder   CEO 
 Graduated in Business and Economics with a Master degree in Sales Management at the Business school IlSole24ore, Antonio Sammarco has gained a long and solid professional experience in the business environment of renowned multinational companies from 2000 to 2015, with roles of growing responsibility in Retail (B2C), e-commerce, in B2B for the Italian and foreign market managing Key accounts and sales networks (both domestic and foreign). He has dealt with consumer products (including high-end) for Retail, Gross and Mass Market Channels. 

 VANESA GREGORC 
 Co-founder and Scientific Director 
 Doctor and Surgeon specialized in Oncology, Vanesa Gregorc is Coordinator of the  Thoracic Neoplasm ,  Cervical-Facial  and  Melanoma  Divisions at the San Raffaele Hospital (Milan) as well as Professor at the Vita Salute San Raffaele University. She is also Secretary of  Working Group Neoplasie del Polmone  of the Italian Cancer Association where she coordinates activities in various hospitals and Scientific Institutions. Vanesa Gregorc has over 20 years of clinical and research experience in Oncology, with national and international recognitions in Thoracic Neoplasms. 
 FAQ 

 When to ask for a medical opinion? 
 To verify or identify alternative solutions in the diagnosis and therapy (surgery, radiotherapy, tomotherapy, chemotherapy, immunotherapy, biological therapy, antiangiogenetics, etc.) 
 For protocols (trials, studies) and innovative experimental therapies or innovative techniques 
 For confirmation of diagnosis and treatment 
 To obtain a diagnosis 
 To receive more detailed or additional information on prescribed, ongoing or alternative treatments 
 To be addressed towards support, pain relief therapies, etc. 
 For Palliative therapies 
 To be reassured and supported in choosing a treatment 
 To be reassured 
 To evaluate other medical opinions 

 How can our Platform help an Ultraspecialista? 
 The medical expert can manage the requested consultations in the time and space most appropriate to him 
 The medical expert is in direct contact with the patient, however the relationship and communication is regulated by the platform (not a chat and not via email) 
 The medical expert can collaborate with his colleagues and if needed, can quickly exchange opinions and information 
 The medical expert can request for the integration of more documents 
 The medical expert can analyze medical images by using an incorporated image viewer 
 The medical expert does not need to handle medical papers or CD that can be lost or misplaced 
 The medical expert can easily locate the patient s medical records in the patient s personal folder 
 The medical expert operates on a safe and private platform 
 The medical expert has IT and customer service support 
 The medical expert has an alert system that notifies him and reminds him of the requested consultations 

 What are the benefits of choosing Ultraspecialisi.com for a medical opinion? 
 Quick identification of the  right  doctor, specialized in the field of interest 
 Abolishment of barriers (travel, time, costs, stress, etc.) 
 Preparation and collection of medical records in a complete and accurate manner 
 Possibility of receiving a multidisciplinary medical opinion when the pathology requires the competence of different specialists 
 Receive an expert and competent opinion 
 Access to innovative treatment 
 Possibility of obtaining clarifications 
 The advantage of avoiding several trips to the hospital by bringing to the visit the complete and appropriate medical documentation thanks to previous medical advice 
 Opportunity of remaining in contact with the selected specialist 

 How long does it take to receive a medical opinion? The response time is certain. The medical expert replies within 5 business days. 
 Who is an Ultraspecialista? Ultraspecialista is a highly experienced medical specialist in a specific discipline or pathology. 
 How is a Ultraspecialista selected? Discover here the  selection criteria of a Ultraspecialista 
 What do I need in order to use the Platform? To use the platform the user needs an Internet connection and one of the following browser versions or updated versions: Internet Explorer 10, Chrome 28, Firefox 37, Safari 7. 

 About us 

 Team 
 Our history 
 Prizes and recognitions 
 Press review 

 Our service 

 Medical opinion 
 Telemedicine 

 Doctors 

 Oncologists 
 Ophthalmologists 
 Surgeons 

 Pathologies 

 Oncology 

 Contacts 

 Login/Register 
 Legal area 
 FAQ 

 Social Media 

 Facebook 
 Twitter 
 Google+ 
 Tumorealpolmone 

 Technological partner  In collaboration with    Copyright   2017 Ultraspecialisti  srl  STI  - All rights reserved 
Ultraspecialisti srl - Via Ampere 61/A, 20131 Milano (Italy) - c.f./p. IVA 09364300963 - REA MI-2085389 - capitale i.v. Euro 10.000.
 WHOLE PROJECT MARK