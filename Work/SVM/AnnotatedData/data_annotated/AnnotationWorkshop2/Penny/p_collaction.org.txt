---------------------------------------
NEW PAGE: Home Page - CollAction

Home Page - CollAction

 Find Project 
 Start Project 
 About 

 Login 

 POWER TO THE CROWD 

                            Find Project 

                            Start Project 

                        Do you want to make the world a better place, but do you sometimes feel that your actions are only a drop in the ocean? 

                        Well, not anymore, because we are introducing crowdacting: taking action knowing that you are one of many. We revamp the neighbourhood with a hundred people or switch to renewable energy with thousands at the same time.  Find out more about crowdacting , take action collectively with one of the existing projects, or  start your own crowdacting project .  

 Crowdacting in 3 steps 

                        Your browser does not support SVG 

 Proposal 

                        Someone proposes a collective action and sets a target number of participants and a deadline 

                        Your browser does not support SVG 

 Crowd 

                        Supporters pledge to join if the target is met before the deadline 

                        Your browser does not support SVG 

 Act 

                        When the target number of supporters is met, we all act! 

                        Starting a Project 

                        Browse Projects 

 Join A Project 

        Any questions, comments, or would you like to work together? Awesome! Email us at  collactionteam@gmail.com 
        Would you like to get an occasional update on CollAction and crowdacting? Sign up for our newsletter (to your right)! 

 Home 
 Find Project 
 Start Project 
 About Us 

 Login 
 Sign Up 
 Mission 
 Team 

 Jobs 
 Partners 
 Press 
 FAQs 

 Follow Us 

 Newsletter 

 *  indicates required 

 Email Address   * 

---------------------------------------
NEW PAGE: Register - CollAction

Register - CollAction

 Find Project 
 Start Project 
 About 

 Login 

 Create an account 

 First name 

 Last name 

 Email 

 Confirm email 

 Password 

 Confirm password 

 I would like to receive an update from CollAction every once in a while - don #x27;t worry, we like spam as little as you do!  #x1F642; 

        Any questions, comments, or would you like to work together? Awesome! Email us at  collactionteam@gmail.com 
        Would you like to get an occasional update on CollAction and crowdacting? Sign up for our newsletter (to your right)! 

 Home 
 Find Project 
 Start Project 
 About Us 

 Login 
 Sign Up 
 Mission 
 Team 

 Jobs 
 Partners 
 Press 
 FAQs 

 Follow Us 

 Newsletter 

 *  indicates required 

 Email Address   * 

---------------------------------------
NEW PAGE: About - CollAction

About - CollAction

 Find Project 
 Start Project 
 About 

 Login 

 About Us 
 About CollAction 
 Meet The Team 
 Jobs 
 Partners 
 FAQ 

 Our Mission 

 We help people solve collective action problems through crowdacting  

                        Many things are going well in the world. But we re also facing some large and urgent problems. We see that traditional solutions to these problems are just not enough. At the same time there are so many people that want to do something to create a better world. That s why we came up with a new approach: crowdacting. With crowdacting we ask:  Would you take action if a [hundred/thousand/million] other people would do the same? 

 About CollAction 

                        Do you sometimes have the feeling that your actions are only a drop in the ocean? Well, not anymore, because we are introducing crowdacting: taking action knowing that you are one of many. We revamp the neighbourhood with a hundred people or switch to renewable energy with hundreds or thousands of people at the same time.  
                        Find out more about what crowdacting is in the video or the FAQs, take action collectively with one of the  existing projects , or  start your own crowdacting project . 

 Power to the Crowd 

 Meet the team 

 Join our team! 

                        CollAction is always looking for fun people who want to make a difference by making CollAction the next big thing! Please note that these are all volunteer positions for now (all current team members are volunteers as well :) ) 

                        We re looking for: 

 Communications guru : As the communications guru you are responsible for developing and implementing our communications strategy and further build the growing CollAction community. For those who need a good excuse to spend all day on Facebook.  Email us  for more info! 

 Project Starter : Do you sometimes think  AAAH THE WORLD IS ON FIRE, I NEED TO DO SOMETHING BUT WHAT!? . Well, maybe chill out first and have some herbal tea :). Then get your hands dirty and start a project  here . 

 Partners 

                        We couldn t do what we re doing without the support of our great partners, including the ones below. Please  send us an email  if you want to join this group of early adopters.  

 SIDN Fonds 
 Essense 
 Social Coder 
 De Ceuvel 

 Frequently Asked Questions 

 Frequently Asked Questions 

                What is CollAction? 

                CollAction is a not-for-profit organization based in the Netherlands that helps people to solve Collective Action Problems through crowdacting. 

                Huh? What is crowdacting? 

                The term Crowdacting, as it is used here, is a term that has been introduced by CollAction. Basically, it means:  taking action collectively and conditionally with the purpose of achieving positive impact on the world and its inhabitants . 
                Simply put, with crowdacting we ask:  Would you take action if you knew that a hundred/a thousand/a million/a billion others would do so too?  An example of a crowdacting project:  If we can find 10,000 people that commit to switch to renewable energy if 9,999 others would do the same, we will all switch together . As a result, we form a critical mass of people that has a huge impact on making this world a better place. The concept is very similar to crowdfunding, but people commit to actions, instead of money. 

                In practice, a crowdacting project follows three steps: 

                        The ProjectStarter launches a project on collaction.org. He/she proposes a certain action (for example switching to renewable energy), a target (minimum number of participants), and a deadline. 

                        ProjectSupporters commit to taking action, if the target is met within the deadline. 

                        If and when the target is met, everybody acts collectively. 

                More information can be found on  www.crowdacting.org . 

                Crowdacting   is that a new thing? 

                Although the term crowdacting in its current meaning is new, the underlying concept is not. There are plenty of other examples of collective actions that contain elements of crowdacting. Think for instance of boycotts, demonstrations, collective bargaining initiatives, or petition websites. The difference is the fact that crowdacting combines three elements: 1) It s (explicitly) conditional (it only happens if a set target is met); 2) it s for social and/or ecological good; and 3) the action goes beyond signing a petition. 
                More information on how crowdacting resembles and differs from other initiatives can be found on  www.crowdacting.org . 

                What are collective action problems? 

                Collective action problems are situations where it is in the interest of the group to work together, but in which coordination is a challenge. Because of this, every individual has an incentive to act in their own interest. This  video  explains it clearly. 

                A well-known example of a collective action problem is the tragedy of the commons. 
                The commons is a communal meadow where cows of different farmers can graze. Every farmer wants to earn as much as possible against the lowest possible costs and therefore has the individual incentive to let as many of his cows graze on the communal grass as possible. However, other farmers will do the same and soon the meadow will be destroyed due to overgrazing. There is little point in doing something about this for each individual farmer   knowing (or fearing) that other farmers will try and use the largest possible share of the communal grass and the meadow will still be overgrazed. The farmer is better off adding some more cows, so he will, at least, benefit from it in the short term. However, in the long term, all the farmers and as a consequence the community will lose out, because the piece of land can be written off due to overgrazing. 

                Examples of (serious and less serious) collective action problems are: global warming, traffic jams, overpopulation, dismal working conditions, keeping a student house clean, corruption, overfishing, etc. 

                Traditionally, there are two solutions to collective action problems: 

                        Privatisation: the commons can be divided between the farmers. Now the farmers benefit from taking good care of their own piece of land, so that it is of use to them in the long term. 

                        Regulation: Agreements can be made and enforced about the usage of the commons. It is, for example, possible to decide on a maximum number of cows per farmer. 

                However, these solutions are not applicable to all collective action problems. It is not so easy (or desirable) to privatise certain goods (how do you privatise the ozone layer? Or migrating fish populations?). Coming up with regulations and enforce these regulations to solve (cross border) problems has proven difficult - just think about the cumbersome process to do something about climate change on an international level. 

                So, we need a new solution to old problems: crowdacting. 

                Where can I learn more about crowdacting? 

 www.crowdacting.org . 

                Can I start a project myself? 

                Yes! You can create a project  here . Make sure to check if your idea meets the CollAction criteria below.  

                What are the criteria that a project has to meet? 

                A project can only be listed when it meets the following criteria: 

                        The goal of the project is to make a positive societal or ecological contribution to your neighbourhood, country or the world. 

                        The project is not geared towards personal gain. 

                        The project does not include activities that are focussed on conversion or activism (religious or political). 

                        The project does not include activities that are illegal or do not abide by the official legislation of the Netherlands or the country of implementation. 

                        The online project registration form is completed fully and truthfully and has a clear and easily readable project description and goal. The CollAction team can ask for clarification and/or edit your text if necessary. 

                        The ProjectStarter has thought through how people can be moved from commitment to action. We can help you with this! 

                        The project is ambitious but realistic   the CollAction evaluation commission judges if this is the case. The ProjectStarter can activate his/her own network, and/or has a good plan to achieve the target. 

                        The ProjectStarter commits to measuring the impact of the action (e.g. the number of people that have acted as a result of the project) and to sharing this information with CollAction. 

                How long does a project run for? 

                Some actions are a one-off   think of the switch to a fair bank or green energy. Other projects can have a longer duration, such as eating less meat, periodically visiting lonely elderly people or helping refugees with language lessons and their integration. In other words, the duration of an action differs from one project to the other. 

                What happens when a target has not been reached? 

                If the target is not met by the deadline, no one needs to act. Crowdacting means: No cure, no action. However, if you are really inspired to take action anyway, we will of course applaud this. 

                How do I know that other people will really take action? 

                You can never be 100% sure. In the first place, crowdacting is about trust. Trusting that people want to work together to create a better world. Trusting that people are attached to their personal ideals. And trusting that people will keep their promises. We hope to build a community of people that take their commitments seriously and try to create a culture in which agreement=agreement.   

                That being said, we will incorporate certain processes and tools to ensure that as many project supporters proceed to taking action. What these processes are depends on the type of project. For some projects (such as switching projects, for example to a fair bank or green energy) the action can already be started conditionally at the time of commitment. When the target is reached, supporters of the project will automatically be transferred to the fair bank or renewable energy provider. When it comes to local projects, social control will play a bigger role. Other projects might be purely based on trust. We will be testing different tools and approaches in the upcoming projects. We ask ProjectStarters to seriously consider this question    the team of CollAction can help you think this through! 

        Any questions, comments, or would you like to work together? Awesome! Email us at  collactionteam@gmail.com 
        Would you like to get an occasional update on CollAction and crowdacting? Sign up for our newsletter (to your right)! 

 Home 
 Find Project 
 Start Project 
 About Us 

 Login 
 Sign Up 
 Mission 
 Team 

 Jobs 
 Partners 
 Press 
 FAQs 

 Follow Us 

 Newsletter 

 *  indicates required 

 Email Address   * 

---------------------------------------
NEW PAGE: Start Project - CollAction

Start Project - CollAction

 Find Project 
 Start Project 
 About 

 Login 

                    Nice! 

 Start Project 

                    Would you like to do something about a social or environmental problem? Would you like to lead the crowdacting movement? Start a project on CollAction!  

                    There are  a few criteria that your project needs to meet    so please check those first. We ll wait while you read it 

                     All good? Then  start your project! 

 GO TO THE REGISTRATION FORM 

        Any questions, comments, or would you like to work together? Awesome! Email us at  collactionteam@gmail.com 
        Would you like to get an occasional update on CollAction and crowdacting? Sign up for our newsletter (to your right)! 

 Home 
 Find Project 
 Start Project 
 About Us 

 Login 
 Sign Up 
 Mission 
 Team 

 Jobs 
 Partners 
 Press 
 FAQs 

 Follow Us 

 Newsletter 

 *  indicates required 

 Email Address   * 

---------------------------------------
NEW PAGE: Log in - CollAction

Log in - CollAction

 Find Project 
 Start Project 
 About 

 Login 

 Log in 
 (use a local account to log in) 

 Email 

 Password 

 Remember me? 

 Register as a new user? 

 Forgot your password? 

        Any questions, comments, or would you like to work together? Awesome! Email us at  collactionteam@gmail.com 
        Would you like to get an occasional update on CollAction and crowdacting? Sign up for our newsletter (to your right)! 

 Home 
 Find Project 
 Start Project 
 About Us 

 Login 
 Sign Up 
 Mission 
 Team 

 Jobs 
 Partners 
 Press 
 FAQs 

 Follow Us 

 Newsletter 

 *  indicates required 

 Email Address   * 

---------------------------------------
NEW PAGE: Find - CollAction

Find - CollAction

 Find Project 
 Start Project 
 About 

 Login 

        Any questions, comments, or would you like to work together? Awesome! Email us at  collactionteam@gmail.com 
        Would you like to get an occasional update on CollAction and crowdacting? Sign up for our newsletter (to your right)! 

 Home 
 Find Project 
 Start Project 
 About Us 

 Login 
 Sign Up 
 Mission 
 Team 

 Jobs 
 Partners 
 Press 
 FAQs 

 Follow Us 

 Newsletter 

 *  indicates required 

 Email Address   * 

---------------------------------------
NEW PAGE: Forgot your password? - CollAction

Forgot your password? - CollAction

 Find Project 
 Start Project 
 About 

 Login 

 Forgot your password? 

    For more information on how to enable reset password please see this  article . 

 Enter your email. 

 Email 

        Any questions, comments, or would you like to work together? Awesome! Email us at  collactionteam@gmail.com 
        Would you like to get an occasional update on CollAction and crowdacting? Sign up for our newsletter (to your right)! 

 Home 
 Find Project 
 Start Project 
 About Us 

 Login 
 Sign Up 
 Mission 
 Team 

 Jobs 
 Partners 
 Press 
 FAQs 

 Follow Us 

 Newsletter 

 *  indicates required 

 Email Address   *
 WHOLE PROJECT MARK