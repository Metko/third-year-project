---------------------------------------
NEW PAGE: bgood

bgood

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

                        Good deeds:

 11962 

                Your good deeds can change the world. bgood shows what you can do, measures your impact and rewards you for it. Become a part of bgood now.                 

 Login with Facebook 

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: Press

Press

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 Let's tell a story 
 For press inquiries, please contact us via mail or call: 

					pr@bgood.at				 

					phone number for the corresponding country (e.g. +61 40 35 00 775)				 

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: Company signup

Company signup

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 Become a partner 

 Company name 

 Email 

 Repeat Email 

 Phone Number 

 Country 

 Password 

 Repeat Password 

I have read the  Privacy Policy  and  Terms and Conditions  and hereby declare my consent.

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: Imprint

Imprint

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 Imprint 
 Disclosure in accordance with  25 of the Media Act 
 www.bgood.at is a private and for everyone accessible online offer Layout and design of the offer overall as well as its detailed elements are copyrighted. It is considered for its content as well. 
 Media owner and editor 

			Verein zur F rderung von sozialem und  kologischem Engagement			 
            ZVR-Zahl 777860467

Laimgrubengasse 19/7 
1060 Wien 
 sterreich 
Tel.: +43 (0) 660 52 35 829 
E-Mail: office@bgood.at 

 Exclusion of liability: 

				1. Liability for content				 
				The complete content of the Internet site was designed with the utmost care. Nevertheless we are not able to take a warrant for the content to be real, complete or current. According to the media law we are responsible for our own content of our Internet site. As per media law we have no obligation to oversee third parties Information that was transmitted or saved or trace circumstances, which give hints on non-legitimate activities. Of this not touched is our obligation to inhibit ate or remove information that imposed on us through general legislations. We are held responsible for the liability of those legislations only as soon as we are aware of it. If we get aware of such violation we will remove that particular content immediately.			 

				2. Liability for links				 
				Our Internet site contains links to third party external Internet sites. We do not have a scope of influence on the content of those external sites. Therefore, we are not able to apply a warrant for those third party contents. The responsibility is always with the owner/business of the particular Internet site. We are reviewing those Internet sites at the time when we about to link with them for any statutory violation. We only link with Internet sites where we are not able to find any statutory violation at the time of the link. Without any indication of statutory violation we are not able to control the particular link on a regular basis. However if we know about any kind of infringement on any of those sites we will act immediately and remove the link from our site. 			 

				3. Copyright				 
				All bgood logos, names, domain names, programs, services and our website are our property and cannot be copied, used or dealt with in any way without our written permission.			 

				4. Data protection				 
				Our Internet site can be used on a regular basis without any information on personal data. If personal data, for example name, e-mail or address is used than it is only voluntary. We are only passing on personal data if you agree to it. The transfer of personal data over the Internet could have security holes and therefore, it is possible that a third party gets hold of that data. Unfortunately there is no 100% protection. If a third party is using our contact details which we have made public throughout our internet site and we will start getting advertisements, junk mail or any inappropriate information then we are going to take legal measures. 			 

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: Contact

Contact

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 Let s stay in touch 
 For feedback or general questions just send us an e-mail or call us. 

					office@bgood.at				 

					phone number for the corresponding country (e.g. +61 40 35 00 775)				 

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: Team

Team

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 Founders 

 Christoph Hantschk 
 CEO 

 Philipp Wasshuber 
 COO 

 Alexander Hackel 
 CCO 

 Team 

 Oliver Bognar 
 Managing Director Australia 

 Philipp Danzinger 
 Web Development Front-   Backend 

 Gerald Urschitz 
 Frontend UX   Android Development 

 Todor Lazov 
 Backend   Android Development 

 Advisory Board 

 Clemens B ge 
 Advisor 

 Ruth Eberhart 
 Mentor 

 Reinhard Herok 
 Advisor 

 Julia Knauseder 
 Advisor 

 Ivo Radulovski 
 Advisor 

 Natascha Stornig 
 Mentor at StartupLeitner 

 Karin Meier-Martetschl ger 
 Advisor 

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: FAQ

FAQ

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 FAQ 
 What is bgood? 
 bgood is an innovative social community that interactively connects users with each other and shows what you can do to improve the community and the environment. bgood also rewards you for your good deeds. 
 What happens when I register? 
 After you register you have access to various challenges, or good deeds. Through completing these deeds you earn good coins, which you can use to reward youself with products that our partners have donated. 
 How can I be a part of bgood? 
 It s easy! Just sign up. 
 What are good coins? 
 good coins are the currency we use to measure your dedication. Whenever you finish an environmental or social deed we reward you wih the amount of good coins that deed is worth -- which is listed on each individual good deed. You can then use these good coins to exchange them for goodies that are provided by our sponsors and partners. 
 How do I get my reward? 
 It s as simple as exchanging your good coins at http://www.bgood.com.au/goodies for a reward of your choice. As soon as you have done that you ll receive a voucher which will be sent to your email address. You can then use that voucher to pick up your goodie of choice from our partners or their webshops. 
 What do I need good coins for? 
 good coins are our currency that you can use to trade for any reward you like. They also allow us to create personalised statistics of your contribution via the good deeds you complete -- in short, they tell the story of the change you ve created in the world. 
 I finished a good deed, but have not received a code. What can I do? 
 Normally this should not happen. At each of our events somebody from our team is there to make sure that everybody who participates through bgood gets a card with a unique code. Should you not receive a card then write us an email at office@bgood.at. 
 What is good impact? 
 good impact is measured through your good coins. Those good coins are divided into social and environmental impact through the good deeds you have completed. 
 Where can I find my good impact? 
 Just go onto the statistics under your profile picture and you will find your good impact in numbers so you can see what change you have effected so far. 
 How is my good impact calculated? 
 We calculate your impact according to a certain formula. The following factors influence your good impact: time, social effect, environmental impact, difficulty, effort, learning effect, sustainability. 
 What can I do with my good coins? 
 You can exchange your good coins for great rewards. 
 What happens to the good coins once I exchange them? 
 As soon as you ve exchanged your good coins for a goodie, they are deducted from your total. However, the impact that you have generated through the various good deeds you have completed remains on your profile. 
 Is there an app so I can use bgood on the go? 
 At this stage you can use the homepage through your web browser as a mobile version; however, we are working on the development of the bgood app at the moment. 
 Who is behind bgood? 
 Behind bgood you can find a young interdisciplinary team, who has set the goal of doing something good in the world with your help. 
 Where do the goodies come from? 
 Our sponsors and partners donate the rewards. We are so grateful to have such generous sponsors! 
 Will I be able to exchange any of my goodies once I ve selected them? 
 Unfortunately you are not able to exchange your goodies. Once you ve picked the one you want, and confirmed your choice, your good coins are deducted. 
 Can I join the bgood team? 
 We are happy to hear from anybody that likes our concept and if you want to support us to make the world a little bit better then shoot us an email to  office@bgood.at 
 Who can participate in bgood? 
 Anybody that wants to do some good! 
 Can I donate my good coins? 
 At this stage you are only able to exchange your good coins for rewards. Nevertheless, we are developing a solution where you will be able to donate your good coins for certain projects. 

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: About

About

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 We change the world step by step, together. 

		    	At bgood you will find social and environmentally geared challenges that will help you create change. Do something good and get rewarded for it.		    	 

		    	You can choose what to do, when, and with whom. We make it possible for you to achieve something big through small deeds.		    	 

				As soon as you finish a task good coins will be deposited into your account. These coins can then be exchanged for goodies of your choice.	    	  	 

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: Partners

Partners

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 companies 
 NGOs 

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: Goodies

Goodies

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 b.goodies 

 Espresso Blend Toby's Estate Coffee Roasters 65 Stock: 5   

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: Login

Login

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 Log in 

 Log in via 

						Not a member yet?						 
 Sign up now 

						Forgot your password?						 
 Restore it here 

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: All Tasks

All Tasks

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 all 
 ecological 
 social 
 volunteering 

 All Tasks 

 Don't overcook! Save power while cooking +5 Tell a friend Tell a friend about bgood or post bgood on Facebook or Twitter.  +2 Recycled Paper Look at the recycled label when purchasing paper.  +6 Bagging for coins Fabric bags instead of plastic bags! +5 Clean ideas Check the required washing temperature! +4 Pickup Artist How many times a day do you pass carelessly discarded bottles, cans and packaging? +4 Less hungry If you see a person in need, offer to buy or give them a bite to eat +8 Think sustainably when you buy seafood Look up the Greenpeace Seafood Guide the next time you buy seafood. +7 Certified Organic Decide to buy certified organic products next time you re doing your grocery shopping.  +6 Fair Trade Decide to buy a Fairtrade product with your next purchase.  +6 Bike 2 Work Leave your car and ride a bike to work or university. +8 Refill your bottle Do something good for the environment and save money by using a drinking bottle +4 Stand up Offer your seat in a public transport vehicle to another person if necessary. +3 Public transport Are any of your drives really necessary? +5   

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: Partners

Partners

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 companies 
 NGOs 

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: Ecological Tasks

Ecological Tasks

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 all 
 ecological 
 social 
 volunteering 

  Household  Outdoors  Office  Mobility  Nutrition  Consumption  Anytime  Your voice 

 Ecological Tasks 

 Don't overcook! Save power while cooking +5 Recycled Paper Look at the recycled label when purchasing paper.  +6 Bagging for coins Fabric bags instead of plastic bags! +5 Clean ideas Check the required washing temperature! +4 Pickup Artist How many times a day do you pass carelessly discarded bottles, cans and packaging? +4 Think sustainably when you buy seafood Look up the Greenpeace Seafood Guide the next time you buy seafood. +7 Certified Organic Decide to buy certified organic products next time you re doing your grocery shopping.  +6 Bike 2 Work Leave your car and ride a bike to work or university. +8 Refill your bottle Do something good for the environment and save money by using a drinking bottle +4 Public transport Are any of your drives really necessary? +5   

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: Recover password

Recover password

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 Recover password 

 e-mail 

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: Volunteering Tasks

Volunteering Tasks

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 all 
 ecological 
 social 
 volunteering 

  Once  Frequently 

 Volunteering Tasks 

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: Sign up

Sign up

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 Sign up 

 Email 

 Password 

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint 

---------------------------------------
NEW PAGE: Social Tasks

Social Tasks

 tasks 
 b.goodies 
 partners 
 log in 

 Australia 

 Austria (Deutsch)   

 all 
 ecological 
 social 
 volunteering 

  People  Animals  Inspiration  Anytime  Your voice 

 Social Tasks 

 Tell a friend Tell a friend about bgood or post bgood on Facebook or Twitter.  +2 Less hungry If you see a person in need, offer to buy or give them a bite to eat +8 Fair Trade Decide to buy a Fairtrade product with your next purchase.  +6 Stand up Offer your seat in a public transport vehicle to another person if necessary. +3   

 Team 
 About 
 FAQ 
 Press 
 Contact 
 Become a partner 
 Imprint
 WHOLE PROJECT MARK