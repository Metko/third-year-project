---------------------------------------
NEW PAGE: LEAPS - Learning System to Leap into the Social Artificial Intelligence Future -  Training Coaching Skills Future Ready 

LEAPS - Learning System to Leap into the Social Artificial Intelligence Future -  Training Coaching Skills Future Ready 

              Home

              About

              Contact Us

 Home About Contact Us 

 AI is the ability of machines to learn to fulfill objectives based on data and algorithms. Machine learning, automation, big data, cognitive computing and deep learning are different levels of AI. AI is in data networks, social technologies, apps, avatars, chatbots, video games, voice assistants and robots. 

 talk to us 

 START MAKING LEAPS Leap into the social AI future Why do you use AI technologies to achieve work and life goals?  How to maintain your values, life meaning and well-being? What it means to take responsibility of your human capacities? LEAPS learning system helps to understand and measure the impact of AI on you, your team or organisation to move forward. 

 Make Your Leaps 

 FUTURE READINESS Build the next generation leadership Digitisation and robotisation have changed human dynamics of work, life and ways of organisation. Now, you, your team and organsiation need to develop a new mindset and skills to inspire and guide people and intelligent machines towards a brighter, inclusive and fair human future. 

 Learn to be ready 

 THE NEXT CULTURE  Integrate human values  + AI design values Become a proactive sense-maker of people's new desires, needs, beliefs and behaviours to transform the culture of your organization. LEAPS 'learning as we grow' model can help you discover new patterns and refine your decision-making processes as they emerge. 

 Build Your Next Culture 

 CREATE NEW VALUE  Discover new human occupations AI technology is already replacing humans in many tasks and the predictions of this trend are huge. Our unique learning framework allows us to help you structure a creative process to discover and experiment with emergent needs and data that may define new human roles and occupations. 

 Create New Value 

 WORKSHOPS, LEARNING   RESOURCES  Work your future Work your own, your team, organisation or community future together with us. 

 Bring Everyone on 

 Keep it human to restore your power and Well-being 

 join our community 

 Why LEAPS 

 Intro 2 

 Intro 3 

 Intro 4 

 Intro 6 

 Intro 5 

 LEAPS Action 

 2017 LEAPS   CONTACT US 

---------------------------------------
NEW PAGE: Contact Us — LEAPS - Learning System to Leap into the Social Artificial Intelligence Future -  Training Coaching Skills Future Ready 

Contact Us   LEAPS - Learning System to Leap into the Social Artificial Intelligence Future -  Training Coaching Skills Future Ready 

              Home

              About

              Contact Us

 Home About Contact Us 

 Contact Us Please complete the form below 

 Name  * 
 Name 

                  First Name 

                  Last Name 

 Email Address  * 

 Subject  * 

 Message  * 

 Thank you! 

 New Page 

 2017 LEAPS   CONTACT US 

---------------------------------------
NEW PAGE: LEAPS - Learning System to Leap into the Social Artificial Intelligence Future -  Training Coaching Skills Future Ready 

LEAPS - Learning System to Leap into the Social Artificial Intelligence Future -  Training Coaching Skills Future Ready 

              Home

              About

              Contact Us

 Home About Contact Us 

 See more 

 2017 LEAPS   CONTACT US 

---------------------------------------
NEW PAGE: About — LEAPS - Learning System to Leap into the Social Artificial Intelligence Future -  Training Coaching Skills Future Ready 

About   LEAPS - Learning System to Leap into the Social Artificial Intelligence Future -  Training Coaching Skills Future Ready 

              Home

              About

              Contact Us

 Home About Contact Us 

 Why Leaps? Proactive "Keep It Human" learning solutions to leap into a positive social AI future.  We help individuals and teams learn to leverage their human capacities and well-being in an increasingly Artificial Intelligence (AI) social life dynamics. We believe in putting people first to achieve progress, not artificial intelligence. The biggest challenge of our society is to balance AI efficiency with a genuine, healthy and fair human collaboration at work, home and within our community. We believe in advocating "keep it human" solutions through awareness and experimentation. Humans cannot develop trust and psychological stability at the speed of social digital interaction. Our Leaps Lab uses our unique "learning as we grow model" to make individuals and teams aware of their own experiences with AI technologies. Our labs include support and development through self and group experimentation. Our goal is to help them leap into a new "digital mindset" and skills focused on using the best of machines to enhance the best of people in multiple contexts. Through research and exploratory practice, we have designed unique assessments and tools to measure the positive and negative impact of AI in the development of human capacities, skills and emotions. AI technologies has changed human dynamics. We help you to learn to discover effective ways to deal with generational gaps, collaboration, human abilities and well-being. Also, to learn how to partner with intelligent machines while exploring new human roles. We believe in making commitments to integrating AI positively into our work and life cultures. The next leadership should be ready to guide people and intelligent machines, and to develop new inclusive and fair ways of social, economic and political organisation. Being proactive is the driving principle behind LEAPS. We support this principle with strong evidence from our own and global research. Luis Bohorquez ,  Founder 

 SUPPORT us 

 Luis Bohorquez Luis Bohorquez is a  Keep it Human  advocate. He believes the power is in people to leap into a positive social future with intelligent machines. As a proactive thinker with a multidisciplinary mind, Bohorquez teaches people and organisations how to understand the impact of intelligent machines on their work and personal lives. His goal is to help them build  Keep it Human  solutions to foster their power and well-being. Bohorquez aims to initiate a movement to inspire the best use of the machine to enhance the best of people. Trained as designer, human-digital behavior researcher and coach, Bohorquez is fascinated by the way people integrate human intelligence with machine intelligence to achieve their goals. This integration he calls Next Intelligence. Next Intelligence is defining emerging work and personal cultures, as it has a great impact in the way we think, trust and organise ourselves, our work and our society. He has been researching on these topics from multiple perspectives for the last 7 years in The Netherlands. 

 Contact me 

 Intro 1 

 Intro 9 

 2017 LEAPS   CONTACT US
 WHOLE PROJECT MARK