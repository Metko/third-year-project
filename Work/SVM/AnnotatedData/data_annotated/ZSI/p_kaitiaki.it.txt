---------------------------------------
NEW PAGE: Katiaki artificial intelligence against cyberbullying

Katiaki artificial intelligence against cyberbullying

 Italiano 

 Artificial intelligence against cyberbullying 

 Artificial intelligence against cyberbullying 

 KAITIAKI is an innovative Digital Social Ecosystem based on artificial intelligence technology to fight against cyberbullying, sexting, hate-speech and self-harm. 

 KAITIAKI is an innovative Digital Social Ecosystem based on artificial intelligence technology to fight against cyberbullying, sexting, hate-speech and self-harm. 

 Problem 

 According to the European research  EU Kids Online  11.5 million kids have suffered acts of violence online. Approximately only 29% of parents are aware of these problems. 

 Opportunity 

 The traditional approach to these problems is offline: educational programs (before the episode happens) and psychological assistance (after the problem occurs). 

 Beneficiaries 

 In Europe 43 million families have kids connected to the internet; Kaitiaki allows kids to have greater awareness and to use socials in a responsible way, making them safer and parents serene. 

 Digital social ecosystem 

 Solution 

 Kaitiaki APP 

 is a non-invasive tool that ensures the privacy of kids, it is available to families who want to be alert when their kids are threatened on the social networks. 

 Kaitiaki EDU 

 is a free tool provided to schools to respond to the lack of awareness of the inappropriate language used online by kids. It also provides mapping of the school s risk level. 

 Kaitiaki GAME 

 is a free mobile app that educates kids on the responsible use of the network through the gamification mechanisms. Parents can reward the kids if they keep a certain score. 

 Kaitiaki CHAT 

 provides free top-level information support, using intelligent chatbots, informing teenagers and parents about cyberbullying and other phenomena-related issues. 

 Kaitiaki provides a range of applications with the goal of reducing social impact and improving social inclusion. 

 Social impact 

 Social impact 

 Kaitiaki and its applications are a true social ecosystem that integrates prevention and assistance activities and provides the missing elements: real time monitoring, mapping phenomena and identifying emerging threats. 
 Furthermore Kaitiaki is a startup with a social vocation (SIAVS for the Italian company law) that operates in DSI (Digital social innovation); this type of company provides a measurable social return on investment (SROI) which results from its activities, in order to decrease the phenomena in each country as well as the public spending to prevent and reduce them. 

 Kaitiaki and its applications are a true social ecosystem that integrates prevention and assistance activities and provides the missing elements: real time monitoring, mapping phenomena and identifying emerging threats. 
 Furthermore Kaitiaki is a startup with a social vocation (SIAVS for the Italian company law) that operates in DSI (Digital social innovation); this type of company provides a measurable social return on investment (SROI) which results from its activities, in order to decrease the phenomena in each country as well as the public spending to prevent and reduce them. 

 If you want to be involved in the Kaitiaki project 

 Get in touch 

 Get in touch 

 Name 

 Email Address 

 Message 

 6 + 15  = 

 Riconoscimenti 

 Kaitiaki S.r.l.   Via Tricesimo 200 
33100 Udine (Italy) 
P.IVA 02887990303 
Powered by  Ambasciata Digitale 

 Questo sito utilizza i cookie per migliorare e tracciare la tua esperienza di navigazione. Continuando a visitare il sito ne accetti l'uso.  Avvertenze Legali   Cookies 
 Accetto Privacy
 WHOLE PROJECT MARK