---------------------------------------
NEW PAGE: KOIKI

KOIKI

          REGISTER/LOGIN

 ES  /
           EN 

            THE GREEN DELIVERY SERVICE  BY PEOPLE FROM YOUR NEIGHBORHOOD

            AN EASY WAY OF SHIPING OR  RECEIVING YOUR PARCELS

            WE DELIVER OR PICKUP YOUR PARCELS  AT THE TIME YOU WANT

            WE DELIVER WALKING OR BIKING

            WE WORK FOR YOUR NEIGHBORHOOD  TO BE TRAFFIC AND CARBON FREE

 bootstrap carousel 

 1. SHOP ON-LINE 
 Shop anywhere online and ship  to your Koiki s address 
 2. SHIP TO YOUR  KOIKI'S ADDRESS 
 Your Koiki is a neighboor  living near you 
 3. SCHEDULE YOUR DELIVERY 
 As soon as the transport company delivers the package  to your Koiki he will contact you,  and you will specify the timing 
 4. GET YOUR STUFF 
 You will receive the package  in your home at the time you want 

 BECOME CUSTOMER 

 YOUR DELIVERY 
 Once Registered  you will find out the Koikis  that work in you neighborhood, who you might already know. 
 When you shop online, ship to your personal  Koiki shipping address . 
 You will get notified when your Koiki receives your parcel and you will decide  the exact time  you want it at your home. 
 Price for this service is 1,85  

 YOUR SHIPPING 
 If you want to ship  parcels or letters, contact your Koiki in your neighborhood. 
 He will pick it up at your home  and he send it thru the post or any express company. 
 You will be informed about  the status  of your shipment at any time. 
 Price for this service is of 7,85  within mainland Spain (up to 5kgs) 

 I WANT TO HIRE A KOIKI!! 

 If you become part of Koiki's Community you will help your neighborhood to be less traffic congested and carbon free. 
 We want to change the way your shipments are delivered and picked up and we want it to be in a social and environmental friendly way. 
 The Koikis are people from your neighborhood that will walk or bike to your home at the time you specify. 
 We estimate that by each parcel received or shipped thru our Koiki service you will save up to 0,42 CO2 kgs. 

 FOR THE TIME BEING WE HAVE SAVED: 
 2.750 
 C02 KILOGRAMS 

 PARTNERSHIPS 
 We believe in the Sharing Economy and in the Social benefit. 

        About Us

        Work with us

        News

        FAQ

        Terms and conditions

        Privacy policy

        Contact

 Powered by CouchCMS 

        Do you want to become a Koiki?

        Sustainability

        Vision, Mission and Values

        About Us

        Work with us

        Our Blog

        FAQ

---------------------------------------
NEW PAGE: KOIKI

KOIKI

          REGISTER/LOGIN

 ES  /
           EN 

 Contacto 

 Please contact us at: 

e-mail::  info@koiki.eu 
Phone: + 34 699 45 30 78 

        About Us

        Work with us

        News

        FAQ

        Terms and conditions

        Privacy policy

        Contact

        Do you want to become a Koiki?

        Sustainability

        Vision, Mission and Values

        About Us

        Work with us

        Our Blog

        FAQ

 Powered by CouchCMS 

---------------------------------------
NEW PAGE: KOIKI

KOIKI

          REGISTER/LOGIN

 ES  /
           EN 

 FAQ 

 Koikis 
 1)  How do I become a Koik? 
  Register in our web page. Fill in your data and a member of our team will call you to explain you the next steps. 
 2)  Can I work from my home? 
  Yes, conditioned to your home is safe enough. 
 3)  Do I have to register as an Aut nomo (Spain)? 
  Yes, if you work in Spain, it s necessary for you to register in the R gimen Especial de Aut nomos de la Seguridad Social. We will help you to do it. 
 4)  Can I define my work schedule? 
  Yes, Koiki activity adapts to your time availability. Note that the broader your schedule, the more activity you will have. 
 5)  In which area will I develop my business? 
  Within your neighborhood in distances that do not exceed 10minutes from your home or local shop from which you work. 
 6)  Do I have to deliver on a bike? 
  Not necessarily. You can deliver walking. If you deliver biking your reach is larger thus your activity also increases. 
 7)  What kind of Smartphone should I use? 
  Anyone with with a touch screen, it can be an Iphone, Android, Blackberry, 

 Customers 
 1)  How do I know if there is a Koiki in my neighborhood? 
  It's easy, go to our homepage and register. You will see the Koikis that can give you the service. If none is available you will get notified when one does. 
 2)  How do I receive a shipment? 
  Inform the person or the On-Line shop to ship to your own Koiki s address.Once your Koiki receives the parcel he will inform you, and then, you will decide the time you want the parcel to be delivered to your home. 
 3)  How do I handle a return? 
  Contact your Koiki by email or phone and he will pick up your parcel whenever you want. 
 4)  How does my parcel reach its destination? 
  With the shipping company you choose or with any transport company already working with us as Correos, MRW, Nacex, ... 
 5)  How much does the service cost? 
  Delivery service price is 1,85  . Shipping price will depend on its destination, contact your Koiki who will detail you the different price and services. 
 6)  Can I make free returns to an On-Line Shop with Koiki? 
  Yes, Koiki will gather in any case the package and deliver it to the parcel company that the On-Line Shop has predefined. You will find, within our list of recipients, the On-Line Shops already working with us. If your On-Line Shop is not on the list, Koiki will pick your parcel and pass it on to the parcel company. 

        About Us

        Work with us

        News

        FAQ

        Terms and conditions

        Privacy policy

        Contact

        Do you want to become a Koiki?

        Sustainability

        Vision, Mission and Values

        About Us

        Work with us

        Our Blog

        FAQ

 Powered by CouchCMS 

---------------------------------------
NEW PAGE: KOIKI

KOIKI

          REGISTER/LOGIN

 ES  /
           EN 

 Privacy Policy 

 Pol tica de Privacidad 
 1. Tratamiento de datos 
 Para acceder a y/o utilizar los servicios de KOIKI HOME, SLU es necesario que los usuarios proporcionen previamente a KOIKI HOME, SLU ciertos datos de car cter personal. De conformidad con lo establecido en la Ley Org nica 15/1999 de Protecci n de Datos (LOPD) de Car cter Personal queremos comunicarles que los datos recabados se incorporar n a un fichero registrado en la Agencia Espa ola de Protecci n de Datos, del que es responsable KOIKI HOME, S.L.U, con domicilio social en Calle Frascuelo 34A, 28043 Madrid, inscrita en el Registro Mercantil de Madrid Tomo 32.515, Folio 41, Hoja M 585223 y provista de CIF B-87071288 con las finalidades que se establecen en cada uno de los formularios de recogida establecidos al uso en el presente sitio web y para poder remitirles informaci n de productos y servicios de nuestra entidad y/o sus partners incluso por medio de correo electr nico o similar y poder tratar los datos relativos a la navegaci n disoci ndolos de sus datos personales, y nos autoriza a que conservemos sus datos para las finalidades anteriores incluso una vez finalizada nuestra relaci n en un futuro. Asimismo se informa que una vez disociados los datos, la informaci n relativa a la navegaci n de los usuarios podr  ser utilizada para fines estad sticos y/o comerciales por parte de KOIKI HOME, S.L.U (en adelante KOIKI). 
 KOIKI garantiza que ha adoptado las medidas oportunas de seguridad de nivel b sico en sus instalaciones, sistemas y ficheros. Asimismo, KOIKI garantiza la confidencialidad de los Datos Personales y que  stos no ser n cedidos a terceros. Ello no obstante, KOIKI revelar  a las autoridades p blicas competentes los Datos Personales y cualquier otra informaci n que est  en su poder o sea accesible a trav s de sus sistemas y sea requerida de conformidad con las disposiciones legales y reglamentarias aplicables. 
 En todo caso, el usuario se compromete a proporcionar informaci n cierta en relaci n con sus datos personales y a mantener los datos facilitados a KOIKI actualizados. El usuario responder , en cualquier caso, de la veracidad de los datos facilitados y de mantenerlos actualizados, reserv ndose KOIKI el derecho de excluir de los servicios registrados a todo usuario que haya facilitado datos falsos, sin perjuicio de las dem s acciones que procedan en Derecho. 
 2. Ejercicio de los derechos en materia de protecci n de datos 
 Puede ejercer sus derechos de acceso, rectificaci n, cancelaci n y oposici n dirigi ndose por escrito al domicilio social de KOIKI HOME, S.L.U a o bien enviando un correo electr nico a info@KOIKI.eu  
 La solicitud del usuario debe cumplir el siguiente contenido m nimo legalmente establecido:

 Nombre y apellidos. 
 Fotocopia del DNI, firma electr nica o copia escaneada del DNI. 
 Petici n concreta. 
 Direcci n a efectos de notificaciones. 
 Fecha y firma del solicitante 

 Por la propia naturaleza del servicio y contenidos del Sitio Web, es imprescindible poder remitir a los usuarios registrados comunicaciones comerciales por correo electr nico. Por lo tanto, en caso de que se produjera la cancelaci n de datos prevista en el p rrafo anterior, ello conllevar  de manera autom tica la cesaci n y abandono en la condici n de usuario registrado de KOIKI 
 3. Menores de edad 
 Nuestros servicios est n dirigidos a personas mayores de edad, en el supuesto que algunos de nuestros servicios vayan dirigidos espec ficamente a menores de edad, KOIKI solicitar  la conformidad de los padres o tutores para la recogida de los datos personales o, en su caso, para el tratamiento automatizado de los datos seg n lo expuesto en la legislaci n vigente. Si el usuario registrado es menor de edad, se requiere que cuente con el previo consentimiento de sus padres o tutores antes de proceder a la inclusi n de sus datos personales en los formularios del Sitio Web. 
 4. Utilizaci n de cookies 
 El acceso a esta p gina web puede implicar la utilizaci n de "cookies". Una cookie es un peque o archivo de texto que el navegador recoge y almacena mientras el usuario navega, lo que conlleva que KOIKI HOME, S.L.U. puede conocer determinada informaci n acerca de la navegaci n del usuario por su p gina: 

 Fecha y hora de la  ltima vez que el usuario visit  la web 
 Dise o y contenidos que el usuario escogi  en su  ltima visita a la web 
 Elementos de seguridad que intervienen en el control de acceso a las  reas restringidas 
 Otras circunstancias an logas. 

 Permiti ndole adaptar la misma a las preferencias del mismo en sus siguientes visitas. En caso de que el usuario no desee recibir cookies durante la navegaci n, deber  desactivar la opci n correspondiente de su navegador, de tal forma que impida la instalaci n de aquellas en su ordenador. Las cookies se utilizan con la finalidad de personalizar los servicios que le ofrecemos, facilit ndole informaci n que pueda ser de su inter s. Las cookies no extraen informaci n de su ordenador, ni determinan d nde se encuentra usted. Si usted lo desea puede configurar su navegador para ser avisado en pantalla de la recepci n de cookies y para impedir la instalaci n de cookies en su disco duro. Por favor, consulte las instrucciones y manuales de su navegador para ampliar esta informaci n . Sin embargo, le hacemos notar que, en todo caso, la calidad del funcionamiento de la p gina web puede disminuir. El usuario tiene la opci n de impedir la generaci n de cookies, mediante la selecci n de la correspondiente opci n en su programa navegador. Sin embargo, KOIKI no se responsabiliza de que la desactivaci n de las mismas impida el buen funcionamiento de la p gina web. 
 5. Google Analytics 
 Esta p gina web utiliza Google Analytics, un servicio anal tico de web prestado por Google, Inc., una compa a de Delaware cuya oficina principal est  en 1600 Amphitheatre Parkway, Mountain View (California), CA 94043, Estados Unidos ( Google ). Google Analytics utiliza  cookies , que son archivos de texto ubicados en su ordenador, para ayudar al website a analizar el uso que hacen los usuarios del sitio web. La informaci n que genera la cookie acerca de su uso del website (incluyendo su direcci n IP) ser  directamente transmitida y archivada por Google en los servidores de Estados Unidos. Google usar  esta informaci n por cuenta nuestra con el prop sito de seguir la pista de su uso del website, recopilando informes de la actividad del website y prestando otros servicios relacionados con la actividad del website y el uso de Internet. Google podr  transmitir dicha informaci n a terceros cuando as  se lo requiera la legislaci n, o cuando dichos terceros procesen la informaci n por cuenta de Google. Google no asociar  su direcci n IP con ning n otro dato del que disponga Google. Puede Usted rechazar el tratamiento de los datos o la informaci n rechazando el uso de cookies mediante la selecci n de la configuraci n apropiada de su navegador, sin embargo, el usuario debe saber que si lo hace puede ser que no pueda usar la plena funcionabilidad de este website. Al utilizar este website el usuario consiente el tratamiento de informaci n acerca del usuario por Google en la forma y para los fines arriba indicados. 
 6. Campos de texto libre 
 Los campos de texto libre que, a disposici n del usuario registrado, puedan aparecer en el Sitio Web, tienen como  nica y exclusiva finalidad el recabar informaci n para mejorar la calidad de los Servicios. El usuario registrado no incluir , en aquellos espacios que el Sitio Web pueda ofertar como "campos de texto libre", ning n dato de car cter personal que pueda ser calificado dentro de aquellos datos para los que se exige un nivel de protecci n de tipo medio o alto, sin previo aviso a KOIKI, seg n define la normativa vigente (a t tulo enunciativo y no limitativo datos relativos a ideolog a, religi n, creencias, afiliaci n sindical, salud, origen racial y/o vida sexual). 

        About Us

        Work with us

        News

        FAQ

        Terms and conditions

        Privacy policy

        Contact

        Do you want to become a Koiki?

        Sustainability

        Vision, Mission and Values

        About Us

        Work with us

        Our Blog

        FAQ

 Powered by CouchCMS 

---------------------------------------
NEW PAGE: KOIKI

KOIKI

          REGISTER/LOGIN

 ES  /
           EN 

 News 

        About Us

        Work with us

        News

        FAQ

        Terms and conditions

        Privacy policy

        Contact

        Do you want to become a Koiki?

        Sustainability

        Vision, Mission and Values

        About Us

        Work with us

        Our Blog

        FAQ

 Powered by CouchCMS 

---------------------------------------
NEW PAGE: CouchCMS

CouchCMS

 Username 

 Password 

 Forgot your password?   

---------------------------------------
NEW PAGE: KOIKI

KOIKI

          REGISTER/LOGIN

 ES  /
           EN 

 Work with us 

 We are permanently searching for people with social and environmental concerns to come on board in our management team or as a Koiki messenger 
 Please send us your CV and comments to:   quieroserkoiki@koiki.eu 

        About Us

        Work with us

        News

        FAQ

        Terms and conditions

        Privacy policy

        Contact

        Do you want to become a Koiki?

        Sustainability

        Vision, Mission and Values

        About Us

        Work with us

        Our Blog

        FAQ

 Powered by CouchCMS 

---------------------------------------
NEW PAGE: KOIKI

KOIKI

          REGISTER/LOGIN

 ES  /
           EN 

 About us 

 For the time being we are a small group of people that want to change how he last mille is performed within our neighborhoods. 
 We firmly believe that our cities and our way of living should and actually can be more social and environmental friendly. 

 Koiki Team 

  Aitor Ojanguren 
 Founder 

  Jos  Andr s Aliste 
 CIO 

  Cristina Arganda 
 Trainer 

  Lis Brusa 
 Trainer 

  Eduardo Camacho 
 Administration and finance 

        About Us

        Work with us

        News

        FAQ

        Terms and conditions

        Privacy policy

        Contact

        Do you want to become a Koiki?

        Sustainability

        Vision, Mission and Values

        About Us

        Work with us

        Our Blog

        FAQ

 Powered by CouchCMS
 WHOLE PROJECT MARK