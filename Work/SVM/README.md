## Implementation Of Text Summarization using SVMs

### Model Diagrams

Binary SVM
<div  align="center">
<img src="images/binary_svm.png" align=center />
</div>

Topics SVM
<div  align="center">
<img src="images/topic_svm.png" align=center />
</div>

## Types of Models
1. `models/binary_svm.joblib` - Trained on the preprocessed KNOWMAK dataset crawled by me.
2. `models/*_svm.joblib` - Trained on KNOWMAK preprocessed annotation dataset. It outputs all the sentences that it classifies as containing certain topics.
3. `models/*_svm.joblib` - Trained on KNOWMAK preprocessed annotation dataset It outputs the top `sents_per_topic` sentences that it classifies as containing certain topics.
4. `models/binary_svm_big_dataset.joblib` - Trained on the **not** preprocessed whole KNOWMAK dataset

### Setup

Implementation of Binary SVM - `Word Sentence SVM Combined` notebook.
Implementation of Topics SVMs using preprocessed annotation dataset - `AnnotatedData/Preprocessed Annotated Data Parsing` notebook.
Implementation of Topics SVMs using **not** preprocessed annotation dataset - `AnnotatedData/Annotated Data Parsing` notebook.

### Usage  

```shell
# run
python3 script/main.py -type 1 -input input.json -output output.json -sents_per_topic 2
```
There are 4 parameters that the script takes:
1. `type` - Select one of the types of pretrained models (1, 2, 3)
2. `input` - This is a *json* file with the texts that you want to summarize. Its format should be:
```json
[
    {"document" : "..."},
    {"document" : "..."},
    ...
]
```
3. `output` - This is the *json* file that would be generated and it would contain the summaries:
```json
[
    {"document" : "...", "summary" : "..."},
    {"document" : "...", "summary" : "..."},
    ...
]
```
4. `sents_per_topic` - Select the number of sentences you want per topic when using model `3`


