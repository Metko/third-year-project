from flask import Flask, render_template, flash, request
from form import TextForm
from validators import url
from boilerpipe.extract import Extractor
from text_processing import preprocess_text
from topic_svm import svms
from summarunner import summarise
from binary_svm import svm
import tfidf_summarize

app = Flask(__name__)
app.config.from_object(__name__)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'

def extract_text_from_input(input):
    try:
        if url(input):
            return Extractor(extractor='DefaultExtractor', url=input).getText()
        else:
            return input
    except:
        return None

def get_summaries(text):
    svm_binary = svm.get_summary(text)
    svm_topic = svms.get_summary(text)
    svm_topic_2sents = svms.get_summary_by_prob(text)
    sumarunner = summarise.summarise(text)
    tfidf = tfidf_summarize.summarise(text)
    return svm_binary, svm_topic, svm_topic_2sents, sumarunner, tfidf

@app.route("/", methods=['GET', 'POST'])
def hello():
    form = TextForm(request.form)
    if request.method == 'POST':
        text = request.form['text']
        text = extract_text_from_input(text)
        if form.validate() and text:
            text = preprocess_text(text)
            binary, a, b, summary, tfidf = get_summaries(text)
            return render_template('form.html', form=form, summarunner=summary, topic_svm=a,
                                   topic_svm_short=b, text=text, binary_svm=binary, tfidf=tfidf)
        else:
            flash('Error! Could not fetch text.')
    return render_template('form.html', form=form)


if __name__ == '__main__':
    app.run()
