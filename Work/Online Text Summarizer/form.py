from wtforms import Form, validators, StringField

class TextForm(Form):
    text = StringField('Text or URL:', validators=[validators.required()])