from joblib import load

def get_summary(text):
    svm = load('./binary_svm/binary_svm.joblib')
    sents = text.split('\n')
    predicted = svm.predict(sents)
    summary = list()
    for index, predict in enumerate(predicted):
        if int(predict) == 1:
            summary.append(sents[index])
    return ' '.join(summary)