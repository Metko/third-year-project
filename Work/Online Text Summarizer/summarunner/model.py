import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

USE_CUDA = False
data_folder = 'data'

class SummaRuNNer(nn.Module):
    def __init__(self, word_embed_input_size, word_embed_dim, hidden_size, pos_input_size, seg_input_size,
                 pos_embed_dim):
        super(SummaRuNNer, self).__init__()
        self.hidden_size = hidden_size

        self.word_embedding = nn.Embedding(word_embed_input_size, word_embed_dim, padding_idx=0)
        self.word_RNN = nn.GRU(
            input_size=word_embed_dim,
            hidden_size=hidden_size,
            batch_first=True,
            bidirectional=True
        )
        self.sent_RNN = nn.GRU(
            input_size=2 * hidden_size,
            hidden_size=hidden_size,
            batch_first=True,
            bidirectional=True
        )
        self.doc_transform = nn.Linear(2 * hidden_size, 2 * hidden_size)

        # Position Embeddings
        self.abs_pos_embed = nn.Embedding(pos_input_size, pos_embed_dim)
        self.rel_pos_embed = nn.Embedding(seg_input_size, pos_embed_dim)

        # Classification Layer Parameters
        self.content = nn.Linear(2 * hidden_size, 1, bias=False)
        self.salience = nn.Bilinear(2 * hidden_size, 2 * hidden_size, 1, bias=False)
        self.novelty = nn.Bilinear(2 * hidden_size, 2 * hidden_size, 1, bias=False)
        self.abs_pos = nn.Linear(pos_embed_dim, 1, bias=False)
        self.rel_pos = nn.Linear(pos_embed_dim, 1, bias=False)
        self.bias = nn.Parameter(torch.FloatTensor(1).uniform_(-0.1, 0.1))

    def load_word_embeddings(self, embeddings):
        self.word_embeddings.weight.data.copy_(embeddings)

    # N - number of Sentences
    # W - number of Words (words_per_sent_max)
    # D - word embedding dimensions (word_embed_dim)
    # H - hidden dimension size (hidden_size)
    def forward(self, input_seqs, doc_lens):
        seq_lens = torch.sum(torch.sign(input_seqs), dim=1).data  # (N)

        # Word Embeddings Layer
        input_seqs = self.word_embedding(input_seqs)  # (N, W, D)
        # Word GRU Layer
        input_seqs, word_hidden = self.word_RNN(input_seqs)  # (N, W, 2*H), (2, N, H)
        words_pooled = self.max_pool1d(input_seqs, seq_lens)  # (N, 2*H)
        input_docs = self.group_by_docs(words_pooled,
                                        doc_lens)  # (BATCH_SIZE, max_doc_len, 2*H) max_doc_len represent sentences
        # Sentence GRU Layer
        input_docs, sent_hidden = self.sent_RNN(input_docs)  # (BATCH_SIZE, max_doc_len, 2*H), (2, BATCH_SIZE, H)
        sents_pooled = self.max_pool1d(input_docs, doc_lens)  # (BATCH_SIZE, 2*H)

        probs = []
        for index, doc_len in enumerate(doc_lens):
            doc = torch.tanh(self.doc_transform(sents_pooled[index])).unsqueeze(0)
            s = Variable(torch.zeros(1, 2 * self.hidden_size))
            if USE_CUDA:
                s = s.cuda()
            input_doc = input_docs[index, :doc_len, :]  # (doc_len, 2*H)
            # Classification Layer
            for sent_index, hidden in enumerate(input_doc):  # position is the i-th sentence and hidden - its weights
                hidden = hidden.view(1, -1)  # use unsqueeze(0)?
                prob, s = self.classify_doc(sent_index, hidden, doc, s, doc_len)
                probs.append(prob)
        return torch.cat(probs).squeeze()

    def max_pool1d(self, seqs, seq_lens):  # seqs: (N, W, 2*H), seq_lens: (N)
        out = []
        for index, seq in enumerate(seqs):
            seq = seq[:seq_lens[index], :]
            seq = torch.t(seq).unsqueeze(0)
            out.append(F.max_pool1d(seq, seq.size(2)))
        return torch.cat(out).squeeze(2)  # (N, 2*H)

    def group_by_docs(self, seqs, doc_lens):  # seqs: (N, 2*H), doc_lens: (BATCH_SIZE)
        pad_dim = seqs.size(1)  # 2*H
        max_doc_len = max(doc_lens)
        docs = []
        start = 0
        for doc_len in doc_lens:
            stop = start + doc_len
            doc = seqs[start:stop]  # (doc_len, 2*H)
            docs.append(self.pad_doc(doc, max_doc_len, pad_dim))
            start = stop
        return torch.cat(docs, dim=0)  # (BATCH_SIZE, max_doc_len, 2*H)

    def pad_doc(self, doc, max_doc_len, pad_dim):
        if doc.size(0) == max_doc_len:  # doc.size(0) == doc_len
            return doc.unsqueeze(0)  # (1, max_doc_len, 2*H) max_doc_len == doc_len
        else:
            pad_len = max_doc_len - doc.size(0)
            pad = Variable(torch.zeros(pad_len, pad_dim))
            if USE_CUDA:
                pad = pad.cuda()
            return torch.cat([doc, pad]).unsqueeze(0)  # (1, max_doc_len, 2*H)

    def classify_doc(self, sent_index, hidden, doc, s, doc_len):
        content = self.content(hidden)
        salience = self.salience(hidden, doc)
        novelty = -1 * self.novelty(hidden, F.tanh(s))
        abs_p = self.abs_pos(self.get_abs_features(sent_index))
        rel_p = self.rel_pos(self.get_rel_features(sent_index, doc_len))
        prob = torch.sigmoid(content + salience + novelty + abs_p + rel_p + self.bias)
        s = s + torch.mm(prob, hidden)
        return prob, s

    def get_abs_features(self, sent_position):
        abs_index = Variable(torch.LongTensor([[sent_position]]))
        if USE_CUDA:
            abs_index = abs_index.cuda()
        return self.abs_pos_embed(abs_index).squeeze(0)

    def get_rel_features(self, sent_position, doc_len):
        rel_index = int(round((sent_position + 1) * 9.0 / doc_len))
        rel_index = Variable(torch.LongTensor([[rel_index]]))
        if USE_CUDA:
            rel_index = rel_index.cuda()
        return self.rel_pos_embed(rel_index).squeeze(0)

    def save(self):
        checkpoint = self.state_dict()
        path = data_folder + '/SummaRuNNer.pt'
        torch.save(checkpoint, path)

    def load(self, path=data_folder + '/SummaRuNNer.pt'):
        if USE_CUDA:
            data = torch.load(path)
        else:
            data = torch.load(path, map_location=lambda storage, loc: storage)
        self.load_state_dict(data)
        if USE_CUDA:
            return self.cuda()
        else:
            return self