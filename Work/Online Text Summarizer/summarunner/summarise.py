import numpy as np
import torch
from torch.autograd import Variable
from summarunner.model import SummaRuNNer, USE_CUDA
import json
from summarunner.vocabulary import Vocab, PAD_TOKEN

def load_embeddings(file_name='embedding.npz'):
    #print('Reading word embeddings...')
    file = np.load('./summarunner/' + file_name)
    return torch.Tensor(file['embedding'])


embeddings = load_embeddings()
WORD_EMBEDDING_INPUT_SIZE = embeddings.size(0)
WORD_EMBEDDING_DIM = embeddings.size(1)
HIDDEN_LAYER_SIZE = 200
POS_INPUT_SIZE = 100 # bound by sents_per_doc_max
SEG_INPUT_SIZE = 10
POS_EMBED_DIM = 50

model = SummaRuNNer(WORD_EMBEDDING_INPUT_SIZE,
                    WORD_EMBEDDING_DIM,
                    HIDDEN_LAYER_SIZE,
                    POS_INPUT_SIZE,
                    SEG_INPUT_SIZE,
                    POS_EMBED_DIM
                   )
model.load('./summarunner/SIPSummaRuNNerPretrained.pt')

SUMMARY_SENTENCES = 3


def summarise(text):
    sents = text.split(SPLIT_TOKEN)
    if len(sents) == 1:  # should this be here?
        return sents[0]
    sents = sents[:min(sents_per_doc_max, len(sents))]
    doc_len = len(sents)
    tokenized_sents = tokenize_sents(sents)
    encoded_sents = encode_and_pad_tokenized_sents(tokenized_sents)
    sents = Variable(torch.LongTensor(encoded_sents))

    if USE_CUDA:
        sents = sents.cuda()

    probs = model(sents, [doc_len])

    summary_len = min(doc_len, SUMMARY_SENTENCES)
    topk_indices = probs.topk(summary_len)[1].cpu().data.numpy()
    topk_indices.sort()
    doc = text.split(SPLIT_TOKEN)[:doc_len]
    result = [doc[index] for index in topk_indices]
    # print(result)
    return ' '.join(result)


SPLIT_TOKEN = '\n'
words_per_sent_max = 50
sents_per_doc_max = 100

def tokenize_sents(sents):
    tokenized_sents = []
    for sent in sents:
        words = sent.split()
        # Truncate sentence
        if len(words) > words_per_sent_max:
            words = words[:words_per_sent_max]
        tokenized_sents.append(words)
    return tokenized_sents


# Replaces words with their corresponding ids
def encode_and_pad_tokenized_sents(sents):
    encoded_sents = []
    for sent in sents:
        pad_length = words_per_sent_max - len(sent)
        encoded_sent = [vocab.word2idx(word) for word in sent] + [PAD_TOKEN for _ in range(pad_length)]
        encoded_sents.append(encoded_sent)
    return encoded_sents




def load_word2id(file_name='word2id.json'):
    word2id = dict()
    with open('./summarunner/' + file_name) as file:
        word2id = json.load(file)
    return word2id

vocab = Vocab(load_word2id())
