import numpy as np
from joblib import load



def convert_to_labels(sent_probs):
    probs = sent_probs.copy()
    probs[::-1].sort()
    for index, prob in enumerate(sent_probs):
        if prob in probs[:2]:
            sent_probs[index] = 1
        else:
            sent_probs[index] = 0
    return sent_probs


def get_summary_by_prob(text):
    objective_svm = load('./topic_svm/objective_svm.joblib')
    actor_svm = load('./topic_svm/actor_svm.joblib')
    innovation_svm = load('./topic_svm/innovation_svm.joblib')
    output_svm = load('./topic_svm/output_svm.joblib')
    sents = text.split('\n')
    objectives = convert_to_labels(objective_svm.predict_proba(sents)[:, 1])
    actors = convert_to_labels(actor_svm.predict_proba(sents)[:, 1])
    innovations = convert_to_labels(innovation_svm.predict_proba(sents)[:, 1])
    outputs = convert_to_labels(output_svm.predict_proba(sents)[:, 1])

    predicted = np.logical_or(objectives, actors)
    predicted = np.logical_or(predicted, innovations)
    predicted = np.logical_or(predicted, outputs)

    summary = list()
    for index, predict in enumerate(predicted):
        if int(predict) == 1:
            summary.append(sents[index])
    return ' '.join(summary)


def get_summary(text):
    objective_svm = load('./topic_svm/objective_svm.joblib')
    actor_svm = load('./topic_svm/actor_svm.joblib')
    innovation_svm = load('./topic_svm/innovation_svm.joblib')
    output_svm = load('./topic_svm/output_svm.joblib')
    sents = text.split('\n')

    objectives = objective_svm.predict(sents)
    actors = actor_svm.predict(sents)
    innovations = innovation_svm.predict(sents)
    outputs = output_svm.predict(sents)

    predicted = np.logical_or(objectives, actors)
    predicted = np.logical_or(predicted, innovations)
    predicted = np.logical_or(predicted, outputs)

    summary = list()
    for index, predict in enumerate(predicted):
        if int(predict) == 1:
            summary.append(sents[index])
    return ' '.join(summary)